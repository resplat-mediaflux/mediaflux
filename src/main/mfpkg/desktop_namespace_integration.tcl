proc createDeclaredServiceCollection { COLLECTION_NAME } {

# The service collection will show up in the Desktop when the
# user right clicks on a namespace (that the collection is 
# registered with).    The 'namespace' argument
# of the declared services will be replaced by the actual 
# namespace the user has clicked on. The other arguments will
# be visible.
#
# Declare the service(s), replacing if pre-existing

# Sum a namespace
system.service.add :replace-if-exists true :name unimelb.namespace.sum \
   :description "Recursively count and sum the assets in the namespace." \
   :label "Recursively count and sum namespace" \
   :access access \
   :definition < \
      :element -name namespace -type string -min-occurs 1 -max-occurs 1 \
    > \
    :execute "unimelb.asset.namespace.child.sum \
       :namespace \[xvalue namespace \$args\]"
       
# Compute and email checksums on all assets in a namespace
system.service.add :replace-if-exists true :name unimelb.namespace.checksum \
   :description "Recursively compute the checksum for assets in the namespace and send via email as a CSV file.  May fail to email if there are too many lines in the CSV file." \
   :label "Recursively compute checksums in namespace" \
   :access access \
   :definition < \
   	  :element -name nb-threads -type integer -min-occurs 0 -max-occurs 1 -default 1 < \
   	      :description "Number of parallel threads - defaults to 4" \
		  :restriction -base integer < \
				:minimum 1 \
				:maximum 8 \
		  > \
	   > \
      :element -name namespace -type string -min-occurs 1 -max-occurs 1 \
      :element -name algorithm -type enumeration -min-occurs 1 -max-occurs 1 < \
         :description "Choose the checksum algorithm" \
         :restriction -base "enumeration" \
          < \
            :value "crc32" \
            :value "md5" \
            :value sha-1 \
			:value sha-256 \
			:value sha-384 \
			:value sha-512 \
			:value aws-tree-sha-256 \
          > \
      > \
      :element -name email  -type string -min-occurs 1 -max-occurs 1 \
    > \
    :execute "unimelb.asset.content.checksum.export \
       :where namespace>='\[xvalue namespace \$args\]' \
       :nb-threads \[xvalue nb-threads \$args\] \
       :algorithm \[xvalue algorithm \$args\] \
       :email \[xvalue email \$args\]"

# Copy a namespace
system.service.add :replace-if-exists true :name unimelb.namespace.copy \
   :description "Copy one namespace (including any meta-data) to a new destination. Any ACLs are dropped." \
   :label "Copy namespace" \
   :access access \
   :definition < \
      :element -name namespace -type string -min-occurs 1 -max-occurs 1 \
      :element -name to -type string -min-occurs 1 -max-occurs 1 < :description "Destination namespace" > \
    > \
    :execute "unimelb.asset.copy :nb-workers 4 \
       :namespace \[xvalue namespace \$args\] \
       :to \[xvalue to \$args\]"
   
# Add a namespace tracking asset   
system.service.add :replace-if-exists true :name unimelb.namespace.tracking.create \
   :description "Creates a ‘tracking’ asset in every child (recursion can be disabled) namespace (folder) that can be queried for (by name or type) as a proxy for the namespace (namespaces cannot be queried for). If the asset pre-exists, it will be skipped.  The asset is named <namespace>.ns where <namespace> is the child part of the namespace path.  The type of the asset is ‘uom-ans’. The full path of the namespace is stored in the meta-data element mf-name/name. For example, if the namespace in which an asset was created was called /a/b/c then the asset would be named ‘c.ns’, would have type uom-ans, and the meta-data element mf-name/name would have the value ‘/a/b/c’." \
   :label "Create namespace tracking assets" \
   :access access \
   :definition < \
      :element -name namespace -type string -min-occurs 1 -max-occurs 1 < :description "The parent namespace." > \
      :element -name recurse -type boolean -min-occurs 1 -max-occurs 1 < :description "Should the process recurse down the namespace tree." > \
    > \
    :execute "unimelb.asset.namespace.asset.tracking.create \
    :namespace \[xvalue namespace \$args\] \
    :recurse \[xvalue recurse \$args\]" 

# Fetch by sftp  
system.service.add :replace-if-exists true :name unimelb.sftp.download \
   :description "Downloads a file or folder of files (recursively) from an sFTP server." \
   :label "Download from sFTP server" \
   :access access \
   :definition < \
  	  :element -name host -type string -min-occurs 1 -max-occurs 1 < :description  "SSH server host address" > \
	  :element -name port -type integer -min-occurs 0 -max-occurs 1 -default 22 < :description "SSH server port. Defaults 22." > \
	  :element -name user -type string -min-occurs 1 -max-occurs 1 < :description "SSH user name." > \
	  :element -name password -type password -min-occurs 1 -max-occurs 1 < :description "SSH user's password." > \
      :element -name path -type string -min-occurs 1 -max-occurs 1 < :description "Remote source file path. Maybe a file or a folder." > \
      :element -name namespace -type string -min-occurs 1 -max-occurs 1 < :description "Destination namespace to download the data to." > \
      :element -name if-exists -type enumeration -min-occurs 1 -max-occurs 1 -default ignore < \
         :description "The action to take if the asset pre-exists at the Mediaflux destination and the file sizes match. Defaults to skip. update means it will upload again and create a new version for the asset." \
         :restriction -base "enumeration" \
          < \
            :value "ignore" \
            :value "update" \
           > \
      > \
      :element -name nb-threads -type string -min-occurs 0 -max-occurs 1 -default 1 < :description "Number of parallel threads (max 5)." > \
      :element -name notify -type boolean -default true -min-occurs 0 -max-occurs 1 \
              < :description "Send email notification to the current user when the download completes." > \
      :element -name stop-on-error -type boolean -default false -min-occurs 0 -max-occurs 1 \
              < :description "If an error occurs either stop processing (true) or continue on with the next file (false)." > \
     > \
    :execute "unimelb.sftp.get \
        :namespace \[xvalue namespace \$args\]\
        :stop-on-error \[xvalue stop-on-error \$args\] \
        :host \[xvalue host \$args\] \
        :port \[xvalue port \$args\] \
        :user \[xvalue user \$args\] \
        :password \[xvalue password \$args\] \
        :path \[xvalue path \$args\] \
        :nb-workers \[xvalue nb-threads \$args\] \
        :if-exists \[xvalue if-exists \$args\] \
        :notify < :self \[xvalue notify \$args\] >"


# Defect 3911 prevents the attribute retry from being presented
#	  :element -name on-error -type enumeration -min-occurs 0 -max-occurs 1 -default stop \
#	     < \
#	       :description "The behaviour to exhibit when encountering an error. Defaults to 'stop'." \
#	       :restriction -base enumeration < :value continue :value stop > \
#		   :attribute -name retry -type integer -min-occurs 0 -default 0 \
#		      < \
#		         :description "Number of retry attempts. Defaults to 0" \
#				 :restriction -base integer < :minimum 0 > \
#		      > \
#       > \

# asset and namespace name validation
system.service.add :replace-if-exists true :name unimelb.name.validate \
   :description "Validates namespace and asset names for viability across Unix and Windows - see https://rcs-knowledge-hub.atlassian.net/wiki/spaces/KB/pages/5474752/Valid+file+names+in+Mediaflux" \
   :label "Validate namespace and asset names." \
   :access access \
   :definition < \
      :element -name namespace -type string -min-occurs 1 -max-occurs 1 < :description "Recursively check this namespace." > \
      :element -name email  -type string -min-occurs 1 -max-occurs 1 < :description "Recipient email address will receive a gzipped CSV file." > \
     > \
    :execute "unimelb.asset.name.validate \
        :compress true \
        :namespace -recursive true \[xvalue namespace \$args\] \
        :notify < :email \[xvalue email \$args\] >"
        
        
        
# set exclusive user ACL. Only available to project administrators
# The :precondition does not work (see ticket 5100) so this is not
# deployed
system.service.add :replace-if-exists true :name unimelb.namespace.user.exclude \
   :description "Sets an ACL to exclude the specified user from the current namespace." \
   :label "Set exclusive user ACL" \
   :access access \
   :precondition "return  false" \
   :definition < \
      :element -name namespace -type string -min-occurs 1 -max-occurs 1 < :description "Set ACL on this namespace." > \
      :element -name user -type string -min-occurs 1 -max-occurs 1 < :description "The user name of the form <domain>:<user> E.g. unimelb:nkilleen" > \
    > \
    :execute "unimelb.asset.namespace.acl.actor.exclude \
        :actor -type user \[xvalue user \$args\] \
        :namespace \[xvalue namespace \$args\]"

#   :precondition "return \[vicnode.user.self.project.admin.is :namespace \[xvalue namespace \$args\]\]" \
#   :precondition "if { \[xvalue admin \[vicnode.user.self.project.admin.is :namespace \[xvalue namespace \$args\]\]\] == \\\"true\\\" } { return true; } else { return false; }" \


# Create a service collection and add the desired declared services
# The service collection gets added to namespaces via the service  asset.namespace.service.collection.add 
# To complete the work the provisioning system needs to call this service to add this service collection
# Needs to be done via application variables to pass into the run-time system
# child namespaces inherit the service collection
if { [xvalue exists [system.service.collection.exists :name ${COLLECTION_NAME}]] == "false" } {

# Create/update the service collection which is registered with
# project namespaces
   system.service.collection.create :name ${COLLECTION_NAME} \
      :include unimelb.namespace.sum \
      :include unimelb.namespace.checksum \
      :include unimelb.namespace.copy \
      :include unimelb.sftp.download \
      :include unimelb.name.validate
 } else {
    system.service.collection.set :name ${COLLECTION_NAME} \
      :include unimelb.namespace.sum \
      :include unimelb.namespace.checksum \
      :include unimelb.namespace.copy \
      :include unimelb.sftp.download \
      :include unimelb.name.validate
 }
 # If a namespace is moved these become useless... 
 # I have seen people do this. So retiring for now
 #      :include unimelb.namespace.tracking.create \
 
}
