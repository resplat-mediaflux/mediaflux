#
# author Neil Killeen
#
# Copyright (c) 2016, The University of Melbourne, Australia
#
# All rights reserved.
#

#############
proc install_explorer_basic_asset_query { NS QNS } {

# This procedure installs a basic (non-domain specific) query that all users
# will get when they use the Mediaflux Explorer

if { [xvalue exists [asset.namespace.exists :namespace "${NS}/${QNS}"]] == "false" } {
	asset.namespace.create :namespace -all true "${NS}/${QNS}"
}	

set NS_ID [xvalue "namespace/@id" [asset.namespace.describe :namespace "/"]]

# Create the basic asset query template
asset.set :create true :id "path=$NS/$QNS/basic-asset-query-with-matches" \
	:meta \
	< \
		:arc.gui.client:application-template \
		< \
			:app "explorer" \
			:name "basic-asset-query" \
			:label "Basic asset query with name matching" \
			:description "Query for assets using basic metadata and name wildcard matching" \
			:criteria "search" \
			:namespace-id -scope all $NS_ID \
		> \
	> \
	:in archive:Queries/basic-asset-query-explorer-with-matches.xml
}

##############
proc install_explorer_basic_text_query { NS QNS TYPE} {

# This procedure configures the simple text search box for either literal or 
# token based searches when using the Explorer
#
# TYPE should be 'literal' or 'token'

if { [xvalue exists [asset.namespace.exists :namespace "${NS}/${QNS}"]] == "false" } {
	asset.namespace.create :namespace -all true "${NS}/${QNS}"
}	

set NS_ID [xvalue "namespace/@id" [asset.namespace.describe :namespace "/"]]

 if { $TYPE == "literal" } {

# Literal based
asset.set :create true :id "path=${NS}/${QNS}/basic-text-query" \
    :meta < \
       :arc.gui.client:application-template \
       < \
          :app "explorer" \
          :name "Explorer configuration" \
          :label "Explorer configuration" \
          :description "Explorer configuration options" \
          :criteria "application-configuration" \
       > \
     > \
    :in archive:Queries/basic-text-literal-query-explorer.xml

} else {

# Token based
asset.set :create true :id "path=${NS}/${QNS}/basic-text-query" \
    :meta < \
       :arc.gui.client:application-template \
       < \
          :app "explorer" \
          :name "Explorer configuration" \
          :label "Explorer configuration" \
          :description "Explorer configuration options" \
          :criteria "application-configuration" \
       > \
     > \
    :in archive:Queries/basic-text-token-query-explorer.xml
}
}


#############
proc install_explorer_shareable_upload_manifest_query { NS QNS } {

# This procedure installs a query to search for shareable upload manifest assets that all users
# will get when they use the Mediaflux Explorer

if { [xvalue exists [asset.namespace.exists :namespace "${NS}/${QNS}"]] == "false" } {
	asset.namespace.create :namespace -all true "${NS}/${QNS}"
}	

set NS_ID [xvalue "namespace/@id" [asset.namespace.describe :namespace "/"]]

# Create the basic asset query template
asset.set :create true :id "path=${NS}/${QNS}/shareable-upload-manifest-query" \
	:meta \
	< \
		:arc.gui.client:application-template \
		< \
			:app "explorer" \
			:name "shareable-upload-manifest-query" \
			:label "Shareable upload manifest query" \
			:description "Query for shareable upload manifest assets." \
			:criteria "search" \
			:namespace-id -scope all $NS_ID \
		> \
	> \
	:in archive:Queries/shareable-upload-manifest-query-explorer.xml
}