#
# author Neil Killeen
#
# Copyright (c) 2016, The University of Melbourne, Australia
#
# All rights reserved.
#


#############
proc install_desktop_shareable_upload_manifest_query { NS QNS } {

# This procedure installs a query to search for shareable upload manifest assets that all users
# will get when they use the Mediaflux Desktop

if { [xvalue exists [asset.namespace.exists :namespace "${NS}/${QNS}"]] == "false" } {
	asset.namespace.create :namespace -all true "${NS}/${QNS}"
}	

asset.set :create true :id "path=${NS}/${QNS}/shareable-upload-manifest-query-desktop" \
    :type "application/arc-finder-view" \
	:in archive:Queries/shareable-upload-manifest-query-desktop.xml
}