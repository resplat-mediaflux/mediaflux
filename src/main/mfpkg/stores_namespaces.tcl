#
# author Neil Killeen
#
# Copyright (c) 2016, The University of Melbourne, Australia
#
# All rights reserved.
#
proc create_stores_namespaces { ASSET_NS ASSET_NS_ADMIN \
                                ASSET_NS_INSTRUMENT_UPLOAD_MANIFESTS \
                                ROLE_NS ROLE_NS_ADMIN \
                                DOC_NS DOC_NS_ADMIN \
                                DICT_NS DICT_NS_ADMIN \
                                DEFAULT_STORE DEFAULT_STORE_POLICY } {


# Default  store.  This is the default store used for various namespaces.
# AFter its creation in the default file-system location volatile/stores
# you may have to move it elsewhere
if { [xvalue exists [asset.store.exists :name $DEFAULT_STORE]] == "false" } {
     asset.store.create :name $DEFAULT_STORE :type file-system :description "Default store supporting various activities (e.g. project provisioning)"
}


# Make a store policy to go with the store. The provisioning system works with store policies
if { [xvalue exists [asset.store.policy.exists :name $DEFAULT_STORE_POLICY]] == "false" } {
     asset.store.policy.create :name $DEFAULT_STORE_POLICY :store $DEFAULT_STORE :type balanced \
         :description "Primary (balanced) store policy supporting project provisioning."
}


# Admin  namespaces
if { [xvalue exists [asset.namespace.exists :namespace $ASSET_NS_ADMIN]] == "false" } {
   asset.namespace.create :namespace $ASSET_NS_ADMIN :store $DEFAULT_STORE :description "Asset namespace supporting project provisioning administration activities."
}

# Asset namespace for facility/instrument DM upload manifests 
if { [xvalue exists [asset.namespace.exists :namespace $ASSET_NS_INSTRUMENT_UPLOAD_MANIFESTS]] == "false" } {
   asset.namespace.create :namespace $ASSET_NS_INSTRUMENT_UPLOAD_MANIFESTS :store $DEFAULT_STORE :description "Asset namespace to keep the global/extra copy of instrument upload manifests."
}

if { [xvalue exists [asset.doc.namespace.exists :namespace $DOC_NS_ADMIN]] == "false" } {
   asset.doc.namespace.create :namespace $DOC_NS_ADMIN :description "Document namespace supporting project provisioning administration activities."
}

if { [xvalue exists [dictionary.namespace.exists :namespace $DICT_NS_ADMIN]] == "false" } {
  dictionary.namespace.create :namespace $DICT_NS_ADMIN :description "Dictionary namespace supporting project provisioning administration activities."
}

if { [xvalue exists [authorization.role.namespace.exists :namespace ${ROLE_NS_ADMIN}]] == "false" } {
  authorization.role.namespace.create :namespace ${ROLE_NS_ADMIN} :description "Role namespace supporting project provisioning administration activities"
}

# User Onboarding assets
if { [xvalue exists [asset.namespace.exists :namespace $ASSET_NS_ADMIN/Onboarding]] == "false" } {
  asset.namespace.create :namespace $ASSET_NS_ADMIN/Onboarding :description "Asset namespace supporting project provisioning user onboarding process activities."
}


# Namespaces that users access
if { [xvalue exists [asset.namespace.exists :namespace $ASSET_NS]] == "false" } {
   # namespace is accessible but not visible to non admins (the visibility is set in script role-permissions.tcl when the roles are available)
   asset.namespace.create :namespace $ASSET_NS :store $DEFAULT_STORE :description "Asset namespace supporting end user activities (e.g. Queries)." 
}

if { [xvalue exists [asset.doc.namespace.exists :namespace $DOC_NS]] == "false" } {
  asset.doc.namespace.create :namespace $DOC_NS :description "Document namespace supporting end users."
}

if { [xvalue exists [dictionary.namespace.exists :namespace $DICT_NS]] == "false" } {
  dictionary.namespace.create :namespace $DICT_NS :description "Dictionary namespace supporting end users."
}

if { [xvalue exists [authorization.role.namespace.exists :namespace $ROLE_NS]] == "false" } {
  authorization.role.namespace.create :namespace $ROLE_NS :description "Role namespace supporting end users."
}


#####

# Projects root namespace. We create it here (rather than allowing the underlying provisioning framework to do it)
# so that we can grant it the right ACLs 
set PROJECTROOT [xvalue property [server.property.get :name "projects.namespace"]]
if { [xvalue exists [asset.namespace.exists :namespace ${PROJECTROOT}]] == "false" } {
     asset.namespace.create :namespace ${PROJECTROOT} :store-policy ${DEFAULT_STORE_POLICY}
}
}

