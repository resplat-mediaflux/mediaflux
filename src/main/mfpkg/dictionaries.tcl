#
# author Neil Killeen
#
# Copyright (c) 2016, The University of Melbourne, Australia
#
# All rights reserved.
#
proc create_dictionaries { DICT_NS \
                           PRIMARY_STORE_POLICIES_DICT \
                           PROJECT_NAMESPACES_ROOT_DICT \
                           PROJECT_COLLECTIONS_ROOT_DICT \
                           PROJECT_NAMESPACES_MAP_DICT \
                           DEFAULT_STORE_POLICY} {
                           
# Dictionary: $NS:research-organisation

if { [xvalue exists [dictionary.exists :name "${DICT_NS}:research-organisation"]] == "false" } {
  dictionary.create :name "${DICT_NS}:research-organisation" :case-sensitive true :description "Lists known research organisations"
}

dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "AAD"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Adelaide University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "ANSTO"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Arcitecta"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Austin Health"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Australian Synchrotron"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Baker IDI Heart and Diabetes Institute"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "BOM"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Bureau of Meteorology"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Bureau of Meterology"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Burnet Institute"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "cesar australia"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Centre for Cancer Biology"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Commonwealth Scientific Industrial Research Organisation"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "CSL"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Curtin University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Department of Health"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Department for Environment and Water, Government of South Australia"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "DELWP" :definition "Department of Environment, Land, Water and Planning"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "DST"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Elettra Sincrotrone Trieste"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "eRSA"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Federal University of Ceara"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Flinders University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Flinders Medical Centre"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Florey Neuroscience Institutes"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Hasanuddin University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Ifremer"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Harvard University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Johns Hopkins University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "La Trobe University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "James Cook University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Ludwig Institute for Cancer Research"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Macquarie University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Massachusetts Institute of Technology"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Melbourne Health"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Mental Health Research Institute"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Monash University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Murdoch Childrens Research Institute"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Museums Victoria"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Naval Research Laboratory"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "NSW Department of Education"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Ocean University of China"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Other"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Oxford University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Pawsey"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Peter MacCallum Cancer Centre"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "QIMR Berghofer Medical Research Institute"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "QUT"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Queensland University of Technology"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Royal Adelaide Hospital"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Royal Melbourne Hospital"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "South Australian Health and Medical Research Institute (SAHMRI)"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Sunshine Hospital"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Swinburne University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Telethon Kids Institute"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "The Australian National University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "The Bionics Institute"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "The Queen Elizabeth Hospital"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "The University of Melbourne"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "The University of New South Wales"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "The University of Sydney"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "The University of Queensland"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Thomson Reuters"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "TQ Media"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "University of Lisbon"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "University of Massachusetts"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "University of South Australia"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "University of Tasmania"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "University of Southern California"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "University of Wollongong"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "UTAS"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "University Medical Centre of Goettingen"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Vanderbilt University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Universidad de Valparaíso"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Victoria University"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Victorian Government"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Walter and Eliza Hall Institute of Medical Research"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation :term "Women's and Children's Hospital"



# Dictionary: $NS:research-organisation-faculty
# This is UoM at the moment. This is bad.  Until MF provide content dependent meta-data selection 
# we can't do a lot better

if { [xvalue exists [dictionary.exists :name "${DICT_NS}:research-organisation-faculty"]] == "false" } {
  dictionary.create :name "${DICT_NS}:research-organisation-faculty" :case-sensitive true :description "Lists known research organisation faculties."
}

dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Bio21"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Faculty of Arts"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Faculty of Business and Economics"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Melbourne Graduate School of Education"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Melbourne School of Design" 
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Melbourne School of Engineering"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Melbourne School of Land and Environment"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Melbourne Law School"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Faculty of Architecture, Building and Planning"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Faculty of Fine Arts and Music"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Faculty of Medicine, Dentistry and Health Sciences"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Faculty of Science"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Faculty of Veterinary and Agricultural Sciences"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "University Services"
dictionary.entry.add :dictionary ${DICT_NS}:research-organisation-faculty :term "Other"


# Dictionaries list the store policy names used for managing projects on the primary and DR servers. These are
# matched lists. A project held by the second store on the primary will be replicated to the second DR store

# Primary store policies
# Add your store policies with dictionary.entry.add 
# The installer makes you a store ($DEFAULT_STORE) and a primary policy ($DEFAULT_STORE_POLICY)
# That gets added to the dictionary here
if { [xvalue exists [dictionary.exists :name $PRIMARY_STORE_POLICIES_DICT]] == "false" } {
  dictionary.create :name $PRIMARY_STORE_POLICIES_DICT :case-sensitive true \
       :description "Lists the primary (the 'term') and (optional) DR (the 'definition') host store policies used for provisioned projects."
  dictionary.entry.add :dictionary $PRIMARY_STORE_POLICIES_DICT :term $DEFAULT_STORE_POLICY
}


# Available project Namespace roots. Include the leading "/"
# Child root namespaces should have the 'definition' of each entry set to the role
# which is set on the child root namespace (you must hold the role to be able to see
# that child root namespace.  E.g. VicNode-Admin:cryo-em-visible)
if { [xvalue exists [dictionary.exists :name $PROJECT_NAMESPACES_ROOT_DICT]] == "false" } {
  dictionary.create :name $PROJECT_NAMESPACES_ROOT_DICT :case-sensitive true \
       :description "Lists the namespaces that are potential roots for projects to be created under."
  set PR  [xvalue property [server.property.get :name projects.namespace]]  
  dictionary.entry.add :dictionary $PROJECT_NAMESPACES_ROOT_DICT :term $PR
}


# The map between project ID and asset namespace. Get current by the provisioning system
if { [xvalue exists [dictionary.exists :name $PROJECT_NAMESPACES_MAP_DICT]] == "false" } {
  dictionary.create :name $PROJECT_NAMESPACES_MAP_DICT :case-sensitive true \
       :description "Maps project ID to the asset namespace used by that project."
}


# Available project collection roots. This is only here in this package
# because the onboarding doc type is shared with ds-mediaflux
# and it has a new element :path supported by this dictionary.
# so we don't want to lose that. We don't need to add any
# entries here. 
if { [xvalue exists [dictionary.exists :name $PROJECT_COLLECTIONS_ROOT_DICT]] == "false" } {
  dictionary.create :name $PROJECT_COLLECTIONS_ROOT_DICT :case-sensitive true \
       :description "Lists the collections that are potential roots for projects to be created under."
}


}
