#
# author Neil Killeen
#
# Copyright (c) 2020, The University of Melbourne, Australia
#
# All rights reserved.
#
proc create_accounts { LOCAL_ADMIN_AUTH PROVISIONING_ROLE } {

# Create provisioning user

 if { [xvalue exists [user.exists :domain $LOCAL_ADMIN_AUTH :user provision]] == "false" } {
 
 # place holder account. system:manager must set the password later (user.password.set) to what they want
     user.create :domain $LOCAL_ADMIN_AUTH :user provision  \
         :add-role $PROVISIONING_ROLE :password provision_123$
 }


}
