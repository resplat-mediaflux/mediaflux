
#
# doc namespace
# 
if { [xvalue exists [asset.doc.namespace.exists :namespace unimelb]] == "false" } {
    asset.doc.namespace.create :namespace unimelb
}

#
# doc type: unimelb:shareable-upload-manifest
# 
asset.doc.type.update :create true :index all \
    :type unimelb:shareable-upload-manifest \
    :definition < \
        :element -name shareable -type document -min-occurs 1 -max-occurs 1 < \
            :attribute -name id -type long -min-occurs 1 \
            :element -name name -type string -min-occurs 0 -max-occurs 1 \
            :element -name namespace -type string -min-occurs 0 -max-occurs 1 \
            :element -name collection -type string -min-occurs 0 -max-occurs 1 \
            :element -name valid-from -type date -min-occurs 0 -max-occurs 1 \
            :element -name valid-to -type date -min-occurs 0 -max-occurs 1 \
            :element -name property -type string -min-occurs 0 -max-occurs infinity < \
                :attribute -name name -type string -min-occurs 1 \
            > \
    	> \
        :element -name upload -type document -min-occurs 1 -max-occurs 1 < \
            :attribute -name shareable-id -type long -min-occurs 1 \
            :attribute -name id -type long -min-occurs 1 \
            :element -name arg -type string -min-occurs 0 -max-occurs infinity < \
                :attribute -name name -type string -min-occurs 1 \
            > \
            :element -name namespace -type string -min-occurs 0 -max-occurs 1 \
            :element -name collection -type string -min-occurs 0 -max-occurs 1 \
            :element -name completion-time -type date -min-occurs 0 -max-occurs 1 \
            :element -name keyword -type string -min-occurs 0 -max-occurs infinity < \
                :restriction -base string < :pattern "\[\\\w .-\]+" > > \
            :element -name share -type document -min-occurs 0 -max-occurs 1 < \
                :description "Options to share the uploaded data" \
                :element -name duration -type integer -min-occurs 0 -max-occurs 1 < \
                    :description "Number of days should the download shareable, download link and user interaction last." \
                    :restriction -base integer < :minimum 1 > \
                    :attribute -name expiry -type date -min-occurs 0 \
                > \
            > \
        > \
        :element -name download-shareable -type document -min-occurs 0 -max-occurs 1 < \
            :element -name id -type long -min-occurs 1 -max-occurs 1 \
        > \
        :element -name user-interaction -type document -min-occurs 0 -max-occurs 1 < \
            :element -name id -type long -min-occurs 1 -max-occurs 1 \
        > \
        :element -name direct-download -type document -min-occurs 0 -max-occurs 1 < \
            :attribute -name has-password -type boolean -min-occurs 0 \
            :element -name token-id -type long -min-occurs 1 -max-occurs 1 \
        > \
        :element -name total-file-count -type long -min-occurs 1 -max-occurs 1 \
        :element -name total-file-size -type long -min-occurs 1 -max-occurs 1 < :attribute -name h -type string -min-occurs 0 > \
        :element -name min-file-size -type long -min-occurs 1 -max-occurs 1 < :attribute -name h -type string -min-occurs 0 > \
        :element -name max-file-size -type long -min-occurs 1 -max-occurs 1 < :attribute -name h -type string -min-occurs 0 > \
        :element -name avg-file-size -type long -min-occurs 1 -max-occurs 1 < :attribute -name h -type string -min-occurs 0 > \
        :element -name file-size-statistics -type document -min-occurs 0 -max-occurs 1 < \
            :element -name group -type document -min-occurs 0 -max-occurs infinity < \
                :attribute -name range -type string -min-occurs 1 \
                :element -name file-count -type long -min-occurs 1 -max-occurs 1 \
                :element -name file-size -type long -min-occurs 1 -max-occurs 1 < \
                    :attribute -name h -type string -min-occurs 0 \
                > \
            > \
        > \
        :element -name file-type-statistics -type document -min-occurs 0 -max-occurs 1 < \
            :element -name group -type document -min-occurs 0 -max-occurs infinity < \
                :attribute -name type -type string -min-occurs 1 \
                :element -name file-count -type long -min-occurs 1 -max-occurs 1 \
                :element -name file-size -type long -min-occurs 1 -max-occurs 1 < \
                    :attribute -name h -type string -min-occurs 0 \
                > \
            > \
        > \
        :element -name property -type string -min-occurs 0 -max-occurs infinity < \
            :attribute -name name -type string -min-occurs 1 \
        > \
        :element -name date-moved-on-DR -type date -min-occurs 0 -max-occurs 1 < \
           :description "The date that the enclosing transactional namespace was moved (after expiry and after the data was destroyed on the primary) on the Disaster Recovery server to the pre-destruction namespace." \
        > \
    >
