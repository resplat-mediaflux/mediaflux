actor.grant :type plugin:service :name vicnode.facility.shareable.upload.mapping.set :perm < :resource -type service asset.namespace.visibility.set :access ADMINISTER >

proc set_instrument_upload_complete_service_permissions { instrument_upload_admin_role } {
    actor.grant :type plugin:service :name vicnode.facility.shareable.upload.complete :role -type role $instrument_upload_admin_role
}

actor.grant :type plugin:service :name  vicnode.rcpadmin.project.user.add :perm < :access administer :resource -type role:namespace ds >
actor.grant :type plugin:service :name  vicnode.rcpadmin.project.describe :perm < :access access :resource -type dictionary:namespace ds-admin >
actor.grant :type plugin:service :name  vicnode.rcpadmin.project.user.add :perm < :access access :resource -type dictionary:namespace ds-admin >
actor.grant :type plugin:service :name  vicnode.rcpadmin.project.user.remove :perm < :access access :resource -type dictionary:namespace ds-admin >