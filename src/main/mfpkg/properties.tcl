#
# author Neil Killeen
#
# Copyright (c) 2016, The University of Melbourne, Australia
#
# All rights reserved.
#
#
proc create_application_property { APP_NAME PROPERTY_NAME PROPERTY_VALUE OVERWRITE } {
    # The property name is matched in the Java class Properties.java
    # This is a way to pass in dynamically configured variables into the run-time environment.
    if { [ xvalue exists [application.property.exists :property -app $APP_NAME $PROPERTY_NAME]] == "false" }  {
       application.property.create :property -app $APP_NAME -name $PROPERTY_NAME 
       application.property.set :property -app $APP_NAME -name $PROPERTY_NAME $PROPERTY_VALUE  
    }
    if { $OVERWRITE == "true" } {
        application.property.set :property -app $APP_NAME -name $PROPERTY_NAME $PROPERTY_VALUE  
    }
}