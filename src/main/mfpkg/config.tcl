####
# Gets the set of configurable variables.  These variables are basically easily
# changeable run-time settings that assist in operations of the system
# (rather than defining the system itself - see config_vars.tcl) 
#
# The intent is that this cfg array will eventually be populated by reading in from an external configuration file.
proc get_config { doAttica } {
    if { $doAttica == 1 } {
       # Disaster Recovery Server.
       # SPecial values looked for in function ProjectReplicate.isDRConfigured
       set cfg(DR_PEER) "none"
       set cfg(DR_UUID) "none"
 
       # Host name and port for services that need to notify users about it.
       set cfg(HOST_AND_PORT) https://n0-mfattica-qh2.storage.unimelb.edu.au:443

       # Support email address used for automated error notifications that require a ticket to be submitted.
       # This is the service now email box
       set cfg(SUPPORT_EMAIL) "rcs-attica-ops@lists.unimelb.edu.au"

       # Operations email for notifications.
       set cfg(OPERATIONS_EMAIL) "rcs-attica-ops@lists.unimelb.edu.au"
    } else {
       # Disaster Recovery Server.
       set cfg(DR_PEER) UoM-DR-Server2
       set cfg(DR_UUID) 1129
  
       # Host name and port for services that need to notify users about it.
       set cfg(HOST_AND_PORT) https://mediaflux.researchsoftware.unimelb.edu.au:443

       # Support email address used for automated error notifications that require a ticket to be submitted.
       # This is the service now email box
       set cfg(SUPPORT_EMAIL) "rd-support@unimelb.edu.au"

       # Operations email for notifications.
       set cfg(OPERATIONS_EMAIL) "resplat-mediaflux-ops@lists.unimelb.edu.au"
    }

    # Root for end-user documentation.
    set cfg(USER_DOC_ROOT) "https://rcs-knowledge-hub.atlassian.net/wiki/spaces/KB/pages/5472333/Mediaflux"
    
    # Mediaflux Explorer end-user documentation
    set cfg(USER_DOC_EXPLORER) "https://rcs-knowledge-hub.atlassian.net/wiki/spaces/KB/pages/5472409/Mediaflux+Explorer+HTTPS+Protocol"
   
    # Mediaflux DataMover end-user documentation
    set cfg(USER_DOC_DATAMOVER) "https://rcs-knowledge-hub.atlassian.net/wiki/spaces/KB/pages/5473227/Data+Mover"

    # Mediaflux DataMover end-user getting started documentation CLI section
    set cfg(USER_DOC_DATAMOVER_CLI) "https://rcs-knowledge-hub.atlassian.net/wiki/spaces/KB/pages/5473964/Data+Mover+Command+Line+Interface"

    # Mediaflux end-user documentation explaining user roles
    set cfg(USER_DOC_STANDARD_PROJECT_ROLES) "https://rcs-knowledge-hub.atlassian.net/wiki/spaces/KB/pages/5474734/Standard+Project+Roles"
  
    # Mediaflux end-user documentation explaining valid file names
    set cfg(USER_DOC_VALID_FILENAMES) "https://rcs-knowledge-hub.atlassian.net/wiki/spaces/KB/pages/5474752/Valid+file+names+in+Mediaflux"

    # Research Computing Services (RCS) – TERMS OF SERVICE
    set cfg(USER_DOC_RCS_TOS) "https://go.unimelb.edu.au/zce8"

    # Web site contact for support
    set cfg(CONTACT_US) "https://gateway.research.unimelb.edu.au/platforms-data-and-reporting/data-and-computation/research-computing-services-rcs?tab=Contact_us"
    
    # Data Registry Host
    set cfg(DATA_REG_HOST) "https://dashboard.storage.unimelb.edu.au"
    
    # Onboarding Asset Namespace
    set cfg(ONBOARDING_ASSET_NAMESPACE) "/local-admin/Onboarding"
      
    array get cfg
}
