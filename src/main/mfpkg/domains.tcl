#
# author Neil Killeen
#
# Copyright (c) 2020, The University of Melbourne, Australia
#
# All rights reserved.
#
proc create_domains { LOCAL_AUTH LOCAL_ADMIN_AUTH } {

# Authentication domains
if { [xvalue exists [authentication.domain.exists :domain $LOCAL_AUTH]] == "false" } {
  authentication.domain.create :domain $LOCAL_AUTH :description "Local authentication domain for standard users."
}
if { [xvalue exists [authentication.domain.exists :domain $LOCAL_ADMIN_AUTH]] == "false" } {
  authentication.domain.create :domain $LOCAL_ADMIN_AUTH :description "Local authentication domain for project provisioning administrators."
}

# AUthentication domain aliases (SMB requires all upper case)
set NSU [string toupper $LOCAL_AUTH]
if { [xvalue alias/@exists [authentication.domain.alias.exists :alias $NSU]] == "false" } {
   authentication.domain.alias.add :alias ${NSU} :domain ${LOCAL_AUTH}
}
set NSL [string tolower $LOCAL_AUTH]
if { [xvalue alias/@exists [authentication.domain.alias.exists :alias $NSL]] == "false" } {
   authentication.domain.alias.add :alias $NSL :domain $LOCAL_AUTH
}
}
