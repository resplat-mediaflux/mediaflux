####
# Gets the set of configurable variables. These variables define much of
# provisioning system.  It's not really intended that they are very changeable
# but they could be.
proc get_config_vars { } {
         
	# Set  namespaces 
	#
	# Asset namespace
	set cfg_vars(ASSET_NS) local
	set cfg_vars(ASSET_NS_ADMIN) local-admin
    set cfg_vars(ASSET_NS_INSTRUMENT_UPLOAD_MANIFESTS) $cfg_vars(ASSET_NS_ADMIN)/instrument-upload-manifests
	
	# Role namespace
	set ROLE_NS ds
	set cfg_vars(ROLE_NS) ${ROLE_NS}
	#
	set ROLE_NS_ADMIN ds-admin
	set cfg_vars(ROLE_NS_ADMIN) ${ROLE_NS_ADMIN}
	
	# Document namespace
	set DOC_NS ds
	set cfg_vars(DOC_NS) ${DOC_NS}
	#
	set DOC_NS_ADMIN ds-admin
	set cfg_vars(DOC_NS_ADMIN) ${DOC_NS_ADMIN}
	
	# Dictionary namespace
	set cfg_vars(DICT_NS) ds
	#
	set DICT_NS_ADMIN ds-admin
	set cfg_vars(DICT_NS_ADMIN) ${DICT_NS_ADMIN}
    ####
    
    
	# Set local authentication domain for end users 
	set cfg_vars(LOCAL_AUTH) local
	
	# Set local authentication domain for provisioning administrator
	set cfg_vars(LOCAL_ADMIN_AUTH) local-admin
	
	# Set Active Directory domains
	set cfg_vars(AD_STAFF) unimelb
	set cfg_vars(AD_STUDENT) unimelb-student
	
	# Set default store and store policy names
	set cfg_vars(DEFAULT_STORE) unimelb-1
	set cfg_vars(DEFAULT_STORE_POLICY) unimelb-balanced
	
	# Service root name. Currently also set in compiled service code - I don't know
	# how to pass this through to compilation.  Maybe set in pom file ?
	set cfg_vars(SERVICE_ROOT) vicnode
	
	# Store policy dictionary name. The dictionary holds the primary (term) and optional
	# matching DR policy (definition)
	set cfg_vars(PRIMARY_STORE_POLICIES_DICT) ${DICT_NS_ADMIN}:primary-store-policies
	
	# Dictionary that holds the list of possible project root namespaces. This must be populated
	# manually by the administrator and it is initialised to hold one value  which is the value 
	# of server property project.namespace  (usually /projects by default). The definition of
	# each root is an optional role controlling the visibility of the namespace
	set cfg_vars(PROJECT_NAMESPACES_ROOT_DICT) ${DICT_NS_ADMIN}:project-namespace-roots
	
	# Dictionary that holds the list of possible project root collections. This is
	# only here as it is required by ds-mediaflux (collections-based
	# package and they share this doc type)
	set cfg_vars(PROJECT_COLLECTIONS_ROOT_DICT) ${DICT_NS_ADMIN}:project-collection-roots

	# Dictionary that holds the mapping of project ID to project namespace
	# This is managed by the service vicnode.project.create
	set cfg_vars(PROJECT_NAMESPACES_MAP_DICT) ${DICT_NS_ADMIN}:project-namespace-map
	
	# Specialised administrator role granted to admins
	set cfg_vars(ADMINISTRATOR_ROLE) ${ROLE_NS_ADMIN}:administrator
	
	# Specialised provisioning role granted to admins
	set cfg_vars(PROVISIONING_ROLE) ${ROLE_NS_ADMIN}:provisioning
	
	# This role is granted ADMINISTER for provisioning services that require ADMINISTER
	# It is held by the ADMINISTRATOR_ROLE and layers out the service permissions
	set cfg_vars(ADMINISTRATOR_SERVICES_ROLE) ${ROLE_NS_ADMIN}:service-administrator
    
    # This role is for vicnode.facility.upload.complete service to create global 
    # instrument-upload manifest assets in $ASSET_NS_INSTRUMENT_UPLOAD_MANIFESTS, 
    # e.g. local-admin/instrument-upload-manifests/.
    set cfg_vars(INSTRUMENT_UPLOAD_ADMIN_ROLE) ${ROLE_NS_ADMIN}:instrument-upload-admin
	
	# STandard end user role
	set cfg_vars(STANDARD_USER_ROLE) ${ROLE_NS}:standard-user
	
	# Read-only access to all Projects - intended to provide
	# administrators access to projects so they can assist users
	set cfg_vars(PROJECT_ACCESS_ONLY_ROLE) ${ROLE_NS_ADMIN}:project-access
	
	# Administer permission to all Project asset namespaces - intended to provide
	# administrative access to some limited services that need additional
	# permission. 
	set cfg_vars(PROJECT_ASSET_NAMESPACE_ADMIN_ROLE) ${ROLE_NS_ADMIN}:project-asset-namespace-administrator

	# Administer access to  all Project role namespaces - intended
	# to provide administrators access to project role namespaces
	# so they can help users by granting roles on behalf of them
	set cfg_vars(PROJECT_ROLE_NAMESPACE_ADMIN_ROLE) ${ROLE_NS_ADMIN}:project-role-namespace-administrator
	
	# Posix mount point document type  
	set cfg_vars(POSIX_MOUNT_DOCTYPE) ${DOC_NS_ADMIN}:Project-Mount
	
	# Project doc type
	set cfg_vars(PROJECT_DOCTYPE) ${DOC_NS_ADMIN}:Project
	
	# Onboarding asset doc type
	set cfg_vars(ONBOARDING_ASSET_DOCTYPE) ${DOC_NS_ADMIN}:User-Onboarding
	
	# Collection Doc Type
	set cfg_vars(COLLECTION_DOCTYPE) ${DOC_NS}:Collection
	
	# User accounts doc type
	set cfg_vars(USER_ACCOUNTS_DOCTYPE) ${DOC_NS}:User-Account-Details
	
	# Project parent namespace doc type for admin tasks like monitoring and notifications
	# when those tasks are managed by a single individual on behalf of the child projects.
	# Generally only set on child project roots. e.g. /projects/my_instrument
	set cfg_vars(PROJECT_PARENT_ADMIN_DOCTYPE) ${DOC_NS_ADMIN}:Project-Parent-Admin
	        
    # Declared service collection made available to project namespaces from Desktop menu
    set cfg_vars(NAMESPACE_SERVICE_COLLECTION) unimelb-user
	    
    # Number of processors for project asset replication processing queue 
    set cfg_vars(REPLICATION_QUEUE_NB_PROCESSORS) 50
    
    # Asset processing queue to make content copies.
    # The number of processors>0 and the store set means the
    # system is configured and so when a project is created, the
    # content copy asset  processing queue is set on it.
    # Once these two properties have been created,
    # their values will not be overwritten by the installer. To disable,
    # set to -1 and remove the asset processing queue from affected
    # project namespaces (likely all)
    # Number of processors for project asset content copy processing queue
    set cfg_vars(CONTENT_COPY_QUEUE_NB_PROCESSORS) -1

    # Destination Store name for project asset content copy processing queue
    set cfg_vars(CONTENT_COPY_QUEUE_STORE_NAME) none
  
    array get cfg_vars
}
