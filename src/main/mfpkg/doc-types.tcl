#
# author Neil Killeen
#
# Copyright (c) 2016, The University of Melbourne, Australia
#
# All rights reserved.
#
proc create_doc_types { DICT_NS \
                        PROJECT_PARENT_ADMIN_DOCTYPE \
                        POSIX_MOUNT_DOCTYPE \
                        PROJECT_DOCTYPE \
                        COLLECTION_DOCTYPE \
                        ONBOARDING_ASSET_DOCTYPE \
                        USER_ACCOUNTS_DOCTYPE \
                        PRIMARY_STORE_POLICIES_DICT \
                        PROJECT_NAMESPACES_ROOT_DICT \
                        PROJECT_COLLECTIONS_ROOT_DICT} {

         
asset.doc.type.update :create yes :type ${PROJECT_PARENT_ADMIN_DOCTYPE}  \
  :source-of-truth "true" \
  :label $PROJECT_PARENT_ADMIN_DOCTYPE  \
  :description "Describes administration meta-data for parent project namespaces.  These namespaces may have several child projects, but the quota on the parent project namespace applies to them all and needed to be administered centrally.\
\
Notifications are only generated by scheduled jobs that are executed to match the frequency of desired notifications." \
  :instructions "Should not be modified manually as this may break various things.\
\
Should not be set on individual Project namespaces." \
  :definition < \
    :element -name "notification" -type "document" -min-occurs "0" -max-occurs "1" \
    < \
      :description "Controls notifications that may be received regarding the administration of the project parent namespace." \
      :element -name "freespace" -type "document" -min-occurs 0 -max-occurs "1" \
      < \
        :description "This element is used to manage notifications when free space under the parent project namespace reaches a critical thresh-hold. 

Quotas are set on the parent project namespace, not the child project namespaces in this scenario." \
        :element -name "email" -type "email-address" -min-occurs 1 -max-occurs infinity \
        < \
          :description "The email address to send the notification to when the threshold is reached." \
        > \
        :element -name "threshold" -type "string" -min-occurs 1 -max-occurs "1" \
        < \
          :description "Threshold e.g. 10TB" \
        > \
        :element -name "frequency" -type "enumeration" -min-occurs 1 -max-occurs "1" \
        < \
          :description "Select the frequency of which you receive notifications.  Notifications are only generated if the thresh-hold is reached." \
          :restriction -base "enumeration" \
          < \
            :value "daily" \
            :value "weekly" \
          > \
        > \
      > \
      :element -name "summary" -type "document" -min-occurs "0" -max-occurs "1" \
      < \
        :description "This element is used to manage regular  notifications about all of the projects under the parent project namespace." \
        :element -name "email" -type "email-address" \
        < \
          :description "The email address to send the notification to." \
        > \
        :element -name "frequency" -type "enumeration" -max-occurs "1" \
        < \
          :description "Select the frequency of which you receive notifications." \
          :restriction -base "enumeration" \
          < \
            :value "weekly" \
            :value "monthly" \
          > \
        > \
        :element -name "depth" -type "integer" -index "true" -min-occurs 1 -max-occurs "1" \
        < \
          :description "Indicates the namespace depth below the project level that is presented. Can be 0 (show immediate child namespaces of project) or 1 (show one level below immediate child namespaces). " \
          :restriction -base "integer" \
          < \
            :minimum "0" \
            :maximum "1" \
          > \
        > \
        :element -name "age" -type "integer" -min-occurs 1 -max-occurs "1" \
        < \
          :description "Only namespaces with data older (mtime or ctime) than this (days) will be considered (by the service vicnode.parent.project.namespace.notifications.for.schedule).
" \
          :restriction -base "integer" \
          < \
            :minimum "0" \
          > \
        > \
      > \
    > \
   >      
                       
asset.doc.type.update :create yes :type $POSIX_MOUNT_DOCTYPE \
  :source-of-truth "true" \
  :label $POSIX_MOUNT_DOCTYPE \
  :description "Describes administration meta-data for creating Posix mounts for a project. Is set on the asset namespace. " \
  :instructions "Should not be modified manually as this may break various things." \
  :definition < \
    :element -name "posix" -type "document" -max-occurs "1" \
    < \
      :description "Specifies the posix mount point state. Mount point name is the project ID." \
       :element -name "should-be-mounted" -type "boolean" -min-occurs 0 -max-occurs 1 \
      < \
        :description "Indicates that the project should be mounted if possible, for example after a server restart (but the service vicnode.project.posix.remount).  Does not indicate whether it actually is or is not mounted (only the posix mount points themselves tell us that)." \
        :instructions "If not present (as this was not originally present in the meta-data), defaults to true). " \
      > \
      :element -name "read-only" -type "boolean" \
      < \
        :description "When last mounted, was the mount point read-only or read/write" \
      > \
      :element -name "allowed-protocol" -type "enumeration" -min-occurs 0 -max-occurs "1" \
      < \
         :description "Specifies a network protocol that will allow users access to the share through POSIX interface. For example, if type is set to 'smb' only connections that use the SMB network protocol will be allowed to access the share. Setting it to 'all' will clear the setting. By default it is not set which will allow any connection to access the share." \
         :instructions "Optional as was added later. Defaults to all if not supplied." \
         :restriction -base "enumeration" \
         < \
           :value "smb" \
           :value "nfs" \
           :value "ssh" \
           :value "all" \
         > \
       > \
      :element -name "apply-mode-bits" -type "boolean" -min-occurs 0 -max-occurs 1 \
      < \
        :description "When last mounted, was the apply-mode-bits argument true or false (see posix.fs.asset.network.mount). If not present (this was added later) it means true (the underlying MF default)." \
      > \
      :element -name metadata-export -type document -min-occurs 0 -max-occurs 1 \
      < \
         :description "Define asset metadata export" \
         :element -name export -type boolean -min-occurs 0 -max-occurs 1 \
          < \
              :description "Export asset metadata in XML sidecar files (defaults to false)" \
          > \
          :element -name extended -type boolean -min-occurs 0 -max-occurs 1 \
          < \
             :description "Export the full metadata definition rather than just the active fields. Defaults to false." \
          > \
	      :element -name extension -type string -min-occurs 0 -max-occurs 1 \
	      < \
	         :description "The sidecar file extension. Defaults to .xml" \
	      > \
	      :element -name prettyprint -type boolean -min-occurs 0 -max-occurs 1 \
	      < \
	        :description "Pretty print the XML metadata so it's human readable. Defaults to true." \
	      > \
	      :element -name replace-dots -type boolean -min-occurs 0 -max-occurs 1 \
	      < \
	         :description  "Replace the '.' before the file extension in XML sidecar filenames with '_'. Defaults to false." \
	      > \
	      :element -name transform -type string -min-occurs 0 -max-occurs 1 \
	      < \
	         :description "Specifies an XSLT transform for metadata presentation in the sidecar file. 'none' (the default) means raw XML format. Use the service 'asset.meta.transform.provider.describe' to view available transforms." \
	      > \
 	  > \
      :element -name "restrict" -type "document" -min-occurs "0" \
      < \
        :description "Access restrictions, if any." \
        :attribute -name "protocol" -type "string" -min-occurs "0" \
        < \
          :description "The name of a calling protocol name (e.g. network service) for which this restriction applies. If not specified, then applies to all protocols." \
         > \
        :attribute -name "description" -type "string" -min-occurs "0" \
        < \
          :description "Arbitrary description for the purpose of the restriction." \
        > \
        :element -name "network" -type "document" -min-occurs "0" -max-occurs "1" \
        < \
          :description "Network access restrictions, if any." \
          :element -name "address" -type "string" -min-occurs "0" \
          < \
            :description " IP network address (IPv4 or IPv6) to restrict user access." \
            :attribute -name "mask" -type "string" -min-occurs "0" \
            < \
              :description "The netmask for the corresponding network address. Defaults to 255.255.255.255" \
            > \
            :attribute -name "description" -type "string" -min-occurs "0" \
            < \
              :description "Arbitrary description for this address." \
            > \
          > \
          :element -name "encrypted" -type "boolean" -min-occurs "0" -max-occurs "1" \
          < \
            :description "Indicates whether encrypted only network transports will be accepted. Defaults to false." \
          > \
        > \
      > \
      :element -name "root" -type document -min-occurs 0 -max-occurs 1 < :description "Root access, if any." > \
      < \
         :element -name "restrict" -type "document" -min-occurs "0" \
         < \
           :description "Access restrictions, if any." \
           :attribute -name "protocol" -type "string" -min-occurs "0" \
           < \
             :description "The name of a calling protocol name (e.g. network service) for which this restriction applies. If not specified, then applies to all protocols." \
            > \
           :attribute -name "description" -type "string" -min-occurs "0" \
           < \
             :description "Arbitrary description for the purpose of the restriction." \
           > \
           :element -name "network" -type "document" -min-occurs "0" -max-occurs "1" \
           < \
             :description "Network access restrictions, if any." \
             :element -name "address" -type "string" -min-occurs "0" \
             < \
               :description " IP network address (IPv4 or IPv6) to restrict user access." \
               :attribute -name "mask" -type "string" -min-occurs "0" \
               < \
                 :description "The netmask for the corresponding network address. Defaults to 255.255.255.255" \
               > \
               :attribute -name "description" -type "string" -min-occurs "0" \
               < \
                 :description "Arbitrary description for this address." \
               > \
             > \
             :element -name "encrypted" -type "boolean" -min-occurs "0" -max-occurs "1" \
             < \
               :description "Indicates whether encrypted only network transports will be accepted. Defaults to false." \
             > \
           > \
         > \
         :element -name "uid" -type "long" -min-occurs 0 -max-occurs 1 \
         < \
            :description "If set, then the UID that will be given 'root' access for mounting (can be specified as 0). If not specified, then no root access for mount, unless the UID is in the identity map." \
             :restriction -base "long"  < :minimum 0 > \
         > \
      > \
    > \
   >
   
   

# This is separated from $DOC_NS_ADMIN:Project-Mount because the manipulation service asset.namespace.asset.meta.* only allow
# whole of document type removal, not children. So it's easier to separate.
asset.doc.type.update :create yes :type $PROJECT_DOCTYPE \
  :source-of-truth "true" \
  :label $PROJECT_DOCTYPE \
  :description "Describes administration meta-data for a Project and is set on the asset namespace. " \
  :instructions "Should not be modified manually as this may break various things." \
  :definition < \
   :element -name "note" -type string -min-occurs 0 -max-occurs infinity \
    < \
       :description "One or more arbitrary notes" \
    > \
   :element -name "provisioning-id" -type asset -min-occurs 0 -max-occurs 1 \
    < \
       :description "The id of the provisioning asset that was used to create the project." \
    > \
   :element -name "type" -type "enumeration" -min-occurs 0 -max-occurs "1" \
    < \
      :description "Is this project for production, test or a stub?  Test and stub projects are not replicated. 'closed-stub' means that the project may have been mistakenly created, or perhaps the content has been removed (by the user)  stub project can should still be present in the namespace dictionary map, but can be filtered out." \
      :instructions "Test projects are not replicated." \
      :restriction -base "enumeration" \
      < \
        :value "production" \
        :value "test" \
        :value "closed-stub" \
      > \
    > \
    :element -name "notification" -type "document" -min-occurs "0" -max-occurs "1" \
    < \
      :description "Controls notifications that project members may receive" \
      :element -name "summary" -type "enumeration" -min-occurs "0" -max-occurs "infinity" \
      < \
        :description "Controls who receives a regular summary report of the project." \
        :instructions "If not supplied, no notifications are sent. Use 'none' for no project role users to receive a notification (so you should set attribute 'email if you set 'none'). 'user' means all project members, 'all' means all project users plus the owner." \
        :restriction -base "enumeration" \
        < \
          :value "all" \
          :value "administrator" \
          :value "owner" \
          :value "user" \
          :value "none" \
        > \
        :attribute -name email -type string -min-occurs 0 < :description "An email address to send the notification to. Can also accept multiple comma-separated emails." > \
        :attribute -name "frequency" -type "enumeration" \
        < \
          :description "The frequency with which the notifications should be sent." \
          :restriction -base "enumeration" \
          < \
            :value "weekly" \
            :value "monthly" \
          > \
        > \
      > \
      :element -name "zero-size" -type "enumeration" -min-occurs "0" -max-occurs "infinity" \
      < \
        :description "Controls who receives a regular report of uploaded zero-sized files to the project." \
        :instructions "If not supplied, no notifications are sent. Use 'none' for no project role users to receive a notification (so you should set attribute 'email if you set 'none'). 'user' means all project members, 'all' means all project users plus the owner." \
        :restriction -base "enumeration" \
        < \
          :value "all" \
          :value "administrator" \
          :value "owner" \
          :value "user" \
          :value "none" \
        > \
        :attribute -name email -type string -min-occurs 0 < :description "An email address to send the notification to" > \
       > \
      :element -name "data-mover-expiry" -type "document" -min-occurs 0 -max-occurs "1" \
      < \
        :description "This element is used to manage notifications for expired data uploaded via the Data Mover." \
        :element -name "email" -type "email-address" -min-occurs 0 -max-occurs infinity \
        < \
          :description "The email address to send the specific type of notification to." \
          :attribute -name "type" -type "enumeration" \
          < \
            :description "There are two kinds of notifications. If type=destroy-expired then a notification is sent to that email which includes a URL to allow the destruction of expired data mover instrument uploads (this is done by service vicnode.project.DM.operator.expiry.notification). Should always be a responsible facility platform operator or manager. If type=end-user, this means send to this email address a copy of the notification that was sent to end users (consumed by service vicnode.project.DM.enduser.expiry.notification - see the arguments to that service) who have not yet downloaded their data." \
            :restriction -base "enumeration" \
            < \
              :value "destroy-expired" \
              :value "end-user" \
            > \
          > \
        > \
        :element -name "exclude-child-path" -type "string" -min-occurs 0 -max-occurs infinity \
        < \
          :description "By default, the notification services looks recursively everywhere in the project for manifest assets.  You can exclude project child namespaces from that search with this element.  Specify the child namespace only relative to the project namespace. For example, if the uploads you wish to exclude are in /projects/<project>/test-uploads, then exclude-child-path should be set to 'test-uploads' (do NOT include leading /). The special value of '.' will prevent it looking directly in the parent project namespace.  Multiple exclude child paths are allowed." \
        > \
       > \
    > \
 >

#


asset.doc.type.update :create yes :type $ONBOARDING_ASSET_DOCTYPE \
  :source-of-truth "true" \
  :label $ONBOARDING_ASSET_DOCTYPE \
  :description "This document describes the resources that a new user will need in Mediaflux. The resources will be provisioned by the asset provisioning system." \
  :definition < \
    :element -name "name" -type "document" -index "true" -max-occurs "1" \
    < \
      :element -name "first" -type "string" -index "true" -max-occurs "1" \
      < \
        :description "First name" \
      > \
      :element -name "middle" -type "string" -index "true" -min-occurs "0" -max-occurs "1" \
      < \
        :description "Middle name" \
      > \
      :element -name "last" -type "string" -index "true" -max-occurs "1" \
      < \
        :description "Last name" \
      > \
    > \
    :element -name "email" -type "email-address" -index "true" -max-occurs "1" \
    < \
      :description "email" \
    > \
    :element -name "phone" -type "string" -index "true" -min-occurs "0" -max-occurs "1" \
    < \
      :description "Phone number" \
    > \
    :element -name "organisation" -type "document" -max-occurs "1" \
    < \
      :description "The organisation that the user is a member of." \
      :element -name "name" -type "enumeration" -max-occurs "1" \
      < \
        :description "The name of the institution" \
        :restriction -base "enumeration" \
        < \
          :dictionary ${DICT_NS}:research-organisation \
        > \
      > \
     :element -name "faculty" -type "enumeration" -min-occurs "0" \
      < \
        :description "Track the faculty (if concept relevant) of the owner." \
        :instructions "There is currently no context-dependence in Mediaflux interfaces so that we could present different lists dependent on the owner's organisation.  For now this is a University of Melbourne list with value 'Other' and
an additional element to fill in." \
        :restriction -base "enumeration" \
         < \
          :dictionary ${DICT_NS}:research-organisation-faculty \
         > \
        :attribute -name "other" -type "string" -min-occurs "0" \
        < \
          :description "Enter this field for Faculties not in the enumerated list (UoM faculties)" \
        > \
      > \
      :element -name "department" -type "string" -min-occurs "0" -max-occurs "1" \
      < \
        :description "The user's department " \
      > \
    > \
     :element -name "account" -type "document" -index "true" -min-occurs "0" -max-occurs "1" \
    < \
      :description "Details about the user's account" \
      :instructions "Must be filled in before creating the project." \
      :element -name "username" -type "string" -index "true" -min-occurs "1" -max-occurs "1" \
      < \
        :description "The username of form domain:name. E.g a local account might be VicNode:nkilleen." \
      > \
    > \
    :element -name "description" -type "string" -min-occurs "1" -max-occurs "1" \
    < \
      :description "Meaningful description of the project" \
    > \
    :element -name "project-name" -type "string" -index "true" -min-occurs "1" -max-occurs "1" \
    < \
      :description "A short meaningful name for the project. It will be used to create the project ID in the form proj-<name>-<cid>" \
    > \
   :element -name "type" -type "enumeration" -max-occurs "1" -default production \
    < \
      :description "Is this project for production or test?" \
      :instructions "The citeable ID at the trailing end of project IDs will be different if production or test" \
      :restriction -base "enumeration" \
      < \
        :value "production" \
        :value "test" \
      > \
    > \
   :element -name "namespace" -type enumeration -min-occurs 0 -max-occurs 1 \
    < \
      :description "The parent namespace that this project's namespace will be a child of." \
      :instructions "Must supply either namespace or path element but not both." \
      :restriction -base "enumeration" \
          < \
            :dictionary $PROJECT_NAMESPACES_ROOT_DICT \
          > \
    > \
   :element -name "path" -type enumeration -min-occurs 0 -max-occurs 1 \
    < \
      :description "The parent collection asset that this project's collection asset will be a child of." \
      :instructions "Must supply either namespace or path element but not both." \
      :restriction -base "enumeration" \
          < \
            :dictionary $PROJECT_COLLECTIONS_ROOT_DICT \
          > \
    > \
   :element -name "storage" -type "document" -min-occurs "1" -max-occurs "1" \
    < \
      :description "Storage.  Must be an extant Mediaflux store on primary and DR." \
      :instructions "When provisioning a new project fill this in." \
      :element -name "primary" -type "document" -min-occurs 1 -max-occurs "1" \
      < \
        :description "Describes the store policy to be used on the primary server for this project." \
        :element -name "store-policy" -type "enumeration" -min-occurs 1 -max-occurs "1"  \
        < \
          :description "The name of the Mediaflux store policy that this project should use." \
          :instructions "Must be one of the known existing store policies. It's counterpart on the DR system will be automatically selected and used." \
          :restriction -base "enumeration" \
          < \
            :dictionary $PRIMARY_STORE_POLICIES_DICT \
          > \
        > \
      > \
      :element -name "disaster-recovery" -type "document" -min-occurs "0" -max-occurs "1" \
      < \
         :description "Specifies processes for Disaster Recovery

On the DR system, the asset namespace structure is <server uuid>/projects/<project ID>" \
        :element -name "replication-queue" -type "boolean" -min-occurs "0" -max-occurs "1" \
        < \
          :description "Indicates that replications should be active and managed by the asset processing queue associated with the project namespace." \
        > \
      > \
      :element -name "quota" -type "string" \
      < \
        :description "The quota to be associated with the project." \
        :instructions "Use units like 100GB, 100MB. If the quota increases over time, add an extra quota record to track it (not linked directly to the provisioned project)." \
        :attribute -name "comment" -type "string" -min-occurs 0 \
      > \
    > \
   :element -name "notification" -type "document" -min-occurs "0" -max-occurs "1" \
    < \
      :description "Controls notifications that project members may receive" \
      :element -name "summary" -type "enumeration" -min-occurs "0" -max-occurs "infinity" -default administrator \
      < \
        :description "Controls who receives a regular summary report of the project. " \
        :instructions "The enumerations are matched to scheduled jobs that run in the server. The absence of the element means no notification. Use 'none' for no project role users to receive a notification (so you should set attribute 'email if you set 'none'). 'user' means all project members, 'all' means all project users plus the owner." \
        :restriction -base "enumeration" \
        < \
          :value "all" \
          :value "administrator" \
          :value "owner" \
          :value "user" \
          :value "none" \
        > \
        :attribute -name email -type string -min-occurs 0 < :description "An email address to send the notification to" > \
        :attribute -name "frequency" -type "enumeration" -min-occurs 0 -default monthly \
        < \
          :description "The frequency with which the notifications should be sent." \
          :restriction -base "enumeration" \
          < \
            :value "weekly" \
            :value "monthly" \
          > \
        > \
      > \
      :element -name "zero-size" -type "enumeration" -min-occurs "0" -max-occurs "infinity" -default administrator \
      < \
        :description "Controls who receives a regular report of uploaded zero-sized files to the project." \
        :instructions "If not supplied, no notifications are sent. Use 'none' for no project role users to receive a notification (so you should set attribute 'email if you set 'none'). 'user' means all project members, 'all' means all project users plus the owner." \
        :restriction -base "enumeration" \
        < \
          :value "all" \
          :value "administrator" \
          :value "owner" \
          :value "user" \
          :value "none" \
        > \
        :attribute -name email -type string -min-occurs 0 < :description "An email address to send the notification to" > \
      > \
    > \
    :element -name "comment" -type "string" -index "true" -min-occurs "0" \
    < \
      :description "Any additional comment you wish to make" \
    > \
    :element -name "project-id" -type "string" -index "true" -min-occurs "0" -max-occurs "1" \
    < \
      :description "Identifier of the provisioned project (assigned for you)." \
      :instructions "When provisioning the project leave this empty. It will be filled in by the system after execution." \
    > \
   >

#
asset.doc.type.update :create yes :type $USER_ACCOUNTS_DOCTYPE \
  :source-of-truth "true" \
  :label $USER_ACCOUNTS_DOCTYPE \
  :description "Provides for supplementary details about the user." \
  :definition < \
    :element -name "organisation" -type "document" -max-occurs "1" \
    < \
      :description "The organisation that the user is a member of." \
      :element -name "name" -type "enumeration" -max-occurs "1" \
      < \
        :description "The name of the institution" \
        :restriction -base "enumeration" \
        < \
          :dictionary $DICT_NS:research-organisation \
        > \
      > \
      :element -name "department" -type "string" -min-occurs "0" -max-occurs "1" \
      < \
        :description "The user's department " \
      > \
    > \
   >


#
asset.doc.type.update :create yes :type $COLLECTION_DOCTYPE  \
  :label $COLLECTION_DOCTYPE  \
  :description "A basic minimum meta-data definition that can be attached to collections. Has been created largely by the med.data project and sanctioned by ANDS.  An XSLT could be defined to convert to RIF-CS" \
  :definition < \
    :element -name "key" -type "string" -index "true" -max-occurs "1" \
    < \
      :description "A unique identifier of the collection for computational purposes (autogenerated)
" \
      :instructions "Auto generate

RIF-CS element : key" \
    > \
    :element -name "rcp-allocation-id" -type "string" -index "true" -min-occurs "0" -max-occurs "1" \
    < \
      :description "The id of the associated RCP allocation" \
    > \
    :element -name "name" -type "string" -index "true" -min-occurs 0 -max-occurs "1" \
    < \
      :description "Name/title of the collection" \
      :instructions "Individual manual entry

RIF-CS element : name" \
    > \
    :element -name "description" -type "string" -index "true" -min-occurs 0 -max-occurs "1" \
    < \
      :description "A description of the collection (can be as short
 or as long as necessary)" \
      :instructions "Maybe automatically popoulated from a provisioning system or individual manual entry

RIF-CS element : description" \
    > \
    :element -name "keyword" -type "string" -index "true" -min-occurs 0 \
    < \
       :description "A keyword that adds value to the context of the collection. E.g. a term someone might search on" \
    > \
    :element -name "subject" -type "string" -index "true" \
    < \
      :description "At least one subject. Should be ANZSRC-FoR/SEO code,
 but can also be ‘keyword’ or any other vocab (as long as it can be identified)" \
      :instructions "Applied across a number of datasets/projects + optional manual entry

RIF-CS element : subject

Should be a dictionary." \
      :attribute -name "type" -type "enumeration" -index "true" \
      < \
        :description "Type of code" \
        :restriction -base "enumeration" \
        < \
          :value "ANZSRC-FOR" \
          :value "ANZSRC-SEO" \
          :value "Other" \
        > \
      > \
      :attribute -name "other_type" -type "string" -index "true"  -min-occurs 0 \
      < \
        :description "Describe what type of code this is if you selected type='other'" \
      > \
    > \
    :element -name "owner" -type "document" -min-occurs 1  \
    < \
      :description "A person or entity that owns this dataset. This is the equivalent of the UoM Research Computing Activity Owner (RCAO)." \
      :instructions "Applied across a number of datasets/projects + optional manual entry

RIF-CS element : owner" \
      :element -name "first" -type "string" -max-occurs "1" \
      < \
        :description "First name" \
      > \
      :element -name "middle" -type "string" -index "true" -min-occurs "0" \
      < \
        :description "Middle name" \
      > \
      :element -name "last" -type "string" -max-occurs "1" \
      < \
        :description "Last name" \
      > \
      :element -name "email" -type "email-address" -index "true" \
      < \
        :description "email address" \
      > \
      :element -name "organisation" -type "enumeration" -index "true" -max-occurs "1" \
      < \
        :description "Organisation that owner belongs to (when the collection was created)" \
        :restriction -base "enumeration" \
        < \
          :dictionary $DICT_NS:research-organisation \
        > \
      > \
     :element -name "faculty" -type "enumeration" -min-occurs "0" \
      < \
        :description "Track the faculty (if concept relevant) of the owner." \
        :instructions "There is currently no context-dependence in Mediaflux interfaces so that we could present different lists dependent on the owner's organisation.  For now this is a University of Melbourne list with value 'Other' and
an additional element to fill in." \
        :restriction -base "enumeration" \
         < \
          :dictionary $DICT_NS:research-organisation-faculty \
         > \
        :attribute -name "other" -type "string" -min-occurs "0" \
        < \
          :description "Enter this field for Faculties not in the enumerated list (UoM faculties)" \
        > \
      > \
      :element -name "NLA-ID" -type "string" -min-occurs "0" -max-occurs "1" \
      < \
        :description "Globally unique National Library of Australia Identifier of the owner" \
      > \
    > \
    :element -name "access_rights" -type "enumeration" -index "true" -max-occurs "1" -default restricted \
    < \
      :description "Whether or not the dataset can be accessed openly (open), or if it requires permission or some other condition to be met (conditional), or is completely closed (restricted).
" \
      :instructions "Applied across a number of datasets/projects + optional manual entry

RIF-CS element : rights/ accessRights/@type" \
      :restriction -base "enumeration" \
      < \
        :value "open" \
        :value "conditional" \
        :value "restricted" \
      > \
      :attribute -name "description" -type "string" -min-occurs "0" \
      < \
        :description "Any free text description you would like to add to qualify the rights." \
      > \
    > \
    :element -name "retention" -type "document" -index "true" -min-occurs "0" -max-occurs "1" \
    < \
      :description "The date of review for retaining the collection.  This should trigger a review of the collection." \
      :instructions "A scheduled job needs to inspect this meta-data and take action at the appropriate time.  You must provide either an email or contact name." \
      :element -name "review" -type "date" -min-occurs 0 -max-occurs "1" \
      < \
        :description "The date of expiration for retaining the collection. " \
        :restriction -base "date" \
        < \
          :time "false" \
        > \
      > \
      :element -name "action" -type "enumeration" -min-occurs 0 -max-occurs "1" \
      < \
        :description "What to do when the expiration date is reached." \
        :restriction -base "enumeration" \
        < \
          :value "notify" \
        > \
      > \
      :element -name contacts -type document -min-occurs 0 -max-occurs 1 \
      < \
         :description "One or more contact persons (e.g. the Chief Investigator)  to assist in decision making when the expiry time has lapsed." \
         :element -name contact -type document -min-occurs 0 -max-occurs infinity \
         < \
           :element -name name -type string -min-occurs 0 -max-occurs 1 \
           < \
               :description "Full name and title." \
           > \
           :element -name "organisation" -type "document" -min-occurs 0 -max-occurs "infinity" \
           < \
             :description "The organisation that the user is a member of." \
             :element -name "name" -type "enumeration" -min-occurs 0  -max-occurs "1" \
              < \
                  :description "The name of the institution" \
                  :restriction -base "enumeration" \
                  < \
                     :dictionary ${DICT_NS}:research-organisation \
                  > \
              > \
              :element -name "other" -type "string" -min-occurs "0" -max-occurs 1 \
              < \
                 :description "Any other information you have about the oragnisational affiliation of the contact." \
              > \
           > \
           :element -name email -type email-address -min-occurs 0 -max-occurs infinity \
         > \
      > \
    > \
    :element -name "licence" -type "string" -index "true" -max-occurs "1" -default closed \
    < \
      :description "Licence under which this dataset is published. If no licence, then explicitly state ‘no licence’." \
      :instructions "Applied across a number of datasets/projects + optional manual entry - RIF-CS element : rights/licence" \
    > \
    :element -name "location" -type "string" -index "true" -max-occurs "1"  -default Melbourne\
    < \
      :description "URI of location as close to data as possible (if not open access, this location can be restricted, as long as the location is identifiable)." \
      :instructions "Auto generate - RIF-CS element : location
" \
    > \
    :element -name "metadata_source" -type "string" -index "true" -max-occurs "1" -default VicNode \
    < \
      :description "Master metadata record, ultimate source of metadata." \
      :instructions "Auto generate

RIF-CS element : relatedinfo" \
    > \
    :element -name "funding_source" -type "string" -index "true" -min-occurs "0" \
    < \
      :description "ARC/NHMRC funded project reference" \
      :instructions "Applied across a number of datasets/projects + optional manual entry

RIF-CS element : relatedObject/project" \
    > \
    :element -name "archive_date" -type "date" -min-occurs "0" -max-occurs "1" \
    < \
      :description "Record the date the data was archived
" \
      :instructions "Auto generate

RIC-CS element : no direct equivalent" \
    > \
    :element -name "related_data" -type "string" -index "true" -min-occurs "0" \
    < \
      :description "Record related research data sets/collections (and optionally specify their relationship)" \
      :instructions "Applied across a number of datasets/projects + optional manual entry

RIF-CS element : relatedObject/ collection
@type" \
    > \
   >
}