# Create integration components for right-click on assets
# from the Desktop

# 
# Provisioning assets
#
proc provisioningAsset { SERVICE_ROOT ASSET_NS_ADMIN PROVISIONING_ROLE } {
system.service.add :replace-if-exists "yes" :name "${SERVICE_ROOT}.desktop.project.create" \
  :label "Provision Project" \
  :description "Utilizes the asset provisioning system to create a standard project from the input of a user resource asset." \
  :instructions "Any user accounts specified in the onboarding asset must be pre-created and specified storage must be pre-allocated." \
  :access "modify" \
  :visible-to ${PROVISIONING_ROLE} -type "role" \
  :usage "adesktop" :usage "asset" :usage "menu" \
  :definition < :element -name "id" -type "string" -max-occurs "1" > \
  :precondition "if { \[xvalue asset/namespace \[asset.get :id \$id\]\] == \\\"/${ASSET_NS_ADMIN}/Onboarding\\\" } { return true; } else { return false; }" \
  :execute "${SERVICE_ROOT}.project.create :id \[xvalue id \$args\]"
}
 

# Download shareable from DataMover upload file

# Create a download shareable from a DataMover upload manifest asset
#
# The :valid-to argument is optional so there is extra clever Wilson
# code to only pass it along to the underlying service if the user
# supplies it and to not pass it if not supplied.
# the user supplies it.  
#
proc manifestAsset { SERVICE_ROOT STANDARD_USER_ROLE } {

system.service.add :replace-if-exists yes :name ${SERVICE_ROOT}.desktop.manifest.shareable.download.create \
    :label "Create download shareable from manifest asset." \
    :description "Creates a download shareable for the current namespace that the asset resides in." \
    :access "modify" \
    :visible-to ${STANDARD_USER_ROLE} -type "role" \
    :usage "adesktop" :usage "asset" :usage "menu" \
    :definition < \
        :element -name "id" -type "string" -max-occurs "1" \
        :element -name "email" -type "email-address" -min-occurs 1 -max-occurs "1" < \
            :description "An email address to send the shareable to." \
        > \
        :element -name "valid-to" -type "date" -min-occurs 0 -max-occurs "1" < \
            :description "The date on which the shareable will expire. Defaults to 30days from the time of creation." \
        > \
    > \
    :precondition "if { \[xexists asset/meta/unimelb:shareable-upload-manifest \[asset.get :id \$id\]\] == \\\"1\\\" } { return true; } else { return false; }" \
    :execute "unimelb.asset.shareable.download.create :id \[xvalue id \$args\] :valid-to \[expr { \[xvalue valid-to \$args\]=={} ? \\\"today+30day\\\" : \[xvalue valid-to \$args\] } \] :email \[xvalue email \$args\]"   
}
    