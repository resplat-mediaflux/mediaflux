#
# author Neil Killeen
#
# Copyright (c) 2016, The University of Melbourne, Australia
#
# All rights reserved.
#
proc set_end_user_permissions { ASSET_NS ASSET_NS_ADMIN \
                                ROLE_NS \
                                DOC_NS DOC_NS_ADMIN \
                                DICT_NS \
                                STANDARD_USER_ROLE  } {

#
#######################
# End-user Permissions
#######################t.

# The Admin asset namespace (ASSET_NS_ADMIN) has onboarding assets
# The user asset namespace (ASSET_NS) has just query templates for all to access
#
# Roles
authorization.role.create :role $STANDARD_USER_ROLE \
    :description "This role is granted to end users. It provides access to administration resources such as the end user meta-data, role and dictionary namespaces" \
    :ifexists ignore

# Grant standard user access to common system namespaces (":" means the global namespace)
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type document:namespace ":" :access ACCESS >
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type document:namespace "arc" :access ACCESS >
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type document:namespace "arc.books" :access ACCESS >
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type document:namespace "unimelb" :access ACCESS >


# The root projects asset namespace. Give users access to parent.
set PROJECTROOT [xvalue property [server.property.get :name "projects.namespace"]]
if { [xvalue exists [asset.namespace.exists :namespace ${PROJECTROOT}]] == "true" } {
  asset.namespace.acl.grant :namespace ${PROJECTROOT} :acl < :access <  :namespace access :namespace execute > :actor -type role ${STANDARD_USER_ROLE} >
}

# This for the asset modelling framework. A developer will need PUBLISH and MODIFY
if { [xvalue exists [asset.doc.namespace.exists :namespace "arc.layout"]] == "true" } {
   actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type document:namespace "arc.layout" :access ACCESS >
}
#
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type document:namespace "arc.licence" :access ACCESS >
if { [xvalue exists [asset.doc.namespace.exists :namespace "arc.workspace"]] == "true" } {
   actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type document:namespace "arc.workspace" :access ACCESS >
}
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type dictionary:namespace ":"  :access ACCESS >
#

# The various end user namespaces that users need access to
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type document:namespace ${DOC_NS} :access ACCESS >
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type dictionary:namespace ${DICT_NS} :access ACCESS >
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type role:namespace ${ROLE_NS} :access ACCESS >
# Access to document types in the admin namespace (e.g. posix mount point doc type)
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type document:namespace ${DOC_NS_ADMIN} :access ACCESS >

# For the end user asset name space (generic query templates live in here) set an ACL so standard users can access
asset.namespace.acl.grant :namespace ${ASSET_NS} :acl < :actor -type role $STANDARD_USER_ROLE :access < :asset access \
     :asset-content access :namespace access :namespace execute > >

# Document types
actor.grant :name $STANDARD_USER_ROLE  :type role :perm < :resource -type document "*" :access PUBLISH >

# Other
actor.grant :name $STANDARD_USER_ROLE  :type role :role -type role service-user
# The following role is very unclear to me, as it's synthesied/automatic. To avoid confusion, grant it explicitly here.
actor.grant :name $STANDARD_USER_ROLE  :type role :role -type role user

}



proc set_admin_user_permissions { LOCAL_AUTH \
								  LOCAL_ADMIN_AUTH \
                                  ADMIN_ROLE \
                                  ADMIN_SERVICES_ROLE \
                                  INSTRUMENT_UPLOAD_ADMIN_ROLE \
                                  ASSET_NS \
                                  ASSET_NS_ADMIN \
                                  ROLE_NS \
                                  ROLE_NS_ADMIN \
                                  DOC_NS \
                                  DOC_NS_ADMIN \
                                  DICT_NS \
                                  DICT_NS_ADMIN \
                                  STANDARD_USER_ROLE \
                                  SERVICE_ROOT } {
# Role creations
authorization.role.create :role $ADMIN_ROLE \
    :description "This role is granted to the project provisioning operations team. It provides access to administration resources such as the provisioning administratin meta-data, role and dictionary namespaces" \
    :ifexists ignore
authorization.role.create :role $ADMIN_SERVICES_ROLE \
    :description "This role is granted the permissions to run services requiring ADMINISTER. It is then in turn granted to the project provisioning operations team." \
    :ifexists ignore

authorization.role.create :role $INSTRUMENT_UPLOAD_ADMIN_ROLE \
    :description "This role is for facility post-upload handler service: vicnode.facility.shareable.upload.complete service to create the global instrument-upload manifest assets in ACL protected asset namespace, such as /local-admin/instrument-upload-manifests." \
    :ifexists ignore

#
actor.grant :name $ADMIN_ROLE :type role :role -type role $ADMIN_SERVICES_ROLE

# Admin asset namespace
asset.namespace.acl.grant :namespace ${ASSET_NS_ADMIN}  :acl < :actor -type role $ADMIN_ROLE \
   :access < :asset create :asset access :asset modify :asset destroy \
   :asset-content access :asset-content modify  \
   :namespace access :namespace execute :namespace administer :namespace destroy :namespace create > > 

# Administration of the end user namespace
asset.namespace.acl.grant :namespace ${ASSET_NS} :acl < :actor -type role $ADMIN_ROLE :access < \
   :asset create :asset access :asset modify :asset destroy \
   :asset-content access :asset-content modify  \
   :namespace access :namespace execute :namespace administer :namespace destroy :namespace create > > 

# Set visibility of this namespace so end-users can't see (but they can access)
asset.namespace.visibility.set :namespace ${ASSET_NS}  :visible-to < :actor -type role ${ADMIN_ROLE} >


# Set up meta-data templates for User onboarding
asset.namespace.template.set :namespace ${ASSET_NS_ADMIN}/Onboarding \
 :template < :metadata < :definition  -requirement optional $DOC_NS:Collection > \
             :metadata < :definition ${DOC_NS_ADMIN}:User-Onboarding > >
#
actor.grant :name $ADMIN_ROLE :type role :perm < :resource -type document:namespace $DOC_NS_ADMIN :access ADMINISTER >
actor.grant :name $ADMIN_ROLE :type role :perm < :resource -type dictionary:namespace $DICT_NS_ADMIN  :access ADMINISTER >
actor.grant :name $ADMIN_ROLE :type role :perm < :resource -type role:namespace $ROLE_NS_ADMIN :access ADMINISTER >

# Holds the standard role
actor.grant :name $ADMIN_ROLE :type role :role -type role $STANDARD_USER_ROLE

# End-user namespaces (That admin needs to administer)
actor.grant :name $ADMIN_ROLE :type role :perm < :resource -type document:namespace $DOC_NS :access ADMINISTER >
actor.grant :name $ADMIN_ROLE :type role :perm < :resource -type dictionary:namespace $DICT_NS  :access ADMINISTER >
actor.grant :name $ADMIN_ROLE :type role :perm < :resource -type role:namespace $ROLE_NS :access ADMINISTER >

# Service permissions. Some services require administer priv. to execute
# These services uses some unimelb essentials package services 
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service unimelb.user.disable.set >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service unimelb.user.disable >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service unimelb.replicate.check >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service unimelb.replicate.path.check >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service unimelb.replicate.synchronize >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service unimelb.asset.index.validate  >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service unimelb.asset.content.access.last  >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service unimelb.asset.namespace.acl.child.list >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service asset.namespace.integrity.check >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service actor.grant >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service actor.revoke >
#
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.namespace.revoke.ACL.all >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.admin.message.send >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.asset.count >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.asset.duplicate.find >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.copy.collection.from.asset >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.copy.project.from.asset >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.content.access.last >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.describe >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.describe.in.date.range >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.grant.access.to.admin >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.integrity.check >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.type.set >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.namespace.mtime.query >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.namespace.metadata.copy >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.namespace.move >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.namespace.set.standard-ACLs >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.namespace.ACL.child.list >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.namespace.child.find >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.namespace.users.exclude >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.parent.namespace.add >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.parent.namespace.list >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.parents.namespace.notifications.for.schedule >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.map.describe >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.map.domain.identity.add >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.map.domain.identity.remove >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.map.uid.add >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.map.uid.remove >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.mount >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.mount.describe >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.mount.list >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.mount.standard >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.remount >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.unmount >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.protocols >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.queries.namespace.add >

actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.quota.set >

actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.quota.set.limit.action >
#actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.rename >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.replication.check >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.replicate.queue.set >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.replicate.queue.unset >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.replicate.queue.list >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.replicate.queue.reset >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.replicate.queue.rebuild >


actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.retention.notify >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.role.add >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.service.collection.modify >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.size >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.store.set >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.summary.for.members >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.summary.for.schedule >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.DM.operator.expiry.notification >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.DM.enduser.expiry.notification  >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.shareable.find  >

actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.user.list >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.user.domain.list >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.user.none >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.user.add >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.user.remove >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.actor.remove.all >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.users.message.send >
#actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.server.cluster.check >
#
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.create >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.grant.standard-roles >
# These services were one offs in a migration process
#actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.ldap.user.meta.fix >
#actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.ldap.migrate.disable >
#actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.ldap.migrate.to >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.message.send >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.project.list >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.project.describe >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.project.has.writable >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.project.roles.remove >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.project.role.list >

# DataMover services
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.DM.enduser.expiry.notification >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.DM.expired.DR.move >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.DM.operator.expiry.notification >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.DM.upload.count >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.DM.manifest.asset.find >
   
# Content Copy processing queue management
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.content-copy.queue.set >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.content-copy.queue.unset >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.content-copy.queue.rebuild >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.content-copy.queue.recreate >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.content-copy.queue.reset >
                               
# Data Registry Services
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.data.registry.mediaflux.collection.describe >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.data.registry.mediaflux.collection.list >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.data.registry.sync >

# Provision email/message
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.provision.message.get >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.provision.message.send >

# Extra standard services it is useful for this role to be able to access
# Posix Mounts
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.identity.map.create >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.identity.map.destroy >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.asset.network.mount >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.asset.network.unmount >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.identity.domain.identity.describe >
actor.grant :name $ADMIN_SERVICES_ROLE :type role :perm < :access MODIFY :resource -type service authentication.user.create >

# This service needs to be able to undertake a range of privileged activities so it's super powerful.
actor.grant :name $SERVICE_ROOT.project.create :type plugin:service \
    :role -type role system-administrator

# These services need to access dictionary namespace VicNode-Admin:project-namespace-map
actor.grant :name $SERVICE_ROOT.project.find :type plugin:service \
   :perm < :access ACCESS :resource -type dictionary:namespace $DICT_NS_ADMIN >
actor.grant :name $SERVICE_ROOT.user.self.project.list :type plugin:service \
   :perm < :access ACCESS :resource -type dictionary:namespace $DICT_NS_ADMIN >
actor.grant :name $SERVICE_ROOT.user.project.has.writable :type plugin:service \
   :perm < :access ACCESS :resource -type dictionary:namespace $DICT_NS_ADMIN >
actor.grant :name $SERVICE_ROOT.project.DM.operator.expiry.notification :type plugin:service \
   :perm < :access ACCESS :resource -type dictionary:namespace $DICT_NS_ADMIN >
actor.grant :name $SERVICE_ROOT.project.DM.operator.expiry.list :type plugin:service \
   :perm < :access ACCESS :resource -type dictionary:namespace $DICT_NS_ADMIN >
actor.grant :name $SERVICE_ROOT.project.DM.enduser.expiry.notification :type plugin:service \
   :perm < :access ACCESS :resource -type dictionary:namespace $DICT_NS_ADMIN >
   
# This service is used by the Desktop plugin allowing project admins to set
# exclusion ACLs.  It needs some privilege to do this.
actor.grant :name $SERVICE_ROOT.user.self.project.admin.is :type plugin:service :perm < :access ACCESS :resource -type dictionary:namespace $DICT_NS_ADMIN  >
actor.grant :name $SERVICE_ROOT.user.self.project.admin.is :type plugin:service :perm < :access ADMINISTER :resource -type service unimelb.asset.namespace.acl.actor.exclude  >

# Authentication domain administration
actor.grant :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service actor.grant >
actor.grant :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type authentication:domain $LOCAL_AUTH >
actor.grant :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type authentication:domain $LOCAL_ADMIN_AUTH >

}


proc set_provisioning_user_permissions { PROVISIONING_ROLE STANDARD_USER_ROLE \
                                         ADMIN_ROLE SERVICE_ROOT } {


authorization.role.create :role $PROVISIONING_ROLE \
    :description "This role is granted to a specialised account used to provision projects. In particular it has access to services that the standard project administration role does not need." \
    :ifexists ignore
actor.grant :name $PROVISIONING_ROLE :type role :role -type role user
actor.grant :name $PROVISIONING_ROLE :type role :role -type role $STANDARD_USER_ROLE
actor.grant :name $PROVISIONING_ROLE :type role :role -type role $ADMIN_ROLE
actor.grant :name $PROVISIONING_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.create >
#
# Allow to run the provisioning services "asset.project.*"
# We no longer use these services
#actor.grant :name $PROVISIONING_ROLE :type role :role -type role provisioning-manager
}




proc set_permissions { LOCAL_AUTH LOCAL_ADMIN_AUTH  \
                       ASSET_NS \
                       ASSET_NS_ADMIN \
                       ROLE_NS \
                       ROLE_NS_ADMIN \
                       DOC_NS \
                       DOC_NS_ADMIN \
                       DICT_NS \
                       DICT_NS_ADMIN \
                       SERVICE_ROOT \
                       ADMIN_ROLE \
                       ADMIN_SERVICES_ROLE \
                       INSTRUMENT_UPLOAD_ADMIN_ROLE \
                       PROVISIONING_ROLE \
                       STANDARD_USER_ROLE \
                       PROJECT_ROLE_NAMESPACE_ADMIN_ROLE \
                       PROJECT_ACCESS_ONLY_ROLE \
                       PROJECT_ASSET_NAMESPACE_ADMIN_ROLE } {

#
#######################
# End-user Permissions
#######################
set_end_user_permissions $ASSET_NS \
						 $ASSET_NS_ADMIN \
                         $ROLE_NS \
                         $DOC_NS \
                         $DOC_NS_ADMIN \
                         $DICT_NS \
                         $STANDARD_USER_ROLE
#
#############################
# Administration Permissions
#############################
#

set_admin_user_permissions $LOCAL_AUTH \
                           $LOCAL_ADMIN_AUTH \
                           $ADMIN_ROLE \
                           $ADMIN_SERVICES_ROLE \
                           $INSTRUMENT_UPLOAD_ADMIN_ROLE \
                           $ASSET_NS \
                           $ASSET_NS_ADMIN \
                           $ROLE_NS \
                           $ROLE_NS_ADMIN \
                           $DOC_NS \
                           $DOC_NS_ADMIN \
                           $DICT_NS \
                           $DICT_NS_ADMIN \
                           $STANDARD_USER_ROLE \
                           $SERVICE_ROOT
                                  
#
######################################
# Provisioning role 
# admin role + plus some extras. 
# Perhaps this role should be junked 
# as it is so similar to admin
######################################
#
set_provisioning_user_permissions $PROVISIONING_ROLE \
                                  $STANDARD_USER_ROLE \
                                  $ADMIN_ROLE \
                                  $SERVICE_ROOT



##############################################################################################
# Read-only access role
#
# This role is used to grant read-only access to all provisioned projects' asset, document and
# dictionary namespaces. It's intended for administration purposes only.  It should 
# be held in conjunction with other roles such as STANDARD_USER or PROVISION_ROLE 
# or ADMIN_ROLE as it only controls access to the provisioned Projects layer
# The role is granted permissions to projects by the project creation service.
##############################################################################################

authorization.role.create :role $PROJECT_ACCESS_ONLY_ROLE \
    :description "This role is granted to administrators to gain read-only access to all projects." \
    :ifexists ignore
    
# Grant to admin role
actor.grant :name $ADMIN_ROLE :type role :role -type role $PROJECT_ACCESS_ONLY_ROLE 
    
################################################################################
#
# Project role namespace admin role
#
# This role is used to grant ADMINISTER to all project role namespaces.
# It's intended for administration purposes only. It should be held in conjunction 
# with other roles such as STANDARD_USER or PROVISION_ROLE or ADMIN_ROLE as 
# it only controls access to provisioned Projects.
# The role is granted permissions to projects by the project creation service
# which sets an ACL for this role.
################################################################################

authorization.role.create :role $PROJECT_ROLE_NAMESPACE_ADMIN_ROLE \
    :description "This role is granted to administrators to gain ADMINISTER access on all project role namespaces" \
    :ifexists ignore
    
# Grant to admin role
actor.grant :name $ADMIN_ROLE :type role :role -type role $PROJECT_ROLE_NAMESPACE_ADMIN_ROLE

# Grant to this service which needs to check user roles
actor.grant :name $SERVICE_ROOT.user.project.has.writable  \
    :type plugin:service :role -type role $PROJECT_ROLE_NAMESPACE_ADMIN_ROLE


################################################################################
#
# Project asset namespace admin role
#
# This role is used to grant ADMINISTER to all project asset namespaces.
# It's intended for administration purposes only, generally to be held
# by specialised services.  
#
# The role is granted permissions to projects by the project creation service
# which sets an ACL for this role
################################################################################

authorization.role.create :role $PROJECT_ASSET_NAMESPACE_ADMIN_ROLE \
    :description "This role is granted to specialised services to gain ADMINISTER access on all project asset namespaces." \
    :ifexists ignore

# service to set quota
actor.grant :name $SERVICE_ROOT.project.quota.set :type plugin:service \
    :role -type role $PROJECT_ASSET_NAMESPACE_ADMIN_ROLE

}
 
 
 
 
proc revoke_deprecated_roles { ADMIN_ROLE SERVICE_ROOT } {

# One off application once the service-admin role is invoked
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.integrity.check >
# These 3 services were in the nig packaghe when this was done (so leave).  They are now in unimelb.*
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service nig.replicate.check >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service nig.replicate.namespace.check >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service nig.replicate.synchronize >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service asset.namespace.integrity.check >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service actor.revoke >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service actor.revoke >
#
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.namespace.revoke.ACL.all >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.admin.message.send >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.copy.collection.from.asset >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.copy.project.from.asset >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.grant.access.to.admin >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.integrity.check >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.make.production >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.namespace.metadata.copy >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.namespace.move >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.namespace.set.standard-ACLs >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.parents.list >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.parents.namespace.notifications.for.schedule >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.remount >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.mount >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.map.uid.add >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.map.uid.remove >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.map.describe >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.describe >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.summary.for.members >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.summary.for.schedule >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.describe.norep >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.user.add >
#
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.queries.namespace.add >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.quota.sum >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.quota.usage >
#actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.rename >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.retention.notify >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.user.remove >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.user.list >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.users.message.send >
#
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.create >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.grant.standard-roles >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.message.send >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.user.project.list >
# ADMINISTER no longer required
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service $SERVICE_ROOT.project.posix.mount.list >
#
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.identity.map.create >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.identity.map.destroy >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.asset.network.mount >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.asset.network.unmount >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.asset.mount.list >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service posix.fs.asset.mount.describe >
actor.revoke :name $ADMIN_ROLE :type role :perm < :access ADMINISTER :resource -type service authentication.user.create >
#
# THese services don't exist any more - they leave behind dangling permissions
# "vicnode.namespace.grant.standard-ACLs"
# "vicnode.project.grant.role-admin.to.admin"
# "vicnode.project.store.migrate"
# "vicnode.project.summary"
 }
