# FOr all projects adds the new role <project>participant-acmd-n
# and reset the ACLs
#

proc addNewProjectRole { SERVICE_ROOT } {

  set ids [xvalues project [${SERVICE_ROOT}.project.list :prod-type all :rep-type all]]
  foreach id $ids {

# Create role
    set role ${id}:participant-acmd-n
    if { [ xvalue exists [authorization.role.exists :role ${role}]] == "false" }  {
      authorization.role.create :role ${role} :ifexists ignore
      srefresh
      actor.grant :name  ${role} :type role \
        :perm < :access "ACCESS" :resource -type "dictionary:namespace" ${id} > \
        :perm <  :access "ACCESS" :resource -type "document:namespace" ${id}  > \
        :perm <  :access "ACCESS" :resource -type "role:namespace" ${id}  >

# Set ACLs on project namespace
      set namespace [xvalue project/namespace [${SERVICE_ROOT}.project.describe :project-id ${id} :prod-type all :rep-type all]]
      ${SERVICE_ROOT}.project.namespace.set.standard-ACLs  :project ${id}

# 
      puts “Added new role ${role} to project ${id}”
    }
  }
}