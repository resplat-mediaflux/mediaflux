# FOr all projects adds the new role namespace permissons  to all project roles.
# These arrived in MF 4.7.020 resolving permisson model issues


proc addRoleNameSpacPerms { SERVICE_ROOT } {

  # Iterate through projects
  set ids [xvalues project [${SERVICE_ROOT}.project.list :prod-type all :rep-type all]]
  foreach id $ids {

#  Find all project roles
    set roles [xvalues role [authorization.role.list :namespace $id]]
   
# Iterate through the roles
    foreach role $roles {
  
  # Add the new permissions as needed
       set t "${id}:administrator"
       if { ${role} == ${t} } {
          if { [xvalue actor/perm [actor.have :type role :name ${role} :perm < :access ADMINISTER :resource -type role:namespace ${id} > ]] == "false" } {
 	         puts "     Adding role namespace ADMINISTER permission to ${role}"
             actor.grant :name $role :type role :perm < :access ADMINISTER :resource -type role:namespace $id > 
          }
       } else {
         if { [xvalue actor/perm [actor.have :type role :name ${role} :perm < :access ACCESS :resource -type role:namespace ${id} > ]] == "false" } {
  	        puts "      Adding role namespace ACCESS permission to ${role}"
            actor.grant :name $role :type role :perm < :access ACCESS :resource -type role:namespace $id >
         }
       }
    }
  }
}
  
