#
# author Neil Killeen
#
# Copyright (c) 2016, The University of Melbourne, Australia
#
# All rights reserved.
#
# remove plugin module
set plugin_namespace  "mflux/plugins"
set plugin_jar        "vicnode-mflux-plugin.jar"
set module_class      "au.org.vicnode.mflux.VicNodePluginModule"
if { [xvalue exists [plugin.module.exists :path ${plugin_namespace}/${plugin_jar} :class ${module_class}]] == "true" } {
    plugin.module.remove :path ${plugin_namespace}/${plugin_jar} :class ${module_class}
}
