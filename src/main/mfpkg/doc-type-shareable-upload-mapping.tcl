#
# doc namespace
# 
if { [xvalue exists [asset.doc.namespace.exists :namespace unimelb]] == "false" } {
    asset.doc.namespace.create :namespace unimelb
}

#
# doc type: unimelb:shareable-upload-mapping
# 
asset.doc.type.update :create true :index all :type unimelb:shareable-upload-mapping \
:definition < \
    :element -name shareable-id -type long -min-occurs 1 -max-occurs 1 < \
        :restriction -base long < :minimum 1 > > \
    :element -name rule -type document -min-occurs 0 -max-occurs infinity < \
        :element -name source -type string -min-occurs 1 -max-occurs 1 < \
            :description "The regular expression pattern to test the context parent path of the upload." \
            :attribute -name ignore-case -type boolean -min-occurs 0 > \
        :element -name destination -type string -min-occurs 1 -max-occurs 1 < \
            :description "The destination parent path pattern." \
            :attribute -name create -type boolean -min-occurs 0 < \
                :description "Create parent namespaces if not found" > > > \
    :element -name add-note -type boolean -min-occurs 0 -max-occurs 1 < \
        :description "Add mf-note meta data to the moved assets to trigger replication. Defaults to true." > >