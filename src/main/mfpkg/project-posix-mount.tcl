#
# author Neil Killeen
#
# Copyright (c) 2016, The University of Melbourne, Australia
#
# All rights reserved.
#
# This file should be installed in $MFLUX_HOME/config/services
# It will be execute when the server is restarted. 
# It recreates transient Posix mount points for Projects (SFTP access)
#
vicnode.project.posix.remount