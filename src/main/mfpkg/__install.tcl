#
# author Neil Killeen
#
# Copyright (c) 2016, The University of Melbourne, Australia
#
# All rights reserved.
#
# Supply arguments with 
#  package.install :arg -name <arg name> <value>
#
# Arguments:
#  attica - configures no DR and ops emails for Attica server if set to true. Default is false.
#
# There is service dependency on the unimelb-essentials package for
# the project integrity and replication check services
if { [xvalue exists [package.exists :package unimelb-mf-essentials-plugin]] == "false" } {
   error "You must install the unimelb-mf-essentials-plugin package first."
}


####
# Configurable variables.  The intent is to just allow you to modify the names of the namespaces
# and service root, not the actual document type or dictionary names. If we ever deploy this
# framework or some derivative nationally, it may be that we want these namespaces to be the
# same everywhere (burden to keep aligned and hard to evolve).  See Properties.java for
# matching compile time strings
#
#
# This is the -app attribute of our application properties (application.property.list)
set APP_PROPERTIES_NAME project-provisioning
source properties.tcl

# Parse input arguments
set doAttica 0
if { [info exists attica ] } {
    if { $attica == "true" } {
	   set doAttica 1
    }
}
# Populate basic operations variables in an array
source config.tcl
array set cfg [get_config $doAttica]

# Populate provisioning system variables in an array
source config_vars.tcl
array set cfg_vars [get_config_vars]

###
# Create application properties that are presented by Mediaflux and read
# by the Java class Properties.java.  It's a means of defining things here
# in the Tcl layer only and not having to define them again in Properties.java
#
# Active Directory authentication domains
create_application_property $APP_PROPERTIES_NAME authentication.ldap.staff.domain $cfg_vars(AD_STAFF) true
create_application_property $APP_PROPERTIES_NAME authentication.ldap.student.domain $cfg_vars(AD_STUDENT) true

# The user and admin authentication domains
create_application_property $APP_PROPERTIES_NAME authentication.local.user $cfg_vars(LOCAL_AUTH) true
create_application_property $APP_PROPERTIES_NAME authentication.local.admin $cfg_vars(LOCAL_ADMIN_AUTH) true

# The user and admin asset namespaces
create_application_property $APP_PROPERTIES_NAME asset.namespace.user $cfg_vars(ASSET_NS) true
create_application_property $APP_PROPERTIES_NAME asset.namespace.admin $cfg_vars(ASSET_NS_ADMIN) true
create_application_property $APP_PROPERTIES_NAME asset.namespace.instrument-upload-manifests $cfg_vars(ASSET_NS_INSTRUMENT_UPLOAD_MANIFESTS) true

# The user and admin document namespaces
create_application_property $APP_PROPERTIES_NAME doc.namespace.user $cfg_vars(DOC_NS) true
create_application_property $APP_PROPERTIES_NAME doc.namespace.admin $cfg_vars(DOC_NS_ADMIN) true

# The user and admin dictionary namespaces
create_application_property $APP_PROPERTIES_NAME dict.namespace.user $cfg_vars(DICT_NS) true
create_application_property $APP_PROPERTIES_NAME dict.namespace.admin $cfg_vars(DICT_NS_ADMIN) true

# The user and admin dictionary namespaces
create_application_property $APP_PROPERTIES_NAME role.namespace.user $cfg_vars(ROLE_NS) true
create_application_property $APP_PROPERTIES_NAME role.namespace.admin $cfg_vars(ROLE_NS_ADMIN) true

# Service collection name (providing context menus on namespaces via Desktop)
create_application_property $APP_PROPERTIES_NAME service.collection.namespace $cfg_vars(NAMESPACE_SERVICE_COLLECTION) true

# Asset processing queue associated with project namespaces for replication
# Number of processors.
create_application_property $APP_PROPERTIES_NAME replication.queue.nb-processors $cfg_vars(REPLICATION_QUEUE_NB_PROCESSORS) false
 
# Asset processing queue associated with project namespaces for content copies
# The system may be configured with content copies being generated as true or false
# We don't want to overwrite that state, so once the properties have been created,
# we don't replace them here in the installer. Remove the property if you want to 
# start afresh or set to false to disable (but you have to remove the queue
# from namespaces also).
# We use the number of processors to detect if we want this capability on (n_proc>0)
# or off (n_proc<0) in the Java layer when creating a project. The default 
# configuration is off (n_proc=-1)
#####
# Number of processors.
create_application_property $APP_PROPERTIES_NAME content-copy.queue.nb-processors $cfg_vars(CONTENT_COPY_QUEUE_NB_PROCESSORS) false
# Destination store name
create_application_property $APP_PROPERTIES_NAME content-copy.queue.store.name $cfg_vars(CONTENT_COPY_QUEUE_STORE_NAME) false
#####
 
# End user documentation root (WIki)
create_application_property $APP_PROPERTIES_NAME documentation.user.root $cfg(USER_DOC_ROOT) true

# End user support contact web page
create_application_property $APP_PROPERTIES_NAME support.contact $cfg(CONTACT_US) true

# End-user Mediaflux Explorer page
create_application_property $APP_PROPERTIES_NAME documentation.user.explorer $cfg(USER_DOC_EXPLORER) true

# End-user Mediaflux Data Mover page
create_application_property $APP_PROPERTIES_NAME documentation.user.datamover $cfg(USER_DOC_DATAMOVER) true

# End-user Mediaflux Data Mover CLI page
create_application_property $APP_PROPERTIES_NAME documentation.user.datamover.cli $cfg(USER_DOC_DATAMOVER_CLI) true

# Mediaflux end-user documentation explaining user roles
create_application_property $APP_PROPERTIES_NAME documentation.user.standardprojectroles $cfg(USER_DOC_STANDARD_PROJECT_ROLES) true

# Mediaflux end-user documentation explaining valid filenames
create_application_property $APP_PROPERTIES_NAME documentation.user.valid-filenames $cfg(USER_DOC_VALID_FILENAMES) true

# RCS Terms of Service
create_application_property $APP_PROPERTIES_NAME documentation.user.rcs-tos $cfg(USER_DOC_RCS_TOS) true

# Notification service properties.
create_application_property $APP_PROPERTIES_NAME notification.host $cfg(HOST_AND_PORT) true
create_application_property $APP_PROPERTIES_NAME notification.support.email $cfg(SUPPORT_EMAIL) true
create_application_property $APP_PROPERTIES_NAME notification.operations.email $cfg(OPERATIONS_EMAIL) true

# Disaster Recovery Server.
# If you don't wish to configure a DR server comment these two lines out.
# The name of the federated peer that is used for Disaster Recovery
create_application_property $APP_PROPERTIES_NAME disaster.recovery.peer.name $cfg(DR_PEER) false

# The UUID of the above host MF server
create_application_property $APP_PROPERTIES_NAME disaster.recovery.peer.uuid $cfg(DR_UUID) false

# Data Registry Host
create_application_property $APP_PROPERTIES_NAME data.registry.host $cfg(DATA_REG_HOST) false

# Onboarding Asset Namespace
create_application_property $APP_PROPERTIES_NAME onboarding.asset.namespace $cfg(ONBOARDING_ASSET_NAMESPACE) false

#####

# Stores and namespaces.  Will create default file-system stores called $DEFAULT_STORE and $DEFAULT_STORE_POLICY
# and associate with same-named namespaces.
source stores_namespaces.tcl
create_stores_namespaces $cfg_vars(ASSET_NS) $cfg_vars(ASSET_NS_ADMIN) \
                         $cfg_vars(ASSET_NS_INSTRUMENT_UPLOAD_MANIFESTS) \
                         $cfg_vars(ROLE_NS) $cfg_vars(ROLE_NS_ADMIN) \
                         $cfg_vars(DOC_NS) $cfg_vars(DOC_NS_ADMIN) \
                         $cfg_vars(DICT_NS) $cfg_vars(DICT_NS_ADMIN) \
                         $cfg_vars(DEFAULT_STORE) $cfg_vars(DEFAULT_STORE_POLICY)

# create/update dictionaries. Adds the store  policy $DEFAULT_STORE_POLICY to the dictionary holding the
# names of store policies for the provisioning system. By default, no store policy is added
# to the equivalent DR store dictionary nor the store policy dictionaries.
source dictionaries.tcl
create_dictionaries $cfg_vars(DICT_NS) \
                    $cfg_vars(PRIMARY_STORE_POLICIES_DICT) \
                    $cfg_vars(PROJECT_NAMESPACES_ROOT_DICT) \
                    $cfg_vars(PROJECT_COLLECTIONS_ROOT_DICT) \
                    $cfg_vars(PROJECT_NAMESPACES_MAP_DICT) \
                    $cfg_vars(DEFAULT_STORE_POLICY)

# create/update doc types
source doc-types.tcl
create_doc_types $cfg_vars(DICT_NS) \
                 $cfg_vars(PROJECT_PARENT_ADMIN_DOCTYPE) \
                 $cfg_vars(POSIX_MOUNT_DOCTYPE) \
                 $cfg_vars(PROJECT_DOCTYPE) \
                 $cfg_vars(COLLECTION_DOCTYPE) \
                 $cfg_vars(ONBOARDING_ASSET_DOCTYPE) \
                 $cfg_vars(USER_ACCOUNTS_DOCTYPE) \
                 $cfg_vars(PRIMARY_STORE_POLICIES_DICT) \
                 $cfg_vars(PROJECT_NAMESPACES_ROOT_DICT) \
                 $cfg_vars(PROJECT_COLLECTIONS_ROOT_DICT)

source doc-type-shareable-upload-manifest.tcl
source doc-type-shareable-upload-mapping.tcl

# set Onboarding asset template.  $Collection_DOCTYPE is optional because its highly unlikely
# all the values will be available during the onboarding process
asset.namespace.template.set :namespace $cfg_vars(ASSET_NS_ADMIN)/Onboarding \
    :template < \
       :metadata < :definition -requirement mandatory $cfg_vars(ONBOARDING_ASSET_DOCTYPE) > \
       :metadata < :definition -requirement optional  $cfg_vars(COLLECTION_DOCTYPE) > \
     >

# add plugin module.
# If you change the name of the code package (away from vicnode) you have to change these strings here also.
set plugin_label      [string toupper PACKAGE_vicnode-mflux]
set plugin_namespace  "mflux/plugins"
set plugin_zip        "vicnode-mflux-plugin.zip"
set plugin_jar        "vicnode-mflux-plugin.jar"
set module_class      "au.org.vicnode.mflux.VicNodePluginModule"
asset.import :url archive:${plugin_zip} \
        :namespace -create yes ${plugin_namespace} \
        :label -create yes ${plugin_label} :label PUBLISHED \
        :update true
if { [xvalue exists [plugin.module.exists :path ${plugin_namespace}/${plugin_jar} :class ${module_class}]] == "true" } {
	plugin.module.remove :class ${module_class} :path ${plugin_namespace}/${plugin_jar}
}
plugin.module.add :path ${plugin_namespace}/${plugin_jar} :class ${module_class}
srefresh


# Install standard queries for Explorer users
source Queries/install_explorer_queries.tcl
install_explorer_basic_asset_query $cfg_vars(ASSET_NS) Query
install_explorer_basic_text_query $cfg_vars(ASSET_NS) Query token
#
install_explorer_shareable_upload_manifest_query $cfg_vars(ASSET_NS) Query

# Install standard queries for Desktop users
source Queries/install_desktop_queries.tcl
install_desktop_shareable_upload_manifest_query $cfg_vars(ASSET_NS) Query

# create local auth domains
source domains.tcl
create_domains $cfg_vars(LOCAL_AUTH) $cfg_vars(LOCAL_ADMIN_AUTH)

# grant/update roles and permissions (do this after installing plugin module otherwise new plugin services are not recognized.)
source role-permissions.tcl
set_permissions $cfg_vars(LOCAL_AUTH) $cfg_vars(LOCAL_ADMIN_AUTH) \
                $cfg_vars(ASSET_NS) $cfg_vars(ASSET_NS_ADMIN) \
                $cfg_vars(ROLE_NS) $cfg_vars(ROLE_NS_ADMIN) \
                $cfg_vars(DOC_NS) $cfg_vars(DOC_NS_ADMIN) \
                $cfg_vars(DICT_NS) $cfg_vars(DICT_NS_ADMIN) \
                $cfg_vars(SERVICE_ROOT) \
                $cfg_vars(ADMINISTRATOR_ROLE) \
                $cfg_vars(ADMINISTRATOR_SERVICES_ROLE) \
                $cfg_vars(INSTRUMENT_UPLOAD_ADMIN_ROLE) \
                $cfg_vars(PROVISIONING_ROLE) \
                $cfg_vars(STANDARD_USER_ROLE) \
                $cfg_vars(PROJECT_ROLE_NAMESPACE_ADMIN_ROLE) \
                $cfg_vars(PROJECT_ACCESS_ONLY_ROLE) \
                $cfg_vars(PROJECT_ASSET_NAMESPACE_ADMIN_ROLE)

source service-permissions.tcl
set_instrument_upload_complete_service_permissions $cfg_vars(INSTRUMENT_UPLOAD_ADMIN_ROLE)

# create specialised 'provisioning' account
source accounts.tcl
create_accounts $cfg_vars(LOCAL_ADMIN_AUTH) $cfg_vars(PROVISIONING_ROLE)

# install project-posix-mount.tcl to ${MFLUX_HOME}/config/services/ directory so that it is executed during server restarts.
# It will overwrite the file if it exists.  If you change the root service name away from "vicnode"you must also change
# it in the project-posix-mount.tcl script.  To do this we make an asset holding the script, and then copy from
# the asset to the server side file system. Then we destroy the asset.
set ans "mflux/plugins"
set aname "project-posix-mount.tcl"
if { [xvalue exists [asset.exists :id path=${ans}/${aname}]] == "true" } {
        asset.hard.destroy :id path=${ans}/${aname}
}
asset.create :url archive:///$aname :namespace ${ans} :name ${aname}
asset.get :id path=${ans}/${aname} \
          :url file:[xvalue property\[@key='mf.home'\] [server.java.environment] ]/config/services/${aname}
asset.hard.destroy :id path=${ans}/${aname} 

# Install Desktop service presentations for when you right-click on an asset
source desktop_asset_integration.tcl
# We don't use this approach any more. Note that when I remove it,
# it was not working anyway (right click in the Desktop did not
# bring up the menu item)
#provisioningAsset $cfg_vars(SERVICE_ROOT) $cfg_vars(ASSET_NS_ADMIN) $cfg_vars(PROVISIONING_ROLE)
manifestAsset $cfg_vars(SERVICE_ROOT) $cfg_vars(STANDARD_USER_ROLE)

# Create declared service collection. This service collection is registered with project namespaces
# and presented in Desktop namespace (rather than asset) menus. 
# Child namespaces inherit the service collection.
source desktop_namespace_integration.tcl
createDeclaredServiceCollection $cfg_vars(NAMESPACE_SERVICE_COLLECTION)
  
