package unimelb.utils;

import java.net.InetAddress;
import java.util.Hashtable;

import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class DNSUtils {

    public static boolean domainExists(String domainName) {
        try {
            InetAddress.getByName(domainName);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * Check if the specified domain name is a valid mail domain (by checking if the domain has associated MX records.)
     *
     * @param domainName the domain name to verify.
     * @return true if the domain has one or more MX records.
     */
    public static boolean isMailDomain(String domainName) {
        try {
            Hashtable<String, String> env = new Hashtable<>();
            env.put("java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory");
            DirContext ictx = new InitialDirContext(env);
            Attributes attrs = ictx.getAttributes(domainName, new String[]{"MX"});
            Attribute attr = attrs.get("MX");
            return attr != null && attr.size() > 0;
        } catch (Throwable e) {
            // System.err.println(DNSUtils.class.getName() + ": Failed to resolve MX records for domain: " + domainName);
            if (!(e instanceof javax.naming.NameNotFoundException)) {
                System.err.println(DNSUtils.class.getName() + ":" + e.getClass().getName() + ": " + e.getMessage());
            //  e.printStackTrace();
            }
        }
        return false;
    }
}
