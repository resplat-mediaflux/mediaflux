package unimelb.utils;

public class AssetPathUtils {

    public static String getName(String path) {
        if ("/".equals(path)) {
            return null;
        }
        path = trimTrailingSlash(path);
        int idx = path.lastIndexOf('/');
        if (idx == -1) {
            return path;
        }
        return path.substring(idx + 1);
    }

    public static String trimTrailingSlash(String path) {
        if ("/".equals(path)) {
            return path;
        }
        while (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        return path;
    }

    public static String trimLeadingSlash(String path) {
        if ("/".equals(path)) {
            return path;
        }
        while (path.startsWith("/")) {
            path = path.substring(1);
        }
        return path;
    }

    public static String trimSlash(String path) {
        return trimTrailingSlash(trimLeadingSlash(path));
    }

    public static String getParent(String path) {
        if ("/".equals(path)) {
            return null;
        }
        path = trimTrailingSlash(path);
        int idx = path.lastIndexOf('/');
        if (idx < 0) {
            return null;
        } else if (idx == 0) {
            return "/";
        } else {
            return path.substring(0, idx);
        }
    }

    public static String join(String... paths) {
        if (paths == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < paths.length; i++) {
            String path = paths[i];
            if (i == 0) {
                sb.append(trimTrailingSlash(path));
            } else {
                sb.append("/");
                sb.append(trimSlash(path));
            }
        }
        return sb.toString();
    }

}
