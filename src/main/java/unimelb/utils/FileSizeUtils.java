package unimelb.utils;

import java.text.DecimalFormat;

public class FileSizeUtils {
    public static String getHumanReadableSize(long nBytes) {
        return getHumanReadableSize(nBytes, true);
    }

    public static String getHumanReadableSize(long nBytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (nBytes < unit) {
            return nBytes + " B";
        }
        int exp = (int) (Math.log(nBytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return new DecimalFormat("#.###").format(nBytes / Math.pow(unit, exp)) + " " + pre + "B";
    }
}
