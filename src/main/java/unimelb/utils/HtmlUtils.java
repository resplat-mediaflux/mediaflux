package unimelb.utils;

public class HtmlUtils {

    public static String removeUnsafeTags(String html) {
        // Define the regular expression pattern to match unsafe tags
        String unsafeTagsPattern = "<(script|iframe|object|embed)[^>]*>.*?</\\1>";

        // Remove the unsafe tags from the HTML string
        String safeHtml = html.replaceAll(unsafeTagsPattern, "");

        return safeHtml;
    }

}
