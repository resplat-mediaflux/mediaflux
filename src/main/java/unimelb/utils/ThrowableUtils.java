package unimelb.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ThrowableUtils {

	private static class SizeLimitedStringWriter extends StringWriter {

		private static class SizeLimitExceededException extends Exception {

			private static final long serialVersionUID = 5926193002813814961L;

		}

		private int maxSize;

		public SizeLimitedStringWriter(int maxSize) {
			super();
			this.maxSize = maxSize;
		}

		private void checkSizeLimit() {
			if (this.getBuffer().length() > this.maxSize) {
				throw new RuntimeException("Size limit exceeded.", new SizeLimitExceededException());
			}
		}

		public void write(int c) {
			super.write(c);
			checkSizeLimit();
		}

		public void write(char cbuf[], int off, int len) {
			super.write(cbuf, off, len);
			checkSizeLimit();
		}

		public void write(String str) {
			super.write(str);
			checkSizeLimit();
		}

		public void write(String str, int off, int len) {
			super.write(str, off, len);
			checkSizeLimit();
		}

	}

	/**
	 * Print (size limited) stack trace of the exception. This is to avoid cross
	 * referenced causes that could result in infinite loops (infinite sized
	 * string).
	 * 
	 * @param e         The exception
	 * @param sb        The output string builder.
	 * @param sizeLimit The max size.
	 */
	public static void printStackTrace(Throwable e, StringBuilder sb, int sizeLimit) {
		SizeLimitedStringWriter sw = new SizeLimitedStringWriter(sizeLimit);
		try (PrintWriter pw = new PrintWriter(sw)) {
			try {
				e.printStackTrace(pw);
			} catch (RuntimeException rex) {
				Throwable cause = rex.getCause();
				if (cause != null && (cause instanceof SizeLimitedStringWriter.SizeLimitExceededException)) {
					// do nothing, ignore the exception
				} else {
					throw rex;
				}
			}
			sb.append(sw.toString());
		}
	}

}
