package unimelb.utils;

import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONObject;

import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDocWriter;
import arc.xml.XmlWriter;

public class JsonUtils {

	public static XmlDoc.Element toXml(String jsonText) throws Throwable {
		if (jsonText == null) {
			return null;
		}
		jsonText = jsonText.trim();
		XmlDocMaker dm = new XmlDocMaker();
		XmlDocWriter w = new XmlDocWriter(dm);
		if (jsonText.startsWith("[")) {
			JSONArray ja = new JSONArray(jsonText);
			addJsonArray(w, null, ja, true);
		} else if (jsonText.startsWith("{")) {
			JSONObject jo = new JSONObject(jsonText);
			addJsonObject(w, null, jo);
		} else {
			throw new Exception("Failed to parse JSON: " + jsonText);
		}
		return dm.root();
	}

	public static void addJsonArray(XmlWriter w, String tag, JSONArray ja) throws Throwable {
		addJsonArray(w, tag, ja, true);
	}

	public static void addJsonArray(XmlWriter w, String tag, JSONArray ja, boolean self) throws Throwable {
		if (self) {
			w.push(tag == null ? "array" : tag, new String[] { "type", "array" });
		}
		for (Object o : ja) {
			if (o instanceof JSONArray) {
				addJsonArray(w, null, (JSONArray) o, true);
			} else if (o instanceof JSONObject) {
				addJsonObject(w, null, (JSONObject) o);
			} else {
				w.add("value", o.toString());
			}
		}
		if (self) {
			w.pop();
		}
	}

	public static void addJsonObject(XmlWriter w, String tag, JSONObject jo) throws Throwable {
		w.push(tag == null ? "object" : tag, new String[] { "type", "object" });
		Set<String> keys = new TreeSet<String>(jo.keySet());
		if (keys != null) {
			for (String key : keys) {
				Object o = jo.get(key);
				if (o instanceof JSONArray) {
					addJsonArray(w, key, (JSONArray) o, true);
				} else if (o instanceof JSONObject) {
					addJsonObject(w, key, (JSONObject) o);
				} else {
					w.add(key, o.toString());
				}
			}
		}
		w.pop();
	}

}
