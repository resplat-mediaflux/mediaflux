package unimelb.utils;

import java.util.Collection;

public class StringUtils {

    public static String join(Collection<String> values, String separator) {
        boolean first = true;
        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            if (first) {
                first = false;
            } else {
                sb.append(separator);
            }
            sb.append(value);
        }
        return sb.toString();
    }
    
}
