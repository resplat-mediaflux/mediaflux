package unimelb.rcs;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;

public class ResPlatStorageReportUtils {

    //@formatter:off
    public static void main(String[] args) throws Throwable {
//        String token = getAuthToken("https://115.146.84.124/get_auth_token/", "USERNAME", "PASSWD");
//        File csvFile = new File("/tmp/sample.csv");
//        sendCSVReport("https://115.146.84.124/ingest/upload/", token, csvFile);
    }
    //@formatter:on

    public static final String API_PATH_GET_AUTH_TOKEN = "/get_auth_token/";
    public static final String API_PATH_POST_DATA = "/ingest/upload/";

    private static String readString(InputStream in, String charsetName) throws Throwable {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(in, charsetName == null ? "UTF-8" : charsetName));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } finally {
            reader.close();
        }
        return sb.toString();
    }

    private static boolean _allowUntrustedSSLCerts = false;

    private static void allowUntrustedSSLCerts() throws Throwable {
        if (!_allowUntrustedSSLCerts) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            } };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            _allowUntrustedSSLCerts = true;
        }
    }

    public static String getAuthToken(String host, boolean ssl, String username, String password) throws Throwable {
        return getAuthToken(urlToGetAuthToken(ssl, host), username, password);
    }

    public static String getAuthToken(String url, String username, String password) throws Throwable {

        /*
         * all untrusted ssl certificates
         */
        allowUntrustedSSLCerts();

        /*
         * request content
         */
        JSONObject jo = new JSONObject();
        jo.put("username", username);
        jo.put("password", password);
        byte[] requestContent = jo.toString().getBytes();

        /*
         * connection
         */
        URL urlObj = URI.create(url).toURL();
        HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
        try {
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            /*
             * request headers
             */
            conn.setRequestProperty("Content-Length", String.valueOf(requestContent.length));
            conn.setRequestProperty("Content-Type", "application/json");

            /*
             * write to request output stream
             */
            try {
                conn.getOutputStream().write(requestContent);
            } finally {
                conn.getOutputStream().close();
            }

            /*
             * check response
             */
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // @formatter:off
                String msg = String.format("Failed to get auth token from %s. Unexpected HTTP response: %d (%s)",
                        url, conn.getResponseCode(), conn.getResponseMessage());
                String responseError = readString(conn.getErrorStream(), "UTF-8");
                System.out.println(msg + "\nHTTP " + conn.getResponseCode() + " Response: " + responseError);
               // @formatter:on
                throw new Exception(msg);
            }

            /*
             * read from response input stream
             */
            String responseContent = readString(conn.getInputStream(), "UTF-8");
            JSONObject responseJO = new JSONObject(responseContent);
            return responseJO.getString("token");
        } finally {
            conn.disconnect();
        }
    }

    public static void sendCSVReport(String host, boolean ssl, String username, String password, File file)
            throws Throwable {
        sendCSVReport(urlToPostData(ssl, host), getAuthToken(host, ssl, username, password), file);
    }

    public static void sendCSVReport(String host, boolean ssl, String token, File file) throws Throwable {
        sendCSVReport(urlToPostData(ssl, host), token, file);
    }

    public static void sendCSVReport(String url, String token, File file) throws Throwable {
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        try {
            sendCSVReport(url, token, file.getName(), in, file.length());
        } finally {
            in.close();
        }
    }

    public static void sendCSVReport(String host, boolean ssl, String username, String password, String fileName,
            String report) throws Throwable {
        sendCSVReport(urlToPostData(ssl, host), getAuthToken(host, ssl, username, password), fileName, report);
    }

    public static void sendCSVReport(String host, boolean ssl, String token, String fileName, String report)
            throws Throwable {
        sendCSVReport(urlToPostData(ssl, host), token, fileName, report);
    }

    public static void sendCSVReport(String url, String token, String fileName, String report) throws Throwable {
        byte[] b = report.getBytes();
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        try {
            sendCSVReport(url, token, fileName, in, b.length);
        } finally {
            in.close();
        }
    }

    public static void sendCSVReport(String host, boolean ssl, String username, String password, String fileName,
            InputStream in, long length) throws Throwable {
        sendCSVReport(urlToPostData(ssl, host), getAuthToken(host, ssl, username, password), fileName, in, length);
    }

    public static void sendCSVReport(String host, boolean ssl, String token, String fileName, InputStream in,
            long length) throws Throwable {
        sendCSVReport(urlToPostData(ssl, host), token, fileName, in, length);
    }

    public static void sendCSVReport(String url, String token, String fileName, InputStream in, long length)
            throws Throwable {
        
        allowUntrustedSSLCerts();
        
        URL urlObj = URI.create(url).toURL();
        HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
        try {
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            /*
             * request headers
             */
            conn.setRequestProperty("Authorization", "Token " + token);
            String boundary = String.valueOf(System.currentTimeMillis());
            conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            OutputStream requestOut = new BufferedOutputStream(conn.getOutputStream());
            try {
                /*
                 * form head
                 */
                StringBuilder sb = new StringBuilder();
                sb.append("--").append(boundary).append("\r\n");
                sb.append("Content-Disposition: form-data; name=\"data\"; filename=\"").append(fileName)
                        .append("\"\r\n");
                sb.append("Content-Type: text/csv\r\n");
                sb.append("\r\n");
                requestOut.write(sb.toString().getBytes());
                requestOut.flush();

                /*
                 * form content file
                 */
                byte[] buffer = new byte[8192];
                int bytesRead = -1;
                while ((bytesRead = in.read(buffer)) != -1) {
                    requestOut.write(buffer, 0, bytesRead);
                }
                requestOut.flush();
                requestOut.write("\r\n".getBytes());
                requestOut.flush();

                /*
                 * form tail
                 */
                requestOut.write(("--" + boundary + "--\r\n").getBytes());
            } finally {
                requestOut.close();
            }

            /*
             * check response
             */
            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                // @formatter:off
                 String msg = String.format("Failed to send report to %s via HTTP POST. Unexpected HTTP response: %d (%s)", url, conn.getResponseCode(),
                        conn.getResponseMessage());
                 String responseError = readString(conn.getErrorStream(), "UTF-8");
                 System.out.println(msg + "\nHTTP " + conn.getResponseCode() + " Response: " + responseError);
                // @formatter:on
                throw new Exception(msg);
            }
        } finally {
            conn.disconnect();
        }
    }

    public static String urlToGetAuthToken(boolean ssl, String host) throws Throwable {
        StringBuilder sb = new StringBuilder();
        sb.append(ssl ? "https" : "http");
        sb.append("://");
        sb.append(host.trim());
        sb.append(API_PATH_GET_AUTH_TOKEN);
        return sb.toString();
    }

    public static String urlToPostData(boolean ssl, String host) throws Throwable {
        StringBuilder sb = new StringBuilder();
        sb.append(ssl ? "https" : "http");
        sb.append("://");
        sb.append(host.trim());
        sb.append(API_PATH_POST_DATA);
        return sb.toString();
    }

}
