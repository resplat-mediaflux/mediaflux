package unimelb.rcs.data.registry;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import arc.xml.XmlDoc;
import arc.xml.XmlWriter;

public class Collection {

    public static enum Type {
        // @formatter:off
		project,
		graduate_research_student_project, 
		program_of_projects,
		collection_or_database, 
		facility_laboratory_or_research_service,
		internal;
		// @formatter:on

        public static Type fromString(String s) {
            if (s != null) {
                Type[] vs = values();
                for (Type v : vs) {
                    if (v.name().equalsIgnoreCase(s)) {
                        return v;
                    }
                }
            }
            return null;
        }
    }

    public static enum Status {
        active, decommissioned;

        public static Status fromString(String s) {
            if (s != null) {
                Status[] vs = values();
                for (Status v : vs) {
                    if (s.equalsIgnoreCase(v.name())) {
                        return v;
                    }
                }
            }
            return null;
        }
    }

    private String url;
    private List<Contact> contacts;
    private List<Product> products;
    private Date created;
    private String code;
    private String name;
    private String requestSource;
    private Collection.Status status;
    private String adminNotes;
    private String category;
    private String organisation;
    private String department;
    private Type type;
    private List<String> forCodes;
    private Date startDate;
    private Date endDate;
    private Date reviewYear;
    private Date reviewYearCustom;
    private String fundingSources;
    private String ethicsIds;
    private String retentionClassification;
    private String retentionNotes;
    private String additionalInformation;
    private boolean addObEthics;
    private boolean addObConsent;
    private boolean addObConfidential;
    private boolean addObPrivate;
    private boolean addObHarm;
    private boolean addObClinical;
    private boolean addObSensitive;
    private boolean addObIndigenous;
    private boolean addObContract;
    private boolean addObLicensing;
    private boolean addObCodes;
    private boolean addObLegal;

    public Collection(JSONObject jso) throws Throwable {
        this.url = jso.getString("url");
        if (jso.has("contacts")) {
            this.contacts = new ArrayList<Contact>();
            JSONArray contactsJSA = jso.getJSONArray("contacts");
            for (int i = 0; i < contactsJSA.length(); i++) {
                JSONObject contactJSO = contactsJSA.getJSONObject(i);
                if (contactJSO.has("user") && !contactJSO.isNull("user")) {
                    this.contacts.add(new Contact(contactJSO));
                }
            }
        }
        if (jso.has("products")) {
            this.products = new ArrayList<Product>();
            JSONArray productsJSA = jso.getJSONArray("products");
            for (int i = 0; i < productsJSA.length(); i++) {
                JSONObject productJSO = productsJSA.getJSONObject(i);
                this.products.add(new Product(productJSO));
            }
        }
        this.created = new SimpleDateFormat("yyyy-MM-dd").parse(jso.getString("created"));
        this.code = jso.getString("code");
        this.name = jso.getString("name");
        this.requestSource = jso.optString("request_source", null);
        this.status = Collection.Status.fromString(jso.getString("status"));
        this.adminNotes = jso.optString("admin_notes", null);
        this.category = jso.optString("category");
        this.organisation = jso.optString("organisation");
        this.department = jso.optString("department");
        this.type = Type.fromString(jso.optString("type", null));
        this.forCodes = new ArrayList<>(4);
        String forCode1 = jso.optString("for_code_1", "");
        if (!forCode1.isEmpty()) {
            this.forCodes.add(forCode1);
        }
        String forCode2 = jso.optString("for_code_2", "");
        if (!forCode2.isEmpty()) {
            this.forCodes.add(forCode2);
        }
        String forCode3 = jso.optString("for_code_3", "");
        if (!forCode3.isEmpty()) {
            this.forCodes.add(forCode3);
        }
        String forCode4 = jso.optString("for_code_4", "");
        if (!forCode4.isEmpty()) {
            this.forCodes.add(forCode4);
        }
        if (jso.has("start_date") && !jso.isNull("start_date")) {
            this.startDate = new SimpleDateFormat("yyyy").parse(Integer.toString(jso.getInt("start_date")));
        }
        if (jso.has("end_date") && !jso.isNull("end_date")) {
            this.endDate = new SimpleDateFormat("yyyy").parse(Integer.toString(jso.getInt("end_date")));
        }
        if (jso.has("review_year") && !jso.isNull("review_year")) {
            this.reviewYear = new SimpleDateFormat("yyyy").parse(Integer.toString(jso.getInt("review_year")));
        }
        if (jso.has("review_year_custom") && !jso.isNull("review_year_custom")) {
            this.reviewYearCustom = new SimpleDateFormat("yyyy")
                    .parse(Integer.toString(jso.getInt("review_year_custom")));
        }
        if (jso.has("funding_sources")) {
            String fundingSources = jso.getString("funding_sources");
            if (!fundingSources.isEmpty()) {
                this.fundingSources = fundingSources;
            }
        }
        if (jso.has("ethics_ids")) {
            String ethicsIds = jso.getString("ethics_ids");
            if (!ethicsIds.isEmpty()) {
                this.ethicsIds = ethicsIds;
            }
        }
        if (jso.has("retention_classification")) {
            String retentionClassification = jso.optString("retention_classification", null);
            if (retentionClassification != null && !retentionClassification.isEmpty()) {
                this.retentionClassification = retentionClassification;
            }
        }
        if (jso.has("retention_notes")) {
            String retentionNotes = jso.getString("retention_notes");
            if (!retentionNotes.isEmpty()) {
                this.retentionNotes = retentionNotes;
            }
        }
        if (jso.has("additional_information")) {
            String additionalInformation = jso.getString("additional_information");
            if (!additionalInformation.isEmpty()) {
                this.additionalInformation = additionalInformation;
            }
        }
        this.addObEthics = jso.optBoolean("add_ob_ethics", false);
        this.addObConsent = jso.optBoolean("add_ob_consent", false);
        this.addObConfidential = jso.optBoolean("add_ob_confidential", false);
        this.addObPrivate = jso.optBoolean("add_ob_private", false);
        this.addObHarm = jso.optBoolean("add_ob_harm", false);
        this.addObClinical = jso.optBoolean("add_ob_clinical", false);
        this.addObSensitive = jso.optBoolean("add_ob_sensitive", false);
        this.addObIndigenous = jso.optBoolean("add_ob_indigenous", false);
        this.addObContract = jso.optBoolean("add_ob_contract", false);
        this.addObLicensing = jso.optBoolean("add_ob_licensing", false);
        this.addObCodes = jso.optBoolean("add_ob_codes", false);
        this.addObLegal = jso.optBoolean("add_ob_legal", false);
    }

    public final String url() {
        return this.url;
    }

    public List<Contact> contacts() {
        return this.contacts;
    }

    public List<Product> products() {
        return this.products;
    }

    public Product product(StorageProduct storageProduct) {
        if (this.products != null) {
            for (Product p : this.products) {
                if (p.storageProduct().equals(storageProduct)) {
                    return p;
                }
            }
        }
        return null;
    }

    public boolean useStorageProduct(StorageProduct storageProduct) {
        return product(storageProduct) != null;
    }

    public final Date created() {
        return this.created;
    }

    public final String code() {
        return this.code;
    }

    public final String name() {
        return this.name;
    }

    public final String requestSource() {
        return this.requestSource;
    }

    public final Collection.Status status() {
        return this.status;
    }

    public final String adminNotes() {
        return this.adminNotes;
    }

    public final String category() {
        return this.category;
    }

    public final String organisation() {
        return this.organisation;
    }

    public final String department() {
        return this.department;
    }

    public final Type type() {
        return this.type;
    }

    public final List<String> forCodes() {
        return this.forCodes;
    }

    public final Date startDate() {
        return this.startDate;
    }

    public final Date endDate() {
        return this.endDate;
    }

    public final Date reviewYear() {
        return this.reviewYear;
    }

    public final Date reviewYearCustom() {
        return this.reviewYearCustom;
    }

    public final String fundingSources() {
        return this.fundingSources;
    }

    public final String ethicsIds() {
        return this.ethicsIds;
    }

    public final String retentionClassification() {
        return this.retentionClassification;
    }

    public final String retentionNotes() {
        return this.retentionNotes;
    }

    public final String additionalInformation() {
        return this.additionalInformation;
    }

    public final boolean addObEthics() {
        return this.addObEthics;
    }

    public final boolean addObConsent() {
        return this.addObConsent;
    }

    public final boolean addObConfidential() {
        return this.addObConfidential;
    }

    public final boolean addObPrivate() {
        return this.addObPrivate;
    }

    public final boolean addObHarm() {
        return this.addObHarm;
    }

    public final boolean addObClinical() {
        return this.addObClinical;
    }

    public final boolean addObSensitive() {
        return this.addObSensitive;
    }

    public final boolean addObIndigenous() {
        return this.addObIndigenous;
    }

    public final boolean addObContract() {
        return this.addObContract;
    }

    public final boolean addObLicensing() {
        return this.addObLicensing;
    }

    public final boolean addObCodes() {
        return this.addObCodes;
    }

    public final boolean addObLegal() {
        return this.addObLegal;
    }

    public CollectionRef getRef() {
        return new CollectionRef(this);
    }

    public JSONObject toJSONObject() {
        JSONObject jso = new JSONObject();
        jso.put("url", this.url);
        if (this.contacts != null && !this.contacts.isEmpty()) {
            JSONArray contactsJSA = new JSONArray();
            for (Contact contact : this.contacts) {
                contactsJSA.put(contact.toJSONObject());
            }
            jso.put("contacts", contactsJSA);
        }
        if (this.products != null && !this.products.isEmpty()) {
            JSONArray productsJSA = new JSONArray();
            for (Product product : this.products) {
                productsJSA.put(product.toJSONObject());
            }
            jso.put("products", productsJSA);
        }
        jso.put("created", new SimpleDateFormat("yyyy-MM-dd").format(this.created));
        jso.put("code", this.code);
        jso.put("name", this.name);
        if (this.requestSource != null && !this.requestSource.isEmpty()) {
            jso.put("request_source", this.requestSource);
        }
        jso.put("status", this.status.name());
        if (this.adminNotes != null && !this.adminNotes.isEmpty()) {
            jso.put("admin_notes", this.adminNotes);
        }
        if (this.category != null && !this.category.isEmpty()) {
            jso.put("category", this.category);
        }
        if (this.organisation != null && !this.organisation.isEmpty()) {
            jso.put("organisation", this.organisation);
        }
        if (this.department != null && !this.department.isEmpty()) {
            jso.put("department", this.department);
        }
        if (this.type != null) {
            jso.put("type", this.type.name());
        }
        if (this.forCodes != null && !this.forCodes.isEmpty()) {
            for (int i = 0; i < this.forCodes.size(); i++) {
                String fc = this.forCodes.get(i);
                jso.put("for_code_" + (i + 1), fc);
            }
        }
        if (this.startDate != null) {
            jso.put("start_date", new SimpleDateFormat("yyyy").format(this.startDate));
        }
        if (this.endDate != null) {
            jso.put("end_date", new SimpleDateFormat("yyyy").format(this.endDate));
        }
        if (this.reviewYear != null) {
            jso.put("review_year", new SimpleDateFormat("yyyy").format(this.reviewYear));
        }
        if (this.reviewYearCustom != null) {
            jso.put("review_year_custom", new SimpleDateFormat("yyyy").format(this.reviewYearCustom));
        }
        if (this.fundingSources != null && !this.fundingSources.isEmpty()) {
            jso.put("funding_sources", this.fundingSources);
        }
        if (this.ethicsIds != null && !this.ethicsIds.isEmpty()) {
            jso.put("ethics_ids", this.ethicsIds);
        }
        if (this.retentionClassification != null && !this.retentionClassification.isEmpty()) {
            jso.put("retention_classification", this.retentionClassification);
        }
        if (this.retentionNotes != null && !this.retentionNotes.isEmpty()) {
            jso.put("retention_notes", this.retentionNotes);
        }
        if (this.additionalInformation != null && !this.additionalInformation.isEmpty()) {
            jso.put("additional_information", this.additionalInformation);
        }
        jso.put("add_ob_ethics", this.addObEthics);
        jso.put("add_ob_consent", this.addObConsent);
        jso.put("add_ob_confidential", this.addObConfidential);
        jso.put("add_ob_private", this.addObPrivate);
        jso.put("add_ob_harm", this.addObHarm);
        jso.put("add_ob_clinical", this.addObClinical);
        jso.put("add_ob_sensitive", this.addObSensitive);
        jso.put("add_ob_indigenous", this.addObIndigenous);
        jso.put("add_ob_contract", this.addObContract);
        jso.put("add_ob_licensing", this.addObLicensing);
        jso.put("add_ob_codes", this.addObCodes);
        jso.put("add_ob_legal", this.addObLegal);
        return jso;
    }

    public void saveXml(XmlWriter w) throws Throwable {
        saveXml(w, null);
    }

    public void saveXml(XmlWriter w, XmlDoc.Element projects) throws Throwable {
        w.push("collection");
        w.add("url", this.url);
        if (this.contacts != null && !this.contacts.isEmpty()) {
            w.push("contacts");
            for (Contact contact : this.contacts) {
                contact.saveXml(w);
            }
            w.pop();
        }
        if (this.products != null && !this.products.isEmpty()) {
            w.push("products");
            for (Product product : this.products) {
                product.saveXml(w);
            }
            w.pop();
        }
        w.add("created", new SimpleDateFormat("yyyy-MM-dd").format(this.created));
        w.add("code", this.code);
        w.add("name", this.name);
        if (this.requestSource != null) {
            w.add("request_source", this.requestSource);
        }
        w.add("status", this.status.name());
        if (this.adminNotes != null) {
            w.add("admin_notes", this.adminNotes);
        }
        if (this.category != null) {
            w.add("category", this.category);
        }
        if (this.organisation != null) {
            w.add("organisation", this.organisation);
        }
        if (this.department != null) {
            w.add("department", this.department);
        }
        if (this.type != null) {
            w.add("type", this.type);
        }
        if (this.forCodes != null && !this.forCodes.isEmpty()) {
            for (int i = 0; i < this.forCodes.size(); i++) {
                w.add("for_code_" + (i + 1), this.forCodes.get(i));
            }
        }
        if (this.startDate != null) {
            w.add("start_date", new SimpleDateFormat("yyyy").format(this.startDate));
        }
        if (this.endDate != null) {
            w.add("end_date", new SimpleDateFormat("yyyy").format(this.endDate));
        }
        if (this.reviewYear != null) {
            w.add("review_year", new SimpleDateFormat("yyyy").format(this.reviewYear));
        }
        if (this.reviewYearCustom != null) {
            w.add("review_year_custom", new SimpleDateFormat("yyyy").format(this.reviewYearCustom));
        }
        if (this.fundingSources != null) {
            w.add("funding_sources", this.fundingSources);
        }
        if (this.ethicsIds != null) {
            w.add("ethics_ids", this.ethicsIds);
        }
        if (this.retentionClassification != null) {
            w.add("retention_classification", this.retentionClassification);
        }
        if (this.retentionNotes != null) {
            w.add("retention_notes", this.retentionNotes);
        }
        if (this.additionalInformation != null) {
            w.add("additional_information", this.additionalInformation);
        }
        w.add("add_ob_ethics", this.addObEthics);
        w.add("add_ob_consent", this.addObConsent);
        w.add("add_ob_confidential", this.addObConfidential);
        w.add("add_ob_private", this.addObPrivate);
        w.add("add_ob_harm", this.addObHarm);
        w.add("add_ob_clinical", this.addObClinical);
        w.add("add_ob_sensitive", this.addObSensitive);
        w.add("add_ob_indigenous", this.addObIndigenous);
        w.add("add_ob_contract", this.addObContract);
        w.add("add_ob_licensing", this.addObLicensing);
        w.add("add_ob_codes", this.addObCodes);
        w.add("add_ob_legal", this.addObLegal);
        if (projects != null) {
            w.add(projects);
        }
        w.pop();
    }

}
