package unimelb.rcs.data.registry;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class DataRegistry {

	public static final String APP_NAME = "project-provisioning";

	public static final String PROPERTY_DATA_REG_HOST = "data.registry.host";

	public static final String SECURE_WALLET_KEY_DATA_REG_TOKEN = "data.registry.token";

	private String _hostUrl;
	private String _token;

	public DataRegistry(String hostUrl, String token) {
		_hostUrl = hostUrl;
		_token = token;
	}

	public List<CollectionRef> listCollections() throws Throwable {
		URL urlObj = URI.create(_hostUrl + "/api/collections/").toURL();
		HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
		try {
			conn.setRequestMethod("GET");
			conn.setDoOutput(false);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			/*
			 * request headers
			 */
			conn.setRequestProperty("Authorization", "Token " + _token);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");

			int responseCode = conn.getResponseCode();

			if (responseCode != HttpURLConnection.HTTP_OK && responseCode != HttpURLConnection.HTTP_ACCEPTED) {
				StringBuilder sb = new StringBuilder("Unexpected HTTP response: ");
				sb.append(responseCode).append("\n");
				sb.append(readAll(conn.getErrorStream(), "UTF-8"));
				throw new Exception(sb.toString());
			}
			JSONArray jsa = new JSONArray(readAll(conn.getInputStream()));
			if (jsa != null && jsa.length() > 0) {
				List<CollectionRef> collections = new ArrayList<>(jsa.length());
				for (int i = 0; i < jsa.length(); i++) {
					JSONObject jso = jsa.getJSONObject(i);
					collections.add(new CollectionRef(jso));
				}
				return collections;
			}
			return null;
		} finally {
			conn.disconnect();
		}
	}

	public Collection getCollection(String code) throws Throwable {
		URL urlObj = URI.create(_hostUrl + "/api/collections/" + code + "/").toURL();
		HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
		try {
			conn.setRequestMethod("GET");
			conn.setDoOutput(false);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			/*
			 * request headers
			 */
			conn.setRequestProperty("Authorization", "Token " + _token);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");

			int responseCode = conn.getResponseCode();

			if (responseCode != HttpURLConnection.HTTP_OK && responseCode != HttpURLConnection.HTTP_ACCEPTED) {
				StringBuilder sb = new StringBuilder("Unexpected HTTP response: ");
				sb.append(responseCode).append("\n");
				sb.append(readAll(conn.getErrorStream(), "UTF-8"));
				throw new Exception(sb.toString());
			}
			JSONObject jso = new JSONObject(readAll(conn.getInputStream()));
			if (jso.has("detail") && !jso.isNull("detail")
					&& "Not found.".equalsIgnoreCase(jso.optString("detail", null))) {
				return null;
			}
			if (!jso.has("url") || !jso.has("code")) {
				return null;
			}
			return new Collection(jso);
		} finally {
			conn.disconnect();
		}
	}

	private static String readAll(InputStream in) throws Throwable {
		return readAll(in, "UTF-8");
	}

	private static String readAll(InputStream in, String charsetName) throws Throwable {
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(in, charsetName == null ? "UTF-8" : charsetName));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} finally {
			reader.close();
		}
		return sb.toString();
	}

	public static DataRegistry get(ServiceExecutor executor) throws Throwable {
		// list application properties
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("app", APP_NAME);
		XmlDoc.Element properties = executor.execute("application.property.list", dm.root());

		// get data.registry.host
		String dataRegHost = properties.value("property[@name='" + PROPERTY_DATA_REG_HOST + "']");
		if (dataRegHost == null) {
			throw new Exception("Application property: " + PROPERTY_DATA_REG_HOST + " not set");
		}

		String dataRegToken = getTokenFromSecureWallet(executor);
		if (dataRegToken == null) {
			throw new Exception(
					"Could not retreive token from secure wallet. " + SECURE_WALLET_KEY_DATA_REG_TOKEN + " not set.");
		}
		return new DataRegistry(dataRegHost, dataRegToken);
	}

	public static String getTokenFromSecureWallet(ServiceExecutor executor) throws Throwable {
		boolean exists = executor.execute("secure.wallet.contains",
				"<args><key>" + SECURE_WALLET_KEY_DATA_REG_TOKEN + "</key></args>", null, null)
				.booleanValue("exists", false);
		if (exists) {
			return executor.execute("secure.wallet.get",
					"<args><key>" + SECURE_WALLET_KEY_DATA_REG_TOKEN + "</key></args>", null, null).value("value");
		}
		return null;
	}

}
