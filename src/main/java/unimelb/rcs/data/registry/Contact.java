package unimelb.rcs.data.registry;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import arc.xml.XmlWriter;

public class Contact {

	public static enum Role {
		custodian, technical_support, graduate_research_student;

		public static Role fromString(String s) {
			if (s != null) {
				Role[] vs = values();
				for (Role v : vs) {
					if (s.equalsIgnoreCase(v.name())) {
						return v;
					}
				}
			}
			return null;
		}
	}

	private String username;
	private String email;
	private String firstName;
	private String lastName;
	private List<String> departments;
	private Role role;
	private boolean isCurrent;

	public Contact(JSONObject jso) {
		JSONObject user = jso.getJSONObject("user");
		this.username = user.getString("username");
		this.email = user.getString("email");
		this.firstName = user.getString("first_name");
		this.lastName = user.getString("last_name");
		this.departments = new ArrayList<>(3);
		if (user.has("extended")) {
			JSONObject extended = user.optJSONObject("extended");
			if (extended != null) {
				this.departments.add(extended.getString("department_1"));
				this.departments.add(extended.getString("department_2"));
				this.departments.add(extended.getString("department_3"));
			}
		}
		this.role = Role.fromString(jso.getString("role"));
		this.isCurrent = jso.getBoolean("is_current");
	}

	public final String username() {
		return this.username;
	}

	public final String email() {
		return this.email;
	}

	public final String firstName() {
		return this.firstName;
	}

	public final String lastName() {
		return this.lastName;
	}

	public final List<String> departments() {
		return this.departments;
	}

	public final Role role() {
		return this.role;
	}

	public final boolean isCurrent() {
		return this.isCurrent;
	}

	public JSONObject toJSONObject() {
		JSONObject user = new JSONObject();
		user.put("username", this.username);
		user.put("email", this.email);
		user.put("first_name", this.firstName);
		user.put("last_name", this.lastName);
		if (!this.departments.isEmpty()) {
			JSONObject extended = new JSONObject();
			extended.put("department_1", this.departments.get(0));
			if (this.departments.size() > 1) {
				extended.put("department_2", this.departments.get(1));
			}
			if (this.departments.size() > 2) {
				extended.put("department_3", this.departments.get(2));
			}
			user.put("extended", extended);
		}

		JSONObject jso = new JSONObject();
		jso.put("user", user);
		jso.put("role", this.role);
		jso.put("is_current", this.isCurrent);
		return jso;
	}

	public void saveXml(XmlWriter w) throws Throwable {
		w.push("contact");

		w.push("user");
		w.add("username", this.username);
		w.add("email", this.email);
		w.add("first_name", this.firstName);
		w.add("last_name", this.lastName);
		w.push("extended");
		if (!this.departments.isEmpty()) {
			for (int i = 0; i < this.departments.size(); i++) {
				String department = this.departments.get(i);
				if (department != null && !department.isEmpty()) {
					w.add("department_" + (i + 1), department);
				}
			}
		}
		w.pop();
		w.pop();

		w.add("role", new String[] { "null", this.role == null ? "true" : null },
				this.role == null ? "none" : this.role.name());

		w.add("is_current", this.isCurrent);
		w.pop();
	}

}
