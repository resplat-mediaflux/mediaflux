package unimelb.rcs.data.registry;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;

/**
 *
 *
 */
public class ProjectTransition {

    public static final String DICT_PROJECT_COLLECTION_MAP = "ds-admin:project-collection-map";
    public static final String DICT_PROJECT_NAMESPACE_MAP = "ds-admin:project-namespace-map";

    public static class Project {
        public final String id;
        public final String path;
        public final boolean isCollectionAsset;

        public Project(String id, String path, boolean isCollectionAsset) {
            this.id = id;
            this.path = path;
            this.isCollectionAsset = isCollectionAsset;
        }
    }

    public static Map<String, Project> getAllProjects(ServiceExecutor executor) throws Throwable {
        return getProjects(executor, true, true);
    }

    public static Map<String, Project> getCollectionAssetProjects(ServiceExecutor executor) throws Throwable {
        return getProjects(executor, true, false);
    }

    public static Map<String, Project> getAssetNameProjects(ServiceExecutor executor) throws Throwable {
        return getProjects(executor, false, true);
    }

    public static Map<String, Project> getProjects(ServiceExecutor executor, boolean includeCollectionAssetProjects,
            boolean includeAssetNameProjects) throws Throwable {
        Map<String, Project> projects = new LinkedHashMap<>();
        if (includeCollectionAssetProjects) {
            if (executor.execute("dictionary.exists", "<args><name>" + DICT_PROJECT_COLLECTION_MAP + "</name></args>",
                    null, null).booleanValue("exists")) {
                List<XmlDoc.Element> es = executor.execute("dictionary.entries.describe", "<args><dictionary>"
                        + DICT_PROJECT_COLLECTION_MAP + "</dictionary><size>infinity</size></args>", null, null)
                        .elements("entry");
                if (es != null) {
                    for (XmlDoc.Element e : es) {
                        String id = e.value("term");
                        String path = e.value("definition");
                        Project project = new Project(id, path, true);
                        projects.put(id, project);
                    }
                }
            }
        }
        if (includeAssetNameProjects) {
            if (executor.execute("dictionary.exists", "<args><name>" + DICT_PROJECT_NAMESPACE_MAP + "</name></args>",
                    null, null).booleanValue("exists")) {
                List<XmlDoc.Element> es = executor.execute("dictionary.entries.describe",
                        "<args><dictionary>" + DICT_PROJECT_NAMESPACE_MAP + "</dictionary><size>infinity</size></args>",
                        null, null).elements("entry");
                if (es != null) {
                    for (XmlDoc.Element e : es) {
                        String id = e.value("term");
                        String path = e.value("definition");
                        Project project = new Project(id, path, false);
                        projects.put(id, project);
                    }
                }
            }
        }
        return projects;
    }

}
