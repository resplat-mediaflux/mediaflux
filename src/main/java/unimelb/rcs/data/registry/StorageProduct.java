package unimelb.rcs.data.registry;

public enum StorageProduct {

	//@formatter:off
		MEDIAFLUX("Market.Melbourne.Mediaflux", "Mediaflux"), 
		CEPH_S3("Ceph.S3","S3 Object Storage"), 
		DARIS("DaRIS", "DaRIS"),
		NETAPP_NFS("NetApp.NFS", "Research NFS"),
		NETAPP_CIFS("NetApp.CIFS", "Research CIFS"),
		ATTICA("Mediaflux.Attica", "Attica");
		//@formatter:on

	public final String typeName;
	public final String verboseName;

	public String toString() {
		return this.typeName;
	}

	StorageProduct(String typeName, String verboseName) {
		this.typeName = typeName;
		this.verboseName = verboseName;
	}

	public static String[] typeNames() {
		StorageProduct[] vs = values();
		String[] tns = new String[vs.length];
		for (int i = 0; i < vs.length; i++) {
			tns[i] = vs[i].typeName;
		}
		return tns;
	}
	
	public static String[] typeNames(String contains) {
		StorageProduct[] vs = values();
		String[] tns = new String[vs.length];
		for (int i = 0; i < vs.length; i++) {
			if (vs[i].typeName.contains(contains)) {
				tns[i] = vs[i].typeName;
			}
		}
		return tns;
	}


	public static String[] verboseNames() {
		StorageProduct[] vs = values();
		String[] vns = new String[vs.length];
		for (int i = 0; i < vs.length; i++) {
			vns[i] = vs[i].verboseName;
		}
		return vns;
	}

	public static StorageProduct fromTypeName(String typeName) {
		if (typeName != null) {
			StorageProduct[] vs = values();
			for (StorageProduct v : vs) {
				if (v.typeName.equalsIgnoreCase(typeName)) {
					return v;
				}
			}
		}
		return null;
	}

	public static StorageProduct fromVerboseName(String verboseName) {
		if (verboseName != null) {
			StorageProduct[] vs = values();
			for (StorageProduct v : vs) {
				if (v.verboseName.equalsIgnoreCase(verboseName)) {
					return v;
				}
			}
		}
		return null;
	}

}
