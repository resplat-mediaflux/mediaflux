package unimelb.rcs.data.registry;

import org.json.JSONObject;

import arc.xml.XmlWriter;

public class Product {

	public static enum Status {
		requested, approved, provisioned;

		public static Status fromString(String s) {
			if (s != null) {
				Status[] vs = values();
				for (Status v : vs) {
					if (s.equalsIgnoreCase(v.name())) {
						return v;
					}
				}
			}
			return null;
		}
	}

	private long id;
	private Status status;
	private StorageProduct storageProduct;
	private String path;
	private long quotaGB;
	private long quotaRequestedGB;
	private double usedGB;

	public Product(JSONObject jso) {
		this.id = jso.getLong("id");
		this.status = Status.fromString(jso.getString("status"));
		this.storageProduct = StorageProduct.fromTypeName(jso.getString("storage_product"));
		this.path = jso.optString("path", null);
		this.quotaGB = jso.optLong("quota", 0);
		this.quotaRequestedGB = jso.optLong("quota_requested", 0);
		this.usedGB = jso.optDouble("used_gb", 0.0);
	}

	public final long id() {
		return this.id;
	}

	public final Status status() {
		return this.status;
	}

	public final StorageProduct storageProduct() {
		return this.storageProduct;
	}

	public final String path() {
		return this.path;
	}

	public final long quota() {
		return this.quotaGB;
	}

	public final long quotaRequested() {
		return this.quotaRequestedGB;
	}

	public final double usedGB() {
		return this.usedGB;
	}

	public void saveXml(XmlWriter w) throws Throwable {
		w.push("product");
		w.add("id", this.id);
		if (this.status != null) {
			w.add("status", this.status.name());
		}
		if (this.storageProduct != null) {
			w.add("storage_product", this.storageProduct.typeName);
		}
		if (this.path != null && !this.path.isEmpty()) {
			w.add("path", this.path);
		}
		w.add("quota", this.quotaGB);
		w.add("quota_requested", this.quotaRequestedGB);
		w.add("used_gb", this.usedGB);
		w.pop();
	}

	public JSONObject toJSONObject() {
		JSONObject jso = new JSONObject();
		jso.put("id", this.id);
		jso.put("status", this.status.name());
		jso.put("storage_product", this.storageProduct.typeName);
		if (this.path != null) {
			jso.put("path", this.path);
		}
		jso.put("quota", this.quotaGB);
		jso.put("quota_requested", this.quotaRequestedGB);
		jso.put("used_gb", this.usedGB);
		return jso;
	}

}
