package unimelb.rcs.data.registry;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

import arc.xml.XmlDoc;
import arc.xml.XmlWriter;

public class CollectionRef {
    public final String url;
    public final Date created;
    public final String code;
    public final String name;
    public final Collection.Type type;
    public final Collection.Status status;
    public final String requestSource;

    public CollectionRef(JSONObject jso) throws Throwable {
        this.url = jso.getString("url");
        this.created = new SimpleDateFormat("yyyy-MM-dd").parse(jso.getString("created"));
        this.code = jso.getString("code");
        this.name = jso.getString("name");
        this.type = Collection.Type.fromString(jso.optString("type", null));
        this.status = Collection.Status.fromString(jso.getString("status"));
        this.requestSource = jso.optString("request_source", null);
    }

    public CollectionRef(Collection c) {
        this.url = c.url();
        this.created = c.created();
        this.code = c.code();
        this.name = c.name();
        this.type = c.type();
        this.status = c.status();
        this.requestSource = c.requestSource();
    }

    public boolean createdBefore(Date date, boolean inclusive) {
        if (inclusive) {
            return this.created.before(date) || this.created.equals(date);
        } else {
            return this.created.before(date);
        }
    }

    public boolean createdAfter(Date date, boolean inclusive) {
        if (inclusive) {
            return this.created.after(date) || this.created.equals(date);
        } else {
            return this.created.after(date);
        }
    }

    public boolean matches(Collection.Status status, Collection.Type type, Date after, boolean afterInclusive,
            Date before, boolean beforeInclusive, String code, String name, String requestSource) {
        if (status != null && !status.equals(this.status)) {
            return false;
        }
        if (type != null && !type.equals(this.type)) {
            return false;
        }
        if (after != null && !this.createdAfter(after, afterInclusive)) {
            return false;
        }
        if (before != null && !this.createdBefore(before, afterInclusive)) {
            return false;
        }
        if (code != null && (this.code == null || !this.code.toLowerCase().contains(code.toLowerCase()))) {
            return false;
        }
        if (name != null && (this.name == null || !this.name.toLowerCase().contains(name.toLowerCase()))) {
            return false;
        }
        if (requestSource != null && (this.requestSource == null
                || !this.requestSource.toLowerCase().contains(requestSource.toLowerCase()))) {
            return false;
        }
        return true;
    }

    public void saveXml(XmlWriter w) throws Throwable {
        saveXml(w, null);
    }

    public void saveXml(XmlWriter w, XmlDoc.Element projects) throws Throwable {
        w.push("collection");
        w.add("code", this.code);
        w.add("name", this.name);
        w.add("created", new SimpleDateFormat("yyyy-MM-dd").format(this.created));
        if (this.type != null) {
            w.add("type", this.type.name());
        }
        w.add("status", this.status.toString());
        if (this.requestSource != null) {
            w.add("request_source", this.requestSource);
        }
        if (projects != null) {
            w.add(projects);
        }
        w.pop();
    }

}
