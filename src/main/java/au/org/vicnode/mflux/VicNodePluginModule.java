/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.ConfigurationResolver;
import arc.mf.plugin.PluginService;
import au.org.vicnode.mflux.services.*;
import au.org.vicnode.mflux.services.admin.SvcAdminRoleNameSpacesMigrate;
import au.org.vicnode.mflux.services.admin.SvcAdminRoleNameSpacesMigrateCleanup;
import au.org.vicnode.mflux.services.contentcopy.SvcProjectContentCopyList;
import au.org.vicnode.mflux.services.contentcopy.SvcProjectContentCopyQueueRebuild;
import au.org.vicnode.mflux.services.contentcopy.SvcProjectContentCopyQueueRecreate;
import au.org.vicnode.mflux.services.contentcopy.SvcProjectContentCopyQueueReset;
import au.org.vicnode.mflux.services.contentcopy.SvcProjectContentCopyQueueSet;
import au.org.vicnode.mflux.services.contentcopy.SvcProjectContentCopyQueueUnset;
import au.org.vicnode.mflux.services.datamover.SvcProjectDMEndUserExpiryNotification;
import au.org.vicnode.mflux.services.datamover.SvcProjectDMExpiredDRFindAndMove;
import au.org.vicnode.mflux.services.datamover.SvcProjectDMManifestAssetFind;
import au.org.vicnode.mflux.services.datamover.SvcProjectDMOperatorExpiryList;
import au.org.vicnode.mflux.services.datamover.SvcProjectDMOperatorExpiryNotification;
import au.org.vicnode.mflux.services.datamover.SvcProjectDMUploadCount;
import au.org.vicnode.mflux.services.datamover.SvcProjectShareableDestinationSetFromXML;
import au.org.vicnode.mflux.services.datamover.SvcProjectShareableFind;
import au.org.vicnode.mflux.services.dataregistry.SvcDataRegistryCollectionDescribe;
import au.org.vicnode.mflux.services.dataregistry.SvcDataRegistryCollectionList;
import au.org.vicnode.mflux.services.dataregistry.SvcDataRegistryMediafluxCollectionDescribe;
import au.org.vicnode.mflux.services.dataregistry.SvcDataRegistryMediafluxCollectionList;
import au.org.vicnode.mflux.services.dataregistry.SvcDataRegistryMediafluxCollectionProjectExport;
import au.org.vicnode.mflux.services.organisation.SvcOrganisationAdd;
import au.org.vicnode.mflux.services.organisation.SvcOrganisationList;
import au.org.vicnode.mflux.services.organisation.SvcOrganisationRemove;
import au.org.vicnode.mflux.services.organisation.SvcOrganisationRename;
import au.org.vicnode.mflux.services.replication.SvcProjectReplicateQueueCleanupMissingContent;
import au.org.vicnode.mflux.services.replication.SvcProjectReplicateQueueGenerateCleanupScript;
import au.org.vicnode.mflux.services.replication.SvcProjectReplicateQueueList;
import au.org.vicnode.mflux.services.replication.SvcProjectReplicateQueueRebuild;
import au.org.vicnode.mflux.services.replication.SvcProjectReplicateQueueRecreate;
import au.org.vicnode.mflux.services.replication.SvcProjectReplicateQueueReset;
import au.org.vicnode.mflux.services.replication.SvcProjectReplicateQueueSet;
import au.org.vicnode.mflux.services.replication.SvcProjectReplicateQueueUnset;
import au.org.vicnode.mflux.services.replication.SvcProjectReplicationCheck;

public class VicNodePluginModule implements arc.mf.plugin.PluginModule {

    private Collection<PluginService> _services = null;

    @Override
    public String description() {

        return "UniMelb Provisioning Mediaflux Plugin Module.";
    }

    @Override
    public void initialize(ConfigurationResolver config) throws Throwable {

        _services = new Vector<PluginService>();

        _services.add(new SvcDataRegistryCollectionDescribe());
        _services.add(new SvcDataRegistryCollectionList());
        _services.add(new SvcDataRegistryMediafluxCollectionDescribe());
        _services.add(new SvcDataRegistryMediafluxCollectionList());
        _services.add(new SvcDataRegistryMediafluxCollectionProjectExport());

        _services.add(new SvcOrganisationAdd());
        _services.add(new SvcOrganisationList());
        _services.add(new SvcOrganisationRemove());
        _services.add(new SvcOrganisationRename());

        //
        _services.add(new SvcProjectDataRegistrySync());

        _services.add(new SvcNamespaceRevokeAllACLs());
        _services.add(new SvcProjectAdminMessageSend());
        //
        _services.add(new SvcProjectActorRestoreFromXML());
        _services.add(new SvcProjectAssetCount());

        _services.add(new SvcProjectCopyCollectionFromAsset());
        _services.add(new SvcProjectCopyProjectFromAsset());
        //
        _services.add(new SvcProjectAudit());
        _services.add(new SvcProjectAssetDuplicateFind());
        _services.add(new SvcProjectContentAccessLast());
        _services.add(new SvcProjectCreate());
        _services.add(new SvcProjectDescribe());
        _services.add(new SvcProjectDescribeDateRange());
        _services.add(new SvcProjectFind());
        _services.add(new SvcProjectGrantAccessToAdmin());
        _services.add(new SvcProjectHistogram());
        _services.add(new SvcProjectID());
        _services.add(new SvcProjectIngestRate());
        _services.add(new SvcProjectIngestRateSimple());
        _services.add(new SvcProjectList());
        _services.add(new SvcProjectBriefList());
        _services.add(new SvcProjectTypeSet());
        _services.add(new SvcProjectMetaDataMissing());
        _services.add(new SvcProjectNamespaceMTIMEQuery());
        _services.add(new SvcProjectNameSpaceDocCopy());
        _services.add(new SvcProjectNameSpaceDocRemove());
        _services.add(new SvcProjectNameSpaceMetaDataCopy());
        _services.add(new SvcProjectNameSpaceMove());
        _services.add(new SvcProjectNameSpaceSetStandardACLs());
        _services.add(new SvcProjectNameSpaceChildACLList());
        _services.add(new SvcProjectNameSpaceChildFind());
        _services.add(new SvcProjectNameSpaceUsersExclude());
        //
        _services.add(new SvcProjectPathList());
        _services.add(new SvcProjectParentNamespaceAdd());
        _services.add(new SvcProjectParentsNamespaceNotificationsForSchedule());
        _services.add(new SvcProjectParentNamespaceList());
        _services.add(new SvcProjectProvisionMessageGet());
        _services.add(new SvcProjectProvisionMessageSend());
        //
        _services.add(new SvcProjectDMEndUserExpiryNotification());
        _services.add(new SvcProjectDMOperatorExpiryNotification());
        _services.add(new SvcProjectDMOperatorExpiryList());
        _services.add(new SvcProjectDMExpiredDRFindAndMove());
        _services.add(new SvcProjectDMUploadCount());
        _services.add(new SvcProjectDMManifestAssetFind());
        //
        _services.add(new SvcProjectShareableDestinationSetFromXML());
        _services.add(new SvcProjectStoreSet());
        _services.add(new SvcProjectPosixReMount());
        _services.add(new SvcProjectPosixMount());
        _services.add(new SvcProjectPosixMountStandard());
        _services.add(new SvcProjectPosixUnMount());
        _services.add(new SvcProjectPosixMountList());
        _services.add(new SvcProjectPosixMountDescribe());
        _services.add(new SvcProjectPosixMapUIDAdd());
        _services.add(new SvcProjectPosixMapUIDRemove());
        _services.add(new SvcProjectPosixMapDescribe());
        _services.add(new SvcProjectPosixMapDomainIdentityAdd());
        _services.add(new SvcProjectPosixMapDomainIdentityRemove());
        _services.add(new SvcProjectProtocols());
        _services.add(new SvcProjectRoleAdd());
        _services.add(new SvcProjectServiceCollectionModify());
        _services.add(new SvcProjectSmartLinkDownloadCreate());
        _services.add(new SvcProjectQueryNameSpaceAdd());
        _services.add(new SvcProjectQuotaGet());
        _services.add(new SvcProjectQuotaSet());
        _services.add(new SvcProjectQuotaSetLimitAction());
        _services.add(new SvcProjectQuotaSum());
        _services.add(new SvcProjectQuotaUsage());
        _services.add(new SvcProjectQuotaUsedCheck());
        _services.add(new SvcProjectSize());
        _services.add(new SvcProjectIntegrityCheck());
        //
        _services.add(new SvcProjectReplicationCheck());
        //
        _services.add(new SvcProjectReplicateQueueSet());
        _services.add(new SvcProjectReplicateQueueUnset());
	    _services.add(new SvcProjectReplicateQueueCleanupMissingContent());
	    _services.add(new SvcProjectReplicateQueueGenerateCleanupScript());
        _services.add(new SvcProjectReplicateQueueList());
        _services.add(new SvcProjectReplicateQueueReset());
        _services.add(new SvcProjectReplicateQueueRebuild());
        _services.add(new SvcProjectReplicateQueueRecreate());
        //
        _services.add(new SvcProjectContentCopyQueueSet());
        _services.add(new SvcProjectContentCopyQueueUnset());
        _services.add(new SvcProjectContentCopyQueueRebuild());
        _services.add(new SvcProjectContentCopyQueueReset());
        _services.add(new SvcProjectContentCopyQueueRecreate());
        _services.add(new SvcProjectContentCopyList());
        //
        _services.add(new SvcProjectUserAdd());
        _services.add(new SvcProjectUserRemove());
        _services.add(new SvcProjectActorRemoveAll());
        _services.add(new SvcProjectUserList());
        _services.add(new SvcProjectUserDomainList());
        _services.add(new SvcProjectUserNone());
        _services.add(new SvcProjectsAssetHistogramGrafanaSend());
        _services.add(new SvcUserStandardRoleRevoke());
        _services.add(new SvcSecureIdentityTokenInvalidPermissionsRevoke());
        //
        _services.add(new SvcProjectRetentionNotify());
        _services.add(new SvcProjectSummaryForMembers());
        _services.add(new SvcProjectSummaryForSchedule());
        _services.add(new SvcProjectUsersMessageSend());
        _services.add(new SvcProjectNameSpaceMapPopulate());
        //
        _services.add(new SvcProjectShareableFind());
        // TODO remove the following java classes
//        _services.add(new SvcFacilityShareableDownloadComplete());
//        _services.add(new SvcFacilityShareableUploadAllList());
//        _services.add(new SvcFacilityShareableUploadArgsValidate());
//        _services.add(new SvcFacilityShareableUploadComplete());
//        _services.add(new SvcFacilityShareableUploadCompleteReset());
//        _services.add(new SvcFacilityShareableUploadCreate());
//        _services.add(new SvcFacilityShareableUploadDescribe());
//        _services.add(new SvcFacilityShareableUploadList());
//        _services.add(new SvcFacilityShareableUploadInteractionComplete());
//        _services.add(new SvcFacilityShareableUploadInteractionValidate());
//        _services.add(new SvcFacilityShareableUploadManifestCreate());
//        _services.add(new SvcFacilityShareableUploadManifestDescribe());
//        _services.add(new SvcFacilityShareableUploadManifestAttributeIncrement());
//        _services.add(new SvcFacilityShareableUploadMappingDescribe());
//        _services.add(new SvcFacilityShareableUploadMappingSet());
//        _services.add(new SvcFacilityShareableUploadMappingUnset());
//        _services.add(new SvcFacilityShareableUploadRecipientList());
//        _services.add(new SvcFacilityShareableUploadUpdate());
//        _services.add(new SvcFacilityShareableUploadUploadList());

        //
        _services.add(new SvcUserCreate());
        _services.add(new SvcUserMessageSend());
        _services.add(new SvcUserProjectList());
        _services.add(new SvcUserProjectRoleList());
        _services.add(new SvcUserProjectDescribe());
        _services.add(new SvcUserSelfProjectList());
        _services.add(new SvcUserSelfProjectEnum());
        _services.add(new SvcUserSelfProjectAdminIs());

        _services.add(new SvcUserProjectHasWritable());
        _services.add(new SvcUserGrantStandardRoles());
        _services.add(new SvcUserProjectRolesRevoke());
        _services.add(new SvcTest());
        _services.add(new SvcRcpAdminProjectDescribe());
        _services.add(new SvcRcpAdminProjectUserRemove());
        _services.add(new SvcRcpAdminProjectUserAdd());
        _services.add(new SvcUserRcpUsrRoleSet());

        /*
         * These are unreliable with large data bases because MF has to check all assets
         * are not referring to the document and role namespaces and it will lock up the
         * system _services.add(new SvcProjectDestroy()); _services.add(new
         * SvcProjectRename());
         */
        _services.add(new SvcProjectDestroy());

        // One offs in the migration of accounts to AD (completed now)
        // _services.add(new SvcUserLDAPMigrateTo());
        // _services.add(new SvcUserLDAPMigrateDisable());
        _services.add(new SvcAdminRoleNameSpacesMigrate());
        _services.add(new SvcAdminRoleNameSpacesMigrateCleanup());
    }

    @Override
    public void shutdown(ConfigurationResolver config) throws Throwable {

    }

    @Override
    public String vendor() {

        return "The University of Melbourne";
    }

    @Override
    public String version() {

        return "1.0";
    }

    @Override
    public Collection<PluginService> services() {

        return _services;
    }

}
