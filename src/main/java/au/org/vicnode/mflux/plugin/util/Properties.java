/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class Properties {

    // Properties holding system attributes. These are created by the installer
    // script. It's a way of getting configurable parameters into the run time
    // environment.
    // All these property names are matched in the installer script.
    // This is used to provide an application context for the properties.
    public static final String APPLICATION_PROPERTY_APP = "project-provisioning";

    // Local authentication domains
    public static final String APPLICATION_PROPERTY_AUTH_LOCAL_USER = "authentication.local.user";
    public static final String APPLICATION_PROPERTY_AUTH_LOCAL_ADMIN = "authentication.local.admin";

    // The property values which give the LDAP domain names
    public static final String APPLICATION_PROPERTY_AD_STAFF_DOMAIN = "authentication.ldap.staff.domain";
    public static final String APPLICATION_PROPERTY_AD_STUDENT_DOMAIN = "authentication.ldap.student.domain"; 

    // Asset namespaces
    public static final String APPLICATION_PROPERTY_ASSET_NAMESPACE_USER = "asset.namespace.user";
    public static final String APPLICATION_PROPERTY_ASSET_NAMESPACE_ADMIN = "asset.namespace.admin";
    public static final String APPLICATION_PROPERTY_ASSET_NAMESPACE_INSTRUMENT_UPLOAD_MANIFESTS = "asset.namespace.instrument-upload-manifests";

    // Document namespaces
    public static final String APPLICATION_PROPERTY_DOC_NAMESPACE_USER = "doc.namespace.user";
    public static final String APPLICATION_PROPERTY_DOC_NAMESPACE_ADMIN = "doc.namespace.admin";

    // Dictionary namespaces
    public static final String APPLICATION_PROPERTY_DICT_NAMESPACE_USER = "dict.namespace.user";
    public static final String APPLICATION_PROPERTY_DICT_NAMESPACE_ADMIN = "dict.namespace.admin";

    // Role namespaces
    public static final String APPLICATION_PROPERTY_ROLE_NAMESPACE_USER = "role.namespace.user";
    public static final String APPLICATION_PROPERTY_ROLE_NAMESPACE_ADMIN = "role.namespace.admin";

    // Service collection
    public static final String APPLICATION_PROPERTY_NAMESPACE_SERVICE_COLLECTION = "service.collection.namespace";

    // The property value gives the name and UUID of the Disaster Recovery system
    public static final String APPLICATION_PROPERTY_DR_UUID = "disaster.recovery.peer.uuid";
    public static final String APPLICATION_PROPERTY_DR_PEER_NAME = "disaster.recovery.peer.name";

    // The property value which gives the server host and port for service
    // notifications
    public static final String APPLICATION_PROPERTY_HOST_AND_PORT = "notification.host";

    // The property value which gives the support email address. Used for automated
    // notifications.
    public static final String APPLICATION_PROPERTY_SUPPORT_EMAIL = "notification.support.email";

    // The property value which gives the operations contact us page in the end-user
    // documentation.
    public static final String APPLICATION_PROPERTY_CONTACT_US = "support.contact";

    // The property value which gives the operations email address.
    public static final String APPLICATION_PROPERTY_OPERATIONS_EMAIL = "notification.operations.email";

    // The property value which gives the root for end-user documentation
    public static final String APPLICATION_PROPERTY_DOC_USER_ROOT = "documentation.user.root";

    // The property value which gives the page for the Mediaflux Explorer
    public static final String APPLICATION_PROPERTY_DOC_USER_EXPLORER = "documentation.user.explorer";

    // The property value which gives the page for the Mediaflux DataMover
    public static final String APPLICATION_PROPERTY_DOC_USER_DATAMOVER = "documentation.user.datamover";

    // The property value which gives the page for the Mediaflux DataMover
    public static final String APPLICATION_PROPERTY_DOC_USER_DATAMOVER_CLI = "documentation.user.datamover.cli";

    // The property value which gives the page for the Standard Project Roles
    public static final String APPLICATION_PROPERTY_DOC_STANDARD_PROJECT_ROLES = "documentation.user.standardprojectroles";

    // The property value which gives the page for valid filenames
    public static final String APPLICATION_PROPERTY_DOC_USER_VALID_FILENAMES = "documentation.user.valid-filenames";

    // The property value which gives the page for the RCS Terms of Service
    public static final String APPLICATION_PROPERTY_DOC_USER_RCS_TOS = "documentation.user.rcs-tos";

    // Properties about the asset processing queue for replication
    public static final String APPLICATION_PROPERTY_REPLICATION_QUEUE_NB_PROCESSORS = "replication.queue.nb-processors";

    // Properties about the asset processing queue for content copies
    public static final String APPLICATION_PROPERTY_CONTENT_COPY_QUEUE_NB_PROCESSORS = "content-copy.queue.nb-processors";
    public static final String APPLICATION_PROPERTY_CONTENT_COPY_QUEUE_STORE_NAME = "content-copy.queue.store.name";

    // The property value which gives the projects root namespace
    public static final String SERVER_PROPERTY_PROJECT_ROOT = "projects.namespace";

    // Replication user. Could be moved to an application property.
    public static final String REPLICATOR_DOMAIN = "system";
    public static final String REPLICATOR_USER = "replicator";

    // The root allocator name for citeable IDs embedded as the trailing part of
    // project IDs
    // proj-<name>-<cid>. 'project' for 'production' and 'project.test' for 'test'
    // projects
    // Could be moved to an application property.
    public static final String CID_ALLOCATOR_ROOT = "project";

    // Could be moved to an application property.
    public static final String PROJECT_TYPE_PROD = "production";
    public static final String PROJECT_TYPE_TEST = "test";
    public static final String PROJECT_TYPE_STUB = "closed-stub";
    public static final String PROJECT_TYPE_ALL = "all";

    // The leading part of project IDs ; proj-<name>-<cid>
    // Could be moved to an application property.
    public static final String PROJECT_ROOT_NAME = "proj-";

    // Name of child query namespaces
    // This could move to an application property
    public static final String QUERY_NAMESPACE_CHILD = "Queries";

    // Static Properties. Need to match the installer scripts and the internals of
    // the service definitions
    // You cannot pass this is via an application property because
    // it won't resolve at compiler time. All the properties
    // we get out of application properties are for at run time
    public static final String SERVICE_ROOT = "vicnode.";

    // Used at compile time
    public static String[] projectTypes = { PROJECT_TYPE_ALL, PROJECT_TYPE_PROD, PROJECT_TYPE_TEST, PROJECT_TYPE_STUB }; // Used
                                                                                                                         // in
                                                                                                                         // service
                                                                                                                         // APIs

    // SOme types for selecting projects. SHould do properly with enums.
    // Used at compile time
    public static String[] repTypes = { "all", "replication", "no-replication" };

    // TYpes of project user to receive regular project notifications
    // Used at compile time
    public static String[] notificationFrequency = {"weekly", "monthly" };

    // user is all users of the project (includes admin)
    // all is user + owner
    // owner is the owner specified in project meta-data
    // manage is admin + owner
    // Used at compile time
    public static String[] notificationRecipients = { "owner", "administrator", "manage", "user", "all" };

    /**
     * Returns value of application property
     * 
     * @param executor
     * @param name
     * @param app
     * @return Will be null if does not exist
     * @throws Throwable
     */
    static public String getApplicationProperty(ServiceExecutor executor, String name, String app) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("property", new String[] { "app", app }, name);
        XmlDoc.Element r = executor.execute("application.property.exists", dm.root());
        if (r.booleanValue("exists")) {
            r = executor.execute("application.property.get", dm.root());
            return r.value("property");
        } else {
            return null;
        }
    }

    /**
     * Get server property
     * 
     * @param executor
     * @param name     property name
     * @return
     * @throws Throwable
     */
    public static String getServerProperty(ServiceExecutor executor, String name) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("name", name);
        XmlDoc.Element r = executor.execute("server.property.get", dm.root());
        return r.value("property");
    }

    /**
     * Return the standard local account admin authentication domain
     * 
     * @return
     */
    static public String getAdminAuthDomain(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_AUTH_LOCAL_ADMIN, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the standard local account user authentication domain
     * 
     * @return
     */
    static public String getUserAuthDomain(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_AUTH_LOCAL_USER, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the staff LDAP domain name
     * 
     * @return
     */
    static public String getLDAPStaffDomain(ServiceExecutor executor) throws Throwable {
        // TBD: fetch from an application property
        return getApplicationProperty(executor, APPLICATION_PROPERTY_AD_STAFF_DOMAIN, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the student LDAP domain name
     * 
     * @return
     */
    static public String getLDAPStudentDomain(ServiceExecutor executor) throws Throwable {
        // TBD: fetch from an application property
        return getApplicationProperty(executor, APPLICATION_PROPERTY_AD_STUDENT_DOMAIN, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the administration asset namespace
     * 
     * @return
     */
    static public String getAdminAssetNameSpace(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_ASSET_NAMESPACE_ADMIN, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the administration role namespace
     * 
     * @return
     */
    static public String getAdminRoleNameSpace(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_ROLE_NAMESPACE_ADMIN, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the administration document namespace
     * 
     * @return
     */
    static public String getAdminDocNameSpace(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_DOC_NAMESPACE_ADMIN, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the administration dictionary namespace
     * 
     * @return
     */
    static public String getAdminDictNameSpace(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_DICT_NAMESPACE_ADMIN, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the administrator role
     * 
     * @return
     */
    static public String getAdministratorRoleName(ServiceExecutor executor) throws Throwable {
        return getAdminRoleNameSpace(executor) + ":administrator";
    }

    /**
     * FInd the name of the Collection document type
     * 
     * @return
     * @throws Throwable
     */
    static public String getCollectionDocType(ServiceExecutor executor) throws Throwable {
        return getUserDocNameSpace(executor) + ":Collection";
    }

    /**
     * Return the onboarding namespace
     * 
     * @return
     */
    static public String getOnboardingNameSpace(ServiceExecutor executor) throws Throwable {
        return getAdminAssetNameSpace(executor) + "/Onboarding";
    }

    /**
     * FInd the name of the user onboarding document type
     * 
     * @param executor
     * @return
     * @throws Throwable
     */
    static public String getOnboardingDocType(ServiceExecutor executor) throws Throwable {
        return getAdminDocNameSpace(executor) + ":User-Onboarding";
    }

    /**
     * Return the Project mount doc type
     * 
     * @return
     */
    static public String getPosixMountDocType(ServiceExecutor executor) throws Throwable {
        return getAdminDocNameSpace(executor) + ":Project-Mount";
    }

    /**
     * FInd the name of the dictionary holding the map of project IDs to asset
     * namespaces
     * 
     * @return
     * @throws Throwable
     */
    static public String getProjectNamespaceMapDictionaryName(ServiceExecutor executor) throws Throwable {
        // TBD: fetch from an application property
        return getAdminDictNameSpace(executor) + ":project-namespace-map";
    }

    /**
     * FInd the name of the dictionary holding the list of possible project root
     * namespaces
     * 
     * @return
     * @throws Throwable
     */
    static public String getProjectNamespaceRootsDictionaryName(ServiceExecutor executor) throws Throwable {
        // TBD: fetch from an application property
        return getAdminDictNameSpace(executor) + ":project-namespace-roots";
    }

    /**
     * FInd the name of the dictionary holding the list of primary system store
     * policies
     * 
     * @return
     * @throws Throwable
     */
    static public String getPrimaryStorePoliciesDictionaryName(ServiceExecutor executor) throws Throwable {
        // TBD: fetch from an application property
        return getAdminDictNameSpace(executor) + ":primary-store-policies";
    }

    /**
     * FInd the name of the project document type
     * 
     * @return
     * @throws Throwable
     */
    static public String getProjectDocType(ServiceExecutor executor) throws Throwable {
        return getAdminDocNameSpace(executor) + ":Project";
    }

    /**
     * FInd the name of the project parent admin document type
     * 
     * @return
     * @throws Throwable
     */
    static public String getProjectParentAdminDocType(ServiceExecutor executor) throws Throwable {
        return getAdminDocNameSpace(executor) + ":Project-Parent-Admin";
    }

    /**
     * Return the provisioning role
     * 
     * @return
     */
    static public String getProvisioningRoleName(ServiceExecutor executor) throws Throwable {
        // TBD: fetch from an application property
        return getAdminRoleNameSpace(executor) + ":provisioning";
    }

    /**
     * FInd the name of the research organisation dictionary
     * 
     * @return
     * @throws Throwable
     */
    static public String getResearchOrgDict(ServiceExecutor executor) throws Throwable {
        // TBD: fetch from an application property
        return getUserDictNameSpace(executor) + ":research-organisation";
    }

    /**
     * Return the standard user role
     * 
     * @return
     */
    static public String getStandardUserRoleName(ServiceExecutor executor) throws Throwable {
        // TBD: fetch from an application property
        return getUserRoleNameSpace(executor) + ":standard-user";
    }

    /**
     * Return the service collection name used for namespace context menus via the
     * Desktop
     * 
     * @return
     */
    static public String getNamespaceServiceCollection(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_NAMESPACE_SERVICE_COLLECTION,
                APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the user asset namespace
     * 
     * @return
     */
    static public String getUserAssetNameSpace(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_ASSET_NAMESPACE_USER, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the asset namespace for instrument upload manifest assets
     * 
     * @return
     */
    static public String getInstrumentUploadManifestAssetNameSpace(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_ASSET_NAMESPACE_INSTRUMENT_UPLOAD_MANIFESTS,
                APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the user role namespace
     * 
     * @return
     */
    static public String getUserRoleNameSpace(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_ROLE_NAMESPACE_USER, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the user document namespace
     * 
     * @return
     */
    static public String getUserDocNameSpace(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_DOC_NAMESPACE_USER, APPLICATION_PROPERTY_APP);
    }

    /**
     * Return the administration dictionary namespace
     * 
     * @return
     */
    static public String getUserDictNameSpace(ServiceExecutor executor) throws Throwable {
        return getApplicationProperty(executor, APPLICATION_PROPERTY_DICT_NAMESPACE_USER, APPLICATION_PROPERTY_APP);
    }

    /**
     * FInd the name of the user accounts document type
     * 
     * @return
     * @throws Throwable
     */
    static public String getUserAccountsDocType(ServiceExecutor executor) throws Throwable {
        return getUserDocNameSpace(executor) + ":User-Account-Details";
    }

    public static Map<String, String> getAll(ServiceExecutor executor) throws Throwable {
        Map<String, String> map = new LinkedHashMap<>();
        List<XmlDoc.Element> pes = executor.execute("application.property.list",
                "<args><app>" + APPLICATION_PROPERTY_APP + "</app></args>", null, null).elements("property");
        if (pes != null) {
            for (XmlDoc.Element pe : pes) {
                String name = pe.value("@name");
                String value = pe.value();
                if (name != null && value != null) {
                    map.put(name, value);
                }
            }
        }
        return map;
    }

    public static String getAdminDocNameSpace(Map<String, String> properties) {
        return properties.get(APPLICATION_PROPERTY_DOC_NAMESPACE_ADMIN);
    }

    public static String getAdminDictNameSpace(Map<String, String> properties) {
        return properties.get(APPLICATION_PROPERTY_DICT_NAMESPACE_ADMIN);
    }

    public static String getProjectNamespaceMapDictionaryName(Map<String, String> properties) throws Throwable {
        // TBD: fetch from an application property
        return getAdminDictNameSpace(properties) + ":project-namespace-map";
    }

    public static String getProjectDocType(Map<String, String> properties) throws Throwable {
        return getAdminDocNameSpace(properties) + ":Project";
    }
}
