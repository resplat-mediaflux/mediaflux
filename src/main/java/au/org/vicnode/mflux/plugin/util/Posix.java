/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;


import java.util.Collection;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class Posix {



	/**
	 * Add a {UID,User} pair to the identity map.
	 * 
	 * @param executor
	 * @param name identity map name
	 * @param uid
	 * @param domain
	 * @param user
	 * @throws Throwable
	 */
	public static void addUIDToPosixIdentityMap  (ServiceExecutor executor, String name, String uid, String domain, String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		dm.add("uid", uid);
		dm.add("map",name);
		executor.execute("posix.fs.identity.uid.add", dm.root());
	}




	/**
	 * Low-level function to ADD the posix mount point meta-data on the project namespace.
	 * Does not actually mount or unmount the actual mount point

	 * @param executor
	 * @param identityMapName
	 * @param name
	 * @param assetNameSpace  - Project asset namespace
	 * @param restrictions - Network restrictions
	 * @param root - root access restrictions
	 * @param readOnly
	 * @param allowedProtocol
	 * @throws Throwable
	 */
	public static void addPosixMountMetaData (ServiceExecutor executor, String assetNameSpace, 
			Collection<XmlDoc.Element> restrictions, XmlDoc.Element rootRestriction, Boolean readOnly, 
			XmlDoc.Element metaExport, Boolean applyModeBits, 
			String allowedProtocol, Boolean shouldBeMounted) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		//dm.add("inherit-asset-meta", false);
		dm.add("namespace", assetNameSpace);
		//
		dm.push("asset-meta", new String[]{"inherit", "false", "propagate", "false"});
		dm.push(Properties.getPosixMountDocType(executor));
		dm.push("posix");
		dm.add("should-be-mounted", shouldBeMounted);
		dm.add("read-only", readOnly);
		dm.add("apply-mode-bits", applyModeBits);
		dm.add("allowed-protocol", allowedProtocol);
		if (restrictions!=null) {
			for (XmlDoc.Element restrict : restrictions) {
				dm.add(restrict);
			}
		}
		if (rootRestriction!=null) {
			dm.add(rootRestriction);
		}
		if (metaExport!=null) {
			dm.add(metaExport);
		}
		dm.pop();
		dm.pop();
		dm.pop();
		executor.execute("asset.namespace.asset.meta.add", dm.root());
	}



	/**
	 * Create/fetch an arbitrary UID set for use woith Posix mounts and SFTP access
	 * 
	 * @param executor
	 * @param domain
	 * @param user
	 * @return
	 * @throws Throwable
	 */
	// The following code may be resurrected when Arcitecta add POSIX Mount point ACLs
	// the uid element used below is not currently in the Doc Type definition
	/*
	public static String getUserUID (ServiceExecutor executor, String domain, String user) throws Throwable {
		// Get User Info
		XmlDoc.Element r = describeUser (executor, domain, user);
		String uid = null;
		if (r!=null) {
		   String t = Properties.getUserAccountsDocType(executor);
		   uid = r.value("user/asset/meta/"+t+"/uid");
		}

		// See if has UID already
		if (uid!=null) return uid;

		// Allocate next UID
		uid = allocateUID (executor);

		// Set in user meta-data
		setUserUID(executor, user, domain, uid);

		return uid;
	}


	private static void setUserUID (ServiceExecutor executor, String user, String domain, String uid) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.push("meta", new String[]{"action", "merge"});
		dm.push(Properties.getUserAccountsDocType(executor));
		dm.add("uid", uid);
		dm.pop();
		dm.pop();
		dm.add("domain", domain);
		dm.add("user", user);
		executor.execute("user.set", dm.root());
	}

	private static String allocateUID (ServiceExecutor executor) throws Throwable {

		// Get root
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", UID_ALLOCATOR_NAME);
		XmlDoc.Element r = executor.execute("citeable.named.id.create", dm.root());
		String t = r.value("cid");

		// Allocate
		dm = new XmlDocMaker("args");
		dm.add("pid", t);
		r = executor.execute("citeable.id.create", dm.root());
		System.out.println("r="+r);
		t = r.value("cid");
		System.out.println("cid="+t);

		String[] t2 = t.split("\\.");
		System.out.println("n="+t2.length);
		System.out.println("0=" + t2[0]);
		System.out.println("1=" + t2[1]);
		System.out.println("2=" + t2[2]);
		return t2[2];	
	}

	 */


	/**
	 * Create an empty identity map
	 * 
	 * @param executor
	 * @param name
	 * @throws Throwable
	 */
	static public void createIdentityMap (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		executor.execute("posix.fs.identity.map.create", dm.root());
	}




	/**
	 * Returns the native posix mount point data. Exception if not mounted. 
	 * 
	 * @param executor
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public static XmlDoc.Element describePosixMountPoint (ServiceExecutor executor, String name) throws Throwable {
		// Describe the mount point
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		return executor.execute("posix.fs.asset.mount.describe", dm.root());
	}


	public static XmlDoc.Element describePosixMountPointIdentity (ServiceExecutor executor, String map) throws Throwable {

		// Describe the identities associated with this mount point (map and name are the same)
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("map", map);
		return executor.execute("posix.fs.identity.domain.identity.describe", dm.root());
	}




	/**
	 * Describes the posix identity map 
	 * 
	 * @param executor
	 * @param name Identiy map name
	 * @return
	 * @throws Throwable
	 */
	public static XmlDoc.Element describePosixIdentityMap  (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		return executor.execute("posix.fs.identity.map.describe", dm.root());
	}





	public static void destroyIdentityMap (ServiceExecutor executor, String identityMap) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", identityMap);
		if (!Posix.posixIdentityMapExists(executor, identityMap)) return;
		executor.execute("posix.fs.identity.map.destroy", dm.root());
	}


	/**
	 * Create the ACLs on a new project asset namespace to allow Posix semantics (so that e.g. SFTP will work)
	 * 
	 * @param executor
	 * @param projectName
	 * @param projectAssetNameSpace
	 * @throws Throwable
	 */
	/*
	 * No longer used.
	public static void grantAssetNameSpacePosixPerm (ServiceExecutor executor, String projectAssetNameSpace) throws Throwable {

		// Asset namespace. Set an ACL on the namespace.
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", projectAssetNameSpace);
		dm.push("acl");
		dm.add("actor", new String[]{"type", "role"}, Properties.getStandardUserRoleName(executor));
		dm.push("access");
		dm.add("namespace", "execute");
		dm.pop();
		dm.pop();
		executor.execute("asset.namespace.acl.grant", dm.root());
	}
	*/



	/**
	 * Finds out if the posix mount point exists for a given project
	 * 
	 * @param executor
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	static public Boolean isProjectMounted (ServiceExecutor executor, String projectID) throws Throwable {

		// If not mounted we get an exception
		try {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("name", projectID);
			executor.execute("posix.fs.asset.mount.describe", dm.root());
			return true;
		} catch (Throwable t ) {
			return false;
		}
	}


	/**
	 * Is the mount point read only - fetches from actual mount point.
	 * Returns null if not mounted.
	 * 
	 * @param executor
	 * @param name The name of the posix mount point
	 * @return
	 * @throws Throwable
	 */
	static public Boolean isReadOnly (ServiceExecutor executor, String name) throws Throwable {

		try {
			XmlDoc.Element r = describePosixMountPoint (executor, name);
			return r.booleanValue("mount/read-only");
		} catch (Throwable t) {
			return null;	
		}
	}


	/**
	 * Populate the default restrictions which restrict NFS to the
	 * local host only
	 * 
	 * @param dm
	 * @throws Throwable
	 */
	static public void makeDefaultRestrictions (XmlDocMaker dm) throws Throwable {
		dm.push("restrict", new String[]{"protocol", "nfs"});
		dm.push("network");
		dm.add("address", new String[]{"mask", "255.255.255.255"}, "127.0.0.1");
		dm.pop();
		dm.pop();
		//
		dm.push("root");
		dm.push("restrict", new String[]{"protocol", "nfs"});
		dm.push("network");
		dm.add("address", new String[]{"mask", "255.255.255.255"}, "127.0.0.1");
		dm.pop();
		dm.pop();
		dm.pop();
	}

	/**
	 * Low-level function to create a posix mount point for the given resources
	 * 
	 * @param executor
	 * @param projectID  - the project ID
	 * @param metaDataExport - contols if side car meta-data are exported
	 * @param nameSpace - the namespace to mount
	 * @param name - the name of the mount point.  
	 * @param restrictions - network restrictions
	 * @param readOnly
	 * @throws Throwable
	 */
	public static Boolean mountPosix (ServiceExecutor executor, String projectID, XmlDoc.Element metaDataExport, String nameSpace, 
			Collection<XmlDoc.Element> restrictions, XmlDoc.Element rootRestrictions, 
			Boolean readOnly, Boolean applyModeBits, String allowedProtocol) throws Throwable {
		if (projectID==null || nameSpace==null) return false;

		// Provision the Posix mount point. 
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("filter-readdir-by-user-access", true);   // Needs to be false for NFS
		dm.add("apply-mode-bits", applyModeBits);
		dm.add("allowed-protocol", allowedProtocol);
		dm.add("imap", projectID);
		dm.add("name", projectID);
		dm.add("case-sensitive-lookup", true);
		dm.add("namespace", nameSpace);
		if (readOnly!=null) {
			dm.add("read-only", readOnly);
		}
		if (metaDataExport!=null) {
			dm.add(metaDataExport);
		}
		if (restrictions!=null) {
			for (XmlDoc.Element restrict : restrictions) {
				dm.add(restrict);
			}
		}
		if (rootRestrictions!=null) {
			dm.add(rootRestrictions);
		}

		// These are lost when the server restarts. So we have a script
		// that runs on server restart that re-generates them
		executor.execute("posix.fs.asset.network.mount", dm.root());
		return true;
	}




	public static Boolean posixIdentityMapExists (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		XmlDoc.Element r = executor.execute("posix.fs.identity.map.exists", dm.root());		
		return r.booleanValue("exists");
	}


	/**
	 * Remove  a  UID from the identity map
	 * 
	 * @param executor
	 * @param projectID
	 * @param uid
	 * @throws Throwable
	 */
	public static void removeUIDFromPosixIdentityMap  (ServiceExecutor executor, String projectID, String uid) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("uid", uid);
		dm.add("map", projectID);
		executor.execute("posix.fs.identity.uid.remove", dm.root());
	}



	/**
	 * Function to rename the posix mount point meta-data when you rename a project
	 * It does this by destroying and recreating the mount point and the meta-data
	 * stored on the namespace. The readOnly state of the mount point is preserved in this process.
	 * 
	 * @param executor
	 * @param oldProjectID
	 * @param assetNameSpace - this is the actual asset namespace that currently holds the
	 * posix mount point meta-data.  It might be the old project ID or the new one depending on
	 * the order in which you are doing things. We aren't changing the asset namespace in this process.
	 * @param newProjectID
	 * @throws Throwable
	 */
	public static void renamePosixMountPoint (ServiceExecutor executor, String oldProjectID,
			String assetNameSpace, String newProjectID) throws Throwable {

		// First unmount the extant mount point. The name of the mount point is the old project ID
		// If it's not mounted we don't care so catch the error and continue
		try {
			Posix.unMountPosix(executor, oldProjectID);
		} catch (Throwable t) {
			//
		}

		// FInd the old Posix mount read-only and network restriction
		XmlDoc.Element r = Posix.describePosixMountMetaData(executor, assetNameSpace);
		Boolean readOnly = r.booleanValue("read-only", false);
		Boolean shouldBeMounted = r.booleanValue("should-be-mounted", true);
		Collection<XmlDoc.Element> restrictions = r.elements("restrict");
		XmlDoc.Element root = r.element("root");
		XmlDoc.Element metaExport = r.element("metadata-export");

		// If this element is missing, it's from before we added it.
		// In this case, the underlying MF service default (true) is what is active.
		Boolean applyModeBits = r.booleanValue("apply-mode-bits", true);

		// If this element is missing, it's from before we added it.
		// In this case, the underlying MF service default is all protocols
		String allowedProtocol = r.stringValue("allowed-protocol", "all");

		// Now remove the mount point meta-data from the asset namespace (it might be the old
		// or the new depending on the order you are renaming the Project in)
		Posix.removePosixMountMetaData(executor, assetNameSpace);

		// Destroy the old identity map
		Posix.destroyIdentityMap (executor, oldProjectID);

		// Create new Identity map
		Posix.createIdentityMap(executor, newProjectID);

		// Recreate the mount point if desired
		if (shouldBeMounted) {
			Posix.mountPosix(executor, newProjectID, r.element("metadata-export"), 
					assetNameSpace, restrictions, root, readOnly, 
					applyModeBits, allowedProtocol);
		}

		// Recreate the mount point meta-data on the asset namespace
		Posix.addPosixMountMetaData(executor,  assetNameSpace, restrictions, root, 
				readOnly, metaExport, applyModeBits, allowedProtocol,
				shouldBeMounted);	
	}



	/**
	 * Updates the posix mount point meta-data with the new values. If does not exist, adds.
	 * 
	 * @param executor
	 * @param projectID
	 * @param readOnly
	 * @return
	 * @throws Throwable
	 */
	public static void updatePosixMountMetaData (ServiceExecutor executor, String projectID, 
			Collection<XmlDoc.Element> restrictions, XmlDoc.Element root,  
			Boolean readOnly, XmlDoc.Element metaExport, Boolean applyModeBits,
			String allowedProtocol, Boolean shouldBeMounted) throws Throwable {

		// Get the current Posix mount state meta-data attached to the namespace
		String nameSpace = Project.assetNameSpace(executor, projectID, false);
		XmlDoc.Element posixMeta = describePosixMountMetaData (executor, nameSpace);
		if (posixMeta==null) {
			addPosixMountMetaData (executor, nameSpace, restrictions, root, readOnly, 
					metaExport, applyModeBits, allowedProtocol, shouldBeMounted);
			return;
		}

		// Replace - the limited service handling makes this rather clumsy
		XmlDoc.Element ns = NameSpaceUtil.describe(null, executor, nameSpace);
		XmlDoc.Element assetMeta = ns.element("namespace/asset-meta");
		XmlDoc.Element posix = assetMeta.element(Properties.getPosixMountDocType(executor)+"/posix");	

		// Set by reference
		XmlDoc.Element rO = posix.element("read-only");     // Mandatory
		rO.setValue(readOnly);

		// This element was added later and may or may not be present.
		XmlDoc.Element amb = posix.element("apply-mode-bits");
		if (amb==null) {
			XmlDoc.Element t = new XmlDoc.Element("apply-mode-bits", applyModeBits);
			posix.add(t);
		} else {
			amb.setValue(applyModeBits);
		}
		
		// This element was added later and may or may not be present.
		XmlDoc.Element sbm = posix.element("should-be-mounted");
		if (sbm==null) {
			XmlDoc.Element t = new XmlDoc.Element("should-be-mounted", shouldBeMounted);
			posix.add(t);
		} else {
			sbm.setValue(shouldBeMounted);
		}

		// This element was added later and may or may not be present.
		XmlDoc.Element ap = posix.element("allowed-protocol");
		if (ap==null) {
			XmlDoc.Element t = new XmlDoc.Element("allowed-protocol", allowedProtocol);
			posix.add(t);
		} else {
			ap.setValue(allowedProtocol);
		}


		// Remove old
		Collection<XmlDoc.Element> oldRestrictions = posix.elements("restrict");       // Optional
		if (oldRestrictions!=null) {
			for (XmlDoc.Element oldRestrict : oldRestrictions) {
				posix.remove(oldRestrict);
			}
		}
		XmlDoc.Element oldRoot = posix.element("root");
		if (oldRoot!=null) {
			posix.remove(oldRoot);
		}
		XmlDoc.Element oldMetaExport = posix.element("metadata-export");
		if (oldMetaExport!=null) {
			posix.remove(oldMetaExport);
		}

		// Add new	
		if (restrictions!=null) {
			for (XmlDoc.Element restrict : restrictions) {
				posix.add(restrict);
			}
		}
		if (root!=null) {
			posix.add(root);
		}
		if (metaExport!=null) {
			posix.add(metaExport);
		}

		// Now fetch all elements to set
		Collection<XmlDoc.Element> els = assetMeta.elements();	

		NameSpaceUtil.setAssetNameSpaceMetaData (null, executor, nameSpace, els);
	}




	/**
	 * Low-level function to unmount a posix mount point 
	 * 
	 * @param executor
	 * @param name - the name of the mount point
	 * 
	 * @throws Throwable
	 */
	public static void unMountPosix (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		executor.execute("posix.fs.asset.network.unmount", dm.root());
	}






	/**
	 * Returns the Project-Mount meta-data on the given project's namespace. Will be null if non-existent
	 * This meta-data is set on a project asset namespace and provides the mount point state
	 * when last mounted.  It's used to remount projects in the same state after a server restart
	 * 
	 * @param executor
	 * @param nameSpace - namespace for the project
	 * @return
	 * @throws Throwable
	 */
	public static XmlDoc.Element describePosixMountMetaData (ServiceExecutor executor, String nameSpace) throws Throwable {

		XmlDoc.Element r = NameSpaceUtil.describe(null, executor, nameSpace);
		return r.element("namespace/asset-meta/"+Properties.getPosixMountDocType(executor)+"/posix");		
	}


	/**
	 * Low-level function to remove the posix mount point meta-data on the project asset namespace.

	 * @param executor
	 * @param assetNameSpace
	 * @throws Throwable
	 */
	private static void removePosixMountMetaData (ServiceExecutor executor, String assetNameSpace) throws Throwable {

		// FInd the ID of the piece of meta-data that we want
		XmlDoc.Element r = NameSpaceUtil.describe(null, executor, assetNameSpace);
		if (r==null) return;
		XmlDoc.Element meta = r.element("namespace/asset-meta/" + Properties.getPosixMountDocType(executor));
		if (meta==null) return;
		String mid = meta.value("@id");

		// Remove it
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", assetNameSpace);
		dm.add("mid", mid);
		executor.execute("asset.namespace.asset.meta.remove", dm.root());
	}



}
