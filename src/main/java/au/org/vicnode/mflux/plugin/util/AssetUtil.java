/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.plugin.util;

import java.util.Collection;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class AssetUtil {

	/**
	 * Copy documents from one asset to another. All attributes are transferred
	 * except 'id' which is generated afresh in the new asset.
	 * 
	 * @param executor
	 * @param docTypes The document types
	 * @param cidIn
	 * @param cidOut
	 * @param ns       - add an optional namespace attribute (ns) string
	 * @param tag      - addd an optional tag attribute (tag) string
	 * @throws Throwable
	 */
	public static void copyMetaData(ServiceExecutor executor, Collection<String> docTypes, String idIn, String idOut,
			String ns, String tag) throws Throwable {

		// Get meta-data in input object
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", idIn);
		XmlDoc.Element r = executor.execute("asset.get", dm.root());
		if (r == null)
			return;

		// Iterate through given document types
		for (String docType : docTypes) {

			// If we have this document type, copy it
			XmlDoc.Element docIn = r.element("asset/meta/" + docType);
			if (docIn != null) {

				// We don't want the 'id' attribute. We want all other attributes
				// such as namespace and tag
				removeAttribute(executor, docIn, "id");

				// Do it
				XmlDocMaker dm2 = new XmlDocMaker("args");
				dm2.add("id", idOut);
				dm2.push("meta", new String[] { "action", "add" });
				if (ns != null) {
					XmlDoc.Attribute attr = new XmlDoc.Attribute("ns", ns);
					docIn.add(attr);
				}
				if (tag != null) {
					XmlDoc.Attribute attr = new XmlDoc.Attribute("tag", tag);
					docIn.add(attr);
				}
				dm2.add(docIn);
				dm2.pop();
				executor.execute("asset.set", dm2.root());
			}
		}
	}

	/**
	 * Get the asset's meta-data
	 * 
	 * @param executor
	 * @param cid      Citable ID
	 * @param id       Asset ID (give one of id or cid)
	 * @throws Throwable
	 */
	public static XmlDoc.Element getAsset(ServiceExecutor executor, String cid, String id) throws Throwable {

		// Get meta-data in input object
		XmlDocMaker dm = new XmlDocMaker("args");
		if (cid != null) {
			dm.add("cid", cid);
		} else if (id != null) {
			dm.add("id", id);
		} else {
			throw new Exception("One of cid or id must be given");
		}

		return executor.execute("asset.get", dm.root());

	}

	public static XmlDoc.Element getAssetMeta(ServiceExecutor executor, String assetId) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", assetId);
		return executor.execute("asset.get", dm.root()).element("asset");
	}
	
	
	/**
	 * Get the asset's attributes
	 * 
	 * @param executor
	 * @param id Asset ID (
	 * @throws Throwable
	 */
	public static XmlDoc.Element getAssetAttributes (ServiceExecutor executor, String id) throws Throwable {

		// Get meta-data in input object
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		return executor.execute("asset.attribute.describe", dm.root());
	}


	
	

	/**
	 * Remove the given document (i.e. a full instantiation of a document type) from
	 * the given asset.
	 * 
	 * @param executor
	 * @param cid      asset CID (give cid or id)
	 * @param id       asset ID
	 * @param doc
	 * @throws Throwable
	 */
	public static void removeDocument(ServiceExecutor executor, String cid, String id, XmlDoc.Element doc)
			throws Throwable {

		String[] attString = doc.attributeArray();

		// Remove
		XmlDocMaker dm = new XmlDocMaker("args");
		if (cid != null) {
			dm.add("cid", cid);
		} else {
			if (id != null)
				dm.add("id", id);
		}
		dm.push("meta", new String[] { "action", "remove" });
		dm.push(doc.qname(), attString); // Use qualified name to set attributes; includes document name space
		dm.pop();
		dm.pop();
		//
		executor.execute("asset.set", dm.root());
	}

	/**
	 * Remove the named attribute from the document
	 * 
	 * @param executor
	 * @param doc
	 * @param attributeName
	 * @throws Throwable
	 */
	public static void removeAttribute(ServiceExecutor executor, XmlDoc.Element doc, String attributeName)
			throws Throwable {
		XmlDoc.Attribute attr = doc.attribute(attributeName);
		if (attr != null) {
			doc.remove(attr);
		}

	}

	public static boolean assetExists(ServiceExecutor executor, String assetId) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", assetId);
		return executor.execute("asset.exists", dm.root()).booleanValue("exists");
	}

    public static void destroyAsset(ServiceExecutor executor, String assetId) throws Throwable {
       XmlDocMaker dm = new XmlDocMaker("args");
       dm.add("id", assetId);
       executor.execute("asset.destroy", dm.root());
    }
}
