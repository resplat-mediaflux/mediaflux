/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;

import java.util.Collection;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;


public class DictionaryUtil {

	/**
	 * Add a new term.
	 * @param executor
	 * @param dictionary
	 * @param term
	 * @param definition - can be null for none
	 * 
	 * @throws Throwable
	 */
	static public void addDictionaryTerm (ServiceExecutor executor, String dictionary, String term, String definition) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("dictionary", dictionary);
		dm.add("term", term);
		if (definition!=null) {
			dm.add("definition", definition);
		}
		executor.execute("dictionary.entry.add", dm.root());
	}

	static public void removeDictionaryTerm (ServiceExecutor executor, String dictionary, String term) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("dictionary", dictionary);
		dm.add("term", term);
		executor.execute("dictionary.entry.remove", dm.root());
	}

	/**
	 * We leave the return as an XMLDoc.Element so we can look things up quickly with XPATH
	 * 
	 * @param executor
	 * @param dictionary
	 * @return
	 * @throws Throwable
	 */
	static public XmlDoc.Element describeDictionaryTerms (ServiceExecutor executor, String dictionary) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("dictionary", dictionary);
		return executor.execute("dictionary.entries.describe", dm.root());
	}

	static public XmlDoc.Element describeDictionaryTerm (ServiceExecutor executor, String dictionary, String term) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("dictionary", dictionary);
		dm.add("term", term);
		return executor.execute("dictionary.entry.describe", dm.root());
	}

	static public String getDictionaryValue (ServiceExecutor executor, String dictionary, int idx) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("idx", idx);
		dm.add("size", 1);
		dm.add("dictionary", dictionary);
		XmlDoc.Element r = executor.execute("dictionary.entries.list", dm.root());
		if (r==null) return null;
		return r.value("term");
	}

	/**
	 * List dictionary entries.  Exception if  does not exist
	 * 
	 * @param executor
	 * @param dictionary
	 * @return
	 * @throws Throwable
	 */
	static public Collection<String> getDictionaryValues (ServiceExecutor executor, String dictionary) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("dictionary", dictionary);
		XmlDoc.Element r = executor.execute("dictionary.entries.list", dm.root());
		if (r==null) return null;
		return  r.values("term");
	}


	/**
	 * Does the dictionary exist
	 * 
	 * @param executor
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	static public Boolean dictionaryExists (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		return executor.execute("dictionary.exists", dm.root()).booleanValue("exists");
	}

	/**
	 * Does the dictionary namespace exist ?
	 * 
	 * @param executor
	 * @param serverUUID - the server to execute on. Null for localhost
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  dictionaryNameSpaceExists (ServiceExecutor executor, String serverUUID, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = Util.execute(executor, serverUUID, "dictionary.namespace.exists", dm);
		return r.booleanValue("exists");
	}
	
	/**
	 * Create dictionary namespace
	 * 
	 * @param name - name of namespace to create
	 * @param description - description of namespace
	 */
	static public void createDictionaryNameSpace (ServiceExecutor executor, 
			String name, String description) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", name);
		if (description!=null) {
			dm.add("description", description);
		}
		Util.execute(executor, null, "dictionary.namespace.create", dm);
	}
}
