/** 
 * @author Neil Killeen
 *
 * Help classes to manage the process of an asset processing queue
 * that makes a content copy of data, as it arrives, into an aggregated store.
 * 
 * Copyright (c) 2022, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;


public class ProjectContentCopy {

	// The name of the :enqueue element of a namespace after
	// the content copy queue is added to it
	private static final String ENQUEUE_NAME = "ContentCopy";

	// The name of the actual asset processing queue
	private static final String ASSET_PROCESSING_QUEUE_NAME = "content-copy";

	/**
	 * Set the standard content copy asset processing queue on a project parent namespace
	 * If the queue does not pre-exist, create it.
	 * 
	 * @param executor
	 * @param projectID
	 * @throws Throwable
	 */
	public static void setStandardQueueOnProject (ServiceExecutor executor, String projectID, XmlWriter w) throws Throwable {

		String namespace = Project.assetNameSpace(executor, projectID, false);
		String queueName = createOrFindStandardQueue (executor, null);	
		AssetProcessingQueue.addQueueToNamespace(executor, namespace, queueName, 
				ENQUEUE_NAME, contentCopyFilter(executor));
		w.add("content-copy-queue", new String[] {"namespace", namespace, "set", "true"}, queueName);
	}

	/**
	 * Unset the standard content copy asset processing queue on a project parent namespace
	 * If the queue does not pre-exist, create it.
	 * 
	 * @param executor
	 * @param projectID
	 * @throws Throwable
	 */
	public static void unsetStandardQueueOnProject (ServiceExecutor executor, String projectID, XmlWriter w) throws Throwable {

		String namespace = Project.assetNameSpace(executor, projectID, false);
		String queueName = createOrFindStandardQueue (executor, null);	
		AssetProcessingQueue.removeQueueFromNamespace(executor, namespace,ENQUEUE_NAME);
		w.add("content-copy-queue", new String[] {"namespace", namespace, "unset", "true"}, queueName);
	}


	/**
	 * Create the standard content copy asset processing queue.
	 * If the queue already exists does nothing
	 * 
	 * @param executor
	 * @param w - XmlWriter can be null for no output
	 * @return the name of the processing queue
	 * @throws Throwable
	 */
	public static String createOrFindStandardQueue (ServiceExecutor executor, 
			XmlWriter w) throws Throwable {

		// Nothing to do if exists
		String queueName = getProjectStandardQueueName();
		if (AssetProcessingQueue.queueExists(executor, queueName)) {
			if (w!=null) {
				w.add("processing-queue", new String[] {"exists", "true"}, queueName);			
			}
			return queueName;
		}

		// Number of processors
		String nbProcessors = Properties.getApplicationProperty(executor, Properties.APPLICATION_PROPERTY_CONTENT_COPY_QUEUE_NB_PROCESSORS, Properties.APPLICATION_PROPERTY_APP);

		// Sanity check.  We should have tested that the system is configured
		// to make content copies before coming here.		
		Integer n = Integer.parseInt(nbProcessors);
		if (n<=0) {
			throw new Exception ("The system is not configured to generate asset content copies");
		}

		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("description", "Queue to handle project asset content copies");
		dm.add("name", queueName);
		dm.add("nb-processors", nbProcessors);
		dm.push("processing-service", new String[] {"name", "asset.content.copy.create"});
		dm.add("fail-if-staging", true);
		dm.add("gen-csum", true);   
		dm.add("store", getStoreName(executor));
		dm.pop();
		//
		executor.execute("asset.processing.queue.create", dm.root());
		if (w!=null) {
			w.add("processing-queue", new String[] {"exists", "false"}, queueName);
		}
		return queueName;
	}

	/**
	 * Return the name of the standard content copy asset processing queue 
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	private static String getProjectStandardQueueName () throws Throwable {
		return ASSET_PROCESSING_QUEUE_NAME;
	}



	/* Fetch store aggregation parameters
	 * 
	 * @result [0] = max content size in bytes
	 *         [1] = max total size in bytes
	 * 
	 */
	private static String[] getStoreSizes (ServiceExecutor executor) throws Throwable {

		String storeName = getStoreName(executor);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", storeName);
		XmlDoc.Element r = executor.execute("asset.store.describe", dm.root());
		if (r==null) {
			throw new Exception("No store description returned");			
		}
		XmlDoc.Element agg = r.element("store/aggregation/policy");	
		if (agg==null) {
			throw new Exception("No aggregation policy found on store " + storeName);			
		}
		String[] results = {agg.value("max-content-size"), agg.value("max-total-size")};
		return results;
	}


	/**
	 * Return a filter which will be set on the namespace when the
	 * asset processing queue is enqueued
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	private static String contentCopyFilter (ServiceExecutor executor) throws Throwable {

		// Fetch details of the content copy store for predicate fitering
		String[] res = getStoreSizes (executor);

		// We  use FAQL as much as is possible to simply just evaluate the
		// meta-data of the asset rather than doing some heavy-weight 
		// data base query.   the asset.content.copy.create service
		// ignores assets with  zero-sized content so we
		// don't need to filter those out.
		String t = "function(asset.has.content()) and " +
				"function(asset.is.ready.to.copy()) and " +
				"function(xvalue('content/size')<=" + res[0] + ")";
		/*	
		String t = "function(asset.has.content()) and " +
				   "function(!asset.content.is.staging()) and " +
		           "asset has checksum and " +
				   "function(xvalue('content/size')<=" + res[0] + ")";
		 */
		return t;
	}

	/**
	 * FInd the name of the store in which we are going to make content copies
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	private static String getStoreName (ServiceExecutor executor) throws Throwable {
		return Properties.getApplicationProperty(executor, Properties.APPLICATION_PROPERTY_CONTENT_COPY_QUEUE_STORE_NAME, Properties.APPLICATION_PROPERTY_APP);
	}


	/**
	 * Inspect the relevant application properties to evaluate if
	 * the system is configured to generate content copies
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static Boolean isConfigured (ServiceExecutor executor) throws Throwable {
		String t = Properties.getApplicationProperty(executor, Properties.APPLICATION_PROPERTY_CONTENT_COPY_QUEUE_NB_PROCESSORS, Properties.APPLICATION_PROPERTY_APP);
		Integer n = Integer.parseInt(t);
		if (n<=0) {
			return false;
		}
		String store = Properties.getApplicationProperty(executor, Properties.APPLICATION_PROPERTY_CONTENT_COPY_QUEUE_STORE_NAME, Properties.APPLICATION_PROPERTY_APP);
		if (store.isEmpty()) {
			return false;
		}
		// Validate store
		if (!Util.storeExists(executor, null, store)) {
			return false;
		}
		return true;
	}

	/**
	 * High-level function to establish if a project is having
	 *   content copies generated by an asset processing queue 
	 * 
	 * @param ph ProjectHolder
	 * @return - 
	 *  If not being replicated returns null
	 *  If being replicated, returns the asset processing queue name
	 * @throws Throwable
	 */
	public static String isProjectContentCopied (ProjectHolder ph) throws Throwable {

		XmlDoc.Element nsMeta = ph.metaData();

		// Asset processing queue
		// Look for associated asset processing queue
		XmlDoc.Element enq = getStandardEnqueueElement(nsMeta);
		if (enq!=null) {
			return enq.value("queue");
		} 
		//
		return null;
	}

	/**
	 * Look for an enqueue element in the asset namespace meta-data
	 * and if it is the specialised content-copy one, return the enqueue element
	 * Otherwise return null
	 * 
	 * @param nsMeta - the meta-data describing the asset namespace
	 */
	public static XmlDoc.Element getStandardEnqueueElement (XmlDoc.Element nsMeta) throws Throwable {
		return AssetProcessingQueue.getEnqueueElement(nsMeta, ENQUEUE_NAME);
	}
}
