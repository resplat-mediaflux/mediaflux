package au.org.vicnode.mflux.plugin.util;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDocMaker;

public class AssetNamespaceUtil {

    public static boolean assetNamespaceExists(ServiceExecutor executor, String assetNS) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", assetNS);
        return executor.execute("asset.namespace.exists", dm.root()).booleanValue("exists");
    }

    public static void createAssetNamespace(ServiceExecutor executor, String assetNS, boolean createAll)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", new String[] { "all", Boolean.toString(createAll) }, assetNS);
        executor.execute("asset.namespace.create", dm.root());
    }

}
