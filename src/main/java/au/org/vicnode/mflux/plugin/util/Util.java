/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.streams.StreamCopy;
import arc.xml.CSVUTF8Writer;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlFileOutput;


public class Util {


	
	// A public enumerator that holds a list of storage unit types, plus methods for setting the storage unit and converting the storage bytes into that unit.  

	public static enum StorageUnit {
		KB, MB, GB, TB, PB;
		// Method to convert an amount of storage in bytes into the relevant unit attached to the object instance
		public XmlDoc.Element convert(Long storage, String purpose, String type) throws Throwable{
			double divKB = 1.0e3;
			double divMB = 1.0e6;
			double divGB = 1.0e9;
			double divTB = 1.0e12;
			double divPB = 1.0e15;
			double quota=0;
			switch(this){
			case KB:
				quota = storage / divKB;
				break;
			case MB:
				quota = storage / divMB;
				break;
			case GB:
				quota = storage / divGB;
				break;
			case TB:
				quota = storage / divTB;
				break;
			case PB:
				quota = storage / divPB;
				break;
			}

			//
			String[] attributes = null;
			if (type==null) {
				attributes = new String[2];
				attributes[0] = "units";
				attributes[1] = this.toString();
			} else {
				attributes = new String[4];
				attributes[0] = "type";
				attributes[1] = type;
				attributes[2] = "units";
				attributes[3] = this.toString();
			}

			XmlDocMaker dm = new XmlDocMaker("root");
			dm.add(purpose,attributes, quota);
			return dm.root().element(purpose);
		}

		/**
		 * Static function to convert a String of the for xxxGB  to bytes
		 * Handles KB, MB, GB, TG, PB
		 * 
		 * @param value
		 * @return
		 * @throws Throwable  if unit is unknown an exception will occur
		 */
		public static  long convertToBytes (String value) throws Throwable{

			int idx = value.lastIndexOf("B");
			String val = value.substring(0,idx-1).trim();
			long iVal = Long.parseLong(val);
			String unit = value.substring(idx-1).trim();
			if (unit.equalsIgnoreCase("KB")) {
				return iVal * 1000l;
			} else if (unit.equalsIgnoreCase("MB")) {
				return iVal * 1000000l;
			} else if (unit.equalsIgnoreCase("GB")) {
				return iVal * 1000000000l;
			} else if (unit.equalsIgnoreCase("TB")) {
				return iVal * 1000000000000l;
			} else if (unit.equalsIgnoreCase("PB")) {
				return iVal * 1000000000000000l;
			} else {
				throw new Exception  ("Unhandled unit '" + unit+"'");
			}
		}


		/**
		 * Convert the storage amount to a value with the current units
		 * 
		 * @param storage
		 * @return
		 * @throws Throwable
		 */
		public String convertToString (Long storage) throws Throwable {
			XmlDoc.Element r = convert (storage, "value", null);
			String value = r.value();
			String units = r.value("@units");
			return value + " " + units;
		}

		// Sets the storage units on the object instance
		public static StorageUnit fromString(String s, StorageUnit defaultValue){
			if(s!=null){
				StorageUnit[] vs = values();
				for(int i=0;i<vs.length;i++){
					if(vs[i].name().equalsIgnoreCase(s)){
						return vs[i];
					}
				}
			}
			return defaultValue;
		}

		public static String fromUnit (Util.StorageUnit unit) {
			if (unit==PB) {
				return "PB";
			} else if (unit==TB) {
				return "TB";
			} else if (unit==GB) {
				return "GB";
			} else if (unit==MB) {
				return "MB";
			} else if (unit==KB) {
				return "KB";
			}

			return null;
		}
		public static StorageUnit toUnit (String unit) {
			if (unit.equals("PB")) {
				return PB;
			} else if (unit.equals("TB")) {
				return TB;
			} else if (unit.equals("GB")) {
				return GB;
			} else if (unit.equals("MB")) {
				return MB;
			} else if (unit.equals("KB")) {
				return KB;
			}

			return null;
		}


		public static String[] units () {
			String[] t = {"KB", "MB", "GB", "TB", "PB"};
			return t;
		}

	}


	/**
	 * Convert from bytes to a more human readable form
	 * 
	 * @param executor
	 * @param multiplier : a string that should be either "MB", "GB", "TB", "PB"
	 * @return XmlDoc.Element : Total with a "unit" attribute equivalent to the multiplier and a value of storage in that unit  
	 * @throws Throwable

	public static XmlDoc.Element convertStorageUnits (ServiceExecutor executor, String multiplier, long storage) throws Throwable {

		double divMB = 1.0e6;
		double divGB = 1.0e9;
		double divTB = 1.0e12;
		double divPB = 1.0e15;
		double quota=0;
		String[] attributes = new String[2];

		switch(multiplier){
			case "MB":
				quota = storage / divMB;
			break;
			case "GB":
				quota = storage / divGB;
			break;
			case "TB":
				quota = storage / divTB;
			break;
			case "PB":
				quota = storage / divPB;
			break;
		}

		attributes[0] = "units";
		attributes[1] = multiplier;
		XmlDocMaker dm = new XmlDocMaker("root");
		dm.add("Total",attributes, quota);
		return dm.root().element("Total");
	}	
	 */



	/**
	 * Create a role 
	 * 
	 * @param executor
	 * @param rolename (includes the namespace)
	 * @throws Throwable
	 */
	static public void  createRole (ServiceExecutor executor, String roleName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("role", roleName);
		dm.add("ifexists", "ignore");
		executor.execute("authorization.role.create", dm.root());
	}


	public static XmlDoc.Element describeActor (ServiceExecutor executor, String name, String type) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		dm.add("type", type);
		return executor.execute("actor.describe", dm.root());
	}


	/**
	 * Check if the string is a citeable id.
	 * 
	 * @param s
	 * @return
	 */
	public static boolean isCiteableId(String s) {

		if (s == null) {
			return false;
		}

		return s.matches("^\\d+(\\.\\d+)*$");
	}

	/**
	 * Check if a string contains only digits
	 * 
	 * @param str
	 * @return
	 */
	public static Boolean onlyDigits(String s) throws Throwable {
		if (s == null) {
			return false;
		}

		return s.matches("[0-9]+");
	} 

	/**
	 * Parse a text file holding XML into an XmlDoc.ELement
	 * 
	 * @param file
	 * @return
	 * @throws Throwable
	 */
	public static XmlDoc.Element parseXmlFile (File file) throws Throwable {
		InputStream targetStream = new FileInputStream(file);
		return new XmlDoc().parse(new InputStreamReader(targetStream));
	}

	
	/**
	 * Check if this role exists
	 * 
	 * @param executor
	 * @param roleName
	 * @return
	 * @throws Throwable
	 */
	public static boolean roleExists (ServiceExecutor executor, String roleName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("role", roleName);
		XmlDoc.Element r = executor.execute("authorization.role.exists", dm.root());
		return r.booleanValue("exists");
	}


	static public String getServerProperty (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		XmlDoc.Element r = executor.execute("server.property.get", dm.root());
		return r.value("property");
	}

	/**
	 * Destroy role
	 * 
	 * @param executor
	 * @param role
	 * @throws Throwable
	 */
	static public void destroyRole (ServiceExecutor executor, String role) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("role", role);
		executor.execute("authorization.role.destroy", dm.root());
	}


	/**
	 * Grant or revoke a permission to a role
	 * 
	 * @param executor
	 * @param user : <domain>:<user>
	 * @param projectRole
	 * @param grant
	 * @throws Throwable
	 */
	static public void  grantRevokePermToRole (ServiceExecutor executor, String role, String access, String resourceType, 
			String resourceName, Boolean grant) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", role);
		dm.add("type", "role");
		dm.push("perm");
		dm.add("access", access);
		dm.add("resource", new String[]{"type", resourceType}, resourceName);
		if (grant) {
			executor.execute("actor.grant", dm.root());
		} else {
			executor.execute("actor.revoke", dm.root());
		}
	}

	/**Grant or revoke a role  to a role
	 * 
	 * @param executor
	 * @param role role to grant the role to
	 * @param roleToApply the role to grant
	 * @param grant
	 * @throws Throwable
	 */
	static public void  grantRevokeRoleToRole (ServiceExecutor executor, String role,
			String roleToApply, Boolean grant) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", role);
		dm.add("type", "role");
		dm.add("role", new String[]{"type", "role"}, roleToApply);
		if (grant) {
			executor.execute("actor.grant", dm.root());
		} else {
			executor.execute("actor.revoke", dm.root());
		}
	}


	static public Boolean haveRole (ServiceExecutor executor, String role) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("role", new String[]{"type", "role"}, role);
		XmlDoc.Element r = executor.execute("actor.self.have", dm.root());
		return r.booleanValue("role");
	}

	/**
	 * FInd the server UUID of the calling system
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static String serverUUID (ServiceExecutor executor) throws Throwable {
		XmlDoc.Element r = executor.execute("server.uuid");
		return r.value("uuid");
	}


	public static XmlDoc.Element execute (ServiceExecutor executor, String serverUUID, String service,
			XmlDocMaker dm) throws Throwable {
		if (serverUUID==null) {
			return executor.execute(service, dm.root());
		} else {
			return executor.execute(new ServerRoute(serverUUID), service, dm.root());
		}

	}

	/**
	 * Get the current server date/time
	 */
	public static Date serverDate (ServiceExecutor executor) throws Throwable {
		XmlDoc.Element r = executor.execute("server.date");
		return r.dateValue("date");	
	}

	/**
	 * Populate a file as a CSV
	 * 
	 * 
	 * @param file
	 * @param columnHeadings
	 * @param rows The inner String[] represents the values for a row; must be of the same length as columnHeadings
	 * @throws Throwable
	 */
	public static void writeCSVFile (File file, String[] columnHeadings, Vector<String[]> rows) throws Throwable  {

		XmlFileOutput f = new XmlFileOutput(file);
		CSVUTF8Writer csv = new  CSVUTF8Writer(f);
		csv.setColHeadings(columnHeadings);
		for (String[] row : rows) {
			int n = row.length;
			for (int i=0; i<n; i++) {
				csv.addColumnValue(columnHeadings[i], row[i]);
			}
			csv.endRow();
		}
		csv.close();
	}


	/**
	 * Write a server-side XML file
	 * 
	 * @param url
	 * @param xe
	 * @throws Throwable
	 */
	public  static void writeServerSideXMLFile(File file, XmlDoc.Element xe) throws Throwable {
		if (file==null) {
			return;
		}
		//
		String str = xe.toString();
		ByteArrayInputStream bis = new ByteArrayInputStream(str.getBytes());
		//
		StreamCopy.copy(bis, file);
	}



	
	/**
	 * Write a client-side text file from a list of rows
	 * If the inputs is empty, you get an empty output file
	 * 
	 * @param output (get from e.g. PluginService.Outputs.output(i)
	 * @param rows
	 * @throws Throwable
	 */
	public static void writeClientSideFile (PluginService.Output output,  Collection<String> rows) throws Throwable  {

		if (output==null) {
			return;
		}
		StringBuilder sb = new StringBuilder();
		if (rows!=null && rows.size()>0 ) {
			for (String row : rows) {
				sb.append(row).append("\n");
			}
		}
		byte[] bytes = sb.toString().getBytes();
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		output.setData(bis, bytes.length, "text/plain");
	}



	/**
	 * Write a client side XML file
	 * @param output
	 * @param xe
	 * @throws Throwable
	 */
	public  static void writeClientSideXMLFile(PluginService.Output output, XmlDoc.Element xe) throws Throwable {
		String str = null;
		if (xe!=null) {
			str = xe.toString();
		}
		ByteArrayInputStream bis = new ByteArrayInputStream(str.getBytes());
		output.setData(bis, str.length(), "text/xml");
	}




	/**
	 * Send file to destination SCP server
	 * 
	 * @param executor
	 * @param host
	 * @param port if null not set (22 default)
	 * @param userName
	 * @param secureWalletKey
	 * @param fileURL of the form =file:<server side path>
	 * @param path The destination path. If null defaults to home directory. Just the path, no leading file:
	 * @return
	 * @throws Throwable
	 */
	public static void sendToSCP (ServiceExecutor executor, String host, String port, String userName,
			String secureWalletKey, String fileURL, String path) throws Throwable {

		// Send to scp
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("url", fileURL);
		dm.add("host", host);
		if (port!=null) dm.add("port", port);
		dm.push("user");
		dm.add("name", userName);
		dm.add("password-key",secureWalletKey);
		dm.pop();
		if (path!=null) {
			dm.add("output-file-name", path);
		}
		executor.execute("secure.shell.copy.to", dm.root());
	}


	/**
	 * Does the store exist ?
	 * 
	 * @param executor
	 * @param ServerRoute
	 * @param store
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  storeExists (ServiceExecutor executor, ServerRoute sr, String store) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", store);
		XmlDoc.Element r = executor.execute(sr, "asset.store.exists", dm.root());
		return r.booleanValue("exists");
	}


	/**
	 * Does the store policy exist ?
	 * 
	 * @param executor
	 * @param ServerRoute
	 * @param store
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  storePolicyExists (ServiceExecutor executor, ServerRoute sr, String storePolicy) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", storePolicy);
		XmlDoc.Element r = executor.execute(sr, "asset.store.policy.exists", dm.root());
		return r.booleanValue("exists");
	}

	/**
	 * Convert bytes to GB
	 * 
	 * @param bytes
	 * @return
	 * @throws Throwable
	 */
	public static String bytesToGBytes(String bytes) throws Throwable {
		Long s = Long.parseLong(bytes);
		Util.StorageUnit su = Util.StorageUnit.fromString("GB", StorageUnit.GB);
		return su.convertToString(s);
	}
}
