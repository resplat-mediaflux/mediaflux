/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;


public class NameSpaceUtil {


	/**
	 * Add the given meta-data document to the asset namespace
	 * Removes the "id" attribute before adding. Inherit status 
	 * is set to false
	 * 
	 * @param executor
	 * @param nameSpace
	 * @param meta
	 * @throws Throwable
	 */
	public static void addAssetNameSpaceMetaData (ServerRoute sr, ServiceExecutor executor, String nameSpace,
			XmlDoc.Element meta) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		//dm.add("inherit-asset-meta", false);
		dm.push("asset-meta", new String[]{"inherit", "false", "propagate", "false"});
		AssetUtil.removeAttribute(executor, meta, "id");
		// TODO remove workaround code below after arcitecta fixes: https://support.arcitecta.com/hc/en-us/requests/7153
		// start workaround - unexpected attributes for Collection/retention/review
		XmlDoc.Element review = meta.element("retention/review");
        if (review!=null) {
            if(review.attribute("tz")!=null) {
            	review.removeAttribute("tz"); 
            }
            if(review.attribute("gmt-offset")!=null) {
            	review.removeAttribute("gmt-offset"); 
            }
            if(review.attribute("dst")!=null) {
            	review.removeAttribute("dst"); 
            }
            if(review.attribute("millisec")!=null) {
            	review.removeAttribute("millisec"); 
            }
        }
		// end workaround
		dm.add(meta);
		dm.pop();
		executor.execute(sr, "asset.namespace.asset.meta.add", dm.root());
	}


	/**
	 * Does the namespace exist ?
	 * 
	 * @param executor
	 * @param serverUUID - the server to execute on. Null for localhost
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  assetNameSpaceExists (ServiceExecutor executor, String serverUUID, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = Util.execute(executor, serverUUID, "asset.namespace.exists", dm);
		return r.booleanValue("exists");
	}


	/**
	 * Create role namespace
	 * 
	 * @param name - name of namespace to create
	 * @param description - description of namespace
	 */
	static public void createRoleNameSpace (ServiceExecutor executor, 
			String name, String description) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", name);
		if (description!=null) {
			dm.add("description", description);
		}
		Util.execute(executor, null, "authorization.role.namespace.create", dm);
	}

	/**
	 * Create the namespace (and all required parents)
	 * 
	 * @param executor
	 * @param nameSpace
	 * @param store (can be null - inherit from parent)
	 * @param storePolicy (can be null - inherit from parent)
	 * @param visibleTo (may be null)
	 * @param description (may be null)
	 * @param ACLs (may be null)
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  createNameSpace (ServiceExecutor executor, String serverUUID, String nameSpace, 
			String store, String storePolicy, String visibleToRole, 
			String description, Collection<XmlDoc.Element> acls) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace",new String[]{"all", "true"},  nameSpace);
		if (store!=null && storePolicy!=null) {
			throw new Exception("You cannot give store and store policy");
		}
		if (store!=null) {
			dm.add("store", store);
		} 
		if (storePolicy!=null) {
			dm.add("store-policy", storePolicy);
		}
		if (visibleToRole!=null) {
			dm.push("visible-to");
			dm.add("actor", new String [] {"type","role"}, visibleToRole);
			dm.pop();
		}
		if (description!=null) {
			dm.add("description", description);
		}
		if (acls!=null) {
			for (XmlDoc.Element acl : acls) {
				dm.add(acl);
			}
		}
		XmlDoc.Element r = Util.execute(executor, serverUUID, "asset.namespace.create", dm);
		return r.booleanValue("exists");
	}

	/**
	 * Return the description of a namespace
	 * 
	 * @param executor
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	static public XmlDoc.Element describe (ServerRoute sr, ServiceExecutor executor, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = executor.execute(sr, "asset.namespace.describe", dm.root());
		return r;
	}


	/**
	 * Does the role namespace exist ?
	 * 
	 * @param executor
	 * @param serverUUID - the server to execute on. Null for localhost
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  roleNameSpaceExists (ServiceExecutor executor, String serverUUID, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = Util.execute(executor, serverUUID, "authorization.role.namespace.exists", dm);
		return r.booleanValue("exists");
	}




	/**
	 * List child namespaces.
	 * 
	 * @param executor
	 * @param nameSpace
	 * @param absolute returns the absolute namespace path, else relative to parent
	 * @return Always returns an object, may be zero length
	 * @throws Throwable
	 */
	static public Collection<String> listNameSpaces (ServiceExecutor executor, String nameSpace, Boolean absolute) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = executor.execute("asset.namespace.list", dm.root());
		String path = r.value("namespace/@path");
		//
		Vector<String> t = new Vector<String>();
		Collection<String> nss = r.values("namespace/namespace");
		if (nss==null) {
			return t;
		}
		//
		if (absolute) {
			if (nss!=null) {
				for (String ns : nss) {
					ns = path + "/" + ns;
					t.add(ns);
				}
			} 
		} else {
			t.addAll(nss);
		}
		return t;
	}


	/**
	 * Does the meta-data namespace exist ?
	 * 
	 * @param executor
	 * @param serverUUID - the server to execute on. Null for localhost
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  metaNameSpaceExists (ServiceExecutor executor, String serverUUID, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = Util.execute(executor, serverUUID, "asset.doc.namespace.exists", dm);
		return r.booleanValue("exists");
	}

	/**
	 * Get full path of the parent namespace
	 * 
	 */
	// Find the parent part of it
	static public String parentNameSpace (String assetNameSpace) throws Throwable {

		// We want the root in the form /parent/path with no trailing "/"
		String t = assetNameSpace;
		int l = assetNameSpace.length();
		int idx = assetNameSpace.lastIndexOf("/");
		if (idx == l-1) {
			t = assetNameSpace.substring(0, l-1);
		}

		// Now find the parent part
		idx = t.lastIndexOf("/");
		if (idx==-1) {
			return t;
		} else {
			return t.substring(idx-1);
		}
	}

	/**
	 * Remove a specific document from an asset namespace
	 * 
	 * @param executor
	 * @param nameSpace
	 * @param mid The document id
	 * @throws Thjrowable
	 */
	static public void removeAssetNameSpaceMetaData (ServerRoute sr, ServiceExecutor executor, String nameSpace, String mid) throws Throwable {
		if (mid==null) {
			throw new Exception ("Namespace meta-data fragment id ('mid') not specified");
		}
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		dm.add("mid", mid);
		executor.execute(sr, "asset.namespace.asset.meta.remove", dm.root());
	}

	/**
	 * Rename an asset namespace
	 * 
	 * @param executor
	 * @param oldName
	 * @param newName
	 * @throws Throwable
	 */
	public static String renameAssetNameSpace (ServiceExecutor executor, ServerRoute sr, String oldNameSpacePath, String newName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");

		// Strip off any leading / from the new name
		int idx = newName.indexOf("/");
		String newName2 = newName;
		if (idx>=0) {
			newName2 = newName.substring(idx+1);
		}
		dm.add("name", newName2);
		dm.add("namespace", oldNameSpacePath);
		// Renames just the child part to the new name
		executor.execute(sr, "asset.namespace.rename", dm.root());

		// Find the new path. The silly service does not tell us.
		idx = oldNameSpacePath.lastIndexOf("/");
		if (idx<0) {
			return "/" + newName2;
		} else {
			return oldNameSpacePath.substring(0,idx+1) + newName2;
		}
	}

	/**
	 * Rename a document namespace
	 * 
	 * @param executor
	 * @param oldName
	 * @param newName
	 * @param n  Number of assets to update per batch.  Null means 500
	 * @throws Throwable
	 */
	public static void renameDocumentNameSpace (ServerRoute sr, ServiceExecutor executor, String oldName, 
			String newName, Integer n) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("new-namespace", newName);
		dm.add("namespace", oldName);
		if (n!=null) dm.add("txn-size", n);
		executor.execute(sr, "asset.doc.namespace.rename", dm.root());
	}


	/**
	 * Rename a dictionary namespace
	 * 
	 * @param executor
	 * @param oldName
	 * @param newName
	 * @throws Throwable
	 */
	public static void renameDictionaryNameSpace (ServerRoute sr, ServiceExecutor executor, String oldName, 
			String newName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", newName);
		dm.add("namespace", oldName);
		executor.execute(sr, "dictionary.namespace.rename", dm.root());
	}


	/**
	 * Rename a role namespace
	 * 
	 * @param executor
	 * @param oldName
	 * @param newName
	 * @throws Throwable
	 */
	public static void renameRoleNameSpace (ServerRoute sr, ServiceExecutor executor, String oldNameSpace, String newNameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", newNameSpace);
		dm.add("namespace", oldNameSpace);
		executor.execute(sr, "authorization.role.namespace.rename", dm.root());
	}


	/**
	 * Recursively revoke all ACLs on namespaces and assets
	 * 
	 * @param executor
	 * @param nameSpace
	 * @throws Throwable
	 */
	public static void recursivelyRevokeAllACLs (ServiceExecutor executor, String nameSpace) throws Throwable {

		PluginTask.checkIfThreadTaskAborted();

		XmlDoc.Element nsMeta = NameSpaceUtil.describe(null, executor, nameSpace);
		if (nsMeta==null) return;

		// Self  first

		// Assets in current namesapce
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", "namespace='"+nameSpace+"'");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());

		// Iterate through assets
		Collection<String> ids = r.values("id");
		if (ids!=null) {
			for (String id : ids) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDoc.Element r2 = AssetUtil.getAsset(executor, null, id);
				if (r2!=null) {

					// Iterate through and revoke each ACL on this asset
					Collection<XmlDoc.Element> acls = r2.elements("asset/acl");
					if (acls!=null) {
						for (XmlDoc.Element acl : acls) {
							String actorName = acl.value("actor");
							String actorType = acl.value("actor/@type");
							XmlDocMaker dm2 = new XmlDocMaker("args");
							dm2.add("id", id);
							dm2.push("acl");
							dm2.add("actor", new String[]{"type", actorType}, actorName);
							dm2.pop();
							executor.execute("asset.acl.revoke", dm2.root());
						}
					}
				}
			}
		}

		// Revoke ACLs on current Namespace
		Collection<XmlDoc.Element> actors = nsMeta.elements("namespace/acl/actor");
		if (actors!=null) {
			for (XmlDoc.Element actor : actors) {
				PluginTask.checkIfThreadTaskAborted();
				MetaUtil.removeAttribute(actor, "id");
				NameSpaceUtil.revokeACL (executor, actor, nameSpace);		
			}
		}

		// Recurse into child namespaces
		Collection<String> namespaces = nsMeta.values("namespace/namespace");
		if (namespaces==null) return;
		String parentPath = nsMeta.value("namespace/path");
		for (String ns : namespaces) {
			PluginTask.checkIfThreadTaskAborted();
			String ns2 = parentPath + "/" + ns;
			revokeAllACLs (executor, ns2);
		}

	}

	/**
	 * Revoke all ACLs for actors in the current namespace
	 * 
	 * @param executor
	 * @param nameSpace
	 * @throws Throwable
	 */
	public static void revokeAllACLs (ServiceExecutor executor, String nameSpace) throws Throwable {
		XmlDoc.Element nsMeta = describe(null, executor, nameSpace);
		if (nsMeta==null) return;
		Collection<XmlDoc.Element> actors = nsMeta.elements("namespace/acl/actor");
		if (actors==null) return;
		for (XmlDoc.Element actor : actors) {
			MetaUtil.removeAttribute(actor, "id");
			revokeACL (executor, actor, nameSpace);		
		}
	}


	public static void revokeACL (ServiceExecutor executor, XmlDoc.Element actor, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		dm.add(actor);
		executor.execute("asset.namespace.acl.revoke", dm.root());
	}

	/**
	 * Set the given meta-data document to the asset namespace
	 * Removes the "id" attribute before adding. INherit status 
	 * is set to false
	 * 
	 * @param executor
	 * @param nameSpace
	 * @param elements Collection of Xml Documents to set for the namespace meta-data
	 * @throws Throwable
	 */
	public static void setAssetNameSpaceMetaData (ServerRoute sr, ServiceExecutor executor, String nameSpace,
			Collection<XmlDoc.Element> elements) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		dm.add("inherit-asset-meta", false);
		dm.push("asset-meta", new String[]{"inherit", "false", "propagate", "false"});
		for (XmlDoc.Element el : elements) {
			AssetUtil.removeAttribute(executor, el, "id");
			dm.add(el);
		}
		dm.pop();
		executor.execute(sr, "asset.namespace.asset.meta.set", dm.root());
	}


	/**
	 * Sum all content in namespace. Will return null if no content
	 * 
	 * @param executor
	 * @param peerUUID
	 * @param assetNameSpace
	 * @return
	 * @throws Throwable
	 */
	static public XmlDoc.Element sumContent (ServiceExecutor executor, String serverUUID, String assetNameSpace, String where) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (where==null) {
			dm.add("where", "namespace>='" + assetNameSpace+"'");
		} else {
			dm.add("where", "namespace>='" + assetNameSpace+"' and (" + where + ")");

		}
		dm.add("action", "sum");
		dm.add("xpath", "content/size");
		return Util.execute(executor, serverUUID, "asset.query", dm);
	}
}
