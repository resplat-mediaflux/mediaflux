/** 
 * Class to hold a project ID and related meta-data
 * Could be implemented as immutable if all information
 * was fetched first and passed in to the constructor
 * 
 * 
 * @author Neil Killeen
 *
 * Copyright (c) 2021, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;


import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;


public final class ProjectHolder {
	private  String id_ = null;
	private  String namespace_ = null;
	private  String namespaceDR_ = null;
	private  String collectionKey_ = null;
	private  ServiceExecutor executor_ = null;
	private  XmlDoc.Element meta_ = null;
	private  String collectionDocType_ = null;


	/**
	 * Constructor from project ID
	 * 
	 * @param executor 
	 * @param projectID - of the form proj-<name>-<cid> (no check is made, you 
	 *    must already have validated this).
	 * @param fetchNow - if true, then all the state describing
	 *    the project is fetched during the construction. If
	 *    false, the information is fetched on demand (the main one
	 *    that takes time is function metaData().  With this
	 *    'lazy' approach it is possible for the underlying
	 *    state in Mediaflux to change throughout the lifetime
	 *    of the ProjectHolder object which might then be
	 *    internally inconsistent.  This is very unlikely in the
	 *    context of how it is used in general (project namespaces
	 *    metadata changes only very occaisionally) 
	 *    
	 * @throws Throwable
	 */
	public ProjectHolder (ServiceExecutor executor, String projectID,
			Boolean fetchNow) throws Throwable {
		this.id_ = projectID;
		this.executor_ = executor;

		// If all the information  is fetched up front
		// then the state of the ProjectHolder object
		// is self-consistent for its lifetime.
		if (fetchNow) {
			setNameSpace();
			setMetaData();
			setCollectionKey();
			setNameSpaceDR();
		}
	}

	/**
	 * Fetch the project ID
	 * 
	 * @return
	 * @throws Throwable
	 */
	public String id () throws Throwable {
		return this.id_;
	}

	/**
	 * Fetch the project asset namespace name (comes from
	 * an underlying dictionary - once a value is set
	 * it never changes in the dictionary)
	 * 
	 * @return
	 * @throws Throwable
	 */
	public String nameSpace () throws Throwable {
		setNameSpace ();
		return this.namespace_;
	}

	/**
	 * Fetch the project collection key (from asset namespace meta-data)
	 * 
	 * @return
	 * @throws Throwable
	 */
	public String collectionKey () throws Throwable {
		setCollectionKey ();
		return this.collectionKey_;
	}

	
	/**
	 * Fetch the meta-data set on the project asset namespace
	 * 
	 * @return
	 * @throws Throwable
	 */
	public XmlDoc.Element metaData () throws Throwable {
		setMetaData();
		return this.meta_;
	}

	/**
	 * Fetch the immediate parent namespace to this project
	 * 
	 * @return
	 * @throws Throwable
	 */
	public String parentNameSpace () throws Throwable {
		setNameSpace();
		// Find the parent part of it
		int startIdx = this.namespace_.indexOf("proj-", 0);
		return this.namespace_.substring(0, startIdx-1);
	}

	/**
	 * Fetch the standard project namespace name on the DR system whether
	 * replications are configured or not.
	 * 
	 * @return
	 * @throws Throwable
	 */
	public String nameSpaceDR () throws Throwable {
		setNameSpaceDR();
		return this.namespaceDR_;
	}
	
	
	/**
	 * Fetch the owner (ARO in data registry) email address from the asset namespace meta-data
	 * 
	 * @return
	 * @throws Throwable
	 */
	public String ownerEmail () throws Throwable {
		setMetaData();
		String type = Properties.getCollectionDocType (executor_);
		return meta_.value("namespace/asset-meta/"+type+"/owner/email");	
	}

	/**
	 * Fetch the executor for convenience
	 * 
	 * @return
	 */
	public ServiceExecutor executor() {
		return this.executor_;
	}

	
	
	private void setMetaData () throws Throwable {
		if (this.meta_==null) {
			setNameSpace();
			this.meta_ = NameSpaceUtil.describe(null, this.executor_, this.namespace_);
		}		
	}

	private void setNameSpace () throws Throwable {
		if (this.namespace_==null) {
			this.namespace_ = Project.assetNameSpace(this.executor_, this.id_, false);
		}
	}

	private void setCollectionKey() throws Throwable {
		if (this.collectionKey_==null) {
			if (collectionDocType_==null) {
			   collectionDocType_ = Properties.getCollectionDocType(this.executor_);	
			}
			setMetaData();
			this.collectionKey_ = this.meta_.value("namespace/asset-meta/"+collectionDocType_+"/key");
		}
	}

	private void setNameSpaceDR () throws Throwable {
		if (this.namespaceDR_==null) {
			this.namespaceDR_ = Project.assetNameSpace(this.executor_, this.id_, true);
		}
	}

}
