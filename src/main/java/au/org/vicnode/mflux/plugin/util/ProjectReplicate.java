/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginService.Outputs;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;


public class ProjectReplicate {

	// Leading filter predicate on replication processes. We don't want to
	// consider assets with content but no checksum, still in staging 
	//
	// New symlinks (first class objects) have type='posix/symlink'
	// but have no content. Old style do have content but are no
	// longer created so we no longer attempt to filter them out.
	/*
	 public static String REPLICATION_FILTER =
			"not(content is staging) and " +
			"((asset has content and asset has checksum) or (asset hasno content))";
	 */
	// We must use FAQL as much as is possible to simply just evaluate the
	// meta-data of the asset rather than doing some heavy-weight 
	// data base query. The staging one should be quick as there won't be
	// many assets in staging.  Can probably drop the function(xvalue('content/size')
	// since if it has a checksum it must have content
	/*
	public static String REPLICATION_FILTER = "not(content is staging) and " +
			"((function(xvalue('content/size')) and asset has checksum) or " +
			"(function(!xvalue('content/size'))))";
	*/
	
	// From 4.14.013 this function is all we need to filter out
	// assets not suitable for replication
	public static String REPLICATION_FILTER = "function(asset.is.ready.to.copy())";


	// The name of the :enqueue element of a namespace after
	// the replication queue is added to it
	private static final String ENQUEUE_NAME= "Replication";

	/**
	 * Return a ServerRoute for the DR Server
	 * 
	 * @return
	 * @throws Throwable
	 */
	public static ServerRoute DRServerRoute (ServiceExecutor executor) throws Throwable {
		ServerRoute sr = new ServerRoute(DRServerUUID(executor));
		return sr;
	}

	/**
	 * Return DR server UUID
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static String DRServerUUID (ServiceExecutor executor) throws Throwable {
		return Properties.getApplicationProperty(executor, Properties.APPLICATION_PROPERTY_DR_UUID,
				Properties.APPLICATION_PROPERTY_APP);
	}


	/**
	 * Return DR server peer name
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static String DRServerPeerName (ServiceExecutor executor) throws Throwable {
		return Properties.getApplicationProperty(executor, Properties.APPLICATION_PROPERTY_DR_PEER_NAME,
				Properties.APPLICATION_PROPERTY_APP);
	}


	/**
	 * See if we are configured for DR.
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static Boolean isDRConfigured (ServiceExecutor executor) throws Throwable {

		// We need a server
		if (DRServerPeerName(executor)==null ||
			DRServerPeerName(executor).equals("none")) {
					return false;
		}
		if (DRServerUUID(executor)==null ||
			DRServerUUID(executor).equals("none")) {
			return false;
		}
		return true;		
	}


	/**
	 * This function simple prefixes the asset namespace  withe given UUID
	 * This is our pattern for DR systems, that the receiving root namespace for a particular
	 * primary server is prefixed with the primary UUID 
	 * 
	 * @param executor
	 * @param serverUUID
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	public static String makeNameSpaceNameForDR (ServiceExecutor executor, String nameSpace) throws Throwable {
		String serverUUID = Util.serverUUID(executor);
		String t = nameSpace;
		if (!nameSpace.startsWith("/")) {
			t = "/" + nameSpace;
		}
		return "/" + serverUUID + t;
	}

	/**
	 * Create the standard replication asset processing queue.
	 * If the queue already exists does nothing
	 * 
	 * @param executor
	 * @param w - XmlWriter can be null for no output
	 * @return the name of the processing queue
	 * @throws Throwable
	 */
	public static String createOrFindStandardQueue (ServiceExecutor executor, 
			XmlWriter w) throws Throwable {

		// Nothing to do if exists
		String queueName = getProjectStandardQueueName(executor);
		if (AssetProcessingQueue.queueExists(executor, queueName)) {
			if (w!=null) {
				w.add("processing-queue", new String[] {"exists", "true"}, queueName);			
			}
			return queueName;
		}

		// Make the queue
		String primaryServerUUID = Util.serverUUID(executor);
		String drServerName = Properties.getApplicationProperty(executor, Properties.APPLICATION_PROPERTY_DR_PEER_NAME, Properties.APPLICATION_PROPERTY_APP);
		String nbProcessors = Properties.getApplicationProperty(executor, Properties.APPLICATION_PROPERTY_REPLICATION_QUEUE_NB_PROCESSORS, Properties.APPLICATION_PROPERTY_APP);
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("description", "Queue to handle project asset replications");
		dm.add("name", queueName);
		dm.add("nb-processors", nbProcessors);
		dm.add("processing-delay", "10000");     // 10s - stop empty MKNOD assets replicating (see ticket 4729).
		dm.push("processing-service", new String[] {"name", "asset.replicate.to"});
		dm.add("allow-incomplete-metadata", true);
		dm.add("allow-move", true);   // Allow namespace moves
		dm.add("auto-name", true);    // Allow replicats to get renamed and versioned
		dm.add("check-if-required", true);
		dm.add("clevel", "0");        // No compression
		dm.add("cmode", "push");
		dm.add("content-must-have-checksum", true);     // Staged assets will have no checksum
		dm.add("if-staging", "skip");                   // Default is error
		dm.add("create-roles", true);    // Will create roles for ACLs (they are placeholders only, don't contain permissions)
		dm.add("dst", new String[]{"create", "false"}, primaryServerUUID);         // Destination namespace parent is sever UUID
		dm.add("include-components", false);   
		dm.add("include-destroyed", true);
		dm.add("keepacls", true);                                          // Keep asset ACLs
		dm.add("overwrite", false);        // we get <file>_1, <file>_2 etc.  
		if (drServerName==null) {
			throw new Exception ("DR Peer name is null");
		}
		dm.push("peer");
		dm.add("name", drServerName);
		dm.pop();
		// dm.add("peer", drServerName);

		dm.add("related", "0");      // The query has to find everything
		// dm.add("retry", 5);          // Hard to know what to use really...
		dm.add("update-doc-types", false);  
		dm.add("update-models", false);
		dm.add("versions", "all");
		dm.pop();

		// Both the processing service and the queue take a user argument
		dm.push("user");
		dm.add("domain",Properties.REPLICATOR_DOMAIN);
		dm.add("name", Properties.REPLICATOR_USER);
		dm.pop();
		//
		executor.execute("asset.processing.queue.create", dm.root());
		if (w!=null) {
			w.add("processing-queue", new String[] {"exists", "false"}, queueName);
		}
		return queueName;
	}


	/**
	 * Look for an enqueue element in the asset namespace meta-data
	 * and if it is the specialised replication one, return the enqueue element
	 * Otherwise return null
	 * 
	 * @param nsMeta - the meta-data describing the asset namespace
	 */
	public static XmlDoc.Element getStandardEnqueueElement (XmlDoc.Element nsMeta) throws Throwable {
		return AssetProcessingQueue.getEnqueueElement(nsMeta, ENQUEUE_NAME);
	}

	/**
	 * High-level function to establish if a project is being replicated
	 *   by an asset processing queue 
	 * 
	 * @param ph ProjectHolder
	 * @return - 
	 *  If not being replicated returns null
	 *  If being replicated, returns the asset processing queue name
	 * @throws Throwable
	 */
	public static String isProjectReplicated (ProjectHolder ph) throws Throwable {

		XmlDoc.Element nsMeta = ph.metaData();

		// Asset processing queue
		// Look for associated asset processing queue
		XmlDoc.Element enq = getStandardEnqueueElement(nsMeta);
		if (enq!=null) {
			return enq.value("queue");
		} 
		//
		return null;
	}

	/**
	 * Set the standard (singular) replication asset processing queue on a project parent namespace
	 * If the queue does not pre-exist, creates it.
	 * 
	 * @param executor
	 * @param projectID
	 * @throws Throwable
	 */
	public static void setStandardQueueOnProject (ServiceExecutor executor, String projectID, XmlWriter w) throws Throwable {
		String namespace = Project.assetNameSpace(executor, projectID, false);
		String queueName = createOrFindStandardQueue (executor, null);	
		AssetProcessingQueue.addQueueToNamespace (executor, 
				namespace, queueName, ENQUEUE_NAME,
				ProjectReplicate.REPLICATION_FILTER);
		w.add("replication-queue", new String[] {"namespace", namespace, "set", "true", "enqueue-name", ENQUEUE_NAME}, queueName);
	}

	/**
	 * Remove the standard replication  asset processing queue from a project parent namespace
	 * 
	 * @param executor
	 * @param projectID
	 * @throws Throwable
	 */
	public static void unsetStandardQueueOnProject (ServiceExecutor executor, String projectID, XmlWriter w) throws Throwable {
		String namespace = Project.assetNameSpace(executor, projectID, false);
		String queueName = createOrFindStandardQueue (executor, null);
		AssetProcessingQueue.removeQueueFromNamespace (executor, 
				namespace,  ENQUEUE_NAME);
		w.add("replication-queue", new String[] {"namespace", namespace, "unset", "true", "enqueue-name", ENQUEUE_NAME}, queueName);
	}


	/**
	 * Return the name of the standard provisioning asset processing queue for replications
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static String getProjectStandardQueueName (ServiceExecutor executor) throws Throwable {
		String drServerUUID = ProjectReplicate.DRServerUUID (executor);
		return "replicate-projects-to-"+drServerUUID;
	}

	/**
	 * Fetch the errors from the asset processing queue and handle the case where the asset is missing the content of one of its versions.
	 * These assets were introduced when the server had a open filehandle leak, causing the server to overrun its limit and get into a state
	 * where it could no longer allocate new filehandles. This caused the server to fail to write the content of the asset to disk, but an asset
	 * version could be created in the database. This asset version is now stuck in the processing queue, and the asset cannot replicate.
	 * 
	 * For each entry, we see if the version that is missing content is the latest or not.
	 * If there is a newer version, we can remove the content entry for the missing file on disk using asset.content.remove
	 * 
	 * If the latest version of the asset is the one with the missing content, then we need to inform the user.
	 * * Write it to a CSV file
	 * * Remove it from the asset processing queue: asset.processing.queue.entry.remove
	 * 
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static void handleEntriesWithMissingContent (ServiceExecutor executor, Outputs outputs, String name, String size, Boolean apply, XmlWriter w) throws Throwable {

		XmlDocMaker dm0 = new XmlDocMaker("args");
		dm0.add("name", name);
		XmlDoc.Element r0 = executor.execute("asset.processing.queue.entry.count", dm0.root());
		// if there are fewer entries in the processing queue, set that as the number of tasks to be done, otherwise use size.
		long longSize=Long.valueOf(size);
		long longCount=r0.element("count").longValue();
		PluginTask.threadTaskBeginSetOf((longSize<longCount) ? longSize : longCount);

		XmlDocMaker dm1 = new XmlDocMaker("args");
		dm1.add("name", name);
		dm1.add("size", size);
		XmlDoc.Element r1 = executor.execute("asset.processing.queue.entry.describe", dm1.root());
		if (null==r1 || null==r1.elements()) {
			w.add(new XmlDoc.Element("entries", "0"));
			return ;
		}
		PluginLog log = PluginLog.log("replication-fixes.log");
		Collection<String> failed_assets = new ArrayList<String>();
		for (XmlDoc.Element e : r1.elements("entry")) {
			int assetid = e.intValue("asset");
			int entryid = e.intValue("@id");

			XmlDocMaker dm2 = new XmlDocMaker("args");
			dm2.add("all-versions", "true");
			dm2.add("id", assetid);
			XmlDoc.Element r2;
			r2 = executor.execute("asset.content.validate", dm2.root());
			if (r2==null) {
				continue;
			}
			// get missing version(s)
			List<Integer> missingVersions = new LinkedList<Integer>();
			for (XmlDoc.Element e2 : r2.elements()) {
				if(e2.stringValue("status").equals("missing")) {
					missingVersions.add(e2.intValue("@version"));
					//w.add(e2);
				}
			}
			String versions = missingVersions.toString();
			log.add(PluginLog.WARNING, String.format("assetid: %s, versions: %s, entryid: %s", assetid, versions, entryid));

			// get latest version
			XmlDocMaker dm3 = new XmlDocMaker("args");
			dm3.add("id", assetid);
			dm3.add("xpath", new String[] {"ename", "version"}, "@version");
			dm3.add("xpath", new String[] {"ename", "path"}, "path");
			XmlDoc.Element r3 = executor.execute("asset.get", dm3.root());
			int latestVersion = r3.intValue("version");
			String path=r3.value("path");
			if (missingVersions.contains(latestVersion)) {
				// if missing version is the latest version, add to the CSV file
				failed_assets.add (String.format("%s,%s,%s", assetid, latestVersion, path));
				log.add(PluginLog.WARNING, String.format("Latest version missing: assetid: %s, version: %s, path: %s", assetid, latestVersion, path));
				if(apply) {
					// remove the entry from the processing queue
					log.add(PluginLog.WARNING, String.format("asset.processing.queue.entry.remove :name %s :id %s",name, entryid));
					XmlDocMaker dm4 = new XmlDocMaker("args");
					dm4.add("name", name);
					dm4.add("id", entryid);
					executor.execute("asset.processing.queue.entry.remove", dm4.root());
				}
			}
			else if (missingVersions.size() > 0){
				// if missing versions does not include the latest version, prune them
				for ( Integer missingVersion: missingVersions ) {
					log.add(PluginLog.WARNING, String.format("asset.prune :id %s :version %s",assetid, missingVersion));
				    XmlDocMaker dm4 = new XmlDocMaker("args");
				    dm4.add("id", assetid);
				    dm4.add("version", missingVersion);
				    if(apply) {
				    	XmlDoc.Element r4 = executor.execute("asset.prune", dm4.root());
				    	for (XmlDoc.Element e4 : r4.elements("pruned")) {
							e4.add(new XmlDoc.Attribute("version", missingVersion));
				    		w.add(e4);
				    	}
				    }
				    else {
				    	XmlDocMaker notPruned = new XmlDocMaker();
				    	notPruned.push("not-pruned", new String[] {"id", String.valueOf(assetid), "version", String.valueOf(missingVersion)});
				    	w.add(notPruned.root());
				    }
				}
				log.add(PluginLog.WARNING, String.format("asset.processing.queue.entry.reexecute :name %s :asset-id %s :fail-on-error false",name, assetid));
				if (apply) {
					//reexecute the processing queue entry
					//asset.processing.queue.entry.reexecute :name <name> :asset-id <id> :fail-on-error false
					XmlDocMaker dm5 = new XmlDocMaker("args");
					dm5.add("name", name);
					dm5.add("asset-id", assetid);
					dm5.add("fail-on-error", "false");
					if (apply) {
						executor.execute("asset.processing.queue.entry.reexecute", dm5.root());
					}
				}
			}

			PluginTask.threadTaskCompleted();
			PluginTask.checkIfThreadTaskAborted();
		}
		PluginTask.threadTaskEndSet();
		if( outputs != null && outputs.size() == 1) {
			// remove duplicates and write to CSV file
			Set<String> set = new HashSet<>(failed_assets);
			failed_assets.clear();
			failed_assets.add("assetid,version");
			failed_assets.addAll(set);
			Util.writeClientSideFile(outputs.output(0), failed_assets);
		}
	}

	/**
	 * Generate a TCL script to be run on the DR server to remove assets that
	 * have already replicated but are now attempting to re-replicate but cannot
	 * due to already existing at a different path.
	 * 
	 * The test is something along the lines of:
	 *   * Error message stating that an asset already exists with the same path
     *   * Asset exists on remote side
     *   * It has a name history that shows the parent has been destroyed
	 *     * asset.get :get-name-history true
     *   * The path is similar (regexp?)
	 *
	 * Do server.log of user details
	 */
	public static void generateTCLScriptToCleanUpExistingAssetsPath (ServiceExecutor executor, Outputs outputs, String name, String size, XmlWriter w) throws Throwable {

		// Get the DR server UUID
		String drServerUUID = ProjectReplicate.DRServerUUID (executor);

		// Get the DR server peer name
		//String drServerName = ProjectReplicate.DRServerPeerName (executor);

		// Get the DR server route
		ServerRoute sr = ProjectReplicate.DRServerRoute(executor);

		// Get the primary server UUID
		String primaryServerUUID = Util.serverUUID(executor);

		PluginLog log = PluginLog.log("replication-fixes.log");

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		dm.add("size", size);
		XmlDoc.Element r = executor.execute("asset.processing.queue.entry.describe", dm.root());
        if (r==null) {
            return ;
        }
        Collection<String> tcl_commands = new ArrayList<String>();
		tcl_commands.add ("#destroy commands - to be run on dr server");
        Collection<String> reexecute_commands = new ArrayList<String>();
        Collection<String> user_info = new ArrayList<String>();
		user_info.add("#User information - not to be run on any server");
		for (XmlDoc.Element e : r.elements("entry")) {
			//w.add(e);
			//int version = e.intValue("asset/@version");
			int assetid = e.intValue("asset");
			int entryid = e.intValue("@id");

			// attempt to replicate, and catch the error if there is one.
			// asset.replicate.to :allow-incomplete-metadata "true" :allow-move "true" :auto-name "true" :check-if-required "true" :clevel "0" :cmode "push" :content-must-have-checksum "true" :if-staging "skip" :create-roles "true" :dst -create "false" "1128" :include-components "false" :include-destroyed "true" :keepacls "true" :overwrite "false" :peer < :name "UoM-DR-Server2" > :related "0" :update-doc-types "false" :update-models "false" :versions "all" :id 1320773491
			XmlDocMaker dm2 = new XmlDocMaker("args");
			dm2.add("allow-incomplete-metadata", "true");
			dm2.add("allow-move", "true");
			dm2.add("auto-name", "true");
			dm2.add("check-if-required", "true");
			dm2.add("clevel", "0");
			dm2.add("cmode", "push");
			dm2.add("content-must-have-checksum", "true");
			dm2.add("if-staging", "skip");
			dm2.add("create-roles", "true");
			dm2.add("dst", new String[] {"create", "false"}, "1128");
			dm2.add("include-components", "false");
			dm2.add("include-destroyed", "true");
			dm2.add("keepacls", "true");
			dm2.add("overwrite", "false");
			dm2.push("peer");
			dm2.add("name", "UoM-DR-Server2");
			dm2.pop();
			dm2.add("related", "0");
			dm2.add("update-doc-types", "false");
			dm2.add("update-models", "false");
			dm2.add("versions", "all");
			dm2.add("id", assetid);
			String error_message="";
			try {
				XmlDoc.Element r2 = executor.execute("asset.replicate.to", dm2.root());
			}
			catch (Throwable t) {
				error_message = t.getMessage();
			}

			// If the error message matches the pattern we are looking for, then compare the asset paths on both sides.
			//Cannot restore asset (/1128/projects/proj-5530_taste-1128.4.853/AU11pt2/TAST_AU110010_AKB948312/2016B0058219_AK12016B0058219_-1717869186/CT_15_CTA__-334104846/SIMON_ISTVAN_MR._AKB948312_20160719T193400_CTAAxialCTA_Axial_15_000195.dcm): The namespace /1128/projects/proj-5530_taste-1128.4.853/TasteDB/data/TAST_AU110010_AKB948312/2016B0058219_AK12016B0058219_-1717869186/CT_15_CTA__-334104846 already contains an asset named 'SIMON_ISTVAN_MR._AKB948313_20160719T193400_CTAAxialCTA_Axial_15_000195.dcm'
			Pattern pattern = Pattern.compile(String.format("Cannot restore asset \\(/%s(/projects/proj-.*?)\\): The namespace /%s.*? already contains an asset named ",primaryServerUUID,primaryServerUUID));
			Matcher matcher = pattern.matcher(error_message);
			String path=null;
			if (matcher.find()) {
				path = matcher.group(1);
			}
			//tcl_commands.add("#path found: "+path);
			if (path != null) {
				XmlDocMaker dm3 = new XmlDocMaker("args");
				dm3.add("id", assetid);
				dm3.add("get-name-history", "true");
				XmlDoc.Element r2 = executor.execute("asset.get", dm3.root());
				String prodPath="/"+primaryServerUUID+r2.stringValue("asset/path");
				//tcl_commands.add("#prodPath: "+prodPath);
				Boolean prevPathDeleted = r2.booleanValue("asset/name-history/previous/namespace/@destroyed");

				XmlDocMaker dm4 = new XmlDocMaker("args");
				dm4.add("id", String.format("rid=%s.%d", primaryServerUUID, assetid));
				XmlDoc.Element r3 = executor.execute( sr, "asset.get", dm4.root() );
				String drPath=r3.stringValue("asset/path");
				//tcl_commands.add("#drPath: "+drPath);

				List<String> prodSplit = new LinkedList<String>(Arrays.asList(prodPath.split("/")));
				List<String>   drSplit = new LinkedList<String>(Arrays.asList(drPath.split("/")));

				//prodSplit.remove(prodSplit.size()-1);
				//drSplit.remove(drSplit.size()-1);
				//drSplit.remove(drSplit.size()-1);
				List<String> prodSub= prodSplit.subList(0,4);
				List<String>   drSub=   drSplit.subList(0,4);

				// If the paths are similar and the prevPath of the asset is deleted, then we can delete the asset on the DR side.
				if( prodSub.containsAll(drSub) && drSub.containsAll(prodSub) && prevPathDeleted ){
					tcl_commands.add(String.format("asset.destroy :id rid=1128.%s", assetid));
					reexecute_commands.add("asset.processing.queue.entry.reexecute :name "+name+" :id "+entryid);
					//log.add(PluginLog.DEBUG, String.format("Asset id: %s User: %s:%s %s <%s>", assetid, r2.stringValue("asset/creator/domain"),
					//		r2.stringValue("asset/creator/user"),r2.stringValue("asset/creator/name"),r2.stringValue("asset/creator/email")));
					user_info.add(String.format("#Asset id: %s User: %s:%s %s <%s>", assetid, r2.stringValue("asset/creator/domain"),
							r2.stringValue("asset/creator/user"),r2.stringValue("asset/creator/name"),r2.stringValue("asset/creator/email")
						));
					user_info.add(String.format("#    Path: %s", prodPath));
					user_info.add(String.format("#  DRPath: %s", drPath));
				}
			}

			/*
			tcl_commands.add(String.format("asset.destroy :id rid=1128.%s", assetid));
			//XmlDoc.Element s = executor.execute(String.format("server.peer.execute :peer %s :service asset.get :args < :id rid=%s.%d >",drServerUUID, primaryServerUUID, assetid));
			XmlDocMaker dm2 = new XmlDocMaker("args");
			dm2.add("id", String.format("rid=%s.%d", primaryServerUUID, assetid));
			XmlDoc.Element s = executor.execute( sr, "asset.get", dm2.root() );
			int drVersion=s.intValue("asset/@version");
			w.add("version", version);
			w.add("dr-version", drVersion);
			*/
			PluginTask.checkIfThreadTaskAborted();
		}
		if(outputs.size() == 1) {
			tcl_commands.add ("#reexecute commands - to be run on primary server");
			tcl_commands.addAll(reexecute_commands);
			tcl_commands.addAll(user_info);
			Util.writeClientSideFile(outputs.output(0), tcl_commands);
		}
	}

	/**
	 * Generate a TCL script to be run on the DR server to remove assets that
	 * have already replicated but are now attempting to re-replicate but cannot
	 * due to already existing according to its rid.
	 * 
	 * In this case, we can simply delete the asset on the DR side.
	 * 
	 * We pull all of the stack traces from the asset processing queue and
	 * search for the text "The replicant citeable identifier 1128.<id> has
	 * already been used by asset (id) <dr-id>" and generate a TCL script to
	 * delete these assets on the DR side based on the rid.
	 */
	//public static List<XmlDoc.Element> generateTCLScriptToCleanUpExistingAssets (ServiceExecutor executor, String name, XmlWriter w) throws Throwable {
	public static void generateTCLScriptToCleanUpExistingAssetsRid (ServiceExecutor executor, Outputs outputs, String name, String size, XmlWriter w) throws Throwable {
		// Get the DR server UUID
		String drServerUUID = ProjectReplicate.DRServerUUID (executor);

		// Get the DR server peer name
		//String drServerName = ProjectReplicate.DRServerPeerName (executor);

		// Get the DR server route
		//ServerRoute sr = ProjectReplicate.DRServerRoute(executor);

		// Get the primary server UUID
		String primaryServerUUID = Util.serverUUID(executor);

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		dm.add("max-nb-failures", size);
		dm.add("display-full-stack-trace", "true");
		XmlDoc.Element r = executor.execute("asset.processing.queue.describe", dm.root());
        if (r==null) {
            return ;
        }
        Collection<String> tcl_commands = new ArrayList<String>();
		for (XmlDoc.Element e : r.elements("queue/processing/error")) {
			//w.add(e);
			//String stack = e.stringValue("stack");
			//The replicant citeable identifier 1128.1364114460 has already been used by asset (id) 1412409569
			Pattern pattern = Pattern.compile(String.format("The replicant citeable identifier %s.(\\d+) has already been used by asset \\(id\\) (\\d+)",primaryServerUUID));
			Matcher matcher = pattern.matcher(e.stringValue("stack"));
			if (matcher.find()) {
				String id = matcher.group(1);
				String dr_id = matcher.group(2);
				//w.add(String.format("Found match: 1128.%s dr_id:%s", id, dr_id));
				//w.add("action", "destroy");
				//w.add("rid", matcher.group(1));
				//w.add("dr-id", matcher.group(2));
				//tcl_commands.add(String.format("Found match: 1128.%s dr_id:%s", id, dr_id));
				tcl_commands.add(String.format("asset.destroy :id rid=1128.%s", id));
				
			}
			PluginTask.checkIfThreadTaskAborted();
		}
		Util.writeClientSideFile(outputs.output(0), tcl_commands);
	}
}
