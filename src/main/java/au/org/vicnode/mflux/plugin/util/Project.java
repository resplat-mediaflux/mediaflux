/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;

import java.util.*;

import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class Project {

	// In all that follows, the "ProjectID" is the project name (argument :name) in
	// the asset.project.create service
	// This ID is used to name all the namespaces (asset, role, doc, dictionary) for
	// the new project
	// It is also used for the posix identity map and mount point.

	// A public enumerator that holds a list of replication job names.
	public static enum ProjectRoleType {
		ADMIN, A, ACM, ACMD, ACMDN,RCPUSR;

		// Method to convert to actual role name from enum
		public String fromEnum() throws Throwable {
			switch (this) {
			case ADMIN:
				return "administrator";
			case A:
				return "participant-a";
			case ACM:
				return "participant-acm";
			case ACMD:
				return "participant-acmd";
			case ACMDN:
				return "participant-acmd-n";
				case RCPUSR:
					return "administrator-rcp";
			}
			throw new Exception("Unhandled role type");
		}

		/**
		 * COnvert from string to enum
		 * 
		 * @param type
		 * @return
		 * @throws Throwable
		 */
		public static ProjectRoleType toEnum(String type) throws Throwable {
			if (type.equals("administrator")) {
				return ProjectRoleType.ADMIN;
			} else if (type.equals("participant-a")) {
				return ProjectRoleType.A;
			} else if (type.equals("participant-acm")) {
				return ProjectRoleType.ACM;
			} else if (type.equals("participant-acmd")) {
				return ProjectRoleType.ACMD;
			} else if (type.equals("participant-acmd-n")) {
				return ProjectRoleType.ACMDN;
			} else if (type.equals("administrator-rcp")) {
				return ProjectRoleType.RCPUSR;
			} else {
				throw new Exception("Unhandled project role " + type);
			}
		}

		public static String[] projectRoleTypes() {
			return new String[] { "administrator", "participant-a", "participant-acm", "participant-acmd",
			"participant-acmd-n","administrator-rcp" };
		}

		public static ProjectRoleType[] projectRoles() {
			return new ProjectRoleType[] { ProjectRoleType.ADMIN, ProjectRoleType.A, ProjectRoleType.ACM, ProjectRoleType.ACMD, ProjectRoleType.ACMDN };
		}

	}

	public static enum ProjectRCPRoleType {
		RCPUSR;

		// Method to convert to actual role name from enum
		public String fromEnum() throws Throwable {
			switch (this) {
				case RCPUSR:
					return "administrator-rcp";
			}
			throw new Exception("Unhandled role type");
		}

		/**
		 * COnvert from string to enum
		 *
		 * @param type
		 * @return
		 * @throws Throwable
		 */
		public static ProjectRoleType toEnum(String type) throws Throwable {
			if (type.equals("administrator-rcp")) {
				return ProjectRoleType.RCPUSR;
			} else {
				throw new Exception("Unhandled project role " + type);
			}
		}

		public static String[] projectRCPRoleTypes() {
			return new String[] {  "administrator-rcp" };
		}
	}

	/**
	 * This role provides read-only access to all Project namespaces It's intended
	 * for administration purposes
	 * 
	 * @return
	 * @throws Throwable
	 */
	public static String accessAssetNamespaceRoleName(ServiceExecutor executor) throws Throwable {
		return Properties.getAdminRoleNameSpace(executor) + ":project-access";
	}

	/**
	 * This role provides administrator access to all Project asset namespaces. It's
	 * intended for administration purposes, generally held by specialised services
	 * 
	 * @return
	 * @throws Throwable
	 */
	public static String administerAssetNamespaceRoleName(ServiceExecutor executor) throws Throwable {
		return Properties.getAdminRoleNameSpace(executor) + ":project-asset-namespace-administrator";
	}

	/**
	 * This role provide admin access to all Project role namespaces It's intended
	 * for administration purposes
	 * 
	 * @return
	 * @throws Throwable
	 */
	public static String roleNameSpaceAdminRoleName(ServiceExecutor executor) throws Throwable {
		return Properties.getAdminRoleNameSpace(executor) + ":project-role-namespace-administrator";
	}

	/**
	 * Return the actual Project role name from the project name and logical project
	 * role type
	 * 
	 * @param projectID
	 * @param projectRoleType
	 * @return
	 */
	static public String actualRoleName(String projectID, Project.ProjectRoleType projectRoleType) throws Throwable {
		return projectID + ":" + projectRoleType.fromEnum();
	}

	/**
	 * Return the actual Project role name from the project name and logical project
	 * rcp role type
	 *
	 * @param projectID
	 * @param projectRoleType
	 * @return
	 */
	static public String actualRCPRoleName(String projectID, Project.ProjectRCPRoleType projectRoleType) throws Throwable {
		return projectID + ":" + projectRoleType.fromEnum();
	}

	/**
	 * Return the actual Project role name from the project name and logical project
	 * role type
	 *
	 * @param projectID
	 * @param projectRoleType
	 * @return
	 */
	static public String actualRoleName(String projectID, String projectRoleType) throws Throwable {
		return projectID + ":" + projectRoleType;
	}

	/**
	 * Return a vector of actual role names for the given project
	 * 
	 * @param projectName
	 * @return
	 */
	static public Vector<String> actualRoleNames(String projectName) throws Throwable {
		return getActualRoleNames(projectName, Arrays.asList(ProjectRoleType.values()));
	}

	/**
	 * Return a vector of user role names for the given project
	 *
	 * @param projectName
	 * @return
	 */
	static public Vector<String> actualUserRoleNames(String projectName) throws Throwable {
		return getActualRoleNames(projectName, Arrays.asList(ProjectRoleType.projectRoles()));
	}

	/**
	 * Return a vector of actual role names for the given project and roles
	 *
	 * @param projectName
	 * @return
	 */
	static public Vector<String> getActualRoleNames(String projectName, Collection<ProjectRoleType> roleTypes) throws Throwable {
		Vector<String> roles = new Vector<String>();
		for (ProjectRoleType type : roleTypes) {
			roles.add(Project.actualRoleName(projectName, type));
		}
		return roles;
	}

	/**
	 * Allocates (or returns) the citeable ID root for provisioned Projects
	 * 
	 * @param executor
	 * @param type     "production' or 'test'
	 * @return
	 * @throws Throwable
	 */
	static private String allocateProjectRootID(ServiceExecutor executor, String type) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (type.equals(Properties.PROJECT_TYPE_PROD) || type.equals(Properties.PROJECT_TYPE_TEST)) {
			dm.add("name", Properties.CID_ALLOCATOR_ROOT);
		} else {
			throw new Exception("Unrecognised project type '" + type + "'. Must be '" + Properties.PROJECT_TYPE_PROD
					+ "' or '" + Properties.PROJECT_TYPE_TEST + "'");
		}

		XmlDoc.Element r = executor.execute("citeable.named.id.create", dm.root());
		return r.value("cid");
	}

	/**
	 * Allocates the citeable ID of the new Project
	 * 
	 * @param executor
	 * @param cidRoot
	 * @return
	 * @throws Throwable
	 */
	static private String allocateProjectID(ServiceExecutor executor, String cidRoot) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("pid", cidRoot);
		XmlDoc.Element r = executor.execute("citeable.id.create", dm.root());
		return r.value("cid");
	}

	/**
	 * Add user to project.
	 * 
	 * @param executor
	 * @param projectID
	 * @param user      " <domain>:<user>
	 * @param roleType
	 * @throws Throwable
	 */
	static public void addUser(ServiceExecutor executor, String projectID, String user,
			Project.ProjectRoleType roleType) throws Throwable {
		String projectRole = actualRoleName(projectID, roleType);
		User.grantRevokeRole(executor, user, projectRole, true);
	}

	/**
	 * Add role to project.
	 * 
	 * @param executor
	 * @param projectID
	 * @param role the role to be granted access to the project
	 * @param roleType
	 * @throws Throwable
	 */
	static public void addRole(ServiceExecutor executor, String projectID, String role,
			Project.ProjectRoleType roleType) throws Throwable {
		String projectRole = actualRoleName(projectID, roleType);
		Util.grantRevokeRoleToRole(executor, role, projectRole, true);
	}

	/**
	 * Add the hidden 'Queries' child namespace. Project admins can see it, all
	 * members can query it
	 * 
	 * @param executor
	 * @param projectID
	 * @throws Throwable
	 */
	public static void addQueriesAssetNameSpace(ServiceExecutor executor, String projectID) throws Throwable {
		String queryNameSpace = Project.assetNameSpace(executor, projectID, false) + "/"
				+ Properties.QUERY_NAMESPACE_CHILD;
		String projectAdminRole = Project.actualRoleName(projectID, Project.ProjectRoleType.ADMIN);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", queryNameSpace);
		XmlDoc.Element r = executor.execute("asset.namespace.exists", dm.root());
		if (r.booleanValue("exists"))
			return;
		//
		dm.push("visible-to");
		dm.add("actor", new String[] { "type", "role" }, projectAdminRole);
		dm.pop();
		executor.execute("asset.namespace.create", dm.root());
	}

	/**
	 * Allocate the next standard project name. Calls the CID allocator
	 * 
	 * @param executor
	 * @param type     'production' or 'test'
	 * @return
	 * @throws Throwable
	 */
	static public String allocateProjectName(ServiceExecutor executor, String name, String type) throws Throwable {

		// Get Project Root
		String cidRoot = Project.allocateProjectRootID(executor, type);

		// Allocate Project CID
		String cid = Project.allocateProjectID(executor, cidRoot);

		// Return project name of the form 'proj'-<name>-<cid>
		// The name is meaningful to the user.
		return Properties.PROJECT_ROOT_NAME + name + "-" + cid;
	}

	/**
	 * Returns the actual project asset namespace for the given project
	 * 
	 * @param executor
	 * @param projectID
	 * @param forDR     if true return the namespace name for the DR system
	 * @return
	 * @throws Throwable
	 */
	public static String assetNameSpace(ServiceExecutor executor, String projectID, Boolean forDR) throws Throwable {
		// Exception if not available
		String dict = Properties.getProjectNamespaceMapDictionaryName(executor);
		if (dict == null) {
			throw new Exception(
					"The project ID to namespace dictionary name does not exist. Contact the administrator");
		}

		XmlDoc.Element d = DictionaryUtil.describeDictionaryTerm(executor, dict, projectID);
		if (d == null) {
			throw new Exception("The project '" + projectID
					+ "' does not have an entry in the project ID to namesapce map. Contact the administrator");
		}
		String ns = d.value("entry/definition");
		if (forDR) {
			return ProjectReplicate.makeNameSpaceNameForDR(executor, ns);
		} else {
			return ns;
		}
	}

	/**
	 * Get the depth of a namespace. "/projects" is depth 1
	 * "/projects/proj-<name>-<cid>" is depth 2
	 * 
	 * @param executor
	 * @return The depth. If the projects root is, say, /projects then this will
	 *         return depth 1. If it was say, /data/projects that would be depth 2
	 * @throws Throwable
	 */
	private static Integer assetNameSpaceDepth(ServiceExecutor executor, String nameSpace) throws Throwable {

		// Add "/" on the front for consistency
		String s1 = nameSpace.substring(0, 1); // Nasty Java API
		String ns = null;
		if (s1.equals("/")) {
			ns = nameSpace;
		} else {
			ns = "/" + nameSpace;
		}

		// Find the depth. Because there is a leading "/", split of
		// "/projects" will return two parts {"","projects"} so we subtract 1 from the
		// array length
		String[] parts = ns.split("/");
		return (parts.length) - 1;
	}

	/**
	 * Get the list of root asset namespaces for all projects.
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static Collection<String> assetNameSpaceRoots(ServiceExecutor executor) throws Throwable {
		String dict = Properties.getProjectNamespaceRootsDictionaryName(executor);
		return DictionaryUtil.getDictionaryValues(executor, dict);
	}

	/**
	 * Fetch the project CID from its name. We assume the name has already been
	 * validated and is known to be of the correct form ("proj-<name>-<cid>)
	 * 
	 * @param projectID
	 * @return
	 */
	public static String citeableID(String projectID) throws Throwable {
		String[] parts = projectID.split("-");
		int n = parts.length;
		String cid = parts[n - 1];
		if (Util.isCiteableId(cid))
			return cid;
		throw new Exception(cid + ", extracted from " + projectID + " is not a citeable ID");
	}

	/**
	 * Create the project role of the given type in the given role namespace
	 * 
	 * @param executor
	 * @param roleNameSpace
	 * @param projectRoleType
	 * @throws Throwable
	 */
	static public void createRole(ServiceExecutor executor, String roleNameSpace,
			Project.ProjectRoleType projectRoleType) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("role", roleNameSpace + ":" + projectRoleType.fromEnum());
		dm.add("ifexists", "ignore");
		executor.execute("authorization.role.create", dm.root());
	}

	static public void createStandardRoles(ServiceExecutor executor, String projectID) throws Throwable {

		// Create all the standard roles
		for (ProjectRoleType type : ProjectRoleType.values()) {
			Project.createRole(executor, projectID, type);
		}
	}

	/**
	 * Describe a project. Exception if does not exist
	 * 
	 * @param executor
	 * @param projectID
	 * @throws Throwable
	 */
	static public XmlDoc.Element describe(ServiceExecutor executor, String projectID, Boolean doDR) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("project-id", projectID);
		dm.add("DR", doDR);
		// Exception if does not exist
		return executor.execute("vicnode.project.describe", dm.root());
	}

	/**
	 * Return the description of a project's namespace
	 * 
	 * @param executor
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	static public XmlDoc.Element describeNameSpace(ServerRoute sr, ServiceExecutor executor, String projectID,
			Boolean forDR) throws Throwable {
		String ns = Project.assetNameSpace(executor, projectID, forDR);
		return NameSpaceUtil.describe(null, executor, ns);
	}

	/**
	 * Destroy (carefully) a project namespace
	 * 
	 * @param executor
	 * @param full     projectID
	 * @return
	 * @throws Throwable
	 */
	static public void destroyProjectAssetNameSpace(ServiceExecutor executor, String projectID, XmlWriter w)
			throws Throwable {

		// Validate project name and that all namespaces exist
		// We don't allow partial project CID matches
		Project.validateProject(executor, projectID, false);

		// Destroy
		String projectNameSpace = Project.assetNameSpace(executor, projectID, false);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", projectNameSpace);
		dm.add("atomic", false);
		executor.execute("asset.namespace.destroy", dm.root());
		w.add("asset-namespace", new String[] { "state", "destroyed" }, projectNameSpace);
	}

	/**
	 * Destroy (carefully) meta-data namespace
	 * 
	 * @param executor
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	static public void destroyProjectMetaNameSpace(ServiceExecutor executor, String projectID, XmlWriter w)
			throws Throwable {

		// VAlidate
		Project.validateProjectName(executor, projectID);
		if (!NameSpaceUtil.metaNameSpaceExists(executor, null, projectID)) {
			throw new Exception("The meta-data namespace " + projectID + " does not exist.");
		}

		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", projectID);
		executor.execute("asset.doc.namespace.destroy", dm.root());
		w.add("meta-namespace", new String[] { "state", "destroyed" }, projectID);
	}

	/**
	 * Destroy (carefully) a dictionary namespace
	 * 
	 * @param executor
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	static public void destroyProjectDictionaryNameSpace(ServiceExecutor executor, String projectID, XmlWriter w)
			throws Throwable {

		// Check not rubbish
		if (projectID.equals("") || projectID.isEmpty()) {
			throw new Exception("Project ID " + projectID + " is not valid.");
		}
		Project.validateProjectName(executor, projectID);
		if (!DictionaryUtil.dictionaryNameSpaceExists(executor, null, projectID)) {
			throw new Exception("The dictionary namespace " + projectID + " does not exist.");
		}

		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", projectID);
		executor.execute("dictionary.namespace.destroy", dm.root());
		w.add("dictionary-namespace", new String[] { "state", "destroyed" }, projectID);
	}

	/**
	 * Destroy (carefully) a role namespace
	 * 
	 * @param executor
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	static public void destroyProjectRoleNameSpace(ServiceExecutor executor, String projectID, XmlWriter w)
			throws Throwable {

		// Check not rubbish
		if (projectID.equals("") || projectID.isEmpty()) {
			throw new Exception("Project ID " + projectID + " is not valid.");
		}
		Project.validateProjectName(executor, projectID);
		if (!NameSpaceUtil.roleNameSpaceExists(executor, null, projectID)) {
			throw new Exception("The role namespace " + projectID + " does not exist.");
		}
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", projectID);
		executor.execute("authorization.role.namespace.destroy", dm.root());
		w.add("role-namespace", new String[] { "state", "destroyed" }, projectID);
	}

	static public Collection<String> findOnboardingAssets(ServiceExecutor executor) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		String query = "namespace=" + Properties.getOnboardingNameSpace(executor) + " and "
				+ Properties.getOnboardingDocType(executor) + " has value";
		dm.add("where", query);
		dm.add("pdist", "0");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		return r.values("id");
	}

	/**
	 * Grant or revoke a project role to the user
	 * 
	 * @param executor
	 * @param user            : <domain>:<user>
	 * @param projectRoleType
	 * @param grant
	 * @throws Throwable
	 */
	static public void grantRevokeRoleToUser(ServiceExecutor executor, Project.ProjectRoleType projectRoleType,
			String projectID, String user, Boolean grant) throws Throwable {
		String actualRoleName = actualRoleName(projectID, projectRoleType);
		User.grantRevokeRole(executor, user, actualRoleName, grant);
	}



	/**
	 * Grant all standard permissions (e.g. access to dictionaries) and set standard
	 * ACLs on namespaces. This is the higher-level function.
	 * 
	 * @param executor
	 * @param projectID
	 * @param doACLs - if true, grant ACLs on the project asset namespace. If false,
	 *                 do not do this
	 * @param doPerms - if true, grant the other (to asset namespace ACLs) permission
	 *                  components
	 * @throws Throwable
	 */
	public static void grantStandardPermissionsAndACLs(ServiceExecutor executor, 
			String projectID, Boolean doACLs, Boolean doPerms) throws Throwable {

		// FIrst set permissions for all of the project-specific roles
		for (ProjectRoleType type : ProjectRoleType.values()) {
			Project.grantPermissionsAndACLs(executor, projectID, type, doACLs, doPerms);
		}

		// The following permissions are for administration purpose only
		// Set an ACL on the asset namespace to allow read-only access
		String assetNameSpace = assetNameSpace(executor, projectID, false);
		if (doACLs) {
			Project.grantAssetNameSpaceACLs(executor, assetNameSpace,
					Project.accessAssetNamespaceRoleName(executor), Project.ProjectRoleType.A, true);


			// Set an ACL on the asset namespace to allow administration (usually for
			// specialised services)
			// This permission set is not one of our standard project roles
			// so we just craft it individually.
			// Get the role we want to set the ACL for
			String roleName = Project.administerAssetNamespaceRoleName(executor);
			grantAssetNamespaceAdministerACL(executor, assetNameSpace, roleName, true);
		}

		// These allow access to the meta-data and dictionary namespaces
		if (doPerms) {
			grantNameSpacePermOnRole(executor, projectID, Project.accessAssetNamespaceRoleName(executor), "document",
					"ACCESS");
			grantNameSpacePermOnRole(executor, projectID, Project.accessAssetNamespaceRoleName(executor), "dictionary",
					"ACCESS");

			// Now grant administer access to the role namespace to the generic role
			// namespace admin role
			grantNameSpacePermOnRole(executor, projectID, Project.roleNameSpaceAdminRoleName(executor), "role",
					"ADMINISTER");
		}
	}


	/**
	 * Set permissions for the given project and individual logical project role. Sets
	 * specific project permissions on namespaces and roles. This is the lower-level function.
	 * 
	 * @param executor
	 * @param projectID
	 * @param projectRole
	 * @param doACLs - if true, grant the ACLs on the project namespace. If false, do not!
	 * @param doPerms - if true, grant the other (than asset namespace ACLs) permission
	 *                  components
	 * @throws Throwable
	 */
	static public void grantPermissionsAndACLs(ServiceExecutor executor, String projectID,
			Project.ProjectRoleType projectRoleType, Boolean doACLs,
			Boolean doPerms) throws Throwable {

		// Fetch asset namespace for this project
		String assetNameSpace = assetNameSpace(executor, projectID, false);

		switch (projectRoleType) {
		case A: {
			String actualRoleName = actualRoleName(projectID, Project.ProjectRoleType.A);

			if (doPerms) {

				// Meta-data namespace. 
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "document", "ACCESS");

				// Dictionary namespace
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "dictionary", "ACCESS");

				// Role namespaces
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "role", "ACCESS");
			}

			// Set Asset Name space ACLs
			if (doACLs) {
				grantAssetNameSpaceACLs(executor, assetNameSpace, actualRoleName, Project.ProjectRoleType.A, true);
			}
			return;
		}

		case ACM: {
			String actualRoleName = actualRoleName(projectID, Project.ProjectRoleType.ACM);

			if (doPerms) {
				// Meta-data namespace. 
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "document", "ACCESS");

				// Dictionary namespace
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "dictionary", "ACCESS");

				// ROle namespace
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "role", "ACCESS");
			}

			// Set Asset Name space ACLs
			if (doACLs) {
				grantAssetNameSpaceACLs(executor, assetNameSpace, actualRoleName, Project.ProjectRoleType.ACM, true);
			}
			return;
		}
		case ACMD: {
			String actualRoleName = actualRoleName(projectID, Project.ProjectRoleType.ACMD);

			if (doPerms) {

				// Meta-data namespace. 
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "document", "ACCESS");

				// Dictionary namespace
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "dictionary", "ACCESS");

				// ROle namespaces
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "role", "ACCESS");
			}

			// Set Asset Name space ACLs
			if (doACLs) {
				grantAssetNameSpaceACLs(executor, assetNameSpace, actualRoleName, Project.ProjectRoleType.ACMD, true);
			}
			return;
		}
		case ACMDN: {
			String actualRoleName = actualRoleName(projectID, Project.ProjectRoleType.ACMDN);

			if (doPerms) {

				// Meta-data namespace. 
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "document", "ACCESS");

				// Dictionary namespace
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "dictionary", "ACCESS");

				// ROle namespace
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "role", "ACCESS");
			}

			// Set Asset Name space ACLs
			if (doACLs) {
				grantAssetNameSpaceACLs(executor, assetNameSpace, actualRoleName, Project.ProjectRoleType.ACMDN, true);
			}
			return;
		}
		case ADMIN: {
			String actualRoleName = actualRoleName(projectID, Project.ProjectRoleType.ADMIN);

			if (doPerms) {

				// Meta-data namespace. 
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "document", "ADMINISTER");

				// Dictionary namespace
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "dictionary", "ADMINISTER");

				// ROle namespace
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "role", "ADMINISTER");
			}

			// Set asset Name space ACLs
			if (doACLs) {
				grantAssetNameSpaceACLs(executor, assetNameSpace, actualRoleName, Project.ProjectRoleType.ADMIN, true);
			}

			// Grant some additional permissons needed by the project admin
			Util.grantRevokeRoleToRole(executor, actualRoleName, "authorizing-user", true);
			return;
		}
		case RCPUSR: {
			String actualRoleName = actualRoleName(projectID, ProjectRoleType.RCPUSR);

			if (doPerms) {
				// Meta-data namespace.
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "document", "ACCESS");

				// Dictionary namespace
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "dictionary", "ACCESS");

				// ROle namespace
				grantNameSpacePermOnRole(executor, projectID, actualRoleName, "role", "ADMINISTER");
			}

			// Set Asset Name space ACLs
			if (doACLs) {
				grantAssetNameSpaceACLs(executor, assetNameSpace, actualRoleName, ProjectRoleType.RCPUSR, true);
			}
			// Grant some additional permissons needed by the project rcp admin
			Util.grantRevokeRoleToRole(executor, actualRoleName, "authorizing-user", true);
			return;
		}
		}
		throw new Exception("Unhandled project role type");
	}




	/**
	 * Set an ACL on the asset namespace to allow administration (usually for
	 * specialised services) This permission set is not one of our standard project
	 * roles so we just craft it individually.
	 * 
	 * @param executor
	 * @param assetNameSpace
	 * @param role
	 * @param grant - actually grant the ACL, else just return the ACL XML
	 * @throws Throwable
	 */
	public static XmlDoc.Element grantAssetNamespaceAdministerACL(ServiceExecutor executor, 
			String assetNameSpace, String role, Boolean grant)
					throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.push("acl");
		dm.add("actor", new String[] { "type", "role" }, role);
		dm.push("access");
		dm.add("namespace", "administer");
		dm.add("namespace", "access");
		// :namespace execute (posix navigation) not currently included
		dm.pop();
		dm.pop();
		if (grant) {
			dm.add("namespace", assetNameSpace);
			executor.execute("asset.namespace.acl.grant", dm.root());
		}
		return dm.root().element("acl");
	}

	/**
	 * Create the ACLs on a new project asset namespace
	 * 
	 * @param executor
	 * @param projectAssetNameSpace
	 * @param actualRoleName     : the actual project role  
	 * @param ProjectRoleType    : The logical project role
	 * @param grant               : actually grant the ACL. Otherwise, just return the ACL structure
	 * @throws Throwable
	 */
	public static XmlDoc.Element grantAssetNameSpaceACLs(ServiceExecutor executor,
			String projectAssetNameSpace, String actualRoleName, 
			Project.ProjectRoleType roleType, Boolean grant) throws Throwable {

		// Asset namespace. Set an ACL on the namespace.
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.push("acl");
		dm.add("actor", new String[] { "type", "role" }, actualRoleName);
		dm.push("access");

		switch (roleType) {
		case A: {
			dm.add("namespace", "access");
			dm.add("namespace", "execute");      // posix navigation
			dm.add("asset", "access");
			dm.add("asset-content", "access");
			break;
		}
		case ACM: {
			dm.add("namespace", "access");
			dm.add("namespace", "create");
			dm.add("namespace", "modify");       // Allows child move
			dm.add("namespace", "execute");      // posix navigation
			dm.add("asset", "access");
			dm.add("asset", "create");
			dm.add("asset", "modify");
			dm.add("asset-content", "access");
			dm.add("asset-content", "modify");
			break;
		}
		case ACMD: {
			dm.add("namespace", "access");
			dm.add("namespace", "create");
			dm.add("namespace", "modify");       // Allows child move
			dm.add("namespace", "execute");      // posix navigation
			dm.add("asset", "access");
			dm.add("asset", "create");
			dm.add("asset", "modify");
			dm.add("asset", "destroy");
			dm.add("asset-content", "access");
			dm.add("asset-content", "modify");
			break;
		}
		case ACMDN: {
			dm.add("namespace", "access");
			dm.add("namespace", "create");
			dm.add("namespace", "modify");
			dm.add("namespace", "destroy");
			dm.add("namespace", "execute");      // posix navigation
			dm.add("asset", "create");
			dm.add("asset", "access");
			dm.add("asset", "modify");
			dm.add("asset", "destroy");
			dm.add("asset", "licence");
			dm.add("asset-content", "access");
			dm.add("asset-content", "modify");
			break;
		}
		case ADMIN: {
			dm.add("namespace", "administer-children");
			dm.add("namespace", "access");
			dm.add("namespace", "create");
			dm.add("namespace", "modify");
			dm.add("namespace", "destroy");
			dm.add("namespace", "execute");      // posix navigation
			dm.add("asset", "create");
			dm.add("asset", "access");
			dm.add("asset", "modify");
			dm.add("asset", "destroy");
			dm.add("asset", "licence");
			dm.add("asset-content", "access");
			dm.add("asset-content", "modify");
			break;
		}case RCPUSR: {
			dm.add("namespace", "access");
			dm.add("namespace", "execute");      // posix navigation
			break;
		}
		default: {
			throw new Exception("Unhandled role type");
		}
		}
		dm.pop();
		dm.pop();
		if (grant) {
			dm.add("namespace", projectAssetNameSpace);
			executor.execute("asset.namespace.acl.grant", dm.root());
		}
		return dm.root().element("acl");
	}

	/**
	 * Grant access to a role, dictionary or document namespace for the given project
	 * 
	 * @param executor
	 * @param projectID
	 * @param roleName
	 * @param type      role, dictionary or document
	 * @param perm      standard MF permission (ACCESS, MODIFY etc)
	 * @throws Throwable
	 */
	public static void grantNameSpacePermOnRole(ServiceExecutor executor, String projectID, String roleName,
			String type, String perm) throws Throwable {
		if (!type.equals("role") && !type.equals("document") && !type.equals("dictionary")) {
			throw new Exception("Illegal namespace resource type : " + type);
		}
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", roleName);
		dm.add("type", "role");
		dm.push("perm");
		dm.add("access", perm);
		dm.add("resource", new String[] { "type", type + ":namespace" }, projectID);
		dm.pop();
		executor.execute("actor.grant", dm.root());
	}

	public static void grantRevokeRoleToProjectRoles(ServiceExecutor executor, String projectID, String role,
			Boolean grant) throws Throwable {
		if (role != null) {
			for (ProjectRoleType type : ProjectRoleType.values()) {
				String arn = Project.actualRoleName(projectID, type);
				Util.grantRevokeRoleToRole(executor, arn, role, grant);
			}
		}
	}

	/**
	 * Does the user hold the specialised provisioning role ?
	 * 
	 * @param executor
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public static Boolean hasProvisioningRole(ServiceExecutor executor, String name) throws Throwable {
		return User.hasRole(executor, name, Properties.getProvisioningRoleName(executor));
	}

	/**
	 * Return project type (production/test)
	 * 
	 * @param executor
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	public static String projectType(ServiceExecutor executor, String projectID) throws Throwable {
		// See if the meta-data is there.
		XmlDoc.Element meta = describeNameSpace(null, executor, projectID, false);
		return projectType(executor, meta);
	}

	/**
	 * Return project type (production/test/closed-stub)
	 * 
	 * @param executor
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	public static String projectType(ServiceExecutor executor, XmlDoc.Element nsMeta) throws Throwable {
		// See if the meta-data is there.
		String type = nsMeta.value("namespace/asset-meta/" + Properties.getProjectDocType(executor) + "/type");
		if (type == null) {
			if (type == null) {
				throw new Exception("No project type found; should be '" + Properties.PROJECT_TYPE_PROD + "' or '"
						+ Properties.PROJECT_TYPE_TEST + "' or '" + Properties.PROJECT_TYPE_STUB + "'");
			}
		}
		return type;
	}

	/**
	 * Is this project a production project
	 * 
	 * @param namespaceMeta
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	public static Boolean isProduction(ServiceExecutor executor, XmlDoc.Element namespaceMeta, String projectID)
			throws Throwable {
		// See if the meta-data is there.
		String type = namespaceMeta.value("namespace/asset-meta/" + Properties.getProjectDocType(executor) + "/type");
		if (type == null) {
			throw new Exception(
					"Project " + projectID + " has no project type; should be '" + Properties.PROJECT_TYPE_PROD
					+ "' or '" + Properties.PROJECT_TYPE_TEST + "' or '" + Properties.PROJECT_TYPE_STUB + "'");
		}
		return type.equals(Properties.PROJECT_TYPE_PROD);
	}

	/**
	 * Is this project a test project
	 * 
	 * @param namespaceMeta
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	public static Boolean isTest(ServiceExecutor executor, XmlDoc.Element namespaceMeta, String projectID)
			throws Throwable {
		String type = namespaceMeta.value("namespace/asset-meta/" + Properties.getProjectDocType(executor) + "/type");
		if (type == null) {
			throw new Exception(
					"Project " + projectID + " has no project type; should be '" + Properties.PROJECT_TYPE_PROD
					+ "' or '" + Properties.PROJECT_TYPE_TEST + "' or '" + Properties.PROJECT_TYPE_STUB + "'");
		}
		return type.equals(Properties.PROJECT_TYPE_TEST);
	}

	/**
	 * Is this project a closed stub project
	 * 
	 * @param namespaceMeta
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	public static Boolean isClosedStub(ServiceExecutor executor, XmlDoc.Element namespaceMeta, String projectID)
			throws Throwable {
		// See if the meta-data is there.
		String type = namespaceMeta.value("namespace/asset-meta/" + Properties.getProjectDocType(executor) + "/type");
		if (type == null) {
			throw new Exception(
					"Project " + projectID + " has no project type; should be '" + Properties.PROJECT_TYPE_PROD
					+ "' or '" + Properties.PROJECT_TYPE_TEST + "' or '" + Properties.PROJECT_TYPE_STUB + "'");
		}
		return type.equals(Properties.PROJECT_TYPE_STUB);
	}

	/**
	 * Is the actual project role read-only or does the role allow write (create,
	 * modify, destroy)
	 * 
	 * @param role Of the form <project id>:<role> E.g.
	 *             proj-test-1128.2.3243:participant-acmd
	 */
	public static Boolean isRoleReadOnly(String role) throws Throwable {
		// The only role which is read-only is participant-a
		// So look for that
		String[] parts = role.split(":");
		if (parts.length != 2) {
			throw new Exception("Invalid role. Must be of the form <project id>:<role>");
		}
		if (parts[1].equals("participant-a")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Select whether to keep the project or not based on a 'containment' string in
	 * the project ID
	 * 
	 * @param projectID
	 * @param keyString
	 * @return
	 */
	static public Boolean keepOnKeyString(String projectID, String keyString) {
		String t1 = projectID.toUpperCase();
		return (keyString == null) || (keyString != null && t1.contains(keyString));
	}

	/**
	 * Select whether to keep the project or not depending on its project-ID and the
	 * desired production type
	 * 
	 * @param executor
	 * @param projectHolder
	 * @param projType      'production', 'test', 'closed-stub', 'all'
	 * @return
	 * @throws Throwable
	 */
	static public Boolean keepOnProjectType(ProjectHolder ph, String projType) throws Throwable {
		return keepOnProjectType(ph.executor(), ph.metaData(), ph.id(), projType);
	}

	static private Boolean keepOnProjectType(ServiceExecutor executor, XmlDoc.Element nameSpaceMeta, String projectID,
			String projType) throws Throwable {
		if (projType == null)
			return true;
		if (projType.equals(Properties.PROJECT_TYPE_ALL))
			return true;
		if (!projType.equals(Properties.PROJECT_TYPE_PROD) && !projType.equals(Properties.PROJECT_TYPE_TEST)
				&& !projType.equals(Properties.PROJECT_TYPE_STUB)) {
			throw new Exception("The production type must be one of : '" + Properties.PROJECT_TYPE_ALL + "'. '"
					+ Properties.PROJECT_TYPE_PROD + "', or '" + Properties.PROJECT_TYPE_TEST + "', or '"
					+ Properties.PROJECT_TYPE_STUB + "'");
		}
		//
		if (projType.equals(Properties.PROJECT_TYPE_PROD) && Project.isProduction(executor, nameSpaceMeta, projectID)) {
			return true;
		}
		if (projType.equals(Properties.PROJECT_TYPE_TEST) && Project.isTest(executor, nameSpaceMeta, projectID)) {
			return true;
		}
		if (projType.equals(Properties.PROJECT_TYPE_STUB) && Project.isClosedStub(executor, nameSpaceMeta, projectID)) {
			return true;
		}
		//
		return false;
	}

	/**
	 * Select whether to keep the project or not depending on whether it's being
	 * replicated or not
	 *
	 * @param executor
	 * @param qName  - replication asset processing queue name for this project. If null, it's not
	 *                 being replicated.
	 * @param type     'all', 'replication', 'no-replication'
	 * @return
	 * @throws Throwable
	 */
	public static Boolean keepOnRepType(ServiceExecutor executor, String qName, String type) throws Throwable {
		if (type == null) {
			return true;
		}
		if (type.equals("all")) {
			return true;
		}
		//
		if (type.equals("replication")) {
			if (qName != null) {
				return true;
			}
		} else if (type.equals("no-replication")) {
			if (qName == null) {
				return true;
			}
		}
		return false;
	}

	public static Boolean keepOnParent(ProjectHolder ph, String parent) throws Throwable {
		if (parent == null) {
			return true;
		}

		// We want the root in the form /parent/path with no trailing "/"
		String parent2 = parent;
		int l = parent.length();
		int lastIdx = parent.lastIndexOf("/");
		if (lastIdx == l - 1) {
			parent2 = parent.substring(0, l - 1);
		}

		// Find the parent part of the asset namespace
		String assetNameSpace2 = ph.parentNameSpace();

		// See of we match exactly (we don't want other parents further up the tree).
		return assetNameSpace2.equals(parent2);
	}

	public static Boolean keepOnOwner (ProjectHolder ph, String email) throws Throwable {

		if (email == null) {
			return true;
		}		
		String owner = ph.ownerEmail();
		if (owner==null) {
			return false;
		}
		return owner.equals(email);
	}

	
	public static Boolean keepOnIgnoreList (String t, Collection<String> ignoreIDs) {
		if (ignoreIDs==null) return true;
		for (String ignoreID : ignoreIDs) {
			// We don't keep this one as it's found in the ignore list.
			if (t.equals(ignoreID)) return false;
		}
		return true;
	}


	/**
	 * Returns a unique list of the emails of all administrators of the give project
	 * 
	 * @param executor
	 * @param projectID
	 * @param dropProvision
	 * @return
	 * @throws Throwable
	 */
	public static Collection<String> listAdminUserEMails(ServiceExecutor executor, String projectID,
			Boolean dropProvision) throws Throwable {
		Collection<String> emails = new HashSet<String>(); // Make unique

		// Find all roles held by users for this project
		XmlDoc.Element r = Project.listUsers(executor, projectID, false, false);
		if (r == null) {
			return null;
		}
		Collection<XmlDoc.Element> roles = r.elements("role");
		if (roles == null)
			return null;

		// Iterate through held roles
		for (XmlDoc.Element role : roles) {
			String name = role.value("@name");
			if (name.endsWith(":administrator")) {

				// authority:domain:user
				Collection<String> users = role.values("user");
				if (users != null) {
					for (String user : users) {

						// Drop any user holding the standard provisioning role if requested
						Boolean includeUser = true;
						if (dropProvision) {
							includeUser = !Project.hasProvisioningRole(executor, user);
						}
						if (includeUser) {
							String email = User.getUserEMail(executor, user);
							if (email != null)
								emails.add(email);
						}
					}
				}
			}
		}
		return emails;
	}

	/**
	 * Returns a unique list of the emails of all users of the given project
	 * 
	 * @param executor
	 * @param projectID
	 * @param dropProvision
	 * @return
	 * @throws Throwable
	 */
	public static Collection<String> listUserEMails(ServiceExecutor executor, String projectID, Boolean dropProvision)
			throws Throwable {
		Collection<String> emails = new HashSet<String>(); // Make unique
		Vector<String> users = Project.listUsers(executor, projectID);
		if (users == null)
			return null;
		//
		for (String user : users) {
			Boolean includeUser = true;
			if (dropProvision) {
				includeUser = !Project.hasProvisioningRole(executor, user);
			}
			if (includeUser) {
				String email = User.getUserEMail(executor, user);
				if (email != null)
					emails.add(email);
			}

		}
		return emails;
	}

	/**
	 * Retrieve unique list of emails according to type of user
	 * 
	 * @param executor
	 * @param projectID
	 * @param who           administrator (just users with the administrator role),
	 *                      owner (users listed in the owner meta-data), manage
	 *                      (admin+owner), user (all users with project roles) and
	 *                      all (user+owner)
	 * @param dropProvision
	 * @return
	 * @throws Throwable
	 */
	public static Collection<String> listProjectEmails(ServiceExecutor executor, String projectID, String who,
			Boolean dropProvision) throws Throwable {

		// Generate emails
		Collection<String> emails = new Vector<String>();
		if (who == null)
			return emails;
		if (who.equals("owner")) {
			Collection<String> t = Project.listOwnerEmails(executor, projectID);
			if (t != null && t.size() > 0) {
				emails.addAll(t);
			}
		} else if (who.equals("administrator")) {
			Collection<String> t = Project.listAdminUserEMails(executor, projectID, dropProvision);
			if (t != null && t.size() > 0) {
				emails.addAll(t);
			}
		} else if (who.equals("manage")) {
			Collection<String> t = Project.listOwnerEmails(executor, projectID);
			if (t != null && t.size() > 0) {
				emails.addAll(t);
			}
			//
			t = Project.listAdminUserEMails(executor, projectID, dropProvision);
			if (t != null && t.size() > 0) {
				emails.addAll(t);
			}
		} else if (who.equals("user")) {
			Collection<String> t = Project.listUserEMails(executor, projectID, dropProvision);
			if (t != null && t.size() > 0) {
				emails.addAll(t);
			}
		} else if (who.equals("all")) {
			Collection<String> t = Project.listUserEMails(executor, projectID, dropProvision);
			if (t != null && t.size() > 0) {
				emails.addAll(t);
			}
			t = Project.listOwnerEmails(executor, projectID);
			if (t != null && t.size() > 0) {
				emails.addAll(t);
			}
		}
		HashSet<String> t = new HashSet<String>();
		if (emails.size() > 0) {
			t.addAll(emails);
		}
		return t;
	}

	// List project users
	// Boolean allRoles : if true also list the roles nobody holds
	// Boolean allUsers: if true list users that are disabled as well as enabled. If
	// false just list enabled.
	static public XmlDoc.Element listUsers(ServiceExecutor executor, String projectID, Boolean allRoles,
			Boolean allUsers) throws Throwable {

		// Get project roles
		Vector<String> roles = Project.actualRoleNames(projectID);

		// See who holds them
		XmlDocMaker dm = new XmlDocMaker("project", new String[] { "name", projectID });

		for (String role : roles) {
			Vector<String> users = User.haveRole(executor, role, allUsers);
			if (users.size() > 0) {
				dm.push("role", new String[] { "name", role });
				for (String user : users) {
					dm.add("user", user);
				}
				dm.pop();
			} else {
				if (allRoles) {
					dm.push("role", new String[] { "name", role });
					dm.pop();
				}
			}
		}
		return dm.current();
	}

	// List identity tokens with access to this project
	// Boolean all : if true also list the roles nobody holds
	static public XmlDoc.Element listIdentityTokens(ServiceExecutor executor, String projectID, Boolean all)
			throws Throwable {

		// Get project roles
		Vector<String> roles = Project.actualRoleNames(projectID);

		// See who holds them
		XmlDocMaker dm = new XmlDocMaker("project", new String[] { "name", projectID });

		for (String role : roles) {
			Collection<XmlDoc.Element> tokenActors = User.identityTokensHaveRole(executor, role);
			if (tokenActors != null && !tokenActors.isEmpty()) {
				dm.push("role", new String[] { "name", role });
				for (XmlDoc.Element tokenActor : tokenActors) {
					String tokenID = User.getTokenID (executor, tokenActor);
					// Token ID, actor ID and  Actor Name
					dm.add("identity-token", 
							new String[] {"id", tokenID, "actor-id", tokenActor.value("@id"), "actor-name",
							tokenActor.value("@name")});
				}
				dm.pop();
			} else {
				if (all) {
					dm.push("role", new String[] { "name", role });
					dm.pop();
				}
			}
		}
		return dm.current();
	}
	

	/**
	 * Returns enabled users that hold any role for the given project
	 * 
	 * @param executor
	 * @param projectID
	 * @return Returns empty vector if none
	 * @throws Throwable
	 */
	static public Vector<String> listUsers(ServiceExecutor executor, String projectID) throws Throwable {
		Vector<String> users = new Vector<String>();
		Vector<String> projectRoles = actualRoleNames(projectID);
		for (String projectRole : projectRoles) {
			users.addAll(User.haveRole(executor, projectRole, false));
		}
		return users;
	}

	/**
	 * Returns enabled users that hold the given role type for the given project
	 * 
	 * @param executor
	 * @param projectID
	 * @param roleType
	 * @param dropProvision
	 * @return Returns empty vector if none
	 * @throws Throwable
	 */
	static public Vector<String> listUsers(ServiceExecutor executor, String projectID, Project.ProjectRoleType roleType,
			Boolean dropProvision) throws Throwable {
		String role = Project.actualRoleName(projectID, roleType);
		Collection<String> users = User.haveRole(executor, role, false);
		Vector<String> users2 = new Vector<String>();
		if (users == null)
			return users2;
		//
		for (String user : users) {
			if (dropProvision) {
				if (!Project.hasProvisioningRole(executor, user)) {
					users2.add(user);
				}
			} else {
				users2.add(user);
			}
		}
		return users2;
	}

	/**
	 * Return the email addresses of all people listed as the owner of the project
	 * 
	 * @param executor
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	static public Collection<String> listOwnerEmails(ServiceExecutor executor, String projectID) throws Throwable {
		String assetNameSpace = Project.assetNameSpace(executor, projectID, false);
		XmlDoc.Element nameSpaceDescription = NameSpaceUtil.describe(null, executor, assetNameSpace);
		Collection<XmlDoc.Element> owners = nameSpaceDescription
				.elements("namespace/asset-meta/" + Properties.getCollectionDocType(executor) + "/owner");
		Collection<String> emails = new HashSet<String>(); // Make unique
		for (XmlDoc.Element owner : owners) {
			emails.add(owner.value("email"));
		}
		return emails;
	}

	/**
	 * A project's path is the concatenation of a parent name space (e.g.
	 * /projects or /projects/instruments or /instruments) and the project ID
	 * 
	 * @param assetNameSpaceRoot
	 * @param projectID
	 * @return
	 */
	public static String makeProjectPath (String assetNameSpaceRoot, String projectID) {
		String t = assetNameSpaceRoot;
		if (!t.startsWith("/")) {
			t = "/" + assetNameSpaceRoot;
		}
		return t + "/" + projectID;
	}

	/**
	 * FInd the CID of the specified citeable root name (can be any, not just the
	 * ones we are using for project management)
	 * 
	 * @param executor
	 * @param namedRoot
	 * @return null if does not exist
	 * @throws Throwable
	 */
	private static String namedCiteableRootValue(ServiceExecutor executor, String namedRoot) throws Throwable {
		XmlDoc.Element r = executor.execute("citeable.named.id.describe");
		Collection<XmlDoc.Element> ids = r.elements("id");
		for (XmlDoc.Element id : ids) {
			String name = id.value("@name");
			String cid = id.value();
			if (name.equals(namedRoot))
				return cid;
		}
		return null;
	}

	/**
	 * Send emails to the new Project user plus the administrators of the project
	 * telling them the new user or role has joined or left the project
	 * 
	 * @param executor
	 * @param projectID
	 * @param userActorName     <domain>:<user> A user who is added/removed to/from the project
	 * @param roleUserActorName  A role that is added/removed to/from the project as a 'role user'
	 * @param projectRole       The project role added (null when being removed)
	 * @param add               If true the project role is being added, else removed
	 * @param dropProvision     If true, don't notify the specialized provisioning
	 *                          user who is granted admin on all projects
	 * @param excludeUserFromAdmin
	 *                          If the user being added/removed is also an admin, don't include them in
	 *                          the notification that goes to the admins (since they will
	 *                          get it anyway in their user context)
	 * @param userOnly          Only notify the user, not the admins
	 */
	static public void notifyUserAndAdminsOfRoleChanges(ServiceExecutor executor, 
			String projectID, String userActorName, String roleUserActorName, 
			String projectRole, Boolean dropProvision,
			Boolean add, Boolean excludeUserFromAdmin, 
			Boolean userOnly,
			XmlWriter w) throws Throwable {

		
		String roleURL = Properties.getApplicationProperty(executor,
				Properties.APPLICATION_PROPERTY_DOC_STANDARD_PROJECT_ROLES, Properties.APPLICATION_PROPERTY_APP);
		String docUserRoot = Properties.getApplicationProperty(executor, Properties.APPLICATION_PROPERTY_DOC_USER_ROOT,
				Properties.APPLICATION_PROPERTY_APP);

		// Notify user being added/removed
		String op = "added to";
		String subject = "Added to Mediaflux project '" + projectID + "'";
		if (!add) {
			op = "removed from";
			subject = "Removed from Mediaflux project '" + projectID + "'";
		}
		//
		String userEmail = null;
		if (userActorName!=null) {
			String body = "Dear Colleague,\n\n";
			body += "At the request of the project owner or technical contact, \n";
			body += "you (" + userActorName + ") have been " + op + " the Mediaflux \n";
			if (add) {
		       body += "project '" + projectID + "' with role '" + projectRole + "'\n\n";
			   body += "\nFor more information on user roles, see: " + roleURL + "\n";
			} else {
			   body += "project '" + projectID  + "\n\n";
			}
			body += "\nFor general information, including Getting Started and Support, see the top-level page of the Mediaflux documentation at: "
					+ docUserRoot + "\n";
			body += "\nRegards,\nResearch Computing Services - Data Solutions Team\nBusiness Services\nThe University of Melbourne\n";
			userEmail = sendUserMail(executor, userActorName, subject, body);
			w.add("email-to-user", userEmail);
		}
		
		// Bug out if user only to be notified
		if (userOnly) {
			return;
		}
		
		// We send an email to the administrators of the project telling 
		// them of the changes
		Collection<String> emails = Project.listAdminUserEMails(executor, projectID, dropProvision);
		if (emails == null) {
			return;
		}
		// Exclude user from admin list if desired
		if (excludeUserFromAdmin) {
			emails.remove(userEmail);
		}


		String body = "Dear Project Administrator,\n\n" + 
				"At the request of the project owner or technical contact, \n";
		if (userActorName!=null) {
			body += "the user '" + userActorName + "' with email '" + userEmail + "' \n";
			body += "has been " + op + " the Mediaflux project '" + projectID + "'\n";
			if (add) {
				body += "' with role '" + projectRole + "'\n\n";			
			}
		}
		if (roleUserActorName!=null) {
			body += "the role user '" + roleUserActorName + "' \n";
			body += "has been " + op + " the Mediaflux project '" + projectID + "\n";
			if (add) {
				body += "' with role '" + projectRole + "'\n\n";			
			}
		}

		body += "\n\nRegards,\nResearch Computing Services - Data Solutions Team\nBusiness Services\nThe University of Melbourne\n";


		// Send to admins
		for (String em : emails) {
			MailHandler.sendMessage(executor, em, subject, body, null, true, null);
			w.add("email-to-admin", em);
		}
	}

	/**
	 * Lists all of the projectIDs. If none, zero length Collection is returned
	 * 
	 * @param executor
	 * @param sort           - sort by CID instead of name
	 * @param fromDictionary - if true, use the dictionary that maps from project id
	 *                       to namespace. if false, iterate through list of project
	 *                       roots and find all namespaces both should give the same
	 *                       answer if all dictionaries are kept in sync. it's
	 *                       faster to use the fromDictionary option
	 * @return A Collection
	 * @throws Throwable
	 */
	static public Collection<String> projectIDs(ServiceExecutor executor, Boolean sort, Boolean fromDictionary)
			throws Throwable {

		ArrayList<String> ids = new ArrayList<String>();
		if (fromDictionary) {
			XmlDoc.Element dt = DictionaryUtil.describeDictionaryTerms(executor,
					Properties.getProjectNamespaceMapDictionaryName(executor));
			if (dt != null) {
				Collection<String> t = dt.values("entry/term");
				if (t != null) {
					ids.addAll(t);
				}
			}
		} else {
			Collection<String> nameSpaces = assetNameSpaceRoots(executor);
			for (String nameSpace : nameSpaces) {
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("namespace", nameSpace);
				XmlDoc.Element r = executor.execute("asset.namespace.list", dm.root());
				XmlDoc.Element t = r.element("namespace");
				String path = t.value("@path");
				Collection<String> lids = t.values("namespace");

				// Validate any children namespaces
				if (lids != null) {

					// If somebody shoves something silly in the projects namespace, we have to find
					// that out.
					for (String lid : lids) {
						// Filter out other project roots
						if (!isProjectNameSpaceRoot(executor, path + "/" + lid)) {
							Project.validateProject(executor, lid, false);
							ids.add(lid);
						}
					}
				}
			}
		}

		// Sort IDS by trailing CID
		if (sort) {
			if (ids.size() > 0) {
				return sortProjectIDs(ids);
			} else {
				return ids;
			}
		} else {
			return ids;
		}
	}

	/**
	 * Lists all of the projectIDs for the given parent namespace. If none, a zero
	 * length collection is returned. Uses the mapping dictionary to find the
	 * projects
	 * 
	 * @param executor
	 * @param parentNameSpace
	 * @return A Collection
	 * @throws Throwable
	 */
	public static Collection<String> projectIDs(ServiceExecutor executor, String parentNameSpace) throws Throwable {
		if (!isProjectNameSpaceRoot(executor, parentNameSpace)) {
			throw new Exception(parentNameSpace + " is not a valid project parent namespace");
		}
		XmlDoc.Element t = DictionaryUtil.describeDictionaryTerms(executor,
				Properties.getProjectNamespaceMapDictionaryName(executor));
		Vector<String> ids = new Vector<String>();
		if (t != null) {
			Collection<XmlDoc.Element> entries = t.elements("entry");
			for (XmlDoc.Element entry : entries) {
				String definition = entry.value("definition");
				if (definition.contains(parentNameSpace)) {
					ids.add(entry.value("term"));
				}
			}
		}
		return ids;
	}

	/**
	 * Fetches the projects root namespace from the server property
	 * 
	 */
	public static String getProjectRootNameSpace(ServiceExecutor executor) throws Throwable {
		return Util.getServerProperty(executor, Properties.SERVER_PROPERTY_PROJECT_ROOT);
	}

	/**
	 * Is the given namespace one of the project namespace root namespaces
	 * 
	 * @param executor
	 * @param path
	 * @return
	 * @throws Throwable
	 */
	public static Boolean isProjectNameSpaceRoot(ServiceExecutor executor, String path) throws Throwable {
		Collection<String> nameSpaces = assetNameSpaceRoots(executor);
		return nameSpaces.contains(path);
	}

	/**
	 * FInd the direct parent namespace of the project's namespace
	 * 
	 * @param executor
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	public static String parentNameSpace(ServiceExecutor executor, String projectID) throws Throwable {

		// Get the project namespace
		String assetNameSpace = Project.assetNameSpace(executor, projectID, false);

		// Find the parent part of it
		int startIdx = assetNameSpace.indexOf("proj-", 0);
		return assetNameSpace.substring(0, startIdx - 1);
	}

	/**
	 * Returns the root CID for production projects
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	static public String productionProjectRootCID(ServiceExecutor executor) throws Throwable {
		return allocateProjectRootID(executor, Properties.PROJECT_TYPE_PROD);
	}

	/**
	 * Rename the underlying asset namespace in accordance with a new ProjectID
	 * 
	 * @param executor
	 * @param oldProjectID
	 * @param newProjectID
	 * @param forDR        If true do the renaming on the DR system
	 * @returns the renamed namespace
	 * @throws Throwable
	 */
	public static String renameAssetNameSpace(ServiceExecutor executor, String oldProjectID, String newProjectID,
			Boolean forDR) throws Throwable {
		String oldAssetNameSpace = Project.assetNameSpace(executor, oldProjectID, forDR);
		ServerRoute sr = null;
		if (forDR) {
			sr = ProjectReplicate.DRServerRoute(executor);
		}
		return NameSpaceUtil.renameAssetNameSpace(executor, sr, oldAssetNameSpace, newProjectID);
	}

	/**
	 * Rename a dictionary namespace and update references to the dictionaries in
	 * the project document namespace. This function supports renaming projects so
	 * depending on the order, the document name space might not have already been
	 * renamed, so its is specified independently.
	 * 
	 * @param executor
	 * @param oldDictionaryNameSpace
	 * @param newDictionaryNameSpace
	 * @param documentNameSpace
	 * @throws Throwable
	 */
	public static void renameDictionaryNameSpace(ServerRoute sr, ServiceExecutor executor,
			String oldDictionaryNameSpace, String newDictionaryNameSpace, String documentNameSpace) throws Throwable {

		// Rename the dictionary namespace
		NameSpaceUtil.renameDictionaryNameSpace(sr, executor, oldDictionaryNameSpace, newDictionaryNameSpace);

		// Now we have some work to do. Document type definitions referring to
		// dictionaries
		// are not updated

		// List all document types in the document namespace
		Collection<String> docTypes = MetaUtil.listDocumentTypes(executor, documentNameSpace);
		if (docTypes == null || docTypes.isEmpty())
			return;

		// List all the dictionaries in the renamed namespace
		Collection<String> newDictionaries = MetaUtil.listDictionaries(executor, newDictionaryNameSpace);
		if (newDictionaries == null || newDictionaries.isEmpty())
			return;

		// Iterate over doc types
		for (String docType : docTypes) {

			// Iterate over all dictionaries in the namespace and replace old dictionary
			// names with the new
			for (String newDictionary : newDictionaries) {

				// Form name of old dictionary
				String[] parts = newDictionary.split(":");
				int n = parts.length;
				if (n == 1) {
					// No namespace component, nothing to do
				} else if (n == 2) {

					// Replace old with new (dependency on daris-essentials package)
					String oldName = oldDictionaryNameSpace + ":" + parts[1];
					DocTypeUtil.replaceDictionaryInDocType(executor, docType, oldName, newDictionary);
				} else {
					throw new Exception("Cannot handle dictionary of name '" + newDictionary + "'");
				}
			}
		}
	}

	public static void revokeAllPermissionsOnStandardRoles(ServerRoute sr, ServiceExecutor executor, String projectID)
			throws Throwable {
		Vector<String> projectRoles = actualRoleNames(projectID);
		for (String projectRole : projectRoles) {
			revokeAllPermissions(sr, executor, projectRole);
		}
	}

	private static void revokeAllPermissions(ServerRoute sr, ServiceExecutor executor, String role) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", role);
		dm.add("type", "role");
		XmlDoc.Element r = executor.execute("actor.describe", dm.root());
		if (r == null)
			return;
		Collection<XmlDoc.Element> perms = r.elements("actor/perm");
		if (perms == null)
			return;

		// Revoke perms
		dm = new XmlDocMaker("args");
		dm.add("name", role);
		dm.add("type", "role");
		for (XmlDoc.Element perm : perms) {
			dm.add(perm);
		}

		dm.add("access-must-be-valid", false);
		executor.execute(sr, "actor.revoke", dm.root());
	}

	/**
	 * Returns the root CID for test projects
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	static public String testProjectRootCID(ServiceExecutor executor) throws Throwable {
		return allocateProjectRootID(executor, "test");
	}

	/**
	 * Remove user from a project by revoking a role
	 * 
	 * @param executor
	 * @param projectID
	 * @param user      " <domain>:<user>
	 * @param roleType
	 * @throws Throwable
	 */
	static public void removeUser(ServiceExecutor executor, String projectID, String user,
			Project.ProjectRoleType roleType) throws Throwable {
		String projectRole = actualRoleName(projectID, roleType);
		User.grantRevokeRole(executor, user, projectRole, false);
	}

	/**
	 * Set the project type.
	 * 
	 * @param executor
	 * @param projectID
	 * @param type:     'production' or 'test'
	 * @throws Throwable
	 */
	static public void setType(ServiceExecutor executor, String projectID, String type) throws Throwable {
		if (type == null) {
			throw new Exception("Project type cannot be null");
		}
		if (!type.equals(Properties.PROJECT_TYPE_PROD) && !type.equals(Properties.PROJECT_TYPE_TEST)
				&& !type.equals(Properties.PROJECT_TYPE_STUB)) {
			throw new Exception("Unhandled project type '" + type + "'");
		}
		String projectNameSpace = Project.assetNameSpace(executor, projectID, false);
		XmlDoc.Element namespaceMeta = NameSpaceUtil.describe(null, executor, projectNameSpace);
		XmlDoc.Element projectMeta = namespaceMeta
				.element("namespace/asset-meta/" + Properties.getProjectDocType(executor));
		if (projectMeta == null) {
			throw new Exception(
					"The project meta-data '" + Properties.getProjectDocType(executor) + " is unexpectedly missing");
		}
		XmlDoc.Element currentType = projectMeta.element("type");
		if (currentType != null) {
			currentType.setValue(type);
		} else {
			XmlDoc.Element t = new XmlDoc.Element("type", type);
			projectMeta.add(t);
		}

		// Remove old
		String mid = projectMeta.value("/@id");
		NameSpaceUtil.removeAssetNameSpaceMetaData(null, executor, projectNameSpace, mid);

		// Set new
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", projectNameSpace);
		dm.push("asset-meta", new String[] { "inherit", "false", "propagate", "false" });
		dm.add(projectMeta);
		dm.pop();
		executor.execute("asset.namespace.asset.meta.add", dm.root());
	}

	/**
	 * Send an email to the give user
	 * 
	 * @param executor
	 * @param user     : <domain>:<user>
	 * @throws Throwable
	 */
	static public String sendUserMail(ServiceExecutor executor, String user, String subject, String body)
			throws Throwable {
		// We send an email to the user
		XmlDoc.Element r = User.describeUser(executor, user);
		String email = r.value("user/e-mail");
		if (email == null) {
			return null;
		}
		MailHandler.sendMessage(executor, email, subject, body, null, true, null);
		return email;
	}

	private static List<String> sortProjectIDs(Collection<String> projectIDs) throws Throwable {
		if (projectIDs == null || projectIDs.isEmpty()) {
			return null;
		}
		List<String> list = new ArrayList<String>(projectIDs);
		Collections.sort(list, new Comparator<String>() {

			@Override
			public int compare(String id1, String id2) {
				// proj-<name>-<cid>
				String[] s1 = id1.split("-");
				String[] s2 = id2.split("-");
				// <cid> of form N1.N2.N3
				String t1 = s1[s1.length - 1];
				String t2 = s2[s2.length - 1];
				// Index of N3
				int idx1 = t1.lastIndexOf(".") + 1;
				int idx2 = t2.lastIndexOf(".") + 1;
				Integer i1 = Integer.parseInt(t1.substring(idx1));
				Integer i2 = Integer.parseInt(t2.substring(idx2));
				if (i1 > i2) {
					return 1;
				} else if (i1.equals(i2)) {
					return 0;
				} else {
					return -1;
				}

			}
		});
		return list;
	}

	/**
	 * Fetch the store policies (term=primary, definition=DR) from the dictionary
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static Collection<XmlDoc.Element> storePolicies(ServiceExecutor executor) throws Throwable {
		String dict = Properties.getPrimaryStorePoliciesDictionaryName(executor);
		if (DictionaryUtil.dictionaryExists(executor, dict)) {
			XmlDoc.Element d = DictionaryUtil.describeDictionaryTerms(executor, dict);
			return d.elements("entry");
		} else {
			throw new Exception("The store policies management dictionary '" + dict
					+ "' does not exist. Contact the administrator");
		}

	}

	/**
	 * Is this primary store policy known to us ?
	 * 
	 * @param primaryStoreName
	 * @throws Throwable
	 */
	public static Boolean storePolicyIsKnown(ServiceExecutor executor, String theStorePolicy) throws Throwable {
		Collection<XmlDoc.Element> storePolicies = Project.storePolicies(executor);
		if (storePolicies == null) {
			throw new Exception("No store policies found");
		}
		for (XmlDoc.Element storePolicy : storePolicies) {
			String primary = storePolicy.value("term");
			if (theStorePolicy.equals(primary))
				return true;
		}
		return false;
	}

	/**
	 * Validate that a project ID is of the correct form 'proj-<name>-<cid<' and
	 * that the CID is an allowed CID
	 * 
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public static void validateProjectName(ServiceExecutor executor, String projectID) throws Throwable {
		if (projectID == null) {
			throw new Exception("The project name '" + projectID + "' is null.");
		}
		if (projectID.isEmpty() || projectID.equals("")) {
			throw new Exception("The project name '" + projectID + "' is empty.");

		}
		if (MetaUtil.hasWhiteSpace(projectID)) {
			throw new Exception("The project ID '" + projectID + "' has white space in it. This is not allowed.");
		}

		String[] t = projectID.split("-");
		int n = t.length;
		if (n < 3) {
			throw new Exception("The project ID '" + projectID + "' is not of the correct form 'proj-<name>-<cid>' (split failure)");
		}
		if (!t[0].equals("proj")) {
			throw new Exception("The project ID '" + projectID + "' is not of the correct form 'proj-<name>-<cid>' (leading proj)");
		}
		if (!Util.isCiteableId(t[n-1])) {
			throw new Exception("The project ID '" + projectID + "' is not of the correct form 'proj-<name>-<cid>' (trailing citable='"+t[n-1]+"')");
		}
		String cid = t[n - 1];
		String namedCIDProd = Project.namedCiteableRootValue(executor, Properties.CID_ALLOCATOR_ROOT);
		if (namedCIDProd != null) {
			if (cid.startsWith(namedCIDProd))
				return;
		}
	}

	/**
	 * Try to expand the partial project ID into the full project ID
	 * 
	 * @param id - just the M part of the ID proj-<proj>-UUID.N.M
	 * 
	 */
	public static String expandProjectID(ServiceExecutor executor, String id) throws Throwable {
		if (Util.onlyDigits(id)) {
			String dict = Properties.getProjectNamespaceMapDictionaryName(executor);
			if (dict == null) {
				throw new Exception(
						"The project ID to namespace dictionary name does not exist. Contact the administrator");
			}

			// Get the terms
			XmlDoc.Element terms = DictionaryUtil.describeDictionaryTerms(executor, dict);
			if (terms == null) {
				throw new Exception("There are no terms in the project/namespace map dictionary : " + dict);
			}
			// See if we can match our project child ID
			Collection<String> projectIDs = terms.values("entry/term");
			for (String projectID : projectIDs) {
				int idx = projectID.lastIndexOf(".");
				String child = projectID.substring(idx + 1);
				if (id.equals(child)) {
					return projectID;
				}
			}
		}
		return null;
	}

	/**
	 * Validate that a project ID is of the correct form 'proj-<name>-<cid<' and
	 * that all the project namespaces exist. Returns the parent namespace root.
	 * Throws exception if not valid
	 * 
	 * @param projectID.
	 * @param allowPartialCID if true then allow the projectID to be the short form
	 *                        M where cid=UUID.N.M and the full project ID is found
	 *                        and set in the return vector
	 * @return a vector holding [Parent asset namespace, actual Project ID]
	 * @throws Throwable
	 */
	public static String[] validateProject(ServiceExecutor executor, String projectID, Boolean allowPartialCID)
			throws Throwable {

		// We allow just the child ID M from proj-<name>-UUID.N.M
		// If that's all we have, expand it.
		Boolean expanded = false;
		if (allowPartialCID) {
			String t = expandProjectID(executor, projectID);
			if (t != null) {
				projectID = t;
				expanded = true;
			}
		}

		// Validate if we didn't expand it
		if (!expanded) {
			// Make sure the name is of the correct form
			validateProjectName(executor, projectID);
		}

		// Make sure all namespaces exist
		String nameSpace = Project.assetNameSpace(executor, projectID, false);

		// Check not rubbish
		if (nameSpace.equals("/") || nameSpace.equals("") || nameSpace.isEmpty()) {
			throw new Exception("Project namespace " + nameSpace + " is not valid.");
		}
		if (!NameSpaceUtil.assetNameSpaceExists(executor, null, nameSpace)) {
			throw new Exception("The asset namespace '" + nameSpace + "' does not exist.");
		}
		// Validate that the asset namespace is sitting underneath a valid root
		// namespace.
		//
		// The only way this can go wrong is if the dictionary supplying the map from
		// project ID to namespace gets corrupted or is not updated correctly.
		String s[] = validateProjectNamespace(executor, nameSpace, false, true);
		//
		if (!NameSpaceUtil.metaNameSpaceExists(executor, null, projectID)) {
			throw new Exception("The meta-data namespace '" + projectID + "' does not exist.");
		}
		if (!DictionaryUtil.dictionaryNameSpaceExists(executor, null, projectID)) {
			throw new Exception("The dictionary namespace '" + projectID + "' does not exist.");
		}
		if (!NameSpaceUtil.roleNameSpaceExists(executor, null, projectID)) {
			throw new Exception("The role namespace '" + projectID + "' does not exist.");
		}
		return s;
	}

	/**
	 * Validate that a namespace belongs to a standard project and that it exists
	 * Returns a vector holding the parent asset namespace for the project and the
	 * project ID (proj-<name>-<cid>)
	 * 
	 * @param project   namespace
	 * @param childOnly if true the namespace must be a child of an existing project.
	 *                  if false can be a child or the project root namespace
	 * @param mustExist the namespace must exist else an exception is generated
	 * @return The parent asset namespace and project ID
	 * 
	 * 
	 * @throws Throwable
	 */
	public static String[] validateProjectNamespace(ServiceExecutor executor, String nameSpace, Boolean childOnly,
			Boolean mustExist) throws Throwable {

		// Make sure there is a slash on the front, because that how the root project
		// namespace will come back
		String s1 = nameSpace.substring(0, 1);
		String ns = null;
		if (s1.equals("/")) {
			ns = nameSpace;
		} else {
			ns = "/" + nameSpace;
		}

		if (mustExist && !NameSpaceUtil.assetNameSpaceExists(executor, null, ns)) {
			throw new Exception("Namespace '" + ns + "' does not exist");
		}

		// FInd the bit beginning with 'proj-'
		int idx = ns.indexOf("proj-");
		if (idx <= 0) {
			throw new Exception("The namespace '" + ns + "' is not a valid project or project child namespace.");
		}
		String parentNameSpace = ns.substring(0, idx - 1);

		// Make sure the prefix is a legitimate project root namespace
		Collection<String> rootNameSpaces = assetNameSpaceRoots(executor);
		boolean ok = false;
		int rootDepth = -1;
		int depth = assetNameSpaceDepth(executor, ns);
		for (String rootNameSpace : rootNameSpaces) {
			if (parentNameSpace.equals(rootNameSpace)) {
				// We have a legitimate root namespace
				rootDepth = assetNameSpaceDepth(executor, rootNameSpace);
				ok = true;
			}
		}
		if (!ok) {
			throw new Exception("Namespace '" + nameSpace
					+ "' is not a valid project namespace - it does not contain any of the project root namespaces");
		}

		if (childOnly) {
			// Must be a child of a project root namespace.
			// e.g. /projects/<project-id>/abc
			if (!(depth > (rootDepth + 1))) {
				throw new Exception("The namespace '" + ns + "' is not a valid project child namespace.");
			}
		} else {
			// Can be either a project root namespace or child
			if (!(depth > (rootDepth))) {
				throw new Exception("The namespace '" + ns + "' is not a valid project root or child namespace.");
			}

			// This code was flawed as it asserted it must be the project root namespace
//			if (depth != rootDepth + 1) {
//				throw new Exception("The namespace '" + ns + "' is not a valid project namespace.");
//			}
		}

		// Fish out the project name from the namespace and validate
		String[] parts = ns.split("/");
		String projectName = parts[rootDepth + 1];
		validateProjectName(executor, projectName);
		//
		String[] s = new String[] { parentNameSpace, projectName };
		return s;
	}

	/**
	 * Set the map in the dictionary for this project ID
	 * 
	 * @param executor
	 * @param projectID
	 * @param assetNameSpace
	 */
	public static void addProjectToNameSpaceMap(ServiceExecutor executor, String projectID, String assetNameSpace)
			throws Throwable {
		String dict = Properties.getProjectNamespaceMapDictionaryName(executor);
		DictionaryUtil.addDictionaryTerm(executor, dict, projectID, assetNameSpace);

	}

	/**
	 * Remove the map entry in the dictionary for this project ID
	 * 
	 * @param executor
	 * @param projectID
	 */
	public static void removeProjectFromNameSpaceMap(ServiceExecutor executor, String projectID) throws Throwable {
		String dict = Properties.getProjectNamespaceMapDictionaryName(executor);
		DictionaryUtil.removeDictionaryTerm(executor, dict, projectID);
	}

}
