/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class MailHandler {



	/**
	 * Send a message to the specified users
	 * 
	 * @param executor
	 * @param emails
	 * @param subject
	 * @param body
	 * @param bodyType mime type for body, if  null, defaults to text/plain
	 * @param bcc true for BCC else all can see each other's email
	 * @param from 
	 * @param w  The Writer (can be null) will have a field "to" or "bcc" appended to it for each recipient
	 * @throws Throwable
	 */
	public static void sendMessage (ServiceExecutor executor, Collection<String> emails, String subject, String body, String bodyType, Boolean bcc, String
			from, XmlWriter w) throws Throwable {
		if (emails==null) return;
		XmlDocMaker dm = new XmlDocMaker("args");
		if (bodyType!=null) {
			dm.add("body", new String[]{"type", bodyType}, body);
		} else {
			dm.add("body", body);
		}
		dm.add("subject", subject);
		if (from!=null) {
			dm.add("from", from);
		}

		if (w!=null && from!=null) {
			w.add("from", from);
		}
		//
		for (String email : emails) {
			if (bcc) {
				if (w!=null) {
					w.add("bcc", email);
				}
				dm.add("bcc", email);
				//
				dm.add("to", from);     // Need one 'to' field (for bcc) so copy self 
				if (w!=null) {
					w.add("to", from);
				}
			} else {
				if (w!=null) {
					w.add("to", email);
				}
				dm.add("to", email);
			}
		}

		executor.execute("mail.send", dm.root());
	}


	/**
	 * Send a message to the specified user
	 * 
	 * @param executor
	 * @param email
	 * @param subject
	 * @param body
	 * @param bodyType mime type for body, if  null, defaults to text/plain
	 * @param async
	 * @param from
	 * @throws Throwable
	 */
	public static void sendMessage(ServiceExecutor executor, String email, String subject, String body, String bodyType, boolean async, String from) throws Throwable {
		sendMessage (executor, email, subject, body, bodyType, async, null, null, null, from);
	}



	/**
	 * Send a message to the specified user and include an optional file as an attachment
	 * 
	 * @param executor
	 * @param email - the to email address
	 * @param subject
	 * @param body
	 * @param bodyType mime type for body, if  null, defaults to text/plain
	 * @param async
	 * @param f The file. Can be null
	 * @param fileName what name should the file have when emailed
	 * @param mimeType the mime type of the attachment
	 * @param from - the from email address. Can be null (defaults to domain default)
	 * @throws Throwable
	 */
	public static void sendMessage(ServiceExecutor executor, String email, String subject, String body, String bodyType, boolean async,
			File f, String fileName, String mimeType, String from) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("to", email);
		dm.add("subject", subject); // Required by mail.send
		if (body != null) {
			if (bodyType!=null) {
				dm.add("body", new String[]{"type", bodyType}, body);
			} else {
				dm.add("body", body);
			}
		}
		dm.add("async", async);
		if (from!=null) {
			dm.add("from", from);
		}
		try {
			if (f!=null) {
				FileInputStream fis = new FileInputStream(f);
				PluginService.Input in = new PluginService.Input(fis,f.length(), mimeType, null);
				PluginService.Inputs ins = new PluginService.Inputs(in);
				if (fileName!=null || mimeType!=null) {
					dm.push("attachment");
					dm.add("name", fileName);
					dm.add("type", mimeType);
					dm.pop();
				}
				executor.execute("mail.send",dm.root(),ins,null);
			} else {
				executor.execute("mail.send", dm.root());
			}
		} catch (Throwable t) {
			// Just drop. Don't want exceptions just because we can't send mail
			System.out.println("Exception '" + t.getMessage() + "'" + body);
		}
	}
}
