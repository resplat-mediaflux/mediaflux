/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;


import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;


public class MetaUtil {

	/**
	 * Return descriptions of document types in the project document namespace
	 * 
	 * @param executor
	 * @param namespace
	 * @return  null if none
	 * @throws Throwable
	 */
	public static Collection<XmlDoc.Element> describeDocumentTypes (ServiceExecutor executor, String namespace) throws Throwable {
		XmlDocMaker dm  = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		XmlDoc.Element r = executor.execute("asset.doc.type.describe", dm.root());
		if (r==null) return null;
		return r.elements("type");
	}

	/**
	 * Return list of document types in the project document namespace
	 * 
	 * @param executor
	 * @param namespace
	 * @return  null if none
	 * @throws Throwable
	 */
	public static Collection<String> listDocumentTypes (ServiceExecutor executor, String namespace) throws Throwable {
		XmlDocMaker dm  = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		XmlDoc.Element r = executor.execute("asset.doc.type.list", dm.root());
		if (r==null) return null;
		return r.values("type");
	}

	/**
	 * Return list  of dictionaries in the project dictionary namespace
	 * 
	 * @param executor
	 * @param namespace
	 * @return  null if none
	 * @throws Throwable
	 */
	public static Collection<String> listDictionaries (ServiceExecutor executor, String namespace) throws Throwable {
		XmlDocMaker dm  = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		XmlDoc.Element r = executor.execute("dictionary.list", dm.root());
		if (r==null) return null;
		return r.values("name");
	}


	/**
	 * Remove the named atrtibute from the document
	 * 
	 * @param executor
	 * @param doc
	 * @param attributeName
	 * @throws Throwable
	 */
	public static void removeAttribute (XmlDoc.Element doc, String attributeName) throws Throwable {
		XmlDoc.Attribute attr = doc.attribute(attributeName);
		if (attr != null) {
			doc.remove(attr);
		}
	}

	public static boolean hasWhiteSpace (String s) throws Throwable {
		Pattern pattern = Pattern.compile("\\s");
		Matcher matcher = pattern.matcher(s);
		return matcher.find();
	}


}

