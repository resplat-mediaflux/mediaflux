package au.org.vicnode.mflux.plugin.util;

import java.util.List;

import arc.xml.XmlDoc;

public class XmlUtils {

	public static void toShellText(XmlDoc.Element e, boolean self, StringBuilder sb) {

		if (self) {
			sb.append(" :").append(e.name());
			List<XmlDoc.Attribute> attrs = e.attributes();
			if (attrs != null) {
				for (XmlDoc.Attribute attr : attrs) {
					sb.append(String.format(" -%s \"%s\"", attr.name(), attr.value()));
				}
			}
			if (e.value() != null) {
				sb.append(" \"" + e.value() + "\"");
			}
		}

		List<XmlDoc.Element> ses = e.elements();
		if (ses != null && !ses.isEmpty()) {
			if (self) {
				sb.append(" <");
			}
			for (XmlDoc.Element se : ses) {
				toShellText(se, true, sb);
			}
			if (self) {
				sb.append(" >");
			}
		}
	}

	public static String toShellText(XmlDoc.Element e, boolean self) {
		StringBuilder sb = new StringBuilder();
		toShellText(e, self, sb);
		return sb.toString();
	}

	public static String getServiceCommand(String serviceName, XmlDoc.Element args) {
		StringBuilder sb = new StringBuilder();
		sb.append(serviceName);
		toShellText(args, false, sb);
		return sb.toString();
	}

}
