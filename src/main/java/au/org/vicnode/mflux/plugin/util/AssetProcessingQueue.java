/** 
 * @author Neil Killeen
 *
 * Class to provide asset processing queue functions
 * and associated  functionality (e.g. replication) with
 * namespaces
 * 
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;


import java.util.Collection;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class AssetProcessingQueue {



	/**
	 * Remove all  asset processing queues from a project parent namespace
	 * 
	 * @param executor
	 * @param projectID
	 * @throws Throwable
	 */
 	public static void removeAllQueuesFromProject (ServiceExecutor executor, String projectID, XmlWriter w) throws Throwable {
		String namespace = Project.assetNameSpace(executor, projectID, false);

		// Big stick. Don't have anything finer grained at present
		removeAllQueuesFromNamespace (executor, namespace);
		if (w!=null) {
			w.add("queue", new String[] {"namespace", namespace, "unset", "true"}, "all");
		}
	}


	/**
	 * Does the given asset processing queue exist ?
	 * 
	 * @param executor
	 * @return Boolean
	 * @throws Throwable
	 */
	public static Boolean queueExists (ServiceExecutor executor, String queueName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", queueName);
		XmlDoc.Element r = executor.execute("asset.processing.queue.exists", dm.root());
		return r.booleanValue("exists");
	}





	
	/**
	 * Add an asset processing queue with a namespace. 
	 * 
	 * @param executor
	 * @param namespace
	 * @param queueName
	 * @param enqueueName the name that the queue will be registered as with the
	 *    namespace
	 * @param where filter to be applied before the asset event will
	 *   be enqueued 
	 * @throws Throwable
	 */
	public static void addQueueToNamespace (ServiceExecutor executor, 
			String namespace, String queueName, String enqueueName, String where) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.push("enqueue");
		dm.add("event", "create");
		dm.add("event", "modify");
		dm.add("event", "content-create");
		dm.add("event", "content-modify");
		dm.add("event", "content-check-sum");
		dm.add("name", enqueueName);
		dm.add("queue", queueName);
		dm.add("where", where);
		dm.pop();
		dm.add("namespace", namespace);
		executor.execute("asset.namespace.enqueue.add", dm.root()); 	
	}
	
	/**
	 * Remove an asset processing queue from a namespace. 
	 * 
	 * @param executor
	 * @param namespace
	 * @param enqueuename the name that the queue will be registered as with the
	 *    namespace
	 * @throws Throwable
	 */
	public static void removeQueueFromNamespace (ServiceExecutor executor, 
			String namespace, String enqueueName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", enqueueName);
		dm.add("namespace", namespace);
		executor.execute("asset.namespace.enqueue.remove", dm.root()); 	
	}


	/**
	 * Remove ALL  asset processing queues associated with a namespace
	 * 
	 * @param executor
	 * @param namespace
	 * 
	 * @throws Throwable
	 */
	private static void removeAllQueuesFromNamespace (ServiceExecutor executor, 
			String namespace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		executor.execute("asset.namespace.enqueue.all.remove", dm.root());
	}

	/**
	 * Reset an asset processing queue.  This will cause it to
	 * re-execute any failed transactions
	 * 
	 * @param executor
	 * @param name
	 * @throws Throwable
	 */
	public static void resetQueue (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		executor.execute("asset.processing.queue.reset", dm.root());
	}




	/**
	 * Optionally suspend and then destroy the asset processing queue.  
	 * 
	 * @param executor
	 * @param name
	 * @throws Throwable
	 */
	public static void destroyQueue (ServiceExecutor executor, String name, Boolean suspend) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (suspend) {
			dm.add("name", name);
			executor.execute("asset.processing.queue.suspend", dm.root());
		}
		//
		dm = new XmlDocMaker("args");
		dm.add("name", name);
		executor.execute("asset.processing.queue.destroy", dm.root());
	}

	/**
	 * Suspend the asset processing queue.  
	 * 
	 * @param executor
	 * @param name
	 * @throws Throwable
	 */
	public static void suspendQueue (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		executor.execute("asset.processing.queue.suspend", dm.root());
	}


	/**
	 * Describe the asset processing queue.  
	 * 
	 * @param executor
	 * @param name
	 * @param activeEntries if true then include all active entries in the description
	 * @throws Throwable
	 */
	public static XmlDoc.Element describeQueue (ServiceExecutor executor, String name, Boolean activeEntries) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		if (activeEntries) {
			dm.add("max-nb-active", "infinity");
		}
		return executor.execute("asset.processing.queue.describe", dm.root());
	}
	
	/**
	 * Return the asset IDs fpr all current (executing and queued) entries in the queue
	 * 
	 * @param executor
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public static Collection<String> getEntryAssetIDs (ServiceExecutor executor,
			                                           String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		dm.add("size", "infinity");
		XmlDoc.Element r = executor.execute("asset.processing.queue.entry.describe", dm.root());
		if (r!=null) {
			return r.values("entry/asset");
		} else {
			return null;
		}
	}

	/**
	 * Add an asset to the  asset processing queue.  
	 * 
	 * @param executor
	 * @param name
	 * @param id the asset id
	 * @throws Throwable
	 */
	public static XmlDoc.Element addAssetToQueue (ServiceExecutor executor, String name, String id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		dm.add("id", id);
		return executor.execute("asset.processing.queue.add", dm.root());
	}


	/**
	 * Look for an enqueue element in the asset namespace meta-data
	 * and return the enqueue element if it matches the given name
	 * Otherwise return null
	 * 
	 * @param nsMeta - the meta-data describing the asset namespace
	 * @param enqueueName - the name of the namespace enqueue element of interest 
	 */
	public static XmlDoc.Element getEnqueueElement (XmlDoc.Element nsMeta, String enqueueName) throws Throwable {
		XmlDoc.Element enq = 
				nsMeta.element("namespace/enqueue[@name='"+enqueueName+"']");
		if (enq!=null) {
			// See if its the replication queue
			String enqName = enq.value("@name");
			if (enqName.equals(enqueueName)) {
				return enq;
			} else {
				return null;
			}
		}
		return null;
	}

	
	
	
}
