/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;


import java.util.Collection;
import java.util.Collections;
import java.util.Vector;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;


public class User {


	/**
	 * Describe the current user
	 * 
	 * @param executor
	 * @param user : <domain>:<user>
	 * @return
	 * @throws Throwable
	 */
	public static XmlDoc.Element describeUser (ServiceExecutor executor) throws Throwable {
		return executor.execute("user.self.describe");
	}

	/**
	 * Describe a user
	 * 
	 * @param executor
	 * @param user : <domain>:<user>
	 * @return
	 * @throws Throwable
	 */
	public static XmlDoc.Element describeUser (ServiceExecutor executor, String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		String[] t = user.split(":");
		if (t.length!=2) {
			throw new Exception ("Did not find <domain>:<user> found in '" + user + "'");
		}

		dm.add("domain", t[0]);
		dm.add("user", t[1]);
		return executor.execute("user.describe", dm.root());
	}

	/**
	 * Describe a user
	 * 
	 * @param executor
	 * @param domain
	 * @param user
	 * @return
	 * @throws Throwable
	 */
	public static XmlDoc.Element describeUser (ServiceExecutor executor, String domain, String user) throws Throwable {
		return describeUser (executor, domain+":"+user);
	}


	/**
	 * Find user by email
	 * 
	 * @param domain authentication domain of interest
	 * @param email user email address
	 */
	public static Collection<XmlDoc.Element> findUser (ServiceExecutor executor, String domain, String email) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("email", email);
		dm.add("domain", domain);
		XmlDoc.Element r = executor.execute("unimelb.user.search", dm.root());
		if (r==null) {
			return null;
		}
		return r.elements("user");
	}

	/**
	 * Fish out the user's email address from their account description
	 * 
	 * @param executor
	 * @param user : <domain>:<user>
	 * @return
	 * @throws Throwable
	 */
	public static String getUserEMail (ServiceExecutor executor, String user) throws Throwable {
		XmlDoc.Element r = describeUser (executor, user);
		return r.value("user/e-mail");
	}

	/**
	 * Fish out the current user's email address from their account description
	 * 
	 * @param executor
	 * @param user : <domain>:<user>
	 * @return
	 * @throws Throwable
	 */
	public static String getUserEMail (ServiceExecutor executor) throws Throwable {
		XmlDoc.Element r = describeUser (executor);
		return r.value("user/e-mail");
	}


	/**
	 * Returns a list of users holding the specified role name.  If none, will return
	 * an empty list
	 * 
	 * @param executor
	 * @param roleName
	 * @param all if true all users are returned. If false,
	 *       only enabled users are returned
	 * @return empty or populated Vector
	 * @throws Throwable
	 */
	public static Vector<String> haveRole (ServiceExecutor executor, 
			String roleName, Boolean all) throws Throwable {
		Vector<String> res = new Vector<String>();
		XmlDocMaker dm = new XmlDocMaker("args");
		if (all) {
			dm.add("include-disabled", true);
		}
		dm.add("role", new String[]{"type", "role"}, roleName);
		XmlDoc.Element r = executor.execute("unimelb.user.describe", dm.root());
		if (r==null) {
			return res;
		}
		Collection<XmlDoc.Element> users = r.elements("user");	
		if (users==null) return res;

		for (XmlDoc.Element user : users) {
			String domain = user.value("@domain");
			String userName = user.value("@user");
			if (isAuthDomainEnabled(executor,domain)) {


				// Actually check the user exists
				// The use case is users no longer in AD
				// but MF leaves a token behind that causes
				// user.describe to find.  Later, services like
				// actor.describe will fail
				// TBD nebk : remove when defect fixed (see ticket 4521)
				if (exists(executor, domain, userName)) {
					Boolean enabled = user.booleanValue("@enabled",  true);
					if (all || (!all && enabled)) {
						String t = domain + ":" + userName;
						res.add(t);
					}
				}
			}
		}
		return res;
	}	

	public static Boolean isAuthDomainEnabled (ServiceExecutor executor, String domain) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		return executor.execute("authentication.domain.enabled", dm.root()).booleanValue("enabled");
	}
	/**
	 * Does the user actually exist ?
	 */
	public static Boolean exists (ServiceExecutor executor, String domain,
			String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		XmlDoc.Element r = executor.execute("user.exists", dm.root());
		return r.booleanValue("exists");
	}

	/**
	 * Returns a list of identity token actor names and IDs holding the specified role name.  If none, will return
	 * an empty list
	 * 
	 * @param executor
	 * @param roleName
	 * @return empty or populated vector
	 * @throws Throwable
	 */
	public static Collection<XmlDoc.Element> identityTokensHaveRole (ServiceExecutor executor, String roleName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("role", new String[] {"type","role"}, roleName);
		dm.add("type", "identity");
		dm.add("relationship","direct");
		XmlDoc.Element re  = executor.execute("actors.granted", dm.root());
		if(re.elementExists("actor")) {
			return re.elements("actor");
		}else {
			return Collections.emptyList();
		}
	}	

	/**
	 * Get the secure identity ID from the actor
	 * 
	 * @param executor
	 * @param tokenActor
	 * @return
	 * @throws Throwable
	 */
	public static String getTokenID (ServiceExecutor executor, XmlDoc.Element tokenActor) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", tokenActor.value("@id"));
		XmlDoc.Element r = executor.execute("secure.identity.token.for.actor.describe", dm.root());
		return r.value("identity/@id");
	}
	/**
	 * See if user holds the specified role name.  
	 * 
	 * @param executor
	 * @param authority - only works for authorities with no protocol attribute
	 * @param domain
	 * @param user
	 * @param roleName
	 * @return
	 * @throws Throwable
	 */
	public static Boolean hasRole (ServiceExecutor executor, String authority, String domain, String user, String roleName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		String t = domain+":"+user;
		if (authority!=null) t = authority + ":" + t;
		dm.add("name", t);
		dm.add("type", "user");
		dm.add("role", new String[]{"type", "role"}, roleName);
		XmlDoc.Element r = executor.execute("actor.have", dm.root());
		if (r==null) return false;
		return r.booleanValue("actor/role");
	}     

	/**
	 * See if user holds any of the specified role names. If none, then keep.
	 * 
	 * @param executor
	 * @param domain
	 * @param user
	 * @param roleNames
	 * @return
	 * @throws Throwable
	 */
	public static Boolean hasRole (ServiceExecutor executor,  String domain, String user, Collection<String> roleNames) throws Throwable {
		if (roleNames==null) return true;

		XmlDocMaker dm = new XmlDocMaker("args");
		String t = domain+":"+user;
		dm.add("name",t);
		dm.add("type", "user");
		if (roleNames!=null) {
			for (String roleName :roleNames) {
				if (Util.roleExists(executor, roleName)) {
					dm.add("role", new String[]{"type", "role"}, roleName);
				}
			}
		}
		XmlDoc.Element r = executor.execute("actor.have", dm.root());
		if (r==null) return false;
		Collection<Boolean> results = r.booleanValues("roles");
		if (results==null) return true;  // Means no roles specified, but first line should take care of that
		for (Boolean result :results) {
			if (result) return true;
		}
		return false;
	}       


	/**
	 * See if user holds the specified role name.  
	 * 
	 * @param executor
	 * @param name
	 * @param roleName
	 * @return
	 * @throws Throwable
	 */
	public static Boolean hasRole (ServiceExecutor executor,String name, String roleName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		dm.add("type", "user");
		dm.add("role", new String[]{"type", "role"}, roleName);
		XmlDoc.Element r = executor.execute("actor.have", dm.root());
		if (r==null) return false;
		return r.booleanValue("actor/role");
	}       

	/**
	 * See if the calling user holds the system-administrator role
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static boolean hasSystemAdministratorRole(ServiceExecutor executor) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("role", new String[] { "type", "role" }, "system-administrator");
		return executor.execute("actor.self.have", dm.root()).booleanValue("role");
	}



	/**
	 * Grant or revoke a role (type 'role')
	 * 
	 * @param executor
	 * @param user : <domain>:<user>
	 * @param role
	 * @param grant
	 * @throws Throwable
	 */
	static public void  grantRevokeRole (ServiceExecutor executor, String user, String role, Boolean grant) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", user);
		dm.add("type", "user");
		dm.add("role", new String[]{"type", "role"}, role);
		if (grant) {
			executor.execute("actor.grant", dm.root());
		} else {
			executor.execute("actor.revoke", dm.root());
		}
	}

	/**
	 * Grant or revoke a permission to a user
	 * 
	 * @param executor
	 * @param user : <domain>:<user>
	 * @param projectRole
	 * @param grant
	 * @throws Throwable
	 */
	static public void  grantRevokePermToUser (ServiceExecutor executor, String user, String access, String resourceType, 
			String resourceName, Boolean grant) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", user);
		dm.add("type", "user");
		dm.push("perm");
		dm.add("access", access);
		dm.add("resource", new String[]{"type", resourceType}, resourceName);
		if (grant) {
			executor.execute("actor.grant", dm.root());
		} else {
			executor.execute("actor.revoke", dm.root());
		}
	}

	/**
	 * Grant or revoke a permission to a role
	 * 
	 * @param executor
	 * @param user : <domain>:<user>
	 * @param projectRole
	 * @param grant
	 * @throws Throwable
	 */
	static public void  grantRevokePermToRole (ServiceExecutor executor, String role, String access, String resourceType, 
			String resourceName, Boolean grant) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", role);
		dm.add("type", "role");
		dm.push("perm");
		dm.add("access", access);
		dm.add("resource", new String[]{"type", resourceType}, resourceName);
		if (grant) {
			executor.execute("actor.grant", dm.root());
		} else {
			executor.execute("actor.revoke", dm.root());
		}
	}




}
