/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.plugin.util;

import java.util.List;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;


public class DocTypeUtil {


	/**
	 * Replace references to a dictionary name in a doc type with a new dictionary name
	 * @param executor
	 * @param docType
	 * @param oldDictioaryName
	 * @param newDictionaryName
	 * @throws Throwable
	 */
	public static void replaceDictionaryInDocType (ServiceExecutor executor, String docType,
			String oldDictionaryName, String newDictionaryName) throws Throwable {
		dictionaryExists (executor, newDictionaryName);
		replaceDictionary(executor, docType, oldDictionaryName, newDictionaryName);
	}
	
	/**
	 * Create document namespace
	 * 
	 * @param name - name of namespace to create
	 * @param description - description of namespace
	 */
	static public void createDocumentNameSpace (ServiceExecutor executor, 
			String name, String description) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", name);
		if (description!=null) {
			dm.add("description", description);
		}
		Util.execute(executor, null, "asset.doc.namespace.create", dm);
	}

	
	
	// 
	private static void dictionaryExists (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		if (!executor.execute("dictionary.exists", dm.root()).booleanValue("exists")) { 
			throw new Exception ("The dictionary '" + name + "' does not exist");
		}
	}

	private static void replaceDictionary(ServiceExecutor executor, String type,
			String dictionary, String newDictionary) throws Throwable {
		XmlDoc.Element te = executor
				.execute("asset.doc.type.describe",
						"<args><type>" + type + "</type></args>", null, null)
						.element("type");
		List<XmlDoc.Element> elements = te.elements("definition/element");
		if (elements != null) {
			for (XmlDoc.Element element : elements) {
				replaceDictionary(element, dictionary, newDictionary);
			}
		}
		XmlDoc.Element e = te.element("access");
		if (e != null) {
			te.remove(e);
		}
		e = te.element("creator");
		if (e != null) {
			te.remove(e);
		}
		e = te.element("ctime");
		if (e != null) {
			te.remove(e);
		}

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("type", type);
		dm.add(te, false);

		// modify the doc type
		executor.execute("asset.doc.type.update", dm.root());
	}

	private static void replaceDictionary(XmlDoc.Element element,
			String dictionary, String newDictionary) throws Throwable {
		if (element.value("@type").equals("enumeration")) {
			XmlDoc.Element dictionaryElement = element
					.element("restriction[@base='enumeration']/dictionary");
			if (dictionaryElement != null
					&& dictionary.equals(dictionaryElement.value())) {
				dictionaryElement.setValue(newDictionary);
			}
		}
		List<XmlDoc.Element> attributes = element.elements("attribute");
		if (attributes != null) {
			for (XmlDoc.Element attribute : attributes) {
				if (attribute.value("@type").equals("enumeration")) {
					XmlDoc.Element dictionaryElement = attribute.element(
							"restriction[@base='enumeration']/dictionary");
					if (dictionaryElement != null
							&& dictionary.equals(dictionaryElement.value())) {
						dictionaryElement.setValue(newDictionary);
					}
				}
			}
		}
		List<XmlDoc.Element> subElements = element.elements("element");
		if (subElements != null) {
			for (XmlDoc.Element subElement : subElements) {
				replaceDictionary(subElement, dictionary, newDictionary);
			}
		}
	}



}
