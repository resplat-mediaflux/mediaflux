/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixMapDomainIdentityRemove extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectPosixMapDomainIdentityRemove()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
		_defn.add(new Interface.Element("domain", StringType.DEFAULT,
				"Which domain should the identity be removed for", 1, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Remove the authentication domain identities for the specified authentication domain from the posix map for SMB protocol.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.map.domain.identity.remove";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Parse arguments
		String theProjectID = args.stringValue("project-id");
		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) return;
		//
		String domain = args.value("domain");
		
		// We don't really care if the domain exists or not,
		// we just want to remove that meta-data
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		XmlDoc.Element r = executor().execute("authentication.domain.exists", dm.root());
		if (r.booleanValue("exists")) {
			w.add("domain", new String[] {"exists", "true"}, domain);
		} else {
			w.add("domain", new String[] {"exists", "false"}, domain);
		}


		// Iterate over projects
		for (String projectID : projectIDs) {
			// Remove specified domain identity
			removeDomainIdentity(executor(), projectID, domain, w);
		}
	}


	private  void removeDomainIdentity (ServiceExecutor executor, String projectID, 
			String domain, XmlWriter w) throws Throwable {

		// Describe the identities
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("map", projectID);
		XmlDoc.Element r = executor.execute("posix.fs.identity.domain.identity.describe", dm.root());
		if (r==null) {
			return;
		}
		Collection<XmlDoc.Element> identities = r.elements("identity");
		for (XmlDoc.Element identity : identities) {
			String value = identity.value();
			String protocol = identity.value("@protocol");
			String dmn = identity.value("domain");
			if (protocol.equals("smb") && dmn.equals(domain)) {
				// Remove it
				dm = new XmlDocMaker("args");
				dm.add("map", projectID);
				dm.add("identity", new String[] {"protocol", protocol}, value);
				executor.execute("posix.fs.identity.domain.identity.remove", dm.root());
				w.push("project");
				w.add("id", projectID);
				w.add(identity);
				w.pop();
			}
		}
	}
}
