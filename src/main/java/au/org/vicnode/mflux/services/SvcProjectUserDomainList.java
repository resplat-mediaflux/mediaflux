/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;

public class SvcProjectUserDomainList extends PluginService {

	private static final String SERVICE_NAME = 	Properties.SERVICE_ROOT + "project.user.domain.list";
	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectUserDomainList()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "Domain of interest.", 1, 1));
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public boolean canBeAborted() {

		return true;
	}

	public int maxNumberOfOutputs() {
		return 0;
	}

	public int minNumberOfOutputs() {
		return 0;
	}

	public String description() {
		return "List users of a project that have an account in the given domain. Includes the collection ID and owner of the project. The listing is also written to a server side CSV file (named as the service in the volatile/tmp folder.";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String theProjectID = args.value("project-id");
		String theDomain = args.value("domain");

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID != null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else {
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size() == 0) {
			return;
		}

		// Iterate over projects
		File f = PluginTask.createTemporaryFile(SERVICE_NAME + ".csv");
		try {
			PrintWriter csv = new PrintWriter(new BufferedWriter(new FileWriter(f)));
			csv.printf("user, user-name, user-email, project-id, collection-id, owner-name, owner-email\n");

			for (String projectID : projectIDs) {
				PluginTask.checkIfThreadTaskAborted();
				w.mark();
				w.push("project");
				w.add("project-id", projectID);
				Boolean some = false;
				XmlDoc.Element meta = Project.describeNameSpace(null, executor(), projectID, false);
				String docType = Properties.getCollectionDocType(executor());
				String key = meta.value("namespace/asset-meta/"+docType+"/key");
				w.add("key", key);
				XmlDoc.Element owner = meta.element("namespace/asset-meta/"+docType+"/owner");
				w.add(owner);
				String ownerName = owner.value("first") + " " + owner.value("last");
				String ownerEMAIL = owner.value("email");
				//
				Vector<String> users = Project.listUsers(executor(), projectID);
				for (String user : users) {
					XmlDoc.Element userMeta = User.describeUser(executor(), user);
					String domain = userMeta.value("user/@domain");
					if (domain.equals(theDomain)) {
						String userName = userMeta.value("user/@user");
						String name = userMeta.value("user/name");
						String email = userMeta.value("user/e-mail");
						w.push("user");
						w.add("domain", domain);
						w.add("username", userName);
						w.add("name", name);
						w.add("email", email);
						w.pop();
						some = true;
	                    csv.printf("%s,%s,%s,%s,%s,%s,%s\n", 
	                    		domain+":"+userName, name, email, projectID, key, ownerName, ownerEMAIL);
					}
				}

				w.pop();
				if (!some) {
					w.resetToMark();
				}
			}
			csv.close();
		}  catch (Throwable e) {
			// delete temp file in case of runtime exception (aborted).
			PluginTask.deleteTemporaryFile(f);
			throw e;
		}

	}
}





