/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.replication;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectReplicateQueueUnset extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectReplicateQueueUnset()  throws Throwable {
		_defn = new Interface();
		// SPecialiased help so don't use the SvcProjectDescribe.addProjectIDs function
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT, "The ID of the project. Should be of the form 'proj-<name>-<cid>'. It will only attempt to unset projects that are currently using the replication asset processing queue. So if you default to all projects, then it will unset all projects currently being replicated via the replication queue.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("all", BooleanType.DEFAULT, "If you don't specify a project all projects are considered.  Set this argument to true (default false) if you really want to do this. Use with care as all replication state is lost once you unset the queue from the project namespace.", 0, 1));
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Unsets the standard asset processing queue which handles replications on the project asset namespace for the selected projects.  Currently unsets all queues associated with the namespace.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.replicate.queue.unset";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		Collection<String> theProjectIDs = args.values("project-id");	
		Boolean all = args.booleanValue("all", false);
		
		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs!=null) {
			if (theProjectIDs.size()>0) {
				for (String theProjectID : theProjectIDs) {
					String t[] = Project.validateProject(executor(), 
							theProjectID, allowPartialCID);
					projectIDs.add(t[1]);
				}

			}
		} else{
			if (!all) {
				throw new Exception("You must set all==true for all projects");
			}
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) {
			return;
		}

	
		// Set queue
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
			String q = ProjectReplicate.isProjectReplicated(ph);
			if (q!=null) { 
				w.push("project");
				w.add("id", new String[] {"queue", q}, ph.id());
				ProjectReplicate.unsetStandardQueueOnProject(executor(), projectID, w);
				w.pop();
			}
		}
	}



}
