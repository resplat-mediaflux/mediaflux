package au.org.vicnode.mflux.services.organisation;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcOrganisationList extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "organisation.list";

    private Interface _defn;

    public SvcOrganisationList() {
        _defn = new Interface();
        _defn.add(new Interface.Element("count-users", BooleanType.DEFAULT,
                "Count the number of users associated with each organisation. Defaults to false.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "list research organisations.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return false;
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        boolean countUsers = args.booleanValue("count-users", false);
        String dict = Properties.getResearchOrgDict(executor());
        Collection<String> orgs = executor()
                .execute("dictionary.entries.list", "<args><dictionary>" + dict + "</dictionary></args>", null, null)
                .values("term");
        String userDocType = countUsers ? Properties.getUserAccountsDocType(executor()) : null;
        if (orgs != null && !orgs.isEmpty()) {
            for (String org : orgs) {
                if (countUsers) {
                    PluginTask.checkIfThreadTaskAborted();
                    long nbUsers = countUsers(executor(), userDocType, org);
                    w.add("organisation", new String[] { "nb-users", Long.toString(nbUsers) }, org);
                } else {
                    w.add("organisation", org);
                }
            }
        }
        w.add("count", orgs == null ? 0 : orgs.size());
    }

    static long countUsers(ServiceExecutor executor, String docType, String organisation) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "xpath(" + docType + "/organisation/name)='" + organisation.replace("'", "\\'") + "'");
        dm.add("action", "count");
        return executor.execute("asset.query", dm.root()).longValue("value");
    }

}
