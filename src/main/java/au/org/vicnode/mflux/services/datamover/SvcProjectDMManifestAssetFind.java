/**
 * @author Neil Killeen
 * <p>
 * Copyright (c) 2016, The University of Melbourne, Australia
 * <p>
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.datamover;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.services.SvcProjectDescribe;



public class SvcProjectDMManifestAssetFind extends PluginService {

	static Boolean ALLOW_PARTIAL_CID = true;
	private static String SERVICE_NAME = Properties.SERVICE_ROOT + "project.DM.manifest.asset.find";
	private static String ADMIN_NAMESPACE = "/local-admin/instrument-upload-manifests";

	private Interface _defn = null;

	public SvcProjectDMManifestAssetFind() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn, ALLOW_PARTIAL_CID);

		Interface.Element ie = new Interface.Element("admin", BooleanType.DEFAULT,
				"Also find (default true) relevant shareables to this project that are in /local-admin/instrument-upload-manifests", 0, 1);
		_defn.add(ie);
		}


	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Finds all manifest assets 1) in the project tree and 2) opitionally in /local-admin/instrument-upload-manifests for which the manifest collection pertains to this project.";
	}



	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {
		return true;
	}



	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {


		// Parse
		String theProjectID = args.stringValue("project-id");
		Boolean doAdmin = args.booleanValue ("admin", true);

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, ALLOW_PARTIAL_CID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;

		// Iterate over projects
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			//
			w.push("project", new String[] {"id", projectID});		
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			//
			w.push("project-manifests");
			Collection<XmlDoc.Element> manifests = findProjectManifestAssets (executor(), ph.nameSpace());
			if (manifests!=null) {
				for (XmlDoc.Element manifest : manifests) {
					Boolean hasShareableMeta = manifest.elementExists("meta/unimelb:shareable-upload-manifest");
					w.add ("manifest",
							new String[]{"has-shareable-meta", hasShareableMeta.toString(),
									"asset-id", manifest.value("@id")},
							manifest.value("path"));
				}
			}
			w.pop();

			//			
			if (doAdmin) {
				PluginTask.checkIfThreadTaskAborted();
				w.push("admin-manifests");
				manifests = findAdminManifestAssets(executor(), projectID);
				if (manifests!=null) {
					for (XmlDoc.Element manifest : manifests) {
						w.add ("manifest", new String[]{"has-shareable-meta", "true",
								"asset-id", manifest.value("@id"), 
								"shareable-collection", 
								manifest.value("meta/unimelb:shareable-upload-manifest/shareable/namespace")},
								manifest.value("path"));
					}
				}
				w.pop();
			}
			w.pop();
		}

	}


	/**
	 * Find the project manifest assets
	 * 
	 * @param executor
	 * @param namespace
	 * @return
	 * @throws Throwable
	 */
	public static Collection<XmlDoc.Element>  findProjectManifestAssets (ServiceExecutor executor, String namespace) throws Throwable {
		// It is  quicker to find ALL Manifest assets in the system
		// and then filter them by path than to make a query
		// involving the specific collection and a literal query
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("size", "infinity");
		// Quicker in this order for large projects
		dm.add("where", "name='__manifest.csv'");
		dm.add("where", "namespace>='"+namespace+"'");
		dm.add("action", "get-meta");
		return executor.execute("asset.query", dm.root()).elements();
	}



	/**
	 * Find all the local admin manifest assets relevant to this project
	 * 
	 * @param executor
	 * @param projID
	 * @return
	 * @throws Throwable
	 */
	public static Vector<XmlDoc.Element>  findAdminManifestAssets (ServiceExecutor executor,
			String projID) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", "namespace>='"+ADMIN_NAMESPACE+"'");
		dm.add("where", "xpath(unimelb:shareable-upload-manifest/shareable/namespace) has value");
		dm.add("size", "infinity");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		if (r==null) {
			return null;
		}
		Collection<String> ids = r.values("id");
		if (ids==null) {
			return null;
		}
		Vector<XmlDoc.Element> assets = new Vector<XmlDoc.Element>();
		for (String id : ids) {
			XmlDoc.Element meta = AssetUtil.getAssetMeta(executor, id);
			String path = meta.value("meta/unimelb:shareable-upload-manifest/shareable/namespace");		
			if (path!=null && path.contains(projID)) {
				assets.add(meta);
			} 
		}
		return assets;
	}
}




