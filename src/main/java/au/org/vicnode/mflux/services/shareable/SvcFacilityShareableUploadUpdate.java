package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;

public class SvcFacilityShareableUploadUpdate extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.update";

    private final Interface _defn;

    public SvcFacilityShareableUploadUpdate() {
        _defn = new Interface();

        SvcFacilityShareableUploadCreate.addArgDefns(_defn, true);

        _defn.add(new Interface.Element("id", LongType.POSITIVE,
                "Upload shareable id. If not specified then a token must be specified instead.", 0, 1));

        _defn.add(new Interface.Element("token", StringType.DEFAULT,
                "Upload shareable token. If not specified then id must be specified instead.", 0, 1));

    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Update an existing upload shareable. Note: completion service options must be supplied in full as it does not merge with the existing options.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String shareableId = args.value("id");
        String shareableToken = args.value("token");
        if (shareableId == null && shareableToken == null) {
            throw new IllegalArgumentException("Either id or token of the upload shareable must be specified.");
        }
        if (shareableId != null && shareableToken != null) {
            throw new IllegalArgumentException("Expects either id or token of the upload shareable. Found both.");
        }
        XmlDoc.Element shareable = shareableId != null ? Shareable.describe(executor(), shareableId)
                : Shareable.describe(shareableToken, executor());

        if (!User.hasSystemAdministratorRole(executor())) {
            validateOwner(executor(), shareable);
        }

        if (!"upload".equals(shareable.value("@type"))) {
            throw new IllegalArgumentException("Shareable " + shareable.value("@id") + " is not a upload shareable.");
        }
        updateUploadShareable(executor(), shareable, args);
        w.add("updated", new String[] { "shareable-id", shareable.value("@id") }, true);
    }

    private static void validateOwner(ServiceExecutor executor, XmlDoc.Element shareable) throws Throwable {
        String ownerActorId = shareable.value("owner/@id");
        String ownerDomain = shareable.value("owner/domain");
        String ownerUser = shareable.value("owner/user");

        String selfActorId = executor.execute("actor.self.describe").value("actor/@id");

        if (!Objects.equals(ownerActorId, selfActorId)) {
            String shareableType = shareable.value("@type");
            String shareableId = shareable.value("@id");
            throw new Exception(shareableType + " shareable " + shareableId + " is owned by " + ownerDomain + ":"
                    + ownerUser
                    + ". Only the owner or system-administrator can modify this shareable.");
        }
    }

    private static void updateUploadShareable(ServiceExecutor executor, XmlDoc.Element shareable, XmlDoc.Element args)
            throws Throwable {
        String name = args.stringValue("name", shareable.value("name"));

        String description = args.stringValue("description", shareable.value("description"));

        Date validFrom = args.dateValue("valid-from", shareable.dateValue("valid-from"));

        Date validTo = args.dateValue("valid-to", shareable.dateValue("valid-to"));
        if (args.elementExists("valid-to") && validTo.getTime() > System.currentTimeMillis()) {
            throw new IllegalArgumentException("Invalid valid-to: " + DateTime.string(validTo));
        }
        if (args.elementExists("valid-from") && validTo != null && validFrom.getTime() > validTo.getTime()) {
            throw new IllegalArgumentException("Invalid range: (valid-from: " + DateTime.string(validFrom)
                    + " valid-to: " + DateTime.string(validTo) + ")");
        }

        String facilityName = args.stringValue("facility/name", Shareable.getFacilityName(shareable));

        String facilityNotificationFrom = args.stringValue("facility/notification/from",
                Shareable.getFacilityNotificationFrom(shareable));

        Set<String> facilityNotificationToEmails = new LinkedHashSet<>();
        Collection<String> facilityNotificationRecipients = args.values("facility/notification/to");
        if (facilityNotificationRecipients != null) {
            facilityNotificationRecipients.forEach(e -> facilityNotificationToEmails.add(e.toLowerCase()));
        } else {
            Collection<String> existingFacilityNotificationRecipients = Shareable
                    .getFacilityNotificationRecipients(shareable);
            if (existingFacilityNotificationRecipients != null) {
                existingFacilityNotificationRecipients.forEach(e -> facilityNotificationToEmails.add(e.toLowerCase()));
            }
        }

        String facilityNotificationTo = facilityNotificationToEmails.isEmpty() ? null
                : Emails.join(facilityNotificationToEmails);

        boolean shareWithEmailDictRemove = args.booleanValue("share-with-email-dictionary/@remove", false);
        final String shareWithEmailDict = shareWithEmailDictRemove ? null
                : args.stringValue("share-with-email-dictionary",
                        shareable.value("args/definition/element[@name='share-with']/restriction/dictionary"));

        Map<String, String> properties = new LinkedHashMap<>();
        List<XmlDoc.Element> existingProps = shareable.elements("properties/property");
        if (existingProps != null) {
            for (XmlDoc.Element p : existingProps) {
                properties.put(p.value("@name"), p.value());
            }
        }
        List<XmlDoc.Element> props = args.elements("property");
        if (props != null) {
            for (XmlDoc.Element p : props) {
                properties.put(p.value("@name"), p.value());
            }
        }

        if (facilityName != null) {
            properties.put(Shareable.PROPERTY_FACILITY_NAME, facilityName);
        }

        if (facilityNotificationFrom != null) {
            properties.put(Shareable.PROPERTY_FACILITY_NOTIFICATION_FROM, facilityNotificationFrom);
        }

        if (facilityNotificationTo != null) {
            properties.put(Shareable.PROPERTY_FACILITY_NOTIFICATION_TO, facilityNotificationTo);
        }

        String facilityUploadNameSuffix = args.stringValue("upload-name-suffix/facility",
                Shareable.getFacilityUploadNameSuffix(shareable));
        if (facilityUploadNameSuffix != null) {
            properties.put(Shareable.PROPERTY_FACILITY_UPLOAD_NAME_SUFFIX, facilityUploadNameSuffix);
        }

        if (shareWithEmailDict != null) {
            String shareWithEmailDictAddEntry = args.stringValue("share-with-email-dictionary/@add-entry", "no");
            properties.put(Shareable.PROPERTY_FACILITY_EMAIL_DICT_ADD_ENTRY, shareWithEmailDictAddEntry);
        }

        boolean userUploadNameSuffix = args.booleanValue("upload-name-suffix/user", true);
        boolean emailSubjectSuffix = args.booleanValue("email-subject-suffix",
                shareable.elementExists("args/definition/element[@name='" + Shareable.ARG_EMAIL_SUBJECT_SUFFIX + "']"));
        boolean dataNote = args.booleanValue("data-note",
                shareable.elementExists("args/definition/element[@name='" + Shareable.ARG_DATA_NOTE + "']"));

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", shareable.value("@id"));
        dm.add("multi-use", true);
        if (name != null) {
            dm.add("name", name);
        }
        if (description != null) {
            dm.add("description", description);
        }
        if (validFrom != null) {
            dm.add("valid-from", validFrom);
        }
        if (validTo != null) {
            dm.add("valid-to", validTo);
        }
        if (!properties.isEmpty()) {
            Set<String> propNames = properties.keySet();
            for (String propName : propNames) {
                String propValue = properties.get(propName);
                dm.add("property", new String[] { "name", propName }, propValue);
            }
        }

        dm.add("create-parent", shareable.booleanValue("parent/@create", true));

        SvcFacilityShareableUploadCreate.addParentNameArg(dm, facilityUploadNameSuffix, userUploadNameSuffix);

        SvcFacilityShareableUploadCreate.addArgsArg(dm, userUploadNameSuffix, emailSubjectSuffix, dataNote,
                shareWithEmailDict);

        if (args.elementExists("request-path-context")) {
            dm.add(args.element("request-path-context"));
        } else if (shareable.elementExists("request-path-context")) {
            dm.add(shareable.element("request-path-context"));
        }

        dm.push("completion-service", new String[] { "name", SvcFacilityShareableUploadComplete.SERVICE_NAME });
        XmlDoc.Element completion = args.element("completion");
        if (completion == null || !completion.hasSubElements()) {
            // if completion service args are not specified, keep using the existing ones.
            completion = shareable
                    .element("completion-service[@name='" + SvcFacilityShareableUploadComplete.SERVICE_NAME + "']");
        }
        if (completion != null && completion.hasSubElements()) {
            dm.add(completion, false);
        }
        dm.pop();

        dm.add("interaction-service",
                new String[] { "name", SvcFacilityShareableUploadInteractionComplete.SERVICE_NAME });

        executor.execute("asset.shareable.upload.set", dm.root());
    }
}
