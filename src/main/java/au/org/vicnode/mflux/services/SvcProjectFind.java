package au.org.vicnode.mflux.services;

import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.CiteableIdType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectFind extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.find";

    private Interface _defn;

    public SvcProjectFind() {
        _defn = new Interface();
        _defn.add(new Interface.Element("cid", CiteableIdType.DEFAULT, "The citeable id of the project.", 0, 1));
        _defn.add(new Interface.Element("ordinal", IntegerType.POSITIVE_ONE, "Ordinal part of the citeable id.", 0, 1));
        Interface.Element endsWith = new Interface.Element("ends-with", StringType.DEFAULT, "Project id ends with?", 0,
                1);
        endsWith.add(new Interface.Attribute("ignore-case", BooleanType.DEFAULT, "Ignore case. Default true", 0));
        _defn.add(endsWith);
        Interface.Element startsWith = new Interface.Element("starts-with", StringType.DEFAULT, "Project id ends with?",
                0, 1);
        startsWith.add(new Interface.Attribute("ignore-case", BooleanType.DEFAULT, "Ignore case. Default true", 0));
        _defn.add(startsWith);
        Interface.Element contains = new Interface.Element("contains", StringType.DEFAULT, "Project id contains?", 0,
                1);
        contains.add(new Interface.Attribute("ignore-case", BooleanType.DEFAULT, "Ignore case. Default true", 0));
        _defn.add(contains);
        _defn.add(new Interface.Element("matches", StringType.DEFAULT,
                "The regular expression that project id matches?", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Find a project.  This service finds all projects (regardless of the calling user permissions as the service itself is granted access to the relevant project dictionary.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String cid = args.value("cid");
        Integer ordinal = args.intOrNullValue("ordinal");
        String contains = args.value("contains");
        boolean containsIgnoreCase = args.booleanValue("contains/@ignore-case", true);
        String startsWith = args.value("starts-with");
        boolean startsWithIgnoreCase = args.booleanValue("starts-with/@ignore-case", true);
        String endsWith = args.value("ends-with");
        boolean endsWithIgnoreCase = args.booleanValue("ends-with/@ignore-case", true);
        String matches = args.value("matches");

        int idx = 1;
        int size = 1000;
        boolean complete = false;
        String dict = Properties.getProjectNamespaceMapDictionaryName(executor());
        String cidRoot = executor().execute("citeable.named.id.describe")
                .value("id[@name='" + Properties.CID_ALLOCATOR_ROOT + "']");

        do {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("idx", idx);
            dm.add("size", size);
            dm.add("count", true);
            dm.add("dictionary", dict);
            XmlDoc.Element re = executor().execute("dictionary.entries.describe", dm.root());
            List<XmlDoc.Element> ees = re.elements("entry");
            if (ees != null) {
                for (XmlDoc.Element ee : ees) {
                    String projectID = ee.value("term");
                    if (cid != null) {
                        if (!projectID.endsWith("-" + cid)) {
                            continue;
                        }
                    }
                    if (ordinal != null) {
                        if (!projectID.endsWith("-" + cidRoot + "." + ordinal)) {
                            continue;
                        }
                    }
                    if (contains != null) {
                        if (containsIgnoreCase) {
                            if (!projectID.toLowerCase().contains(contains.toLowerCase())) {
                                continue;
                            }
                        } else {
                            if (!projectID.contains(contains)) {
                                continue;
                            }
                        }
                    }
                    if (startsWith != null) {
                        if (startsWithIgnoreCase) {
                            if (!projectID.toLowerCase().startsWith(startsWith.toLowerCase())) {
                                continue;
                            }
                        } else {
                            if (!projectID.startsWith(startsWith)) {
                                continue;
                            }
                        }
                    }
                    if (endsWith != null) {
                        if (endsWithIgnoreCase) {
                            if (!projectID.toLowerCase().endsWith(endsWith.toLowerCase())) {
                                continue;
                            }
                        } else {
                            if (!projectID.endsWith(endsWith)) {
                                continue;
                            }
                        }
                    }
                    if (matches != null) {
                        if (!projectID.matches(matches)) {
                            continue;
                        }
                    }
                    String projectNS = ee.value("definition");
                    w.add("project", new String[] { "namespace", projectNS }, projectID);
                }
            }
            idx = re.intValue("cursor/next");
            complete = re.booleanValue("cursor/total/@complete");
        } while (!complete);

    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
