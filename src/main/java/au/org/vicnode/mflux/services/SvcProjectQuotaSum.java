/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.HashSet;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;
import au.org.vicnode.mflux.plugin.util.Util.StorageUnit;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;




public class SvcProjectQuotaSum extends PluginService {


	private Interface _defn;

	public SvcProjectQuotaSum()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("unit", new EnumType(Util.StorageUnit.values()),
				"The storage unit you wish the result returned in. Default is GB.", 0, 1));
		_defn.add(new Interface.Element("store", StringType.DEFAULT, "If supplied, limit the summaries to only projects that use this store (defaults to all stores).",	 0, 1));

	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns a sum of all of the Project namespace quotas along with the total amount of storage used and a list of all Project namespaces that have no quota.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.quota.sum";
	}

	public boolean canBeAborted() {

		return true;
	}

	
	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String store = args.value("store");
		// Sets the unit type from the argument, defaults to GB
		Util.StorageUnit storageUnit = Util.StorageUnit.fromString(args.value("unit"), StorageUnit.GB);
		sum (storageUnit, store, w);
	}

	// Sum function to go through all projects and sum their quotas.
	private void sum (Util.StorageUnit storageUnit, String store, XmlWriter w) throws Throwable {
		ServiceExecutor executor = executor();
		Vector<String> projectIDs = new Vector<String>();

		// FInd all the projects and add to the list
		projectIDs.addAll(Project.projectIDs(executor,true, true));

		// Iterate over projects
		if (projectIDs.size()==0) {
			return;
		}
		long quota=0;
		long usage=0;

		HashSet<String> parentsWithNoQuotaChildren = new HashSet<String>();
		Vector<String> projectsWithNoQuota = new Vector<String>();
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
			String projectNameSpace = ph.nameSpace();
			XmlDoc.Element nameSpaceDescription = ph.metaData();
			String projectStore = nameSpaceDescription.value("namespace/store");
			if (store==null || (store!=null && store.equals(projectStore))) {

				// Build the arguments for the asset.query service which will sum the assets under the project namespace, based on the below command
				//> asset.query :where namespace>='/projects/' :action sum :xpath content/size
				String projectNameSpaceQuery = "namespace>='"+projectNameSpace+"'";		
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("where", projectNameSpaceQuery);
				dm.add("action","sum");
				dm.add("xpath","content/size");			

				// Run the asset.query service
				XmlDoc.Element projectUsageDetails = executor.execute("asset.query", dm.root());
				// Extract the summed asset size in bytes
				long projectUsage = projectUsageDetails.longValue("value",0);


				String projectQuota = nameSpaceDescription.value("namespace/quota/allocation");
				if (projectQuota == null){
					projectsWithNoQuota.add(projectID);
					parentsWithNoQuotaChildren.add(Project.parentNameSpace(executor, ph.id()));

					// For the case that a project has no quota but has usage.
					usage += projectUsage;
				} else{
					quota += nameSpaceDescription.longValue("namespace/quota/allocation");
					usage += projectUsage;
				}
			}
		}


		//Converts the quota into the appropriate units and returns an XML element
		w.add(storageUnit.convert(quota, "Quota-Total", null));
		//Converts the usage into the appropriate units and returns an XML element
		w.add(storageUnit.convert(usage, "Used", null));		

		// Now look at the unique set of parent namespace that had children with no quota\
		if (projectsWithNoQuota.size()>0) {
			w.push("no-quota");
			for (String projectID : projectsWithNoQuota) {
				w.add("project-id", projectID);
			}
			if (parentsWithNoQuotaChildren.size()>0) {
				for (String parent : parentsWithNoQuotaChildren) {
					XmlDoc.Element r = NameSpaceUtil.describe(null, executor, parent);
					Long quotaAlloc = r.longValue("namespace/quota/allocation");	
					if (quotaAlloc!=null) {
						w.push("parent");
						w.add("namespace", parent);
						w.add(storageUnit.convert(quotaAlloc, "Quota", null));
						w.pop();
					}
				}
				w.pop();
			}
			w.pop();
		}
	}
}
