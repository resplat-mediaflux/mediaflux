/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixMountList extends PluginService {


	private Interface _defn;

	public SvcProjectPosixMountList()  throws Throwable {
		_defn = new Interface();
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Lists all mounted posix mount points for provisioned projects.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.mount.list"; 
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {


		// Iterate over projects
		Collection<String> ids = Project.projectIDs(executor(), true, true);
		if  (ids==null) return;
		for (String id : ids) {
			list (executor(), id, w);
		}
	}


	private void list (ServiceExecutor executor, String name, XmlWriter w) throws Throwable {
		try {			
			// Exception if not mounted
			XmlDoc.Element r = Posix.describePosixMountPoint(executor, name);
			String id = r.value("mount/@id");
			String  readOnly = r.value("mount/read-only");
			w.add ("mount", new String[]{"read-only", readOnly, "id", id}, name);
		} catch (Throwable t) {
			//
		}
	}
}
