/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;

import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DictionaryUtil;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;


public class SvcProjectParentNamespaceAdd extends PluginService {

	private Interface _defn;

	public SvcProjectParentNamespaceAdd()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The new parent namespace that will be created.  Must be a child of the project root given by server property projects.namespace", 1, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Create a new project parent namespace and associated visibility role (of the form <child part of parent namespace>-visible in the admin role namespace). The namespace will inherit the store or store policy from its parent.  The namespace and visibility role will be added to the dictionary project-namespace-roots which is in the admin dictionary namespace.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.parent.namespace.add";
	}

	public boolean canBeAborted() {

		return false;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String nameSpace = args.stringValue("namespace");

		// Validate the namespace as being under the primary root
		String projectRoot = Project.getProjectRootNameSpace(executor());
		int n = projectRoot.length();
		if (nameSpace.length()<=n) {
			throw new Exception("The namespace must start with '" +
					projectRoot + "'");
		}
		String t = nameSpace.substring(0, n);
		if (!t.equals(projectRoot)) {
			throw new Exception("The namespace must start with '" +
					projectRoot + "'");
		}
		
		// Make sure is not a child of an existing project
		int idx = nameSpace.indexOf("proj-");
		if (idx>=0) {
			throw new Exception("Namespace cannot be a child of an existing project");
		}
		
		// CHeck does not exist
		if (NameSpaceUtil.assetNameSpaceExists(executor(), null, nameSpace)) {
			throw new Exception ("Namespace '" + nameSpace + "' already exists.");
		}

		// Create the visibility role. The project roles for projects
		// under this root are created holding this role so users
		// who hold one of these roles can see the project parent
		// namespace.
		idx = nameSpace.lastIndexOf("/");
		String childNameSpace = nameSpace.substring(idx+1);
		String visibleTo = Properties.getAdminRoleNameSpace(executor()) + ":" +
				childNameSpace + "-visible";

		// Create the role
		Util.createRole(executor(), visibleTo);

		// Create the asset namespace with the visibility role. 
		// It inherits the store / store policy from the parent
		NameSpaceUtil.createNameSpace(executor(), null, nameSpace, 
				null, null, visibleTo, null, null);

		// Add to the dictionary. The definition is the visibility role
		String dictionary = Properties.getProjectNamespaceRootsDictionaryName(executor());	
		DictionaryUtil.addDictionaryTerm(executor(), dictionary, nameSpace, visibleTo);

		// Grant the visibility role to the administrator role
		// to support its ACCESS to all projects
		String adminRole = Properties.getAdministratorRoleName(executor());
		Util.grantRevokeRoleToRole(executor(), adminRole, visibleTo, true);

		// Feedback	
		w.add("namespace", new String[] {"created", "true"}, nameSpace);
		w.add("visible-to", new String[] {"granted-to", adminRole}, visibleTo);
		w.add("dictionary-populated", dictionary);
	}

}
