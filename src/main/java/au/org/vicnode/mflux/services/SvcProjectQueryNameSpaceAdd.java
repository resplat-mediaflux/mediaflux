/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services;

import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectQueryNameSpaceAdd extends PluginService {




	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectQueryNameSpaceAdd()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn,  allowPartialCID);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Adds a hidden (except to project admins) child sub-namespace called 'Queries'to the parent project asset namespace. If the project already has this namespace, returns silently.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.queries.namespace.add";
	}

	public boolean canBeAborted() {
		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}


	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String projectID = args.stringValue("project-id");

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (projectID!=null) {
			String t[] = Project.validateProject(executor(), projectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;

		//
		for (String pid : projectIDs) {
			Project.addQueriesAssetNameSpace(executor(), pid);
		}
	}


}
