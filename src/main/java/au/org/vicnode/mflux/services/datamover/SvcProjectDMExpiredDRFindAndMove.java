/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.datamover;


import java.util.Collection;
import java.util.Date;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;
import au.org.vicnode.mflux.services.SvcProjectDescribe;


public class SvcProjectDMExpiredDRFindAndMove extends PluginService {

	// The parent namespace on the DR server to which data that has expired
	// will be moved. We maintain the next level exactly as in the source. THus
	// /1128/projects/<proj>/<transactional namespace> moves to
	// <DR_NAMESPACE_DESTINATION>/1128/projects/<proj>/<transactional namespace>
	private static final String DR_NAMESPACE_DESTINATION = "/ExpiredInstrumentDataMoverUploadsReadyForDestroy";

	private Interface _defn;
	private static Boolean allowPartialCID = false;


	public SvcProjectDMExpiredDRFindAndMove()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addMandatoryProjectIDs(_defn, allowPartialCID);
		//
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT,
				"The parent namespace on the DR server into which expired DataMover transactional namespaces will be moved. The default is " + DR_NAMESPACE_DESTINATION, 0,1));
		_defn.add(new Interface.Element("move", BooleanType.DEFAULT,
				"If true (default false), instead of just checking, actually move the namespace on the DR server.", 0,1));
		_defn.add(new Interface.Element("show-all", BooleanType.DEFAULT,
				"If true (default false), list to the terminal all details for all projects and transactional namespaces considered, whether data are moved or not.  If false, only list output to the terminal for a project if a transactional namespace is actually moved.", 0,1));
		_defn.add(new Interface.Element("expiry-offset", IntegerType.DEFAULT, "Subtract this many days from the actual acquisition time to force data to look they have expired. For testing only. Defaults to 0.", 0, 1));
		_defn.add(new Interface.Element("email", StringType.DEFAULT, 
				"An email address to send a notification to advise the outcome of this service  (i.e. it moved some data).", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Checks projects on the DR server (service must be executed on primary) for DataMover uploaded instrument data (in transactional namespaces) that has expired (older than the download links) AND no longer exists on the primary. The found transactional namespaces are moved to a new parent namespace. Another scheduled job subsequently destroys these. ";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.DM.expired.DR.move";
	}

	public boolean canBeAborted() {

		return true;
	}


	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		String namespaceDR = args.stringValue("namespace", DR_NAMESPACE_DESTINATION);
		Collection<String> projectIDs = args.values("project-id");
		Boolean move = args.booleanValue("move", false);
		Integer expiryOffset = args.intValue("expiry-offset", 0);
		String email = args.stringValue("email");
		Boolean showAll = args.booleanValue("show-all", false);
		// Validate projects (min 1)
		for (String projectID : projectIDs) {
			Project.validateProject(executor(), projectID, allowPartialCID);
		}

		// Iterate over projects. 
		XmlDocMaker result = new XmlDocMaker("args");
		for (String projectID : projectIDs) {
			XmlDocMaker res = new XmlDocMaker("args");
			res.push("project");
			//
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			res.add("id", ph.id());
			Boolean show = checkProject (ph, namespaceDR, showAll, move, expiryOffset, res);
			res.pop();
			//
			if (show) {
				result.addAll(res.root().elements());
			}
		}

		// Populate terminal output
		if (result.root().hasSubElements()) {
			// Populate terminal output
			w.addAll(result.root().elements());

			// Send email
			if (email!=null) {
				if (result.root().hasSubElements()) {
					result.push("service", new String[] {"name", name()});
					result.add(args);
					result.pop();
					String body = XmlPrintStream.outputToString(result.root());
					String uuid = Util.serverUUID(executor());
					String subject = "["+uuid+"] - DataMover instrument expired+destroyed upload find and move on DR server (date="+new Date().toString()+")";
					MailHandler.sendMessage(executor(), email, subject, body, null, false, null);
				} 
			}
		}
	}


	private Boolean checkProject (ProjectHolder ph, String namespaceDR, Boolean showAll,
			Boolean move, Integer expiryOffset, XmlDocMaker  w) throws Throwable {		


		// Returned true if there were any to-move or moved namespaces for this project
		Boolean some = false;

		// Get project namespace meta-data
		XmlDoc.Element nsMeta = ph.metaData();
		ServiceExecutor executor = ph.executor();

		// See if we can find the Data Mover expiry notification meta-data
		// ds-admin:Project/notification/data-mover-expiry on the project
		// on the primary server
		//
		// The presence of this meta-data tells us that these 
		// data will be destroyed on the primary by the operator 
		// and is thus a candidate for destruction on the DR.
		// 
		// Because replication status on the primary project may be turned 
		// off or on, we will look for the data on the DR regardless of that status.
		XmlDoc.Element dmMeta = Utils.getNotificationMetaData(executor, nsMeta);

		// FInd the project exclusion child paths, if any, to consider, to exclude when
		// looking for manifest assets on the DR.
		Collection<String> excludeChildPaths = null;
		if (dmMeta!=null) {
			excludeChildPaths = dmMeta.values("exclude-child-path");
		}

		// Find manifest assets in and under this  path on DR except for
		// where excluded
		String uuid = ProjectReplicate.DRServerUUID(executor);
		ServerRoute sr = new ServerRoute(uuid);
		Collection<XmlDoc.Element> assets = 
				Utils.findManifestAssets(executor, sr, ph.nameSpaceDR(), excludeChildPaths);

		w.add("uuid", uuid);
		
		// For each manifest asset (which represents one transactional upload),
		// determine if the data are older than the expiry date of the download 
		// links.    If the data also no longer exist on the primary, then
		// move the transactional namespace containing that manifest asset
		// to it's new location
		int n = 0;
		if (assets!=null) {
			for (XmlDoc.Element asset : assets) {
				Boolean showThis = false;
				XmlDocMaker res = new XmlDocMaker("args");
				res.push("manifest");
				// Absolute namespace on the DR
				String transactionalNameSpace = asset.value("namespace");
				String assetID = asset.value("@id");
				String path  = asset.value("path");
				//
				res.add("namespace", transactionalNameSpace);
				res.add("path", path);
				res.add("id", assetID);

				// Get manifest asset meta-data
				XmlDoc.Element mfMeta = asset.element("meta/"+Utils.MANIFEST_DOCTYPE);

				// Get the expiry date of the download links 
				Date expiryDate = Utils.getExpiryDate(mfMeta);
				if (expiryOffset>0) {
					// Push into the past for testing
					expiryDate = DateUtil.addDays(expiryDate, -expiryOffset);  
				}
				res.add("download-link-expiry", expiryDate);

				// See if the data are expired. We do this simply by seeing
				// if today is after the  download link expiry date.
				Date today = new Date();
				Boolean expired = today.after(expiryDate);

				// Do the data still exist on the primary ?
				// We just check that the namespace is not where
				// we expect it.  
				//
				// TBD We could also use the manifest
				// asset (which could be anywhere if the namespace
				// was moved)
				Boolean existsOnPrimary = existsOnPrimary (executor, 
						transactionalNameSpace, res);
				res.add("exists-on-primary", existsOnPrimary);
				res.add("expired", expired);			

				if (expired && !existsOnPrimary) {
					showThis = true;
					if (move) {
						moveNameSpace (executor, sr, transactionalNameSpace, 
								namespaceDR, res);
						res.add("moved", true);

						// Set the date on the manifest asset when we moved the data. 
						// A second process is going to destroy these data, say, 2 months after it
						// was moved
						XmlDocMaker dm = new XmlDocMaker ("args");
						dm.add("id", assetID);
						dm.push("meta");
						dm.push(Utils.MANIFEST_DOCTYPE);
						dm.add("date-moved-on-DR", today);
						dm.pop();
						dm.pop();
						dm.add("allow-incomplete-meta", true);
						executor.execute(sr, "asset.set", dm.root());
						res.add("move-meta-set", true);
					} else {
						res.add("moved", false);
						res.add("move-meta-set", false);	
					}
				} else {
					res.add("moved", false);
					res.add("move-meta-set", false);
				}
				res.pop();
				
				// Populate output. If something is ready to move or actually
				// moved or if we just want to see everything
				if (showAll || showThis) {
					w.addAll(res.root().elements());
					n++;
					// Indicate that there is something we want to see 
					// for this project
					some = true;
				}
			}
		}
		//
		w.add("n-manifest", n);

		w.pop();
		return some;
	}

	private Boolean existsOnPrimary (ServiceExecutor executor, 
			String namespaceDR, XmlDocMaker w) throws Throwable {

		// Our layout is 
		// primary        /projects
		// DR      /<UUID>/projects
		int idx = namespaceDR.indexOf("/", 1);
		String namespacePrimary = namespaceDR.substring(idx);
		w.add("namespace-primary", namespacePrimary);

		return NameSpaceUtil.assetNameSpaceExists(executor, null, namespacePrimary);
	}

	private void moveNameSpace (ServiceExecutor executor, ServerRoute sr, 
			String transactionalNamespace, String destinationParent, 
			XmlDocMaker w) throws Throwable {

		// We want to reproduce the path under the new parent
		// So /1128/projects/proj-xyz-1128.5.23/data/<transactional namespace>
		// gets copied to <destination parent>/DataMoverDestroys/1128/projects/proj-xyz-1128.5.23/data/<transactional namespace>
		// where <destination parent> is say /ExpiredDataMoverUploadDestroys
		// which has an ACL to limit access. There is no easy way to do this
		// apart from re-create the full namespace path.

		// Find all of the parent path of the input transactional namespace
		int idx = transactionalNamespace.lastIndexOf("/");
		String parentIn = transactionalNamespace.substring(0, idx);
		if (!parentIn.substring(0,1).equals("/")) {
			throw new Exception ("Source parent namespace does not begin with a '/' : " + parentIn);
		}

		// Add on the root of the destination namespace
		String parentOut = destinationParent + parentIn;
		w.add("destination", parentOut);

		// If doesn't exist create destination parent on DR
		createNameSpace (executor, sr, parentOut);

		// Move the transactional namespace over to its new home
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("to", parentOut);
		dm.add("namespace", transactionalNamespace);	
		executor.execute(sr, "asset.namespace.move", dm.root());
	}




	private void createNameSpace (ServiceExecutor executor, ServerRoute sr, 
			String namespace) throws Throwable {

		// Create parent namespace if required
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		XmlDoc.Element r = executor.execute(sr, "asset.namespace.exists", dm.root());
		if (!r.booleanValue("exists")) {
			dm = new XmlDocMaker("args");
			dm.add("namespace", new String[] {"all", "true"}, namespace);
			executor.execute(sr, "asset.namespace.create", dm.root());	
		}
	}
}
