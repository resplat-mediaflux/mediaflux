package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcFacilityShareableUploadAllList extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.all.list";

    private final Interface _defn;

    public SvcFacilityShareableUploadAllList() {
        _defn = new Interface();
        _defn.add(new Interface.Element("owner", StringType.DEFAULT, "The owner user of the shareables. If not specified, all shareables are included", 0, 1));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "List all facility upload shareables owned by any user.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String ownerUser = args.value("owner");
        long count = 0;

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("size", "infinity");
        dm.add("type", "upload");
        if (ownerUser != null) {
            dm.add("user", ownerUser);
        }
        PluginTask.checkIfThreadTaskAborted();
        XmlDoc.Element re = executor().execute("asset.shareable.all.list", dm.root());
        Collection<String> sids = re.values("shareable");
        if (sids != null) {
            for (String sid : sids) {
                PluginTask.checkIfThreadTaskAborted();
                XmlDoc.Element se = Shareable.describe(executor(), sid);
                // check if it is
                if (se.elementExists("completion-service[@name='" + SvcFacilityShareableUploadComplete.SERVICE_NAME + "']")) {
                    String name = se.value("name");
                    String id = se.value("@id");
                    String owner = String.format("%s:%s", se.value("owner/domain"), se.value("owner/user"));
                    w.add("shareable", new String[]{"type", "upload", "name", name, "owner", owner, "uploads", se.value("uploads/count"), "enabled", se.value("enabled")}, id);
                    count++;
                }
            }
        }
        w.add("count", count);
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }
}

