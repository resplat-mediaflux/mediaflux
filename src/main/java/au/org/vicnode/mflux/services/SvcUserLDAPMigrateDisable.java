package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;

public class SvcUserLDAPMigrateDisable extends PluginService {

	public static final String SERVICE_NAME = "vicnode.user.ldap.migrate.disable";

	private Interface _defn;

	public SvcUserLDAPMigrateDisable() {
		_defn = new Interface();

		addToDefn(_defn);
	}


	static void addToDefn(Interface defn) {

		defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain the user being migrated belongs to", 1, 1));
		defn.add(new Interface.Element("exclude-user", StringType.DEFAULT, "Exclude this user (e.g. a service account). Just give the username, we already know their domain.", 0, Integer.MAX_VALUE));
		defn.add(new Interface.Element("check-only", BooleanType.DEFAULT, "Just check the process only (default true, but doesn't actually send adisable or send a notification. ", 0, 1));
		defn.add(new Interface.Element("reminder", BooleanType.DEFAULT, "The notification is a reminder only (default true).  If false, the account is disabled. ", 0, 1));
		defn.add(new Interface.Element("date", StringType.DEFAULT, "The date when we disable accounts for the reminder notification message.", 0, 1));
		defn.add(new Interface.Element("bcc", StringType.DEFAULT, "BCC this address", 0, 1));
		defn.add(new Interface.Element("from", StringType.DEFAULT, "Notification emails to users are 'from' this address.", 1, 1));
		defn.add(new Interface.Element("host", StringType.DEFAULT,
				"Specify host for the notification, E.g. mediaflux.researchsoftware.unimelb.edu,au.", 1, 1));
		//
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String description() {
		return "Reminds users or actually disables local or AAF accounts, based on the meta-data on their account stating they have been migrated to AD.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		Date date = args.dateValue("date");
		String domain = args.value("domain");
		String host = args.value("host");
		Boolean reminder = args.booleanValue("reminder", true);
		Boolean checkOnly = args.booleanValue("check-only", true);
		Collection<String> excludeUsers = args.values("exclude-user");
		if (reminder && date==null) {
			throw new Exception("You must supply the reminder date");
		}
		//
		String bcc = args.value("bcc");
		String from = args.value("from");

		// Iterate over users
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("size", "infinity");
		XmlDoc.Element r = executor().execute("user.describe", dm.root());
		Collection<XmlDoc.Element>users = r.elements("user");
		for (XmlDoc.Element user : users) {
			PluginTask.checkIfThreadTaskAborted();
			String now = DateUtil.formatDate(new Date(), "dd-MMM-YYYY HH:mm:ss");

			// 
			String userName = user.value("@user");
			String name = user.value("name");
			String protocol = user.value("@protocol");
			String email = user.value("e-mail");
			if (email==null) {
				email = user.value("asset/meta/mf-user/email");
			}
			Boolean isSAML = (protocol!=null && protocol.equals("saml"));

			// Filter out exclusion list
			Boolean keep = true;
			if (excludeUsers!=null && excludeUsers.size()>0) {
				if (excludeUsers.contains(userName)) {
					keep = false;
				}
			}

			// See if they were migrated
			Boolean migrated = false;
			Collection<String> notes = user.values("asset/meta/mf-note/note");
			String[] parts = null;
			if (notes!=null && notes.size()>0) {
				for (String note : notes) {
					if (note.contains("Migrated to LDAP")) {
						migrated = true;
						parts = note.split(" ");
					}
				}
			}
			//
			w.push("user");
			w.add("user");
			w.add("email", email);
			w.add("domain", domain);
			w.add("username", userName);

			if (keep && migrated) {

				String p4 = parts[4];       // 'domain:user'
				int idx = p4.indexOf(":");
				String ldapDomain = p4.substring(1,idx);
				String ldapUserName = p4.substring(idx+1,p4.length()-1);

				String subject = null;
				String body = null;
				
				
				if (reminder) {	
					String dates = DateUtil.formatDate(date, false, false);

					// Reminder only
					if (isSAML) {
						subject = "IMPORTANT: Reminder your Mediaflux (" + host + ") AAF account (aaf:"+ldapUserName+") will be disabled on " + dates;
					} else {
						subject = "IMPORTANT: Reminder your Mediaflux (" + host + ") local account (" + domain+":"+userName+") will be disabled on " + dates;
					}
					//
					if (name!=null) {
						body = "Dear " + name;
					} else {
						body = "Dear colleague";
					}


					body += "\n\n A reminder that the way you log in to the Mediaflux system " + host + " has  changed. University of Melbourne" +
							"\n staff and students can now log in directly with their central account, simplifying and improving the process." +
							"\n\n Your new account details are" +
							"\n      domain : " + ldapDomain + 
							"\n      username : " + ldapUserName +
							"\n      password : <your UoM password>" +
							"\n\n Your old account details are";
					if (!isSAML) {
						body+= "\n      domain : " + domain +
								"\n      username : " + userName;
						body+= "\n\n Your old local account will be disabled on " + dates;

					} else {							
						body+= "\n      domain : " + domain +
								"\n      provider : The University of Melbourne (shortname='unimelb')" +
								"\n      username : " + ldapUserName;
						body+= "\n\n Your old AAF account will be disabled on " + dates;

					}
					body +=	"\n\n Details of this change process can be found on the wiki page - https://wiki.cloud.unimelb.edu.au/resplat/doku.php?id=data_management:mediaflux:aaf_to_ad_migration" +
							"\n\n If you encounter any issues or have any questions, please contact us via email to rd-support@unimelb.edu.au" +
							"\n\n Regards, \n Neil Killeen for the Research Computing Services Data Team." +
							"\n The University of Melbourne";
					w.add("status", new String[] {"date", now}, "reminded");
					//
					if (checkOnly) {
						//
					} else {
						notifyUser (executor(), email, subject, body, bcc, from);
					}
				} else {
					// DIsable
					if (isSAML) {
						subject = "IMPORTANT: your old Mediaflux (" + host + ") AAF account (aaf:"+ldapUserName+") has now been disabled";
					} else {
						subject = "IMPORTANT: your old Mediaflux (" + host + ") local account (" + domain+":"+userName+") has now been disabled";
					}

					if (name!=null) {
						body = "Dear " + name;
					} else {
						body = "Dear colleague";
					}
					//
					body += "\n\n Following the change to the way in which you log in to the Mediaflux system " + host  + "," +
							"\n your old account has now been disabled.";
					if (!isSAML) {
						body+= "\n      domain : " + domain +
								"\n      username : " + userName;
					} else {							
						body+= "\n      domain : " + domain +
								"\n      provider : The University of Melbourne (shortname='unimelb')" +
								"\n      username : " + ldapUserName;
					}
					body +=	"\n\n Details of this change process can be found on the wiki page - https://wiki.cloud.unimelb.edu.au/resplat/doku.php?id=data_management:mediaflux:aaf_to_ad_migration" +
							"\n\n If you encounter any issues or have any questions, please contact us via email to rd-support@unimelb.edu.au" +
							"\n\n Regards, \n Neil Killeen for the Research Computing Services Data Team." +
							"\n The University of Melbourne";
					//
					w.add("subject", subject);
					w.add("body", body);
					String note = "Disabled, as part of migration to LDAP, at " + now;
					w.add("note", note);
					if (checkOnly) {
						//
					} else {
						disableUser (executor(), domain, userName, now);
						setMeta (executor(), domain, userName, note, name, email);
						notifyUser (executor(), email, subject, body, bcc, from);
					}
					w.add("status", new String[] {"date", now}, "disabled");

				}
			}  else {
				if (!keep) {
					w.add("status", "excluded");
				} else if (!migrated) {
					w.add("status", "not-migrated");
				} else {
					w.add("status", "unknown");
				}
			}
			w.pop();
		}
	}

	private void disableUser (ServiceExecutor executor, String domain, String userName, String now) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", userName);
		executor.execute("authentication.user.disable", dm.root());
	}

	private void setMeta (ServiceExecutor executor, String domain, String userName, 
			String note, String name, String email) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", userName);
		// A defect in user.set is wiping out email/name in AAF accounts.
		// So put it back here.
		if (domain.equalsIgnoreCase("aaf")) {
			dm.add("email", email);
			dm.add("name", name);
		}
		dm.push("meta", new String[] {"action", "add"});
		dm.push("mf-note");
		dm.add("note", note);
		dm.pop();
		dm.pop();
		executor.execute("user.set", dm.root());
	}



	private void notifyUser (ServiceExecutor executor, String email, String subject, String body, String bcc,
			String from) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("async", false);
		dm.add("subject", subject);
		dm.add("body", body);
		if (bcc!=null) {
			dm.add("bcc", bcc);
		}
		if (from!=null) {
			dm.add("from", from);
		}
		dm.add("to", email);
		executor.execute("mail.send", dm.root());
	}
}