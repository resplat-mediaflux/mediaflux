/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;

/**
 * 
 * @author nebk
 *
 */
public class SvcProjectAudit extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.audit";
	public static final String[] types = { "asset.accessed.content", "asset.accessed.meta", "asset.created",
			"asset.deleted" };
	private static Boolean allowPartialCID = true;

	private Interface _defn;

	public SvcProjectAudit() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("from", DateType.DEFAULT, "The start date to collect auditing information.", 1,
				1));
		_defn.add(new Interface.Element("to", DateType.DEFAULT,
				"The end date to collect auditing information (defaults to now).", 0, 1));
		_defn.add(new Interface.Element("email", StringType.DEFAULT, "Send the report to this email address.", 0,
				Integer.MAX_VALUE));
		_defn.add(new Interface.Element("include-system", BooleanType.DEFAULT,
				"Include actors from the system domain (default true).", 0, 1));
		SvcProjectDescribe.addProjectIDs(_defn, allowPartialCID);

	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Produces an audit report for the given projects in the given date range.";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		Collection<String> theProjectIDs = args.values("project-id");
		Date from = args.dateValue("from");
		Date to = args.dateValue("to", new Date());
		Collection<String> emails = args.values("email");
		Boolean includeSystem = args.booleanValue("include-system", true);
		//
		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs != null) {
			for (String pid : theProjectIDs) {
				String t[] = Project.validateProject(executor(), pid, allowPartialCID);
				projectIDs.add(t[1]);
			}
		} else {
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size() == 0) {
			return;
		}

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("from", from);
		dm.add("to", to);
		Boolean some = false;
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			Boolean t = audit(executor(), ph, from, to, includeSystem, dm);
			if (t) {
				some = true;
			}
		}

		if (some) {
			w.add(dm.root(), false);
			if (emails != null) {
				String uuid = Util.serverUUID(executor());
				String fs = DateUtil.formatDate(from, true, false);
				String ts = DateUtil.formatDate(to, true, false);
				String subject = "[" + uuid + "] Mediaflux project audit report (" + fs + " to " + ts + ")";
				String body = XmlPrintStream.outputToString(dm.root());
				String fromAddress = Properties.getServerProperty(executor(), "notification.from");
				for (String email : emails) {
					MailHandler.sendMessage(executor(), email, subject, body, null, false, fromAddress);
				}
			}
		}
	}

	private Boolean audit(ServiceExecutor executor, ProjectHolder ph, Date from, Date to, Boolean includeSystem,
			XmlDocMaker w) throws Throwable {

		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("size", "infinity");
		dm.add("from", from);
		dm.add("to", to);
		for (int i = 0; i < types.length; i++) {
			dm.add("type", types[i]);
		}
		String nameSpace = ph.nameSpace();
		dm.add("filter", "starts-with(audit.field.value('namespace'),'" + nameSpace + "')");
		XmlDoc.Element r = executor.execute("audit.query", dm.root());

		// Iterate over events and build up some information about who has done what
		HashMap<String, Integer> assetAccess = new HashMap<String, Integer>(); // user, number
		HashMap<String, Integer> assetCreate = new HashMap<String, Integer>(); // user,number
		HashMap<String, Integer> assetDelete = new HashMap<String, Integer>(); // user,number
		Boolean some = false;
		if (r != null) {
			Collection<XmlDoc.Element> events = r.elementsByName("event");
			if (events != null) {
				for (XmlDoc.Element event : events) {
					PluginTask.checkIfThreadTaskAborted();

					String type = event.value("@type");
					String actorName = event.value("actor");
					String actorId = event.value("actor/@id");
					String actorType = event.value("actor/@type");
					String ipAddress = event.value("ip-address");

					// NOTE: event for identity actor have non-standard format
					// @formatter:off
					/*
						<event id="2588362998" type="asset.accessed.content">
						  <object>1636827031</object>
						  <time tz="Australia/Melbourne" gmt-offset="10.0" dst="false" millisec="1719792969418">01-Jul-2024 10:16:09</time>
						  <actor id="604702" type="identity" public-id="37999">unimelb:xxxxx</actor>
						  <session-id>267.222768</session-id>
						  <vector secure="true">http</vector>
						  <ip-address>123.123.123.123</ip-address>
						  <payload>
						    <name>xxx123.tar</name>
						    <namespace-id>588482632</namespace-id>
						    <namespace>/projects/proj-demonstration-1128.4.1/data_mover_uploads/fastq</namespace>
						    <size>176773120</size>
						    <version>1</version>
						  </payload>
						</event>
					 */
					// @formatter:on

					// Make a unique key which is easy to parse later when we need
					// to extract information from it.
					// actor_name_or_id/actor_type/IP
					String key = ("identity".equals(actorType) ? actorId : actorName) + "/" + actorType + "/"
							+ ipAddress;
					Boolean isSystem = actorType.equals("user") && actorName.startsWith("system");
					if (includeSystem || (!includeSystem && !isSystem)) {
						if (type.equals("asset.accessed.content") || type.equals("asset.accessed.meta")) {
							addOne(assetAccess, key);
							some = true;
						} else if (type.equals("asset.created")) {
							addOne(assetCreate, key);
							some = true;
						} else if (type.equals("asset.deleted")) {
							addOne(assetDelete, key);
							some = true;
						}
					}
				}
			}

			// Update if something to list
			if (some) {
				w.push("project");
				w.add("id", ph.id());

				// List
				w.push("assets-created");
				addValues(executor, ph.id(), assetCreate, w);
				w.pop();

				w.push("assets-deleted");
				addValues(executor, ph.id(), assetDelete, w);
				w.pop();

				w.push("assets-accessed");
				addValues(executor, ph.id(), assetAccess, w);
				w.pop();
				w.pop();
			}

		}
		return some;
	}

	private void addValues(ServiceExecutor executor, String projectID, HashMap<String, Integer> map, XmlDocMaker w)
			throws Throwable {
		Collection<String> keys = map.keySet();
		if (keys != null) {
			for (String key : keys) {
				// Of the form actor_name:actor_type:IP
				// Parse out the actor name and type.
				String[] parts = key.split("/");
				String actorType = parts[1];
				String actorName = "identity".equals(actorType) ? getActorName(executor, parts[0]) : parts[0];
				String actorIP = parts[2];

				// Now find out if that actor holds a project role
				String isProjectMember = isProjectMember(executor, projectID, actorName, actorType);
				w.add("actor", new String[] { "nb", "" + map.get(key), "is-project-member", isProjectMember, "type",
						actorType, "source-IP", actorIP }, actorName);
			}
		}
	}

	private static String getActorName(ServiceExecutor executor, String actorId) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", actorId);
		PluginTask.checkIfThreadTaskAborted();
		return executor.execute("actor.describe", dm.root()).value("actor/@name");
	}

	/**
	 * Is the actor a formal project member
	 * 
	 * @param executor
	 * @param projectID
	 * @param actorName
	 * @param actorType
	 * @return -1 for false, 1 for true, 0 for unknown
	 * @throws Throwable
	 */
	private String isProjectMember(ServiceExecutor executor, String projectID, String actorName, String actorType)
			throws Throwable {
		// An actor of type token is really a secure identity token
		if (actorType.equals("token")) {
			actorType = "identity";
		}
		// If a secure identity token has been destroyed (common)
		// then we can't do any queries on it. The actor name
		// will be missing.
		if (actorName.equals("missing")) {
			return "unknown";
		}

		Collection<String> projectRoles = Project.actualRoleNames(projectID);
		for (String projectRole : projectRoles) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("name", actorName);
			dm.add("type", actorType);
			dm.add("role", new String[] { "type", "role" }, projectRole);

			XmlDoc.Element r = executor.execute("actor.have", dm.root());
			if (r != null) {
				if (r.booleanValue("actor/role")) {
					return "true";
				}
			}
		}
		return "false";
	}

	private void addOne(HashMap<String, Integer> map, String actor) throws Throwable {
		if (map.containsKey(actor)) {
			Integer n = map.get(actor) + 1;
			map.put(actor, n);
		} else {
			map.put(actor, 1);
		}

	}
}
