/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.replication;




import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetProcessingQueue;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectReplicateQueueReset extends PluginService {


	private Interface _defn;

	public SvcProjectReplicateQueueReset()  throws Throwable {
		_defn = new Interface();
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Suspends, resets (setting the pointer back to the beginning) and resumes the  standard asset processing queue which handles replications on all projects.  This will kick any failed items into retrying.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.replicate.queue.reset";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		w.push("processing-queue");
		String queueName = ProjectReplicate.createOrFindStandardQueue(executor(), w);
		XmlDoc.Element q = AssetProcessingQueue.describeQueue(executor(), 
				queueName, false);
		w.add("processing-errors-before-reset", q.value("queue/processing/nb-processing-failures"));
		AssetProcessingQueue.resetQueue(executor(), queueName);
		w.add("reset", true);
		w.pop();
	}
}
