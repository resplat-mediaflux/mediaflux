package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class DownloadNotification {

    protected final XmlDoc.Element downloadShareable;
    private final Date downloadCompletionTime;
    private final String namespace;
    private final String facilityName;
    private final Collection<String> facilityNotificationRecipients;
    private final String facilityNotificationEmailSubjectSuffix;
    private final PluginLog log;

    public DownloadNotification(XmlDoc.Element downloadShareable, Date downloadCompletionTime, PluginLog log)
            throws Throwable {
        this.downloadShareable = downloadShareable;
        this.downloadCompletionTime = downloadCompletionTime;
        this.namespace = downloadShareable.value("namespace");
        this.facilityName = Shareable.getFacilityName(downloadShareable);
        this.facilityNotificationRecipients = Shareable.getFacilityNotificationRecipients(downloadShareable);
        this.facilityNotificationEmailSubjectSuffix = Shareable.getProperty(downloadShareable,
                Shareable.PROPERTY_FACILITY_NOTIFICATION_EMAIL_SUBJECT_SUFFIX);
        this.log = log;
    }

//    public DownloadNotification(ServiceExecutor executor, String downloadShareableId, Date downloadCompletionTime)
//            throws Throwable {
//        this(Shareable.describe(executor, downloadShareableId), downloadCompletionTime);
//    }

    public String subject() {
        StringBuilder sb = new StringBuilder();
        if (this.facilityName != null) {
            sb.append(String.format("%s Data Downloaded", this.facilityName));
        } else {
            sb.append("Facility Data Downloaded");
        }
        if (this.facilityNotificationEmailSubjectSuffix != null) {
            sb.append(" ").append(this.facilityNotificationEmailSubjectSuffix);
        }
        return sb.toString();
    }

    public String htmlMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append("<p>Hello,</p><br>\n");
        if (this.facilityName != null) {
            sb.append(this.facilityName).append(" data: ");
        } else {
            sb.append("Facility data: ");
        }
        sb.append("<b>").append(this.namespace).append("</b> has been downloaded.<br><br><br>");

        String tableOpen = "<table style=\"border:1px solid black; border-collapse:collapse; width:85%;\">";
        String tdLeftOpen = "<td align=\"right\" style=\"width:90px; border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em;\">";
        String tdRightOpen = "<td style=\"border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em\">";

        sb.append(tableOpen).append("\n");
        if (this.facilityName != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Facility Name:</b></td>").append(tdRightOpen)
                    .append(this.facilityName).append("</td></tr>\n");
        }
        sb.append("<tr>").append(tdLeftOpen).append("<b>Asset Namespace:</b></td>").append(tdRightOpen)
                .append(this.namespace).append("</td></tr>\n");
        sb.append("<tr>").append(tdLeftOpen).append("<b>Download Completion Time:</b></td>").append(tdRightOpen)
                .append(this.downloadCompletionTime).append("</td></tr>\n");
        sb.append("</table>\n");

        return sb.toString();
    }

    public void send(ServiceExecutor executor) throws Throwable {
        if (this.facilityNotificationRecipients == null || this.facilityNotificationRecipients.isEmpty()) {
            return;
        }
        String shareableId = this.downloadShareable.value("@id");
        String recipients = String.join(",", this.facilityNotificationRecipients);
        if (this.log != null) {
            this.log.add(PluginLog.WARNING,
                    "[shareable: " + shareableId + "] sending notifications to facility operators: " + recipients);
        }
        XmlDocMaker dm = new XmlDocMaker("args");
        for (String recipient : this.facilityNotificationRecipients) {
            dm.add("to", recipient);
        }
        dm.add("subject", subject());
        dm.add("body", new String[] { "type", "text/html" }, htmlMessage());
        executor.execute("mail.send", dm.root());
        if (this.log != null) {
            this.log.add(PluginLog.WARNING,
                    "[shareable: " + shareableId + "] sent notifications to facility operators: " + recipients);
        }
    }
}
