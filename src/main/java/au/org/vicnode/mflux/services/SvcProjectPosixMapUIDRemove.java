/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixMapUIDRemove extends PluginService {


	private Interface _defn;

	public SvcProjectPosixMapUIDRemove()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT, 
				"The ID of the project (of the form 'proj-<name>-<cid>').", 1, 1));
		_defn.add(new Interface.Element("uid", StringType.DEFAULT, "Unix UID.", 1, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Remove a Unix UID to the posix identity map associated with the project.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.map.uid.remove";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String projectID = args.value("project-id");
		String t[] = Project.validateProject(executor(), projectID, false);
		projectID = t[1];
		String uid = args.value("uid");
		Posix.removeUIDFromPosixIdentityMap (executor(), projectID, uid);
		XmlDoc.Element r = Posix.describePosixIdentityMap(executor(), projectID);
		w.add(r.element("map"));
	}

}
