package au.org.vicnode.mflux.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcUserProjectRoleList extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "user.project.role.list";

    private Interface _defn;

    public SvcUserProjectRoleList() {
        _defn = new Interface();
        _defn.add(new Interface.Element("vca", BooleanType.DEFAULT, "Show VCA roles. Defaults to true", 0, 1));
        _defn.add(new Interface.Element("authority", StringType.DEFAULT, "Authority", 0, 1));
        _defn.add(new Interface.Element("domain", StringType.DEFAULT, "The user's uthentication domain", 1, 1));
        _defn.add(new Interface.Element("user", StringType.DEFAULT, "The user's username", 1, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "List the top level project roles for the user.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String authority = args.value("authority");
        String domain = args.value("domain");
        String user = args.value("user");
        boolean vca = args.booleanValue("vca", true);
        String actorName = domain + ":" + user;
        if (authority != null) {
            actorName = authority + ":" + actorName;
        }
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("name", actorName);
        dm.add("type", "user");
        Collection<String> roles = executor().execute("actor.describe", dm.root()).values("actor/role");
        List<String> result = new ArrayList<>();
        if (roles != null) {
            for (String role : roles) {
                if (role.matches("^proj-.*\\d+.\\d+.\\d+:[a-zA-Z]+(-[a-zA-Z]+){0,2}$")) {
                    result.add(role);
                } else if (vca && role.startsWith("VCA-FTV:")) {
                    if (role.equals("VCA-FTV:VCA-Guest") || role.equals("VCA-FTV:VCA-Student")
                            || role.equals("VCA-FTV:VCA-Staff") || role.equals("VCA-FTV:VCA-Admin")) {
                        result.add(role);
                    }
                }
            }
        }
        if (!result.isEmpty()) {
            Collections.sort(result);
            for (String role : result) {
                w.add("role", role);
            }
        }
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
