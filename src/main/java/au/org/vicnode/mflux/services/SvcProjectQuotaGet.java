package au.org.vicnode.mflux.services;

import java.util.List;
import java.util.Map;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectQuotaGet extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.quota.get";

    private Interface _defn;

    public SvcProjectQuotaGet() throws Throwable {
        _defn = new Interface();
        _defn.add(new Interface.Element("project-id", StringType.DEFAULT,
                "The ID of the project. Should be of the form 'proj-<name>-<UUID.N.M>' or just M - that is, you can enter just the child part of the CID of the project. Defaults to all projects.",
                1, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Get project quota details.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String id = args.value("project-id");
        Map<String, String> props = Properties.getAll(executor());
        String dict = Properties.getProjectNamespaceMapDictionaryName(props);
        List<XmlDoc.Element> ees = executor().execute("dictionary.entries.describe",
                "<args><dictionary>" + dict + "</dictionary></args>", null, null).elements("entry");
        if (ees == null || ees.isEmpty()) {
            throw new Exception("Could not find project " + id);
        }
        boolean numberOnly = id.matches("\\d+");
        String projectID = null;
        String projectNS = null;
        for (XmlDoc.Element ee : ees) {
            String term = ee.value("term");
            String defn = ee.value("definition");
            if ((numberOnly && term.endsWith("." + id)) || term.equals(id)) {
                projectID = term;
                projectNS = defn;
                break;
            }
        }
        if (projectID == null) {
            throw new Exception("Could not find project " + id);
        }
        if (projectNS == null) {
            throw new Exception("Could not resolve asset namespace for project " + projectID);
        }
        XmlDoc.Element ne = executor().execute("asset.namespace.describe",
                "<args><namespace>" + projectNS + "</namespace></args>", null, null).element("namespace");

        w.push("project");
        w.add("id", projectID);
        String prodType = ne.value("asset-meta/" + Properties.getProjectDocType(props) + "/type");
        w.add("prod-type", prodType);
        w.add("namespace", projectNS);
        if (ne.elementExists("quota")) {
            w.add(ne.element("quota"));
        }
        w.pop();
    }

}
