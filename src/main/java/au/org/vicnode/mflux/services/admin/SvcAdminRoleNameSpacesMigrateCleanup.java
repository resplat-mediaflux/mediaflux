/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2023, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.admin;

import java.util.Collection;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcAdminRoleNameSpacesMigrateCleanup extends PluginService {




	private Interface _defn;


	public SvcAdminRoleNameSpacesMigrateCleanup()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("revoke-role", BooleanType.DEFAULT, "Check and report or  actually revoke  roles in step 1 (default false = check only).", 0, 1));
		_defn.add(new Interface.Element("revoke-network", BooleanType.DEFAULT, "Check and report rather or actually remove network restrictions in step 2 (default false = check only).", 0, 1));
		_defn.add(new Interface.Element("revoke-visibility", BooleanType.DEFAULT, "Check and report or actually remove visibility actors in step 3 (default false = check only).", 0, 1));
		_defn.add(new Interface.Element("revoke-admin-ACL", BooleanType.DEFAULT, "Check and report  or actually remove admin ACLs in step 4a (default false = check only).", 0, 1));
		_defn.add(new Interface.Element("revoke-project-ACL", BooleanType.DEFAULT, "Check and report or actually remove project ACLs in step 4b (default false = check only).", 0, 1));

	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Use with extreme care. One off service to clean up after migrating role namespaces with vicnode.admin.namespace.role.migrate. This service  1) revoke roles from VicNode (2a) and VicNode-Admin (2b) role namespaces from all appropriate actors (it does not bother revoking from VicNode: and VicNode-Admin: roles), 2) removes  network service restrictions with VicNode: or VicNode-Admin: roles, 3) removes visibility actors to project parent namespaces with roles from the VicNode: and VicNode-Admin: namespaces, if any, 4a) remove ACLs for roles (VicNode: and VicNode-Admin:) for asset namespaces /local, /local-admin, /local-admin/instrument-upload-manifests, /training and 4b) revoke ACLs for roles (VicNode: and VicNode-Admin:) from the /projects tree (slow).";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "admin.role.namespaces.migrate.cleanup";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		Boolean revokeRole = args.booleanValue("revoke-role", false);
		Boolean revokeNetwork = args.booleanValue("revoke-network", false);
		Boolean revokeVisibility = args.booleanValue("revoke-visibility", false);
		Boolean revokeAdminACLs = args.booleanValue("revoke-admin-ACL", false);
		Boolean revokeProjectACLs = args.booleanValue("revoke-project-ACL", false);


		// Step 1a
		// We need to revoke any VicNode: roles on any actors that hold them.
		w.push("revoke-VicNode-roles-from-actors");
		PluginTask.checkIfThreadTaskAborted();
		revokeVicNodeRolesFromActors (executor(), "VicNode", revokeRole, w);
		w.pop();


		// Step 1b
		// We need to revoke any VicNode-Admin: roles on any actors that hold them.
		w.push("revoke-VicNode-Admin-roles-from-actors");
		PluginTask.checkIfThreadTaskAborted();
		revokeVicNodeRolesFromActors (executor(), "VicNode-Admin", revokeRole, w);
		w.pop();


		// Step 2
		// Handle network services. There are some restrictions on authentication domains
		// utilising roles.
		PluginTask.checkIfThreadTaskAborted();
		w.push("update-network-restrictions");
		updateNetworkRestrictions (executor(), revokeNetwork, w);
		w.pop();

		// Step 3
		// Handle visibility actors. Visibility actors are applied to some
		// project parent namespaces
		PluginTask.checkIfThreadTaskAborted();
		w.push("update-visibility-actors");
		updateVisibilityActors (executor(), revokeVisibility, w);
		w.pop();

		// STep 4
		// ACL updates. 
		// Step 4a
		PluginTask.checkIfThreadTaskAborted();
		w.push("update-admin-ACLs");
		updateAdminACLs (executor(), revokeAdminACLs, w);
		w.pop();

		// Step 4b
		PluginTask.checkIfThreadTaskAborted();
		w.push("projects");
		updateProjectACLs (executor(), revokeProjectACLs, w);
		w.pop();
	}


	private static void updateAdminACLs (ServiceExecutor executor, Boolean revoke, XmlWriter w) throws Throwable {

		String ns = "/local";
		if (NameSpaceUtil.assetNameSpaceExists(executor, null, ns)) {
			w.push("namespace", new String[] {"path", ns});
			findAndRemoveACLs (executor, ns, "VicNode:", revoke, w);
			findAndRemoveACLs (executor, ns, "VicNode-Admin:", revoke, w);
			w.pop();
		}
		//
		ns = "/local-admin";
		if (NameSpaceUtil.assetNameSpaceExists(executor, null, ns)) {
			w.push("namespace", new String[] {"path", ns});
			findAndRemoveACLs (executor, ns, "VicNode:", revoke, w);
			findAndRemoveACLs (executor, ns, "VicNode-Admin:", revoke, w);
			w.pop();
		}
		//
		ns = "/local-admin/instrument-upload-manifests";
		if (NameSpaceUtil.assetNameSpaceExists(executor, null, ns)) {
			w.push("namespace", new String[] {"path", ns});
			findAndRemoveACLs (executor, ns, "VicNode:", revoke, w);
			findAndRemoveACLs (executor, ns, "VicNode-Admin:", revoke, w);
			w.pop();
		}
		//
		ns = "/training";
		if (NameSpaceUtil.assetNameSpaceExists(executor, null, ns)) {
			w.push("namespace", new String[] {"path", ns});
			findAndRemoveACLs (executor, ns, "VicNode:", revoke, w);
			findAndRemoveACLs (executor, ns, "VicNode-Admin:", revoke, w);
			w.pop();
		}
	}

	// We revoke the ACLs whether they exist or not (so the reporting may
	// be inaccurate)
	private static void updateProjectACLs (ServiceExecutor executor, Boolean revoke, XmlWriter w) throws Throwable {

		// First remove the standard ACL for /projects
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", "/projects");
		dm.add("actor", new String[] {"type", "role"}, "VicNode:standard-user");
		if (revoke) {
			executor.execute("asset.namespace.acl.revoke",dm.root());
		}
		w.add("namespace", new String[] {"actor", "VicNode:standard-user", "ACL-removed", revoke.toString()}, "/projects");

		// Now all of the projects
		Collection<String> ids = Project.projectIDs(executor, true, true);
		for (String id : ids) {
			PluginTask.checkIfThreadTaskAborted();

			ProjectHolder ph = new ProjectHolder(executor, id, true);

			// Get namespace
			String ns = ph.nameSpace();
			w.push("project", new String[] {"id", ph.id()});

			// Revoke standard ACLs.
			dm = new XmlDocMaker("args");
			dm.add("namespace", ns);
			dm.add("actor", new String[] {"type", "role"}, "VicNode-Admin:project-access");
			if (revoke) {
				executor.execute("asset.namespace.acl.revoke",dm.root());
			}
			w.add("namespace", new String[] {"actor", "VicNode-Admin:project-access", "ACL-removed", revoke.toString()}, ns);

			//
			dm = new XmlDocMaker("args");
			dm.add("namespace", ns);
			dm.add("actor", new String[] {"type", "role"}, "VicNode-Admin:project-asset-namespace-administrator");
			if (revoke) {
				executor.execute("asset.namespace.acl.revoke",dm.root());
			}
			w.add("namespace", new String[] {"actor", "VicNode-Admin:project-asset-namespace-administrator", "ACL-removed", revoke.toString()}, ns);

			//
			w.pop();
		}

	}


	private static void findAndRemoveACLs (ServiceExecutor executor, String namespace, String roleNS, Boolean revoke, XmlWriter w) throws Throwable {

		XmlDoc.Element meta = NameSpaceUtil.describe(null, executor, namespace);
		Collection<XmlDoc.Element> acls = meta.elements("namespace/acl"); 
		if (acls!=null) {
			for (XmlDoc.Element acl : acls) {
				XmlDoc.Element actor = acl.element("actor");
				String actorName = actor.value();
				String actorType = actor.value("@type");
				if (actorName.contains(roleNS)) {
					// Remove the ACL
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add("namespace", namespace);
					dm.add("actor", new String[] {"type", actorType}, actorName);

					if (revoke) {
						executor.execute("asset.namespace.acl.revoke", dm.root());
					}
					w.add("namespace", new String[] {"actor", actorName, "removed", revoke.toString()}, namespace);
				}
			}
		}
	}

	private static void updateVisibilityActors (ServiceExecutor executor, Boolean revoke, XmlWriter w) throws Throwable {

		// We have VicNode-Admin:<role> visibility actors on project parent namespaces
		XmlDoc.Element r = executor.execute("vicnode.project.parent.namespace.list");
		if (r==null) {
			return;
		}
		//
		Collection<String> parents = r.values("parent");
		if (parents!=null) {
			for (String parent : parents) {
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("namespace", parent);
				r = executor.execute("asset.namespace.describe", dm.root());
				XmlDoc.Element visibleTo = r.element("namespace/visible-to");
				if (visibleTo!=null) {		
					Collection<XmlDoc.Element> actors = visibleTo.elements("actor");
					if (actors!=null) {
						dm = new XmlDocMaker("args");
						dm.add("namespace", parent);
						dm.push("visible-to");
						w.push("namespace", new String[] {"path", parent});
						//
						Boolean some = false;
						for (XmlDoc.Element actor : actors) {
							String actorName = actor.value();
							String actorType = actor.value("@type");
							if (actorName.contains("VicNode-Admin:")) {	
								// Don't want this one
								w.add("visible-to", 
										new String[] {"removed", revoke.toString()}, 
										actorName);
								some = true;
							} else {
								// Do want this one
								dm.add("actor", new String[] {"type", actorType}, 
										actorName);
							}
						}
						dm.pop();
						w.pop();

						// We can only set all the actors, not remove one							if (revoke) {
						if (revoke && some) {
							executor.execute("asset.namespace.visibility.set", dm.root());
						}
					} 
				}
			}
		}
	}


	private static void updateNetworkRestrictions (ServiceExecutor executor, Boolean revoke, XmlWriter w) throws Throwable {

		//	:service < :restrict < :exclude-role <role> > >
		XmlDoc.Element r = executor.execute("network.describe");
		if (r==null) {
			return;
		}

		// We only have restrictions of the form    
		// :service < :restrict < :exclude-role "VicNode-Admin:enigma-external" > >
		Collection<XmlDoc.Element> services = r.elements("service");
		if (services!=null) {
			for (XmlDoc.Element service : services) {
				XmlDoc.Element restrict = service.element("restrict");   // Singleton
				if (restrict!=null) {
					// We have a restriction element
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add("port", service.value("@port"));
					dm.push("restrict");
					w.push("service", new String[] {"type", service.value("@type"), 
							"port", service.value("@port")});
					Collection<String> ers = restrict.values("exclude-role");
					Boolean some = false;
					for (String er : ers) {
						if (er.contains("VicNode-Admin:") || er.contains("VicNode:")) {
							// We don't want this one anymore
							w.add("exclude-role", new String[] {"removed", revoke.toString()}, er);
							some = true;
						} else {
							// Keep this one
							dm.add("exclude-role", er);
						}
					}
					dm.pop();
					w.pop();
					//
					if (revoke && some) {
						executor.execute("network.restrict.set", dm.root());
					} 
				}
			}
		}
	}

	private static void revokeVicNodeRolesFromActors (ServiceExecutor executor,
			String vnNamespace, Boolean revoke, XmlWriter w) throws Throwable {

		// FInd all roles in the desired role namespace
		Collection<XmlDoc.Element> vnRoles = describeRoles(executor, vnNamespace);

		// For each role, find the actors granted that role
		for (XmlDoc.Element vnRole : vnRoles) {
			Collection<XmlDoc.Element> actors = findGrantedActors (executor,
					vnRole);

			// Now for each actor, revoke the role. Some of the found actors will be 
			// roles and they will be in the VicNode or VicNode-Admin role namespace.
			// Just ignore these since they will be destroyed ultimately.
			if (actors!=null) {
				for (XmlDoc.Element actor : actors) {
					String actorName = actor.value("@name");
					String actorType = actor.value("@type");
					Boolean destroyed = actor.booleanValue("@destroyed",  false);
					
					// Work around the LDAP domains not working on old test (where I have disabled the domains)
					// actors.granted will find them ok but I can't revoke to them. So just drop them.
					Boolean proceed = true;
					if (destroyed || (actorType.equals("user") && !SvcAdminRoleNameSpacesMigrate.isActorDomainEnabled(executor,actorName))) {
						proceed = false;
					}
					//		
					if (proceed) {
						// We will find lots of destroyed plugin service actors
						// from previous installations.  Ignore these.
						Boolean revokeIt = true;
						if (actorType.equals("role")) {
							// See if this actor that holds the (VicNode or VicNode-ADmin) role 
							// is itself a VicNode or VicNode-Admin role. If so, we ignore it
							// the roles in the new namespaces
							if (actorName.contains("VicNode-Admin:") || actorName.contains("VicNode:")) {
								revokeIt = false;
							}
						} 

						// Revoke
						String vnRoleName = vnRole.value("name");
						if (revokeIt) {
							if (revoke) {
								revokeRole (executor, actorName, actorType, vnRoleName);
							}
							w.add("actor", new String[]{"type", actorType,
									"role-to-revoke", vnRoleName, "revoked", revoke.toString()},
									actorName);
						}

					}
				}
			}
		}

	}


	private static void revokeRole (ServiceExecutor executor, String actorName,
			String actorType, String roleToRevoke) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", actorName);
		dm.add("type",actorType);
		dm.add("role", new String[] {"type", "role"}, roleToRevoke);
		executor.execute("actor.revoke", dm.root());
	}

	private static Collection<XmlDoc.Element> findGrantedActors (ServiceExecutor executor,
			XmlDoc.Element role) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.push("role", new String[] {"type", "role"}, role.value("name"));
		XmlDoc.Element r = executor.execute("actors.granted", dm.root());
		return r.elements("actor");
	}


	private static Collection<XmlDoc.Element> describeRoles (ServiceExecutor executor, String namespace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		XmlDoc.Element r = executor.execute("authorization.role.describe", dm.root());
		return r.elements("role");
	}

}
