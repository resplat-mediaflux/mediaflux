package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.*;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;
import unimelb.utils.FileSizeUtils;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class SvcProjectQuotaUsedCheck extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.quota.used.check";

    private final Interface _def;

    public SvcProjectQuotaUsedCheck() {
        _def = new Interface();
        _def.add(new Interface.Element("project-id", StringType.DEFAULT, "A string to search for in the project IDs to restrict which projects are selected", 0, 1));
        _def.add(new Interface.Element("collection", StringType.DEFAULT, "A string to search for in the project's collection code of RCS Data Registry", 0, 1));
        _def.add(new Interface.Element("owner-email", EmailAddressType.DEFAULT, "Specifies, by email, the owner (RCAO) of the collection so that only the projects belong to this owner are selected", 0, 1));
        _def.add(new Interface.Element("prod-type", new EnumType(new String[]{"production", "test", "closed-stub", "all"}), "Select from 'all', (default, all types), 'production' (only production projects), 'test' (only test projects), 'closed-stub' (only closed-stub projects).", 0, 1));

        Interface.Element tolerance = new Interface.Element("tolerance", LongType.POSITIVE, "When comparing total content size with quota usage on live projects being modified, you cannot get the two values at the same time point. To work around this, we can set tolerance value and consider the two values match when the difference is below the tolerance. Defaults to 0.", 0, 1);
        tolerance.add(new Interface.Attribute("unit", new EnumType(Tolerance.Unit.values()), "The unit of the tolerance. Can be 'PERCENT', 'TB', 'GB', 'MB', 'KB' and 'B'. If set to 'PERCENT', it will be the percentage of the allocated quota.", 0));
        _def.add(tolerance);

        _def.add(new Interface.Element("regenerate", BooleanType.DEFAULT, "Regenerate the quota if the quota usage is invalid. Defaults to false", 0, 1));

        _def.add(new Interface.Element("result-include-all", BooleanType.DEFAULT, "Include all projects in the result. Defaults to true. If set to false, only the projects that have invalid quota usage are included.", 0, 1));

        Interface.Element email = new Interface.Element("email", XmlDocType.DEFAULT, "Send the results to email recipients", 0, 1);
        email.add(new Interface.Attribute("always", BooleanType.DEFAULT, "Always send email even if there is no invalid quota usage. Defaults to true. If set to false, it will only send email when there are projects with invalid quota usage.", 0));
        email.add(new Interface.Element("to", EmailAddressType.DEFAULT, "The recipient(s)", 1, 10));
        _def.add(email);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "verify the project quota usage and (optionally) regenerate it if issue is detected. It can also send the result to email recipients if specified.";
    }

    @Override
    public Interface definition() {
        return _def;
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String projectId = args.value("project-id");
        String collection = args.value("collection");
        String ownerEmail = args.value("owner-email");
        String prodType = args.value("prod-type");

        Tolerance tolerance = Tolerance.parse(args.element("tolerance"));

        boolean regenerate = args.booleanValue("regenerate", false);

        boolean resultIncludeAll = args.booleanValue("result-include-all", true);

        Collection<String> emailTo = args.uniqueValues("email/to");
        boolean emailAlways = args.booleanValue("email/@always", true);

        List<Project> allProjects = checkProjects(executor(), projectId, collection, ownerEmail, prodType, tolerance, regenerate);
        List<Project> invalidProjects = allProjects.stream().filter(p -> !p.quotaUsedMatchesTotalContentSize(tolerance)).collect(Collectors.toList());
        List<Project> resultProjects = resultIncludeAll ? allProjects : invalidProjects;


        if (!resultProjects.isEmpty()) {
            for (Project proj : resultProjects) {
                proj.saveXml(w, tolerance, regenerate);
            }
        }
        w.push("count");
        w.add("invalid", invalidProjects.size());
        w.add("total", allProjects.size());
        w.pop();

        if (emailTo != null && !emailTo.isEmpty() && (!invalidProjects.isEmpty() || emailAlways)) {
            sendEmail(executor(), allProjects, invalidProjects, resultIncludeAll, tolerance, regenerate, emailTo);
            w.push("email");
            for (String email : emailTo) {
                w.add("to", email);
            }
            w.pop();
        }
    }

    private static class Project {

        public final String projectId;
        public final String prodType;
        public final String collection;
        public final String namespacePath;
        public final long totalContentSize;
        public final Quota quota;

        Project(String projectId, String prodType, String collection, String namespacePath, long totalContentSize, Quota quota) {
            this.projectId = projectId;
            this.prodType = prodType;
            this.collection = collection;
            this.namespacePath = namespacePath;
            this.totalContentSize = totalContentSize;
            this.quota = quota;
        }

        boolean quotaUsedMatchesTotalContentSize(Tolerance tolerance) {
            long toleranceBytes = tolerance.numberOfBytes(this.quota);
            return Math.abs(this.totalContentSize - this.quota.used) <= toleranceBytes;
        }

        void saveXml(XmlWriter w, Tolerance tolerance, boolean regenerate) throws Throwable {
            w.push("project", new String[]{"id", this.projectId, "type", this.prodType, "collection", this.collection});
            w.add("path", this.namespacePath);
            if (this.quota != null) {
                boolean overflow = this.quota.allocation < this.totalContentSize;
                w.push("quota", new String[]{"overflow", overflow ? "true" : null, "regenerate", regenerate ? "true" : null});
                w.add("allocation", new String[]{"h", FileSizeUtils.getHumanReadableSize(this.quota.allocation)}, this.quota.allocation);
                boolean valid = this.quotaUsedMatchesTotalContentSize(tolerance);
                w.add("used", new String[]{"h", FileSizeUtils.getHumanReadableSize(this.quota.used), "valid", Boolean.toString(valid)}, this.quota.used);
                w.pop();
            }
            w.add("total-content-size", new String[]{"all-versions", "true", "h", FileSizeUtils.getHumanReadableSize(this.totalContentSize)}, this.totalContentSize);
            w.pop();
        }

        void saveCsvRow(StringBuilder sb, boolean regenerate) {
            sb.append(this.projectId).append(",");
            sb.append(this.collection == null ? "" : this.collection).append(",");
            sb.append(this.prodType).append(",");
            sb.append(this.namespacePath).append(",");
            if (this.quota != null) {
                sb.append(this.quota.allocation).append(",");
                sb.append(this.quota.used).append(",");
            } else {
                sb.append(",");
                sb.append(",");
            }
            sb.append(this.totalContentSize);
            if (regenerate) {
                sb.append(",");
                sb.append("true");
            }
            sb.append("\n");
        }

        void saveHtmlTableRow(StringBuilder sb, Tolerance tolerance, boolean regenerate) {
            boolean valid = this.quotaUsedMatchesTotalContentSize(tolerance);
            if (!valid) {
                sb.append("<tr style=\"color:#3333dd; background:#ccc; font-weight:bold;\">");
            } else {
                sb.append("<tr>");
            }
            sb.append("<td>").append(this.projectId).append("</td>");

            sb.append("<td>");
            if (this.collection != null) {
                String collectionUrl = collectionUrl(this.collection);
                sb.append(String.format("<a href=\"%s\">%s</a>", collectionUrl, this.collection));
            }
            sb.append("</td>");

            if (this.quota != null) {
                sb.append("<td style=\"text-align:right\">").append(FileSizeUtils.getHumanReadableSize(this.quota.allocation)).append("<br/>").append(String.format("%,d bytes", this.quota.allocation)).append("</td>");
                sb.append("<td style=\"text-align:right\">").append(FileSizeUtils.getHumanReadableSize(this.quota.used)).append("<br/>").append(String.format("%,d bytes", this.quota.used)).append("</td>");
            } else {
                sb.append("<td>").append("</td>");
                sb.append("<td>").append("</td>");
            }
            sb.append("<td style=\"text-align:right\">").append(FileSizeUtils.getHumanReadableSize(this.totalContentSize)).append("<br/>").append(String.format("%,d bytes", this.totalContentSize)).append("</td>");
            if (regenerate) {
                sb.append("<td style=\"text-align:center\">").append(valid ? "" : "true").append("</td>");
            }
            sb.append("</tr>");
        }

        static void appendHtmlTable(StringBuilder sb, List<Project> projects, Tolerance tolerance, boolean regenerate) {
            sb.append("<style>\n");
            sb.append("table, th, td { border: 1px solid;}\n");
            sb.append("</style>\n");
            sb.append("<table><thead><tr>");
            sb.append("<th>Project ID</th><th>Collection</th><th>Quota Allocated</th><th>Quota Used</th><th>Total Content Size<br/>(All versions)</th>");
            if (regenerate) {
                sb.append("<th>Regenerate Quota</th>");
            }
            sb.append("</tr></thead><tbody>");
            if (projects != null) {
                projects.forEach(p -> p.saveHtmlTableRow(sb, tolerance, regenerate));
            }
            sb.append("</tbody></table>");
        }

        static String toCSV(List<Project> projects, boolean regenerate) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Project ID,Collection,Type,Path,Quota Allocated,Quota Used,Total Content Size(All versions)");
            if (regenerate) {
                sb.append(",Regenerate Quota");
            }
            sb.append("\n");
            if (projects != null) {
                projects.forEach(p -> p.saveCsvRow(sb, regenerate));
            }
            return sb.toString();
        }

    }

    private static class Quota {
        public final long allocation;
        public final long used;

        Quota(XmlDoc.Element qe) throws Throwable {
            this.allocation = qe.longValue("allocation", 0);
            this.used = qe.longValue("used", 0);
        }

    }

    private static class Tolerance {

        enum Unit {
            PERCENT, TB, GB, MB, KB, B
        }

        public final long value;
        public final Unit unit;

        Tolerance(long value, Unit unit) {
            this.value = value;
            this.unit = unit;
        }

        long numberOfBytes(Quota quota) {
            if (this.value == 0) {
                return 0;
            }
            switch (this.unit) {
                case PERCENT:
                    return (long) (((double) quota.allocation) * ((double) this.value / 100.0));
                case TB:
                    return 1000000000000L * this.value;
                case GB:
                    return 1000000000L * this.value;
                case MB:
                    return 1000000 * this.value;
                case KB:
                    return 1000 * this.value;
                default:
                    return this.value;
            }
        }

        public String toString() {
            return String.format("%d%s", this.value, this.unit == Unit.PERCENT ? "%" : this.unit.name().toUpperCase());
        }

        static Tolerance parse(XmlDoc.Element te) throws Throwable {
            if (te == null) {
                return new Tolerance(0, Unit.B);
            }
            long value = te.longValue();
            Unit unit = Unit.valueOf(te.stringValue("@unit", "B").toUpperCase());
            return new Tolerance(value, unit);
        }
    }

    private static void sendEmail(ServiceExecutor executor, List<Project> allProjects, List<Project> invalidProjects, boolean resultIncludeAll, Tolerance tolerance, boolean regenerate, Collection<String> emailTo) throws Throwable {
        String csv = null;
        StringBuilder body = new StringBuilder();
        body.append("<html>\n");
        body.append("<p><b>").append(invalidProjects.size()).append("/").append(allProjects.size()).append("</b> project").append(invalidProjects.size() > 1 ? "s have" : " has").append(" invalid quota usage.</p>\n");
        if (resultIncludeAll) {
            if (!allProjects.isEmpty()) {
                body.append("<h3>Mediaflux Project Quota Usage");
                if (tolerance != null && tolerance.value > 0) {
                    body.append("(Tolerance: ").append(tolerance).append(")");
                }
                body.append("</h3>");
                Project.appendHtmlTable(body, allProjects, tolerance, regenerate);
                csv = Project.toCSV(allProjects, regenerate);
            }
        } else {
            if (!invalidProjects.isEmpty()) {
                body.append("<h3>Mediaflux Projects with Invalid Quota Usage");
                if (tolerance != null && tolerance.value > 0) {
                    body.append("(Tolerance: ").append(tolerance).append(")");
                }
                body.append("</h3>");
                Project.appendHtmlTable(body, invalidProjects, tolerance, regenerate);
                csv = Project.toCSV(invalidProjects, regenerate);
            }
        }
        XmlDocMaker dm = new XmlDocMaker("args");
        for (String email : emailTo) {
            dm.add("to", email);
        }
        PluginTask.checkIfThreadTaskAborted();
        String uuid = executor.execute("server.uuid").value("uuid");
        dm.add("subject", String.format("[%s] %s", uuid, SERVICE_NAME.replaceAll("^vicnode.", "")));
        dm.add("body", new String[]{"type", "text/html"}, body.toString());
        if (csv != null) {
            String attachmentName = String.format("%s-%s-%s.csv", uuid, SERVICE_NAME.replaceAll("^vicnode.", "").replace('.', '_'), new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            dm.push("attachment");
            dm.add("name", attachmentName);
            dm.add("type", "text/csv");
            dm.pop();
            byte[] csvBytes = csv.getBytes();
            try (ByteArrayInputStream bais = new ByteArrayInputStream(csvBytes)) {
                PluginTask.checkIfThreadTaskAborted();
                executor.execute("mail.send", dm.root(), new PluginService.Inputs(new PluginService.Input(bais, csvBytes.length, "text/csv", null)), null);
            }
        } else {
            PluginTask.checkIfThreadTaskAborted();
            executor.execute("mail.send", dm.root());
        }


    }


    private static List<Project> checkProjects(ServiceExecutor executor, String projectId, String collection, String ownerEmail, String prodType, Tolerance tolerance, boolean regenerate) throws Throwable {
        List<XmlDoc.Element> pes = listProjects(executor, projectId, collection, ownerEmail, prodType);
        List<Project> results = new ArrayList<>(pes == null || pes.isEmpty() ? 0 : pes.size());
        if (pes != null) {
            PluginTask.threadTaskBeginSetOf(pes.size());
            for (XmlDoc.Element pe : pes) {
                Project r = checkProject(executor, pe, tolerance, regenerate);
                results.add(r);
                PluginTask.threadTaskCompleted(1);
            }
            PluginTask.threadTaskEndSet();
        }
        return results;
    }

    private static Project checkProject(ServiceExecutor executor, XmlDoc.Element pe, Tolerance tolerance, boolean regnerate) throws Throwable {
        String namespacePath = pe.value("@path");
        String collection = pe.value("@collection");
        String prodType = pe.value("@type");
        String projectId = pe.value();

        PluginTask.setCurrentThreadActivity("checking project: " + projectId);

        PluginTask.checkIfThreadTaskAborted();
        long totalContentSize = getTotalContentSize(executor, namespacePath);

        PluginTask.checkIfThreadTaskAborted();
        Quota quota = getAssetNamespaceQuota(executor, namespacePath);

        PluginTask.clearCurrentThreadActivity();

        Project project = new Project(projectId, prodType, collection, namespacePath, totalContentSize, quota);
        if (regnerate && !project.quotaUsedMatchesTotalContentSize(tolerance)) {
            PluginTask.checkIfThreadTaskAborted();
            PluginTask.setCurrentThreadActivity("regenerating quota for project: " + projectId);
            regenerateQuota(executor, namespacePath);
            PluginTask.clearCurrentThreadActivity();
        }
        return project;
    }

    private static List<XmlDoc.Element> listProjects(ServiceExecutor executor, String projectId, String collection, String ownerEmail, String prodType) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");

        dm.addIfSet("contains", projectId);
        dm.addIfSet("collection", collection);
        dm.addIfSet("owner-email", ownerEmail);
        dm.addIfSet("prod-type", prodType);

        PluginTask.checkIfThreadTaskAborted();
        return executor.execute(SvcProjectList.SERVICE_NAME, dm.root()).elements("project");
    }


    private static Quota getAssetNamespaceQuota(ServiceExecutor executor, String namespace) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", namespace);

        PluginTask.checkIfThreadTaskAborted();
        XmlDoc.Element qe = executor.execute("asset.namespace.describe", dm.root()).element("namespace/quota");
        if (qe.elementExists("allocation")) {
            return new Quota(qe);
        } else {
            return null;
        }
    }


    private static long getTotalContentSize(ServiceExecutor executor, String namespace) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "namespace>='" + namespace + "'");
        dm.add("action", "sum");
        dm.add("xpath", "content/@total-size");

        PluginTask.checkIfThreadTaskAborted();
        return executor.execute("asset.query", dm.root()).longValue("value", 0);
    }

    private static void regenerateQuota(ServiceExecutor executor, String namespace) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", namespace);

        PluginTask.checkIfThreadTaskAborted();
        executor.execute("asset.namespace.quota.regenerate", dm.root());
    }

    private static String collectionUrl(String collection) {
        return String.format("https://dashboard.storage.unimelb.edu.au/collections/%s/", collection);
    }
}
