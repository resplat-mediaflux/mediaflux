package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

import java.util.*;

public class SvcProjectBriefList extends PluginService {

    private static class ProjectBrief {
        final String id;
        final String namespace;
        final String collection;
        final String projectType;
        final String replicationQueue;
        final boolean replicationEnabled;

        ProjectBrief(String projId, String projNS, String collectionKey, String projectType, String replicationQueue) {
            this.id = projId;
            this.namespace = projNS;
            this.collection = collectionKey;
            this.projectType = projectType;
            this.replicationQueue = replicationQueue;
            this.replicationEnabled = (replicationQueue != null && !replicationQueue.equalsIgnoreCase("none"));            
        }

        public String parentNamespace() {
            return this.namespace.substring(0, this.namespace.lastIndexOf('/'));
        }

        public void save(XmlWriter w) throws Throwable {
            w.add("project", new String[]{"parent", this.parentNamespace(), "type", this.projectType, "replicate", Boolean.toString(this.replicationEnabled), "collection", this.collection}, this.id);
        }
    }

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.brief.list";

    private Interface _defn;

    public SvcProjectBriefList() {
        _defn = new Interface();
        _defn.add(new Interface.Element("contains", StringType.DEFAULT,
                "A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",
                0, 1));
        _defn.add(new Interface.Element("parent", StringType.DEFAULT,
                "The parent namespace to consider when looking for projects. Should start with a '/'.  Defaults to all parents.",
                0, 1));
        _defn.add(new Interface.Element("collection", StringType.DEFAULT,
                "A string to search for in the project's data registry collection code.", 0, 1));

        SvcProjectDescribe.addProdType(_defn);
        SvcProjectDescribe.addRepType(_defn);

        _defn.add(new Interface.Element("order", new EnumType(new String[]{"asc", "desc"}), "Sort ascending or descending. Default is ascending.", 0, 1));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "List projects briefly.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String projeIDContains = args.value("contains");
        String parentNS = args.value("parent");
        if (parentNS != null) {
            if (!parentNS.startsWith("/")) {
                parentNS = "/" + parentNS;
            }
            while (parentNS.endsWith("/")) {
                parentNS = parentNS.substring(0, parentNS.length() - 1);
            }
        }
        String collectionContains = args.value("collection");

        String projType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
        String repType = args.stringValue("rep-type", Properties.repTypes[0]);
        String order = args.stringValue("order", "asc");
        boolean descOrder = "desc".equalsIgnoreCase(order);

        SortedMap<String, ProjectBrief> projBriefs = getProjectBriefs(executor(), projeIDContains, parentNS, collectionContains, projType, repType, descOrder);

        Set<String> projIDs = projBriefs.keySet();
        for (String projID : projIDs) {
            ProjectBrief proj = projBriefs.get(projID);
            proj.save(w);
        }
    }

    static SortedMap<String, ProjectBrief> getProjectBriefs(ServiceExecutor executor,
                                                            String projIDContains,
                                                            String parentNS,
                                                            String collectionContains,
                                                            String projectType,
                                                            String replicationType,
                                                            boolean desc) throws Throwable {

        String projDocType = Properties.getProjectDocType(executor);
        String collectionDocType = Properties.getCollectionDocType(executor);
        String projNSMapDict = Properties.getProjectNamespaceMapDictionaryName(executor);
        SortedMap<String, ProjectBrief> projBriefs = new TreeMap<String, ProjectBrief>((s1, s2) -> {
            int idx1 = s1.lastIndexOf('.');
            int idx2 = s2.lastIndexOf('.');
            int n1 = Integer.parseInt(s1.substring(idx1 + 1));
            int n2 = Integer.parseInt(s2.substring(idx2 + 1));
            return desc ? Integer.compare(n2, n1) : Integer.compare(n1, n2);
        });
        List<XmlDoc.Element> ees = executor.execute("dictionary.entries.describe",
                "<args><dictionary>" + projNSMapDict + "</dictionary></args>",
                null, null).elements("entry");
        if (ees != null) {
            for (XmlDoc.Element ee : ees) {
                String projID = ee.value("term");
                if (projIDContains != null && !projID.contains(projIDContains)) {
                    continue;
                }
                String projNS = ee.value("definition");
                if (parentNS != null && !projNS.startsWith(parentNS + "/")) {
                    continue;
                }
                XmlDoc.Element projNSE = describeAssetNamespace(executor, projNS);
                String projType = projNSE.value("asset-meta/" + projDocType + "/type");
                if (projectType != null && !projectType.equals(Properties.PROJECT_TYPE_ALL) && !projectType.equalsIgnoreCase(projType)) {
                    continue;
                }
                String collectionKey = projNSE.value("asset-meta/" + collectionDocType + "/key");
                if (collectionKey != null) {
                    collectionKey = collectionKey.replaceAll("^.*:", "");
                }
                if (collectionContains != null) {
                    if (collectionKey == null || !collectionKey.toLowerCase().contains(collectionContains.toLowerCase())) {
                        continue;
                    }
                }
                String replicationQueue = projNSE.stringValue("enqueue[@name='Replication']/queue", "none");
                ProjectBrief proj = new ProjectBrief(projID, projNS, collectionKey, projType,  replicationQueue);
                if (replicationType != null && !replicationType.equalsIgnoreCase("all")) {
                    if ("no-replication".equalsIgnoreCase(replicationType) && proj.replicationEnabled) {
                        continue;
                    }
                    if ("replication".equalsIgnoreCase(replicationType) && !proj.replicationEnabled) {
                        continue;
                    }
                }
                projBriefs.put(proj.id, proj);
            }
        }
        return projBriefs;
    }

    static XmlDoc.Element describeAssetNamespace(ServiceExecutor executor, String assetNamespace) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", assetNamespace);
        return executor.execute("asset.namespace.describe", dm.root()).element("namespace");
    }
}
