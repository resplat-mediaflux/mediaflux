package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.*;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.services.shareable.Projects;

import java.util.ArrayList;
import java.util.List;

public class SvcUserSelfProjectEnum extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "user.self.project.enum";

    public static final long DEFAULT_IDX = 1L;
    public static final int DEFAULT_SIZE = 100;

    private final Interface _defn;

    public SvcUserSelfProjectEnum() {
        _defn = new Interface();
        _defn.add(new Interface.Element("valid", StringType.DEFAULT, " If present, a value to check for validity.", 0
                , 1));
        Interface.Element values = new Interface.Element("values", XmlDocType.DEFAULT,
                "If present a request for enumerated values.", 0, 1);
        values.add(new Interface.Element("idx", LongType.POSITIVE_ONE,
                "The initial index for the results. Defaults to " + DEFAULT_IDX + ".", 0, 1));
        values.add(new Interface.Element("size", IntegerType.POSITIVE_ONE,
                "The maximum number of results to return. Defaults to " + DEFAULT_SIZE + ".", 0, 1));
        Interface.Element prefix = new Interface.Element("prefix", StringType.DEFAULT,
                "Prefix for filtering.", 0, 1);
        prefix.add(new Interface.Attribute("case-sensitive", BooleanType.DEFAULT,
                "Should the comparison be case-sensitive? Defaults to true", 0));
        values.add(prefix);
        _defn.add(values);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Enumeration service that list or validate the assessible projects.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        if (args.elementExists("values")) {
            XmlDoc.Element ve = args.element("values");
            long idx = ve.longValue("idx", DEFAULT_IDX);
            int size = ve.intValue("size", DEFAULT_SIZE);
            String prefix = ve.value("prefix");
            boolean prefixCaseSensitive = ve.booleanValue("prefix/@case-sensitive", true);
            List<String> projectIds = listProjects((int) idx, size, prefix, prefixCaseSensitive);
            if (projectIds != null) {
                for (String projectId : projectIds) {
                    w.push("value");
                    w.add("name", projectId);
                    w.add("description", "project: " + projectId);
                    w.pop();
                }
            }
        }
        if (args.elementExists("valid")) {
            String v = args.value("valid");
            w.add("valid", new String[]{"value", v}, projectExists(v));
        }
    }

    private boolean projectExists(String v) throws Throwable {
        List<String> projectIds = Projects.getWritableProjectIds(executor());
        if (projectIds != null) {
            return projectIds.contains(v);
        }
        return false;
    }

    private List<String> listProjects(int idx, int size, String prefix, boolean prefixCaseSensitive) throws Throwable {
        List<String> projectIds = Projects.getWritableProjectIds(executor());
        if (projectIds == null || projectIds.isEmpty()) {
            return null;
        }
        List<String> r = prefix == null ? projectIds : new ArrayList<>();
        if (prefix != null) {
            for (String projectId : projectIds) {
                if (prefixCaseSensitive) {
                    if (projectId.startsWith(prefix)) {
                        r.add(projectId);
                    }
                } else {
                    if (projectId.toLowerCase().startsWith(prefix.toLowerCase())) {
                        r.add(projectId);
                    }
                }
            }
        }
        if (idx > r.size()) {
            return null;
        }
        return r.subList(idx - 1, idx - 1 + size > r.size() ? r.size() : idx - 1 + size);
    }
}
