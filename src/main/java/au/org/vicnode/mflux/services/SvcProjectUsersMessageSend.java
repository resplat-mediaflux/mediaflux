/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.HashSet;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;
import au.org.vicnode.mflux.plugin.util.Util;

/**
 * 
 * @author nebk
 *
 */

public class SvcProjectUsersMessageSend extends PluginService {

	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectUsersMessageSend()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("email", StringType.DEFAULT, "Send the message to this address also.",
				0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("body", StringType.DEFAULT, "The message body.", 1, 1));
		_defn.add(new Interface.Element("subject", StringType.DEFAULT, "The message subject.", 1, 1));
		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
		_defn.add(new Interface.Element("from-is-caller",BooleanType.DEFAULT,"Set the 'from' field to the caller if true. If false (default), 'from' is set to the value of the server property 'mail.from'.",0,1));
		_defn.add(new Interface.Element("provision", BooleanType.DEFAULT, "Notify the standard provisioning user (that holds the admin role for all projects) as well (default false) .", 0, 1));
		_defn.add(new Interface.Element("who", new EnumType(Properties.notificationRecipients), 
				"Select from 'administrator' (users with admin role), 'owner' (users specified as owners in meta-data),"
						+ "'manage' (admin+owner), 'user' (all users with a project role) and 'all' (user + owner + administrator).  Default is 'user'", 0, 1));
		_defn.add(new Interface.Element("send", StringType.DEFAULT, "Actually send the message (default true).", 0, 1));
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Send a message to all users of a specified project.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.users.message.send";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		Collection<String> emails = args.values("email");
		String who = args.stringValue("who", "user");
		String subject = args.stringValue("subject");
		String body = args.stringValue("body");
		Boolean fromIsCaller = args.booleanValue("from-is-caller",  false);
		Boolean dropProvision = !(args.booleanValue("provision", false));
		Boolean send = args.booleanValue("send", true);
		//
		w.add("subject", subject);
		w.add("body", body);

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) {
			return;
		}

		// FInd users and add to list
		HashSet<String> emails2 = new HashSet<String>();        // WIll make unique list
		for (String projectID : projectIDs) {
			Collection<String> t = Project.listProjectEmails(executor(), projectID, who, dropProvision);
			if (t!=null && t.size()>0) {
				emails2.addAll(t);
			}
		}

		// Add extra emails
		if (emails !=null && emails.size()>0) {
			emails2.addAll(emails);
		}
		
		w.add("size", emails2.size());

		if (emails2!=null && emails2.size()>0) {
			String from;
			if (fromIsCaller) {
				from = User.getUserEMail(executor());
			} else {
				from = Util.getServerProperty(executor(), "notification.from");
			}
			if (send) {
				MailHandler.sendMessage (executor(), emails2, subject, body, null, true, from, w);
			} else {
				for (String email : emails2) {
					w.add("email", email);
				}
			}
		}
	}
}
