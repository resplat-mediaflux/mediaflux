/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Vector;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;

public class SvcProjectStoreSet extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectStoreSet()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
		_defn.add(new Interface.Element("policy", StringType.DEFAULT, "The name of the new primary store policy.", 1, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Sets the store or store policy of a primary project.  Note this has no impact on existing assets - see also vicnode.project.store.migrate";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.store.set";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		// Inputs
		String theProjectID = args.value("project-id");
		Boolean doDR = args.booleanValue("doDR", false);
		String storePolicy = args.value("policy");

		// ServerRoute to DR
		ServerRoute sr = ProjectReplicate.DRServerRoute(executor());


		// Validate store or store policy
		String drStore = null;
		String drStorePolicy = null;

		if (!Project.storePolicyIsKnown(executor(), storePolicy)) {
			throw new Exception("The primary store policy '" + storePolicy + "' is not known in the Project provisioning system dictionary.");
		}
		if (!Util.storePolicyExists(executor(), null, storePolicy)) {
			throw new Exception ("The store policy '" + storePolicy + "' does not exist on the primary server");
		}

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;
		//
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);

			w.push("project");
			w.add("id", projectID);

			// Set primary
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("namespace", ph.nameSpace());
			w.push("primary");
			dm.add("store-policy", storePolicy);
			w.add("store-policy", storePolicy);
			executor().execute("asset.namespace.store.set", dm.root());
			w.pop();


			// Set DR 
			if (doDR) {
				w.push("DR");

				// Not all projects are replicated so make sure it exists.
				String DRServerUUID = ProjectReplicate.DRServerUUID(executor());

				if (NameSpaceUtil.assetNameSpaceExists(executor(), DRServerUUID, ph.nameSpaceDR())) {					dm = new XmlDocMaker("args");
					dm.add("namespace", ph.nameSpaceDR());
					dm.add("store-policy", drStorePolicy);
					w.add("store-policy", drStore);
					executor().execute(sr, "asset.namespace.store.set", dm.root());
				}
				w.pop();
			}
			w.pop();
		}	
	}

}
