package au.org.vicnode.mflux.services.shareable;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.Session;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.services.SvcUserSelfProjectList;

public class Projects {

    public static final String VALUE_SEPARATORS = ";,";

    public static Collection<String> parseProjectIds(String listOfProjectIds) {
        if (listOfProjectIds == null || listOfProjectIds.trim().isEmpty()) {
            return null;
        }
        String[] parts = listOfProjectIds.trim().replaceAll("^[" + VALUE_SEPARATORS + "]+", "")
                .replaceAll("[" + VALUE_SEPARATORS + "]+$", "").split(" *[" + VALUE_SEPARATORS + "]+ *");
        if (parts.length == 0) {
            return null;
        }
        Set<String> projectIds = new LinkedHashSet<String>();
        for (String projectId : parts) {
            if (!projectId.isEmpty()) {
                projectIds.add(projectId.toLowerCase());
            }
        }
        if (projectIds.isEmpty()) {
            return null;
        }
        return projectIds;

    }

    static Set<String> parseProjectNamespaces(ServiceExecutor executor, String listOfProjectIds) throws Throwable {
        if (listOfProjectIds == null || listOfProjectIds.trim().isEmpty()) {
            return null;
        }
        Collection<String> projectIds = Projects.parseProjectIds(listOfProjectIds);
        if (projectIds == null) {
            throw new IllegalArgumentException("Failed to parse project id from: " + listOfProjectIds);
        }
        Set<String> nss = new LinkedHashSet<String>();
        for (String id : projectIds) {
            if (!id.isEmpty()) {
                PluginTask.checkIfThreadTaskAborted();
                String ns = resolveProjectNamespace(executor, id);
                if (ns != null) {
                    nss.add(ns);
                }
            }
        }
        if (nss.isEmpty()) {
            throw new IllegalArgumentException("Failed to parse project id: " + listOfProjectIds);
        }
        return nss;
    }

    static XmlDoc.Element resolveProject(ServiceExecutor executor, String id) throws Throwable {
        // result project element
        XmlDoc.Element rpe = null;

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("contains", id);
        List<XmlDoc.Element> pes = executor.execute(SvcUserSelfProjectList.SERVICE_NAME, dm.root()).elements("project");
        if (pes != null) {
            for (XmlDoc.Element pe : pes) {
                boolean writable = pe.booleanValue("role/@writable");
                if (writable) {
                    if (rpe == null) {
                        rpe = pe;
                    } else {
                        // more than one project found.
                        return null;
                    }
                }
            }
        }
        return rpe;
    }

    static String resolveProjectNamespace(ServiceExecutor executor, String id) throws Throwable {
        XmlDoc.Element pe = resolveProject(executor, id);
        if (pe != null) {
            String projectParent = pe.value("project/@parent");
            String projectId = pe.value("project");
            return String.format("%s/%s", projectParent, projectId);
        }
        return null;
    }

    static String resolveProjectId(ServiceExecutor executor, String id) throws Throwable {
        XmlDoc.Element pe = resolveProject(executor, id);
        if (pe != null) {
            return pe.value("project");
        }
        return null;
    }

    public static List<XmlDoc.Element> getAccessibleProjects(ServiceExecutor executor, String contains,
            boolean writableOnly) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        if (contains != null) {
            dm.add("contains", contains);
        }
        List<XmlDoc.Element> pes = executor.execute(SvcUserSelfProjectList.SERVICE_NAME, dm.root()).elements("project");
        if (pes == null || pes.isEmpty()) {
            return null;
        }
        if (!writableOnly) {
            return pes;
        }
        List<XmlDoc.Element> r = new ArrayList<XmlDoc.Element>();
        for (XmlDoc.Element pe : pes) {
            boolean writable = pe.booleanValue("role/@writable");
            if (writable) {
                r.add(pe);
            }
        }
        return r.isEmpty() ? null : r;
    }

    public static List<XmlDoc.Element> getWritableProjects(ServiceExecutor executor, String contains) throws Throwable {
        return getAccessibleProjects(executor, contains, true);
    }

    public static List<XmlDoc.Element> getWritableProjects(ServiceExecutor executor) throws Throwable {
        return getAccessibleProjects(executor, null, true);
    }

    public static List<String> getAccessibleProjectIds(ServiceExecutor executor, String contains, boolean writableOnly)
            throws Throwable {
        List<XmlDoc.Element> pes = getAccessibleProjects(executor, contains, writableOnly);
        if (pes == null) {
            return null;
        }
        List<String> projectIds = new ArrayList<String>(pes.size());
        for (XmlDoc.Element pe : pes) {
            projectIds.add(pe.value("project"));
        }
        return projectIds;
    }

    public static List<String> getWritableProjectIds(ServiceExecutor executor, String contains) throws Throwable {
        return getAccessibleProjectIds(executor, contains, true);
    }

    public static List<String> getWritableProjectIds(ServiceExecutor executor) throws Throwable {
        return getAccessibleProjectIds(executor, null, true);
    }

    public static String resolveAccessibleProjectIdFromNSPath(ServiceExecutor executor, String assetNamespace)
            throws Throwable {
        XmlDoc.Element re = executor.execute(SvcUserSelfProjectList.SERVICE_NAME);
        List<XmlDoc.Element> pes = re.elements("project");
        if (pes == null || pes.isEmpty()) {
            String actorName = Session.user() == null ? null : (Session.user().domain() + ":" + Session.user().name());
            throw new Exception(
                    "No accessible project found" + (actorName == null ? "." : (" for user: " + actorName)));
        }

        for (XmlDoc.Element pe : pes) {
            String projectParent = pe.value("project/@parent");
            String projectId = pe.value("project");
            String projectNS = projectParent + "/" + projectId;
            if (assetNamespace.startsWith(projectNS + "/")) {
                return projectId;
            }
        }
        return null;
    }

    public static String resolveProjectIdFromNSPath(String assetNamespace) throws Throwable {
        // https://stackoverflow.com/questions/175103/regex-to-match-url-end-of-line-or-character
        // pattern to find project id
        Pattern pattern = Pattern.compile("/(proj-[^/]+-\\d+.\\d+.\\d+)(/|\\z)");
        Matcher matcher = pattern.matcher(assetNamespace);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    public static String resolveProjectNSFromNSPath(String assetNamespace) throws Throwable {
        // https://stackoverflow.com/questions/175103/regex-to-match-url-end-of-line-or-character
        // pattern to find project namespace path
        Pattern pattern = Pattern.compile("/(proj-[^/]+-\\d+.\\d+.\\d+)(/|\\z)");
        Matcher matcher = pattern.matcher(assetNamespace);
        if (matcher.find()) {
            return assetNamespace.substring(0, matcher.end(1));
        }
        return null;
    }

    public static String getProjectRole(String projectId, Project.ProjectRoleType type) throws Throwable {
        return projectId + ":" + type.fromEnum();
    }

}
