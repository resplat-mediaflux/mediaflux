/**
 * @author Neil Killeen
 * <p>
 * Copyright (c) 2016, The University of Melbourne, Australia
 * <p>
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;

public class SvcProjectActorRemoveAll extends PluginService {


	private Interface _defn;

	public SvcProjectActorRemoveAll() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT, "The project ID (of the form 'proj-<name>-<cid>'.", 1, 1));
		_defn.add(new Interface.Element("user", BooleanType.DEFAULT, "Apply the process to users (default false) including adding self as admin.", 0, 1));
		_defn.add(new Interface.Element("identity", BooleanType.DEFAULT, "Apply the process to secure identity tokens (default false).", 0, 1));
		_defn.add(new Interface.Element("list-only", BooleanType.DEFAULT, 
				"Just list, don't actually revoke (default false).", 0, 1));

	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "By revoking roles, removes access to the given project for all of the given project users (including disabled ones) and secure identity tokens and adds administrator access to the calling user.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.actor.remove.all";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		final String projectID = args.value("project-id");
		final Boolean user = args.booleanValue("user",  false);
		final Boolean identity = args.booleanValue("identity", false);
		Boolean listOnly = args.booleanValue("list-only", false);
		if (!user && !identity) {
			throw new Exception ("Set either user and/or identity = true");
		}

		// Validate.
		String s[] = Project.validateProject(executor(), projectID, false);
		String projectID2 = s[1];

		// Do it
		removeActorsAndAddSelf (projectID2, user, identity, listOnly, w);
	}


	private void removeActorsAndAddSelf (String projectID, Boolean doUser,
			Boolean doIdentity, Boolean listOnly, XmlWriter w) throws Throwable {

		ServiceExecutor executor = executor();

		// Users
		if (doUser) {
			w.push("users");
			// Get the users
			XmlDoc.Element currentUsers = Project.listUsers(executor, projectID, false, true);
			if (currentUsers!=null) {

				// Now revoke the project roles on these users
				Collection<XmlDoc.Element> roles = currentUsers.elements("role");
				if (roles!=null) {
					for (XmlDoc.Element role : roles) {
						Collection<String> users = role.values("user");
						if (users!=null) {
							for (String user : users) {
								if (!listOnly) {
									User.grantRevokeRole(executor, user, role.value("@name"), false);
									w.add("user", 
											new String[] {"role", role.value("@name"), "revoked", "true"}, user);
								} else {
									w.add("user", 
											new String[] {"role", role.value("@name"), "revoked", "false"}, user);
								}
							}
						}
					}
				}
			}

			// Add self as administrator
			XmlDoc.Element self = User.describeUser(executor);
			String selfUser = self.value("user/@domain")+":"+self.value("user/@user");

			Project.addUser(executor, projectID, selfUser,
					Project.ProjectRoleType.ADMIN);
			w.add("granted", new String[] {"role", "administrator"}, selfUser);
			w.pop();
		}

		// Identity tokens
		if (doIdentity) {
			w.push("identity-tokens");
			XmlDoc.Element t = Project.listIdentityTokens(executor, projectID, false);
			Collection<XmlDoc.Element> roles = t.elements("role");
			if (roles!=null) {
				String standardUserRoleName = Properties.getStandardUserRoleName(executor);
				for (XmlDoc.Element role  : roles) {
					String roleName = role.value("@name");
					w.push("role", new String[] {"name", roleName});
					Collection<XmlDoc.Element> tokenActors = role.elements("identity-token");
					for (XmlDoc.Element tokenActor : tokenActors) {
						String actorName = tokenActor.value("@actor-name");
						if (!listOnly) {
							revokeRoleOnToken(executor, actorName, roleName);
							w.add("actor-name", new String[] {"revoked", "true"}, actorName);
							// Also revoke standard-user but we don't print it out
							revokeRoleOnToken(executor, actorName, standardUserRoleName);
						} else {
							w.add("actor-name", new String[] {"revoked", "false"}, actorName);	
						}
					}
					w.pop();
				}
			}
			w.pop();
		}
	}


	static private void revokeRoleOnToken (ServiceExecutor executor, String token, String role) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", token);
		dm.add("type", "identity");
		dm.add("role", new String[] {"type", "role"}, role);
		executor.execute("actor.revoke", dm.root());
	}
}
