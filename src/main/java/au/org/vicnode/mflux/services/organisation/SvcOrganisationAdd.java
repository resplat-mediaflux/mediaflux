package au.org.vicnode.mflux.services.organisation;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcOrganisationAdd extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "organisation.add";

    private Interface _defn;

    public SvcOrganisationAdd() {
        _defn = new Interface();
        _defn.add(new Interface.Element("name", StringType.DEFAULT, "Name of the organisation", 1, Integer.MAX_VALUE));
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Add research organisation.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        Collection<String> names = args.values("name");

        String dict = Properties.getResearchOrgDict(executor());

        for (String name : names) {
            PluginTask.checkIfThreadTaskAborted();
            addDictionaryEntry(executor(), dict, name);
        }
    }

    static void addDictionaryEntry(ServiceExecutor executor, String dict, String term) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("dictionary", dict);
        dm.add("term", term);
        executor.execute("dictionary.entry.add", dm.root());
    }

}
