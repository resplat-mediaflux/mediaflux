package au.org.vicnode.mflux.services.shareable;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.streams.StreamCopy;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDocWriter;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetUtil;
import au.org.vicnode.mflux.plugin.util.Properties;
import unimelb.utils.FileSizeUtils;

public class Manifest {

    public static final String MANIFEST_DOC_TYPE = "unimelb:shareable-upload-manifest";

    public static final String MANIFEST_FILE_NAME = "__manifest.csv";

    public static final int MAX_KEEP_USER_INTERACTION_USERS = 100;

    public static final String ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED = "shareable.download.consumed";

    public static final String ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED_SUBSET = "shareable.download.consumed.subset";

    public static final String ATTRIBUTE_USER_INTERACTION_COSUMED = "user.interaction.consumed";

    public static final String ATTRIBUTE_DIRECT_DOWNLOAD_ATTEMPT = "direct.download.attempt";

    public static final int NB_TYPES = 10;

    public static class RangeGroup {

        public static final long SIZE_KB = 1000L;
        public static final long SIZE_MB = 1000000L;
        public static final long SIZE_GB = 1000000000L;
        public static final long SIZE_TB = 1000000000000L;

        public final long fromInc;
        public final long toExc;
        public final long fileCount;
        public final long fileSize;

        public RangeGroup(long fromInc, long toExc, long fileCount, long fileSize) {
            this.fromInc = fromInc;
            this.toExc = toExc;
            this.fileCount = fileCount;
            this.fileSize = fileSize;
        }

        public String rangeNameForXml() {
            return String.format("[%s, %s)", FileSizeUtils.getHumanReadableSize(this.fromInc),
                    this.toExc == Long.MAX_VALUE ? "infinity" : FileSizeUtils.getHumanReadableSize(this.toExc));
        }

        public String rangeNameForCSV() {
            return String.format("[%s ~ %s)", FileSizeUtils.getHumanReadableSize(this.fromInc),
                    this.toExc == Long.MAX_VALUE ? "infinity" : FileSizeUtils.getHumanReadableSize(this.toExc));
        }

        public void saveXml(XmlWriter w) throws Throwable {
            w.push("group", new String[] { "range", rangeNameForXml() });
            w.add("file-count", this.fileCount);
            w.add("file-size", new String[] { "h", FileSizeUtils.getHumanReadableSize(this.fileSize) }, this.fileSize);
            w.pop();
        }

        public void saveCSV(PrintStream out) {
            out.printf("file.count %s:,%d,,%n", this.rangeNameForCSV(), this.fileCount);
            out.printf("file.size %s:,%d,,%n", this.rangeNameForCSV(), this.fileSize);
        }

        public static RangeGroup getRangeGroup(ServiceExecutor executor, String ns, long fromInc, long toExc)
                throws Throwable {
            StringBuilder where2 = new StringBuilder();
            if (fromInc >= 0) {
                if (where2.length() > 0) {
                    where2.append(" and ");
                }
                where2.append("csize>=").append(fromInc);
            }
            if (toExc >= 0) {
                if (where2.length() > 0) {
                    where2.append(" and ");
                }
                where2.append("csize<").append(toExc);
            }

            String where1 = ns != null ? String.format("namespace>='%s' and asset has content", ns)
                    : "asset has content";

            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("where", where1);
            dm.add("where", where2.toString());
            dm.add("action", "sum");
            dm.add("xpath", "content/size");
            XmlDoc.Element re = executor.execute("asset.query", dm.root());
            long fileSize = re.longValue("value", 0);
            long fileCount = re.longValue("value/@nbe", 0);
            return new RangeGroup(fromInc, toExc, fileCount, fileSize);
        }

        public static List<RangeGroup> getRangeGroups(ServiceExecutor executor, String ns) throws Throwable {
            List<RangeGroup> groups = new ArrayList<>();
            PluginTask.checkIfThreadTaskAborted();
            groups.add(getRangeGroup(executor, ns, 0, SIZE_KB * 10));
            PluginTask.checkIfThreadTaskAborted();
            groups.add(getRangeGroup(executor, ns, SIZE_KB * 10, SIZE_KB * 100));
            PluginTask.checkIfThreadTaskAborted();
            groups.add(getRangeGroup(executor, ns, SIZE_KB * 100, SIZE_MB));
            PluginTask.checkIfThreadTaskAborted();
            groups.add(getRangeGroup(executor, ns, SIZE_MB, SIZE_MB * 10));
            PluginTask.checkIfThreadTaskAborted();
            groups.add(getRangeGroup(executor, ns, SIZE_MB * 10, SIZE_MB * 100));
            PluginTask.checkIfThreadTaskAborted();
            groups.add(getRangeGroup(executor, ns, SIZE_MB * 100, SIZE_GB));
            PluginTask.checkIfThreadTaskAborted();
            groups.add(getRangeGroup(executor, ns, SIZE_GB, SIZE_GB * 10));
            PluginTask.checkIfThreadTaskAborted();
            groups.add(getRangeGroup(executor, ns, SIZE_GB * 10, SIZE_GB * 100));
            PluginTask.checkIfThreadTaskAborted();
            groups.add(getRangeGroup(executor, ns, SIZE_GB * 100, SIZE_TB));
            PluginTask.checkIfThreadTaskAborted();
            groups.add(getRangeGroup(executor, ns, SIZE_TB, Long.MAX_VALUE));
            return groups;
        }
    }

    public static class TypeGroup {

        public final String type;
        public final long fileCount;
        public final long fileSize;

        public TypeGroup(String type, long fileCount, long fileSize) {
            this.type = type;
            this.fileCount = fileCount;
            this.fileSize = fileSize;
        }

        public void saveXml(XmlWriter w) throws Throwable {
            if (this.fileCount > 0) {
                w.push("group", new String[] { "type", this.type == null ? "null" : this.type });
                w.add("file-count", this.fileCount);
                w.add("file-size", new String[] { "h", FileSizeUtils.getHumanReadableSize(this.fileSize) },
                        this.fileSize);
                w.pop();
            }
        }

        public void saveCSV(PrintStream out) {
            if (this.fileCount > 0) {
                out.printf("file.count (%s):,%d,,%n", this.type == null ? "null" : this.type, this.fileCount);
                out.printf("file.size (%s):,%d,,%n", this.type == null ? "null" : this.type, this.fileSize);
            }
        }

        public static TypeGroup getTypeGroup(ServiceExecutor executor, String ns, String type) throws Throwable {
            StringBuilder sb = new StringBuilder("asset has content");
            if (ns != null) {
                sb.append(" and namespace>='").append(ns).append("'");
            }
            if (type != null) {
                sb.append(" and ctype='").append(type).append("'");
            } else {
                sb.append(" and ctype='content/unknown'");
            }

            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("where", sb.toString());
            dm.add("action", "sum");
            dm.add("xpath", "content/size");
            XmlDoc.Element re = executor.execute("asset.query", dm.root());
            long fileSize = re.longValue("value", 0);
            long fileCount = re.longValue("value/@nbe", 0);
            return new TypeGroup(type, fileCount, fileSize);
        }

        public static List<TypeGroup> getTypeGroups(ServiceExecutor executor, String ns) throws Throwable {
            StringBuilder sb = new StringBuilder("asset has content");
            if (ns != null) {
                sb.append(" and namespace>='").append(ns).append("'");
            }
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("where", sb.toString());
            dm.add("action", "get-distinct-values");
            dm.add("xpath", "content/type");
            dm.add("size", "infinity"); // TODO limit the size
            PluginTask.checkIfThreadTaskAborted();
            XmlDoc.Element re = executor.execute("asset.query", dm.root());
            List<Element> ves = re.elements("value");
            if (ves != null && !ves.isEmpty()) {
                ves.sort((v1, v2) -> {
                    try {
                        long c1 = v1.longValue("@nbe");
                        long c2 = v2.longValue("@nbe");
                        return Long.compare(c2, c1); // desc
                    } catch (Throwable e) {
                        e.printStackTrace();
                        return 0;
                    }
                });
                List<TypeGroup> groups = new ArrayList<>();
                long n = 0;
                for (Element ve : ves) {
                    if (n >= NB_TYPES) {
                        break;
                    }
                    String type = ve.value();
                    if (type != null) {
                        PluginTask.checkIfThreadTaskAborted();
                        groups.add(getTypeGroup(executor, ns, type));
                        ++n;
                    }
                }
                return groups;
            }
            return null;
        }
    }

    public final String namespace;
    public final long totalFileCount;
    public final long totalFileSize;
    public final long minFileSize;
    public final long maxFileSize;
    public final long avgFileSize;
    public final List<TypeGroup> fileTypeStatistics;
    public final List<RangeGroup> fileSizeStatistics;
    public final Set<String> keywords;

    public Manifest(String namespace, Set<String> keywords, long totalFileCount, long totalFileSize, long minFileSize,
            long maxFileSize, long avgFileSize, List<TypeGroup> fileTypeStatistics,
            List<RangeGroup> fileSizeStatistics) {
        this.namespace = namespace;
        this.keywords = keywords;
        this.totalFileCount = totalFileCount;
        this.totalFileSize = totalFileSize;
        this.minFileSize = minFileSize;
        this.maxFileSize = maxFileSize;
        this.avgFileSize = avgFileSize;
        this.fileTypeStatistics = fileTypeStatistics;
        this.fileSizeStatistics = fileSizeStatistics;
    }

    public void saveXml(XmlWriter w) throws Throwable {
        // w.add("namespace", this.namespace);
        w.add("total-file-count", this.totalFileCount);
        w.add("total-file-size", new String[] { "h", FileSizeUtils.getHumanReadableSize(this.totalFileSize) },
                this.totalFileSize);
        w.add("min-file-size", new String[] { "h", FileSizeUtils.getHumanReadableSize(this.minFileSize) },
                this.minFileSize);
        w.add("max-file-size", new String[] { "h", FileSizeUtils.getHumanReadableSize(this.maxFileSize) },
                this.maxFileSize);
        w.add("avg-file-size", new String[] { "h", FileSizeUtils.getHumanReadableSize(this.avgFileSize) },
                this.avgFileSize);
        if (this.fileTypeStatistics != null) {
            w.push("file-type-statistics");
            for (TypeGroup group : this.fileTypeStatistics) {
                group.saveXml(w);
            }
            w.pop();
        }
        if (this.fileSizeStatistics != null) {
            w.push("file-size-statistics");
            for (RangeGroup group : this.fileSizeStatistics) {
                if (group.fileCount > 0) {
                    group.saveXml(w);
                }
            }
            w.pop();
        }
    }

    public void saveCSV(PrintStream csvOut) {
        csvOut.println("SUMMARY,,,");
        csvOut.printf("file.count.total:,%d,,%n", this.totalFileCount);
        csvOut.printf("file.size.total:,%d,,%n", this.totalFileSize);
        csvOut.printf("file.size.min:,%d,,%n", this.minFileSize);
        csvOut.printf("file.size.max:,%d,,%n", this.maxFileSize);
        csvOut.printf("file.size.avg:,%d,,%n", this.avgFileSize);
        if (this.fileTypeStatistics != null) {
            csvOut.println(",,,");
            csvOut.println("FILE TYPE STATISTICS,,,");
            for (TypeGroup group : this.fileTypeStatistics) {
                group.saveCSV(csvOut);
            }
        }
        if (this.fileSizeStatistics != null) {
            csvOut.println(",,,");
            csvOut.println("FILE SIZE STATISTICS,,,");
            for (RangeGroup group : this.fileSizeStatistics) {
                if (group.fileCount > 0) {
                    group.saveCSV(csvOut);
                }
            }
        }
    }

    public void saveXml(XmlWriter w, XmlDoc.Element shareable, String uploadId, String uploadNS, XmlDoc.Element args,
            Date completionTime, Integer shareDuration) throws Throwable {
        String shareableId = shareable.value("@id");
        w.push("shareable", new String[] { "id", shareableId });
        if (shareable.elementExists("name")) {
            w.add("name", shareable.value("name"));
        }
        if (shareable.elementExists("namespace")) {
            w.add("namespace", shareable.value("namespace"));
        }
        if (shareable.elementExists("valid-from")) {
            w.add("valid-from", shareable.value("valid-from"));
        }
        if (shareable.elementExists("valid-to")) {
            w.add("valid-to", shareable.value("valid-to"));
        }
        if (shareable.elementExists("properties/property")) {
            List<XmlDoc.Element> pes = shareable.elements("properties/property");
            for (XmlDoc.Element pe : pes) {
                String name = pe.stringValue("@key", pe.value("@name"));
                String value = pe.value();
                w.add("property", new String[] { "name", name }, value);
            }
        }
        w.pop();

        w.push("upload", new String[] { "id", uploadId, "shareable-id", shareableId });
        if (args != null) {
            List<XmlDoc.Element> es = args.elements();
            if (es != null) {
                for (XmlDoc.Element e : es) {
                    String n = e.name();
                    String v = e.value();
                    w.add("arg", new String[] { "name", n }, v);
                }
            }
        }
        w.add("namespace", uploadNS);
        if (completionTime != null) {
            w.add("completion-time", completionTime);
        }
        if (this.keywords != null) {
            for (String keyword : keywords) {
                w.add("keyword", keyword);
            }
        }
        if (shareDuration != null) {
            w.push("share");
            w.add("duration",
                    new String[] { "expiry", DateTime.string(DateTime.parse("today+" + shareDuration + "day")) },
                    shareDuration);
            w.pop();
        }
        w.pop();
        saveXml(w);
    }

    public void saveXml(XmlDocMaker dm, XmlDoc.Element shareable, String uploadId, String uploadNS, XmlDoc.Element args,
            Date completionTime, Integer shareDuration) throws Throwable {
        XmlDocWriter w = new XmlDocWriter(dm);
        saveXml(w, shareable, uploadId, uploadNS, args, completionTime, shareDuration);
    }

    public void saveCSV(PrintStream csvOut, XmlDoc.Element shareable, String uploadId, String uploadNS,
            XmlDoc.Element args, Date completionTime) throws Throwable {
        csvOut.println(",,,");
        csvOut.println(",,,");
        csvOut.println(",,,");
        saveCSV(csvOut);
        csvOut.println(",,,");
        csvOut.println(",,,");
        csvOut.println(",,,");
        csvOut.println("MEDIAFLUX METADATA,,,");

        String shareableId = shareable.value("@id");
        csvOut.printf("shareable.id:,%s,,%n", shareableId);

        if (shareable.elementExists("name")) {
            csvOut.printf("shareable.name:,%s,,%n", shareable.value("name"));
        }
        if (shareable.elementExists("namespace")) {
            csvOut.printf("shareable.namespace:,%s,,%n", shareable.value("namespace"));
        }
        if (shareable.elementExists("valid-from")) {
            csvOut.printf("shareable.valid.from:,%s,,%n", shareable.value("valid-from"));
        }
        if (shareable.elementExists("valid-to")) {
            csvOut.printf("shareable.valid.to:,%s,,%n", shareable.value("valid-to"));
        }
        if (shareable.elementExists("properties/property")) {
            List<XmlDoc.Element> pes = shareable.elements("properties/property");
            for (XmlDoc.Element pe : pes) {
                String name = pe.stringValue("@key", pe.value("@name"));
                String value = pe.value();
                csvOut.printf("shareable.property(%s):,\"%s\",,%n", name, value.replace(',', ';'));
            }
        }

        csvOut.printf("upload.id:,%s,,%n", uploadId);
        if (args != null) {
            List<XmlDoc.Element> es = args.elements();
            if (es != null) {
                for (XmlDoc.Element e : es) {
                    String n = e.name();
                    String v = e.value();
                    csvOut.printf("upload.arg(%s):,\"%s\",,%n", n, v.replace(',', ';'));
                }
            }
            csvOut.printf("upload.namespace:,%s,,%n", uploadNS);
            if (completionTime != null) {
                csvOut.printf("upload.completion.time:,%s,,%n", completionTime);
            }
        }
        if (this.keywords != null) {
            csvOut.printf("upload.keywords:,\"%s\",,%n", String.join(";", this.keywords));
        }

    }

    public static Manifest getManifest(ServiceExecutor executor, String uploadNS, XmlDoc.Element uploadArgs,
            boolean getTypeStatistics, boolean getSizeStatistics) throws Throwable {
        String ns = uploadNS == null ? "/" : uploadNS;
        String where = "namespace>='" + ns + "' and asset has content and not(namespace='" + ns + "' and name='"
                + Metadata.METADATA_FILE_NAME + "')";

        PluginTask.checkIfThreadTaskAborted();
        SimpleEntry<Long, Long> entry = getTotalFileSize(executor, where);
        long totalFileCount = entry.getKey();
        long totalFileSize = entry.getValue();
        long avgFileSize = totalFileCount == 0 ? 0 : totalFileSize / totalFileCount;

        PluginTask.checkIfThreadTaskAborted();
        long minFileSize = getMinFileSize(executor, where);

        PluginTask.checkIfThreadTaskAborted();
        long maxFileSize = getMaxFileSize(executor, where);

        List<TypeGroup> typeStatistics = getTypeStatistics ? TypeGroup.getTypeGroups(executor, uploadNS) : null;

        List<RangeGroup> sizeStatistics = getSizeStatistics ? RangeGroup.getRangeGroups(executor, uploadNS) : null;

        return new Manifest(uploadNS, parseKeywords(uploadArgs), totalFileCount, totalFileSize, minFileSize,
                maxFileSize, avgFileSize, typeStatistics, sizeStatistics);
    }

    static Set<String> parseKeywords(XmlDoc.Element uploadArgs) throws Throwable {
        if (uploadArgs != null && uploadArgs.elementExists(Shareable.ARG_KEYWORDS)) {
            Collection<String> keywords = uploadArgs.values(Shareable.ARG_KEYWORDS);
            if (keywords.size() > 1) {
                return new LinkedHashSet<String>(keywords);
            } else {
                String kwv = keywords.iterator().next();
                kwv = kwv.trim();
                String[] kws = kwv.split("\\s*,\\s*");
                if (kws.length > 0) {
                    Set<String> rkws = new LinkedHashSet<>();
                    for (String kw : kws) {
                        if (kw != null && !kw.trim().isEmpty()) {
                            rkws.add(kw.trim());
                        }
                    }
                    return rkws.isEmpty() ? null : rkws;
                }
            }
        }
        return null;
    }

    static SimpleEntry<Long, Long> getTotalFileSize(ServiceExecutor executor, String where) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");

        dm.add("where", where);
        dm.add("action", "sum");
        dm.add("xpath", "content/size");
        XmlDoc.Element re = executor.execute("asset.query", dm.root());

        long totalFileCount = re.longValue("value/@nbe", 0);
        long totalFileSize = re.longValue("value", 0);
        return new SimpleEntry<>(totalFileCount, totalFileSize);
    }

    static long getMinFileSize(ServiceExecutor executor, String where) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", where);
        dm.add("action", "min");
        dm.add("xpath", "content/size");
        XmlDoc.Element re = executor.execute("asset.query", dm.root());
        return re.longValue("value", 0);
    }

    static long getMaxFileSize(ServiceExecutor executor, String where) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", where);
        dm.add("action", "max");
        dm.add("xpath", "content/size");
        XmlDoc.Element re = executor.execute("asset.query", dm.root());
        return re.longValue("value", 0);
    }

    private static void saveAssetQueryResultCSV(ServiceExecutor executor, String uploadNS, OutputStream csvOut)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", String.format("namespace>='%s'", uploadNS));
        dm.add("action", "get-value");
        dm.add("size", "infinity");
        dm.add("output-format", "csv");
        dm.add("xpath", new String[] { "ename", "ASSET_PATH" }, "path");
        dm.add("xpath", new String[] { "ename", "ASSET_ID" }, "id");
        dm.add("xpath", new String[] { "ename", "CONTENT_LENGTH" }, "content/size");
        dm.add("xpath", new String[] { "ename", "CRC32_CHECKSUM" }, "content/csum");
        PluginService.Outputs outputs = new PluginService.Outputs(1);
        PluginTask.checkIfThreadTaskAborted();
        executor.execute("asset.query", dm.root(), null, outputs);
        PluginService.Output output = outputs.output(0);
        try {
            try (InputStream in = output.stream()) {
                StreamCopy.copy(in, csvOut);
            }
        } finally {
            output.close();
            outputs.close();
        }
    }

    static String createManifestAsset(ServiceExecutor executor, XmlDoc.Element shareable, String uploadId,
            String uploadNS, XmlDoc.Element uploadArgs, Date completionTime, boolean getFileTypeStatistics,
            boolean getFileSizeStatistics, Integer shareDuration, Shareable downloadShareable,
            Interaction userInteraction, DownloadLink downloadLink) throws Throwable {
        String downloadShareableId = downloadShareable == null ? null : downloadShareable.id;
        String userInteractionId = userInteraction == null ? null : userInteraction.id;
        return createManifestAsset(executor, shareable, uploadId, uploadNS, uploadArgs, completionTime,
                getFileTypeStatistics, getFileSizeStatistics, shareDuration, downloadShareableId, userInteractionId,
                downloadLink == null ? null : downloadLink.tokenId,
                downloadLink != null && downloadLink.password != null);
    }

    static String createManifestAsset(ServiceExecutor executor, XmlDoc.Element shareable, String uploadId,
            String uploadNS, XmlDoc.Element uploadArgs, Date completionTime, boolean getFileTypeStatistics,
            boolean getFileSizeStatistics, Integer shareDuration, String downloadShareableId, String userInteractionId,
            String directDownloadTokenId, boolean directDownloadHasPassword) throws Throwable {

        Manifest manifest = Manifest.getManifest(executor, uploadNS, uploadArgs, getFileTypeStatistics,
                getFileSizeStatistics);

        File manifestFile = PluginTask.createTemporaryFile(MANIFEST_FILE_NAME);

        try (PrintStream csvOut = new PrintStream(new BufferedOutputStream(new FileOutputStream(manifestFile)))) {
            saveAssetQueryResultCSV(executor, uploadNS, csvOut);
            manifest.saveCSV(csvOut, shareable, uploadId, uploadNS, uploadArgs, completionTime);
        }

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("create", true);
        String path = String.format("%s/%s", uploadNS, MANIFEST_FILE_NAME);
        dm.add("id", String.format("path=%s", path));
        dm.push("meta", new String[] { "action", "replace" });
        dm.push(MANIFEST_DOC_TYPE);
        manifest.saveXml(dm, shareable, uploadId, uploadNS, uploadArgs, completionTime, shareDuration);
        if (downloadShareableId != null) {
            dm.push("download-shareable");
            dm.add("id", downloadShareableId);
            dm.pop();
        }
        if (userInteractionId != null) {
            dm.push("user-interaction");
            dm.add("id", userInteractionId);
            dm.pop();
        }
        if (directDownloadTokenId != null) {
            dm.push("direct-download", new String[] { "has-password", Boolean.toString(directDownloadHasPassword) });
            dm.add("token-id", directDownloadTokenId);
            dm.pop();
        }
        dm.pop();
        dm.pop();
        PluginService.Input in = new PluginService.Input(PluginTask.deleteOnCloseInputStream(manifestFile),
                manifestFile.length(), "text/csv", null);
        PluginTask.checkIfThreadTaskAborted();
        executor.execute("asset.set", dm.root(), new PluginService.Inputs(in), null);

        // create global manifest asset
        createGlobalManifest(executor, shareable.value("@id"), uploadId,
                dm.root().element("meta/" + MANIFEST_DOC_TYPE));

        return path;
    }

    private static String createGlobalManifest(ServiceExecutor executor, String shareableId, String uploadId,
            XmlDoc.Element manifestDoc) throws Throwable {
        String assetNS = Properties.getInstrumentUploadManifestAssetNameSpace(executor);
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("create", true);
        String path = String.format("%s/%s.%s.manifest", assetNS, shareableId, uploadId);
        dm.add("id", String.format("path=%s", path));
        dm.push("meta", new String[] { "action", "replace" });
        dm.add(manifestDoc);
        dm.pop();
        PluginTask.checkIfThreadTaskAborted();
        executor.execute("asset.set", dm.root());
        return path;
    }

    public static boolean manifestAssetExists(ServiceExecutor executor, String uploadNS) throws Throwable {
        return AssetUtil.assetExists(executor, String.format("path=%s/%s", uploadNS, MANIFEST_FILE_NAME));
    }

    public static Map<String, Integer> getManifestAssetAttributes(ServiceExecutor executor, String assetId)
            throws Throwable {
        Map<String, Integer> r = new LinkedHashMap<String, Integer>();
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", assetId);
        List<XmlDoc.Element> aes = executor.execute("asset.attribute.describe", dm.root()).elements("attribute");
        if (aes != null) {
            for (XmlDoc.Element ae : aes) {
                String attrName = ae.value("@name");
                Integer value = ae.intOrNullValue("value");
                if (value != null) {
                    r.put(attrName, value);
                }
            }
        }
        return r;
    }

    static XmlDoc.Element findManifestAsset(ServiceExecutor executor, String shareableId, String uploadId,
            String action) throws Throwable {

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", String.format("xpath(%s/shareable/@id)=%s and xpath(%s/upload/@id)=%s and name='%s'",
                MANIFEST_DOC_TYPE, shareableId, MANIFEST_DOC_TYPE, uploadId, MANIFEST_FILE_NAME));
        dm.add("where", "asset has content");
        dm.push("sort");
        dm.add("key", "id");
        dm.add("order", "asc");
        dm.pop();
        dm.add("action", action);
        return executor.execute("asset.query", dm.root());
    }

    static String getManifestAssetId(ServiceExecutor executor, XmlDoc.Element shareable, String uploadId,
            boolean checkMoved) throws Throwable {
        String assetPath = getManifestAssetPath(executor, shareable, uploadId, checkMoved);
        if (assetPath != null) {
            String assetId = "path=" + assetPath;
            return assetId;
        } else {
            return null;
        }
    }

    static String getManifestAssetId(ServiceExecutor executor, String shareableId, String uploadId, boolean checkMoved)
            throws Throwable {
        return getManifestAssetId(executor, Shareable.describe(executor, shareableId), uploadId, checkMoved);
    }

    private static String getManifestAssetPath(ServiceExecutor executor, XmlDoc.Element shareable, String uploadId,
            boolean checkMoved) throws Throwable {
        String uploadNS = UploadShareable.getUploadNamespace(executor, shareable, uploadId, checkMoved);
        if (uploadNS != null) {
            String assetPath = String.format("%s/%s", uploadNS, Manifest.MANIFEST_FILE_NAME);
            return assetPath;
        } else {
            return null;
        }
    }

    static String getManifestAssetPath(String uploadNS) {
        return String.format("%s/%s", uploadNS, Manifest.MANIFEST_FILE_NAME);
    }

}
