package au.org.vicnode.mflux.services.dataregistry;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import au.org.vicnode.mflux.plugin.util.Properties;
import arc.xml.XmlWriter;
import unimelb.rcs.data.registry.DataRegistry;

public class SvcDataRegistryCollectionDescribe extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "data.registry.collection.describe";

	private Interface _defn;

	public SvcDataRegistryCollectionDescribe() {
		_defn = new Interface();
		_defn.add(new Interface.Element("code", StringType.DEFAULT, "Collection code.", 1, Integer.MAX_VALUE));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Get the details of a data registry collection.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		Collection<String> codes = args.values("code");
		DataRegistry reg = DataRegistry.get(executor());
		int count = 0;
		for (String code : codes) {
			PluginTask.checkIfThreadTaskAborted();
			unimelb.rcs.data.registry.Collection collection = describeCollection(executor(), reg, code);
			if (collection != null) {
				collection.saveXml(w);
			} else {
				w.add("not-found", new String[] { "code", code });
			}
			count++;
		}
		w.add("count", count);
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {
		return true;
	}

	static unimelb.rcs.data.registry.Collection describeCollection(ServiceExecutor executor, DataRegistry registry,
			String code) throws Throwable {
		return registry.getCollection(code);
	}

}
