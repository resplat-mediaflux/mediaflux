/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.contentcopy;

import java.util.Collection;
import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectContentCopy;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.services.SvcProjectDescribe;

/**
 * 
 * @author nebk
 *
 */
public class SvcProjectContentCopyList extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.content-copy.list";

	private Interface _defn;

	public SvcProjectContentCopyList() throws Throwable {
		_defn = new Interface();
		//
		SvcProjectDescribe.addProdType (_defn);
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns the list of projects that are making content copies into the aggregated store.";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String projType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);

		// List of projects
		Collection<String> projectIDs = Project.projectIDs(executor(), true, true);
		if (projectIDs.size() == 0)
			return;
		int n = 0;
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			if (Project.keepOnProjectType(ph, projType)) {
				String ccQueue = ProjectContentCopy.isProjectContentCopied(ph);
				if (ccQueue!=null) {
					w.add("project", 
							new String[] { "parent", ph.parentNameSpace(), 
									"type", projType,  
									"content-copy-queue", ccQueue,"collection", ph.collectionKey() }, ph.id());
					n++;
				}
			}
		}
		w.add("n-projects", new String[] { "total-number", "" + projectIDs.size() }, n);
	}
}
