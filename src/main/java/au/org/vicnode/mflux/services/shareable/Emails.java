package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

public class Emails {

    public static final String VALUE_SEPARATORS = ";,";

    public static Collection<String> parse(Collection<String> values) {
        Set<String> emails = new LinkedHashSet<>();
        if(values!=null){
           for(String v : values){
               Collection<String> es = parse(v);
               if(es!=null){
                   emails.addAll(es);
               }
           }
        }
        if(emails.isEmpty()){
            return null;
        }
        return emails;
    }

    public static Collection<String> parse(String value) {
        if (value == null || value.trim().isEmpty()) {
            return null;
        }
        String[] parts = value.trim().replaceAll("^[" + VALUE_SEPARATORS + "]+", "")
                .replaceAll("[" + VALUE_SEPARATORS + "]+$", "").split(" *[" + VALUE_SEPARATORS + "]+ *");
        if (parts.length == 0) {
            return null;
        }
        Set<String> emails = new LinkedHashSet<String>();
        for (String email : parts) {
            if (!email.isEmpty()) {
                emails.add(email.toLowerCase());
            }
        }
        if (emails.isEmpty()) {
            return null;
        }
        return emails;

    }

    public static String join(Collection<String> emails) {
        if(emails==null||emails.isEmpty()){
            return null;
        }
        boolean first = true;
        StringBuilder sb = new StringBuilder();
        for (String email : emails) {
            if (first) {
                first = false;
            } else {
                sb.append("; ");
            }
            sb.append(email);
        }
        return sb.toString();
    }

}
