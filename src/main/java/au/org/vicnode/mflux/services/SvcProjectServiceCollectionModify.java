/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;

import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectServiceCollectionModify extends PluginService {

	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectServiceCollectionModify() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
		_defn.add(new Interface.Element("name", StringType.DEFAULT,
				"The name of the service collection (see system.service.collection.list)", 1, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("action", 
				new EnumType(new String[] {"add", "remove"}),
				"The action is one of 'add' or 'remove'. Adding a pre-existing service collection is the same as replacing it.", 1, 1));
	}




	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Adds or removes the given service collection to/from the parent namespace of the selected projects. If you add, and it pre-exists, it will replace and update.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.service.collection.modify";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		Collection<String> names = args.values("name");
		String action = args.stringValue("action");

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID != null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else {
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size() == 0) {
			return;
		}

		// Iterate  over projects and summarise
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
			String namespace = ph.nameSpace();

			// You can just re-add and it will replace and update
			for (String name : names) {
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("namespace", namespace);   
				if (action.equals("add")) {
					dm.add("collection", new String[] {"inheritance", "inherited"},  name);
					executor().execute("asset.namespace.service.collection.add",dm.root());
				} else {
					dm.add("collection",  name);
					executor().execute("asset.namespace.service.collection.remove",dm.root());
				}
			}
			
			// List
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("namespace", namespace); 
			XmlDoc.Element r = executor().execute("asset.namespace.service.collection.list",dm.root());
			w.push("project");
			w.add("project-id", ph.id());
			w.addAll(r.elements());
			w.pop();
		}
	}
}
