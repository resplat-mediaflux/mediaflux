package au.org.vicnode.mflux.services;

import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectQuotaSet extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.quota.set";

    private Interface _defn;
    private static Boolean allowPartialCID = true;

    public SvcProjectQuotaSet() throws Throwable {
        _defn = new Interface();

        Interface.Element quota = new Interface.Element("quota", XmlDocType.DEFAULT,
                "A space quota to add to the namespace. The quota applies to all content in the namespace (irrespective of backing store) all descendent namespaces.",
                1, 1);
        quota.add(new Interface.Element("on-overflow", new EnumType(new String[] { "allow", "fail" }),
                "What should occur if use exceeds the quota? Defaults to allow", 0, 1));
        quota.add(new Interface.Element("allocation", StringType.DEFAULT,
                "The quota in bytes (may be specified with units KB, MB, GB, TB or EB).", 1, 1));
        _defn.add(quota);

        _defn.add(new Interface.Element("replace", BooleanType.DEFAULT, "Replace an existing quota. Defaults to true.",
                0, 1));
        SvcProjectDescribe.addMandatoryProjectID(_defn, allowPartialCID);
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {

        return _defn;
    }

    @Override
    public String description() {
        return "Set project quota.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String projectID = args.value("project-id");
        String t[] = Project.validateProject(executor(), projectID, allowPartialCID);
        projectID = t[1];

        ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
        String namespace = ph.nameSpace();
        boolean replace = args.booleanValue("replace", true);
        String allocation = args.value("quota/allocation");
        String onOverflow = args.stringValue("quota/on-overflow", "allow");

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", namespace);
        dm.add("replace", replace);
        dm.push("quota");
        dm.add("allocation", allocation);
        dm.add("on-overflow", onOverflow);
        dm.pop();

        executor().execute("asset.namespace.quota.set", dm.root());

        XmlDoc.Element qe = describeQuota(executor(), namespace);
        if (qe != null) {
            w.add(qe);
        }
    }

    static XmlDoc.Element describeQuota(ServiceExecutor executor, String namespace) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", namespace);
        return executor.execute("asset.namespace.describe", dm.root()).element("namespace/quota");
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
