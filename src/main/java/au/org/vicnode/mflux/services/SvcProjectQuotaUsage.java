/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;
import au.org.vicnode.mflux.plugin.util.Util.StorageUnit;




public class SvcProjectQuotaUsage extends PluginService {

	private Interface _defn;

	public SvcProjectQuotaUsage()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("unit", new EnumType(Util.StorageUnit.values()),
				"The storage unit you wish the result returned in. Default is GB.", 0, 1));
		_defn.add(new Interface.Element("store", StringType.DEFAULT, "If supplied, limit the summaries to only projects that use this store (defaults to all stores).",	 0, 1));
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns all projects and sums of all of the sizes of assets under each the respective project namespace along with that projects allocated quota.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.quota.usage";
	}
	
	public boolean canBeAborted() {

		return true;
	}


	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String store = args.value("store");

		// Sets the unit type from the argument, defaults to GB
		Util.StorageUnit storageUnit = Util.StorageUnit.fromString(args.value("unit"), StorageUnit.GB);
		projectUsages (storageUnit, store, w);
	}

	// Sum function to go through all projects and sum their quotas.
	private void projectUsages (Util.StorageUnit storageUnit, String store, XmlWriter w) throws Throwable {
		ServiceExecutor executor = executor();

		// FInd all the projects and add to the list
		Collection<String> projectIDs = Project.projectIDs(executor, true, true);

		// Iterate over projects
		if (projectIDs.size()==0) return;

		Vector<String> inheritedProjectIDs = new Vector<String>();   // Project IDs with inherited quota
		Vector<String> noneProjectIDs = new Vector<String>();        // Projects IDs with no quota
		//
		Double totalAllocated = 0.0;
		// Total actually used
		Double totalDirectUsed = 0.0;           // Project has its own quota
		Double totalInheritedUsed = 0.0;        // Project inherits its quota (but we don't know from where exactly)
		Double totalNoneUsed = 0.0;             // Project has no quota
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);

			// Build the project namespace and retrieve the Collection level metadata key and quota allocation, this should hold a unique identifier
			// tying the collection back to the VicNode reporting IDs.
			String nameSpace = ph.nameSpace();	
			XmlDoc.Element nsMeta = ph.metaData();

			if (nsMeta!=null) {
				String projectStore = nsMeta.value("namespace/store");
				if (store==null || (store!=null && projectStore.equals(store))) {
					String t = Properties.getCollectionDocType(executor);
					String projectName = nsMeta.value("namespace/asset-meta/"+t+"/key");
					if (projectName == null) {
						projectName = "null";
					}

					// Build the arguments for the asset.query service which will sum the assets under the project namespace, based on the below command
					//> asset.query :where namespace>='/projects/' :action sum :xpath content/size
					String projectNameSpaceQuery = "namespace>='"+nameSpace+"'";		
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add("where", projectNameSpaceQuery);
					dm.add("action","sum");
					dm.add("xpath","content/size");
					dm.add("size", "infinity");

					// Run the asset.query service. This service always returns a non-null result
					// but the value(the sum) may be missing if there are no assets  with content
					XmlDoc.Element projectUsageDetails = executor.execute("asset.query", dm.root());
					
					// Extract the summed asset size in bytes
					// Need to figure out what to do with inherited quota like cryo-em
					Long projectUsage = projectUsageDetails.longValue("value",0);

					// Build the response xml
					w.push("project");
					w.add("id", ph.id());
					w.add("namespace", nameSpace);
					if (!projectName.isEmpty()) {
						w.add("key_name", projectName);
					}
					w.add(storageUnit.convert(projectUsage,"Usage", null));

					// In case the project has no quota set
					String projectQuota = nsMeta.value("namespace/quota/allocation");
					if (projectQuota == null){
						projectQuota = nsMeta.value("namespace/quota/inherited/allocation");
						if (projectQuota==null) {
							w.add("Quota", "none");
							totalNoneUsed += projectUsage.doubleValue();
							noneProjectIDs.add(ph.id());
						} else {
							w.add(storageUnit.convert(Long.parseLong(projectQuota), "Quota", "inherited"));	
							totalInheritedUsed += projectUsage.doubleValue();
							inheritedProjectIDs.add(ph.id());
						}
					} else{
						totalDirectUsed += projectUsage.doubleValue();
						//
						totalAllocated += Double.parseDouble(projectQuota);
						w.add(storageUnit.convert(Long.parseLong(projectQuota), "Quota", "direct"));	
					}
					w.add("number-assets", projectUsageDetails.longValue("value/@nbe"));
					w.pop();
				}
			}
		}
		w.push("direct-quotas");
		w.add("total-allocated", new String[]{"unit", "bytes"}, totalAllocated);
		w.add("total-used", new String[]{"unit", "bytes"}, totalDirectUsed);
		w.pop();

		// We'd like to know the total used against the inherited quota but difficult to do
		// as we don't know where our quota came from.  We'd have to work down and traverse
		// the tree of inherited projects
		w.push("inherited-quotas");
		w.add("total-used", new String[]{"unit", "bytes"}, totalInheritedUsed);
		for (String inheritedProjectID : inheritedProjectIDs) {
			w.add("project-id", inheritedProjectID);
		}
		w.pop();
		//
		w.push("none-quotas");
		w.add("total-used", new String[]{"unit", "bytes"}, totalNoneUsed);
		for (String noneProjectID : noneProjectIDs) {
			w.add("project-id", noneProjectID);
		}
		w.pop();
	}

}
