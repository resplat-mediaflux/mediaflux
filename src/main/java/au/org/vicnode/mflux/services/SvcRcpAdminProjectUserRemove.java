/**
 * @author Raj
 *
 * Copyright (c) 2025, The University of Melbourne, Australia
 *
 * All rights reserved.a
 */
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

import java.util.Collection;

import static au.org.vicnode.mflux.services.SvcRcpAdminProjectDescribe.checkUserIsProjectAdminRcp;

public class SvcRcpAdminProjectUserRemove extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "rcpadmin.project.user.remove";

    private Interface _defn;

    public SvcRcpAdminProjectUserRemove() throws Throwable {
        _defn = new Interface();
        SvcProjectUserRemove.addProjectRemoveArgs(_defn,false);
    }

    public Access access() {
        return ACCESS_MODIFY;
    }

    public Interface definition() {
        return _defn;
    }

    public String description() {
        return "Remove project user for projects that the calling user is an rcsadmin of";
    }

    public String name() {
        return SERVICE_NAME;
    }



    public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
        String contains = null;
        String projectId = args.value("project-id");

        SvcProjectUserRemove remove = new SvcProjectUserRemove();
        remove.setServiceExecutor(executor());

        Collection<String> projectIds = checkUserIsProjectAdminRcp(contains,projectId,executor());
        if(projectIds.contains(projectId)){
            remove.execute(args,inputs,outputs,w);
        }
    }
}
