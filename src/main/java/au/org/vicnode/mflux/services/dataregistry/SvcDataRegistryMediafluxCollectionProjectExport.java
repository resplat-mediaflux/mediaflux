package au.org.vicnode.mflux.services.dataregistry;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONWriter;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EnumType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

/**
 * TODO remove after RCP migration
 * 
 * Note: this is a temporary service for RCP migration. It should be removed
 * after the migration.
 */
public class SvcDataRegistryMediafluxCollectionProjectExport extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "data.registry.collection.project.export";

	private static final String ATTICA_SERVER_UUID = "1379";
	private static final String MEDIAFLUX_SERVER_UUID = "1128";
	private static final String MEDIAFLUX_PRODUCT = "mediaflux";
	private static final String ATTICA_PRODUCT = "attica";
	private static final String VCA_NAMESPACE = "/avarchives/VCA-FTV";

	private final Interface _defn;

	public SvcDataRegistryMediafluxCollectionProjectExport() {
		_defn = new Interface();
		_defn.add(new Interface.Element("format", new EnumType(new String[] { "json", "csv" }),
				"Output file format. Defaults to json.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Exports projects into json or csv file for RCP migration.";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}

	@Override
	public int minNumberOfOutputs() {
		return 1;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs outputs, XmlWriter w) throws Throwable {
		String format = args.stringValue("format", "json");
		String mimeType = "csv".equalsIgnoreCase(format) ? "text/csv" : "application/json";
		PluginService.Output output = outputs.output(0);
		String product;
		String serverUUID = executor().execute("server.uuid").value("uuid");
		if (MEDIAFLUX_SERVER_UUID.equals(serverUUID)) {
			product = MEDIAFLUX_PRODUCT;
		} else if (ATTICA_SERVER_UUID.equals(serverUUID)) {
			product = ATTICA_PRODUCT;
		} else {
			throw new Exception("Cannot run this service on this server: UUID: " + serverUUID);
		}
		File of = PluginTask.createTemporaryFile("projects." + format);
		try {
			if ("csv".equalsIgnoreCase(format)) {
				exportProjectsToCSV(executor(), product, of);
			} else {
				exportProjectsToJSON(executor(), product, of);
			}
			output.setData(PluginTask.deleteOnCloseInputStream(of), of.length(), mimeType);
		} catch (Throwable t) {
			try {
				PluginTask.deleteTemporaryFile(of);
				PluginLog.log().add(PluginLog.WARNING, "deleting temporary file: " + of.getAbsolutePath());
			} catch (Throwable e) {
				e.printStackTrace();
			}
			throw t;
		}
	}

	private static void exportProjectsToCSV(ServiceExecutor executor, String product, File outputFile)
			throws Throwable {
		try (FileOutputStream fos = new FileOutputStream(outputFile);
				BufferedOutputStream bos = new BufferedOutputStream(fos);
				PrintStream ps = new PrintStream(bos)) {
			ps.println("project_id,collection_code,project_path,quota,created,quota_requested,status,product_name");

			PluginTask.checkIfThreadTaskAborted();
			List<XmlDoc.Element> pes = executor
					.execute("vicnode.project.list", "<args><prod-type>production</prod-type></args>", null, null)
					.elements("project");
			if (pes != null) {
				for (XmlDoc.Element pe : pes) {
					PluginTask.checkIfThreadTaskAborted();
					exportProjectDetailsToCSV(executor, product, pe, ps);
				}
			}

			PluginTask.checkIfThreadTaskAborted();
			pes = executor
					.execute("vicnode.project.list", "<args><prod-type>closed-stub</prod-type></args>", null, null)
					.elements("project");
			if (pes != null) {
				for (XmlDoc.Element pe : pes) {
					PluginTask.checkIfThreadTaskAborted();
					exportProjectDetailsToCSV(executor, product, pe, ps);
				}
			}

			// include two test projects with collection code
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element pe = executor.execute("vicnode.project.list",
					"<args><prod-type>test</prod-type><contains>proj-9870_service_rcs_dst_genericdatatransfer-1128.4.358</contains></args>",
					null, null).element("project");
			if (pe != null) {
				PluginTask.checkIfThreadTaskAborted();
				exportProjectDetailsToCSV(executor, product, pe, ps);
			}

			PluginTask.checkIfThreadTaskAborted();
			pe = executor.execute("vicnode.project.list",
					"<args><prod-type>test</prod-type><contains>proj-migration_shared_test-1128.4.544</contains></args>",
					null, null).element("project");
			if (pe != null) {
				PluginTask.checkIfThreadTaskAborted();
				exportProjectDetailsToCSV(executor, product, pe, ps);
			}

			// export VCA project
			if (MEDIAFLUX_PRODUCT.equalsIgnoreCase(product)) {
				PluginTask.checkIfThreadTaskAborted();
				exportNamespaceDetailsToCSV(executor, VCA_NAMESPACE, ps);
			}
		}
	}

	private static void exportProjectDetailsToCSV(ServiceExecutor executor, String product, XmlDoc.Element pe,
			PrintStream ps) throws Throwable {
		String projectId = pe.value();
		String collectionCode = pe.value("@collection");
		String projectPath = pe.value("@path");
		String projectType = pe.value("@type");
		if (collectionCode != null) {
			String status = "production".equalsIgnoreCase(projectType) ? "provisioned" : "decommissioned";
			XmlDoc.Element ne = executor.execute("asset.namespace.describe",
					"<args><namespace>" + projectPath + "</namespace></args>", null, null).element("namespace");
			long quotaBytes = ne.longValue("quota/allocation", 0);
			long quotaGB = Math.round(quotaBytes / 1000000000L);
			Date ctime = ne.dateValue("ctime");
			String created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS'000'X").format(ctime);
			ps.printf("%s,%s,%s,%d,\"%s\",%d,%s,%s%n", projectId, collectionCode, projectId, quotaGB, created, quotaGB,
					status, product);
		}
	}

	private static void exportNamespaceDetailsToCSV(ServiceExecutor executor, String namespace, PrintStream ps)
			throws Throwable {
		XmlDoc.Element ne = executor.execute("asset.namespace.describe",
				"<args><namespace>" + namespace + "</namespace></args>", null, null).element("namespace");
		String collectionCode = ne.value("asset-meta/ds:Collection/key");
		if (collectionCode == null) {
			throw new Exception("Could not find asset-meta/ds:Collection/key from namespace: " + namespace);
		}
		if (collectionCode.toLowerCase().startsWith("vicnode:")) {
			collectionCode = collectionCode.substring(8);
		}
		long quotaBytes = ne.longValue("quota/allocation", 0);
		long quotaGB = Math.round(quotaBytes / 1000000000L);
		Date ctime = ne.dateValue("ctime");
		String created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS'000'X").format(ctime);
		ps.printf("%s,%s,%s,%d,\"%s\",%d,%s,%s%n", collectionCode, collectionCode, collectionCode, quotaGB, created,
				quotaGB, "provisioned", MEDIAFLUX_PRODUCT);
	}

	private static void exportProjectsToJSON(ServiceExecutor executor, String product, File outputFile)
			throws Throwable {
		try (FileOutputStream fos = new FileOutputStream(outputFile);
				BufferedOutputStream bos = new BufferedOutputStream(fos);
				PrintStream ps = new PrintStream(bos)) {
			JSONWriter jw = new JSONWriter(ps);
			jw.array();
			// production projects
			PluginTask.checkIfThreadTaskAborted();
			List<XmlDoc.Element> pes = executor
					.execute("vicnode.project.list", "<args><prod-type>production</prod-type></args>", null, null)
					.elements("project");
			if (pes != null) {
				for (XmlDoc.Element pe : pes) {
					PluginTask.checkIfThreadTaskAborted();
					exportProjectDetailsToJSON(executor, product, pe, jw);
				}
			}

			// closed-stub projects
			PluginTask.checkIfThreadTaskAborted();
			pes = executor
					.execute("vicnode.project.list", "<args><prod-type>closed-stub</prod-type></args>", null, null)
					.elements("project");
			if (pes != null) {
				for (XmlDoc.Element pe : pes) {
					PluginTask.checkIfThreadTaskAborted();
					exportProjectDetailsToJSON(executor, product, pe, jw);
				}
			}

			// include two test projects with collection code
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element pe = executor.execute("vicnode.project.list",
					"<args><prod-type>test</prod-type><contains>proj-9870_service_rcs_dst_genericdatatransfer-1128.4.358</contains></args>",
					null, null).element("project");
			if (pe != null) {
				PluginTask.checkIfThreadTaskAborted();
				exportProjectDetailsToJSON(executor, product, pe, jw);
			}

			PluginTask.checkIfThreadTaskAborted();
			pe = executor.execute("vicnode.project.list",
					"<args><prod-type>test</prod-type><contains>proj-migration_shared_test-1128.4.544</contains></args>",
					null, null).element("project");
			if (pe != null) {
				PluginTask.checkIfThreadTaskAborted();
				exportProjectDetailsToJSON(executor, product, pe, jw);
			}

			// export VCA project
			if (MEDIAFLUX_PRODUCT.equalsIgnoreCase(product)) {
				PluginTask.checkIfThreadTaskAborted();
				exportNamespaceDetailsToJSON(executor, VCA_NAMESPACE, jw);
			}

			jw.endArray();
		}
	}

	private static void exportProjectDetailsToJSON(ServiceExecutor executor, String product, XmlDoc.Element pe,
			JSONWriter jw) throws Throwable {
		String projectId = pe.value();
		String collectionCode = pe.value("@collection");
		String projectPath = pe.value("@path");
		String projectType = pe.value("@type");
		if (collectionCode != null) {
			String status = "production".equalsIgnoreCase(projectType) ? "provisioned" : "decommissioned";
			XmlDoc.Element ne = executor.execute("asset.namespace.describe",
					"<args><namespace>" + projectPath + "</namespace></args>", null, null).element("namespace");
			long quotaBytes = ne.longValue("quota/allocation", 0);
			long quotaGB = Math.round(quotaBytes / 1000000000L);
			Date ctime = ne.dateValue("ctime");
			String created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS'000'X").format(ctime);

			jw.object();
			jw.key("project_id").value(projectId);
			jw.key("collection_code").value(collectionCode);
			jw.key("project_path").value(projectId);
			jw.key("quota").value(quotaGB);
			jw.key("created").value(created);
			jw.key("quota_requested").value(quotaGB);
			jw.key("status").value(status);
			jw.key("product_name").value(product);
			jw.endObject();
		}
	}

	private static void exportNamespaceDetailsToJSON(ServiceExecutor executor, String namespace, JSONWriter jw)
			throws Throwable {
		XmlDoc.Element ne = executor.execute("asset.namespace.describe",
				"<args><namespace>" + namespace + "</namespace></args>", null, null).element("namespace");
		String collectionCode = ne.value("asset-meta/ds:Collection/key");
		if (collectionCode == null) {
			throw new Exception("Could not find asset-meta/ds:Collection/key from namespace: " + namespace);
		}
		if (collectionCode.toLowerCase().startsWith("vicnode:")) {
			collectionCode = collectionCode.substring(8);
		}
		long quotaBytes = ne.longValue("quota/allocation", 0);
		long quotaGB = Math.round(quotaBytes / 1000000000L);
		Date ctime = ne.dateValue("ctime");
		String created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS'000'X").format(ctime);
		jw.object();
		jw.key("project_id").value(collectionCode);
		jw.key("collection_code").value(collectionCode);
		jw.key("project_path").value(collectionCode);
		jw.key("quota").value(quotaGB);
		jw.key("created").value(created);
		jw.key("quota_requested").value(quotaGB);
		jw.key("status").value("provisioned");
		jw.key("product_name").value(MEDIAFLUX_PRODUCT);
		jw.endObject();
	}
}
