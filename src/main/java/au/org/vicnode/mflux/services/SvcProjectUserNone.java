/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectUserNone extends PluginService {


	private Interface _defn;

	public SvcProjectUserNone()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProdType(_defn);
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "List projects that have no users.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.user.none";
	}

	public boolean canBeAborted() {
		return true;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String projType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);

		Collection<String> projectIDs = Project.projectIDs(executor(), true, true);
		if (projectIDs.size() == 0) {
			return;
		}
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);

			if (Project.keepOnProjectType(ph, projType)) {
				XmlDoc.Element users = Project.listUsers(executor(), ph.id(), false, false);		
				if (!users.hasSubElements()) {
					w.add("project-id", ph.id());
				}
			}
		}
	}
}
