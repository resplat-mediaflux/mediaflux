package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcFacilityShareableUploadInteractionValidate extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT
            + "facility.shareable.upload.interaction.validate";

    private final Interface _defn;

    public SvcFacilityShareableUploadInteractionValidate() {
        _defn = new Interface();

        _defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "Shareable id.", 0, 1));
        _defn.add(new Interface.Element("upload-id", LongType.POSITIVE, "Upload id.", 0, 1));
        _defn.add(new Interface.Element("interaction-id", StringType.DEFAULT, "User interaction id.", 0, 1));
        _defn.add(new Interface.Element("interaction-token", StringType.DEFAULT, "User interaction token.", 0, 1));
        Interface.Element args = new Interface.Element("args", XmlDocType.DEFAULT, "args", 1, 1);
        args.add(new Interface.Element(Interaction.ARG_COPY_TO_PROJECT, StringType.DEFAULT,
                "Destination path.", 1, 1));
        _defn.add(args);
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Validate the field values of the user interaction.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

//        System.out.println(String.format("DEBUG: %s: %s", SERVICE_NAME, args));

        // String shareableId = args.value("shareable-id");
        // String uploadId = args.value("upload-id");
        // String interactionId = args.value("interaction-id");
        // String interactionToken = args.value("interaction-token");
        String copyToProject = args.value("args/" + Interaction.ARG_COPY_TO_PROJECT);
        if (copyToProject != null && !copyToProject.trim().isEmpty()) {
            boolean exists = executor().execute("asset.namespace.exists", "<args><namespace>" + copyToProject + "</namespace></args>",
                    null, null).booleanValue("exists");
            if (exists) {
                w.add("valid", new String[]{"name", Interaction.ARG_COPY_TO_PROJECT}, true);
            } else {
                w.add("valid", new String[]{"name", Interaction.ARG_COPY_TO_PROJECT,
                        "message", "destination namespace does not exist", "suggested-value", null}, true);
            }
        } else {
            w.add("valid", new String[]{"name", Interaction.ARG_COPY_TO_PROJECT, "message",
                    "Missing value of " + Interaction.ARG_COPY_TO_PROJECT, "suggested-value", null}, false);
        }
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

}
