/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixReMount extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectPosixReMount()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn,  allowPartialCID);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Reads the posix mount state from meta-data set on the project namespace. If  the element should-be-mounted is true (or missing), the project is mounted, applying the rest of the state found in the meta-data. If the apply-mode-bits meta-data is missing (we did not set initially), it defaults to true (for backwards compatibility).";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.remount";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String theProjectID = args.stringValue("project-id");
		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;

		String restrictAction = "merge";
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);

			// FInd last set read only state of mount point
			XmlDoc.Element r = Posix.describePosixMountMetaData(executor(), ph.nameSpace());
			Boolean shouldBeMounted = r.booleanValue("should-be-mounted", true);
			if (shouldBeMounted) {
				Boolean readOnly = r.booleanValue("read-only", false);

				// If the value is not there, for backwards compatibility, we set to true
				// This is because previously we implicitly mounted with apply-mode-bits true and
				// and no meta-data was set on the namespace
				Boolean applyModeBits = r.booleanValue("apply-mode-bits", true); 

				// If the value is not there, for backwards compatibility, we set to 'all'
				// This is because previously we implicitly mounted with allowed-protocol 'all'
				// and no meta-data was set on the namespace
				String allowedProtocol = r.stringValue("allowed-protocol", "all"); 
				//
				XmlDoc.Element metaExport = r.element("metadata-export");

				// The function below will take care of finding the last restrictions
				// state (as action = merge)
				Collection<XmlDoc.Element> restrictions = null;

				// Remount
				XmlDoc.Element root = null;
				String rootAction = "merge";
				SvcProjectPosixMount.mount(executor(), ph.id(), metaExport, restrictions, 
						restrictAction, root, rootAction, readOnly, false, 
						applyModeBits, allowedProtocol, w);
			}
		}
	}
}
