package au.org.vicnode.mflux.services.dataregistry;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.services.SvcProjectQuotaGet;
import unimelb.rcs.data.registry.Collection;
import unimelb.rcs.data.registry.CollectionRef;
import unimelb.rcs.data.registry.DataRegistry;
import unimelb.rcs.data.registry.Product;
import unimelb.rcs.data.registry.StorageProduct;

public class SvcDataRegistryMediafluxCollectionList extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "data.registry.mediaflux.collection.list";

    public static final String SERVICE_PROJECT_QUOTA_GET_VN = SvcProjectQuotaGet.SERVICE_NAME;
    public static final String SERVICE_PROJECT_QUOTA_GET_DS = "ds.project.quota.get";
    public static final String ONBOARDING_ASSET_NAMESPACE = "/local-admin/Onboarding";

    private Interface _defn;

    public SvcDataRegistryMediafluxCollectionList() {
        _defn = new Interface();

        // Note: we want to set created-after to 'today-30day' by default to reduce the
        // unnecessary api calls to get collection details.
        SvcDataRegistryCollectionList.addArgsDefn(_defn, true);
        _defn.add(new Interface.Element("product-status", new EnumType(Product.Status.values()),
                "Filter the result by product status.", 0, 1));
        _defn.add(new Interface.Element("details", BooleanType.DEFAULT,
                "Return details of the collection. Defaults to true.", 0, 1));
        _defn.add(new Interface.Element("list-projects", BooleanType.DEFAULT,
                "Include associated projects. Defaults to true.", 0, 1));

    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "List data registry collections with Mediaflux allocation.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

        // Note: we want to set created-after to 'today-30day' by default to reduce the
        // unnecessary api calls to get collection details.
        if (!args.elementExists("created-after")) {
            args.add(new XmlDoc.Element("created-after", "today-30day"));
        }

        boolean details = args.booleanValue("details", true);
        boolean listProjects = args.booleanValue("list-projects", true);
        List<Collection> collections = listMediafluxCollections(executor(), args);
        if (collections != null) {
            for (Collection collection : collections) {
                PluginTask.checkIfThreadTaskAborted();
                XmlDoc.Element projects = listProjects ? getProjects(collection.code(), executor()) : null;
                if (details) {
                    collection.saveXml(w, projects);
                } else {
                    collection.getRef().saveXml(w, projects);
                }
            }
        }
        w.add("count", collections.size());
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    public boolean canBeAborted() {
        return true;
    }

    private static List<Collection> listMediafluxCollections(ServiceExecutor executor, XmlDoc.Element args)
            throws Throwable {

        Product.Status productStatus = Product.Status.fromString(args.stringValue("product-status"));
        List<CollectionRef> cs = SvcDataRegistryCollectionList.listCollections(executor, args);
        List<Collection> resultCollections = new ArrayList<>();
        DataRegistry reg = DataRegistry.get(executor);
        for (CollectionRef c : cs) {
            PluginTask.checkIfThreadTaskAborted();
            Collection collection = SvcDataRegistryCollectionDescribe.describeCollection(executor, reg, c.code);
            if (collection == null) {
                continue;
            }
            Product mediafluxProduct = collection.product(StorageProduct.MEDIAFLUX);
            if (mediafluxProduct == null) {
                continue;
            }
            if (productStatus != null && !productStatus.equals(mediafluxProduct.status())) {
                continue;
            }
            resultCollections.add(collection);
        }
        return resultCollections;
    }

    static XmlDoc.Element getProjects(String code, ServiceExecutor executor) throws Throwable {
        java.util.Collection<ProjectInfo> projects = getProjects(executor, code).values();
        XmlDocMaker dm = new XmlDocMaker("projects");
        long totalQuotaAllocation = 0;
        for (ProjectInfo p : projects) {
            dm.add("project", new String[] { "prod-type", p.prodType, "quota-gb", p.quotaAllocationGBAsString(),
                    "used-gb", p.quotaUsedGBAsString() }, p.projectId);
            if (p.quotaAllocation != null && p.quotaAllocation > 0) {
                totalQuotaAllocation += p.quotaAllocation;
            }
        }
        dm.add("count", projects.size());
        dm.add("total-quota-gb", Long.toString(totalQuotaAllocation / 1000000000));
        return dm.root();
    }

    static Map<String, ProjectInfo> getProjects(ServiceExecutor executor, String code) throws Throwable {
        Map<String, ProjectInfo> projects = new LinkedHashMap<>();

        // search for the onboarding assets to get the project ids
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "xpath(ds:Collection/key)='VicNode:" + code + "' or xpath(ds:Collection/key)='" + code + "'");
        dm.add("namespace", ONBOARDING_ASSET_NAMESPACE);
        dm.add("action", "get-value");
        dm.add("xpath", new String[] { "ename", "project-id" }, "meta/ds-admin:User-Onboarding/project-id");
        dm.add("xpath", new String[] { "ename", "project-parent-namespace" },
                "meta/ds-admin:User-Onboarding/namespace");
        dm.add("xpath", new String[] { "ename", "project-parent-path" }, "meta/ds-admin:User-Onboarding/path");
        List<XmlDoc.Element> aes = executor.execute("asset.query", dm.root()).elements("asset");
        if (aes != null) {
            for (XmlDoc.Element ae : aes) {
                String projectId = ae.value("project-id");
                boolean isCollectionAssetProject = ae.value("project-parent-path") != null;
                projects.put(projectId, ProjectInfo.resolve(executor, projectId, isCollectionAssetProject));
            }
        }
        return projects;
    }

    static class ProjectInfo {
        public final String projectId;
        public final String prodType;
        public final String path;
        public final Long quotaAllocation;
        public final Long quotaUsed;

        ProjectInfo(XmlDoc.Element pe) throws Throwable {
            this.projectId = pe.value("id");
            this.prodType = pe.value("prod-type");
            this.path = pe.stringValue("path", pe.value("namespace"));
            this.quotaAllocation = pe.longValue("quota/allocation", null);
            this.quotaUsed = pe.longValue("quota/used", null);
        }

        public final Double quotaAllocationGB() {
            if (this.quotaAllocation == null) {
                return null;
            }
            if (this.quotaAllocation > 0) {
                return (double) this.quotaAllocation / 1000000000.0;
            } else {
                return 0.0;
            }
        }

        public final String quotaAllocationGBAsString() {
            Double gb = quotaAllocationGB();
            if (gb == null) {
                return null;
            } else {
                return new DecimalFormat("#").format(gb);
            }
        }

        public final Double quotaUsedGB() {
            if (this.quotaUsed == null) {
                return null;
            }
            if (this.quotaUsed > 0) {
                return (double) this.quotaUsed / 1000000000.0;
            } else {
                return 0.0;
            }
        }

        public final String quotaUsedGBAsString() {
            Double gb = quotaUsedGB();
            if (gb == null) {
                return null;
            } else {
                return new DecimalFormat("#").format(gb);
            }
        }

        static ProjectInfo resolve(ServiceExecutor executor, String projectId, boolean isCollectionAssetProject)
                throws Throwable {
            String serviceName = isCollectionAssetProject ? SERVICE_PROJECT_QUOTA_GET_DS : SERVICE_PROJECT_QUOTA_GET_VN;
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("project-id", projectId);
            XmlDoc.Element pe = executor.execute(serviceName, dm.root()).element("project");
            return new ProjectInfo(pe);
        }
    }

}
