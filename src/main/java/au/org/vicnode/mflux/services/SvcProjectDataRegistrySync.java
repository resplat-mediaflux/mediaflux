/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectDataRegistrySync extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectDataRegistrySync()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
		_defn.add(new Interface.Element("contains", StringType.DEFAULT, "A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",	 0, 1));
		SvcProjectDescribe.addProdType (_defn);
		_defn.add(new Interface.Element("list-only", BooleanType.DEFAULT, "If true (default), list the differences, but don't actually set.",	 0, 1));
		_defn.add(new Interface.Element("show-all", BooleanType.DEFAULT, "If true (default false), show the results for all projects. If false, only show the results for projects where there is actually a difference between the project meta-data and the registry.",	 0, 1));
		_defn.add(new Interface.Element("show-full-error", BooleanType.DEFAULT, "If there is an error looking up the registry, report the full error (default false). If false, just repot that the lookup failed.",	 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Synchronize data registry meta-data with Mediaflux project meta-data (ds:Collection) by updating the latter if it differs from the former.  Currently only inspects  the mediaflux 'owner' (DataRegistry custodian) and the Collection:retention/review (DataRegistry review_year).  To use this service, you must have your data registry API key stored in your secure wallet.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.data.registry.sync";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		String prodType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		String keyString = args.value("contains");
		if (keyString!=null) {
			keyString = keyString.toUpperCase();
		}
		Boolean listOnly = args.booleanValue("list-only", true);
		Boolean showAll = args.booleanValue("show-all", false);
		Boolean showFullError = args.booleanValue("show-full-error", false);

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) return;

		//

		// Iterate through projects
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);

			if (Project.keepOnProjectType(ph, prodType) &&
					Project.keepOnKeyString(ph.id(), keyString)) {

				XmlDocMaker out = new XmlDocMaker("args");
				out.push("project");
				out.add("id", ph.id());

				// Describe the registry entry
				String key2 = ph.collectionKey();

				// Strip off the any leading namespace authority as the registry
				// just holds the value (we hold in Mediaflux VicNode:<key>
				// as VicNode is the original authority. This is a bit ugly.
				// TBD maybe time to remove the VicNode prefix.
				int idx = key2.indexOf(":");
				String key = key2;
				if (idx>=0) {
					key = key2.substring(idx+1);
				}

				if (key==null || (key!=null && key.isEmpty())) {
					// We always want to see these so such projects
					// are included regardless in the output.
					out.add("key", "missing");
					out.pop();
				} else {
					out.add("key", key);

					// Catch exceptions. If the collection ID does not
					// exist we will get an exception
					Boolean differ = false;
					Boolean failed = false;
					try {
						XmlDocMaker dm = new XmlDocMaker("args");
						dm.add("code", key);
						XmlDoc.Element reg = executor().execute(Properties.SERVICE_ROOT+"data.registry.collection.describe", dm.root());

						// Replace 'owner's with the 'custodians' (RCAO)
						Boolean differ1 = syncCustodians (executor(), reg, ph, listOnly, out);

						// Sync retention review date
						Boolean differ2 = syncRetentionReviewDate (executor(), reg, ph, listOnly, out);
						// 
						differ = differ1 || differ2;
					} catch (Throwable t) {
						// Very verbose messages
						if (showFullError) {
							out.add("registry-lookup-failed", t.getMessage());
						} else {
							out.add("registry-lookup", "failed");
						}
						failed = true;
					}

					out.pop();

					if (showAll || differ || failed) {
						// Show output
						w.addAll(out.root().elements());
					} 
				}
			}
		}
	}


	private  Boolean syncCustodians (ServiceExecutor executor, XmlDoc.Element reg, 
			ProjectHolder ph, 
			Boolean listOnly, XmlDocMaker  w) throws Throwable {

		// Find the contacts in the registry
		Collection<XmlDoc.Element> contacts = reg.elements("collection/contacts/contact");
		if (contacts==null) {
			// Nothing to sync
			return false;
		}

		// Find the contacts who are custodians in the registry and add 
		// them to a list
		List<XmlDoc.Element> custodians = new Vector<XmlDoc.Element>();
		if (contacts!=null) {
			for (XmlDoc.Element contact : contacts) {
				String role = contact.value("role");
				if (role.equals("custodian")) {
					custodians.add(contact);
				}
			}
		}

		// Proceed if we have some custodians in the registry
		Boolean differ = false;
		if (custodians.size()>0) {
			w.push("rcao-custodian");

			// Find collection meta-data from Mediaflux. We MUST re-fetch from the
			// asset namespace in these comparison functions, because we are going
			// to change the XmlDoc.Element elements by reference.  That will
			// muck up the copy stored in the ProjectHolder (ph) object.
			XmlDoc.Element nsMeta = 
					NameSpaceUtil.describe(null, executor, ph.nameSpace()).element("namespace/asset-meta");
			String collectionDocType = Properties.getCollectionDocType(executor);
			XmlDoc.Element cMeta = nsMeta.element(collectionDocType);
			Collection<XmlDoc.Element> owners = cMeta.elements("owner");

			/// Remove these 'owner' meta-data elements from the parent
			// (which we will make use of by reference) because we will
			// just add a new 'owner' from the registry
			if (owners!=null) {
				for (XmlDoc.Element owner : owners) {
					cMeta.remove(owner);

					// The registry does not contain the faculty element
					// so we need to remove it before comparing details
					if (owner.elementExists("faculty")) {
						XmlDoc.Element fac = owner.element("faculty");
						owner.remove (fac);
					}
				}

				// Output the current owners with the faculty removed
				w.push("mediaflux");
				w.addAll(owners);
				w.pop();
			}

			// Iterate through custodians and put them in a form suitable
			// for comparison. list them to the output also
			w.push("registry");
			List<XmlDoc.Element> newOwners = new Vector<XmlDoc.Element>();
			for (XmlDoc.Element custodian : custodians) {
				// Build new
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.push("owner");
				dm.add("first",custodian.value("user/first_name"));
				dm.add("last", custodian.value("user/last_name"));
				dm.add("email", custodian.value("user/email"));
				dm.add("organisation", "The University of Melbourne");
				dm.pop();

				// Add to the list of new owners
				newOwners.add(dm.root().element("owner"));
				w.add(dm.root().element("owner"));
			}
			w.pop();

			// See if there is anything to actually update
			// We don't really need to do this - we could
			// just replace current by what is in the registry.
			// This is just for fun...
			if (owners.size()!=newOwners.size()) {
				// They really should both be size 1 (one RCAO)
				// but there have been cases of multiple RCAOs
				// during transition
				differ = true;
			} else {
				for (XmlDoc.Element owner : owners) {
					Boolean found = false;
					for (XmlDoc.Element newOwner : newOwners) {
						if (owner.equals(newOwner)) {
							found = true;
						}
					}
					if (!found) {
						differ = true;
						break;
					}
				}
			}
			w.add("meta-data-differ", differ);

			// If changed, proceed with the update
			if (differ && !listOnly) {
				// Add new owner XML to document
				cMeta.addAll(newOwners);

				// Now get the List of all meta-data documents by reference
				Collection<XmlDoc.Element> docs = nsMeta.elements();

				// Set new meta-data. With namespace meta-data you
				// have to fully replace all. This function strips
				// out id attributes first
				NameSpaceUtil.setAssetNameSpaceMetaData(null, executor, ph.nameSpace(), docs);
				w.add("meta-data-replaced", "true");
			} else {
				w.add("meta-data-replaced", "false");
			}
			w.pop();
		} else {
			// THis really should not happen so we better throw an exception
			throw new Exception("There are no custodians (RCAO)in the registry for project " + ph.id() + "(collection " + ph.collectionKey() + ")");
		}

		return differ;


	}


	private  Boolean syncRetentionReviewDate (ServiceExecutor executor, XmlDoc.Element reg, 
			ProjectHolder ph, Boolean listOnly, XmlDocMaker  w) throws Throwable {

		// Find the review date in the registry
		String year = reg.value("collection/review_year");	
		// This will set day and month to 1
		Date date = new SimpleDateFormat("yyyy").parse(year);
		String dateReg = DateUtil.formatDate(date, "dd-MMM-yyyy");

		//
		w.push("review-date");

		// Find collection meta-data from Mediaflux. We MUST re-fetch from the
		// asset namespace in these comparison functions, because we are going
		// to change the XmlDoc.Element elements by reference.  That will
		// muck up the copy stored in the ProjectHolder (ph) object.
		XmlDoc.Element nsMeta = 
				NameSpaceUtil.describe(null, executor, ph.nameSpace()).element("namespace/asset-meta");		String collectionDocType = Properties.getCollectionDocType(executor);
				XmlDoc.Element cMeta = nsMeta.element(collectionDocType);
				date = cMeta.dateValue("retention/review");
				String dateMF =  null;
				if (date!=null) {
					dateMF = DateUtil.formatDate(date, "dd-MMM-yyyy");
				}

				// Convert to strings for easier comparison
				Boolean differ = false;
				if (dateMF==null) {
					differ = true;
				} else {
					differ = !dateReg.equals(dateMF);
				}


				w.push("mediaflux");
				if (dateMF==null) {
					w.add("review-date", "missing");
				} else {
					w.add("review-date", dateMF);
				}
				w.pop();

				w.push("registry");
				w.add("review-date", dateReg);
				w.pop();
				w.add("meta-data-differ", differ);

				// If changed, proceed with the update
				if (differ && !listOnly) {

					// We have to handle the case that this element does not
					// exist in the MF meta-data (fully or partially)
					XmlDoc.Element ret = cMeta.element("retention");
					if (ret==null) {
						XmlDocMaker m = new XmlDocMaker("retention");
						m.add("review", dateReg);
						cMeta.add(m.root());
					} else {
						XmlDoc.Element rev = ret.element("review");
						if (rev!=null) {
							rev.setValue(dateReg);
						} else {
							rev = new XmlDoc.Element("review");
							rev.setValue(dateReg);
							ret.add(rev);
						}
					}

					// Now get the List of all meta-data documents - changes
					// will update here though references
					Collection<XmlDoc.Element> docs = nsMeta.elements();

					// Set new meta-data. With namespace meta-data you have to fully replace all. This 
					// function strips out id attributes first
					NameSpaceUtil.setAssetNameSpaceMetaData(null, executor, ph.nameSpace(), docs);
					w.add("meta-data-replaced", "true");
				} else {
					w.add("meta-data-replaced", "false");
				}
				w.pop();
				//

				return differ;
	}



}
