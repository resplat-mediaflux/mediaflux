/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;
import au.org.vicnode.mflux.plugin.util.Util.StorageUnit;



public class SvcProjectSize extends PluginService {




	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectSize()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
		_defn.add(new Interface.Element("sort", BooleanType.DEFAULT, "Sort projects by total size ingested (true).", 0, 1));
		_defn.add(new Interface.Element("use-quota", BooleanType.DEFAULT, "Use the quota to get storage used (default true).", 0, 1));
		_defn.add(new Interface.Element("sum5", BooleanType.DEFAULT, "Also sum up how much is in files greater than or equal to 5GB. Default is false.", 0, 1));
		_defn.add(new Interface.Element("contains", StringType.DEFAULT, "A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",	 0, 1));
		_defn.add(new Interface.Element("size", IntegerType.DEFAULT, "The number of projects to list (if sorting, it's the sorted list that is shortened).",	 0, 1));
		_defn.add(new Interface.Element("unit", new EnumType(Util.StorageUnit.units()), "Human readable unit.  Defaults to TB.", 0, 1));
		_defn.add(new Interface.Element("show-user", BooleanType.DEFAULT, "Show the users in the output. Default false.", 0, 1));
		SvcProjectDescribe.addProdType (_defn);
		SvcProjectDescribe.addRepType (_defn);
}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns project sizes (bytes and number of assets) with optional sort. Can either sum up all the assets (slow) or use quota (fast - but does not handle inherited quotas yet).";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.size";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		Boolean sortBySize = args.booleanValue("sort",  true);
		Boolean useQuota = args.booleanValue("use-quota",  true);
		Boolean doSum5  = args.booleanValue("sum5",  false);
		Boolean showUser  = args.booleanValue("show-user",  false);
		if (useQuota) {
			w.add("warning", "Does not handled inherited quotas");
		}
		String prodType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		String keyString = args.value("contains");
		if (keyString!=null) keyString = keyString.toUpperCase();
		int size = args.intValue("size",-1);
		String repType = args.stringValue("rep-type", Properties.repTypes[0]);
		String unit = args.stringValue("unit", "TB");

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) {
			return;
		}
		Util.StorageUnit storageUnit = Util.StorageUnit.fromString(args.value("unit"), StorageUnit.toUnit(unit));

		// Iterate and summarise
		Integer nZero = 0;
		ArrayList<XmlDoc.Element> projectResults = new ArrayList<XmlDoc.Element>();
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			
			// FInd replication asset processing queue
			String qName = ProjectReplicate.isProjectReplicated(ph);

			if (Project.keepOnRepType(executor(), qName, repType) &&
				Project.keepOnProjectType(ph, prodType) &&
				Project.keepOnKeyString(ph.id(), keyString)) {
				
				String assetNameSpace = ph.nameSpace();
				String parentNameSpace = ph.parentNameSpace();
				XmlDoc.Element users = Project.listUsers(executor(), ph.id(), false, false);

				// Get the size of the project. Does not handle inherited quotas yet.
				XmlDocMaker dm2 = new XmlDocMaker("args");
				dm2.push("project");
				dm2.add("project-id", projectID);
				dm2.add("key", ph.collectionKey());
				if (showUser && users!=null) {
					dm2.add(users);
				}

				String sum = "0";
				String n = "0";
				//
				String sum5 = "0";
				String n5 = "0";
				if (useQuota) { 
					XmlDoc.Element nsMeta = ph.metaData();
					sum = nsMeta.value("namespace/quota/used");
					n = nsMeta.value("namespace/quota/count");
				} else {
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add("where", "namespace>='"+assetNameSpace+"'");
					dm.add("action", "sum");
					dm.add("xpath", "content/size");
					XmlDoc.Element r = executor().execute("asset.query", dm.root());
					sum = r.value("value");
					n = r.value("value/@nbe");
				}
				//
				{
					String hvalue = null;
					if (sum==null) {
						sum = "0";
						hvalue = "0 " + unit;
					} else {
						Long t1 = Long.parseLong(sum);
						XmlDoc.Element t2 = storageUnit.convert(t1, "size", null);
						hvalue = t2.value() + " " + unit;					
					}
					if (sum.equals("0")) {
						nZero++;
					}
					dm2.add("size", new String[] {"parent", parentNameSpace, "size-human", hvalue}, sum);
					dm2.add("n", n);
				}
				//
				if (doSum5) {
					// Sum files > 5GB
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add("where", "namespace>='"+assetNameSpace+"' and asset has content and csize>=5000000000");
					dm.add("action", "sum");
					dm.add("xpath", "content/size");
					XmlDoc.Element r = executor().execute("asset.query", dm.root());
					sum5 = r.value("value");
					n5 = r.value("value/@nbe");

					String hvalue = null;
					if (sum5==null) {
						sum5 = "0";
						hvalue = "0 " + unit;
					} else {
						Long t1 = Long.parseLong(sum5);
						XmlDoc.Element t2 = storageUnit.convert(t1, "size", null);
						hvalue = t2.value() + " " + unit;					
					}
					dm2.push("sum5");
					dm2.add("size", new String[] {"size-human", hvalue}, sum5);
					dm2.add("n", n5);
					dm2.pop();
				}
				//
				dm2.pop();
				projectResults.add(dm2.root());
			}
			PluginTask.checkIfThreadTaskAborted();
		}
		//
		if (size==-1) {
			size = projectResults.size();
		}
		if (projectResults.size()>0) {
			if (sortBySize) {
				ArrayList<XmlDoc.Element> projectsSorted = sortBySize(projectResults);
				int i = 1;
				for (XmlDoc.Element project : projectsSorted) {
					w.addAll(project.elements());
					i++;
					if (i>size) {
						break;
					}
				}
			} else {
				int i = 1;
				for (XmlDoc.Element project : projectResults) {
					w.addAll(project.elements());
					i++;
					if (i>size) {
						break;
					}
				}
			}
		}
		w.add("n-projects", new String[] {"zero-sized", ""+nZero}, projectResults.size());
	}


	private static ArrayList<XmlDoc.Element> sortBySize (ArrayList<XmlDoc.Element> projectResults) throws Throwable {
		if (projectResults == null || projectResults.isEmpty()) {
			return null;
		}
		ArrayList<XmlDoc.Element> list = new ArrayList<XmlDoc.Element>(projectResults);
		Collections.sort(list, new Comparator<XmlDoc.Element>() {

			@Override
			public int compare(XmlDoc.Element d1, XmlDoc.Element d2)   {

				Long d1d = null;
				try {
					d1d = Long.parseLong(d1.value("project/size"));
				} catch (Throwable e) {
				}

				Long d2d = null;
				try {
					d2d = Long.parseLong(d2.value("project/size"));
				} catch (Throwable e) {
				}


				// This will never happen but we have to handle the exception
				if (d1d==null || d2d==null) {
					return 0;
				}
				int retval = Long.compare(d1d, d2d);

				if(retval > 0) {
					return -1;
				} else if(retval < 0) {
					return 1;
				} else {
					return 0;
				}

			}
		});
		return list;
	}

}
