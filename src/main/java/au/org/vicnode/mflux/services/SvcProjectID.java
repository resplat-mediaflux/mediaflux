/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;



import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import arc.xml.XmlWriter;



public class SvcProjectID extends PluginService {

	private Interface _defn;
	private static Boolean ALLOW_PARTIAL_CID = true;

	public SvcProjectID() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addMandatoryProjectID(_defn, ALLOW_PARTIAL_CID);
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Takes a project partial CID (e.g. 123) and returns the full project ID.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.id";
	}

	public boolean canBeAborted() {
		return false;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String projectID = args.stringValue("project-id");
		String t[] = Project.validateProject(executor(), projectID, ALLOW_PARTIAL_CID);
		w.add("project-id", t[1]);
	}	
}