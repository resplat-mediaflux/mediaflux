package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.EnumType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcFacilityShareableUploadManifestAttributeIncrement extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT
			+ "facility.shareable.upload.manifest.attribute.increment";

	private Interface _defn;

	public SvcFacilityShareableUploadManifestAttributeIncrement() {
		_defn = new Interface();
		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The id of the manifest asset.", 1, 1));
		_defn.add(new Interface.Element("name",
				new EnumType(new String[] { Manifest.ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED,
						Manifest.ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED_SUBSET,
						Manifest.ATTRIBUTE_USER_INTERACTION_COSUMED, Manifest.ATTRIBUTE_DIRECT_DOWNLOAD_ATTEMPT }),
				"The name of the attribute.", 1, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Increment the specified attribute of the manifest asset.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add(args, false);
		executor().execute("asset.attribute.atomic.increment", dm.root());
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
