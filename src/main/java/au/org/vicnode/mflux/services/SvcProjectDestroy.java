/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DictionaryUtil;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

/**
 * 
 * @author nebk
 *
 */


public class SvcProjectDestroy extends PluginService {

	private Interface _defn;
	private static Boolean allowPartialCID = false;

	public SvcProjectDestroy()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
		_defn.add(new Interface.Element("destroy",  BooleanType.DEFAULT, "Destroy the project (defaults to false - just a validation check on the project is done) ?", 0, 1));
		_defn.add(new Interface.Element("ignore-errors", BooleanType.DEFAULT, "If errors are encountered, catch them and list them, bit continue with the next destruction component (default false).", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Destroy the project and all associates resources on the primary server. Before destroying the role namespace, the service revokes permissions and granted roles on all actors.  Does not interact with DR copy. You must be system-administrator to execute this.  Do not use on the production UoM system because this service will hang because of the large number of assets (MF has to check every asset is not using the project document namespace).";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.destroy";
	}

	public boolean canBeAborted() {

		return true;
	}



	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {


		// Args
		String projectID = args.stringValue("project-id");
		Boolean destroy = args.booleanValue("destroy",false);
		Boolean ignoreErrors = args.booleanValue("ignore-errors", false);

		// Validate project name is valid
		// We don't allow child  CID matches
		Project.validateProject(executor(), projectID, allowPartialCID);

		if (!destroy) {
			w.add("project-id", new String[]{"validated", "true"});
			return;
		}
		destroy (executor(), projectID, ignoreErrors, w);
	}


	private void destroy (ServiceExecutor executor, String projectID,  Boolean ignoreErrors, XmlWriter w) throws Throwable {

		// Revoke permissions/roles on actors
		revokePerms (executor, projectID, w);

		// Unmount posix mount point. Exception if fails.
		PluginTask.checkIfThreadTaskAborted();
		try {
			SvcProjectPosixUnMount.unMount(executor, projectID, true, w);	
		} catch (Throwable t) {
			if (ignoreErrors) {
				w.add("posix-unmount-error", t.getMessage());
			} else {
				throw new Exception (t);
			}
		}

		// Destroy identity map
		try {
			Posix.destroyIdentityMap(executor, projectID);
		} catch (Throwable t) {
			if (ignoreErrors) {
				w.add("posix-identity-map-destroy-error", t.getMessage());
			} else {
				throw new Exception (t);
			}
		}

		// Destroy asset namespace
		PluginTask.checkIfThreadTaskAborted();
		try {
			Project.destroyProjectAssetNameSpace(executor, projectID, w);
		} catch (Throwable t) {
			if (ignoreErrors) {
				w.add("asset-namespace-destroy-error", t.getMessage());
			} else {
				throw new Exception (t);
			}
		}

		// Destroy document type namespace
		PluginTask.checkIfThreadTaskAborted();
		try {
			Project.destroyProjectMetaNameSpace(executor, projectID, w);
		} catch (Throwable t) {
			if (ignoreErrors) {
				w.add("document-namespace-destroy-error", t.getMessage());
			} else {
				throw new Exception (t);
			}
		}


		// Destroy dictionary namespace
		PluginTask.checkIfThreadTaskAborted();
		try {
			Project.destroyProjectDictionaryNameSpace(executor, projectID, w);
		} catch (Throwable t) {
			if (ignoreErrors) {
				w.add("dictionary-namespace-destroy-error", t.getMessage());
			} else {
				throw new Exception (t);
			}
		}


		// Destroy role namespace 
		PluginTask.checkIfThreadTaskAborted();
		try {
			Project.destroyProjectRoleNameSpace(executor, projectID, w);
		} catch (Throwable t) {
			if (ignoreErrors) {
				w.add("role-namespace-destroy-error", t.getMessage());
			} else {
				throw new Exception (t);
			}
		}


		// Remove term from dictionary mapping project ID to namespace 
		String dict = Properties.getProjectNamespaceMapDictionaryName(executor);
		try {
			DictionaryUtil.removeDictionaryTerm(executor, dict, projectID);
		} catch (Throwable t) {
			if (ignoreErrors) {
				w.add("dictionay-map-removal-error", t.getMessage());
			} else {
				throw new Exception (t);
			}
		}
	}


	private static void revokePerms (ServiceExecutor executor, 
			String projectID, XmlWriter w) throws Throwable {

		w.push("revoke-perms");

		// Revoke perms on ds-admin:project-access
		String role = Project.accessAssetNamespaceRoleName(executor);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", role);
		dm.add("type", "role");
		dm.push("perm");
		dm.add("access", "ACCESS");
		dm.add("resource", new String[] {"type", "dictionary:namespace"}, projectID+":");
		dm.pop();
		dm.push("perm");
		dm.add("access", "ACCESS");
		dm.add("resource", new String[] {"type", "document:namespace"}, projectID+":");
		dm.pop();
		executor.execute("actor.revoke", dm.root());
		w.add("role", role);


		// Revoke perms on ds-admin:project-role-namespace-administrator
		role = Project.roleNameSpaceAdminRoleName(executor);
		dm = new XmlDocMaker("args");
		dm.add("name", role);
		dm.add("type", "role");
		dm.push("perm");
		dm.add("access", "ADMINISTER");
		dm.add("resource", new String[] {"type", "role:namespace"}, projectID+":");
		dm.pop();
		executor.execute("actor.revoke", dm.root());
		w.add("role", role);
		w.pop();

		// Now revoke the project roles on any actor that they
		// are granted to
		w.push("revoke-project-roles");
		Collection<String> projectRoles = Project.actualRoleNames(projectID);
		for (String projectRole : projectRoles) {
			dm = new XmlDocMaker("args");
			dm.add("role", new String[] {"type", "role"}, projectRole);
			XmlDoc.Element r = executor.execute("actors.granted", dm.root());
			if (r!=null) {
				Collection<XmlDoc.Element> actors = r.elements ("actor"); 
				if (actors!=null) {
					w.push("project-role", new String[] {"name", projectRole});
					for (XmlDoc.Element actor : actors) {
						// Revoke this project role
						dm = new XmlDocMaker("args");
						dm.add("name", actor.value("@name"));
						dm.add("type", actor.value("@type"));
						dm.add("role", new String[] {"type", "role"}, projectRole);
						executor.execute ("actor.revoke", dm.root());
						w.add("actor", new String[] {"type", actor.value("@type")}, actor.value("@name"));
					}
					w.pop();
				}
			}
		}
		w.pop();
	}
}
