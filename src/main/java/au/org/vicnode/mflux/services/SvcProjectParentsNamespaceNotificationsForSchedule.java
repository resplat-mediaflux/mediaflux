/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;
import au.org.vicnode.mflux.plugin.util.Util.StorageUnit;

public class SvcProjectParentsNamespaceNotificationsForSchedule extends PluginService {


	private Interface _defn;

	public SvcProjectParentsNamespaceNotificationsForSchedule()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("frequency", new EnumType(Properties.notificationFrequency), 
				"The notification frequency to select from the namespace meta-data - the notification is only sent for the meta-data that matches this frequency.", 1, 1));
		_defn.add(new Interface.Element("email", StringType.DEFAULT, 
				"If an email is provided, send the notifrication here instead of to that provided in the namespace meta-data.", 1, Integer.MAX_VALUE));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Sends notifications (by email) to recipients for parent project namespaces where the recipient is managing a quota for multiple child projects.  Notifications are a summary of usage and a critical threshold warning. Looks at the ${NS-ADMIN}:Project-Parent-Admin/notification.  Run this service in a regular (e.g. weekly) scheduled job.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.parents.namespace.notifications.for.schedule";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String freq = args.value("frequency");
		Collection<String> emailsIn = args.values("email");

		// Iterate through parent project namespaces. Only do more if the namespace
		// has the notification meta-data
		Collection<String> parents = Project.assetNameSpaceRoots(executor());
		Collection<String> emails = new Vector<String>();
		if (emailsIn!=null) emails.addAll(emailsIn);

		for (String parent : parents) {
			w.push("project-parent");
			w.add("namespace", parent);
			XmlDoc.Element r = NameSpaceUtil.describe(null, executor(), parent);

			// Fetch meta-data from $NS_ADMIN:Project/notification
			XmlDoc.Element meta = r.element("namespace/asset-meta/" + Properties.getProjectParentAdminDocType(executor()));

			// Fetch/convert some quota values if there is one.
			String quotaAllocHuman = null;
			String quotaUsedHuman = null;
			String quotaFractionUsed = null;

			// If the quota is null (should not be), it is handled in the summary function
			// which prints it out in its message.  Can proceeed with juyst notification info
			XmlDoc.Element quota = r.element("namespace/quota");
			if (quota!=null) {
				quotaAllocHuman = quota.value("allocation/@h");
				String t = quota.stringValue("allocation");
				Long quotaAllocBytes = Long.parseLong(t);
				//
				quotaUsedHuman = quota.value("used/@h");
				t = quota.value("used");
				long quotaUsedBytes = Long.parseLong(t);
				//
				quotaFractionUsed = String.format("%.2f", 100.0d * (double)(quotaUsedBytes) / (double)(quotaAllocBytes));
			}
			w.add("quotaallocahuman", quotaAllocHuman);
			w.add("quotaUsedHuman", quotaUsedHuman);
			w.add("quotafractionused", quotaFractionUsed);

			// Handle notifications
			if (meta!=null) {

				// Summary of data.  Namespaces with data older than age will be presented
				XmlDoc.Element s = meta.element("notification/summary");
				if (s!=null) {
					String when = s.value("frequency");
					if (when.equalsIgnoreCase(freq)) {
						if (emails.size()==0) {
							emails.addAll(s.values("email"));
						}
						if (emails.size()>0) {
							String age= s.value("age");
							Integer depth = s.intValue("depth");
							w.add("age", age);
							w.add("depth", depth);
							sendSummary (executor(),  emails, parent, depth, age, quotaAllocHuman,
									quotaUsedHuman, quotaFractionUsed, w);
						}
					}
				}

				// Freespace critical warning
				XmlDoc.Element f = meta.element("notification/freespace");
				if (f!=null) {
					String when = f.value("frequency");
					if (when.equalsIgnoreCase(freq)) {
						if (emails.size()==0) {
							emails.addAll(f.values("email"));
						}
						String threshold = f.value("threshold");
						if (threshold!=null && emails.size()>0) {
							sendThreshold (executor(),  emails, parent, threshold, w);
						}
					}
				}

			}
			w.pop();
		}
	}


	private void sendSummary (ServiceExecutor executor,  Collection<String> emails, String parent, int depth, 
			String age, String quotaAllocHuman, String quotaUsedHuman, String quotaFractionUsed, XmlWriter w) throws Throwable {

		if (emails.size()==0) {
			return;
		}

		// FInd all projects
		Collection<String> projectIDs = Project.projectIDs(executor(), true, true);
		if (projectIDs.size()==0) {
			return;
		}

		// Things to be sent in email
		// Key is mtime (in sortable format dd-MM-YYYY HH:MM:SS)
		// Vector[0] = namespace
		// Vector[1] = usedStorage (bytes)
		// Vector[2] = daysSinceModified  
		// Vector[3] = mtime dd-MMM-YYYY HH:MM:SS 

		HashMap<String,Vector<String>> map = new HashMap<String,Vector<String>>();

		// Iterate and only keep the ones for this parent namespace
		Vector<String> theNameSpaces = new Vector<String>();
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);

			if (Project.keepOnParent(ph, parent)) {
				w.push("project");
				w.add("id", projectID);

				// Make a list of project child namespaces we want to look at
				if (depth==0) {
					// We just look at the top level of namespaces in the Project
					theNameSpaces.addAll(NameSpaceUtil.listNameSpaces(executor, ph.nameSpace(), true));
				} else if (depth==1) {
					// We just look one level below the top level of namespaces in the Project
					Collection<String> topNameSpaces = NameSpaceUtil.listNameSpaces(executor, ph.nameSpace(), true);
					for (String topNameSpace : topNameSpaces) {
						theNameSpaces.addAll(NameSpaceUtil.listNameSpaces(executor, topNameSpace, true));
					}
				}

				// Now filter so we keep just the namespaces which have data older than the given age
				// and extract some quantities into the map
				for (String theNameSpace : theNameSpaces) {
					filterOnTime (executor, theNameSpace, map, age, w);
				}
				w.pop();
			} 
		}

		// Generate message for email
		if (map.size()==0) return;
		String body = createSummaryMessage (executor, parent, map,  quotaAllocHuman, quotaUsedHuman, quotaFractionUsed, w);
		w.add("body", body);

		// Send message to recipients
		String subject = "Data Storage Usage Check for parent namespace '" + parent + "'";
		String from = Util.getServerProperty(executor, "notification.from");
		MailHandler.sendMessage(executor, emails, subject, body, "text/html", false, from, w);
	}


	
	
	private String createSummaryMessage (ServiceExecutor executor, String parentNameSpace, HashMap<String,Vector<String>> map,  
			String quotaAllocHuman, String quotaUsedHuman, String quotaFractionUsed, XmlWriter w) throws Throwable {

		// Create the table component of the message
		String table = "<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\" style=\"width: 95%;\"><thead><tr style=\"font-weight: bold;\"><th>Path</th><th>Storage Used</th><th>Last Modified</th><th>Days Since Last Modified</th></tr></thead><tbody>";


		// Get key vector and sort in natural time order by using TreeSet (a SortedSet)
		Set<String> t = map.keySet();
		TreeSet<String> keys = new TreeSet<String>(t);

		Double totalUsedGB = 0d;
		for (String key : keys) {
			// Vector[0] = namespace
			// Vector[1] = usedStorage (bytes)
			// Vector[2] = daysSinceModified  
			// Vector[3] = mtime dd-MMM-YYYY HH:MM:SS 
			Vector<String> v = map.get(key);

			int dsm = Integer.parseInt(v.get(2));
			String style = null;
			if (dsm < 7) {
				style = "background: #ccffcc; text-shadow:1px 1px 0 #444;";
			} else if (dsm < 14 ) {
				style = "background: #ccffcc; text-shadow:1px 1px 0 #444;";
			} else if (dsm < 21 ) {
				style = "background: #ffff80; text-shadow:1px 1px 0 #444;";
			} else {
				style = "background: #ff3333; color: white; text-shadow:1px 1px 0 #444;";
			}

			table += "<tr style=\"" + style + "\">";
			table += "<td align=\"left\">" + v.get(0) + "</td>";          
			//
			Double usedB = Double.parseDouble(v.get(1));
			Double usedGB = usedB / 1000000000d;
			totalUsedGB += usedGB;
			table += "<td align=\"right\">" + String.format("%.2f", usedGB) + " GB</td>";
			//
			table += "<td align=\"center\">" + v.get(3) + "</td>";          
			table += "<td align=\"right\">" + v.get(2) + " days</td>";          
			table += "</tr>";
		}

		// Finalise table
		table += "<tr><td align=\"right\"><b>Total Storage Used:</b></td><td align=\"right\"><b>" + String.format("%.2f", totalUsedGB) + " GB</b></td><td></td><td></td></tr>";
		table += "</tbody></table>";

		// NOw add outer components of message body
		String html = "<html><head><title>Project Data Check for " + parentNameSpace + "</title></head><body>";
		html += "<br/><p><b>Dear Administrator,</b><br/><br/>";
		if (quotaUsedHuman!=null && quotaAllocHuman!=null && quotaFractionUsed!=null) {
			html += "<b>The Mediaflux projects under this parent have used " + 
					quotaUsedHuman + " of " + quotaAllocHuman + " (" + quotaFractionUsed + "%).</b></p>";
		}
		html += "<b>Please consider destroying old data to reclaim storage as needed.</b>";
		html += "<p></p>";

		html += table + "</body></html>";

		return html;
	}


	private void filterOnTime (ServiceExecutor executor, String nameSpace, HashMap<String,Vector<String>> map,  String age, XmlWriter w) throws Throwable {

		// FInd mtime of all assets older than diffDay days
		String t = "<='now-"+age+"day'";
		XmlDocMaker dm = new XmlDocMaker("args");
		String where = "namespace>='" + nameSpace + "' and (ctime"+t+" or mtime"+t+")";
		dm.add("where", where);
		dm.add("action", "max");
		dm.add("xpath", "mtime");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		if (r==null) return;

		// Convert mtime to a sortable format
		String mtime = r.value("value");		
		if (mtime==null) {
			return;
		}
		String mtimeSort = DateUtil.convertDateString(mtime, "dd-MMM-yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

		// FInd date difference in days from NOW to the asset mtime
		String mtimeDiff = dateDiff (mtime);

		// FInd sum of content
		XmlDoc.Element sum = NameSpaceUtil.sumContent(executor, null, nameSpace, null);

		// Populate results
		Vector<String> v = new Vector<String>();
		v.add(nameSpace);
		v.add(sum.value("value"));
		v.add(mtimeDiff);
		v.add(mtime);
		map.put(mtimeSort, v);

	}

	private String dateDiff (String mtime) throws Throwable {
		// Convert the mtime to a date
		Date mtimeDate = DateUtil.dateFromString(mtime, "dd-MMM-yyyy HH:mm:ss");
		Date now = new Date();
		return ""+DateUtil.dateDifferenceInDays(now, mtimeDate);		
	}

	private void sendThreshold (ServiceExecutor executor,  Collection<String> emails, String parent, String threshold, XmlWriter w) throws Throwable {

		if (emails.size()==0) return;

		// Get threshold units
		String unit = getUnit (threshold);
		if (unit==null) {
			throw new Exception ("Could not extract units from threshold string '" + threshold);
		}

		// Get the free space under this parent namespace
		XmlDoc.Element meta = NameSpaceUtil.describe(null, executor, parent);
		//
		String quotaBytes = meta.value("namespace/quota/allocation");
		long iQuotaBytes = Long.parseLong(quotaBytes);
		//
		String usedBytes = meta.value("namespace/quota/used");
		long iUsedBytes = Long.parseLong(usedBytes);
		//
		long iFreeBytes = iQuotaBytes - iUsedBytes;
		StorageUnit su = StorageUnit.fromString(unit, StorageUnit.GB); 
		String freeBytes = su.convertToString(iFreeBytes);
		//

		long iThresh = Util.StorageUnit.convertToBytes(threshold);
		if (iFreeBytes < iThresh) {
			// Generate notification
			String subject = "Warning: only " + freeBytes + " free space left for '" + parent + "' projects";
			String body = "Dear Administrator \n\n" + 
					"There is only " + freeBytes + " of storage left in your quota for parent project namespace '" + parent + "'\n" +
					"This is less than the warning threshold of " + threshold + "\n\n" +
					"     Allocated Quota : " + su.convertToString(iQuotaBytes) + "\n" +
					"     Used Storage : " + su.convertToString(iUsedBytes) + "\n" +
					"     Free Space : " + freeBytes + "\n\n" +
					"It is strongly recommended you destroy some data or you \n" +
					"may not be able to upload new data.\n\n" + 
					"regards \nThe Mediaflux team\n";

			// Send
			String from = Util.getServerProperty(executor, "notification.from");
			MailHandler.sendMessage(executor, emails, subject, body, null, false, from, w);
			w.add("subject", subject);
			w.add("body", body);
		} 
	}




	private String getUnit (String value) {
		int idx = value.lastIndexOf("B");
		if (idx<=0) return null;
		return value.substring(idx-1).trim();
	}
}
