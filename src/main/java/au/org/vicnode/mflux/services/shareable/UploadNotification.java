package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import unimelb.utils.FileSizeUtils;

public class UploadNotification {

    public static final String DEFAULT_SUBJECT = "Facility Data Upload";
    public final XmlDoc.Element uploadShareable;
    public final String uploadNS;
    private final XmlDoc.Element uploadArgs;
    public final Collection<String> sharedWithRecipients;
    public final SharedItems sharedItems;
    public final String facilityName;
    public final Collection<String> facilityNotificationRecipients;

    public UploadNotification(XmlDoc.Element shareable, String uploadNS, XmlDoc.Element uploadArgs,
            Collection<String> shareRecipients, SharedItems sharedItems, Collection<String> notificationRecipients)
            throws Throwable {
        this.uploadShareable = shareable;
        this.uploadNS = uploadNS;
        this.uploadArgs = uploadArgs;
        this.sharedWithRecipients = shareRecipients;
        this.sharedItems = sharedItems;
        this.facilityName = Shareable.getFacilityName(shareable);
        this.facilityNotificationRecipients = notificationRecipients;
    }

    public String emailSubjectSuffix() throws Throwable {
        if (this.uploadArgs != null) {
            return this.uploadArgs.value(Shareable.ARG_EMAIL_SUBJECT_SUFFIX);
        }
        return null;
    }

    private String htmlMessage(ServiceExecutor executor) throws Throwable {

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", String.format("namespace>='%s' and not(namespace='%s' and name='%s')", this.uploadNS,
                this.uploadNS, Manifest.MANIFEST_FILE_NAME));
        dm.add("xpath", "content/size");
        dm.add("action", "sum");

        XmlDoc.Element re = executor.execute("asset.query", dm.root());
        long fileCount = re.longValue("value/@nbe", 0);
        long totalSize = re.longValue("value", 0);

        StringBuilder sb = new StringBuilder();
        sb.append("<p>Hello,</p>\n");
        sb.append("<p>New data has been uploaded");
        if (this.facilityName != null) {
            sb.append(" from <b>").append(this.facilityName).append("</b>");
        }
        sb.append(".<p>\n");

        String tableOpen = "<table style=\"border:1px solid black; border-collapse:collapse; width:85%;\">";
        String tdLeftOpen = "<td align=\"right\" style=\"width:90px; border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em;\">";
        String tdRightOpen = "<td style=\"border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em\">";

        sb.append(tableOpen).append("\n");
        if (this.facilityName != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Facility Name:</b></td>").append(tdRightOpen)
                    .append(this.facilityName).append("</td></tr>\n");
        }
        sb.append("<tr>").append(tdLeftOpen).append("<b>Asset Namespace:</b></td>").append(tdRightOpen)
                .append(this.uploadNS).append("</td></tr>\n");
        sb.append("<tr>").append(tdLeftOpen).append("<b>File Count:</b></td>").append(tdRightOpen).append(fileCount)
                .append("</td></tr>\n");
        sb.append("<tr>").append(tdLeftOpen).append("<b>Total Size:</b></td>").append(tdRightOpen)
                .append(FileSizeUtils.getHumanReadableSize(totalSize)).append("</td></tr>\n");
        if (this.sharedWithRecipients != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Shared With:</b></td>").append(tdRightOpen);
            boolean first = true;
            for (String r : this.sharedWithRecipients) {
                if (first) {
                    first = false;
                } else {
                    sb.append("; ");
                }
                sb.append(r);
            }
            sb.append("</td></tr>\n");
        }
        String nameSuffix = this.uploadArgs.value(Shareable.ARG_NAME_SUFFIX);
        if (nameSuffix != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Name Suffix:</b></td>").append(tdRightOpen)
                    .append(nameSuffix).append("</td></tr>\n");
        }
        String keywords = this.uploadArgs.value(Shareable.ARG_KEYWORDS);
        if (keywords != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Keywords:</b></td>").append(tdRightOpen).append(keywords)
                    .append("</td></tr>\n");
        }
        String dataNote = this.uploadArgs.value(Shareable.ARG_DATA_NOTE);
        if (dataNote != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Data Note:</b></td>").append(tdRightOpen).append(dataNote)
                    .append("</td></tr>\n");
        }
        String emailSubjectSuffix = this.uploadArgs.value(Shareable.ARG_EMAIL_SUBJECT_SUFFIX);
        if (emailSubjectSuffix != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Email Subject Suffix:</b></td>").append(tdRightOpen)
                    .append(emailSubjectSuffix).append("</td></tr>\n");
        }

        if (this.sharedItems != null) {
            if (this.sharedItems.downloadLink != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>Direct Download Link:</b></td>").append(tdRightOpen);
                sb.append("<a href=\"").append(this.sharedItems.downloadLink.url).append("\">")
                        .append(this.sharedItems.downloadLink.url).append("</a>");
                if (this.sharedItems.downloadLink.expiry != null) {
                    sb.append("<br>Expiry: ").append(this.sharedItems.downloadLink.expiry);
                }
                if (this.sharedItems.downloadLink.password != null) {
                    sb.append("<br>Password: ").append(this.sharedItems.downloadLink.password);
                }
                sb.append("</td></tr>\n");
            }
            if (this.sharedItems.downloadShareable != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>(Data Mover) Client Download Link:</b></td>")
                        .append(tdRightOpen);
                sb.append("<a href=\"").append(this.sharedItems.downloadShareable.url).append("\">")
                        .append(this.sharedItems.downloadShareable.url).append("</a>");
                if (this.sharedItems.downloadShareable.validTo != null) {
                    sb.append("<br>Expiry: ").append(this.sharedItems.downloadShareable.validTo);
                }
                if (this.sharedItems.downloadShareable.downloadPassword != null) {
                    sb.append("<br>Password: ").append(this.sharedItems.downloadShareable.downloadPassword);
                }
                sb.append("</td></tr>\n");
            }
            if (this.sharedItems.userInteraction != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>Interaction Link:</b></td>").append(tdRightOpen);
                sb.append("<a href=\"").append(this.sharedItems.userInteraction.url).append("\">")
                        .append(this.sharedItems.userInteraction.url).append("</a>");
                if (this.sharedItems.userInteraction.expiresAt != null) {
                    sb.append("<br>Expiry: ").append(this.sharedItems.userInteraction.expiresAt);
                }
                sb.append("</td></tr>\n");
            }
        }
        sb.append("</table>\n");
        return sb.toString();
    }

    public String subject() throws Throwable {
        StringBuilder sb = new StringBuilder();
        if (this.facilityName != null) {
            sb.append(String.format("%s Data Upload", this.facilityName));
        } else {
            sb.append(DEFAULT_SUBJECT);
        }
        String suffix = this.emailSubjectSuffix();
        if (suffix != null) {
            sb.append(" ").append(suffix);
        }
        return sb.toString();
    }

    public void send(ServiceExecutor executor) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        for (String recipient : this.facilityNotificationRecipients) {
            dm.add("to", recipient);
        }
        dm.add("subject", subject());
        dm.add("body", new String[] { "type", "text/html" }, htmlMessage(executor));
        executor.execute("mail.send", dm.root());
    }

}
