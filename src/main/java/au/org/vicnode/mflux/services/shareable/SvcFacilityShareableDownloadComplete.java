package au.org.vicnode.mflux.services.shareable;

import java.util.Date;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcFacilityShareableDownloadComplete extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.download.complete";

    public static final String FACILITY_SHAREABLE_DOWNLOAD_LOG = "facility-shareable-download";

    static PluginLog log() {
        return PluginLog.log(FACILITY_SHAREABLE_DOWNLOAD_LOG);
    }

    static void logWarning(String msg) {
        log().add(PluginLog.WARNING, msg);
    }

    private final Interface _defn;

    public SvcFacilityShareableDownloadComplete() {
        _defn = new Interface();
        _defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "Shareable id.", 1, 1));
        _defn.add(new Interface.Element("notify", BooleanType.DEFAULT,
                "Notify the facility contacts specified in property: " + Shareable.PROPERTY_FACILITY_NOTIFICATION_TO
                        + ". Defaults to false.",
                0, 1));
        _defn.add(new Interface.Element("subset-selected", BooleanType.DEFAULT,
                "Indicates subset of the shareable was selected and downloaded by the user. Defaults to false", 0, 1));
        Interface.Element args = new Interface.Element("args", XmlDocType.DEFAULT, "Arguments.", 0, 1);
        args.setIgnoreDescendants(true);
        _defn.add(args);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Send email notification to the facility operators specified in property: '"
                + Shareable.PROPERTY_FACILITY_NOTIFICATION_TO + "' after the data is downloaded.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_MODIFY;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        String downloadShareableId = args.value("shareable-id");
        boolean subsetSelected = args.booleanValue("subset-selected", false);

        try {
            logWarning("[shareable: " + downloadShareableId + "] started post-download processing");
            XmlDoc.Element downloadShareable = Shareable.describe(executor(), downloadShareableId);
            Date downloadCompletionTime = new Date();
            boolean notify = args.booleanValue("notify", false);
            if (notify) {
                notifyOfDownload(executor(), downloadShareable, downloadCompletionTime);
            }
            String assetNamespace = downloadShareable.value("namespace");
            updateManifestAttribute(executor(), assetNamespace, downloadShareableId, subsetSelected);
            logWarning("[shareable: " + downloadShareableId + "] completed post-download processing");
        } catch (Throwable e) {
            log().add(PluginLog.ERROR,
                    "[shareable: " + downloadShareableId + "] failed post-download processing. Error: " + e.getMessage(),
                    e);
            throw e;
        }
    }

    static void notifyOfDownload(ServiceExecutor executor, XmlDoc.Element downloadShareable,
            Date downloadCompletionTime) throws Throwable {
        new DownloadNotification(downloadShareable, downloadCompletionTime, log()).send(executor);
    }

    private static void updateManifestAttribute(ServiceExecutor executor, String assetNamespace,
            String downloadShareableId, boolean subsetSelected) throws Throwable {
        String manifestAssetPath = Manifest.getManifestAssetPath(assetNamespace);
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", "path=" + manifestAssetPath);
        logWarning(
                "[shareable: " + downloadShareableId + "] updating attributes for manifest asset: " + manifestAssetPath);
        if (subsetSelected) {
            dm.add("name", Manifest.ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED_SUBSET);
        } else {
            dm.add("name", Manifest.ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED);
        }
        executor.execute(SvcFacilityShareableUploadManifestAttributeIncrement.SERVICE_NAME, dm.root());
        logWarning(
                "[shareable: " + downloadShareableId + "] updated attributes for manifest asset: " + manifestAssetPath);
    }

}
