package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

import java.util.Collection;

public class InteractionNotification {

    private final XmlDoc.Element interaction;
    private final XmlDoc.Element user;
    private final String uploadNS;
    private final String dstNamespace;
    private final boolean notifyFacility;
    private final boolean notifyUser;
    private final PluginLog log;

    public InteractionNotification(XmlDoc.Element interaction, XmlDoc.Element user, String uploadNS,
                                   String dstNamespace, boolean notifyFacility, boolean notifyUser, PluginLog log) {
        this.interaction = interaction;
        this.user = user;
        this.uploadNS = uploadNS;
        this.dstNamespace = dstNamespace;
        this.notifyFacility = notifyFacility;
        this.notifyUser = notifyUser;
        this.log = log;
    }

    public void send(ServiceExecutor executor) throws Throwable {
        if (this.notifyUser) {
            String userEmail = this.user.value("e-mail");
            if (userEmail != null) {
                notifyUser(executor, userEmail);
            }
        }
        if (this.notifyFacility) {
            notifyFacility(executor);
        }
    }

    private void notifyUser(ServiceExecutor executor, String userEmail) throws Throwable {
        String interactionId = this.interaction.value("@id");
        String facilityName = Interaction.getFacilityName(interaction);
        StringBuilder sb = new StringBuilder();
        sb.append("<p>Hello,</p>\n");
        sb.append("<p>");
        if (facilityName != null) {
            sb.append("<i>").append(facilityName).append("</i> data");
        } else {
            sb.append("Data");
        }
        sb.append(" has been copied to your Mediaflux project.");
        sb.append("<p>\n");

        String tableOpen = "<table style=\"border:1px solid black; border-collapse:collapse; width:85%;\">";
        String tdLeftOpen = "<td align=\"right\" style=\"width:90px; border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em;\">";
        String tdRightOpen = "<td style=\"border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em\">";

        sb.append(tableOpen).append("\n");
        if (facilityName != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Facility Name:</b></td>").append(tdRightOpen)
                    .append(facilityName).append("</td></tr>\n");
        }
        sb.append("<tr>").append(tdLeftOpen).append("<b>Copied From:</b></td>").append(tdRightOpen)
                .append(this.uploadNS).append("</td></tr>\n");
        sb.append("<tr>").append(tdLeftOpen).append("<b>To:</b></td>").append(tdRightOpen);
        sb.append(this.dstNamespace);
        sb.append("</td></tr>\n");
        sb.append("</table>\n");

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("to", userEmail);
        dm.add("subject", subject(interactionId));
        dm.add("body", new String[]{"type", "text/html"}, sb.toString());
        if (this.log != null) {
            this.log.add(PluginLog.WARNING,
                    "[interaction: " + interactionId + "] sending notification to user: " + userEmail);
        }
        executor.execute("mail.send", dm.root());
        if (this.log != null) {
            this.log.add(PluginLog.WARNING,
                    "[interaction: " + interactionId + "] sent notification to user: " + userEmail);
        }
    }

    private void notifyFacility(ServiceExecutor executor) throws Throwable {

        String interactionId = this.interaction.value("@id");
        Collection<String> facilityNotificationRecipients = Interaction.getFacilityNotificationRecipients(interaction);
        if (facilityNotificationRecipients == null || facilityNotificationRecipients.isEmpty()) {
            return;
        }

        String facilityName = Interaction.getFacilityName(interaction);
        String userFullName = this.user.value("name");
        String actorName = String.format("%s:%s", this.user.value("@domain"),
                this.user.stringValue("@user", this.user.value()));

        StringBuilder sb = new StringBuilder();
        sb.append("<p>Hello,</p>\n");
        sb.append("<p>");
        if (facilityName != null) {
            sb.append("<i>").append(facilityName).append("</i> data: ");
        } else {
            sb.append("Data: ");
        }
        sb.append("<b>").append(this.uploadNS).append("</b>");
        sb.append(" has been copied to ");
        sb.append("<b>").append(userFullName == null ? actorName : userFullName).append("'s</b> Mediaflux project.");
        sb.append("<p><br><br>\n");

        String tableOpen = "<table style=\"border:1px solid black; border-collapse:collapse; width:85%;\">";
        String tdLeftOpen = "<td align=\"right\" style=\"width:90px; border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em;\">";
        String tdRightOpen = "<td style=\"border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em\">";

        sb.append(tableOpen).append("\n");
        if (facilityName != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Facility Name:</b></td>").append(tdRightOpen)
                    .append(facilityName).append("</td></tr>\n");
        }
        sb.append("<tr>").append(tdLeftOpen).append("<b>Asset Namespace:</b></td>").append(tdRightOpen)
                .append(this.uploadNS).append("</td></tr>\n");
        sb.append("<tr>").append(tdLeftOpen).append("<b>Recipient User:</b></td>").append(tdRightOpen);
        if (userFullName != null) {
            sb.append(String.format("%s (%s)", userFullName, actorName));
        } else {
            sb.append(actorName);
        }
        sb.append("</td></tr>\n");
        sb.append("</table>\n");

        String recipients = String.join(",", facilityNotificationRecipients);
        XmlDocMaker dm = new XmlDocMaker("args");
        for (String recipient : facilityNotificationRecipients) {
            dm.add("to", recipient);
        }
        dm.add("subject", subject(interactionId));
        dm.add("body", new String[]{"type", "text/html"}, sb.toString());
        if (this.log != null) {
            this.log.add(PluginLog.WARNING,
                    "[interaction: " + interactionId + "] sending notification to facility operators: " + recipients);
        }
        executor.execute("mail.send", dm.root());
        if (this.log != null) {
            this.log.add(PluginLog.WARNING,
                    "[interaction: " + interactionId + "] sent notification to facility operators: " + recipients);
        }
    }

    private String subject(String interactionId) throws Throwable {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("User Interaction %s Completed", interactionId));
        String emailSubjectSuffix = Interaction.getFacilityNotificationEmailSubjectSuffix(this.interaction);
        if (emailSubjectSuffix != null) {
            sb.append(" ").append(emailSubjectSuffix);
        }
        return sb.toString();
    }
}
