package au.org.vicnode.mflux.services.shareable;

import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.LongType;
import arc.xml.XmlDoc.Element;
import au.org.vicnode.mflux.plugin.util.Properties;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcFacilityShareableUploadDescribe extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.describe";

	private Interface _defn;

	public SvcFacilityShareableUploadDescribe() {
		_defn = new Interface();
		_defn.add(new Interface.Element("id", LongType.POSITIVE, "The id of the shareable.", 0, 1));
		_defn.add(new Interface.Element("get-token", BooleanType.DEFAULT,
				"If set to true the token of the shareable will be displayed.  Defaults to false.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Describe facility upload shareables owned by the current user.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		String id = args.value("id");
		boolean getToken = args.booleanValue("get-token", false);

		if (id != null) {
			XmlDoc.Element se = Shareable.describe(executor(), id);
			if (!"upload".equals(se.value("@type"))) {
				throw new IllegalArgumentException("Asset shareable " + id + " is not of type upload.");
			}
			w.add(se);
			w.add("count", 1);
			return;
		}

		long count = 0;

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("size", "infinity");
		dm.add("type", "upload");
		dm.add("generate-url", true);
		dm.add("get-token", getToken);

		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element re = executor().execute("asset.shareable.describe", dm.root());
		List<XmlDoc.Element> ses = re.elements("shareable");
		if (ses != null) {
			for (XmlDoc.Element se : ses) {
				// check if it is facility upload
				if ("upload".equals(se.value("@type")) && se.elementExists(
						"completion-service[@name='" + SvcFacilityShareableUploadComplete.SERVICE_NAME + "']")) {
					w.add(se);
					count++;
				}
			}
		}
		w.add("count", count);

	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
