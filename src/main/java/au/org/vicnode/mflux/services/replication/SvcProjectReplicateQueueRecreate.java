/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.replication;


import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetProcessingQueue;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectReplicateQueueRecreate extends PluginService {


	private Interface _defn;

	public SvcProjectReplicateQueueRecreate()  throws Throwable {
		_defn = new Interface();
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "This service destroys and re-creates the replication processing queue used to make content copies.  Any queued entries just before the queue is destroyed will be re-queued. Any copy actually being processed.  This service is the same as the rebuild service except it does NOT unset and reset the queue on the project asset namespace.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.replicate.queue.recreate";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Check the DR environment is configured		
		if (!ProjectReplicate.isDRConfigured(executor())) {
			throw new Exception ("Replications are not configured on this system");
		}

		// Recreate
		recreate (executor(), w);
	}	


	private  void recreate (ServiceExecutor executor, XmlWriter w) throws Throwable {

		// In this approach we assert that the :enqueue element
		// set on project namespaces refers to the queue by name
		// not id.  So we don't actually need to unset the queue
		// from the namespace (like the rebuild service does)

		// Suspend the queue
		String qName = ProjectReplicate.createOrFindStandardQueue(executor, null);
		AssetProcessingQueue.suspendQueue(executor, qName);

	
		// Fetch existing  entries (executing and queued) as we will requeue them
		Collection<String> entries = AssetProcessingQueue.getEntryAssetIDs(executor, qName);

		// Now destroy  the asset processing queue
		AssetProcessingQueue.destroyQueue(executor, qName, false);
		w.push("destroy");
		w.add("queue", new String[] {"destroyed", "true"}, qName);
		w.pop();

		// Now rebuild (picking up application properties 
		// replication.queue.nb-processors
		w.push("recreate");
		qName = ProjectReplicate.createOrFindStandardQueue(executor, w);
		if (entries!=null && entries.size()>0) {
			w.push("entries");
			for (String entry : entries) {
				w.add("id", entry);
				AssetProcessingQueue.addAssetToQueue (executor, qName, entry);
			}
			w.pop();
		}
		w.pop();
	}
}
