package au.org.vicnode.mflux.services.dataregistry;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;
import unimelb.rcs.data.registry.DataRegistry;

public class SvcDataRegistryMediafluxCollectionDescribe extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "data.registry.mediaflux.collection.describe";

    private Interface _defn;

    public SvcDataRegistryMediafluxCollectionDescribe() {
        _defn = new Interface();
        _defn.add(new Interface.Element("code", StringType.DEFAULT, "Collection code", 1, 1));
        _defn.add(new Interface.Element("list-projects", BooleanType.DEFAULT,
                "List related Mediaflux projects. Defaults to true.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Describe the specified (data registry) collection (with Mediaflux product).";
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String code = args.value("code");
        boolean listProjects = args.booleanValue("list-projects", true);
        unimelb.rcs.data.registry.Collection collection = DataRegistry.get(executor()).getCollection(code);
        XmlDoc.Element projects = listProjects ? SvcDataRegistryMediafluxCollectionList.getProjects(code, executor())
                : null;
        collection.saveXml(w, projects);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
