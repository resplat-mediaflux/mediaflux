/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.atomic.AtomicOperation;
import arc.mf.plugin.atomic.AtomicTransaction;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;

public class SvcProjectUserAdd extends PluginService {

	private Interface _defn;
	private static Boolean ALLOW_PARTIAL_CID = true;

	public SvcProjectUserAdd() throws Throwable {
		_defn = new Interface();
		addProjectAddArgs(_defn, ALLOW_PARTIAL_CID);
	}

	public static void addProjectAddArgs(Interface _defn, boolean partial_cid) throws Throwable {
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain for the user.", 0, 1));
		_defn.add(
				new Interface.Element("user", StringType.DEFAULT, "A user name to grant access to the project.", 0, 1));
		_defn.add(new Interface.Element("role", StringType.DEFAULT, "A role to grant access to the project.", 0, 1));
		SvcProjectDescribe.addMandatoryProjectID(_defn, partial_cid);
		Interface.Element projectRole = new Interface.Element("project-role",
				new EnumType(Project.ProjectRoleType.projectRoleTypes()), "The role type in the project to grant.", 1,
				1);
		projectRole.add(new Interface.Attribute("replace", BooleanType.DEFAULT,
				"Replace the project role if the user already has access to the project. Defaults to false.", 0));
		_defn.add(projectRole);
		Interface.Element el = new Interface.Element("notify", BooleanType.DEFAULT,
				"Notify the user and the project admninistrator by email. Defaults to false.", 0, 1);
		el.add(new Interface.Attribute("user-only", BooleanType.DEFAULT,
				"Only notify the user not the admins (defaults to false).",0));
		_defn.add(el);
		//
		_defn.add(new Interface.Element("provision", BooleanType.DEFAULT,
				"Notify the standard provisioning user (that holds the admin role for all projects) as well (default false) .",
				0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Adds a user or role (a 'role user') to an extant project, created by vicnode.project.create. Also grants them the VicNode:standard-user role. Emails are sent to the user and all administrators of the Project if requested.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.user.add";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		final String domain = args.value("domain");
		final String user = args.value("user");
		final String roleUser = args.value("role");
		final String projectRole = args.value("project-role");
		final boolean replaceProjectRole = args.booleanValue("project-role/@replace", false);
		final String theProjectID = args.value("project-id");
		final XmlDoc.Element notify = args.element("notify");
		final Boolean dropProvision = !(args.booleanValue("provision", false));
		//
		if (roleUser == null && user == null) {
			throw new Exception("You must supply a user or a role user");
		}
		if (user != null && domain == null) {
			throw new Exception("You must supply a domain and a user.");
		}

		// Validate project
		String s[] = Project.validateProject(executor(), theProjectID, ALLOW_PARTIAL_CID);
		final String projectID = s[1];

		// Atomic creation so if fails, all partial resources are destroyed
		try {
			new AtomicTransaction(new AtomicOperation() {

				@Override
				public boolean execute(ServiceExecutor executor) throws Throwable {
					add(domain, user, roleUser, projectID, projectRole, 
							replaceProjectRole, notify, dropProvision, w);
					return false;
				}
			}).execute(executor());
		} catch (Throwable t) {

			// Rethrow
			throw new Exception(t);
		}

	}

	private void add(String domain, String user, String roleUserActorName, String projectID, 
			String projectRole, Boolean replaceProjectRole, XmlDoc.Element notify, 
			Boolean dropProvision, XmlWriter w) throws Throwable {

		ServiceExecutor executor = executor();

		// Check the project exists. Exception if not
		String t[] = Project.validateProject(executor, projectID, false);
		projectID = t[1];

		// Add the user to the project
		String userActorName = user != null ? domain + ":" + user : null;
		if (user != null) {

			if (replaceProjectRole) {
				revokeProjectRoles(executor, "user", userActorName, projectID);
			}
			Project.addUser(executor, projectID, userActorName, Project.ProjectRoleType.toEnum(projectRole));
			User.grantRevokeRole(executor, userActorName, Properties.getStandardUserRoleName(executor), true);
		}

		// Add the role to the project
		if (roleUserActorName != null) {
			if (replaceProjectRole) {
				revokeProjectRoles(executor, "role", roleUserActorName, projectID);
			}
			Project.addRole(executor, projectID, roleUserActorName, Project.ProjectRoleType.toEnum(projectRole));
		}

		// Send notifications 
		Boolean nf = false;
		Boolean userOnly = false;
		if (notify!=null) {
			nf = notify.booleanValue();
			userOnly = notify.booleanValue("@user-only", false);
		}
		if (nf) {
			String projectRoleName = Project.actualRoleName(projectID, Project.ProjectRoleType.toEnum(projectRole));
			Project.notifyUserAndAdminsOfRoleChanges(executor, projectID, 
					userActorName, roleUserActorName, projectRoleName,
                    dropProvision, true, true, userOnly, w);
		}

	}

	private static void revokeProjectRoles(ServiceExecutor executor, String actorType, String actorName,
			String projectId) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("type", actorType);
		dm.add("name", actorName);
		Collection<String> actorRoles = executor.execute("actor.describe", dm.root()).values("actor/role");
		int nbProjectRoles = 0;
		Collection<String> projectRoles = Project.actualUserRoleNames(projectId);
		if (actorRoles != null) {
			for (String actorRole : actorRoles) {
				if (actorRole != null && projectRoles.contains(actorRole)) {
					dm.add("role", new String[] { "type", "role" }, actorRole);
					nbProjectRoles++;
				}
			}
		}
		if (nbProjectRoles > 0) {
			executor.execute("actor.revoke", dm.root());
		}
	}
}
