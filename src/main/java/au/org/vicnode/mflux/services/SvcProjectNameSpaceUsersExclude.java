/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectNameSpaceUsersExclude extends PluginService {


	private Interface _defn;

	public SvcProjectNameSpaceUsersExclude() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT,
				"The ID of the project. Should be of " + "the form 'proj-<name>-<UUID.N.M>'.", 1,1));
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace of interest.  Must be a child of the given project root namespace.", 1, 1));
		_defn.add(new Interface.Element("project-role", new EnumType(Project.ProjectRoleType.projectRoleTypes()),
				"The user's role in the project.", 1, 1));	
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.namespace.users.exclude";
	}

	public String description() {
		return "Service to grant exclusion ACLs on the given namespace for every user who holds the given project role.  Inheritance on the namespace is set to true.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String projectID = args.value("project-id");
		String namespace = args.value("namespace");
		String roleType = args.value("project-role");

		// Validate project ID
		String t[] = Project.validateProject(executor(), projectID, false);
		projectID = t[1];

		// CHeck the namespace is part of the project
		ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
		String projectRoot = ph.nameSpace();
		int idx = namespace.indexOf(projectRoot);
		if (idx!=0) {
			throw new Exception("The given namespace is not a child namespace of the given project.");
		}
		// We disallow /projects/<proj-id>/  
		// The user must give at least
		// /projects/<proj-id>/<name>
		if (namespace.length() <= (projectRoot.length()+1)) {
			throw new Exception("The given namespace is not a child namespace of the given project.");
		}

		// Get users
		Project.ProjectRoleType rt = Project.ProjectRoleType.toEnum(roleType);
		Collection<String> users = Project.listUsers(executor(), ph.id(), rt, true);
		if (users.size()>0) {
			for (String user : users) {
				w.add("user", user);
				// grant the exclusion ACL. We set inherit to true meaning
				// that the access to the namespace is determined by the parent 
				// ACLs and those on this namespace.
				exclude (executor(), namespace, user, "true",w);
			}
		}
	}

	private void exclude (ServiceExecutor executor, String nameSpace, 
			String user, String inherit, XmlWriter w) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace",  nameSpace);
		dm.push("acl", new String[] {"type", "exclusive"});
		dm.add("actor", new String[] {"type", "user"}, user);
		dm.push("access");
		//
		dm.add("asset", "create");
		dm.add("asset", "access");					
		dm.add("asset", "modify");
		dm.add("asset", "destroy");
		dm.add("asset", "licence");
		dm.add("asset", "publish");
		//		
		dm.add("asset-content", "access");
		dm.add("asset-content", "modify");
		//
		dm.add("namespace", "access");
		dm.add("namespace", "administer");
		dm.add("namespace", "create");
		dm.add("namespace", "modify");
		dm.add("namespace", "destroy");
		dm.add("namespace", "execute");	
		dm.pop();
		dm.pop();
		executor.execute("asset.namespace.acl.grant", dm.root());
		//
		if (inherit!=null) {
			dm = new XmlDocMaker("args");
			dm.add("namespace",  nameSpace);
			dm.add("inherit", inherit);
			executor.execute("asset.namespace.acl.inherit.set", dm.root());
		}
	}
}
