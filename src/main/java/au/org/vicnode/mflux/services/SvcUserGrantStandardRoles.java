/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;



import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcUserGrantStandardRoles extends PluginService {


	private Interface _defn;

	public SvcUserGrantStandardRoles()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain for the user.", 1, 1));
		_defn.add(new Interface.Element("user", StringType.DEFAULT, "The user name.", 1, 1));

	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Grant a user the standard role (e.g. ds:standard-user) that provides the user basic access to system resources (but not specific projects).";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "user.grant.standard-roles";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String domain = args.value("domain");
		String user = args.value("user");

		XmlDocMaker dm = new XmlDocMaker("args");
		String name = domain+":"+user;
		dm.add("name", name);
		w.add("user", name);

		Boolean isAdmin = false;
		SvcUserCreate.addStandardRoles(executor(), dm, isAdmin);
		dm.add("type", "user");
		executor().execute("actor.grant", dm.root());
	}
}
