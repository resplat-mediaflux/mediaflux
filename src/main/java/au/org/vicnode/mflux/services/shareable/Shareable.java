package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class Shareable {

    public static final String PROPERTY_FACILITY_NOTIFICATION_FROM = "facility.notification.from";

    public static final String PROPERTY_FACILITY_NOTIFICATION_TO = "facility.notification.to";

    public static final String PROPERTY_FACILITY_NAME = "facility.name";

    public static final String PROPERTY_FACILITY_EMAIL_DICT_ADD_ENTRY = "facility.email.dict.add.entry";

    public static final String PROPERTY_FACILITY_SHAREABLE_ID = "facility.shareable.id";

    public static final String PROPERTY_FACILITY_UPLOAD_ID = "facility.upload.id";

    public static final String PROPERTY_FACILITY_UPLOAD_NAME_SUFFIX = "facility.upload.name.suffix";

    public static final String PROPERTY_FACILITY_NOTIFICATION_EMAIL_SUBJECT_SUFFIX = "facility.notification.email.subject.suffix";


    public static final String ARG_NAME_SUFFIX = "name-suffix";

    public static final String ARG_KEYWORDS = "keywords";

    public static final String ARG_DATA_NOTE = "data-note";

    public static final String ARG_SHARE_WITH = "share-with";

    public static final String ARG_EMAIL_SUBJECT_SUFFIX = "email-subject-suffix";

    public static final String PATTERN_NAME_SUFFIX = "[\\w.-]+";

    public static final String PATTERN_KEYWORDS = "([\\w .-]+)(,\\s*[\\w .-]*)*";

    public static final int EMAIL_SUBJECT_SUFFIX_MAX_LENGTH = 32;

    public static final String PATTERN_SHAREABLE_NAME = "[\\w-. ]+";

    public static final String PATTERN_FACILITY_NAME = "[\\w-. ]+";

    public static final String SERVER_PROPERTY_ASSET_SHAREABLE_ADDRESS = "asset.shareable.address";

    public final String id;
    public final String token;
    public final String url;
    public final String namespace;
    public final Date validFrom;
    public final Date validTo;

    Shareable(XmlDoc.Element shareable) throws Throwable {
        this.id = shareable.value("@id");
        this.token = shareable.value("token");
        this.namespace = shareable.value("namespace");
        this.validFrom = shareable.dateValue("valid-from", null);
        this.validTo = shareable.dateValue("valid-to", null);
        this.url = shareable.value("url");
    }

    static XmlDoc.Element describe(ServiceExecutor executor, String shareableId) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", shareableId);
        dm.add("get-token", true);
        dm.add("generate-url", true);
        return executor.execute("asset.shareable.describe", dm.root()).element("shareable");
    }

    static XmlDoc.Element describe(String shareableToken, ServiceExecutor executor) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("token", shareableToken);
        dm.add("get-token", true);
        dm.add("generate-url", true);
        return executor.execute("asset.shareable.describe", dm.root()).element("shareable");
    }

    public static String getAssetShareableAddress(ServiceExecutor executor) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("name", SERVER_PROPERTY_ASSET_SHAREABLE_ADDRESS);
        String hostAddr = executor.execute("server.property.get", dm.root()).value("property");
        if (hostAddr == null) {
            throw new Exception("Server property: " + SERVER_PROPERTY_ASSET_SHAREABLE_ADDRESS + " is not set.");
        }
        return hostAddr;
    }

    public static String getServerHostAddress(ServiceExecutor executor) throws Throwable {
        // remove path section
        return getAssetShareableAddress(executor).replaceAll("/+ *$", "");
    }

    static String getProperty(XmlDoc.Element shareable, String name) throws Throwable {
        return shareable.stringValue("properties/property[@key='" + name + "']",
                shareable.value("properties/property[@name='" + name + "']"));
    }

    static String getFacilityName(XmlDoc.Element shareable) throws Throwable {
        return getProperty(shareable, PROPERTY_FACILITY_NAME);
    }

    static String getFacilityNotificationFrom(XmlDoc.Element shareable) throws Throwable {
        return getProperty(shareable, PROPERTY_FACILITY_NOTIFICATION_FROM);
    }

    static String getFacilityNotificationTo(XmlDoc.Element shareable) throws Throwable {
        return getProperty(shareable, PROPERTY_FACILITY_NOTIFICATION_TO);
    }

    static Collection<String> getFacilityNotificationRecipients(XmlDoc.Element shareable) throws Throwable {
        String notificationTo = getFacilityNotificationTo(shareable);
        return Emails.parse(notificationTo);
    }

    static String getFacilityUploadNameSuffix(XmlDoc.Element shareable) throws Throwable {
        return getProperty(shareable, PROPERTY_FACILITY_UPLOAD_NAME_SUFFIX);
    }

}
