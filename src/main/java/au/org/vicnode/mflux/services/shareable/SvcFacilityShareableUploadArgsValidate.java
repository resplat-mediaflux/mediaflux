package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;
import unimelb.utils.DNSUtils;

import java.util.Collection;

public class SvcFacilityShareableUploadArgsValidate extends PluginService {

	public static class Validity {
		public final String argName;
		public final boolean valid;
		public final String reasonForIssue;
		public final String suggestedValue;

		public Validity(String argName, boolean valid, String reasonForIssue, String suggestedValue) {
			this.argName = argName;
			this.valid = valid;
			this.reasonForIssue = reasonForIssue;
			this.suggestedValue = suggestedValue;
		}
	}

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.args.validate";

	private final Interface _defn;

	public SvcFacilityShareableUploadArgsValidate() {
		_defn = new Interface();
		_defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "Shareable id.", 1, 1));

		Interface.Element args = new Interface.Element("args", XmlDocType.DEFAULT, "Shareable args.", 0, 1);
		_defn.add(new Interface.Element(Shareable.ARG_NAME_SUFFIX, StringType.DEFAULT,
				"Suffix of upload namespace name.", 0, 1));
		_defn.add(new Interface.Element(Shareable.ARG_KEYWORDS, StringType.DEFAULT, "Keywords separated by comma.", 0,
				1));
		_defn.add(new Interface.Element(Shareable.ARG_SHARE_WITH, StringType.DEFAULT,
				"The recipient email addresses separated by comma or semicolon.", 0, 1));
		_defn.add(args);
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Validate shareable args.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		// TODO debug
//        System.out.println(SERVICE_NAME + ": " + args);
		String shareableId = args.value("shareable-id");
		String nameSuffix = args.value("args/" + Shareable.ARG_NAME_SUFFIX);
		if (nameSuffix != null) {
			Validity v = validateNameSuffix(nameSuffix);
			w.add("valid", new String[] { "arg", Shareable.ARG_NAME_SUFFIX, "message", v.reasonForIssue }, v.valid);
		} else {
			// the argument is optional. It's OK if it's not specified.
			w.add("valid", new String[] { "arg", Shareable.ARG_NAME_SUFFIX }, true);
		}

		if (args.count("args/" + Shareable.ARG_KEYWORDS) > 1) {
			Collection<String> keywords = args.values("args/" + Shareable.ARG_KEYWORDS);
			XmlDoc.Element se = Shareable.describe(executor(), shareableId);
			Collection<String> enumKeywords = se.values("args/definition/element[@name='keywords']/restriction/value");
			boolean hasInvalidKeyword = false;
			for (String keyword : keywords) {
				if (enumKeywords != null && !enumKeywords.contains(keyword)) {
					w.add("valid",
							new String[] { "arg", Shareable.ARG_KEYWORDS, "message", "Unknown keyword: " + keyword },
							false);
					hasInvalidKeyword = true;
					break;
				}
			}
			if (!hasInvalidKeyword) {
				w.add("valid", new String[] { "arg", Shareable.ARG_KEYWORDS }, true);
			}
		} else {
			String keywords = args.value("args/" + Shareable.ARG_KEYWORDS);
			if (keywords != null) {
				Validity v = validateCommaSeparatedKeywords(keywords);
				w.add("valid", new String[] { "arg", Shareable.ARG_KEYWORDS, "message", v.reasonForIssue }, v.valid);
			} else {
				// the argument is optional. It's OK if it's not specified.
				w.add("valid", new String[] { "arg", Shareable.ARG_KEYWORDS }, true);
			}
		}

		// String shareableId = args.value("shareable-id");
		String shareWith = args.value("args/" + Shareable.ARG_SHARE_WITH);
		if (shareWith != null) {
			Validity v = validateShareWithEmails(shareWith);
			w.add("valid", new String[] { "arg", Shareable.ARG_SHARE_WITH, "message", v.reasonForIssue }, v.valid);
		} else {
			// the argument is optional. It's OK if it's not specified.
			w.add("valid", new String[] { "arg", Shareable.ARG_SHARE_WITH }, true);
		}
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	private static Validity validateNameSuffix(String nameSuffix) {
		if (nameSuffix == null || nameSuffix.trim().isEmpty()) {
			return new Validity(Shareable.ARG_NAME_SUFFIX, true, null, null);
		}
		boolean valid = nameSuffix.matches(Shareable.PATTERN_NAME_SUFFIX);
		return new Validity(Shareable.ARG_NAME_SUFFIX, valid,
				valid ? null : "Invalid name. It should not contain any blank space or special characters.", null);
	}

	private static Validity validateCommaSeparatedKeywords(String keywords) {
		if (keywords == null || keywords.trim().isEmpty()) {
			return new Validity(Shareable.ARG_KEYWORDS, true, null, null);
		}
		boolean valid = keywords.matches(Shareable.PATTERN_KEYWORDS);
		return new Validity(Shareable.ARG_KEYWORDS, valid, valid ? null : "Invalid keyword.", null);
	}

	private static Validity validateShareWithEmails(String shareWith) {
		if (shareWith == null || shareWith.trim().isEmpty()) {
			// It is valid because the argument is optional.
			return new Validity(Shareable.ARG_SHARE_WITH, true, null, null);
		}

		Collection<String> emails = Emails.parse(shareWith);
		if (emails == null || emails.isEmpty()) {
			String reason = String.format("Failed to parse email addresses from: %s", shareWith);
			return new Validity(Shareable.ARG_SHARE_WITH, false, reason, null);
		}
		int nbEmails = 0;
		for (String email : emails) {
			if (!email.isEmpty()) {
				if (!arc.dtype.EmailAddressType.isValidAddress(email) || !email.matches("^([^@]+)@([^@]+)$") || email.endsWith(".")) {
					String reason = String.format("Invalid email address: %s", email);
					return new Validity(Shareable.ARG_SHARE_WITH, false, reason, null);
				}

				String domainName = email.substring(email.lastIndexOf('@') + 1);
				if (!DNSUtils.isMailDomain(domainName)) {
					String reason = String.format("Invalid or unreachable email domain: %s", domainName);
					return new Validity(Shareable.ARG_SHARE_WITH, false, reason, null);
				}
				nbEmails++;
			}
		}
		if (nbEmails == 0) {
			String reason = String.format("Failed to parse email addresses from: %s", shareWith);
			return new Validity(Shareable.ARG_SHARE_WITH, false, reason, null);
		}
		return new Validity(Shareable.ARG_SHARE_WITH, true, null, null);
	}

}
