/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.datamover;


import java.io.File;
import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.UrlType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;
import au.org.vicnode.mflux.plugin.util.Util;
import au.org.vicnode.mflux.services.SvcProjectDescribe;

public class SvcProjectShareableFind extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.shareable.find";

	private Interface _defn;
	private static Boolean ALLOW_PARTIAL_CID = true;

	public SvcProjectShareableFind() throws Throwable {
		_defn = new Interface();

		SvcProjectDescribe.addMandatoryProjectID (_defn, ALLOW_PARTIAL_CID);
		_defn.add(new Interface.Element("disable", BooleanType.DEFAULT, "As well as finding the shareables, disable them as well. Default is false. Use with care.", 0, 1));
		_defn.add(new Interface.Element("type", new EnumType(new String[] {"upload", "download"}), "If specified, restricts the shareable type, ",  0, 1));
		_defn.add(new Interface.Element("url", UrlType.DEFAULT, "If specified, file on the server to save the output as an XML file. Of the form file:<path>", 0, 1));
		_defn.add(new Interface.Element("self-only", BooleanType.DEFAULT, "When listing all shareables, restrict to the calling user, instead of all users. Default false.", 0, 1));
	}



	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Administration service to find all asset shareables associated with this project and save the description of each shareable in an XML file.  The service can also optionally disable each found shareable.";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {
		return false;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		Boolean useCaller = args.booleanValue("self-only", false);
		String projectID = args.stringValue("project-id");
		String t[] = Project.validateProject(executor(), projectID, ALLOW_PARTIAL_CID);
		projectID = t[1];
		ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
		//
		Boolean disable = args.booleanValue("disable", false);
		String type = args.value("type");
		String url = args.value("url");

		// Find all shareables, up and down, for this project
		Collection<XmlDoc.Element> shareables = findShareables (executor(), useCaller, ph, type, disable, w);
		
		// Write output files
		XmlDocMaker dm = new XmlDocMaker("project");
		if (outputs != null || url!=null) {
			dm.add("id", projectID);
			dm.add("namespace", ph.nameSpace());
			dm.push("shareables");
			if (shareables.size()>0) {
				dm.addAll(shareables);
			}
			dm.pop();
		}
		
		// Still writes even if no actual shareables
		if (outputs!=null) {
			Util.writeClientSideXMLFile(outputs.output(0), dm.root());
		}
		
		// Write server side XML file
		if (url!=null) {
			String urlPath = url.substring(5);   // Get rid of file:
			File outputFile = new File(urlPath);
			Util.writeServerSideXMLFile(outputFile, dm.root());
		}
	}

	private static Collection<XmlDoc.Element> findShareables (ServiceExecutor executor, 
			Boolean useCaller, ProjectHolder ph, String type, Boolean disable, XmlWriter w) throws Throwable {
		Vector<XmlDoc.Element> res = new Vector<XmlDoc.Element>();
		XmlDocMaker dm = new XmlDocMaker("args");
		if (useCaller) {
			XmlDoc.Element self = User.describeUser(executor);
			dm.add("user", self.value("user/@domain") + ":" + self.value("user/@user"));
		}
		dm.add("size", "infinity");
		XmlDoc.Element r = executor.execute("asset.shareable.all.list", dm.root());
		if (r==null) {
			return res;
		}
		Integer n = 0;	
		Collection<String> shIDs = r.values("shareable");
		if (shIDs!=null) {
			for (String shID : shIDs) {
				XmlDoc.Element sh = describeShareable (executor, shID);
				String namespace = sh.value("shareable/namespace");
				if (type==null || (type!=null && type.equals(sh.value("shareable/@type")))) {
					if (namespace!=null && namespace.contains(ph.nameSpace())) {
						if (disable) {
							disableShareable (executor, shID);
						}
						// Check if the shareable is valid
						Boolean missing = false;
						if (!NameSpaceUtil.assetNameSpaceExists(executor, null, namespace)) {
							XmlDoc.Element t = new XmlDoc.Element ("missing", "true");
							sh.add(t);
							missing = true;
						} else {
							XmlDoc.Element t = new XmlDoc.Element ("missing", "false");
							sh.add(t);
						}
						res.add(sh.element("shareable"));
						w.add("shareable", new String[] {"type", sh.value("shareable/@type"), "namespace", namespace, 
								"enabled", sh.value("shareable/enabled"), "missing", missing.toString()}, shID);
						n++;
					}
				}
			}
		}
		w.add("count", n);

		return res;

	}

	private static XmlDoc.Element describeShareable (ServiceExecutor executor, String id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		return executor.execute("asset.shareable.describe", dm.root());
	}

	private static void disableShareable (ServiceExecutor executor, String id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		executor.execute("asset.shareable.disable", dm.root());
	}
}
