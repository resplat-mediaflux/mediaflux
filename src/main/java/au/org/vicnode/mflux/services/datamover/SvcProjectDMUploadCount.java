/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.datamover;


import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.services.datamover.Utils;
import au.org.vicnode.mflux.services.SvcProjectDescribe;

public class SvcProjectDMUploadCount extends PluginService {

	private Interface _defn;
	private Boolean allowPartialCID = true;

	public SvcProjectDMUploadCount()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectIDs(_defn, allowPartialCID);
		_defn.add(new Interface.Element("where", StringType.DEFAULT, 
				"The query to find manifest assets.", 1, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Analyses DM Instrument uploaded manifest assets and finds the number of distinct instruments (and total number of uploads for all instruments) that uploaded data in the given query.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.DM.upload.count";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Parse inputs
		String where = args.stringValue("where");
		
		// Find manifest assets
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", where);
		dm.add("action", "get-distinct-values");
		dm.add("xpath", Utils.MANIFEST_DOCTYPE+"/shareable/@id");
		dm.add("size", "infinity");
		XmlDoc.Element r = executor().execute("asset.query", dm.root());

		// Parse results. The :value element tell us how many
		// times each shareable has been uploaded.
		//       :value -nbe "2" "2296"
		//       -nbe =  no. uploads for this shareable  
		Collection<XmlDoc.Element> values = r.elements("value");
		Integer nInstruments = 0;
		Integer nTotalUploads = 0;
		if (values!=null) {
			nInstruments = values.size();
			for (XmlDoc.Element value : values) {
				// This is the number of uploads for this shareable (insrtument)
			   Integer n = value.intValue("@nbe");
			   nTotalUploads += n;
			}
		}
		w.add("n-instrument-uploads", nInstruments);
		w.add("n-total-uploads", nTotalUploads);
	}
}




