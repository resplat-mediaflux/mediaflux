/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginService.Interface.Attribute;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.PasswordType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.UrlType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectContentCopy;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;
import unimelb.rcs.ResPlatStorageReportUtils;
import unimelb.rcs.data.registry.StorageProduct;

public class SvcProjectDescribe extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.describe";

	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectDescribe() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("date", StringType.DEFAULT,
				"A date/time to apply to all asset queries (counting and summing will only find data inclusively before this date) and to inject into the CSV file for when the service was run. If not supplied, today's date is used.", 0,
				1));
		_defn.add(new Interface.Element("namespace-path", StringType.DEFAULT, "Additional namespaces (which are not part of the projects structure such as the /avarchives) which should be checked. Parameters prod-type and rep-type are irrelevant to this parameter. Defaults to none.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("posix", BooleanType.DEFAULT,
				"Show posix mount point information (default false).",
				0, 1));
		_defn.add(new Interface.Element("meta", BooleanType.DEFAULT,
				"Show all meta-data on the project namespace (default false).",
				0, 1));
		//
		Interface.Element me = new Interface.Element("users", BooleanType.DEFAULT,
				"Show information about the users (default true).", 0, 1);
		Interface.Attribute att = new Interface.Attribute("include-disabled", BooleanType.DEFAULT, "Include disabled accounts (default false).", 0);
		me.add(att);
		_defn.add(me);
		//
		_defn.add(new Interface.Element("identity", BooleanType.DEFAULT,
				"Show information about secure identity tokens (defaults to false) that hold a role for this project.",
				0, 1));
		_defn.add(new Interface.Element("sum", BooleanType.DEFAULT,
				"Sum project content on primary and DR systems (defaults to false). By default, the namespace report includes the used amount on the primary only. Using this option forces a resum on both primary and DR (slow).",
				0, 1));
		_defn.add(new Interface.Element("email", StringType.DEFAULT, "Send the report to this email address.", 0,
				Integer.MAX_VALUE));
		_defn.add(new Interface.Element("store", StringType.DEFAULT,
				"If supplied, limit the summaries to only projects that use this store (defaults to all stores).", 0,
				1));
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",
				0, 1));
		_defn.add(new Interface.Element("parent", StringType.DEFAULT,
				"The parent namespace to consider when looking for projects. Should start with a '/'. Defaults to all parents.",
				0, 1));


		_defn.add(new Interface.Element("storage-product", 
				new EnumType(StorageProduct.typeNames("Mediaflux")),
				"Storage product type to be included in the reports. Select from 'Market.Melbourne.Mediaflux' (default) and 'Mediaflux.Attica'.",
				0, 1));	

		me = new Interface.Element("csv", XmlDocType.DEFAULT,
				"Creates a CSV with selected UniMelb reporting and sends to email address.", 0, 1);
		me.add(new Interface.Element("email", StringType.DEFAULT, "email address", 0, Integer.MAX_VALUE));
		me.add(new Interface.Element("save-on-server", BooleanType.DEFAULT, "If true (default false), will save a file server side named <date>.csv. Will not overwrite.  You must clean up afterwards manually.", 0, 1));

		/*
		 * send csv report to remote HTTP server (POST)
		 */
		Interface.Element httpPost = new Interface.Element("post", XmlDocType.DEFAULT,
				"Send CSV report to remote HTTP server using HTTP POST.", 0, 1);

		httpPost.add(new Interface.Element("ssl", BooleanType.DEFAULT,
				"Is the remote server has SSL(HTTPS) enabled? Defaults to true.", 0, 1));
		httpPost.add(new Interface.Element("host", StringType.DEFAULT, "Remote HTTP server host", 1, 1));
		httpPost.add(new Interface.Element("username", StringType.DEFAULT,
				"Username to access the remote HTTP server. If token is supplied, username and password are ignored. Otherwise, username and password are used to retrieve a auth token from remote HTTP server.",
				0, 1));
		httpPost.add(new Interface.Element("password", PasswordType.DEFAULT,
				"Password to access the remote HTTP server. If token is supplied, username and password are ignored. Otherwise, username and password are used to retrieve a auth token from remote HTTP server.",
				0, 1));
		httpPost.add(new Interface.Element("token", PasswordType.DEFAULT,
				"Auth token to access the remote HTTP server. If token is supplied, username and password are ignored. Otherwise, username and password are used to retrieve a auth token from remote HTTP server.",
				0, 1));
		me.add(httpPost);
		_defn.add(me);
		//
		_defn.add(new Interface.Element("sort", BooleanType.DEFAULT,
				"Sort projects by name (the default is to sort by CID).", 0, 1));
		_defn.add(new Interface.Element("DR", BooleanType.DEFAULT,
				"As well as listing replication state (from namespace meta-data), attempt to discover (default true) information from the Disaster Recovery server. Defaults to false.", 0, 1));
		_defn.add(new Interface.Element("multi-version", BooleanType.DEFAULT,
				"Count and sum the content of assets with multiple versions and with one version only (default false).", 0, 1));
		_defn.add(new Interface.Element("debug", BooleanType.DEFAULT,
				"Print information to server log (default false).", 0, 1));
		_defn.add(new Interface.Element("name-validation", BooleanType.DEFAULT,
				"Include a listing of assets (could be a lot!) that are found to have name validation cross-operating system inconsistency issues.  Defaults to false.", 0, 1));

		//
		addProjectID (_defn, allowPartialCID);
		addProdType (_defn);
		addRepType (_defn);
		addOwner (_defn);

		_defn.add(new Interface.Element("url", UrlType.DEFAULT, "If specified, file on the server to save the output as XML.   Of the form file:<path>", 0, 1));


	}

	static public void addProdType (Interface defn) {
		defn.add(new Interface.Element("prod-type", new EnumType(Properties.projectTypes),
				"Select from 'all', (default; all types), 'production' (only production projects), 'test' (only test projects), 'closed-stub' (only closed-stub projects).",
				0, 1));
	}

	static public void addRepType (Interface defn) {
		defn.add(new Interface.Element("rep-type", new EnumType(Properties.repTypes),
				"Select from 'all', (default, all projects), 'replication' (only projects for which the namespace meta-data indicates should be replicated), 'no-replication' (projects for which the namespace meta-data indicates should not be replicated).",
				0, 1));	}

	static public void addMandatoryProjectID (Interface defn, Boolean allowPartialCID) throws Throwable {
		if (allowPartialCID) {
			defn.add(new Interface.Element("project-id", StringType.DEFAULT,
					"The ID of the project. Should be of " + "the form 'proj-<name>-<UUID.N.M>' or just M - that is, you can enter just the child part of the CID of the project.",
					1, 1));
		} else {
			defn.add(new Interface.Element("project-id", StringType.DEFAULT,
					"The ID of the project. Should be of " + "the form 'proj-<name>-<UUID.N.M>'.", 
					1,1));

		}
	}
	static public void addProjectID (Interface defn, Boolean allowPartialCID) throws Throwable {
		if (allowPartialCID) {
			defn.add(new Interface.Element("project-id", StringType.DEFAULT,
					"The ID of the project. Should be of " + "the form 'proj-<name>-<UUID.N.M>' or just M - that is, you can enter just the child part of the CID of the project. Defaults to all projects.", 0,
					1));
		} else {
			defn.add(new Interface.Element("project-id", StringType.DEFAULT,
					"The ID of the project. Should be of " + "the form 'proj-<name>-<UUID.N.M>'. Defaults to all projects.", 0,
					1));

		}
	}

	static public void addProjectIDs (Interface defn, Boolean allowPartialCID) throws Throwable {
		if (allowPartialCID) {
			defn.add(new Interface.Element("project-id", StringType.DEFAULT,
					"The ID of the project. Should be of " + "the form 'proj-<name>-<UUID.N.M>' or just M - that is, you can enter just the child part of the CID of the project. Defaults to all projects.", 0,
					Integer.MAX_VALUE));
		} else {
			defn.add(new Interface.Element("project-id", StringType.DEFAULT,
					"The ID of the project. Should be of " + "the form 'proj-<name>-<UUID.N.M>'. Defaults to all projects.", 0,
					Integer.MAX_VALUE));
		}
	}

	static public void addMandatoryProjectIDs (Interface defn, Boolean allowPartialCID) throws Throwable {
		if (allowPartialCID) {
			defn.add(new Interface.Element("project-id", StringType.DEFAULT,
					"The ID of the project. Should be of " + "the form 'proj-<name>-<UUID.N.M>' or just M - that is, you can enter just the child part of the CID of the project.", 1,
					Integer.MAX_VALUE));
		} else {
			defn.add(new Interface.Element("project-id", StringType.DEFAULT,
					"The ID of the project. Should be of " + "the form 'proj-<name>-<UUID.N.M>'.", 1,
					Integer.MAX_VALUE));
		}
	}

	// Partial IDs allowed
	static public void addIgnoreProjectIDs (Interface defn) throws Throwable {
		defn.add(new Interface.Element("ignore", StringType.DEFAULT, 
				"A list of project IDs to ignore. Should be of the form 'proj-<name>-<UUID.N.M>' or just M - that is, you can enter just the child part of the CID of the project.  Defaults to none.", 0, Integer.MAX_VALUE));
	}

	static public void addOwner (Interface defn) throws Throwable {
		Interface.Element me = new Interface.Element("owner-email", StringType.DEFAULT,
				"Specifies, by email, the owner (Research Computing Activity Owner [RCAO]) of the collection.", 0, 1);
		defn.add(me);
	}


	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns desciptions of all or specified projects.  The report can be 1) sent as email, 2) saved as XML in a client side file.    3) converted to CSV and send to an email address. You can abort the service as it accumulates information. The check point is between projects.";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		Collection<String> namespacePaths = args.values("namespace-path");
		Boolean multiVersion = args.booleanValue("multi-version", false);
		Boolean showPosix = args.booleanValue("posix", false);
		Boolean showMeta = args.booleanValue("meta", false);
		Boolean doDR = args.booleanValue("DR", false);
		Boolean doSum = args.booleanValue("sum", false);
		Boolean sortByName = args.booleanValue("sort", false);
		XmlDoc.Element showUsers = args.element("users");
		Boolean showIdentity = args.booleanValue("identity", false);
		Boolean debug = args.booleanValue("debug", false);
		Collection<String> emails = args.values("email");
		String repType = args.stringValue("rep-type", Properties.repTypes[0]);
		String projType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		String store = args.value("store");
		String keyString = args.value("contains");
		if (keyString != null) {
			keyString = keyString.toUpperCase();
		}
		String parent = args.value("parent");
		Date dateDate = args.dateValue("date");    // CHeck legitimate
		// Server-side URL
		String url = args.value("url");
		String ownerEmail = args.value("owner-email");

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID != null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else {
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), !sortByName, true));
		}
		if (projectIDs.size() == 0) {
			return;
		}
		String storageProduct = args.stringValue("storage-product", "Market.Melbourne.Mediaflux");
		XmlDoc.Element csv = args.element("csv");

		// TOday's date for CSV file and sums. dd-MMM-YYYY if not supplied
		String date = null;
		if (dateDate==null) {
			date = DateUtil.todaysDate(2);
		} else {
			date = DateUtil.formatDate(dateDate, false, false);
		}

		// Iterate  over projects and summarise
		Vector<String[]> csvRows = new Vector<String[]>();
		XmlDocMaker dm = new XmlDocMaker("projects");
		for (String projectID : projectIDs) {
			if (debug) {
				System.out.println("Project = " + projectID);
			}
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);

			// FInd out if the project is replicated.
			String qName = ProjectReplicate.isProjectReplicated(ph);	

			// See if we want to process this one
			if (Project.keepOnRepType (executor(), qName, repType) && 
					Project.keepOnProjectType(ph, projType) && 
					Project.keepOnKeyString(ph.id(), keyString) &&
					Project.keepOnParent(ph, parent) &&
					Project.keepOnOwner(ph,ownerEmail)) {

				String[] row = summariseProject (executor(), ph, showPosix, 
						showMeta, showUsers, doSum, date, store, doDR, 
						multiVersion, showIdentity, storageProduct, dm);
				if (row != null) {
					csvRows.add(row);
				}
			}
			PluginTask.checkIfThreadTaskAborted();
		}

		// Iterate over additional namespaces. 
		if (namespacePaths!=null) {
			for (String namespacePath :namespacePaths) {
				String[] row = summariseNameSpace (executor(), namespacePath, doSum, 
						date, store, multiVersion, storageProduct, dm);
				if (row != null)
					csvRows.add(row);
			}
		}


		// Convert DocMaker to a Writer for aterm return
		w.add(dm.root(), false);

		// Write client side XML file
		if (outputs != null) {
			Util.writeClientSideXMLFile(outputs.output(0), dm.root());
		}

		// Write server side XML file
		if (url != null) {
			File outputFile = new File(url.substring(5)); // Get rid of file:
			Util.writeServerSideXMLFile(outputFile, dm.root());
		}

		if (emails != null) {
			String msg = XmlPrintStream.outputToString(dm.root());
			String subject = new String("Mediaflux Project Summaries");
			String from = Util.getServerProperty(executor(), "notification.from");
			MailHandler.sendMessage(executor(), emails, subject, msg, null, false, from, w);
		}

		if (csv!=null && (csv.elementExists("email") || csv.elementExists("post") ||
				csv.elementExists("save-on-server"))) {
			w.push("csv");

			// Create temporary server-side CSV File with reporting results
			File tempFile = PluginService.createTemporaryFile(".csv");
			try {
				String[] colHeaders = { "Extraction Date", "Collection Code Authority", "Collection Code",
						"Collection Name", "Product", "Mediaflux ID", "Allocated Storage (GB)", "Used Storage (GB)", "Owners"};
				Util.writeCSVFile(tempFile, colHeaders, csvRows);

				Date currentTime = new Date();
				String uuid = executor().execute("server.uuid").value("uuid");

				String reportCSVFileName = "ResPlat-Mediaflux-Storage-Report-" + uuid + "-"
						+ new SimpleDateFormat("yyyyMMddHHmmss").format(currentTime) + ".csv";

				String dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime);

				Collection<String> csvEmails = csv.values("email");
				if (csvEmails != null) {
					// Email file
					for (String csvEmail : csvEmails) {
						w.add("email", csvEmail);
						MailHandler.sendMessage(executor(), csvEmail,
								"Mediaflux Project Summary Report [" + dateTime + "]", "Report attached", null, false,
								tempFile, reportCSVFileName, "text/csv", null);
					}
				}

				//
				if (csv.booleanValue("save-on-server",false)) {
					File copyFile = new File("/tmp/"+date+".csv");
					if (!copyFile.exists()) {
						Files.copy(tempFile.toPath(), copyFile.toPath());
						w.add("file", new String[] {"status", "created"}, copyFile.toPath());
					} else {
						w.add("file", new String[] {"status", "exists"}, copyFile.toPath());
					}
				}

				XmlDoc.Element post = csv.element("post");
				if (post != null) {
					boolean ssl = post.booleanValue("ssl", true);
					String host = post.value("host");
					String username = post.value("username");
					String password = post.value("password");
					String token = post.value("token");
					if (token == null && (username == null || password == null)) {
						throw new IllegalArgumentException(
								"Either csv/post/token or combination of csv/post/username and csv/post/password must be supplied.");
					}

					InputStream in = new BufferedInputStream(new FileInputStream(tempFile));
					try {
						if (token != null) {
							ResPlatStorageReportUtils.sendCSVReport(host, ssl, token, reportCSVFileName, in,
									tempFile.length());
						} else {
							ResPlatStorageReportUtils.sendCSVReport(host, ssl, username, password, reportCSVFileName,
									in, tempFile.length());
						}
						w.add("posted", host);
					} finally {
						in.close();
					}

					w.pop();


				}

				// Send CSV for collection reporting
				// Util.sendToSCP(executor(), "edward.hpc.unimelb.edu.au", null,
				// "nkilleen", "edward-scp",
				// "file:"+tempFile.getAbsolutePath(), null);
			} finally {
				// Clean up temporary file
				tempFile.delete();
			}
		}

	}

	private String[] summariseProject (ServiceExecutor executor, ProjectHolder ph, 
			Boolean showPosix, Boolean showMeta, XmlDoc.Element showUsers, Boolean doSum, String date,
			String store, Boolean doDR, Boolean multiVersion, Boolean showIdentity, 
			String storageProduct, XmlDocMaker w) throws Throwable {

		String projectDocType = Properties.getProjectDocType(executor);
		String collectionDocType = Properties.getCollectionDocType(executor);

		// Discover some stuff
		String assetNameSpace = ph.nameSpace();

		XmlDoc.Element nsMeta = ph.metaData();
		XmlDoc.Element nsAssetMeta = nsMeta.element("namespace/asset-meta");
		String primaryStore = nsMeta.value("namespace/store");
		String primaryStorePolicy = nsMeta.value("namespace/store-policy");
		Date ctime = nsMeta.dateValue("namespace/ctime");
		String ctimeS = DateUtil.formatDate(ctime, false, false);


		// Filter on store
		if (store != null && primaryStore != null) {
			if (!primaryStore.equals(store)) {
				return null;
			}
		}

		// Accumulate rows for CSV file
		String[] csvRow = new String[] {"","","","","","","","",""};
		csvRow[0] = date;
		csvRow[4] = storageProduct;
		// Format XML
		w.push("project");
		//
		String key = null;
		if (nsAssetMeta!=null) {
			key = nsAssetMeta.value(collectionDocType + "/key");
		}
		if (key!=null) {
			w.add("key", key);

			// Of the form <authority>:<id>
			// Split off the authority
			int idx = key.indexOf(":");
			if (idx <= 0) {
				csvRow[1] = "";
				csvRow[2] = key;
			} else {
				csvRow[1] = key.substring(0, idx);
				csvRow[2] = key.substring(idx + 1);
			}
		} else {
			w.add("key", "");
		}
		//
		w.add("id", ph.id());
		csvRow[5] = ph.id();
		//
		String prodType = Project.projectType(executor, ph.id());
		w.add("prod-type", prodType);
		//
		String name = null;	
		if (nsAssetMeta!=null) {
			name = nsAssetMeta.value(collectionDocType + "/name");
			w.add("name", name);
			csvRow[3] = name;
		} else {
			w.add("name","");
		}
		//
		w.add("namespace", new String[] {"ctime", ctimeS}, nsMeta.value("namespace/path"));
		//
		if (nsAssetMeta!=null) {
			Collection<XmlDoc.Element> owners = nsAssetMeta.elements(collectionDocType + "/owner");
			if (owners != null) {
				String t = "";
				for (XmlDoc.Element owner : owners) {
					w.add(owner);
					String em = owner.value("email");
					if (t.isEmpty()) {
						t = em;
					} else {
						t += "," + em;
					}
				}
				csvRow[8] = t;
			}
		}
		//
		if (primaryStore != null) {
			w.add("store", primaryStore);
		} else if (primaryStorePolicy != null) {
			w.add("store-policy", primaryStorePolicy);
		}
		//
		XmlDoc.Element quotaMeta = nsMeta.element("namespace/quota");
		String quotaUsedBytes = null;
		String quotaAllocationBytes = null;
		Boolean inherited = false;
		if (quotaMeta != null) {
			w.add(quotaMeta);
			//
			quotaAllocationBytes = quotaMeta.value("allocation");
			quotaUsedBytes = quotaMeta.value("used");
			if (quotaAllocationBytes == null) {

				// Get the inherited quota and divide by number of projects
				XmlDoc.Element quotaInherited = quotaMeta.element("inherited");
				if (quotaInherited != null) {
					quotaAllocationBytes = quotaInherited.value("allocation");
					if (quotaAllocationBytes != null) {
						Integer nProjects = findNumberChildProjects(executor, ph.id());
						Long quotaBytesInt = Long.parseLong(quotaAllocationBytes);
						Long quotaPerProject = quotaBytesInt / nProjects;
						quotaAllocationBytes = "" + quotaPerProject;
					}
					inherited = true;
				}
			}
		}

		if (quotaAllocationBytes != null) {
			String t = Util.bytesToGBytes(quotaAllocationBytes);
			int idx = t.indexOf("GB");
			csvRow[6] = t.substring(0, idx - 1);
		} else {
			csvRow[6] = "0";
		}

		// Now usage. If inherited allocation force summation
		if (doSum || inherited || (!inherited && quotaUsedBytes == null)) {
			XmlDoc.Element s = NameSpaceUtil.sumContent(executor, null, assetNameSpace, "ctime<='"+date+"'");
			String size = s.value("value");
			if (size == null) {
				size = "0";
			}
			String sizeGB = Util.bytesToGBytes(size);
			w.add("size", sizeGB);
			w.add("count", s.value("value/@nbe"));
			//
			int idx = sizeGB.indexOf("GB");
			csvRow[7] = sizeGB.substring(0, idx - 1);

		} else {
			if (quotaUsedBytes != null) {
				String t = Util.bytesToGBytes(quotaUsedBytes);
				int idx = t.indexOf("GB");
				csvRow[7] = t.substring(0, idx - 1);
			} else {
				csvRow[7] = "0";
			}
		}

		// See how many assets have multiple versions
		if (multiVersion) {
			w.push("versions");
			//
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("where", addDate("namespace>='"+assetNameSpace+"' and version count>1", date));
			dm.add("include-destroyed", true);

			// Count all assets
			dm.add("action", "count");
			XmlDoc.Element r = executor.execute("asset.query", dm.root());
			w.push("multiple-versions");
			w.add("count", r.value("value"));

			// Sum the ones with content
			dm = new XmlDocMaker("args");
			dm.add("where", addDate("namespace>='"+assetNameSpace+"' and version count>1", date));
			dm.add("include-destroyed", true);
			dm.add("action", "sum");
			dm.add("xpath", "content/size");
			r = executor.execute("asset.query", dm.root());
			w.add("sum-content", new String[] {"count", r.value("value/@nbe")}, r.value("value"));
			w.pop();

			// COunt the ones with one versions
			dm = new XmlDocMaker("args");
			dm.add("where", addDate("namespace>='"+assetNameSpace+"' and version count=1", date));
			dm.add("include-destroyed", true);
			dm.add("action", "count");
			r = executor.execute("asset.query", dm.root());
			w.push("single-version");
			w.add("count", r.value("value"));

			// Sum the ones with a single version
			dm = new XmlDocMaker("args");
			dm.add("where", addDate("namespace>='"+assetNameSpace+"' and version count=1", date));
			dm.add("include-destroyed", true);
			dm.add("action", "sum");
			dm.add("xpath", "content/size");
			r = executor.execute("asset.query", dm.root());
			w.add("sum-content", new String[] {"count", r.value("value/@nbe")}, r.value("value"));
			w.pop();
			//
			w.pop();
		}

		// Show all meta-data
		if (showMeta) {
			if (nsAssetMeta!=null) {
				w.add(nsAssetMeta);
			}
		} else {

			// Notifications. This is a subset of the meta-data, so if we
			// are showing it all, don't show this here.
			if (nsAssetMeta!=null) {
				XmlDoc.Element notificationMeta = nsAssetMeta.element(projectDocType + "/notification");
				if (notificationMeta!=null) {
					w.add(notificationMeta);
				}
			}
		}

		// Ask the system to see of there is a posix mount point and what its
		// state is
		if (showPosix) {
			// Actual mount state
			SvcProjectPosixMountDescribe.describe(executor, ph.id(), w);
		}

		//
		if (showUsers==null || (showUsers!=null && showUsers.booleanValue(true))) {
			Boolean includeDisabled = false;
			if (showUsers!=null) {
				includeDisabled = showUsers.booleanValue("@include-disabled", false);
			}
			XmlDoc.Element users = Project.listUsers(executor, ph.id(), false, includeDisabled);

			w.push("users");
			Collection<XmlDoc.Element> rolesAndUsers = users.elements("role");
			if (rolesAndUsers!=null) {
				for (XmlDoc.Element roleAndUser : rolesAndUsers) {
					w.add(roleAndUser);
				}
			}
			w.pop();
		}

		if (showIdentity) {
			w.push("identity-tokens");
			XmlDoc.Element identityTokens = Project.listIdentityTokens(executor, ph.id(), false);
			Collection<XmlDoc.Element> rolesAndTokens = identityTokens.elements("role");
			if (rolesAndTokens!=null) {
				for (XmlDoc.Element roleAndToken : rolesAndTokens) {
					w.add(roleAndToken);
				}
			}
			w.pop();
		}


		// DR things
		w.push("replication");

		// Look for the replication asset processing queue associated with this
		// asset namespace
		String queueName = ProjectReplicate.isProjectReplicated(ph);
		if (queueName!=null) {
			w.add("processing-queue", queueName);
		} else {
			w.add("processing-queue", "none");
		}

		if (doDR) {
			// Get stuff from DR server

			String DRServerUUID = ProjectReplicate.DRServerUUID(executor);
			String assetNameSpaceDR = ph.nameSpaceDR ();
			if (NameSpaceUtil.assetNameSpaceExists(executor, DRServerUUID, assetNameSpaceDR)) {
				w.add("namespace", assetNameSpaceDR);
				XmlDoc.Element nameSpaceDescriptionDR = NameSpaceUtil.describe(new ServerRoute(DRServerUUID), executor,
						assetNameSpaceDR);
				String drStore = nameSpaceDescriptionDR.value("namespace/store");
				if (drStore != null) {
					w.add("store", drStore);
				} else {
					String drStorePolicy = nameSpaceDescriptionDR.value("namespace/store-policy");
					w.add("store-policy", drStorePolicy);
				}
				if (doSum) {
					XmlDoc.Element s = NameSpaceUtil.sumContent(executor, DRServerUUID, assetNameSpaceDR, "ctime<='"+date+"'");
					String size = s.value("value");
					if (size == null)
						size = "0";
					String sizeGB = Util.bytesToGBytes(size);
					w.add("size", sizeGB);
					w.add("count", s.value("value/@nbe"));
				}
			}
		}
		w.pop();


		// content copy things
		w.push("content-copies");

		queueName = ProjectContentCopy.isProjectContentCopied(ph);
		if (queueName!=null) {
			w.add("processing-queue", queueName);
		} else {
			w.add("processing-queue", "none");
		}
		w.pop();

		w.pop();
		return csvRow;
	}


	private String[] summariseNameSpace (ServiceExecutor executor, String assetNameSpace, 
			Boolean doSum, String date, String store,  Boolean multiVersion,
			String storageProduct, XmlDocMaker w) throws Throwable {

		// Discover some stuff
		XmlDoc.Element namespaceMeta = NameSpaceUtil.describe(null, executor, assetNameSpace);
		String primaryStore = namespaceMeta.value("namespace/store");
		String primaryStorePolicy = namespaceMeta.value("namespace/store-policy");

		// Filter on store
		if (store != null && primaryStore != null) {
			if (!primaryStore.equals(store)) {
				return null;
			}
		}

		// Accumulate rows for CSV file
		String[] csvRow = new String[9];
		csvRow[0] = date;
		csvRow[4] = storageProduct;
		// Format XML
		w.push("namespace");
		//
		String collectionDocType = Properties.getCollectionDocType(executor);
		String key = namespaceMeta.value("namespace/asset-meta/" + collectionDocType + "/key");
		if (key!=null) {
			w.add("key", key);

			// Of the form <authority>:<id>
			// Split off the authority
			int idx = key.indexOf(":");
			if (idx <= 0) {
				csvRow[1] = "";
				csvRow[2] = key;
			} else {
				csvRow[1] = key.substring(0, idx);
				csvRow[2] = key.substring(idx + 1);
			}
		} else {
			w.add("key", "");
			csvRow[1] = "";
			csvRow[2] = "";
		}
		//
		w.add("id", "none");
		csvRow[5] = "none";
		//
		//
		String name = namespaceMeta.value("namespace/asset-meta/" + collectionDocType + "/name");
		w.add("name", name);
		csvRow[3] = name;
		//
		w.add("namespace", namespaceMeta.value("namespace/path"));
		//
		Collection<XmlDoc.Element> owners = namespaceMeta
				.elements("namespace/asset-meta/" + collectionDocType + "/owner");
		if (owners != null) {
			for (XmlDoc.Element owner : owners) {
				w.add(owner);
			}
		}
		//
		if (primaryStore != null) {
			w.add("store", primaryStore);
		} else if (primaryStorePolicy != null) {
			w.add("store-policy", primaryStorePolicy);
		}
		//
		XmlDoc.Element quotaMeta = namespaceMeta.element("namespace/quota");
		String quotaUsedBytes = null;
		String quotaAllocationBytes = null;
		Boolean inherited = false;
		if (quotaMeta != null) {
			w.add(quotaMeta);
			//
			quotaAllocationBytes = quotaMeta.value("allocation");
			quotaUsedBytes = quotaMeta.value("used");
		}

		if (quotaAllocationBytes != null) {
			String t = Util.bytesToGBytes(quotaAllocationBytes);
			int idx = t.indexOf("GB");
			csvRow[6] = t.substring(0, idx - 1);
		} else {
			csvRow[6] = "0";
		}

		// Now usage. If inherited allocation force summation
		if (doSum || inherited || (!inherited && quotaUsedBytes == null)) {
			XmlDoc.Element s = NameSpaceUtil.sumContent(executor, null, assetNameSpace, "ctime<='"+date+"'");
			String size = s.value("value");
			if (size == null) {
				size = "0";
			}
			String sizeGB = Util.bytesToGBytes(size);
			w.add("size", sizeGB);
			w.add("count", s.value("value/@nbe"));
			//
			int idx = sizeGB.indexOf("GB");
			csvRow[7] = sizeGB.substring(0, idx - 1);

		} else {
			if (quotaUsedBytes != null) {
				String t = Util.bytesToGBytes(quotaUsedBytes);
				int idx = t.indexOf("GB");
				csvRow[7] = t.substring(0, idx - 1);
			} else {
				csvRow[7] = "0";
			}
		}

		// See how many assets have multiple versions
		if (multiVersion) {
			w.push("versions");
			//
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("where", addDate("namespace>='"+assetNameSpace+"' and version count>1", date));
			dm.add("include-destroyed", true);

			// Count all assets
			dm.add("action", "count");
			XmlDoc.Element r = executor.execute("asset.query", dm.root());
			w.push("multiple-versions");
			w.add("count", r.value("value"));

			// Sum the ones with content
			dm = new XmlDocMaker("args");
			dm.add("where", addDate("namespace>='"+assetNameSpace+"' and version count>1", date));
			dm.add("include-destroyed", true);
			dm.add("action", "sum");
			dm.add("xpath", "content/size");
			r = executor.execute("asset.query", dm.root());
			w.add("sum-content", new String[] {"count", r.value("value/@nbe")}, r.value("value"));
			w.pop();
			//
			dm = new XmlDocMaker("args");
			dm.add("where", addDate("namespace>='"+assetNameSpace+"' and version count=1",date));
			dm.add("include-destroyed", true);
			dm.add("action", "count");
			r = executor.execute("asset.query", dm.root());
			w.push("single-version");
			w.add("count", r.value("value"));
			//
			dm = new XmlDocMaker("args");
			dm.add("where", addDate("namespace>='"+assetNameSpace+"' and version count=1", date));
			dm.add("include-destroyed", true);
			dm.add("action", "sum");
			dm.add("xpath", "content/size");
			r = executor.execute("asset.query", dm.root());
			w.add("sum-content", new String[] {"count", r.value("value/@nbe")}, r.value("value"));
			w.pop();
			//
			w.pop();
		}


		w.pop();
		return csvRow;
	}

	private String addDate (String predicate, String date) {
		if (date==null) return predicate;
		return predicate + "(ctime<='" + date + "')";
	}

	private  Integer findNumberChildProjects(ServiceExecutor executor, String projectID) throws Throwable {
		String parentNameSpace = Project.parentNameSpace(executor, projectID);
		return Project.projectIDs(executor, parentNameSpace).size();

	}





}
