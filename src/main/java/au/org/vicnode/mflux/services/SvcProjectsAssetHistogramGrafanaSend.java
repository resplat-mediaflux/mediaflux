package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

import java.util.*;

public class SvcProjectsAssetHistogramGrafanaSend extends PluginService {
        private Interface _defn;

        public SvcProjectsAssetHistogramGrafanaSend() {
            _defn = new Interface();
        }

        @Override
        public Access access() {
            return ACCESS_ADMINISTER;
        }

        @Override
        public Interface definition() {
            return _defn;
        }

        @Override
        public String description() {
            return "Returns a histogram of asset counts for all project namespaces";
        }

        @Override
        public String name() {
            return Properties.SERVICE_ROOT +"projects.asset.histogram.grafana.send";
        }

    // Add this method to calculate percentiles
    private void addPercentileInfo(List<Integer> counts, XmlWriter w, String pathVar) throws Throwable {
        int[] percentiles = {20, 40, 60, 80, 100};
        w.push("percentiles");

        for (int percentile : percentiles) {
            int index = (int) Math.ceil((percentile / 100.0) * counts.size()) - 1;
            if (index < 0) index = 0;
            w.add("percentile",
                    new String[] {
                            "value", String.valueOf(percentile),
                            pathVar, String.valueOf(counts.get(index))
                    }
            );
        }
        w.pop();
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        ServiceExecutor executor = executor();
        Map<String, Integer> countHistogram = new HashMap<>();
        Map<String, Integer> sizeHistogram = new HashMap<>();

        // Get all project namespaces
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", "projects");

        List<XmlDoc.Element> namespaces = executor.execute("asset.namespace.list", dm.root()).elements("namespace/namespace");

        // Count assets in each namespace
        for (XmlDoc.Element ns : namespaces) {
            String nsPath = ns.value();

            XmlDocMaker countDm = new XmlDocMaker("args");
            countDm.add("namespace", "/projects/" + nsPath);
            countDm.add("acount", true);

            XmlDoc.Element countElements = executor.execute("asset.namespace.count", countDm.root());
            int count = countElements.intValue("count") + countElements.intValue("acount");
            countHistogram.put(nsPath, count);

            XmlDocMaker sizeDm = new XmlDocMaker("args");
            sizeDm.add("namespace", "/projects/" + nsPath);

            XmlDoc.Element sizeElements = executor.execute("asset.namespace.describe", sizeDm.root());
            int size;
            try{
                String sizeString = sizeElements.value("namespace/quota/used");
                sizeString = sizeString.substring(0,sizeString.length()-8);
                size = Integer.parseInt(sizeString)/10;
            }catch(Exception e){
                size = 0;
                w.add("nullsize",nsPath);

            }
            sizeHistogram.put(nsPath,size);
        }

        // After collecting histogram data
        List<Integer> counts = new ArrayList<>(countHistogram.values());
        Collections.sort(counts);

        List<Integer> sizes = new ArrayList<>(sizeHistogram.values());
        Collections.sort(sizes);

        // add counts to output
        addPercentileInfo(counts,w,"count");
        addPercentileInfo(sizes,w,"size");

        // Send to Graphite
        sendToGraphite(executor, counts,w,"count");
        sendToGraphite(executor, sizes,w,"size");

        // Add total count
        int totalAssets = countHistogram.values().stream().mapToInt(Integer::intValue).sum();
        w.add("total-assets", totalAssets);
    }
    private void sendToGraphite(ServiceExecutor executor, List<Integer> counts, XmlWriter w, String pathvar) throws Throwable {
        long timestamp = System.currentTimeMillis() / 1000;
        int[] percentiles = {20, 40, 60, 80, 100};

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("host","grafana.cloud.unimelb.edu.au");
        dm.add("port","2004");
        dm.add("protocol","pickle");

        for (int percentile : percentiles) {
            int index = (int) Math.ceil((percentile / 100.0) * counts.size()) - 1;
            if (index < 0) index = 0;
            dm.push("metric");
            dm.add("path", "projects.assets."+pathvar+".distribution.percentile." + percentile);
            dm.add("value", String.valueOf(counts.get(index)));
            dm.add("time", "now");
            dm.pop();
        }


        executor.execute("graphite.metrics.send", dm.root());
    }


}

