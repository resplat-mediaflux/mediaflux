/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.replication;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.services.SvcProjectDescribe;

public class SvcProjectReplicateQueueSet extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectReplicateQueueSet()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addMandatoryProjectIDs(_defn,  allowPartialCID);
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "This service sets the standard asset processing queue for replications on the specified projects.  Projects must be of type 'production' for the queue to be successfully set.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.replicate.queue.set";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Check the DR environment is configured		
		if (!ProjectReplicate.isDRConfigured(executor())) {
			throw new Exception ("Replications are not configured on this system");
		}
		
		// Inputs
		Collection<String> theProjectIDs = args.values("project-id");	

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs.size()>0) {
			for (String theProjectID : theProjectIDs) {
				String t[] = Project.validateProject(executor(), 
						theProjectID, allowPartialCID);
				projectIDs.add(t[1]);
			}
		}


		// Set queue
		for (String projectID : projectIDs) {
			XmlDoc.Element namespaceMeta = Project.describeNameSpace(null, executor(), projectID, false);
			w.push("project");
			w.add("id", projectID);
			if (Project.isProduction(executor(), namespaceMeta, projectID)) {
				w.add("type", Project.projectType(executor(), projectID));
				ProjectReplicate.setStandardQueueOnProject(executor(), projectID, w);
			} else {
				w.add("type", Project.projectType(executor(), projectID));
				w.add("replication-queue", "not-set");
			}
			w.pop();

		}
	}



}
