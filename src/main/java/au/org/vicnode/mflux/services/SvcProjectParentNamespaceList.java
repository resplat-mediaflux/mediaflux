/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;

import arc.xml.XmlDoc.Element;

import arc.xml.XmlWriter;

import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectParentNamespaceList extends PluginService {

	private Interface _defn;

	public SvcProjectParentNamespaceList()  throws Throwable {
		_defn = new Interface();
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Lists all of the parent namespaces that projects may be created under.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.parent.namespace.list";
	}

	public boolean canBeAborted() {

		return false;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		Collection<String> parents = Project.assetNameSpaceRoots(executor());
		for (String parent : parents) {
			w.add("parent", parent);
		}
	}




}
