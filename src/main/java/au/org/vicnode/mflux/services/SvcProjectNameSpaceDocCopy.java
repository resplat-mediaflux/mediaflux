/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services;

/**
 * Depends on unimelb-essentials package
 */


import java.util.Vector;

import arc.mf.plugin.*;

import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectNameSpaceDocCopy extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectNameSpaceDocCopy() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
		//
		SvcProjectDescribe.addProdType (_defn);	
		_defn.add(new Interface.Element("type", StringType.DEFAULT, "The Document Type name.", 1, 1));
		_defn.add(new Interface.Element("new-type", StringType.DEFAULT, "The new Document Type name.", 1, 1));
	}

	public String name() {
		return "vicnode.project.namespace.doc.copy";
	}

	public String description() {
		return "Specialised service to copy a namespace meta-data document to one of another name.  Both the source and destination document types must exist, and their definitions should be the same, apart from the document name (including document namespace). The method is to copy the document (changing its name). The original is left in place.  The service only looks at the root namespace of the project (e.g. /projects/<proj id>). Use this to change the namespace of a document, say renaming from VicNode:Collection to ds:Collection.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		String prodType = args.stringValue("prod-type",  Properties.PROJECT_TYPE_ALL);
		String docType = args.value("type");
		String docTypeNew = args.value("new-type");

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;


		// Copy
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
			//
			if (Project.keepOnProjectType(ph, prodType)) {
				w.push("project", new String[]{"id", ph.id()});
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("namespace", ph.nameSpace());
				dm.add("mode", "copy");
				dm.add("type", docType);
				dm.add("new-type", docTypeNew);
				XmlDoc.Element r = executor().execute("unimelb.asset.namespace.metadata.rename", dm.root());
				w.add(r.element("copied"));
				w.pop();
			}
		}
	}
}
