/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services;

/**
 * Depends on unimelb-essentials package
 */


import java.util.Vector;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.CiteableIdType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectNameSpaceMetaDataCopy extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectNameSpaceMetaDataCopy() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
		//
		Interface.Element me = new Interface.Element("to", StringType.DEFAULT, "The parent namespace to copy to which must pre-exist. The project child string (e.g. proj-XYZ-1.2.3) will be appended to this root namespace.", 1, 1);
		me.add(new Interface.Attribute("proute", CiteableIdType.DEFAULT,
				"In a federation, specifies the route to the peer that manages this namespace.  If not supplied, then the namespace will be assumed to be local.", 0));
		_defn.add(me);
		_defn.add(new Interface.Element("list", BooleanType.DEFAULT, "List all namespaces traversed (defaults to false).", 0, 1));
		_defn.add(new Interface.Element("recurse", BooleanType.DEFAULT, "By default this service will recurse down the namespace tree. Set to false to take the top level only.", 0, 1));
		SvcProjectDescribe.addProdType (_defn);
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.namespace.metadata.copy";
	}

	public String description() {
		return "Specialised service (for backup purposes) to recursively copy (set) project namespace meta-data and template meta-data to another parent root.  For example to copy namespaces for project 'project-id=proj-XYZ-1.2.3' to /namespace-backup/proj-XYZ-1.2.3 set 'to=/namespace-backup'. Depends on unimelb-essentials services package being installed.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		String toParent = args.value("to");
		String toRoute = args.value("to/@proute");                  // Local if null
		Boolean list = args.booleanValue("list", false);
		String prodType = args.stringValue("prod-type",  Properties.PROJECT_TYPE_ALL);
		Boolean recurse = args.booleanValue("recurse", true);

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;


		// Copy
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
			//
			if (Project.keepOnProjectType(ph, prodType)) {
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("create", "true");
				dm.add("list", list);
				dm.add("from", ph.nameSpace());
				dm.add("recurse", recurse);

				// Add the project child name to the destination parent. E.g.
				// 
				//          from                      to                add this bit
				//  -----------------------         ----------------
				// /projects/proj-XYZ-1.2.3 ->     /backups/projects/proj-XYZ-1.2.3
				String parts[] = ph.nameSpace().split("/");
				int n = parts.length;
				String child = parts[n-1];
				String toPath = toParent + "/" + child;
				//
				dm.add("to", new String[]{"proute", toRoute}, toPath);
				//
				XmlDoc.Element r = executor().execute("unimelb.asset.namespace.metadata.tree.copy", dm.root());
				if (r!=null) {
					w.addAll(r.elements());
				}
			}
		}
	}
}
