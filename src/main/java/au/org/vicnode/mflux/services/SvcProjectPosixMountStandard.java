/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Vector;
import arc.mf.plugin.PluginService;

import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixMountStandard extends PluginService {


	private Interface _defn;

	public SvcProjectPosixMountStandard()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT, 
				"The ID of the project (of the form 'proj-<name>-<cid>').", 1, 1));
	}



	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Mounts or remounts a posix mount point for the project, with the standard mount state (readonly=false, apply-mode-bits=true, should-be-mounted=true, NFS restrictions to local host). ";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.mount.standard";
	}

	public static XmlDoc.Element makeStandardMetaExport () throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.push("metadata-export");
		dm.add("export", "false");
		dm.pop();
		return dm.root().element("metadata-export");
	}
	
	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {


		// Inputs
		String projectID = args.value("project-id");
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		Posix.makeDefaultRestrictions(dm);
		XmlDoc.Element restrict = dm.root().element("restrict");
		Vector<XmlDoc.Element> restrictions = new Vector<XmlDoc.Element>();
		restrictions.add(restrict);
		XmlDoc.Element rootRestriction = dm.root().element("root");
		//
		String restrictAction = "replace";
		String rootAction = "replace";
		Boolean readOnly = SvcProjectCreate.READ_ONLY;
		Boolean applyModeBits = SvcProjectCreate.APPLY_MODE_BITS;
		String allowedProtocol = SvcProjectCreate.ALLOWED_POSIX_PROTOCOL;
		//
		SvcProjectPosixMount.mount (executor(), projectID, makeStandardMetaExport(), 
				restrictions, restrictAction, rootRestriction, rootAction, readOnly, 
				true, applyModeBits, allowedProtocol, w);
	}

}
