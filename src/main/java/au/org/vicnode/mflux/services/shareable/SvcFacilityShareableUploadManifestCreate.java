package au.org.vicnode.mflux.services.shareable;

import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcFacilityShareableUploadManifestCreate extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.manifest.create";

	private final Interface _defn;

	public SvcFacilityShareableUploadManifestCreate() {
		_defn = new Interface();
		_defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "Shareable id.", 1, 1));
		_defn.add(new Interface.Element("upload-id", LongType.POSITIVE, "Upload id.", 1, 1));
		_defn.add(new Interface.Element("upload-namespace", StringType.DEFAULT, "Path to the upload namespace.", 1, 1));
		_defn.add(new Interface.Element("completion-time", DateType.DEFAULT, "Upload completion date and time.", 0, 1));

		Interface.Element args = new Interface.Element("args", XmlDocType.DEFAULT, "Shareable args.", 0, 1);
		args.setIgnoreDescendants(true);
		_defn.add(args);

		_defn.add(new Interface.Element("file-type-statistics", BooleanType.DEFAULT,
				"Get asset/file type statistics. Defaults to true.", 0, 1));
		_defn.add(new Interface.Element("file-size-statistics", BooleanType.DEFAULT,
				"Get file size statistics. Defaults to true.", 0, 1));

		Interface.Element share = new Interface.Element("share", XmlDocType.DEFAULT,
				"Options for share the uploaded data.", 0, 1);
		share.add(new Interface.Element("duration", new IntegerType(1, Integer.MAX_VALUE),
				"Number of days the download shareable, direct download link and user interaction.", 0, 1));
		_defn.add(share);

		Interface.Element downloadShareable = new Interface.Element("download-shareable", XmlDocType.DEFAULT,
				"Share the data via a download shareable.", 0, 1);
		downloadShareable.add(new Interface.Element("id", LongType.POSITIVE, "Id of the download shareable.", 1, 1));
		_defn.add(downloadShareable);

		Interface.Element userInteraction = new Interface.Element("user-interaction", XmlDocType.DEFAULT,
				"Share the data via a user interaction.", 0, 1);
		userInteraction.add(new Interface.Element("id", LongType.POSITIVE, "Id of the user interaction.", 1, 1));
		_defn.add(userInteraction);

		Interface.Element directDownload = new Interface.Element("direct-download", XmlDocType.DEFAULT,
				"Direct download link.", 0, 1);
		directDownload.add(new Interface.Attribute("has-password", BooleanType.DEFAULT,
				"Is the link protected with password?", 0));
		directDownload.add(new Interface.Element("token-id", LongType.POSITIVE,
				"secure identity token associated with the link.", 1, 1));
		_defn.add(directDownload);

	}

	@Override
	public Access access() {
		return ACCESS_MODIFY;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Create manifest asset for the specified shareable upload.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		String shareableId = args.value("shareable-id");

		// shareable
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element shareable = Shareable.describe(executor(), shareableId);

		// upload id
		String uploadId = args.value("upload-id");

		// upload namespace
		String uploadNS = args.value("upload-namespace");

		// upload completion time
		Date completionTime = args.dateValue("completion-time", null);

		boolean getFileTypeStatistics = args.booleanValue("file-type-statistics", true);

		boolean getFileSizeStatistics = args.booleanValue("file-size-statistics", true);

		Integer shareDuration = args.intValue("share/duration", null);

		String downloadShareableId = args.value("download-shareable/id");

		String userInteractionId = args.value("user-interaction/id");

		String directDownloadTokenId = args.value("direct-download/token-id");

		boolean directDownloadHasPassword = args.booleanValue("direct-download/@has-password");

		String assetPath = Manifest.createManifestAsset(executor(), shareable, uploadId, uploadNS, args.element("args"),
				completionTime, getFileTypeStatistics, getFileSizeStatistics, shareDuration, downloadShareableId,
				userInteractionId, directDownloadTokenId, directDownloadHasPassword);

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("path", assetPath);
		String assetId = executor().execute("asset.identifier.get", dm.root()).value("id");

		w.add("id", new String[] { "path", assetPath }, assetId);
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

}
