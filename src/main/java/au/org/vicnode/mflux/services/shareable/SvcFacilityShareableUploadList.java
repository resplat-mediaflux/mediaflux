package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

import java.util.List;

public class SvcFacilityShareableUploadList extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.list";

    private final Interface _defn;

    public SvcFacilityShareableUploadList() {
        _defn = new Interface();
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "List facility upload shareables owned by the current user.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        long count = 0;

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("size", "infinity");
        dm.add("type", "upload");

        PluginTask.checkIfThreadTaskAborted();
        XmlDoc.Element re = executor().execute("asset.shareable.describe", dm.root());
        List<XmlDoc.Element> ses = re.elements("shareable");
        if (ses!=null) {
            for (XmlDoc.Element se : ses) {
                // check if it is facility upload
                if (se.elementExists("completion-service[@name='" + SvcFacilityShareableUploadComplete.SERVICE_NAME + "']")) {
                    String name = se.value("name");
                    String id = se.value("@id");
                    String owner = String.format("%s:%s", se.value("owner/domain"), se.value("owner/user"));
                    w.add("shareable", new String[]{"type", "upload", "name", name, "owner", owner, "uploads", se.value("uploads/count"), "enabled", se.value("enabled")}, id);
                    count++;
                }
            }
        }
        w.add("count", count);

    }

    @Override
    public boolean canBeAborted() {
        return true;
    }
}
