package au.org.vicnode.mflux.services.shareable;

import java.util.Date;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.PasswordType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetUtil;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcFacilityShareableUploadUploadList extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.upload.list";

    private Interface _defn;

    public SvcFacilityShareableUploadUploadList() {
        _defn = new Interface();
        _defn.add(new Interface.Element("id", LongType.POSITIVE,
                "The id of the shareable.  If not specified then a token must be specified instead.", 0, 1));
        _defn.add(new Interface.Element("token", PasswordType.DEFAULT,
                "The token of the shareable.  If not specified then an id must be specified instead.", 0, 1));
        _defn.add(new Interface.Element("manitest", BooleanType.DEFAULT,
                "Include the manifest information of the upload(s). Defaults to true.", 0, 1));
        _defn.add(new Interface.Element("completed-after", DateType.DEFAULT,
                "List the uploads completed after this time(inclusive).", 0, 1));
        _defn.add(new Interface.Element("completed-before", DateType.DEFAULT,
                "List the uploads completed before this time(exclusive).", 0, 1));
        _defn.add(new Interface.Element("path-context", StringType.DEFAULT,
                "list only the uploads with matching path-context.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "List uploads of the (upload) shareable.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String id = args.value("id");
        String token = args.value("token");
        boolean manifest = args.booleanValue("manifest", true);
        Date completedAfter = args.dateValue("completed-after", null);
        Date completedBefore = args.dateValue("completed-before", null);
        String pathContext = args.value("path-context");
        if (id == null && token == null) {
            throw new IllegalArgumentException("id or token of the shareable must be specified.");
        }
        XmlDoc.Element shareableElement = id != null ? Shareable.describe(executor(), id)
                : Shareable.describe(token, executor());
        if (id == null) {
            id = shareableElement.value("@id");
        }
        List<XmlDoc.Element> ues = shareableElement.elements("uploads/upload");
        long count = 0;
        if (ues != null) {
            for (XmlDoc.Element uploadElement : ues) {
                String uploadId = uploadElement.value("@id");
                if (pathContext != null) {
                    String uploadPathContext = uploadElement.value("path-context");
                    if (uploadPathContext == null || !pathContextMatch(uploadPathContext, pathContext)) {
                        continue;
                    }
                }
                if (completedAfter != null || completedBefore != null) {
                    Date completedAt = uploadElement.dateValue("completed-at");
                    if (completedAt != null) {
                        if (completedAfter != null) {
                            if (completedAt.compareTo(completedAfter) < 0) {
                                continue;
                            }
                        }
                        if (completedBefore != null) {
                            if (completedAt.compareTo(completedBefore) >= 0) {
                                continue;
                            }
                        }
                    } else {
                        continue;
                    }
                }
                PluginTask.checkIfThreadTaskAborted();
                XmlDoc.Element manifestElement = manifest ? getManifest(executor(), shareableElement, uploadId) : null;
                describeUpload(executor(), shareableElement, uploadElement, manifestElement, w);
                count += 1;
            }
        }
        w.add("count", count);
    }

    private static void describeUpload(ServiceExecutor executor, XmlDoc.Element shareable, XmlDoc.Element upload,
            XmlDoc.Element manifest, XmlWriter w) throws Throwable {
        String uploadId = upload.value("@id");
        String interactionId = upload.value("@interaction-id");
        String completedAt = upload.value("completed-at");
        String pathContext = upload.value("path-context");
        String parentName = upload.value("parent-name");
        String shareableNamespace = shareable.value("namespace");
        String uploadNamespace = manifest != null ? manifest.value("upload/namespace")
                : String.format("%s:%s", shareableNamespace, parentName);

        w.push("upload", new String[] { "id", uploadId, "interaction-id", interactionId });
        w.add("namespace", uploadNamespace);
        XmlDoc.Element re = getTotalFileSize(executor, uploadNamespace);
        long totalFileSize = re.longValue("value", 0);
        long totalFileCount = re.longValue("value/@nbe", 0);
        w.add("total-file-count", totalFileCount);
        w.add("total-file-size", totalFileSize);
        if (completedAt != null) {
            w.add("completed-at", new String[] { "millisec", upload.value("completed-at/@millisec") }, completedAt);
        }
        if (pathContext != null) {
            w.add("path-context", pathContext);
        }
        if (manifest != null) {
            if (manifest.elementExists("upload/arg")) {
                w.addAll(manifest.elements("upload/arg"));
            }
            if (manifest.elementExists("upload/share")) {
                w.add(manifest.element("share"));
            }
        }
        w.pop();
    }

    private static XmlDoc.Element getManifest(ServiceExecutor executor, XmlDoc.Element shareable, String uploadId)
            throws Throwable {
        String manifestAssetId = Manifest.getManifestAssetId(executor, shareable, uploadId, true);
        if (manifestAssetId != null) {
            if (AssetUtil.assetExists(executor, manifestAssetId)) {
                XmlDocMaker dm = new XmlDocMaker("args");
                dm.add("id", manifestAssetId);
                return executor.execute(SvcFacilityShareableUploadManifestDescribe.SERVICE_NAME, dm.root())
                        .element("shareable-upload-manifest");
            }
        }
        return null;
    }

    private static XmlDoc.Element getTotalFileSize(ServiceExecutor executor, String uploadNS) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "namespace>='" + uploadNS + "' and asset has content");
        dm.add("where", "not(namespace='" + uploadNS + "' and (name='" + Metadata.METADATA_FILE_NAME + "' or name='"
                + Manifest.MANIFEST_FILE_NAME + "'))");
        dm.add("action", "sum");
        dm.add("xpath", "content/size");
        return executor.execute("asset.query", dm.root());
    }

    private static boolean pathContextMatch(String uploadPathContext, String path) {
        uploadPathContext = uploadPathContext.replaceFirst("/+$", "");
        path = path.replaceFirst("/+$", "");
        if (uploadPathContext.equals(path)) {
            return true;
        }
        if (path.matches("^[a-zA-Z]{1}:/.*")) {
            // Windows Path starts with drive letter such as C:/Downloads/abc
            if (uploadPathContext.equals(path.substring(2))) {
                return true;
            }
        }
        return false;
    }

}
