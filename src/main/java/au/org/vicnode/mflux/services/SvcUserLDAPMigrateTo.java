package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;

public class SvcUserLDAPMigrateTo extends PluginService {

	public static final String SERVICE_NAME = "vicnode.user.ldap.migrate.to";

	private Interface _defn;

	public SvcUserLDAPMigrateTo() {
		_defn = new Interface();

		addToDefn(_defn);
	}


	static void addToDefn(Interface defn) {

		Interface.Element ie = new Interface.Element("authority", StringType.DEFAULT,
				"The identity of the authority/repository where the user identity originates. If unspecified, then refers to a user in this repository.",
				0, 1);
		ie.add(new Interface.Attribute("protocol", StringType.DEFAULT,
				"The protocol of the identity authority. If unspecified, defaults to federated user within the same type of repository.",
				0));
		defn.add(ie);
		defn.add(new Interface.Element("email-filter", StringType.DEFAULT, "Only consider accounts for which the email for the account contains this string. If multiple provided, an account is kept if any of the filter strings are held. e.g. '@unimelb.edu.au'", 1, Integer.MAX_VALUE));
		defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain the user being migrated belongs to", 1, 1));
		defn.add(new Interface.Element("user", StringType.DEFAULT, "The user name being migrated. If not supplied, all users in the domain are migrated.", 0, Integer.MAX_VALUE));
		//
		ie = new Interface.Element("to-user", StringType.DEFAULT,
				"The user we are migrating to. If not specified, service tries to look up in LDAP based on the source user email.  Only use if one input :user specified.",
				0, 1);
		ie.add(new Interface.Attribute("domain", StringType.DEFAULT,
				"The domain of the user we are migrating to.",
				1));
		defn.add(ie);
		//
		defn.add(new Interface.Element("exclude-user", StringType.DEFAULT, "Exclude this user (e.g. a service account). Just give the username, we already know their domain.", 0, Integer.MAX_VALUE));
		defn.add(new Interface.Element("check-only", BooleanType.DEFAULT, "Just check the process only (default true) and attempt to look the user up in LDAP, but doesn't actually migrate them. ", 0, 1));
		defn.add(new Interface.Element("candidates-only", BooleanType.DEFAULT, "Only show, in the terminal output, the user accounts that are actually selected for attempted migration (default false). So they must 1) not be an exclusion list, 2) be enabled, 3) have an extractable email, 4) not have been migrated already (unmigrated-only=true),  ", 0, 1));
		//
		ie = new Interface.Element("unmigrated-only", BooleanType.DEFAULT, "Only attempt to migrate accounts that have not yet been migrated (default true).", 0, 1);
		ie.add(new Interface.Attribute("show", BooleanType.DEFAULT,
				"If only restricting to unmigrated accounts, show (default) or do not show those accounts in the log",
				0));
		defn.add(ie);
		//
		defn.add(new Interface.Element("ignore-daris-only", BooleanType.DEFAULT, "Do not consider users who are DaRIS (only) because they will soon move from this server anyway.", 0, 1));
		//
		ie = new Interface.Element("notify-user", BooleanType.DEFAULT,
				"Notify the user that the account migration has occurred with a standardized UoM message. No notifications are sent when check-only is true.",
				0, 1);
		ie.add(new Interface.Attribute("bcc", StringType.DEFAULT,
				"BCC this address",
				0));
		ie.add(new Interface.Attribute("from", StringType.DEFAULT,
				"Notification emails to users are 'from' this address.",
				0));
		ie.add(new Interface.Attribute("host", StringType.DEFAULT,
				"Specify host for the notification, E.g. mediaflux.researchsoftware.unimelb.edu,au.",
				0));
		defn.add(ie);
		//
		defn.add(new Interface.Element("idx", IntegerType.DEFAULT, "The starting position to list user entries. Defaults to 1. Ignored if 'user' is specified.", 0, 1));
		defn.add(new Interface.Element("size", IntegerType.DEFAULT, "Number of users in domain to migrate (if 'user' not given). ", 0, 1));
		defn.add(new Interface.Element("set-meta", BooleanType.DEFAULT, "If true (default is false), set some meta-data on the user account successfully  migrated indictating it was migrated to LDAP.  Could be used to disable the account later. ", 0, 1));
		defn.add(new Interface.Element("filter-output", StringType.DEFAULT, "Provide a string which will be used to filter the output records seen. It filters the element 'status' by seeing if the provided string is contained. If it is, then the user record is presented.", 0, 1));
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String description() {
		return "Attempts to find a University of Melbourne staff or student user in Active Directory (by extracting their email address from their account) and then copies their permissions, settings and meta-data to that AD user.   Staff are migrated to Active Directory Mediaflux domain 'unimelb'.  Students are migrated to Active Directory Mediaflux domain 'unimelb-student'.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		XmlDoc.Element authority = args.element("authority");
		String domain = args.value("domain");
		Collection<XmlDoc.Element> users = args.elements("user");
		Boolean checkOnly = args.booleanValue("check-only", true);
		XmlDoc.Element unmigratedOnly = args.element("unmigrated-only");
		String idx = args.stringValue("idx", "1");
		String size = args.stringValue("size", "infinity");
		Boolean setMeta = args.booleanValue("set-meta", false);
		Boolean candidatesOnly = args.booleanValue("candidates-only", false);
		Boolean ignoreDaRISOnly = args.booleanValue("ignore-daris-only", true);
		Collection<String> excludeUsers = args.values("exclude-user");
		String filter = args.value("filter-output");
		XmlDoc.Element toUser = args.element("to-user");
		Collection<String> emailFilters = args.values("email-filter");
		if (toUser!=null) {
			if (users==null) {
				throw new Exception("To specify the 'to-user', you must specify one source user in argument 'user'.");
			}
			if (users.size()!=1) {
				throw new Exception("To specify the 'to-user', you must specify one source user in argument 'user'.");
			}
			String d = toUser.value("@domain");
			if (!d.equals("unimelb") && !d.equals("unimelb-student")) {
				throw new Exception("The 'to-user' domain must be one of 'unimelb' or 'unimelb-student'.");
			}
		}
		//
		XmlDoc.Element notify = args.element("notify-user");
		if (notify!=null) {
			if (notify.booleanValue()) {
				String host = notify.value("@host");
				String bcc = notify.value("@bcc");
				String from = notify.value("@from");
				if (host==null || bcc==null || from==null) {
					throw new Exception("You must specify all of host, bcc and from in the notify element");
				}
			}
		}

		// Migrate Users
		migrateUser (executor(), authority, domain, emailFilters, users, ignoreDaRISOnly, 
				unmigratedOnly, candidatesOnly, excludeUsers, idx, size, checkOnly, 
				notify, setMeta, filter, toUser, w);

	}

	private void migrateUser (ServiceExecutor executor, Element authority, String domain, 
			Collection<String> emailFilters, Collection<XmlDoc.Element> users0, Boolean ignoreDaRISOnly, XmlDoc.Element unmigratedOnly, Boolean candidatesOnly, Collection<String> excludeUsers, String idx, String size,
			Boolean checkOnly, XmlDoc.Element notify, Boolean setMeta, String filter, XmlDoc.Element toUser,
			XmlWriter w) throws Throwable {

		// Get the extant user
		XmlDocMaker dm = new XmlDocMaker("args");
		if (authority!=null) {
			dm.add(authority);
		}
		dm.add("domain", domain);
		if (users0!=null) {
			dm.addAll(users0);
		} else {
			dm.add("idx", idx);
			dm.add("size", size);
		}
		XmlDoc.Element r = executor.execute("user.describe", dm.root());
		Collection<XmlDoc.Element> usersMeta = r.elements("user");
		if (usersMeta==null) return;
		if (usersMeta.size()==0) return;

		// FInd protocol of input domain
		dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		XmlDoc.Element domainDescription = executor().execute("authentication.domain.describe", dm.root());
		String protocol = domainDescription.value("domain/@protocol");
		String now = DateUtil.formatDate(new Date(), "dd-MMM-YYYY HH:mm:ss");

		// Iterate over users
		for (XmlDoc.Element userMeta : usersMeta) {
			PluginTask.checkIfThreadTaskAborted();

			// Fetch input account attributes with which to find the user
			String userName = userMeta.value("@user");
			String name = userMeta.value("name");
			String email = userMeta.value("e-mail");
			if (email==null) {
				email = userMeta.value("asset/meta/mf-user/email");
			}
			Boolean enabled = userMeta.booleanValue("@enabled", true);

			XmlDocMaker dmw = new XmlDocMaker("args");
			dmw.push("user");
			dmw.add("user-from", new String[] {"domain", domain, "email", email},  userName);

			// If desired, skip accounts already migrated
			Boolean isCandidate = false;
			Boolean unmOnly = true;
			Boolean showUnmOnly = true;
			if (unmigratedOnly!=null) {
				unmOnly = unmigratedOnly.booleanValue();
				showUnmOnly = unmigratedOnly.booleanValue("@show", true);
			}
			if (unmOnly && hasBeenMigrated(executor, userMeta.element("asset/meta"))) {
				if (showUnmOnly) {
					dmw.add("status", "account-already-migrated");
					dmw.pop();
					w.addAll(dmw.root().elements());
				}
			} else {
				if (!enabled) {
					dmw.add("status", "account-not-enabled");
				} else {
					Boolean keep = true;
					if (excludeUsers!=null && excludeUsers.size()>0) {
						if (excludeUsers.contains(userName)) {
							keep = false;
						}
					}
					if (!keep) {
						dmw.add("status", "in-exclusion-list");
					} else 	if (email==null) {
						dmw.add("status", "cannot-extract-email");
					} else {

						// UoM emails will be of the form user@florey.edu.au or user@unimelb.edu.au or user@student.unimelb.edu.au
						if (filterOnEmail(emailFilters, email)) {						
							isCandidate = true;

							// See if user has any specific roles. If not, it means all they have done is log in and out.
							// We don't migrate them as they will be confused. We need to set a different mf-note on those.
							int iret = filterOnRoles (executor, domain, userName, ignoreDaRISOnly);
							if (iret==0) {

								// Look up user by email in appropriate LDAP domain or insert
								// the one given as the service argument
								Collection<XmlDoc.Element> ldapUsers = null;
								if (toUser!=null) {
									ldapUsers = createUserRecord (executor, toUser, dmw);   
								} else {
									ldapUsers = findInLDAP (executor, userMeta, dmw);   
								}
								if (ldapUsers==null) { 
									dmw.add("status", "not-found-in-LDAP");
								} else if (ldapUsers.size()>1) {
									dmw.add("status", "multiple-users-found-in-LDAP");
								} else {
									XmlDoc.Element[] t = ldapUsers.toArray(new XmlDoc.Element[ldapUsers.size()]);
									XmlDoc.Element ldapUser = t[0];
									String ldapDomain = ldapUser.value("@domain");
									String ldapUserName = ldapUser.value();
									String ldapUserDisplayName = ldapUser.value("@display-name");
									dmw.add("user-to", new String[] {"domain", ldapDomain, "name", ldapUserDisplayName}, ldapUserName);
									
									Boolean isSAML = (protocol!=null && protocol.equals("saml"));

									if (checkOnly) {
										// List settings. We don't have list modes yet for the other functions
										dm = makeDocMaker(authority, domain, userName, ldapUser);
										dm.add("list-only", true);
										r = executor.execute("unimelb.user.settings.copy", dm.root());
										dmw.addAll(r.elements("user/settings"));
									} else {
										dmw.push("migrating");
										// Copy roles
										dm = makeDocMaker(authority, domain, userName, ldapUser);
										r = executor.execute("unimelb.user.permissions.copy", dm.root());
										dmw.push("permissions");
										Collection<XmlDoc.Element>roles = r.elements("roles"); 
										if (roles!=null) {
											dmw.addAll(roles);
										}
										if (r.elements("permissions")!=null) {
											dmw.addAll(r.elements("permissions"));
										}
										dmw.pop();

										// Copy settings
										dm = makeDocMaker(authority, domain, userName, ldapUser);
										r = executor.execute("unimelb.user.settings.copy", dm.root());
										dmw.push("settings");
										dmw.addAll(r.elements("user/settings"));
										dmw.pop();

										// Copy meta-data
										dm = makeDocMaker(authority, domain, userName, ldapUser);
										r = executor.execute("unimelb.user.metadata.copy", dm.root());
										dmw.push("meta-data");
										dmw.addAll(r.elements("meta"));
										dmw.pop();
										
										// Grant SAML user VicNode (because that is held as a role in the AAF domain)
										dm = new XmlDocMaker("args");
										String aname = ldapDomain+":"+ldapUserName;
										dm.add("name", aname);
										dm.add("type", "user");
										dm.add("role", new String[] {"type", "role"}, "VicNode:standard-user");
										executor().execute("actor.grant", dm.root());
							

										// Notify
										if (notify!=null && notify.booleanValue()) {
											String host = notify.value("@host");
											String s = "IMPORTANT: Change to your Mediaflux account details on " + host;								String b = null;
											if (name!=null) {
												b = "Dear " + name;
											} else {
												b = "Dear colleague";
											}

											// If the input domain is of protocol SAML then it's an AAF domain
											// Although the username at the lowest level is the user's email address,
											// what they actually enter is their University username.  So in the 
											// notification, make that substitution
											String oldUserName = userName;
											if (isSAML) {
												oldUserName = ldapUserName;
											}

											b += "\n\n The way you log in to the Mediaflux system " + host + " is changing. University of Melbourne" +
													"\n staff and students can now log in directly with their central account, simplifying and improving the process." +
													"\n\n Your new account details are" +
													"\n      domain : " + ldapDomain + 
													"\n      username : " + ldapUserName +
													"\n      password : <your UoM password>" +
													"\n\n Your old account details are";
											if (!isSAML) {
												b+= "\n      domain : " + domain +
														"\n      username : " + oldUserName;
											} else {							
												b+= "\n      domain : " + domain +
														"\n      provider : The University of Melbourne (shortname='unimelb')" +
														"\n      username : " + oldUserName;
											}
											b+=   "\n\n Please check these account details and that you can log in with the new account in your usual way (e.g. MF Explorer)." +
													"\n\n Your old account will remain active until Monday, 02 September 2019, in case of any issues." +
													"\n However, you should use your new account from now on. It has all the same permissions as your old account." +
													"\n\n Make sure you update any configuration files that you may be using such as " +
													"\n for network mounts and the UoM Java upload/download clients (e.g. on Spartan)." +
													"\n\n The new login method means that regardless of the protocol you are using, you log in with the same credential." +
													"\n That is, whether you are using The Explorer, Desktop, sFTP or SMB, your credential is as above.";
											if (isSAML) {
												b+= "\n Previously, with AAF accounts such as yours, you also had to include the Institutional Provider via drop-down menus in GUIs" +
														"\n or via the institutional provider short-name (''unimelb'') in configuration files. You will no longer do this.";
											}
											b+= "\n\n Details of this change process can be found on the wiki page - https://wiki.cloud.unimelb.edu.au/resplat/doku.php?id=data_management:mediaflux:aaf_to_ad_migration" +
													"\n\n If you encounter any issues or have any questions, please contact us via email to rd-support@unimelb.edu.au" +
													"\n\n Regards, \n Neil Killeen for the Research Computing Services Data Team." +
													"\n The University of Melbourne";

											String bcc = notify.value("@bcc");
											String from = notify.value("@from");
											PluginTask.checkIfThreadTaskAborted();
											notifyUser (executor, email, s, b, bcc, from);
											dmw.add("notification-to-user", email);
										}

										// Set some meta-data if desired on the input user account
										// indicating it was migrated
										if (setMeta) {
											PluginTask.checkIfThreadTaskAborted();
											setMeta (executor, domain, userName, 
													"Migrated to LDAP user '" +
															ldapDomain + ":" + ldapUserName + "' at " + now,
															name, email);
										}
										dmw.pop();
									}
								}
							} else if (iret==1) {
								dmw.add("status", "no-specific-roles-held");
							} else if (iret==2) {
								dmw.add("status", "daris-only-user");
							}
						} else {
							dmw.add("status",  "filtered-out-by-email");
						}
					} 
				}

				// See if we want to filter this one in
				Boolean keep = true;
				if (filter!=null) {
					keep = false;
					XmlDoc.Element el = dmw.root().element("user");
					Collection<String> stati = el.values("status");
					for (String status : stati) {
						if (status.contains(filter)) {
							keep = true;
						}
					}
				}
				if (keep && (!candidatesOnly || (candidatesOnly && isCandidate))) {
					w.addAll(dmw.root().elements());
				}
			}
			dmw.pop();
		}
	}


		private Boolean filterOnEmail (Collection<String> filters, String email) throws Throwable {
			if (filters==null) {
				return true;
			}
			for (String filter : filters) {
				if (email.contains(filter)) {
					return true;
				}
			}
			return false;
		}

		
		
	private Boolean hasBeenMigrated (ServiceExecutor executor, XmlDoc.Element meta) throws Throwable {
		if (meta==null) {
			return false;
		}
		Collection<XmlDoc.Element> mfNotes = meta.elements("mf-note"); 
		if (mfNotes==null) {
			return false;
		}
		if (mfNotes.size()==0) {
			return false;
		}
		for (XmlDoc.Element mfNote : mfNotes) {
			Collection<String> notes = mfNote.values("note");
			if (notes!=null) {
				for (String note : notes) {
					if (note.startsWith("Migrated to")) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private void setMeta (ServiceExecutor executor, String domain, String userName, 
			String note, String name, String email) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", userName);
		// A defect in user.set is wiping out email/name in AAF accounts.
		// So put it back here.
		if (domain.equalsIgnoreCase("aaf")) {
			dm.add("email", email);
			dm.add("name", name);
		}
		dm.push("meta", new String[] {"action", "merge"});
		dm.push("mf-note");
		dm.add("note", note);
		dm.pop();
		dm.pop();
		executor.execute("user.set", dm.root());
	}

	// 
	// return 0 keep
	//        1 drop as no roles
	//        2 drop as daris only roles
	private Integer filterOnRoles (ServiceExecutor executor, String domain, String user, Boolean ignoreDaRISOnly) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", domain+":"+user);
		dm.add("type", "user");
		XmlDoc.Element r = executor.execute("actor.describe", dm.root());
		Collection<XmlDoc.Element> roles = r.elements("actor/role");

		// return true to keep user for migration
		if (roles==null) {
			// No roles so we don't want them
			return 1;
		}
		if (ignoreDaRISOnly) {
			Boolean darisOnly = true;
			for (XmlDoc.Element role : roles) {
				String t = role.value();
				if (!(t.startsWith("daris") || t.startsWith("vicnode.daris"))) {
					darisOnly = false;
				}
			}
			if (darisOnly) {
				return 2;
			}
		}

		// COnsider this user
		return 0;
	}

	private void notifyUser (ServiceExecutor executor, String email, String subject, String body, String bcc,
			String from) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("async", false);
		dm.add("subject", subject);
		dm.add("body", body);
		if (bcc!=null) {
			dm.add("bcc", bcc);
		}
		if (from!=null) {
			dm.add("from", from);
		}
		dm.add("to", email);
		executor.execute("mail.send", dm.root());
	}

	private XmlDocMaker makeDocMaker(XmlDoc.Element authority, String domain, String user, XmlDoc.Element ldapUser) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.push("from");
		if (authority!=null) {
			dm.add(authority);
		}
		dm.add("domain", domain);
		dm.add("user", user);
		dm.pop();

		// The provider, authority and domain are all overloaded with the same string
		dm.push("to");
		dm.add("authority", new String[] {"protocol", "ldap"}, ldapUser.value("@domain"));   
		dm.add("domain", ldapUser.value("@domain"));
		dm.add("user", ldapUser.value());
		dm.pop();
		return dm;
	}


	private Collection<XmlDoc.Element> createUserRecord (ServiceExecutor executor,
			XmlDoc.Element toUser, XmlDocMaker dmw) throws Throwable {


		String domain = toUser.value("@domain");
		String user = toUser.value();
		Boolean isStudent = domain.contains("student");

		if (isStudent) {
			domain = "unimelb-student";
			dmw.add("status", "is-UoM-student");
		} else {
			dmw.add("status", "is-UoM-staff");
		}
		//
		XmlDocMaker dm2 = new XmlDocMaker("args");
		dm2.add("user", new String[] {"domain", domain, "display-name", "unknown", "email", "unknown"},
				user);
		return dm2.root().elements("user");
	}



	private Collection<XmlDoc.Element> findInLDAP (ServiceExecutor executor,
			XmlDoc.Element userRecord, XmlDocMaker dmw) throws Throwable {

		String email = userRecord.value("e-mail");
		Boolean isStudent = email.contains("@student.unimelb.edu.au");
		String  domain = "unimelb";

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("filter", "mail="+email);
		if (isStudent) {
			dm.add("provider", "unimelb-student");
			dm.add("path", "dc=student,dc=unimelb,dc=edu,dc=au");
			domain = "unimelb-student";
			dmw.add("status", "is-UoM-student");
		} else {
			//People means staff. 
			dm.add("provider", "unimelb");
			dm.add("path", "ou=people,dc=unimelb,dc=edu,dc=au");
			dmw.add("status", "is-UoM-staff");
		}
		// The uidAttrName "sAMAccountName" can be discovered from the LDAP
		// domain descriptions.  FOr this one off migration process, ok to hard wire
		Collection<XmlDoc.Element> users = executor.execute("ldap.search", dm.root()).elements("entry");	
		if (users==null) {
			return null;
		}
		if (users.size()==0) {
			return null;
		}

		// Populate a simpler record with user details
		XmlDocMaker dm2 = new XmlDocMaker("args");
		for (XmlDoc.Element user : users) {
			dm2.add("user", new String[] {"domain", domain, "display-name", user.value("displayName"), "email", email },
					user.value("sAMAccountName"));
		}
		return dm2.root().elements("user");
	}
}