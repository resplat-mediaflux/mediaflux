/**
 * @author Neil Killeen
 * <p>
 * Copyright (c) 2016, The University of Melbourne, Australia
 * <p>
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import java.util.List;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.atomic.AtomicTransaction;
import arc.mf.plugin.atomic.AtomicOperation;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;
import au.org.vicnode.mflux.plugin.util.Util;

public class SvcProjectUserRemove extends PluginService {


    private Interface _defn;
	private  static Boolean ALLOW_PARTIAL_CID = true;


    public SvcProjectUserRemove() throws Throwable {
        _defn = new Interface();
        addProjectRemoveArgs(_defn,ALLOW_PARTIAL_CID);
    }

    public static void addProjectRemoveArgs(Interface _defn, boolean partial_cid) throws Throwable {
        _defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain for the user.", 0, 1));
        _defn.add(new Interface.Element("user", StringType.DEFAULT, "A user name to remove project access for.", 0, 1));
        _defn.add(new Interface.Element("role", StringType.DEFAULT, "A role to remove project access for (that is, the role is a project member).", 0, 1));
        SvcProjectDescribe.addMandatoryProjectID(_defn, partial_cid);
        _defn.add(new Interface.Element("notify", BooleanType.DEFAULT, "Notify the user and the project administrator(s) by email. Defaults to false.", 0, 1));
        _defn.add(new Interface.Element("provision", BooleanType.DEFAULT, "Notify the standard provisioning user (that holds the admin role for all projects) as well (default false) .", 0, 1));

    }

    public Access access() {
        return ACCESS_ADMINISTER;
    }

    public Interface definition() {
        return _defn;
    }

    public String description() {
        return "Removes access to the given project for a specific actor (a user or a role or both), created by vicnode.project.create.  Emails are sent to the user and all administrators of the Project if requested.";
    }

    public String name() {
        return Properties.SERVICE_ROOT + "project.user.remove";
    }

    public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
        // Inputs
        final String domain = args.value("domain");
        final String user = args.value("user");
        final String role = args.value("role");
        if (role == null && user == null) {
            throw new Exception("You must supply a user or a role");
        }
        if (user != null && domain == null) {
            throw new Exception("You must supply a domain and a user.");

        }
        final String projectID = args.value("project-id");
        final Boolean notify = args.booleanValue("notify", false);
        final Boolean dropProvision = !(args.booleanValue("provision", false));

		// Validate.
		String s[] = Project.validateProject(executor(), projectID, ALLOW_PARTIAL_CID);
		String projectID2 = s[1];


        // Atomic creation so if fails, all partial resources are destroyed
        try {
            new AtomicTransaction(new AtomicOperation() {

                @Override
                public boolean execute(ServiceExecutor executor) throws Throwable {
                    remove(domain, user, role, projectID2, notify, dropProvision, w);
                    return false;
                }
            }).execute(executor());
        } catch (Throwable t) {

            // Rethrow
            throw new Exception(t);
        }

    }


    private void remove(String domain, String user, String roleUserActorName,
                        String projectID, Boolean notify, Boolean dropProvision, XmlWriter w) throws Throwable {

        ServiceExecutor executor = executor();

        //Get the user
        String userActorName = null;
        Boolean done = false;
        Vector<String> revokedUser = new Vector<String>();
        if (domain != null && user != null) {
            userActorName = domain + ":" + user;

            XmlDoc.Element actor = Util.describeActor(executor, userActorName, "user");
            // User
            if (actor != null) {
                Collection<XmlDoc.Element> actorRoles = actor.elements("actor/role");

                if (actorRoles != null) {
                    // Get the allowed roles for this project
                    Vector<String> allowedRoles = Project.actualUserRoleNames(projectID);

                    // Iterate through user's top-level roles
                    for (XmlDoc.Element actorRole : actorRoles) {
                        String roleName = actorRole.value();

                        // See if user role matches a project role
                        // and if so revoke it.
                        for (String allowedRole : allowedRoles) {
                            if (roleName.equals(allowedRole)) {
                                User.grantRevokeRole(executor, userActorName, roleName, false);
                                revokedUser.add(roleName);
                                w.push("user", new String[]{"name", userActorName});
                                w.add("revoked-role", roleName);
                                // revoke the standard user role if it is the only thing left
                                String revokedStandardUserRoleName = revokeStandardUserRoleIfOnly(executor, domain, user);
                                if (revokedStandardUserRoleName != null) {
                                    w.add("revoked-role", revokedStandardUserRoleName);
                                }
                                w.pop();
                                done = true;
                            }
                        }
                    }
                }
            }

        }

        // Role
        Vector<String> revokedRole = new Vector<String>();
        if (roleUserActorName != null) {
            XmlDoc.Element actor = Util.describeActor(executor, roleUserActorName, "role");
            if (actor != null) {
                Collection<XmlDoc.Element> actorRoles = actor.elements("actor/role");

                // Get the allowed roles for this project
                Vector<String> allowedRoles = Project.actualUserRoleNames(projectID);

                // Iterate through role's top-level roles
                for (XmlDoc.Element actorRole : actorRoles) {
                    String roleName = actorRole.value();

                    // See if the actor's role matches an allowed project role
                    for (String allowedRole : allowedRoles) {
                        if (roleName.equals(allowedRole)) {
                            Util.grantRevokeRoleToRole(executor, roleUserActorName, roleName, false);
                            revokedRole.add(roleName);
                            w.push("role", new String[]{"name", roleUserActorName});
                            w.add("revoked-role", roleName);
                            w.pop();
                        }
                    }
                }
            }
        }

        // Notify
        if (notify && done) {
            Project.notifyUserAndAdminsOfRoleChanges(executor, projectID,
                    userActorName, roleUserActorName, null,
                    dropProvision, false, true, false, w);
        }

    }

    private static String revokeStandardUserRoleIfOnly(ServiceExecutor executor, String domain, String user) throws Throwable {
        String standardUserRoleName = Properties.getStandardUserRoleName(executor);
        String actorName = domain + ":" + user;

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", "user");
        dm.add("name", actorName);
        XmlDoc.Element ae = executor.execute("actor.describe", dm.root()).element("actor");
        if (ae.hasSubElements()) {
            // the user actor has role(s) or perms(s)
            List<Element> subElements = ae.elements();
            if (subElements.size() == 1) {
                // the user actor has only one role/perm
                XmlDoc.Element se = subElements.get(0);
                if ("role".equals(se.name()) && "role".equals(se.value("@type")) && standardUserRoleName.equals(se.value())) {
                    // the user actor has only one role, which is VicNode:standard-user
                    dm = new XmlDocMaker("args");
                    dm.add("type", "user");
                    dm.add("name", actorName);
                    dm.add("role", new String[]{"type", "role"}, standardUserRoleName);
                    executor.execute("actor.revoke", dm.root());
                    return standardUserRoleName;
                }
            }
        }
        return null;
    }

}
