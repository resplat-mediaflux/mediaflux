/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectContentCopy;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;

/**
 * 
 * @author nebk
 *
 */
public class SvcProjectMetaDataMissing extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.metadata.missing";

	private Interface _defn;

	public SvcProjectMetaDataMissing() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",
				0, 1));
		_defn.add(new Interface.Element("type", StringType.DEFAULT,
				"Document type of interest.", 1, Integer.MAX_VALUE));
		//
		SvcProjectDescribe.addProdType (_defn);
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns a list of  projects which are missing the specified meta-data document types.";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String projType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		String keyString = args.value("contains");
		if (keyString != null) {
			keyString = keyString.toUpperCase();
		}
		Collection<String> types = args.values("type");

		// List of projects
		Collection<String> projectIDs = Project.projectIDs(executor(), true, true);
		if (projectIDs.size() == 0) {
			return;
		}
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			if (Project.keepOnProjectType(ph, projType) && 
				Project.keepOnKeyString(ph.id(), keyString)) {
				find(executor(), ph, types, w);
			}
		}
	}

	private void find (ServiceExecutor executor, ProjectHolder ph,
			Collection<String> types, XmlWriter w)
					throws Throwable {

		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("project");
		dmOut.add("id", ph.id());
		
		// Get namespace meta-data
		XmlDoc.Element nsMeta = ph.metaData();
		Collection<XmlDoc.Element> docs = nsMeta.element("namespace/asset-meta").elements();
		
		Boolean some  = false;
		for (String type : types) {
			Boolean found = false;
			for (XmlDoc.Element doc : docs) {
				String docType = doc.qname();
				if (type.equals(docType)) {
					found = true;
				}
			}
			if (!found) {
				dmOut.add("type", new String[] {"missing", "true"}, type);
				some = true;
			}
		}
		dmOut.pop();
		if (some) {
			w.addAll(dmOut.root().elements());
		}
	}

}
