/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.*;
import arc.mf.plugin.PluginService.Interface;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.UrlType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectNameSpaceChildFind extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectNameSpaceChildFind() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectIDs (_defn, allowPartialCID);
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A case-sensitive string that the child namespace name (just the relative part) of interest must contain.",
				0, 1));
		_defn.add(new Interface.Element("has-asset", BooleanType.DEFAULT, "Is there an asset with the same name as a namespace? (Default is to not check, false)", 0, 1));
		_defn.add(new Interface.Element("url", UrlType.DEFAULT, "If specified, file on the server to save the output as an XML file. Of the form file:<path>", 0, 1));
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.namespace.child.find";
	}

	public String description() {
		return "Service to recursively list any namespaces that contain the given string in their name or have an asset of the same name.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Parse inputs				
		Collection<String> theProjectIDs = args.values("project-id");
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs!=null) {
			for (String theProjectID : theProjectIDs) {
				String [] t = Project.validateProject(executor(), theProjectID, allowPartialCID);
				projectIDs.add(t[1]);
			}
		} else{
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) {
			return;
		}

		// 
		String contains = args.value("contains");
		Boolean hasAsset = args.booleanValue("has-asset", false);
		if (contains==null && !hasAsset) {
			throw new Exception("You must give one of :contains or :has-asset");
		}
		String url = args.value("url");

		// Iterate over projects
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
			PluginTask.checkIfThreadTaskAborted();
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("namespace", ph.nameSpace());
			if (contains!=null) {
				dm.add("contains", contains);
			}
			if (hasAsset) {
				dm.add("has-asset", true);
			}
			if (url!=null) {
				dm.add("url", url);
			}
			XmlDoc.Element r = executor().execute("unimelb.asset.namespace.child.find", dm.root());
			if (r!=null && r.hasSubElements()) {
				w.push("project", new String[] {"id", ph.id()});
				w.addAll(r.elements());
				w.pop();
			}
		}
	}
}
