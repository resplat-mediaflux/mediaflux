/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;

public class SvcProjectContentAccessLast extends PluginService {

	private Interface _defn;
	private static Boolean allowPartialCID = true;
	private static String SERVICE_NAME = Properties.SERVICE_ROOT + "project.content.access.last";



	public SvcProjectContentAccessLast() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectIDs (_defn, allowPartialCID);
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",
				0, 1));
		_defn.add(new Interface.Element("parent", StringType.DEFAULT,
				"The parent namespace to consider when looking for projects. Should start with a '/'. Defaults to all parents.",
				0, 1));
		_defn.add(new Interface.Element("where", StringType.DEFAULT,
				"An additional predicate to restrict the seelction of assets, for example, a time predicate.",
				0, 1));
		_defn.add(new Interface.Element("sort", BooleanType.DEFAULT,
				"Sort the projects in reverse access time order. Default false.",
				0, 1));
		_defn.add(new Interface.Element("csv", BooleanType.DEFAULT,
				"Create a server-side CSV file in the Mediaflux termporary directory called " + SERVICE_NAME + "_<current time>.csv.  Default false.",
				0, 1));
		addProdType(_defn);
		addRepType(_defn);
	}

	static public void addProdType (Interface defn) throws Throwable {
		defn.add(new Interface.Element("prod-type", new EnumType(Properties.projectTypes),
				"Select from 'all', (all types), 'production' (default; only production projects), 'test' (only test projects), 'closed-stub' (only closed-stub projects).",
				0, 1));
	}

	static public void addRepType (Interface defn) throws Throwable {
		defn.add(new Interface.Element("rep-type", new EnumType(Properties.repTypes),
				"Select from 'all', (default, all projects), 'replication' (only projects which are being replicated), 'no-replication' (projects that are not being replicated).",
				0, 1));
	}


	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns the most recent access content time for the specified projects. Projects are sorted in reverse time order.";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		Collection<String> theProjectIDs = args.values("project-id");
		String repType = args.stringValue("rep-type", Properties.repTypes[0]);
		String projType = args.stringValue("prod-type", Properties.PROJECT_TYPE_PROD);
		String keyString = args.value("contains");
		if (keyString != null) {
			keyString = keyString.toUpperCase();
		}
		String parent = args.value("parent");
		Boolean sort = args.booleanValue("sort",  false);
		String where = args.value("where");
		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs != null) {
			for (String pid : theProjectIDs) {
				String s[] = Project.validateProject(executor(), pid, allowPartialCID);
				projectIDs.add(s[1]);
			}
		} else {
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size() == 0) {
			return;
		}
		Boolean doCSV = args.booleanValue("csv", false);

		// Iterate  over projects 
		XmlDocMaker dm = new XmlDocMaker("container");
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);

			// FInd replication asset processing queue name
			String qName = ProjectReplicate.isProjectReplicated(ph);	
			// See if we want to process this one
			if (Project.keepOnRepType (executor(), qName, repType) && 
					Project.keepOnProjectType(ph, projType) && 
					Project.keepOnKeyString(ph.id(), keyString) &&
					Project.keepOnParent(ph, parent)) {
				dm.push("project");
				dm.add("id", projectID);
				checkAccess (executor(), ph, where, dm);
				Long n = countNamespaces (executor(), ph);
				dm.add("number-namespaces", n);
				dm.pop();
			}
		}
		XmlDoc.Element result = dm.root();
		Collection<XmlDoc.Element> results = result.elements();

		// Optionally sort in reverse time order 
		if (results!=null && results.size()>0) {
			ArrayList<XmlDoc.Element> sortedResults = new ArrayList<XmlDoc.Element>(results);

			if (sort) {
				Collections.sort(sortedResults, new Comparator<XmlDoc.Element>() {
					@Override
					public int compare(XmlDoc.Element o1, XmlDoc.Element o2) {
						//				System.out.println("o1="+o1);
						//				System.out.println("o2="+o2);
						Date d1 = null;
						try {
							String sd1 = o1.value("last-content-access");
							if (sd1==null || (sd1!=null && sd1.equals("unknown"))) {
								return 1;
							}
							//					System.out.println("sd1="+sd1);
							//					d1 = DateUtil.dateFromString(sd1, "dd-MMM-yyyy HH:mm:ss");
							//					System.out.println("d1="+d1.toString());
							d1 = o1.dateValue("last-content-access");
						} catch (Throwable t) {
							System.out.println("failed 1 with " + t.getMessage());
						}
						Date d2 = null;
						try {
							String sd2 = o2.value("last-content-access");
							if (sd2==null || (sd2!=null && sd2.equals("unknown"))) {
								return 1;
							}
							//					System.out.println("sd2="+sd2);
							//					d2 = DateUtil.dateFromString(sd2, "dd-MMM-yyyy HH:mm:ss");
							//					System.out.println("d2="+d2.toString());
							d2 = o2.dateValue("last-content-access");
						} catch (Throwable t) {
							System.out.println("failed 2 with " + t.getMessage());
						}
						if (d1.before(d2)) {
							return -1;
						} else if (d1.after(d2)) {
							return 1;
						} else {
							return 0;
						}
					}
				});
				w.addAll(sortedResults);
				results = sortedResults;
			} else {
				w.addAll(results);
			}

			// By reference, result now contains sorted Results if we have sorted
			// Write client side XML file. 
			if (outputs != null) {
				Util.writeClientSideXMLFile(outputs.output(0), result);
			}

			if (doCSV) {
	            String time = DateUtil.todaysTime(0);
				File f = PluginTask.createTemporaryFile();
				String p = f.getParent();
				String t = p + "/"+SERVICE_NAME+"_"+time+".csv";
				PrintWriter csv = new PrintWriter(new BufferedWriter(new FileWriter(f)));
				csv.printf("project-id, n-assets, last-content-access, last-content-create, last-content-modification, number-namespaces\n");
				for (XmlDoc.Element r : results) {
					String t1 = r.stringValue("last-content-access", "unknown");
					String t2 = r.stringValue("last-asset-create", "unknown");
					String t3 = r.stringValue("last-asset-modification", "unknown");
							csv.printf("%s,%s,%s,%s,%s,%s\n", 
							r.value("id"), r.value("nassets"), t1, t2, t3,
							r.value("number-namespaces"));
				}
				csv.close();
				//
				Path in = Paths.get(f.getPath());
				Path out = Paths.get(t);
				Files.move(in, out);
			}
		}
	}


	private Long countNamespaces (ServiceExecutor executor, ProjectHolder ph) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", ph.nameSpace());
		XmlDoc.Element r = executor.execute("asset.namespace.count", dm.root());
		return r.longValue("count");
	}


	private void checkAccess (ServiceExecutor executor, ProjectHolder ph,
			String where, XmlDocMaker w) throws Throwable {
		String namespace = ph.nameSpace();
		String where2 = "namespace>='" + namespace + "'";	
		if (where!=null) {
			where2 += " and (" + where + ")";
		}
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", where2);
		XmlDoc.Element r = executor.execute("unimelb.asset.content.access.last", dm.root());
		if (r!=null) {
			w.addAll(r.elements());
		}

	}

}
