package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.PasswordType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetNamespaceUtil;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.XmlUtils;
import unimelb.utils.AssetPathUtils;
import unimelb.utils.ThrowableUtils;

/**
 * This service suppose to be called by asset.shareable.self.upload.end service.
 * <p>
 * :shareable-id argument must be supplied by the calling service.
 *
 * @author wliu5
 */
public class SvcFacilityShareableUploadComplete extends PluginService {

    // NOTE: this service has role (permission)
    // ds-admin:instrument-upload-admin to access the global asset namespace
    // /local-admin/instrument-upload-manifests/
    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.complete";

    public static final int EXCEPTION_NOTIFICATION_BODY_MAX_SIZE = 10240; // The email body should not be too big.

    public static final int INTERACTION_ADD_EMAIL_DICT_ENTRY_DURATION_DAYS = 7;

    public static final String FACILITY_SHAREABLE_UPLOAD_LOG = "facility-shareable-upload";

    static PluginLog log() {
        return PluginLog.log(FACILITY_SHAREABLE_UPLOAD_LOG);
    }

    static void logWarning(String msg) {
        log().add(PluginLog.WARNING, msg);
    }

    private final Interface _defn;

    public SvcFacilityShareableUploadComplete() {
        _defn = new Interface();
        _defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "Shareable id.", 1, 1));
        _defn.add(new Interface.Element("upload-id", LongType.POSITIVE, "Upload id.", 1, 1));

        Interface.Element args = new Interface.Element("args", XmlDocType.DEFAULT, "Arguments.", 0, 1);
        args.add(new Interface.Element(Shareable.ARG_NAME_SUFFIX, StringType.DEFAULT, "Upload namespace name suffix.",
                0, 1));
        args.add(new Interface.Element(Shareable.ARG_KEYWORDS, StringType.DEFAULT, "Comma separated keywords.", 0, 1));
        args.add(new Interface.Element(Shareable.ARG_SHARE_WITH, StringType.DEFAULT,
                "The recipient email addresses separated with comma or semicolon.", 0, 1));
        args.add(new Interface.Element(Shareable.ARG_EMAIL_SUBJECT_SUFFIX, StringType.DEFAULT,
                "Notification email subject suffix.", 0, 1));
        args.add(new Interface.Element(Shareable.ARG_DATA_NOTE, StringType.DEFAULT,
                "Note for the upload.", 0, 1));
        _defn.add(args);

        // options
        addOptions(_defn, null);
    }

    static void addOptions(Interface defn, Interface.Element root) {

        /*
         * generate manifest asset
         */
        Interface.Element manifest = new Interface.Element("manifest", BooleanType.DEFAULT,
                "Generate manifest asset. Defaults to true.", 0, 1);
        manifest.add(new Interface.Attribute("content-type-statistics", BooleanType.DEFAULT,
                "generate content type statistics. Defaults to true.", 0));
        manifest.add(new Interface.Attribute("file-size-statistics", BooleanType.DEFAULT,
                "generate file size statistics. Defaults to true.", 0));
        if (defn != null) {
            defn.add(manifest);
        } else {
            if (root != null) {
                root.add(manifest);
            }
        }

        /*
         * set worm state
         */
        Interface.Element worm = new Interface.Element("worm", XmlDocType.DEFAULT,
                "Sets the WORM state for the uploaded assets. If not set, no WORM state are applied to the uploaded assets.",
                0, 1);
        worm.add(new Interface.Element("can-add-versions", BooleanType.DEFAULT,
                "Indicates whether new versions of metadata and content are allowed when in the WORM state. Defaults to false.",
                0, 1));
        worm.add(new Interface.Element("can-move", BooleanType.DEFAULT,
                "Indicates whether the collection, or asset(s) can be moved. Defaults to 'true'.", 0, 1));
        worm.add(new Interface.Element("expiry", DateType.DEFAULT,
                "The expiry time of the WORM state. If not set, then no expiry.", 0, 1));
        if (defn != null) {
            defn.add(worm);
        } else {
            if (root != null) {
                root.add(worm);
            }
        }

        /*
         * notify facility managers?
         */
        Interface.Element notify = new Interface.Element("notify", BooleanType.DEFAULT,
                "Send email notifications when uploads are complete to email addresses specified in property: '"
                        + Shareable.PROPERTY_FACILITY_NOTIFICATION_TO + "'. Defaults to true.",
                0, 1);
        if (defn != null) {
            defn.add(notify);
        } else {
            if (root != null) {
                root.add(notify);
            }
        }

        /*
         * share; options for creating download shareable and user interaction
         */
        Interface.Element share = new Interface.Element("share", XmlDocType.DEFAULT,
                "Options for sharing the data via download shareable, direct download link and user interaction.", 0,
                1);
        if (defn != null) {
            defn.add(share);
        } else {
            if (root != null) {
                root.add(share);
            }
        }

        share.add(new Interface.Attribute("always", BooleanType.DEFAULT,
                "Always generate download links and user interaction, even if there is no recipient email specified. Defaults to true.",
                0));

        share.add(new Interface.Element("duration", IntegerType.POSITIVE,
                "Number of days before the expiry of the download shareable and user interaction. Defaults to 0, which never expires.",
                0, 1));

        share.add(new Interface.Element("dispatch-metadata", BooleanType.DEFAULT,
                "Dispatch the metadata about the uploaded data, such as keywords and note, along with the data through an additional XML file. Defaults to false.",
                0, 1));

        // share/download: options for creating download shareable
        Interface.Element shareDownload = new Interface.Element("download", XmlDocType.DEFAULT,
                "Options for creating download shareable.", 0, 1);
        share.add(shareDownload);

        // share/download/@type
        shareDownload.add(
                new Interface.Attribute("type", new EnumType(new String[]{"client", "direct", "threshold", "both"}),
                        "Type(s) of download link(s) to generate. Defaults to client.", 0));

        // share/download/threshold
        Interface.Element shareDownloadThreshold = new Interface.Element("threshold", new LongType(1, Long.MAX_VALUE),
                "The threshold of total content size to determine what type of download link to be generated. Ignored if type is not set to threshold.",
                0, 1);
        shareDownloadThreshold
                .add(new Interface.Attribute("unit", new EnumType(new String[]{"B", "KB", "MB", "GB", "TB"}),
                        "Unit of the threshold. Defaults to B(byte).", 0));
        shareDownload.add(shareDownloadThreshold);

        // share/download/client
        Interface.Element shareDownloadClient = new Interface.Element("client", XmlDocType.DEFAULT,
                "Settings for Data Mover client download shareable link. Ignored if no Data Mover download shareable is generated.",
                0, 1);
        shareDownloadClient.add(new Interface.Element("enable-selection", BooleanType.DEFAULT,
                "Whether or not the recipient should be able to select subsets of the assets being shared. Defaults to true.",
                0, 1));
        shareDownloadClient
                .add(new Interface.Element("password", PasswordType.DEFAULT, "A password for the shareable.", 0, 1));
        shareDownloadClient.add(new Interface.Element("notify", BooleanType.DEFAULT,
                "When data is downloaded, send notifications to facility contacts specified in property: '"
                        + Shareable.PROPERTY_FACILITY_NOTIFICATION_TO + "'. Defaults to false.",
                0, 1));
        shareDownload.add(shareDownloadClient);

        // share/download/direct
        Interface.Element shareDownloadDirect = new Interface.Element("direct", XmlDocType.DEFAULT,
                "Settings for direct download link. Ignored if no direct download link is generated.", 0, 1);
        shareDownloadDirect.add(new Interface.Element("compress", BooleanType.DEFAULT,
                "Compress the archive file. Defaults to true.", 0, 1));
        shareDownloadDirect.add(new Interface.Element("generate-password", BooleanType.DEFAULT,
                "Generate a password for the direct download link. Defaults to false.", 0, 1));
        shareDownload.add(shareDownloadDirect);

        // share/interaction: options for creating user interaction
        Interface.Element shareInteraction = new Interface.Element("interaction", XmlDocType.DEFAULT,
                "Options for creating user interaction.", 0, 1);
        share.add(shareInteraction);

        shareInteraction.add(new Interface.Element("ifexists", new EnumType(new String[]{"ignore", "rename"}),
                "Action to take if the asset namespace exists at destination. Default to ignore.", 0, 1));

        shareInteraction.add(new Interface.Element("notify",
                new EnumType(new String[]{"all", "facility", "user", "none"}),
                "Whom to receive notifications when user interaction is completed. Set to facility to send to facility contacts specified in property: "
                        + Shareable.PROPERTY_FACILITY_NOTIFICATION_TO
                        + ". Set to user to send to the end user who invoke the interaction. Defaults to none.",
                0, 1));

        // share/note
        share.add(
                new Interface.Element("note", StringType.DEFAULT, "Note to be added to the user notification.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_MODIFY;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Post-process the data uploaded by data mover client.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

        // @formatter:off
        // System.out.println(String.format("DEBUG: %s: %s", SERVICE_NAME, XmlPrintStream.outputToString(args)));
        // @formatter:on

        try {

            // shareable id
            final String shareableId = args.value("shareable-id");

            // upload id
            final String uploadId = args.value("upload-id");

            // log
            logWarning("[shareable: " + shareableId + ", upload: " + uploadId + "] started executing " + SERVICE_NAME);

            Date currentTime = executor().execute("server.date").dateValue("date");

            // shareable
            final XmlDoc.Element shareable = Shareable.describe(executor(), shareableId);

            // uploadNotificationEmails
            Collection<String> notificationRecipients = Shareable.getFacilityNotificationRecipients(shareable);
            boolean notify = notificationRecipients != null && args.booleanValue("notify", true);

            /*
             * resolve source/upload namespace path
             */
            String uploadNS = UploadShareable.getUploadNamespace(executor(), shareable, uploadId, false);
            if (uploadNS == null) {
                throw new Exception("Could not find upload: " + uploadId + " of shareable " + shareableId);
            }

            // Move to mapped destination if upload mapping is defined
            UploadMapping mapping = UploadMapping.get(executor(), shareable);
            String uploadPathContext = UploadShareable.getUploadPathContext(shareable, uploadId);

            if (mapping != null && uploadPathContext != null) {
                logWarning("[shareable: " + shareableId + ", upload: " + uploadId + ", contextSrcPath: "
                        + uploadPathContext + "]: found upload mapping (asset: " + mapping.assetId()
                        + "), start evaluating the mapping rules for the context source path...");
                uploadNS = moveToMappedDestination(executor(), uploadNS, uploadPathContext, mapping, args);
            } else if (mapping != null && uploadPathContext == null) {
                // log warning ...
                logWarning("[shareable: " + shareableId + ", upload: " + uploadId + ", contextSrcPath: "
                        + uploadPathContext + "]: found upload mapping (asset: " + mapping.assetId()
                        + "), however, the context source path is not presented.");
            } else if (mapping == null && uploadPathContext != null) {
                // log warning ...
                logWarning("[shareable: " + shareableId + ", upload: " + uploadId + ", contextSrcPath: "
                        + uploadPathContext
                        + "]: no upload mapping is defined although the source context path is presented.");
            }

            // TODO make sure uploadNS is always correct if it was moved to mapped
            // destination...

            XmlDoc.Element shareOptions = args.elementExists("share") ? args.element("share")
                    : new XmlDoc.Element("share");

            XmlDoc.Element uploadArgs = args.elementExists("args") ? args.element("args") : new XmlDoc.Element("args");

            /*
             * NOTE: To code below is to work around the issue that this service is called
             * repeatedly on the same upload. *
             */
            // Begin of WA Code
            boolean manifestExists = Manifest.manifestAssetExists(executor(), uploadNS);
            boolean userInteractionExists = Interaction.userInteractionExists(executor(), shareableId, uploadId);
            if (manifestExists || userInteractionExists) {
                logRepeatedExecution(args, shareableId, uploadId, uploadNS);
                logWarning(
                        "[shareable: " + shareableId + ", upload: " + uploadId + "] skipped executing " + SERVICE_NAME);
                w.add("executed", false);
                return;
            }
            // End of WA Code

            // generate shareable download and send link via emails.
            Collection<String> shareWith = args.values("args/" + Shareable.ARG_SHARE_WITH);

            Collection<String> shareRecipients = Emails.parse(shareWith);
            SharedItems sharedItems = null;
            boolean alwaysShare = shareOptions.booleanValue("@always", true);

            boolean dispatchMetadata = shareOptions.booleanValue("dispatch-metadata", false);
            if (dispatchMetadata) {
                String keywords = args.value("args/" + Shareable.ARG_KEYWORDS);
                String dataNote = args.value("args/" + Shareable.ARG_DATA_NOTE);
                if (keywords != null || dataNote != null) {
                    logWarning("[shareable: " + shareableId + ", upload: " + uploadId + "] creating '" + uploadNS + "/"
                            + Metadata.METADATA_FILE_NAME + "'");
                    String metadataAssetId = Metadata.createMetadataAsset(executor(), uploadNS, keywords, dataNote);
                    logWarning("[shareable: " + shareableId + ", upload: " + uploadId + "] created " + uploadNS + "/"
                            + Metadata.METADATA_FILE_NAME + "' (asset_id: " + metadataAssetId + ")");
                }
            }
            if (shareRecipients != null || alwaysShare) {
                sharedItems = createSharedItems(executor(), shareable, uploadId, uploadNS, uploadArgs,
                        shareOptions);
            }

            // generate manifest (defaults to true)
            boolean manifest = args.booleanValue("manifest", true);
            if (manifest) {
                boolean getContentTypeStatistics = args.booleanValue("manifest/@content-type-statistics", true);
                boolean getFileSizeStatistics = args.booleanValue("manifest/@file-size-statistics", true);
                Integer shareDuration = shareOptions.intValue("duration", null);
                Manifest.createManifestAsset(executor(), shareable, uploadId, uploadNS, uploadArgs, currentTime,
                        getContentTypeStatistics, getFileSizeStatistics, shareDuration,
                        sharedItems == null ? null : sharedItems.downloadShareable,
                        sharedItems == null ? null : sharedItems.userInteraction,
                        sharedItems == null ? null : sharedItems.downloadLink);
            }

            // set worm state
            XmlDoc.Element worm = args.element("worm");
            if (worm != null) {
                setWorm(executor(), uploadNS, worm);
            }

            if (notify) {
                // Notify platform staff
                new UploadNotification(shareable, uploadNS, uploadArgs, shareRecipients, sharedItems,
                        notificationRecipients).send(executor());
            }

            if (shareRecipients != null && !shareRecipients.isEmpty()) {
                // send (download & UI) invitation to end users
                logWarning("[shareable: " + shareableId + ", upload: " + uploadId + "] sending invitation to '"
                        + shareWith + "'");
                sendInvitations(executor(), sharedItems, shareRecipients);

                String emailDict = shareable.value("args/definition/element[@name='share-with']/restriction/dictionary[@suggested='true']");
                String emailDictAddEntry = Shareable.getProperty(shareable, Shareable.PROPERTY_FACILITY_EMAIL_DICT_ADD_ENTRY);
                if (emailDict != null && emailDictAddEntry != null && !emailDictAddEntry.equalsIgnoreCase("no")) {
                    Collection<String> newEmails = getEntriesNotInDict(executor(), shareRecipients, emailDict);
                    if (!newEmails.isEmpty()) {
                        if ("confirm".equalsIgnoreCase(emailDictAddEntry)) {
                            if (notificationRecipients != null && !notificationRecipients.isEmpty()) {
                                createUserInteractionForAddingNewEmailsAndNotify(executor(), shareableId, uploadId,
                                        newEmails, emailDict, notificationRecipients);
                            }
                        } else if ("yes".equalsIgnoreCase(emailDictAddEntry)) {
                            addDictionaryEntries(executor(), emailDict, newEmails);
                            if (notificationRecipients != null && !notificationRecipients.isEmpty()) {
                                notifyOfNewEmailsAdded(executor(), newEmails, emailDict, notificationRecipients);
                            }
                        }
                    }
                }
            }
            logWarning(
                    "[shareable: " + shareableId + ", upload: " + uploadId + "] completed executing " + SERVICE_NAME);
            w.add("executed", true);
        } catch (Throwable e) {
            notifyAdminOfException(executor(), e, args);
            notifyFacilityOfException(executor(), e, args);
            try {
                final String shareableId = args.value("shareable-id");
                final String uploadId = args.value("upload-id");
                logWarning(
                        "[shareable: " + shareableId + ", upload: " + uploadId + "] failed executing " + SERVICE_NAME);
            } catch (Throwable t) {
                t.printStackTrace();
            }
            throw e;
        }
    }

    private static Set<String> getEntriesNotInDict(ServiceExecutor executor, Collection<String> entries, String dict) throws Throwable {
        Collection<String> dictEntries = executor.execute("dictionary.entries.list",
                "<args><dictionary>" + dict + "</dictionary><case-sensitive>false</case-sensitive></args>",
                null, null).values("term");
        Set<String> result = new LinkedHashSet<>();
        if (dictEntries == null || dictEntries.isEmpty()) {
            result.addAll(entries);
        } else {
            for (String entry : entries) {
                if (!dictEntries.contains(entry.toLowerCase())) {
                    result.add(entry);
                }
            }
        }
        return result;
    }

    private static void logRepeatedExecution(XmlDoc.Element args, String shareableId, String uploadId,
                                             String uploadNS) {
        StringBuilder sb = new StringBuilder();
        sb.append("Repeated execution of service: ").append(SERVICE_NAME).append(": ");
        sb.append("shareable=").append(shareableId).append(", ");
        sb.append("upload=").append(uploadId).append(", ");
        sb.append("namespace='").append(uploadNS).append("', ");
        sb.append("args=").append(args);
        log().add(PluginLog.ERROR, sb.toString());
    }

    private static void notifyAdminOfException(ServiceExecutor executor, Throwable exception, XmlDoc.Element args) {
        String shareableId = null;
        try {
            shareableId = args.value("shareable-id");
            String adminEmail = Properties.getApplicationProperty(executor,
                    Properties.APPLICATION_PROPERTY_OPERATIONS_EMAIL, Properties.APPLICATION_PROPERTY_APP);
            if (adminEmail == null) {
                return;
            }

            String serverUUID = executor.execute("server.uuid").value("uuid");
            XmlDocMaker dm = new XmlDocMaker("args");

            // subject
            String subject = "[" + serverUUID + "] Error: '" + SERVICE_NAME + "' failed to handle upload for shareable "
                    + shareableId;
            dm.add("subject", subject);

            // recipient address
            dm.add("to", adminEmail);

            // body/message
            StringBuilder body = new StringBuilder();
            body.append("Shareable ID: ").append(shareableId).append("\n");

            XmlDoc.Element shareable = Shareable.describe(executor, shareableId);
            String facilityName = shareable == null ? null : Shareable.getFacilityName(shareable);
            body.append("Facility Name: ").append(facilityName).append("\n");

            body.append("Service Call:\n");
            body.append(XmlUtils.getServiceCommand(SERVICE_NAME, args)).append("\n\n");

            body.append("Exception:\n");
            ThrowableUtils.printStackTrace(exception, body, EXCEPTION_NOTIFICATION_BODY_MAX_SIZE);
            body.append("\n");
            dm.add("body", body);

            executor.execute("mail.send", dm.root());
        } catch (Throwable ex1) {
            log().add(PluginLog.ERROR,
                    SERVICE_NAME + ": failed to send exception notification for upload shareable: " + shareableId, ex1);
        }
    }

    private static void notifyFacilityOfException(ServiceExecutor executor, Throwable exception, XmlDoc.Element args) {
        String shareableId = null;
        Date now = new Date();
        try {
            shareableId = args.value("shareable-id");
            if (shareableId == null) {
                throw new IllegalArgumentException(
                        "Failed to resolve shareable-id from args of service: " + SERVICE_NAME);
            }
            XmlDoc.Element shareable = Shareable.describe(executor, shareableId);
            if (shareable == null) {
                throw new Exception("Failed to describe shareable " + shareableId);
            }
            Collection<String> facilityRecipients = Shareable.getFacilityNotificationRecipients(shareable);
            if (facilityRecipients == null || facilityRecipients.isEmpty()) {
                return;
            }
            String facilityName = Shareable.getFacilityName(shareable);
            String uploadId = args.value("upload-id");
            String uploadParentName = UploadShareable.getUploadParentName(shareable, uploadId);

            XmlDocMaker dm = new XmlDocMaker("args");

            // subject
            String subject = String.format("Error in Mediaflux DataMover post-upload workflow for Facility %s",
                    facilityName);
            dm.add("subject", subject);

            // recipient addresses
            for (String recipient : facilityRecipients) {
                dm.add("to", recipient);
            }

            // body/message
            StringBuilder body = new StringBuilder();
            body.append("Dear colleague,\n\n");
            body.append(
                    "A Mediaflux Data Mover upload has failed to complete the post-upload handler workflow (so the data has been uploaded to Mediaflux).\n");
            body.append("The detailed error message is copied below:\n\n");
            body.append("Facility Name: ").append(facilityName).append("\n");
            body.append("Shareable ID: ").append(shareableId).append("\n");
            body.append("Upload ID: ").append(uploadId).append("\n");
            body.append("Upload Folder Name: ").append(uploadParentName).append("\n");
            body.append("Date: ").append(DateTime.string(now, false)).append("\n");
            body.append("Error: ").append(exception.getMessage()).append("\n\n");
            body.append(
                    "Please contact the RCS Data Solutions team if you need assistance to resolve this issue. DST will also reach out to you as required.\n\n");
            body.append("Regards\n");
            body.append("Mediaflux Operations\n");
            body.append("Data Solutions Team\n");
            body.append("Research Computing Services\n\n\n\n\n\n");
            body.append("=== Error Stack Trace: ===\n");
            ThrowableUtils.printStackTrace(exception, body, EXCEPTION_NOTIFICATION_BODY_MAX_SIZE);
            body.append("\n");
            dm.add("body", body);

            executor.execute("mail.send", dm.root());

        } catch (Throwable ex1) {
            log().add(PluginLog.ERROR,
                    SERVICE_NAME + ": failed to send exception notification for upload shareable: " + shareableId, ex1);
        }
    }

    private static SharedItems createSharedItems(ServiceExecutor executor, XmlDoc.Element uploadShareable,
                                                 String uploadId, String uploadNS, XmlDoc.Element uploadArgs, XmlDoc.Element shareOptions) throws Throwable {
        DownloadShareable downloadShareable = null;
        DownloadLink downloadLink = null;

        String uploadShareableId = uploadShareable.value("@id");
        String downloadType = shareOptions.stringValue("download/@type", "client");
        Long threshold = parseThreshold(shareOptions.element("download/threshold"));
        if (downloadType.equalsIgnoreCase("client")) {
            downloadShareable = DownloadShareable.create(uploadShareable, uploadId, uploadNS, uploadArgs, shareOptions,
                    executor);
        } else if (downloadType.equalsIgnoreCase("direct")) {
            downloadLink = DownloadLink.create(executor, uploadNS, shareOptions, uploadShareable, uploadId);
        } else if (downloadType.equalsIgnoreCase("threshold")) {
            if (threshold == null) {
                throw new IllegalArgumentException("share/download/threshold is missing.");
            }
            long totalContentSize = getTotalContentSize(executor, uploadNS);
            if (totalContentSize >= threshold) {
                downloadShareable = DownloadShareable.create(uploadShareable, uploadId, uploadNS, uploadArgs,
                        shareOptions, executor);
            } else {
                downloadLink = DownloadLink.create(executor, uploadNS, shareOptions, uploadShareable, uploadId);
            }
        } else {
            downloadShareable = DownloadShareable.create(uploadShareable, uploadId, uploadNS, uploadArgs, shareOptions,
                    executor);
            downloadLink = DownloadLink.create(executor, uploadNS, shareOptions, uploadShareable, uploadId);
        }

        // create user interaction
        Interaction userInteraction = Interaction.createUserInteraction(executor, uploadShareableId, uploadId, uploadNS,
                uploadArgs, shareOptions);

        // set user interaction
        UploadShareable.setInteraction(executor, uploadShareableId, uploadId, userInteraction.id);

        return new SharedItems(downloadShareable, downloadLink, userInteraction, uploadNS, uploadShareable, uploadArgs,
                shareOptions);
    }

    private static void sendInvitations(ServiceExecutor executor, SharedItems sharedItems,
                                        Collection<String> shareRecipients) throws Throwable {
        String dataMoverUserDocUrl = Properties.getApplicationProperty(executor,
                Properties.APPLICATION_PROPERTY_DOC_USER_DATAMOVER, Properties.APPLICATION_PROPERTY_APP);
        String dataMoverCLIUserDocUrl = Properties.getApplicationProperty(executor,
                Properties.APPLICATION_PROPERTY_DOC_USER_DATAMOVER_CLI, Properties.APPLICATION_PROPERTY_APP);
        new Invitation(sharedItems.uploadNS, sharedItems.uploadShareable, sharedItems.uploadArgs,
                sharedItems.downloadShareable, dataMoverUserDocUrl, dataMoverCLIUserDocUrl, sharedItems.downloadLink,
                sharedItems.userInteraction, shareRecipients, sharedItems.shareOptions).send(executor);
    }

    private static Long parseThreshold(XmlDoc.Element thresholdElement) throws Throwable {
        if (thresholdElement != null) {
            long threshold = thresholdElement.longValue();
            String unit = thresholdElement.stringValue("@unit", "B");
            if (unit.equalsIgnoreCase("KB")) {
                threshold = threshold * 1000L;
            } else if (unit.equalsIgnoreCase("MB")) {
                threshold = threshold * 1000000L;
            } else if (unit.equalsIgnoreCase("GB")) {
                threshold = threshold * 1000000000L;
            } else if (unit.equalsIgnoreCase("TB")) {
                threshold = threshold * 1000000000000L;
            }
            return threshold;
        }
        return null;
    }

    private static long getTotalContentSize(ServiceExecutor executor, String assetNS) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "namespace>='" + assetNS + "'");
        dm.add("action", "sum");
        dm.add("xpath", "content/size");
        return executor.execute("asset.query", dm.root()).longValue("value");
    }

    private static void setWorm(ServiceExecutor executor, String assetNS, XmlDoc.Element wormOptions) throws Throwable {
        // set WORM for all assets except the manifest asset...
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "namespace>='" + assetNS + "'");
        dm.add("action", "pipe");
        dm.push("service", new String[]{"name", "asset.worm.set"});
        dm.add(wormOptions, false);
        dm.pop();
        executor.execute("asset.query", dm.root());
    }

    private static String moveToMappedDestination(ServiceExecutor executor, String uploadNS, String uploadPathContext,
                                                  UploadMapping mapping, XmlDoc.Element args) throws Throwable {
        String shareableId = args.value("shareable-id");
        String uploadId = args.value("upload-id");
        UploadMapping.Result result = mapping.evaluate(uploadPathContext);
        if (result != null) {
            log().add(PluginLog.WARNING,
                    "[shareable: " + shareableId + ", upload: " + uploadId + ", contextSrcPath: " + uploadPathContext
                            + "]: resolved destination parent path: '" + result.dstParentPath + "'. Moving '" + uploadNS
                            + "' to '" + result.dstParentPath + "' ...");
            try {
                if (!AssetNamespaceUtil.assetNamespaceExists(executor, result.dstParentPath)) {
                    if (result.rule.dstCreate) {
                        AssetNamespaceUtil.createAssetNamespace(executor, result.dstParentPath, true);
                    } else {
                        throw new Exception(
                                "Destination parent asset namespace: '" + result.dstParentPath + "' does not exist.");
                    }
                }
                XmlDocMaker dm = new XmlDocMaker("args");
                dm.add("namespace", uploadNS);
                dm.add("to", result.dstParentPath);
                executor.execute("asset.namespace.move", dm.root());
                String dstPath = AssetPathUtils.join(result.dstParentPath, AssetPathUtils.getName(uploadNS));
                if (mapping.addNote()) {
                    // add note to moved assets to trigger replication.
                    try {
                        log().add(PluginLog.WARNING,
                                "[shareable: " + shareableId + ", upload: " + uploadId + ", srcContextPath: "
                                        + uploadPathContext + "]: adding mf-note to assets in '" + dstPath
                                        + "' to trigger replication");
                        addNote(executor, dstPath, "shareable: " + shareableId + ", upload: " + uploadId);
                    } catch (Throwable e) {
                        log().add(PluginLog.ERROR, "[shareable: " + shareableId + ", upload: " + uploadId
                                + ", dstPath: " + dstPath + "]: failed to apply mf-note meta data to moved assets", e);
                    }
                }
                return dstPath;
            } catch (Throwable e) {
                log().add(PluginLog.ERROR, "[shareable: " + shareableId + ", upload: " + uploadId + ", srcContextPath: "
                                + uploadPathContext + "]: failed to move '" + uploadNS + "' to '" + result.dstParentPath + "'",
                        e);
                notifyAdminOfUploadMoveException(executor, e, result.dstParentPath, args);
                notifyFacilityOfUploadMoveException(executor, e, result.dstParentPath, args);
                return uploadNS;
            }
        } else {
            log().add(PluginLog.WARNING,
                    "[shareable: " + shareableId + ", upload: " + uploadId + ", srcContextPath: " + uploadPathContext
                            + "]: No mapping rule matches source context path. Uploaded data in '" + uploadNS
                            + "' is not moved.");
            return uploadNS;
        }
    }

    private static void addNote(ServiceExecutor executor, String assetNS, String note) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "namespace>='" + assetNS + "'");
        dm.add("action", "pipe");
        dm.push("service", new String[]{"name", "asset.set"});
        dm.push("meta");
        dm.push("mf-note");
        dm.add("note", note);
        dm.pop();
        dm.pop();
        dm.pop();
        executor.execute("asset.query", dm.root());
    }

    private static void notifyAdminOfUploadMoveException(ServiceExecutor executor, Throwable e, String dstParentPath,
                                                         XmlDoc.Element args) {
        String shareableId = null;
        String uploadId = null;
        try {
            shareableId = args.value("shareable-id");
            uploadId = args.value("upload-id");
            XmlDoc.Element shareable = Shareable.describe(executor, shareableId);
            String uploadNS = UploadShareable.getUploadNamespace(executor, shareable, uploadId, false);
            String uploadPathContext = UploadShareable.getUploadPathContext(shareable, uploadId);

            String adminEmail = Properties.getApplicationProperty(executor,
                    Properties.APPLICATION_PROPERTY_OPERATIONS_EMAIL, Properties.APPLICATION_PROPERTY_APP);
            if (adminEmail == null) {
                return;
            }

            String serverUUID = executor.execute("server.uuid").value("uuid");
            XmlDocMaker dm = new XmlDocMaker("args");

            // subject
            String subject = "[" + serverUUID + "; shareable: " + shareableId + "; upload: " + uploadId + "] Warning: "
                    + SERVICE_NAME + ": failed to move uploaded data";
            dm.add("subject", subject);

            // recipient address
            dm.add("to", adminEmail);

            // body/message
            StringBuilder body = new StringBuilder();
            String facilityName = shareable == null ? null : Shareable.getFacilityName(shareable);
            body.append("Facility Name: ").append(facilityName).append("\n");
            body.append("Shareable ID: ").append(shareableId).append("\n");
            body.append("Upload ID: ").append(uploadId).append("\n");
            body.append("Context Source Path: ").append(uploadPathContext).append("\n");
            body.append("Upload Namespace: ").append(uploadNS).append("\n");
            body.append("Destination Parent Namespace: ").append(dstParentPath).append("\n");

            body.append("Service Call:\n");
            body.append(XmlUtils.getServiceCommand(SERVICE_NAME, args)).append("\n\n");

            body.append("Exception:\n");
            ThrowableUtils.printStackTrace(e, body, EXCEPTION_NOTIFICATION_BODY_MAX_SIZE);
            body.append("\n");
            dm.add("body", body);

            executor.execute("mail.send", dm.root());
        } catch (Throwable ex1) {
            log().add(PluginLog.ERROR,
                    SERVICE_NAME + ": failed to send exception notification for upload shareable: " + shareableId, ex1);
        }
    }

    private static void notifyFacilityOfUploadMoveException(ServiceExecutor executor, Throwable e, String dstParentPath,
                                                            XmlDoc.Element args) {
        String shareableId = null;
        Date now = new Date();
        try {
            shareableId = args.value("shareable-id");
            XmlDoc.Element shareable = Shareable.describe(executor, shareableId);
            Collection<String> facilityRecipients = Shareable.getFacilityNotificationRecipients(shareable);
            if (facilityRecipients == null || facilityRecipients.isEmpty()) {
                return;
            }
            String facilityName = Shareable.getFacilityName(shareable);
            String uploadId = args.value("upload-id");
            String uploadParentName = UploadShareable.getUploadParentName(shareable, uploadId);
            String uploadNS = UploadShareable.getUploadNamespace(executor, shareable, uploadId, false);
            String uploadPathContext = UploadShareable.getUploadPathContext(shareable, uploadId);

            XmlDocMaker dm = new XmlDocMaker("args");

            // subject
            String subject = String.format(
                    "[%s] Warning: Mediaflux Data Mover post-upload workflow failed to move uploaded data",
                    facilityName);
            dm.add("subject", subject);

            // recipient addresses
            for (String recipient : facilityRecipients) {
                dm.add("to", recipient);
            }

            // body/message
            StringBuilder body = new StringBuilder();
            body.append("Dear colleague,\n\n");
            body.append("The post-upload workflow failed to move the uploaded data to the destination namespace: '"
                    + dstParentPath + "'\n");
            body.append("The detailed error message is copied below:\n\n");
            body.append("Facility Name: ").append(facilityName).append("\n");
            body.append("Shareable ID: ").append(shareableId).append("\n");
            body.append("Upload ID: ").append(uploadId).append("\n");
            body.append("Upload Folder Name: ").append(uploadParentName).append("\n");
            body.append("Context Source Path: ").append(uploadPathContext).append("\n");
            body.append("Upload Namespace: ").append(uploadNS).append("\n");
            body.append("Destination Parent Namespace: ").append(dstParentPath).append("\n");
            body.append("Date: ").append(DateTime.string(now, false)).append("\n");
            body.append("Error: ").append(e.getMessage()).append("\n\n");
            body.append(
                    "Please contact the RCS Data Solutions team if you need assistance to resolve this issue. DST will also reach out to you as required.\n\n");
            body.append("Regards\n");
            body.append("Mediaflux Operations\n");
            body.append("Data Solutions Team\n");
            body.append("Research Computing Services\n\n\n\n\n\n");
            body.append("=== Error Stack Trace: ===\n");
            ThrowableUtils.printStackTrace(e, body, EXCEPTION_NOTIFICATION_BODY_MAX_SIZE);
            body.append("\n");
            dm.add("body", body);

            executor.execute("mail.send", dm.root());

        } catch (Throwable ex1) {
            log().add(PluginLog.ERROR,
                    SERVICE_NAME + ": failed to send exception notification for upload shareable: " + shareableId, ex1);
        }
    }

    private static void createUserInteractionForAddingNewEmailsAndNotify(
            ServiceExecutor executor, String shareableId, String uploadId, Collection<String> emails, String dict,
            Collection<String> facContactEmails) throws Throwable {

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("name", String.format("facility-upload-%s.%s-add-email-dict-entries", shareableId, uploadId));
        dm.add("title", String.format("Add new emails to dictionary: %s", dict));
        dm.add("description", "Add following new emails to dictionary: " + dict);

        dm.push("field-definitions");
        dm.push("element", new String[]{"name", "emails", "label", "Emails", "type",
                "string", "min-occurs", "1", "max-occurs", "1"});
        dm.add("description", "The data recipients' emails to add to dictionary: " + dict);
        dm.add("value", Emails.join(emails));
        dm.pop();
        dm.pop();

        dm.add("option", new String[]{"name", "cancel", "label", "Cancel", "description", "Cancels the operation"});

        dm.push("option", new String[]{"name", "add", "label", "Add", "description", "Add emails to dictionary: " + dict});
        dm.push("service", new String[]{"name", "service.execute"});
        dm.push("args");
        for (String email : emails) {
            dm.push("service", new String[]{"name", "dictionary.entry.add"});
            dm.add("dictionary", dict);
            dm.add("term", email.toLowerCase());
            dm.pop();
        }
        dm.pop();
        dm.pop();
        dm.pop();

        Date expireAt = DateTime.parse("today+" + INTERACTION_ADD_EMAIL_DICT_ENTRY_DURATION_DAYS + "day");
        dm.add("expires-at", expireAt);

        PluginTask.checkIfThreadTaskAborted();
        String interactionId = executor.execute("automation.user.interaction.create", dm.root())
                .value("user-interaction/@id");
        logWarning("Created user interaction: " + interactionId +
                " for the facility operators to confirm adding new emails to the dictionary: " + dict);

        String interactionUrl = Interaction.url(executor, interactionId);
        dm = new XmlDocMaker("args");
        for (String facContactEmail : facContactEmails) {
            dm.add("to", facContactEmail);
        }
        dm.add("subject", "[Mediaflux] New data recipient emails can be added to your email dictionary");
        StringBuilder sb = new StringBuilder();
        sb.append("<html>\n");
        sb.append("<style>\n");
        sb.append("table, th, td { border: 1px solid black; }\n");
        sb.append("</style>\n");
        sb.append("Dear Colleague,<br/><br/>");
        sb.append("A recent instrument upload has been shared with ").append(emails.size()).append(" new data recipient").append(emails.size()>1? "s. ":". ");
        sb.append("The email address").append(emails.size()>1?"es are": " is").append(" as follows: <br/>");
        sb.append("<ul>");
        for (String email : emails) {
            sb.append("<li>").append(email.toLowerCase()).append("</li>");
        }
        sb.append("</ul><br/><br/>");
        sb.append("Click <a href=\"").append(interactionUrl).append("\">").append("this link (by ").append(DateTime.string(expireAt)).append(")</a> to add the above email")
                .append(emails.size()>1?"s":"").append(" to your email dictionary: <b>").append(dict).append("</b>.<br/><br/>");
        sb.append("You can ignore this email if you do not want to add the email").append(emails.size()>1?"s":"").append(" to your dictionary.");
        sb.append("</html>");
        dm.add("body", new String[]{"type", "text/html"}, sb.toString());

        PluginTask.checkIfThreadTaskAborted();
        executor.execute("mail.send", dm.root());
        logWarning("Sent email to facility operators to confirm adding new emails to dictionary: " + dict);
    }

    private static void addDictionaryEntries(ServiceExecutor executor, String dict, Collection<String> newEntries) throws Throwable {
        if (newEntries != null) {
            for (String e : newEntries) {
                PluginTask.checkIfThreadTaskAborted();
                executor.execute("dictionary.entry.add",
                        "<args><dictionary>" + dict + "</dictionary><term>" + e + "</term></args>", null, null);
            }
        }
    }

    private static void notifyOfNewEmailsAdded(ServiceExecutor executor, Collection<String> emails, String dict,
                                               Collection<String> facContactEmails) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        for (String facContactEmail : facContactEmails) {
            dm.add("to", facContactEmail);
        }
        dm.add("subject", String.format("[Mediaflux] %d email address%s ha%s been added to your dictionary: %s",
                emails.size(), emails.size() > 1 ? "es" : "", emails.size() > 1 ? "ve" : "s", dict));
        StringBuilder sb = new StringBuilder();
        sb.append("<html>\n");
        sb.append("<style>\n");
        sb.append("table, th, td { border: 1px solid black; }\n");
        sb.append("</style>\n");
        sb.append("Dear Colleague,<br/><br/>");
        sb.append("A recent instrument upload has been shared with some new data recipients. Their email addresses below have been automatically added to the email address dictionary of your instrument shareable:<br/>");
        sb.append("<table><thead><tr><th>Email</th><th>Dictionary</th></tr></thead><tbody>");
        for (String email : emails) {
            sb.append("<tr><td>").append(email.toLowerCase()).append("</td><td>").append(dict).append("</td></tr>");
        }
        sb.append("</tbody></table>\n");
        sb.append("</html>");
        dm.add("body", new String[]{"type", "text/html"}, sb.toString());
        executor.execute("mail.send", dm.root());
    }

}
