/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcUserProjectRolesRevoke extends PluginService {


	private Interface _defn;

	public SvcUserProjectRolesRevoke()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain for the user.", 1, 1));
		_defn.add(new Interface.Element("user", StringType.DEFAULT, "The user name. If not given, all users in the domain are considered.", 0, 1));
		_defn.add(new Interface.Element("role", StringType.DEFAULT, "Some other role that is revoked for each user.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("disabled", BooleanType.DEFAULT, "Only apply revokation to user accounts that are disabled (default true).", 0, 1));
		_defn.add(new Interface.Element("migrated", BooleanType.DEFAULT, "Only apply revokation this to users that were migrated to LDAP domain (default true). There will be some special meta-data on their account if so and the accounts will be disabled.", 0, 1));
		_defn.add(new Interface.Element("check-only", BooleanType.DEFAULT, "Just check what would happen, don't actually do it (default true).", 0, 1));
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Revokes all project roles that the user holds.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "user.project.roles.remove";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		String domain = args.value("domain");
		String user0 = args.value("user");
		Collection<String> roles0 = args.values("role");
		Boolean mustBeDisabled = args.booleanValue("disabled", true);
		Boolean mustBeMigrated = args.booleanValue("migrated", true);
		Boolean checkOnly = args.booleanValue("check-only", true);

		// Get user meta-data
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("size", "infinity");
		dm.add("permissions", "true");
		if (user0!=null) {
			dm.add("user", user0);
		}
		XmlDoc.Element r = executor().execute("user.describe", dm.root());
		Collection<XmlDoc.Element> users = r.elements("user");

		// Iterate over users
		for (XmlDoc.Element user : users) {
			String userName = user.value("@user");
			Boolean enabled = user.booleanValue("@enabled", true);
			//
			Collection<String> notes = user.values("asset/meta/mf-note/note");
			Boolean isMigrated = false;
			if (notes!=null && notes.size()>0) {
				for (String note : notes) {
					if (note.startsWith("Migrated to LDAP")) {
						isMigrated = true;
					}
				}
			}

			// Filter users
			Boolean keep = true;
			if (mustBeDisabled) {
				if (enabled) {
					keep = false;
				}
			}
			if (keep) {
				if (mustBeMigrated) {
					if (!isMigrated) {
						keep = false;
					}
				}
			}

			// Iterate through the roles and find project roles
			if (keep) {
				w.push("user");
				w.add("name", domain+":"+userName);
				//
				Collection<XmlDoc.Element> roles = user.elements("role");
				Vector<String> rolesToRevoke = new Vector<String>();
				for (XmlDoc.Element role : roles) {
					String type = role.value("@type");
					String value = role.value();
					if (type.equals("role")) {
						String[] parts = value.split(":");
						if (value.startsWith("proj-") && parts.length==2) {
							rolesToRevoke.add(value);
						}
					}
				}

				// Add other given roles
				if (roles0!=null) {
					rolesToRevoke.addAll(roles0);
				}

				// Revoke the project and other roles
				if (rolesToRevoke.size()>0) {
					//
					dm = new XmlDocMaker("args");
					dm.add("name", domain+":"+userName);
					dm.add("type", "user");
					for (String roleToRevoke : rolesToRevoke) {
						dm.add("role", new String[] {"type", "role"}, roleToRevoke);
						w.add("role", roleToRevoke);
					}
					if (!checkOnly) {
						executor().execute("actor.revoke", dm.root());
					}
				}
				w.pop();
			} 
		}
	}
}
