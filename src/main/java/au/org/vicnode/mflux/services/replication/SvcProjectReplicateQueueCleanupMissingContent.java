/** 
 * @author Robert Hutton
 *
 * Copyright (c) 2023, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.replication;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.BooleanType;
//import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
//import au.org.vicnode.mflux.plugin.util.AssetProcessingQueue;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectReplicateQueueCleanupMissingContent extends PluginService {

	private Interface _defn;

	public SvcProjectReplicateQueueCleanupMissingContent()  throws Throwable {
		_defn = new Interface();
        _defn.add(new Interface.Element("name", StringType.DEFAULT, "Name of the queue", 1, 1));
        _defn.add(new Interface.Element("size", IntegerType.POSITIVE, "Maximum number of items to retrieve from the queue.  Default 100.", 0, 1));
        _defn.add(new Interface.Element("apply", BooleanType.DEFAULT , "Apply changes (destroy asset content and reexecute processing queue entry).  Default false.", 0, 1));
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int maxNumberOfOutputs() {
		return 1;
	}
    
	public int minNumberOfOutputs() {
		return 0;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Clean up assets that are stuck in replication queue due to missing one or more content versions.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.replicate.queue.cleanup.content";
	}

	public boolean canBeAborted() {
		return true;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		if (args.value("size") == null) {
			args.add(new Element("size", "100"));
		}
		Boolean apply = args.booleanValue("apply", false);
		ProjectReplicate.handleEntriesWithMissingContent(executor(), outputs, args.value("name"), args.value("size"), apply, w);
	}
}
