package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;
import java.util.List;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.utils.Task.ExAborted;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import au.org.vicnode.mflux.services.SvcUserProjectHasWritable;
import unimelb.utils.FileSizeUtils;
import unimelb.utils.HtmlUtils;

public class Invitation {

    public static final String DEFAULT_SUBJECT = "Facility Data Delivery";

    public static final String DISCLAIMER_MSG = "When copying to your own project, the platform/owner who provided the data will be notified of that copy, including first name, last name and username.";

    public final String assetNS;
    final XmlDoc.Element uploadShareable;
    final XmlDoc.Element uploadArgs;
    public final String facilityName;
    public final String facilityNotificationFrom;
    public final DownloadShareable downloadShareable;
    public final String dataMoverUserDocUrl;
    public final String dataMoverCLIUserDocUrl;
    public final DownloadLink downloadLink;
    public final Interaction userInteraction;
    public final Collection<String> shareRecipients;
    public final String shareNote;

    Invitation(String assetNS, XmlDoc.Element uploadShareable, XmlDoc.Element uploadArgs,
               DownloadShareable downloadShareable, String dataMoverUserDocUrl, String dataMoverCLIUserDocUrl,
               DownloadLink downloadLink, Interaction userInteraction, Collection<String> shareRecipients,
               XmlDoc.Element shareOptions) throws Throwable {
        this.assetNS = assetNS;
        this.uploadShareable = uploadShareable;
        this.uploadArgs = uploadArgs;

        this.facilityName = Shareable.getFacilityName(uploadShareable);
        this.facilityNotificationFrom = Shareable.getFacilityNotificationFrom(uploadShareable);
        this.downloadShareable = downloadShareable;
        this.dataMoverUserDocUrl = dataMoverUserDocUrl;
        this.dataMoverCLIUserDocUrl = dataMoverCLIUserDocUrl;
        this.downloadLink = downloadLink;
        this.userInteraction = userInteraction;
        this.shareRecipients = shareRecipients;
        this.shareNote = shareOptions.value("note");
    }

    public String subject() throws Throwable {
        StringBuilder sb = new StringBuilder();
        if (this.facilityName != null) {
            sb.append(String.format("%s Data Delivery", this.facilityName));
        } else {
            sb.append(DEFAULT_SUBJECT);
        }
        String emailSubjectSuffix = this.uploadArgs == null ? null
                : this.uploadArgs.value(Shareable.ARG_EMAIL_SUBJECT_SUFFIX);
        if (emailSubjectSuffix != null) {
            sb.append(" ").append(emailSubjectSuffix);
        }
        return sb.toString();
    }

    public String htmlMessage(ServiceExecutor executor, boolean includeUserInteraction) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", String.format("namespace>='%s' and not(namespace='%s' and name='%s')", this.assetNS,
                this.assetNS, Manifest.MANIFEST_FILE_NAME));
        dm.add("xpath", "content/size");
        dm.add("action", "sum");

        XmlDoc.Element re = executor.execute("asset.query", dm.root());
        long fileCount = re.longValue("value/@nbe", 0);
        long totalSize = re.longValue("value", 0);

        StringBuilder sb = new StringBuilder();
        sb.append("<p>Hello,</p>\n");
        sb.append("<p>");
        sb.append("New data");
        if (this.facilityName != null) {
            sb.append(" from <b>").append(this.facilityName).append("</b>");
        }
        sb.append(" is available for you to download");
        if (includeUserInteraction) {
            sb.append(" or copy to your own Mediaflux project");
        }
        sb.append(".<p>\n");

        String tableOpen = "<table style=\"border:1px solid black; border-collapse:collapse; width:85%;\">";
        String tdLeftOpen = "<td align=\"right\" style=\"width:90px; border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em;\">";
        String tdRightOpen = "<td style=\"border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em\">";

        /*
         * Summary of the data in html table
         */
        sb.append(tableOpen).append("\n");
        sb.append("<tr>").append(tdLeftOpen).append("<b>Source Directory/File:</b></td>").append(tdRightOpen)
                .append(assetNamespaceContentsAsHtml(executor, this.assetNS)).append("</td></tr>\n");
        String nameSuffix = this.uploadArgs.value(Shareable.ARG_NAME_SUFFIX);
        if (nameSuffix != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Name Suffix:</b></td>").append(tdRightOpen)
                    .append(nameSuffix).append("</td></tr>\n");
        }
        String keywords = this.uploadArgs.value(Shareable.ARG_KEYWORDS);
        if (keywords != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Keywords:</b></td>").append(tdRightOpen).append(keywords)
                    .append("</td></tr>\n");
        }
        String dataNote = this.uploadArgs.value(Shareable.ARG_DATA_NOTE);
        if (dataNote != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Data Note:</b></td>").append(tdRightOpen).append(dataNote)
                    .append("</td></tr>\n");
        }
        String emailSubjectSuffix = this.uploadArgs.value(Shareable.ARG_EMAIL_SUBJECT_SUFFIX);
        if (emailSubjectSuffix != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Email Subject Suffix:</b></td>").append(tdRightOpen)
                    .append(emailSubjectSuffix).append("</td></tr>\n");
        }
        sb.append("<tr>").append(tdLeftOpen).append("<b>File Count:</b></td>").append(tdRightOpen).append(fileCount)
                .append("</td></tr>\n");
        sb.append("<tr>").append(tdLeftOpen).append("<b>Total Size:</b></td>").append(tdRightOpen)
                .append(FileSizeUtils.getHumanReadableSize(totalSize)).append("</td></tr>\n");
        sb.append("</table><br><br><br>");

        /*
         * download & UI links
         */
        sb.append("<ul>\n");

        // direct download link
        if (this.downloadLink != null) {
            sb.append("<li>To download directly via browser, click the URL below:<br>\n");
            sb.append(tableOpen).append("\n");
            sb.append("<tr>").append(tdLeftOpen).append("<b>URL</b></td>").append(tdRightOpen).append("<a href=\"")
                    .append(this.downloadLink.url).append("\">").append(this.downloadLink.url)
                    .append("</a></td></tr>\n");
            if (this.downloadLink.password != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>Password</b></td>").append(tdRightOpen)
                        .append(this.downloadLink.password).append("</td></tr>\n");
            }
            if (this.downloadLink.expiry != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>Expiry</b></td>").append(tdRightOpen)
                        .append(this.downloadLink.expiry).append("</td></tr>\n");
            }
            sb.append("</table><br><br><br></li>\n");
        }

        // shareable download via Data Mover GUI
        if (this.downloadShareable != null) {
            sb.append("<li>To download the data using Mediaflux Data Mover GUI, click the URL below.<br>\n");
            sb.append(tableOpen).append("\n");
            sb.append("<tr>").append(tdLeftOpen).append("<b>URL</b></td>").append(tdRightOpen).append("<a href=\"")
                    .append(this.downloadShareable.url).append("\">").append(this.downloadShareable.url)
                    .append("</a></td></tr>\n");
            sb.append("<tr>").append(tdLeftOpen).append("<b>Token</b></td>").append(tdRightOpen)
                    .append(this.downloadShareable.token).append("</td></tr>\n");
            if (this.downloadShareable.downloadPassword != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>Password</b></td>").append(tdRightOpen)
                        .append(this.downloadShareable.downloadPassword).append("</td></tr>\n");
            }
            if (this.downloadShareable.validTo != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>Expiry</b></td>").append(tdRightOpen)
                        .append(this.downloadShareable.validTo).append("</td></tr>\n");
            }
            if (this.dataMoverUserDocUrl != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>Data Mover Documentation</b></td>").append(tdRightOpen)
                        .append("<a href=\"").append(this.dataMoverUserDocUrl).append("\">")
                        .append(this.dataMoverUserDocUrl).append("</a></td></tr>\n");
            }
            sb.append("</table><br><br><br></li>\n");
        }

        // Download with the CLI
        if (this.downloadShareable != null && this.dataMoverUserDocUrl != null && this.dataMoverCLIUserDocUrl != null) {
            String unixCmd = "mediaflux-data-mover-cli -download " + "-csv ./download.csv -exists rename "
                    + this.downloadShareable.token + " .";
            String winCmd = "mediaflux-data-mover-cli.cmd -download " + "-csv ./download.csv -exists rename "
                    + this.downloadShareable.token + " .";
            sb.append(
                    "<li>To download the data using Mediaflux Data Mover CLI to your current working directory use the command below (note period at the command end which specifies the download location).<br>\n");
            sb.append(tableOpen).append("\n");
            sb.append("<tr>").append(tdLeftOpen).append("<b>Unix Command</b></td>").append(tdRightOpen).append(unixCmd)
                    .append("</td></tr>\n");
            sb.append("<tr>").append(tdLeftOpen).append("<b>Windows Command</b></td>").append(tdRightOpen)
                    .append(winCmd).append("</td></tr>\n");
            sb.append("<tr>").append(tdLeftOpen).append("<b>Note</b></td>").append(tdRightOpen).append(
                            "Typically used when no graphical environment is available such as in a High Performance Computing environment (e.g. UoM Spartan).  If this command does not work, the links below will help you install the Data Mover if required and then use the CLI.")
                    .append("</td></tr>\n");
            sb.append("<tr>").append(tdLeftOpen).append("<b>Note</b></td>").append(tdRightOpen)
                    .append("Get help on arguments with the command mediaflux-data-mover-cli -help")
                    .append("</td></tr>\n");
            if (this.dataMoverUserDocUrl != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>Data Mover  Documentation</b></td>").append(tdRightOpen)
                        .append("<a href=\"").append(this.dataMoverUserDocUrl).append("\">")
                        .append(this.dataMoverUserDocUrl).append("</a></td></tr>\n");
            }
            if (this.dataMoverCLIUserDocUrl != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>Data Mover CLI Documentation</b></td>")
                        .append(tdRightOpen).append("<a href=\"").append(this.dataMoverCLIUserDocUrl).append("\">")
                        .append(this.dataMoverCLIUserDocUrl).append("</a></td></tr>\n");
            }
            sb.append("</table></li><br><br><br>\n");
        }

        // to copy the data to user's project interactively
        if (includeUserInteraction) {
            sb.append("<li>To copy the data to your own Mediaflux project, click the URL below.<br>\n");
            sb.append(tableOpen).append("\n");
            sb.append("<tr>").append(tdLeftOpen).append("<b>URL</b></td>").append(tdRightOpen).append("<a href=\"")
                    .append(this.userInteraction.url).append("\">").append(this.userInteraction.url)
                    .append("</a></td></tr>\n");
            if (this.userInteraction.expiresAt != null) {
                sb.append("<tr>").append(tdLeftOpen).append("<b>Expiry</b></td>").append(tdRightOpen)
                        .append(this.userInteraction.expiresAt).append("</td></tr>\n");
            }
            sb.append("<tr>").append(tdLeftOpen).append("<b>Disclaimer</b></td>").append(tdRightOpen).append("<b><i>")
                    .append(DISCLAIMER_MSG).append("</i></b></td></tr>\n");
            sb.append("</table></li><br><br><br>\n");
        }

        // share/note
        if (shareNote != null) {
            String safeShareNote = HtmlUtils.removeUnsafeTags(shareNote);
            sb.append("<li style=\"font-weight: bold\">Note:<p>").append(safeShareNote).append("</p></li>\n");
        }

        sb.append("</ul>");
        return sb.toString();
    }

    private void send(ServiceExecutor executor, String recipientEmail) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        if (this.facilityNotificationFrom != null) {
            dm.add("from", this.facilityNotificationFrom);
        }
        dm.add("to", recipientEmail);
        dm.add("subject", subject());
        boolean includeInteraction = hasProject(executor, recipientEmail);
        dm.add("body", new String[]{"type", "text/html"}, htmlMessage(executor, includeInteraction));
        executor.execute("mail.send", dm.root());
    }

    public void send(ServiceExecutor executor) throws Throwable {
        StringBuilder failedEmails = new StringBuilder();
        Throwable error = null;
        for (String recipientEmail : this.shareRecipients) {
            PluginTask.checkIfThreadTaskAborted();
            try {
                send(executor, recipientEmail);
            } catch (Throwable t) {
                // when fails to send to this recipient, save the error and process the rest
                // recipients. So that it will not affect other/valid email recipients.
                if (failedEmails.length() > 0) {
                    failedEmails.append(", ");
                }
                failedEmails.append(recipientEmail);
                if (error == null) {
                    error = t;
                }
            }
        }
        if (error != null) {
            // throw a exception if there was any exception.
            throw new Exception("Failed to send notification email to: " + failedEmails.toString(), error);
        }
    }

    // TODO remove when the UnknownHostException issue for ldap search is fixed.
    // Begin of WA Code
    private static final int USER_SEARCH_NB_RETRIES = 3;
    private static final int USER_SEARCH_RETRY_WAIT_SECONDS = 5;

    private static final List<XmlDoc.Element> searchUserByEmail(ServiceExecutor executor, String userEmail,
                                                                int nbRetries) throws Throwable {
        try {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("email", userEmail);
            PluginTask.checkIfThreadTaskAborted();
            return executor.execute("unimelb.user.search", dm.root()).elements("user");
        } catch (Throwable e) {
            if (nbRetries > 0 && !(e instanceof ExAborted)) {
                SvcFacilityShareableUploadComplete.log().add(PluginLog.WARNING, "unimelb.user.search (email="
                        + userEmail + ") failed. Retry in " + USER_SEARCH_RETRY_WAIT_SECONDS + " seconds...");
                Thread.sleep(USER_SEARCH_RETRY_WAIT_SECONDS * 1000);
                return searchUserByEmail(executor, userEmail, nbRetries - 1);
            } else {
                throw e;
            }
        }
    }
    // End of WA Code

    private static boolean hasProject(ServiceExecutor executor, String userEmail) throws Throwable {

        List<XmlDoc.Element> ues = searchUserByEmail(executor, userEmail, USER_SEARCH_NB_RETRIES);
        if (ues == null || ues.isEmpty()) {
            return false;
        }
        for (XmlDoc.Element ue : ues) {

            String email = ue.value("@email");
            if (email == null || !email.equalsIgnoreCase(userEmail)) {
                continue;
            }

            String authority = ue.value("@authority");
            String domain = ue.value("@domain");
            String user = ue.value("@user");

            XmlDocMaker dm = new XmlDocMaker("args");
            if (authority != null) {
                dm.add("authority", authority);
            }
            dm.add("domain", domain);
            dm.add("user", user);
            PluginTask.checkIfThreadTaskAborted();
            XmlDoc.Element re = executor.execute(SvcUserProjectHasWritable.SERVICE_NAME, dm.root());
            boolean hasProjectWritable = re.booleanValue("writable-project", false);
            if (hasProjectWritable) {
                return true;
            }
        }
        return false;
    }

    private static String assetNamespaceContentsAsHtml(ServiceExecutor executor, String assetNS) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", assetNS);
        dm.add("assets", true);
        dm.add("action", "get-name");
        dm.add("size", 100);
        XmlDoc.Element re = executor.execute("asset.namespace.list", dm.root());

        Collection<String> namespaces = re.values("namespace/namespace");
        Collection<String> assets = re.values("namespace/asset");
        boolean complete = re.booleanValue("namespace/complete", true);
        StringBuilder sb = new StringBuilder();
        if (namespaces != null) {
            for (String nsName : namespaces) {
                sb.append(nsName).append("/<br>");
            }
        }
        if (assets != null) {
            for (String assetName : assets) {
                if (!Manifest.MANIFEST_FILE_NAME.equals(assetName) && !Metadata.METADATA_FILE_NAME.equals(assetName)) {
                    sb.append(assetName).append("<br>");
                }
            }
        }
        if (!complete) {
            sb.append("... ... ...");
        }
        String result = sb.toString();
        if (result.endsWith("<br>")) {
            result = result.substring(0, result.length() - 4);
        }
        return result;
    }

}
