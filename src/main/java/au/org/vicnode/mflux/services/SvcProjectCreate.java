/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.atomic.AtomicTransaction;
import arc.mf.plugin.atomic.AtomicOperation;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetUtil;
import au.org.vicnode.mflux.plugin.util.DictionaryUtil;
import au.org.vicnode.mflux.plugin.util.DocTypeUtil;
import au.org.vicnode.mflux.plugin.util.MetaUtil;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;
import au.org.vicnode.mflux.plugin.util.Util;

public class SvcProjectCreate extends PluginService {


	// Set the posix mount point to read/write
	public static Boolean READ_ONLY = false;

	// Don't set the mode bits when making the posix mount point.  
	// If set to 'false' requests to set permissions, owner, or group will be ignored.
	// If set to 'true' requests to set permissions, owner, or group will be honoured. 
	public static Boolean APPLY_MODE_BITS = false;

	// Set the default allowed posix protocol which is all (smb, ssh, nfs)
	public static String ALLOWED_POSIX_PROTOCOL = "all";

	private Interface _defn;

	public SvcProjectCreate()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The asset ID of the 'user resource asset' tracking details about the user and project.", 1, 1));
		_defn.add(new Interface.Element("fail-if-not-enabled", 
				BooleanType.DEFAULT, "When adding posix authentication domain identities to the identity map, if the authentication domain is not enabled, either fail or simply don't add the identity (default). In the latter case, the domain will just be listed indicating it is not enabled. ", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Utilizes the asset provisioning system to create a standard project from the input of a user resource asset.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.create";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Validate some resources
		validateResources (executor());

		// Inputs
		final String assetID = args.value("id");
		final Boolean failIfAuthDomainNotEnabled = args.booleanValue("fail-if-not-enabled", false);

		// Atomic creation so if fails, all partial resources are destroyed
		final XmlDoc.Element info = new XmlDoc.Element("parent");
		try {
			new AtomicTransaction(new AtomicOperation() {

				@Override
				public boolean execute(ServiceExecutor executor) throws Throwable {
					provision (assetID, failIfAuthDomainNotEnabled, info, w);
					return false;
				}
			}).execute(executor()); 
		} catch (Throwable t) {
			// We have to clean up some things ourselves

			// Posix mount point
			String posixName = info.value("posix");

			// An Exception may arise before the posix mount is provisioned
			if (posixName!=null) {
				Posix.unMountPosix(executor(), posixName);
			}
			// Rethrow
			throw new Exception(t);
		}
	}


	private void validateResources (ServiceExecutor executor) throws Throwable {
		// Store dictionaries. Exception if don't exist
		Collection<XmlDoc.Element> psp = Project.storePolicies(executor);
		if (psp==null) {
			throw new Exception ("There are no primary store policies registered in the primary stores dictionary - contact the administrator");
		}
		// We don't care the result here, but a misconfigured store policy is detected here
		ProjectReplicate.isDRConfigured(executor);
	}


	private void provision (String assetID, Boolean failIfAuthDomainNotEnabled, XmlDoc.Element info, XmlWriter w) throws Throwable {

		// Fetch asset meta-data documents
		ServiceExecutor executor = executor();
		XmlDoc.Element asset = AssetUtil.getAsset(executor, null, assetID);
		if (asset==null) {
			throw new Exception ("User resource asset is empty");
		}

		// Fetch all meta-data
		XmlDoc.Element meta = asset.element("asset/meta");

		// Properties
		String onBoardingDocType = Properties.getOnboardingDocType(executor);

		// Get mandatory onboarding meta-data
		XmlDoc.Element userOnboardMeta = meta.element(onBoardingDocType);
		if (userOnboardMeta==null) {
			throw new Exception("No user resource document of type " + onBoardingDocType + " found on user resource asset.");		
		}


		// Some things are optional in the Doc Type because the user resource asset is also used by the
		// vicnode.project.user.add service that needs much less information
		if (userOnboardMeta.value("description") == null) {
			throw new Exception("The description element is empty. You must populate to provision a project.");
		}

		// Name must be supplied.
		// Get the name string that the user specifies. WIll be combined into 'proj-<name-<cid>
		// We convert to lower case to help ourselves with some SMB clients (OS X) converting
		// to upper case (MF tries uppper and lower).  Mixed case is hard to handle.
		String projectName = userOnboardMeta.value("project-name").toLowerCase();
		if (projectName==null) {
			throw new Exception ("You must specify a project name string.");
		}

		// Make sure no spaces in it !
		if (MetaUtil.hasWhiteSpace(projectName)) {
			throw new Exception ("The project name '" + projectName + "' has white space in it. This is not allowed.");
		}

		if (projectName.contains("-")) {
			throw new Exception ("The project name '" + projectName + "' has a hyphen in it. This is not allowed.");
		}

		// Allocate the Project identifier. It is of the form 'proj-<name>-<cid>'
		String projectID = userOnboardMeta.value("project-id");
		if (projectID!=null) {
			throw new Exception("The project-id element is not empty. This indicates that this project has already been provisioned.");
		}

		// Allocate project name
		String type = meta.value(onBoardingDocType + "/type");
		if (type==null) {
			throw new Exception("The element " + onBoardingDocType  + "/type is mandatory");
		}
		projectID  = Project.allocateProjectName(executor, projectName, type);

		// Fetch store policy
		String primaryStorePolicy = userOnboardMeta.value("storage/primary/store-policy");
		if (!Util.storePolicyExists(executor, null, primaryStorePolicy)) {
			throw new Exception ("The primary server store policy '" + primaryStorePolicy + "' does not exist");
		}

		// Provision resources. 
		// Quotas are provisioned with namespaces.
		w.push("Primary");
		w.add("project-id", projectID);
		provisionProject (executor, projectID, userOnboardMeta, 
				primaryStorePolicy, w);

		// Fetch the asset namespace from the namespace map dictionary
		String assetNameSpace = Project.assetNameSpace(executor, projectID, false);
		w.add("namespace", assetNameSpace);
		w.pop();

		// Copy standard $NS_ADMIN:User-Onboarding meta-data to asset namespace in doc type $NS_ADMIN:Project
		// No inheritance. This is mandatory.
		setStandardOnBoardMeta (executor, assetID, projectID, assetNameSpace, userOnboardMeta);

		// Copy meta-data from $NS:Collection, this is optional
		XmlDoc.Element collectionMeta = meta.element(Properties.getCollectionDocType(executor));
		if (collectionMeta!=null) {
			// Copy some elements from $NS_ADMIN:User-Onboarding to $NS:Collection
			// as these are auto-populated
			copyToCollection (executor, projectID, userOnboardMeta.value("description"), collectionMeta);

			// Copy standard $NS:Collection meta-data to asset namespace; no inheritance
			NameSpaceUtil.addAssetNameSpaceMetaData(null, executor, assetNameSpace, collectionMeta);
		}

		// Add the UoM service collection that will be visible
		// via the namespace context menus in the Arcitecta Desktop
		addServiceCollections (executor, assetNameSpace, w);

		// Now provision the posix mount point for this project 
		provisionPosix (executor, assetNameSpace, projectID, failIfAuthDomainNotEnabled, w);

		// Add the posix mount point name to this object. We need this if an exception
		// arises so we can unmount it as the AtomicTransaction fails to do so.
		info.add(new Element("posix", projectID));

		// Provision DR system resources. We only do this for production projects
		// We no longer explicitly create the DR namespace. Instead, we
		// just reply on replication to make the required namespace
		// and rely on the parent namespace /1128/projects having the 
		// correct inherited store policy.
		XmlDoc.Element dr = userOnboardMeta.element("storage/disaster-recovery");
		if (type.equals(Properties.PROJECT_TYPE_PROD) &&
				dr!=null) {

			// Check the DR environment is configured		
			if (ProjectReplicate.isDRConfigured(executor)) {

				Boolean useQueue = userOnboardMeta.booleanValue("storage/disaster-recovery/replication-queue", false);
				provisionDR (executor, projectID, primaryStorePolicy, useQueue, w);
			} else {
				w.push("Replication", new String[]{"status", "environment-not-set-up"});
				w.pop();
			}
		} else {
			w.push("Replication", new String[]{"status", "test-project"});
			w.add("namespace", new String[]{"store", "none"}, "none");
			w.pop();
		}
		
		// Set content-copy asset processing queue 
		// This is to evaluate the robustness of content copies
		// with an aggregated store
		// TBD nebk: turn this on
		/*
		if (ProjectContentCopy.isConfigured(executor)) {
			ProjectContentCopy.setStandardQueueOnProject(executor, projectID, w);
		} else {
			w.push("ContentCopy", new String[]{"status", "environment-not-set-up"});
			w.pop();
		}
		*/

		// Update resourcing asset with the project-ID
		updateResourceAsset (executor, assetID, projectID);

	}

	private void addServiceCollections (ServiceExecutor executor, String namespace, XmlWriter w) throws Throwable {
		String colln = Properties.getNamespaceServiceCollection(executor);
		if (colln==null) {
			return;
		}
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		dm.add("collection", new String[] {"inheritance", "inherited"}, colln);
		executor.execute("asset.namespace.service.collection.add",dm.root());
	}

	private void provisionPosix (ServiceExecutor executor, String nameSpace, String projectID, 
			                     Boolean failIfAuthDomainNotEnabled, XmlWriter w) throws Throwable {

		// The default restriction is localhost (the MF server) for NFS
		XmlDocMaker dm = new XmlDocMaker("args");
		Posix.makeDefaultRestrictions(dm);
		XmlDoc.Element restrict = dm.root().element("restrict");
		Vector<XmlDoc.Element> restrictions = new Vector<XmlDoc.Element>();
		restrictions.add(restrict);
		XmlDoc.Element rootRestriction = dm.root().element("root");
		//
		w.push("Posix");
		w.add("name", projectID);

		// Create an empty identity map for this project. You can add UIDs to it later with other
		// services (e.g. for NFS mounts)
		w.push("identity-map");
		w.add("name", projectID);;
		XmlDoc.Element map = Posix.describePosixIdentityMap(executor, projectID);
		if (map==null || (map!=null && map.element("map")==null)) {
			Posix.createIdentityMap(executor, projectID);
			w.add("created", "true");

			// For SMB we need to add the domain (AD and local domains) identities to the maps
			SvcProjectPosixMapDomainIdentityAdd.addLDAPDomainSIDs (executor, projectID, failIfAuthDomainNotEnabled, w);
			SvcProjectPosixMapDomainIdentityAdd.addLocalDomainSIDs (executor, projectID, failIfAuthDomainNotEnabled, w);	
		} else {
			w.add("created", "false");
		}
		w.pop();

		// Mount points are not persistent through server starts.
		// Save the mount point state in some meta-data so that it can remounted
		// when the server restarts.
		XmlDoc.Element metaExport = SvcProjectPosixMountStandard.makeStandardMetaExport();
		Boolean shouldBeMounted = true;
		Posix.addPosixMountMetaData (executor, nameSpace, restrictions, rootRestriction, READ_ONLY, 
				metaExport, APPLY_MODE_BITS, ALLOWED_POSIX_PROTOCOL,
				shouldBeMounted);	

		// Set the namespace execute permission without which Posix semantics won't work,
		// No longer done here. We now incorporate the :namespace execute
		// ACL into the standard project-role ACLs.
		// Posix.grantAssetNameSpacePosixPerm(executor, nameSpace);

		// Create the actual mount point. The identity map and mount point are the same
		// as the project ID. By default there are no network restrictions on the
		// posix mount point. We set a default meta-data export set.
		if (shouldBeMounted) {
			Posix.mountPosix (executor, projectID, metaExport, nameSpace, restrictions, 
					rootRestriction, READ_ONLY, APPLY_MODE_BITS, ALLOWED_POSIX_PROTOCOL);
		}
		w.add("location", "/Volumes/"+projectID);
		w.pop();
	}



	public static void copyToCollection (ServiceExecutor executor, String projectID, String description,
			XmlDoc.Element collectionMeta) throws Throwable {

		// This element is optional so cope with it not being there in Collection 
		XmlDoc.Element collectionDescription = collectionMeta.element("description");
		if (collectionDescription==null) {
			collectionDescription = new XmlDoc.Element ("description", description);
			collectionMeta.add(collectionDescription);
		} else {
			collectionDescription.setValue(description);
		}
		//
		/*
		XmlDoc.Element collectionKey = collectionMeta.element("key");
		collectionKey.setValue(projectID);
		 */
	}



	public static void provisionDR (ServiceExecutor executor, String projectID, String primaryStorePolicy, 
			Boolean useQueue, XmlWriter w) throws Throwable {
		w.push("Replication");

		// The namespace pattern on the DR system is
		// /<uuid of source server>/projects/<project>
		// Add this project to the standard replication queue
		if (useQueue) {
			// Set the standard asset processing queue on project namespace. This is all the
			// state you need on the namespace.
			ProjectReplicate.setStandardQueueOnProject(executor, projectID, w);
		} 
		w.pop();
	} 




	private void updateResourceAsset (ServiceExecutor executor, String assetID, String projectID) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", assetID);
		dm.push("meta", new String[]{"action", "merge"});
		dm.push(Properties.getOnboardingDocType(executor));
		dm.add("project-id", projectID);
		dm.pop();
		executor.execute("asset.set", dm.root());
	}


	public static void setStandardOnBoardMeta (ServiceExecutor executor, String assetID, String projectID, String projectNameSpace, XmlDoc.Element onBoardMeta) throws Throwable {
		XmlDoc.Element notification = onBoardMeta.element("notification");
		String type = onBoardMeta.value("type");
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", projectNameSpace);
		//dm.add("inherit-asset-meta", false);
		dm.push("asset-meta", new String[]{"inherit", "false", "propagate", "false"});
		dm.push(Properties.getProjectDocType(executor));
		if (notification!=null) {
			dm.add(notification);
		}
		dm.add("type", type);    // production or test
		dm.add("provisioning-id", assetID);   // Store the provisioning asset ID
		dm.pop();
		dm.pop();
		executor.execute("asset.namespace.asset.meta.add", dm.root());
	}



	/**
	 * Build the XML Structure for the asset namespace ACLs
	 * 
	 * @param executor
	 * @param projectID
	 * @return
	 * @throws Throwable
	 */
	private Collection<XmlDoc.Element> buildStandardAssetNameSpaceACLs (ServiceExecutor executor, String projectID, String assetNameSpace) throws Throwable {

		// Build XML for all the ACLs we set on standard projects
		XmlDocMaker dm = new XmlDocMaker("args");

		// administrator
		String actualRoleName = Project.actualRoleName(projectID, Project.ProjectRoleType.ADMIN);
		dm.add(Project.grantAssetNameSpaceACLs(executor, assetNameSpace, 
				actualRoleName, Project.ProjectRoleType.ADMIN, false));

		// participant-a
		actualRoleName = Project.actualRoleName(projectID, Project.ProjectRoleType.A);
		dm.add(Project.grantAssetNameSpaceACLs(executor, assetNameSpace, 
				actualRoleName, Project.ProjectRoleType.A, false));

		// participant-acm
		actualRoleName = Project.actualRoleName(projectID, Project.ProjectRoleType.ACM);
		dm.add(Project.grantAssetNameSpaceACLs(executor, assetNameSpace, 
				actualRoleName, Project.ProjectRoleType.ACM, false));

		// participant-acmd
		actualRoleName = Project.actualRoleName(projectID, Project.ProjectRoleType.ACMD);
		dm.add(Project.grantAssetNameSpaceACLs(executor, assetNameSpace, 
				actualRoleName, Project.ProjectRoleType.ACMD, false));

		// participant-acmd-n
		actualRoleName = Project.actualRoleName(projectID, Project.ProjectRoleType.ACMDN);
		dm.add(Project.grantAssetNameSpaceACLs(executor, assetNameSpace, 
				actualRoleName, Project.ProjectRoleType.ACMDN, false));

		actualRoleName = Project.actualRoleName(projectID, Project.ProjectRoleType.RCPUSR);
		dm.add(Project.grantAssetNameSpaceACLs(executor, assetNameSpace,
				actualRoleName, Project.ProjectRoleType.RCPUSR, false));


		// allow read-only access
		dm.add(Project.grantAssetNameSpaceACLs(executor, assetNameSpace, 
			   Project.accessAssetNamespaceRoleName(executor), 
			   Project.ProjectRoleType.A, false));

		// allow administration (usually for specialised services)
		// This permission set is not one of our standard project roles
		// so we just craft it individually.
		String roleName = Project.administerAssetNamespaceRoleName(executor);
		dm.add(Project.grantAssetNamespaceAdministerACL(executor, assetNameSpace, roleName, false));



		return dm.root().elements();
	}



	private void createStandardRoles (ServiceExecutor executor, String projectID, String visibilityRole) throws Throwable {

		// Create all the standard project roles
		Project.createStandardRoles (executor, projectID);

		// Grant the project root namespace visibility role, if there is one,
		// to the project roles
		if (visibilityRole!=null) {
			Project.grantRevokeRoleToProjectRoles(executor, projectID, visibilityRole, true);
		}
	}


	private void provisionProject (ServiceExecutor executor, String projectID,  XmlDoc.Element userOnboardMeta,
			String primaryStorePolicy, XmlWriter w) throws Throwable {		


		// Extract things we need from user meta-data
		String user  = userOnboardMeta.stringValue("account/username");
		if (user==null) {
			throw new Exception ("Field account/username not found in user resource asset.");
		}

		String quota = userOnboardMeta.value("storage/quota");
		w.add("quota", quota);
		String description = userOnboardMeta.value("description");
		w.add("description", description);
		String assetNameSpaceRoot = userOnboardMeta.value("namespace");
		Boolean collectionAsset = false;

		// Dictionary namespace
		DictionaryUtil.createDictionaryNameSpace(executor, projectID, "Project namespace");

		// Document namespace
		DocTypeUtil.createDocumentNameSpace(executor, projectID,"Project namespace");

		// Role namespace
		NameSpaceUtil.createRoleNameSpace(executor, projectID, "Project namespace");

		// Find out if the project root namespace has a visibility role associated
		// with it. If it does, we want to grant it to the project roles.
		String visibilityRole = findVisibilityRole (executor, assetNameSpaceRoot);

		// Create standard project roles and grant the visibility role if there is one
		createStandardRoles (executor, projectID, visibilityRole);

		// Asset namespace or collection asset
		if (collectionAsset) {
			throw new Exception("Collection assets not yet implemented");
		} else {

			// Create asset namespace name
			String assetNameSpace =  Project.makeProjectPath (assetNameSpaceRoot, projectID);

			// Create ACLs so we create them atomically with the asset namespace
			// This way it is never visible to other users
			Collection<XmlDoc.Element> acls =  
					buildStandardAssetNameSpaceACLs (executor, projectID, assetNameSpace);

			// Make the asset namespace with ACLs
			NameSpaceUtil.createNameSpace(executor, null, assetNameSpace, null, 
					primaryStorePolicy, null, "Project namespace", acls);

			// Apply the quota
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("namespace", assetNameSpace);
			dm.push("quota");
			dm.add("allocation", quota);
			dm.add("on-overflow", "allow");   // SOft quota
			dm.pop();
			executor.execute("asset.namespace.quota.set", dm.root());

			// Populate the mapping dictionary with the mapping from project ID to namespace
			Project.addProjectToNameSpaceMap (executor, projectID, assetNameSpace);
		}

		// Set standard permissions to access project resources.
		// This complements the setting of asset namespace ACLs
		Boolean doACLs = false;         // We set the ACLs when we created the namespace
		Boolean doPerms = true;
		Project.grantStandardPermissionsAndACLs (executor, projectID, doACLs, doPerms);


		// Now grant the admin role to the specified user.  The provisioning user holds the role <NS_ADMIN>:project-role-namespace-administrator
		// This role is granted administration access to the project role namespace 
		// Therefore provisioning user can grant a role from this namespace to someone else
		Project.grantRevokeRoleToUser(executor, Project.ProjectRoleType.ADMIN, projectID, user, true);
		Project.grantRevokeRoleToUser(executor, Project.ProjectRoleType.RCPUSR, projectID, user, true);
		w.add("user", new String[] {"role", "administrator"}, user);


		// Also grant them the basic access role as well in case this has not been done
		User.grantRevokeRole(executor, user, Properties.getStandardUserRoleName(executor), true);

		// Create a child namespace only visible to project admins (but queryable by others)
		// This is where project-based query templates go
		Project.addQueriesAssetNameSpace(executor, projectID);
	}


	public static String findVisibilityRole (ServiceExecutor executor, String assetNameSpaceRoot) throws Throwable {
		String dict = Properties.getProjectNamespaceRootsDictionaryName(executor);
		XmlDoc.Element d = DictionaryUtil.describeDictionaryTerm(executor, 
				dict, assetNameSpaceRoot);
		String defn = d.value("entry/definition");    // may be null
		return defn;
	}
}
