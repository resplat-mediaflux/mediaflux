/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;

public class SvcUserProjectList extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "user.project.list";

	private Interface _defn;

	public SvcUserProjectList() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("authority", StringType.DEFAULT, "The authentication authority for the user (only authorities thhat use the default protocol).",
				0, 1));
		_defn.add(new Interface.Element("email", EmailAddressType.DEFAULT, "The email address of the user of interest (if user not given) to be looked up for the given domain.", 0, 1));
		_defn.add(new Interface.Element("user", StringType.DEFAULT, "The user name. If not given, the email must be supplied.", 0, 1));
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain for the user. If user not given and email is given, looks them up in this domain.", 1, 1));
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A string to search for in the project identifiers to restrict which projects are selected.", 0, 1));
		_defn.add(new Interface.Element("role", new EnumType(Project.ProjectRoleType.projectRoleTypes()),
				"role of the user on the project", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "List projects that a user is a member of.";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		String authority = args.value("authority");
		String domain = args.value("domain");
		String user = args.value("user");
		String contains = args.value("contains");
		String email = args.value("email");
		String role = args.value("role");
		//
		if (user==null && email==null) {
			throw new Exception ("You must supply user or email");
		}
		if (user!=null && email!=null) {
			throw new Exception ("You must supply user or email");
		}

		// Find the user from email as required
		if (email!=null) {
			Collection<XmlDoc.Element> users = User.findUser(executor(), domain, email);
			if (users!=null) {
				authority = null;
				for (XmlDoc.Element theUser : users) {
					user = theUser.value("@user");
					w.push("user", 
							new String[] {"email", email, 
									"name", theUser.value("@name"),
									"domain", domain,
									"user", user});
					list(executor(), authority, domain, user, contains,role, w);
					w.pop();
				}
			}
		} else {
			w.push("user", new String[] {"domain", domain,"user", user});
			list(executor(), authority, domain, user, contains,role, w);
			w.pop();
		}
	}

	public static void list(ServiceExecutor executor, String authority, String domain, String user, String contains,String withRole,
			XmlWriter w) throws Throwable {
		// List Projects
		Collection<String> projectIDs = Project.projectIDs(executor, true, true);
		if (projectIDs == null) {
			return;
		}

		// Iterate through projects
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor, projectID, false);

			if (contains == null || projectID.toLowerCase().contains(contains.toLowerCase())) {

				// Get roles
				Vector<String> roles;
				if (withRole == null) {
					roles = Project.actualRoleNames(projectID);
				}else{
					roles = new Vector<String>();
					roles.add(Project.actualRoleName(projectID,withRole));
				}


				// See if user holds any role from this project (should be just one)
				Vector<String> heldRoles = new Vector<String>();
				for (String role : roles) {
					if (User.hasRole(executor, authority, domain, user, role)) {
						heldRoles.add(role);
					}
				}
				if (heldRoles.size() > 0) {
					// FInd attributes
					String qName = ProjectReplicate.isProjectReplicated(ph);
					//
					String parentNameSpace = ph.parentNameSpace();
					//
					XmlDoc.Element nsMeta = ph.metaData();
					String projectType = Project.projectType(executor, nsMeta);
					w.push("project");
					if (qName != null) {
						w.add("project", new String[] { "parent", parentNameSpace, 
								"type", projectType,
								"replication", qName }, ph.id());
					} else {
						w.add("project", new String[] { "parent", parentNameSpace, 
								"type", projectType,
								"replication", "none" }, ph.id());
					}
					for (String heldRole : heldRoles) {
						Boolean writable = !Project.isRoleReadOnly(heldRole);
						w.add("role", new String[] { "writable", writable.toString() }, 
								heldRole);
					}
					w.pop();
				}

			}
		}
	}
}