/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginService.Outputs;
import arc.mf.plugin.dtype.StringType;
import arc.streams.StreamCopy;
import arc.xml.XmlDoc.Element;
import arc.xml.CSVUTF8Writer;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlFileOutput;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcTest extends PluginService {



	private Interface _defn;

	public SvcTest()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("name", StringType.DEFAULT, "The new child namespace name", 
				0, 1));		

	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Test";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "test";
	}

	public boolean canBeAborted() {
		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}



	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		Vector<String> rows = new Vector<String>();
		rows.add("a b c");
		rows.add("d e f");
		rows.add("g h i");
		
		writeClientSideFile (outputs, rows);
	}

	public static void writeClientSideFile (Outputs outputs,  Vector<String> rows) throws Throwable  {
		PluginService.Output out = outputs.output(0);
        StringBuilder sb = new StringBuilder();
		for (String row : rows) {
            sb.append(row).append("\n");
		}
        byte[] bytes = sb.toString().getBytes();
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        out.setData(bis, bytes.length, "text/xml");
	}
	
	
}
