/** 
 * @author Robert Hutton
 *
 * Copyright (c) 2023, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.replication;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
//import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
//import au.org.vicnode.mflux.plugin.util.AssetProcessingQueue;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectReplicateQueueGenerateCleanupScript extends PluginService {

	private Interface _defn;

	public SvcProjectReplicateQueueGenerateCleanupScript()  throws Throwable {
		_defn = new Interface();
        _defn.add(new Interface.Element("name", StringType.DEFAULT, "Name of the queue to generate cleanup script for", 1, 1));
        _defn.add(new Interface.Element("type", new EnumType(new String[]{"path","rid"}), "Type of error to handle: path collision or rid collision.", 1, 1));
        _defn.add(new Interface.Element("size", IntegerType.POSITIVE, "Maximum number of items to retrieve from the queue.  Default 100.", 0, 1));
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

    public int maxNumberOfOutputs() {
        return 1;
    }
    
    public int minNumberOfOutputs() {
        return 0;
    }

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Generate a TCL script to be run on the DR server to remove assets that have already replicated but are now attempting to re-replicate but cannot due to already existing.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.replicate.queue.generate.cleanup.script";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		/*w.push("processing-queue");
		String queueName = ProjectReplicate.createOrFindStandardQueue(executor(), w);
		XmlDoc.Element q = AssetProcessingQueue.describeQueue(executor(), 
				queueName, false);
		w.add("processing-errors-before-reset", q.value("queue/processing/nb-processing-failures"));
		AssetProcessingQueue.resetQueue(executor(), queueName);
		w.add("reset", true);
		w.pop();*/
		if (args.value("type").equals("path")) {
        	ProjectReplicate.generateTCLScriptToCleanUpExistingAssetsPath(executor(), outputs, args.value("name"), args.value("size"), w);
		}
		else if (args.value("type").equals("rid")) {
			ProjectReplicate.generateTCLScriptToCleanUpExistingAssetsRid(executor(), outputs, args.value("name"), args.value("size"), w);
		}
		else {
			throw new Exception("Invalid type specified.  Must be path or rid.");
		}
	}
}