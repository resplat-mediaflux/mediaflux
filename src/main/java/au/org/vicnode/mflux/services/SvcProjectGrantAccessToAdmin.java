/** 
	<version>1.9.1</version>
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

/**
 * Depends on nig-essentials package
 */


import java.util.Vector;

import arc.mf.plugin.*;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectGrantAccessToAdmin extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = false;

	public SvcProjectGrantAccessToAdmin() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.grant.access.to.admin";
	}

	public String description() {
		return "Service to 1) grant the role $NS_ADMIN:project-access ACCESS to the project meta-data and dictionary namespaces and 2)  grant the role  $NS_ADMIN:project-role-namespace-administrator ADMINISTER of the project role name space. These roles are held by $NS_ADMIN:administrator.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;


		for (String projectID : projectIDs) {
			// Grant ADMINSTER on the role name space
			Project.grantNameSpacePermOnRole(executor(), projectID, Project.roleNameSpaceAdminRoleName(executor()), "role", "ADMINISTER");
			
			// Grant ACCESS on the meta-data and dictionary namespaces
			Project.grantNameSpacePermOnRole(executor(), projectID, Project.accessAssetNamespaceRoleName(executor()), "document", "ACCESS");
			Project.grantNameSpacePermOnRole(executor(), projectID, Project.accessAssetNamespaceRoleName(executor()), "dictionary", "ACCESS");
			w.add("project-id", projectID);
		}
	}
}
