/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectIngestRate extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectIngestRate()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
		_defn.add(new Interface.Element("year", StringType.DEFAULT, "The calendar year of interest (no default)", 1, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("month", StringType.DEFAULT, "The calendar month(s) of interest (defaults to all). Use 3 letters: Jan, Feb, Mar etc", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("sort", BooleanType.DEFAULT, "Sort projects by total size ingested (true) over the entire time period selected.", 0, 1));
		_defn.add(new Interface.Element("contains", StringType.DEFAULT, "A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",	 0, 1));
		_defn.add(new Interface.Element("size", IntegerType.DEFAULT, "The number of projects to list (if sorting, it's the sorted list that is shortened).",	 0, 1));
		SvcProjectDescribe.addProdType (_defn);
		SvcProjectDescribe.addRepType (_defn);
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns monthly ingest rates of the given projects.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.ingest.rate";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		Collection<String> years = args.values("year");	
		Collection<String> months = args.values("month");				
		Boolean sortBySize = args.booleanValue("sort",  true);
		String prodType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		String repType = args.stringValue("rep-type", Properties.repTypes[0]);
		String keyString = args.value("contains");
		if (keyString!=null) keyString = keyString.toUpperCase();
		int size = args.intValue("size",-1);

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;


		// Iterate and summarise
		ArrayList<XmlDoc.Element> projectResults = new ArrayList<XmlDoc.Element>();
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			// FInd replication asset processing queue
			String qName = ProjectReplicate.isProjectReplicated(ph);

			if (Project.keepOnRepType (executor(), qName, repType) && 
				Project.keepOnProjectType(ph, prodType) &&
				Project.keepOnKeyString(ph.id(), keyString)) {
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("where", "namespace>='"+ph.nameSpace()+"'");
				for (String year : years) {
					dm.add("year", year);
				}
				if (months!=null) {
					for (String month : months) {
						dm.add("month", month);
					}
				}
				XmlDoc.Element r = executor().execute("unimelb.asset.ingest.rate", dm.root());
				//
				XmlDocMaker dm2 = new XmlDocMaker("args");
				dm2.push("project");
				dm2.add("project-id", ph.id());
				dm2.addAll(r.elements());			
				dm2.pop();
				projectResults.add(dm2.root());
			}
			PluginTask.checkIfThreadTaskAborted();
		}
		//
		if (size==-1) {
			size = projectResults.size();
		}
		if (sortBySize) {
			ArrayList<XmlDoc.Element> projectsSorted = sortByTotalIngest(projectResults);
			int i = 1;
			for (XmlDoc.Element project : projectsSorted) {
				w.addAll(project.elements());
				i++;
				if (i>size) {
					break;
				}
			}
		} else {
			int i = 1;
			for (XmlDoc.Element project : projectResults) {
				w.addAll(project.elements());
				i++;
				if (i>size) {
					break;
				}
			}

		}
	}


	private static ArrayList<XmlDoc.Element> sortByTotalIngest (ArrayList<XmlDoc.Element> projectResults) throws Throwable {
		if (projectResults == null || projectResults.isEmpty()) {
			return null;
		}
		ArrayList<XmlDoc.Element> list = new ArrayList<XmlDoc.Element>(projectResults);
		Collections.sort(list, new Comparator<XmlDoc.Element>() {

			@Override
			public int compare(XmlDoc.Element d1, XmlDoc.Element d2)   {

				Double d1d = null;
				try {
					// "1.23 GB"
					String[] s = d1.value("project/total-size-ingested").split(" ");
					d1d = Double.parseDouble(s[0]);
				} catch (Throwable e) {
				}

				Double d2d = null;
				try {
					// "1.23 GB"
					String[] s = d2.value("project/total-size-ingested").split(" ");
					d2d = Double.parseDouble(s[0]);
				} catch (Throwable e) {
				}
				// This will never happen but we have to handle the exception
				if (d1d==null || d2d==null) {
					return 0;
				}
				int retval = Double.compare(d1d, d2d);

				if(retval > 0) {
					return -1;
				} else if(retval < 0) {
					return 1;
				} else {
					return 0;
				}

			}
		});
		return list;
	}

}
