package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectProvisionMessageGet extends PluginService {

	public final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.provision.message.get";

	private Interface _defn;

	public SvcProjectProvisionMessageGet() {
		_defn = new Interface();
		addArgDefn(_defn);
	}

	static void addArgDefn(Interface defn) {
		defn.add(new Interface.Element("project-id", StringType.DEFAULT, "The project id.", 1, 1));
		defn.add(new Interface.Element("domain", StringType.DEFAULT,
				"The authentication domain of the recipient user (must be a project administrator).", 1, 1));
		defn.add(new Interface.Element("user", StringType.DEFAULT,
				"The username of the recipient user (must be a project administrator).", 1, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Return generated project provision message for the specified project and user";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		String projectId = args.value("project-id");
		String domain = args.value("domain");
		String user = args.value("user");
		if (!isProjectAdmin(executor(), domain, user, projectId)) {
			throw new IllegalArgumentException(
					"User " + domain + ":" + user + " is not administrator of project: " + projectId);
		}
		String message = generateProvisionMessage(executor(), projectId, domain, user);
		w.add("message", message);
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	static String generateProvisionMessage(ServiceExecutor executor, String projectId, String domain, String user)
			throws Throwable {
		XmlDoc.Element ue = describeUser(executor, domain, user);
		return generateProvisionMessage(executor, projectId, ue);
	}

	static String generateProvisionMessage(ServiceExecutor executor, String projectId, XmlDoc.Element ue)
			throws Throwable {
		String domain = ue.value("@domain");
		String user = ue.value("@user");
		String name = ue.value("name");
		int idx = name.indexOf(' ');
		String firstName = idx > 0 ? name.substring(0, idx) : name;
		XmlDoc.Element pe = describeProject(executor, projectId);
		StringBuilder sb = new StringBuilder();
		sb.append("Dear ").append(firstName).append(",\n\n");
		sb.append(
				"Thank you for your storage request. You are now the administrator of the following new Mediaflux project:\n\n");
		sb.append("Project name: ").append(projectId).append("\n");
		String quota = pe.value("quota/allocation/@h");
		if (quota != null) {
			sb.append("Quota: ").append(quota).append("\n");
		}
		String namespace = pe.value("namespace");
		sb.append("Full path to project in Mediaflux: ").append(namespace).append("\n\n\n");
		sb.append("1. Logging in to your project:\n");
		sb.append(
				"Access your Mediaflux project with your central University credentials (Active Directory). Mediaflux requires a domain and username, in your case:\n\n");
		sb.append("        domain: ").append("unimelb-student".equals(domain) ? "student" : domain).append("\n");
		sb.append("        username: ").append(user).append("\n");
		sb.append("        password: <your UoM password>\n\n\n");
		sb.append("2. Getting started, finding out more:\n");
		sb.append(
				"The primary user guides for ongoing reference are maintained by Research Computing Services: https://go.unimelb.edu.au/x4t8\n\n");
		sb.append(
				"But if you've never used Mediaflux before, we recommend using the Mediaflux Explorer as the best way to get started: https://go.unimelb.edu.au/r4t8\n\n");
		sb.append(
				"There are also other ways to access Mediaflux; these are described here: https://go.unimelb.edu.au/r2t8\n\n");
		sb.append(
				"Additionally, if you also use our HPC platform (Spartan) with Mediaflux, see this guide to copying data between the two systems: https://go.unimelb.edu.au/i4t8\n\n");
		sb.append("3. Giving other users access to your project:\n");
		sb.append(
				"To give other people access to your project, please fill out this ServiceNow request form (http://go.unimelb.edu.au/zak6), letting us know their name, email, which project to add them to and what role they should have. Here is the guide for adding/removing people to/from your project: https://go.unimelb.edu.au/e4t8\n\n");
		sb.append("4. Getting help and support:\n");
		sb.append(
				"For any help with Mediaflux, please request support via our contact channels: https://go.unimelb.edu.au/ra7i\n\n");
		sb.append("5. Keep your collection information up to date:\n");
		sb.append(
				"Review and keep your collection information up to date using the Research Computing Services (RCS) Storage Dashboard: https://dashboard.storage.unimelb.edu.au\n\n");
		sb.append(
				"The storage dashboard is available on the University network or via the VPN (Virtual Private Network), please refer this guide on how to connect to the University VPN Network: https://go.unimelb.edu.au/2aza\n\n\n");
		sb.append("Regards,\n\n");
		sb.append("Data Solutions Team\n");
		sb.append("Research Computing Services | Business Services\n");
		sb.append("The University of Melbourne\n");
		return sb.toString();
	}

	static XmlDoc.Element describeProject(ServiceExecutor executor, String projectId) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("project-id", projectId);
		// :DR false
		dm.add("DR", false);
		// :users false
		dm.add("users", false);
		// :debug false
		dm.add("debug", false);
		// :sum false
		dm.add("sum", false);
		// :sort false
		dm.add("sort", false);
		// :posix false
		dm.add("posix", false);
		// :name-validation false
		dm.add("name-validation", false);
		// :multi-version false
		dm.add("multi-version", false);
		return executor.execute(SvcProjectDescribe.SERVICE_NAME, dm.root()).element("project");
	}

	static XmlDoc.Element describeUser(ServiceExecutor executor, String domain, String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		return executor.execute("user.describe", dm.root()).element("user");
	}

	static boolean isProjectAdmin(ServiceExecutor executor, String domain, String user, String projectId)
			throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("type", "user");
		dm.add("name", domain + ":" + user);
		dm.add("role", new String[] { "type", "role" }, projectId + ":administrator");
		return executor.execute("actor.have", dm.root()).booleanValue("actor/role");
	}
}
