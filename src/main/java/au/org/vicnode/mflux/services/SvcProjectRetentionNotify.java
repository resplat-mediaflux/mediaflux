/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;

public class SvcProjectRetentionNotify extends PluginService {




	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectRetentionNotify()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("notify-user", BooleanType.DEFAULT,
				"Notify the retention recipient (in namespace meta-data) of the project. Defaults to true.", 0, 1));
		_defn.add(new Interface.Element("notify-ops", BooleanType.DEFAULT,
				"Notify the Mediaflux ops team (project-provisioning application property notification.operations.email).  Defaults to true.", 0, 1));

		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "This service inspects the VicNode:Collection/retention element attached to Project namespaces. It sends a notificasiton to the recipients if the review time has been reached. If no email recipient is specified, it goes to the service operations email. This service is generally run in a regular scheduled job.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.retention.notify";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String theProjectID = args.stringValue("project-id");
		Boolean notifyUser = args.booleanValue("notify-user", true);
		Boolean notifyOps = args.booleanValue("notify-ops", true);
		if (!notifyOps && !notifyUser) {
			throw new Exception ("One of notify-user and notify-ops must be true");
		}

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;

		//
		for (String projectID : projectIDs) {
			notify (executor(), projectID, notifyUser, notifyOps, w);
		}
	}

	private void notify (ServiceExecutor executor, String projectID, 
			Boolean notifyUser, Boolean notifyOps, XmlWriter w) throws Throwable {

		Date now = Util.serverDate(executor);
		String emailOPS = Properties.getApplicationProperty(executor, Properties.APPLICATION_PROPERTY_OPERATIONS_EMAIL, 
				Properties.APPLICATION_PROPERTY_APP);

		//
		XmlDoc.Element meta = Project.describeNameSpace(null, executor, projectID, false);
		XmlDoc.Element retention = meta.element("namespace/asset-meta/" + Properties.getCollectionDocType(executor) + "/retention");
		if (retention!=null) {
			Date review = retention.dateValue("review");
			String action = retention.value("action");
			if (review!=null) {
				if (now.after(review)) {
					if (action.equalsIgnoreCase("notify")) {
						w.add("project", projectID);

						String subject = "Expiration of retention period for Mediaflux-managed collection of name : '" + projectID +"'";
						String body = "Dear Colleague,\n\n"+
								"The research data collection called '" + projectID + "' has reached " +
								"its retention review date.  This means that it can potentially be destroyed. " +
								"The Mediaflux operations team will contact you to review this collection.\n\n" +
								"Regards\n"+
								"Research Computing Services - Data Solutions Team\nBusiness Services\nThe University of Melbourne\n";

						// Emails of contacts
						ArrayList<String> emails = new ArrayList<String>();

						// Notify contacts
						Collection<XmlDoc.Element> contacts = retention.elements("contacts/contact");
						if (contacts!=null) {							
							for (XmlDoc.Element contact : contacts) {
								String name = contact.value("name");
								String email = contact.value("email");
								if (email==null) {
									// Use service operations email if none supplied in meta-data
									String property = Properties.APPLICATION_PROPERTY_OPERATIONS_EMAIL;
									email = Properties.getApplicationProperty(executor, property, Properties.APPLICATION_PROPERTY_APP);
								}
								emails.add(email);
								if (notifyUser) {
									w.add("contact", email);
									if (name==null) {
										name = "Colleague";
									}
									String body2 = "Dear " + name + "\n\n" + body;
									MailHandler.sendMessage(executor, email, subject, body2, null, false, null);
								}
							}
						}


						// Notify Mediaflux operations support
						if (notifyOps) {
							String body2 = null;
							if (notifyUser) {
								body2 = "Dear Mediaflux operations,\n\n" +
										"The following message was sent to the list of contacts provided in " +
										"the 'retention' meta-data element associated with project '"+ projectID +"'.\n\n" +
										"********************************************************************************** \n" +
										body + "\n" +				 
										"********************************************************************************** \n\n" +
								"The list of user email contacts is:\n";
								for (String em : emails) {
									body2 += "   " + em + "\n";
								}
								body2 += "\n Please make contact with them and discuss the future of this collection which may include destruction.\n\n" +
										"Regards\n" +
										"The Research Computing Services - Data Solutions Team from the past\n";
							} else {
								body2 = "Dear Mediaflux operations,\n\n" +
										"The following message was NOT sent to the list of contacts provided in " +
										"the 'retention' meta-data element associated with project '"+ projectID +"'.\n\n" +
										"********************************************************************************** \n" +
										body + "\n" +				 
										"********************************************************************************** \n\n" +
								"The list of user email contacts is:\n";
								for (String em : emails) {
									body2 += "   " + em + "\n";
								}
								body2 += "\n Please discuss this collection and optionally make contact with the users and discuss the future of this collection which may include destruction.\n\n" +
										"Regards\n" +
										"The Research Computing Services - Data Solutions Team from the past\n";
							}
							w.add("contact", emailOPS);
							MailHandler.sendMessage(executor, emailOPS, subject, body2, null, false, null);
						}
					}
				}
			}
		}
	}
}
