/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.contentcopy;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.ProjectContentCopy;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.services.SvcProjectDescribe;

public class SvcProjectContentCopyQueueUnset extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectContentCopyQueueUnset()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addMandatoryProjectIDs(_defn,  allowPartialCID);
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "This service unsets the standard asset processing queue for content copies on the specified projects.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.content-copy.queue.unset";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Check the system is configured for content copies	
		if (!ProjectContentCopy.isConfigured(executor())) {
			throw new Exception ("Content copies are not configured on this system");
		}

		// Inputs
		Collection<String> theProjectIDs = args.values("project-id");	

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs.size()>0) {
			for (String theProjectID : theProjectIDs) {
				String t[] = Project.validateProject(executor(), 
						theProjectID, allowPartialCID);
				projectIDs.add(t[1]);
			}
		}

		// Unset queue
		for (String projectID : projectIDs) {
			w.push("project");
			w.add("id", projectID);
			w.add("type", Project.projectType(executor(), projectID));
			ProjectContentCopy.unsetStandardQueueOnProject(executor(), projectID, w);
			w.pop();
		}
	}
}
