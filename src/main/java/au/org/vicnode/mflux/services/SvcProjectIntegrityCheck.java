/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

/**
 * Depends on nig-essentials package
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Vector;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;

public class SvcProjectIntegrityCheck extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectIntegrityCheck()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace-path", StringType.DEFAULT, "Additional namespaces (which are not part of the projects structure such as the /avarchives or /apps) which should be checked. Parameters prod-type and rep-type are irrelevant to this parameter. Defaults to none.", 0, Integer.MAX_VALUE));
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
		SvcProjectDescribe.addIgnoreProjectIDs(_defn);
		//
		Interface.Element ie = new Interface.Element("email", StringType.DEFAULT, "Email address to send report to. Defaults to no email.", 0, 1);
		ie.add(new Interface.Attribute("subject", StringType.DEFAULT,
				"Subject of message. If not specified, a default is used.", 0));
		ie.add(new Interface.Attribute("subject-suffix", StringType.DEFAULT,
				"A suffix string directly appended to the auto-generated subject.", 0));
		_defn.add(ie);
		//
		_defn.add(new Interface.Element("list-all", BooleanType.DEFAULT, "List the results from all projects. Defaults to just the ones that need attention.", 0, 1));
		_defn.add(new Interface.Element("debug", BooleanType.DEFAULT, "Write stuff in server log file. Defaults to false.", 0, 1));
		ie = new Interface.Element("count", BooleanType.DEFAULT, "Just counts the number of assets on the primary and DR systems and compares.  Returns a result if they differ. If there is no equivalent DR namespace for the primary namespace then it's a nullop). Of modest value. Defaults to false.  Does this after all other operations.", 0, 1);
		_defn.add(ie);
		//
		_defn.add(new Interface.Element("integrity", BooleanType.DEFAULT, "Namespace integrity check (checks entire namespace). Does not currently attempt to recover faults. Defaults to false (it's quite slow for large numbers of assets). Output in element 'integrity'.", 0, 1));
		//	
		ie = new Interface.Element("staging", BooleanType.DEFAULT, "Check that no assets are left in the staging state. Defaults to false.", 0, 1);
		ie.add(new Interface.Attribute("force-end", BooleanType.DEFAULT,
				"Force end the staging (default false) if it has been staging for longer than the specified time. ", 0));
		ie.add(new Interface.Attribute("delay", StringType.DEFAULT,
				"The delay in seconds to 1) report files are still in staging and 2) optional force commit them. The default is 6 hours.", 0));
		_defn.add(ie);
		//		
		ie = new Interface.Element("content-validation", BooleanType.DEFAULT, "Validates the content (compares checksum with recomputed value).  This is quite time consuming! Validates all content copies but only the latest asset version. Defaults to false.", 0, 1);
		ie.add(new Interface.Attribute("all-versions", BooleanType.DEFAULT,
				"Validate all asset versions. Defaults to false, in which case it just validates the most recent asset version.", 0));
		ie.add(new Interface.Attribute("nb-threads", IntegerType.DEFAULT,
				"Number of threads to run in parallel. Defaults to 1.", 0));
		_defn.add(ie);
		//
		ie = new Interface.Element("index-validation", BooleanType.DEFAULT, "Checks  assets for correct indices. Optionally repairs any that are broken (including missing - so indexes are turned off).  Defaults to false.", 0, 1);
		ie.add(new Interface.Attribute("action", new EnumType(
				new String[] { "status", "recover"}),
				"The action to perform. Defaults to 'recover'. If recover no output can be returned.", 0));
		ie.add(new Interface.Attribute("nb-threads", IntegerType.DEFAULT,
				"Pumber of threads to run in parallel. Defaults to 1.", 0));
		_defn.add(ie);
		//
		_defn.add(new Interface.Element("filename-validation", BooleanType.DEFAULT, "Checks assets for correct filenames using indexes.  Defaults to false.", 0, 1));
		//
		_defn.add(new Interface.Element("where",StringType.DEFAULT, "Query predicate to further restrict (for the given project[s])  the selected assets on the local host. You cannot supply 'where' and 'age'", 0, 1));
		//
		ie = new Interface.Element("zero-size", StringType.DEFAULT, "Check for assets with content of zero size (defaults to false). Count presented if non-zero in element 'count-zero-size'.",	 0, 1);
		ie.add(new Interface.Attribute("notify-project-team", BooleanType.DEFAULT,
				"Notifies the configured (in namespace meta-data Project/notification/zero-count) project members as well as the service 'email' destination. Default is false.", 0));
		ie.add(new Interface.Attribute("subject-suffix-for-project-team", StringType.DEFAULT,
				"This is the subject suffix (the prefix is 'Mediaflux project : '<project>') in the email notification for project team members. If not supplied, a default subject is used.", 0));
		_defn.add(ie);
		//
		ie = new Interface.Element("incomplete", BooleanType.DEFAULT, "Check (default false) for incomplete assets that may arise from a failed upload (no content and no appopriate meta-data. Exemplar was failed sftp uploads). Output in element 'count-incomplete'.", 0, 1);
		ie.add(new Interface.Attribute("protocol", new EnumType(new String[] {"ssh", "smb", "nfs", "http", "dicom"}),
				"Which protocol to screen for. Don't set for all protocols. Pick from 'ssh' (sftp), 'smb', 'nfs', 'http' and 'dicom'", 0));
		ie.add(new Interface.Attribute("list-path", BooleanType.DEFAULT,
				"List the paths of all such assets (default false), else just counts.", 0));
		_defn.add(ie);
		//
		SvcProjectDescribe.addProdType (_defn);
		SvcProjectDescribe.addRepType (_defn);

	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Checks projects for: 0) namespace integrity (index checks), 1) have all data been replicated, 2) is there data on the DR not on the primary (possibly for destruction) and 3) have there been namespace moves that need to be tracked.  Depends on nig-essentials services package being installed.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.integrity.check";
	}

	public boolean canBeAborted() {

		return true;
	}


	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		String project = args.value("project-id");
		Collection<String> namespacePaths = args.values("namespace-path");
		Boolean listAll = args.booleanValue("list-all");
		Boolean integrity = args.booleanValue("integrity", false);
		XmlDoc.Element staging = args.element("staging");
		XmlDoc.Element zeroSize = args.element("zero-size");
		XmlDoc.Element  contentValidation = args.element("content-validation");
		XmlDoc.Element  indexValidation = args.element("index-validation");
		Boolean filenameValidation = args.booleanValue("filename-validation", false);

		//
		Boolean debug = args.booleanValue("debug", false);
		Boolean count = args.booleanValue("count", false);
		//
		Collection<String> theIgnores = args.values("ignore");
		XmlDoc.Element email = args.element("email");
		String repType = args.stringValue("rep-type",  Properties.repTypes[0]);
		String prodType = args.stringValue("prod-type",Properties.PROJECT_TYPE_ALL);
		String where = args.value("where");
		XmlDoc.Element incomplete = args.element("incomplete");


		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (project!=null) {
			String t[] = Project.validateProject(executor(), project, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) return;
		
		// List of ignores
		Vector<String> ignores = new Vector<String>();
		if (theIgnores!=null) {
			for (String theIgnore : theIgnores) {
				String t[] = Project.validateProject(executor(), theIgnore, true);
				ignores.add(t[1]);
			} 
		}


		// Iterate over projects
		String today = DateUtil.todaysTime(0);
		XmlDocMaker dm = new XmlDocMaker("args");
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);

			// Find replication  asset processing queue
			String qName = ProjectReplicate.isProjectReplicated(ph);

			// We AND the type and ignore checks.
			if (Project.keepOnProjectType(ph, prodType) &&
					Project.keepOnRepType(executor(), qName, repType) &&
					keepOnList(ph.id(), ignores)) {
				if (debug) {
					System.out.println("Keeping Project " + projectID);
				}
				XmlDocMaker dmp = new XmlDocMaker("args");
				Boolean some = checkProject (executor(), contentValidation, indexValidation, filenameValidation, zeroSize, debug,
						ph, where, integrity,  count, incomplete, staging, dmp);
				if (some || listAll) {
					dm.add(dmp.root(),false);
				}
			}
			PluginTask.checkIfThreadTaskAborted();
		}

		// Iterate over addition namespaces
		if (namespacePaths!=null) {
			for (String namespacePath :namespacePaths) {
				XmlDocMaker dmp = new XmlDocMaker("args");
				Boolean some = checkNameSpacePath (executor(), contentValidation, indexValidation, filenameValidation, zeroSize, debug,
						namespacePath, where, integrity,  count,  incomplete, staging, dmp);
				if (some || listAll) {
					dm.add(dmp.root(),false);
				}
			}
		}


		// Report to caller
		w.add(dm.root(), false);

		// Email. We send regarldess of whether issues are found or not.
		if (email!=null) {
			String msg = XmlPrintStream.outputToString(dm.root());
			if (msg.isEmpty()) {
				msg = "No issues found";
			}
			String subject = email.value("@subject");
			String subjectSuffix = email.value("@subject-suffix");
			if (subject==null) {
				subject = "[" + Util.serverUUID(executor()) + "]" +			
						" Mediaflux Project Integrity Summaries (date=" + today + ", where = " + where +")";
				if (subjectSuffix!=null) {
					subject += subjectSuffix;
				}
			}
			ArrayList<String> emails = new ArrayList<String>();
			emails.add(email.value());
			String from = Util.getServerProperty(executor(), "notification.from");
			MailHandler.sendMessage (executor(), emails, subject, msg, null, false, from, w);
		}
	}


	private Boolean keepOnList (String t, Collection<String> ids) {
		if (ids==null) return true;
		for (String id : ids) {
			// We don't keep this one as it's found in the ignore list.
			if (t.equals(id)) return false;
		}
		return true;
	}



	private Boolean  checkProject (ServiceExecutor executor,  XmlDoc.Element contentValidation, XmlDoc.Element indexValidation, Boolean filenameValidation,
			XmlDoc.Element zeroSize, Boolean debug, ProjectHolder ph,  String where, 
			Boolean integrity, Boolean count, XmlDoc.Element incomplete, 
			XmlDoc.Element staging, XmlDocMaker dm) throws Throwable {		

		// Fetch Project namespace meta-data
		String assetNameSpace = ph.nameSpace();
		String assetNameSpaceDR = ph.nameSpaceDR();

		// Ugly to have this out here, but otherwise the checkNameSpace function depends on projectID and then I can't
		// re-use it in the pure namespace (non project) context
		Collection<String> projectTeamEmails =  null;
		String zeroSizeSubjectForProjectTeam = null;
		if (zeroSize!=null && zeroSize.booleanValue()) {
			if (zeroSize.booleanValue("@notify-project-team", false)) {
				XmlDoc.Element nameSpaceDescription = NameSpaceUtil.describe(null, executor, assetNameSpace);
				String projectDocType = Properties.getProjectDocType(executor);
				XmlDoc.Element projectNotificationMeta = nameSpaceDescription.element("namespace/asset-meta/" + projectDocType + "/notification");
				projectTeamEmails = addUsers (ph.id(), projectNotificationMeta, "zero-size");
			}

			zeroSizeSubjectForProjectTeam = "Mediaflux project : '" + ph.id() + "' - zero-sized files detected in daily integrity checks";
			String t = zeroSize.stringValue("@subject-suffix-for-project-team");
			if (t!=null) {
				zeroSizeSubjectForProjectTeam = "Mediaflux project : '" + ph.id() + "' " + t;
			}
		}
		//
		dm.push("project", new String[]{"id", ph.id()});

		Boolean some = checkNameSpace (executor, assetNameSpace, assetNameSpaceDR, projectTeamEmails, zeroSizeSubjectForProjectTeam,
				contentValidation, indexValidation, filenameValidation, zeroSize, debug,
				where, integrity, count, incomplete, staging, dm);
		dm.pop();

		return some;
	}

	private Boolean  checkNameSpacePath (ServiceExecutor executor,  XmlDoc.Element contentValidation, XmlDoc.Element indexValidation, Boolean filenameValidation,
			XmlDoc.Element zeroSize, Boolean debug,  String assetNameSpace, String where, 
			Boolean integrity, Boolean count,  XmlDoc.Element incomplete, 
			XmlDoc.Element staging, XmlDocMaker dm) throws Throwable {		

		String assetNameSpaceDR =  ProjectReplicate.makeNameSpaceNameForDR(executor, assetNameSpace);
		//
		dm.push("namespace", new String[]{"path", assetNameSpace});

		Boolean some = checkNameSpace (executor, assetNameSpace, assetNameSpaceDR, null, null, 
				contentValidation, indexValidation, filenameValidation, zeroSize, debug,
				where, integrity, count, incomplete, staging, dm);
		dm.pop();

		return some;
	}

	private Boolean checkNameSpace (ServiceExecutor executor, String assetNameSpace, String assetNameSpaceDR, 
			Collection<String> projectTeamEmails, String zeroSizeSubjectForProjectTeam,
			XmlDoc.Element contentValidation, XmlDoc.Element indexValidation, Boolean filenameValidation,
			XmlDoc.Element zeroSize, Boolean debug,  String where, 
			Boolean integrity, Boolean count, 
			XmlDoc.Element incomplete,  XmlDoc.Element staging, XmlDocMaker dm) throws Throwable {

		Boolean someI = false;
		Boolean someC = false;
		Boolean someZ = false;
		Boolean someCV = false;
		Boolean someIV = false;
		Boolean someFV = false;
		Boolean someIN = false;
		Boolean someS = false;

		// Namespace index integrity check
		if (integrity) {
			if (debug) {
				System.out.println("   Starting integrity check");
			}
			PluginTask.checkIfThreadTaskAborted();
			someI = integrityCheck (executor(), assetNameSpace, dm);
		}

		// Make sure assets are not stuck  in staging (sftp/smb)
		if (staging!=null && staging.booleanValue()) {
			if (debug) {
				System.out.println("   Starting staging check");
			}
			PluginTask.checkIfThreadTaskAborted();
			Boolean forceEnd = staging.booleanValue("@force-end", false);
			Long delay = staging.longValue("@delay", 21600);                  // Seconds (6 hours default)
			delay *= 1000;  // msec

			someS = stagingCheck (executor(), assetNameSpace, where, forceEnd, delay, dm);
		}


		// Count the assets
		if (count) {
			if (debug) {
				System.out.println("   Starting count");
			}
			PluginTask.checkIfThreadTaskAborted();
			someC = countCheck (executor(), assetNameSpace, assetNameSpaceDR, where, dm);
		}


		if (zeroSize!=null && zeroSize.booleanValue()) {
			if (debug) {
				System.out.println("   Starting zero size");
			}
			PluginTask.checkIfThreadTaskAborted();
			someZ = countZeroSize (executor(), projectTeamEmails, zeroSizeSubjectForProjectTeam, assetNameSpace, assetNameSpaceDR,  where, dm);	
		}

		if (incomplete!=null && incomplete.booleanValue()) {
			if (debug) {
				System.out.println("   Starting incomplete");
			}
			Boolean listPath = false;
			listPath = incomplete.booleanValue("@list-path", false);
			String protocol = null;
			protocol = incomplete.value("@protocol");

			PluginTask.checkIfThreadTaskAborted();
			someIN = countIncomplete (executor(), assetNameSpace,  where,  listPath, protocol, dm);	
		}

		if (contentValidation!=null && contentValidation.booleanValue()) {
			if (debug) {
				System.out.println("   Starting content validation");
			}
			PluginTask.checkIfThreadTaskAborted();
			Integer nbThreads = contentValidation.intValue("@nb-threads", 1);
			Boolean checkAllVersions = contentValidation.booleanValue("@all-versions", false);
			someCV = checkSumValidationCheck (executor(),  assetNameSpace,  where, 
					nbThreads, checkAllVersions, dm);	
		}

		if (indexValidation!=null && indexValidation.booleanValue()) {
			if (debug) {
				System.out.println("   Starting index validation");
			}
			PluginTask.checkIfThreadTaskAborted();
			Integer nbThreads = indexValidation.intValue("@nb-threads", 1);
			String action =  indexValidation.stringValue("@action", "recover");
			someIV = indexCheckAndRepair (executor(),  assetNameSpace, action, nbThreads, where,  dm);	
		}

		if (filenameValidation) {
			if (debug) {
				System.out.println("   Starting filename validation");
			}
			PluginTask.checkIfThreadTaskAborted();
			someFV = filenameCheck (executor(),  assetNameSpace, where, dm);
		}

		dm.pop();

		return (someI || someC || someZ || someIN || someCV || someIV || someFV
				|| someS);
	}

	private Collection<String> addUsers (String projectID, XmlDoc.Element notificationMeta, String xpath) throws Throwable {
		HashSet<String> emails = new HashSet<String>();        // WIll make unique list
		if (notificationMeta==null) {
			return emails;
		}
		Collection<XmlDoc.Element> ss = notificationMeta.elements(xpath);
		Boolean dropProvision = true;
		if (ss!=null) {
			for (XmlDoc.Element s : ss) {
				String who = s.value();
				String email = s.value("@email");
				SvcProjectSummaryForSchedule.addUsers (executor(), who, email, projectID, dropProvision, emails);
			}
		}
		return emails;
	}

	private Boolean integrityCheck (ServiceExecutor executor, String assetNameSpace, XmlDocMaker w) throws Throwable {	
		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("integrity");
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", assetNameSpace);
		dm.add("action", "status");   // TBD change back to recover when defect 1803 resolved
		XmlDoc.Element r = executor.execute("asset.namespace.integrity.check", dm.root());
		int n = 0;
		if (r!=null) {
			n = r.intValue("faults");
			if (n>0) {
				dmOut.addAll(r.elements());
			}
		}
		dmOut.pop();
		if (n>0) {
			w.addAll(dmOut.root().elements());
		}
		return (n>0);
	}



	private Boolean countCheck (ServiceExecutor executor, String assetNameSpace, String assetNameSpaceDR, String where,  XmlDocMaker w) throws Throwable {	
		PluginTask.checkIfThreadTaskAborted();
		XmlDocMaker dmOut = new XmlDocMaker("args");

		// Do we have a DR namespace (the project may not be replicated)
		ServerRoute sr = ProjectReplicate.DRServerRoute(executor);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", assetNameSpaceDR);
		XmlDoc.Element r = executor.execute(sr, "asset.namespace.exists", dm.root());
		if (!r.booleanValue("exists")) return false;

		// Continue as we have a DR namespace
		dmOut.push("count");
		w.add("namespace", assetNameSpace);
		w.add("namespace-dr", assetNameSpaceDR);

		// Count on primary
		dm = new XmlDocMaker("args");
		String query = "namespace>='"+assetNameSpace+"'";
		if (where!=null) {
			query += " and (" + where + ")";
		}
		dm.add("where", query);
		dm.add("size","infinity");
		dm.add("action", "count");
		r = executor.execute("asset.query", dm.root());

		String vp = r.value("value");
		dmOut.add("primary",vp);

		// Count on DR (if exists)
		dm = new XmlDocMaker("args");
		query = "namespace>='"+assetNameSpaceDR+"'";
		if (where!=null) {
			query += " and (" + where + ")";
		}
		dm.add("where", query);
		dm.add("size","infinity");
		dm.add("action", "count");
		//
		r = executor.execute(sr, "asset.query", dm.root());
		String vdr = r.value("value");
		dmOut.add("DR", vdr);
		//
		dmOut.pop();
		if (!(vp.equals(vdr))) {
			w.addAll(dmOut.root().elements());
		}
		return !(vp.equals(vdr)); 
	}




	private Boolean countZeroSize (ServiceExecutor executor, Collection<String> projectTeamEmails, String subjectForProjectTeam, 
			String assetNameSpace, String assetNameSpaceDR, String where, XmlDocMaker w) throws Throwable {	

		XmlDocMaker dmOut = new XmlDocMaker("args");		
		XmlDocMaker dmProjectTeamOut = new XmlDocMaker("args");		
		dmOut.push("count-zero-size");
		dmProjectTeamOut.push("count-zero");

		// FInd paths on primary
		XmlDocMaker dm = new XmlDocMaker("args");
		String query = "namespace>='"+assetNameSpace+"' and asset has content and csize=0";
		if (where!=null) {
			query += " and (" + where + ")";
		}
		dm.add("where", query);
		dm.add("action", "get-path");
		dm.add("size", "infinity");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		Integer nzP = 0;
		Boolean notifyProjectTeam = projectTeamEmails!=null && projectTeamEmails.size()>0;
		if (r!=null) {
			Collection<XmlDoc.Element> paths = r.elements("path");
			if (paths!=null) {
				nzP = paths.size();
			}
			dmOut.push("primary");
			if (nzP>0) {
				if (!notifyProjectTeam) {
					for (XmlDoc.Element path : paths) {
						dmOut.add(path);
					}
				} else {
					// If we are notifying the project team, then we ops people don't
					// need to see all the full paths (which just clog up the daily email)
					dmOut.add("paths", "removed-for-brevity");
					for (XmlDoc.Element path : paths) {
						dmProjectTeamOut.add(path);
					}

				}
				dmOut.add("count", nzP);
				dmProjectTeamOut.add("count", nzP);
			}
			dmOut.pop();

			// Notify project team as well - we only advise them on primary
			if (nzP>0 && notifyProjectTeam) {
				String supportURL = Properties.getApplicationProperty(executor,
						Properties.APPLICATION_PROPERTY_CONTACT_US, Properties.APPLICATION_PROPERTY_APP);
				String msg = "Dear Mediaflux user \n\n" + 
						"Our regular data integrity check has detected that some zero-sized files \n" + 
						"have been uploaded to your project asset namespace '" + assetNameSpace + "'. \n" +
						"Invariably, this indicates the files are zero-sized at the source and \n" +
						"that they have been correctly uploaded to Mediaflux.   Usually these \n" +
						"files are system files, or empty log or other similar files. \n \n" +
						"However, you may like to check that the files listed below are correctly zero-sized. \n" +
						"If you find that they were NOT zero-sized at the source, please contact the Mediaflux \n" +
						"support team via the contact web page : " + supportURL + "\n\n" +
						"regards \n the Mediaflux support team\n\n";
				msg += XmlPrintStream.outputToString(dmProjectTeamOut.root());
				for (String projectTeamEmail : projectTeamEmails) {

					XmlDocMaker dm2 = new XmlDocMaker("args");
					dm2.add("subject", subjectForProjectTeam);
					dm2.add("to", projectTeamEmail);
					dm2.add("body", msg);
					executor.execute("mail.send", dm2.root());
				}

				// Add a note in what we, the ops team see, that the user has been notified
				for (String projectTeamEmail : projectTeamEmails) {
					dmOut.add("notified", projectTeamEmail);	
				}
			}
		}
		//
		dmOut.pop();
		if (nzP!=0) {
			w.addAll(dmOut.root().elements());
			return true;
		}
		//
		return false;
	}

	private Boolean countIncomplete (ServiceExecutor executor, String assetNameSpace, String where, 
			Boolean listPath, String protocol, XmlDocMaker w) throws Throwable {	

		XmlDocMaker dmOut = new XmlDocMaker("args");		
		dmOut.push("count-incomplete");

		// Count on primary
		XmlDocMaker dm = new XmlDocMaker("args");
		String query = "namespace>='"+assetNameSpace+"' and asset hasno content and mf-revision-history hasno value";
		if (where!=null) {
			query += " and (" + where + ")";
		} 
		dm.add("where", query);
		dm.add("action", "get-meta");
		dm.add("size", "infinity");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		if (r==null) return false;

		Integer nzP = 0;
		Collection<XmlDoc.Element> assets = r.elements("asset");
		if (assets!=null) {
			for (XmlDoc.Element asset : assets) {
				String path = asset.value("path");
				// TBD: when this is available via :action get-value
				// replace the :get-meta with a :get-value
				String createdVia = asset.value("created-via");
				if (protocol!=null) {
					if (createdVia!=null && createdVia.equals(protocol)) {
						if (listPath) {
							dmOut.add("path", path);
						}
						nzP++;
					} else {
						// If we don't know the protocol we can't test.  We will do nothing in this case.
					}
				} else {
					if (listPath) {
						dmOut.add("path", path);
					}
					nzP++;			
				}
			}
		}
		dmOut.add("count", nzP);;
		dmOut.pop();

		if (nzP>0) {
			w.addAll(dmOut.root().elements());
			return true;
		}
		return false;
	}



	private Boolean stagingCheck  (ServiceExecutor executor, String assetNameSpace, String where, 
			Boolean forceEnd, Long delay, XmlDocMaker w) throws Throwable {	


		XmlDocMaker dmOut = new XmlDocMaker("args");		
		dmOut.push("staging");

		// Find assets in staging for which the quiescent time is long
		// I can't query directly for the quiescent time
		XmlDocMaker dm = new XmlDocMaker("args");
		String query = "namespace>='"+assetNameSpace+"' and content is staging";
		if (where!=null) {
			query += " and (" + where + ")";
		} 
		dm.add("where", query);
		dm.add("action", "get-meta");
		dm.add("size", "infinity");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		if (r==null) return false;

		// Iterate through
		Collection<XmlDoc.Element> assets = r.elements("asset");
		if (assets==null) {
			dm.pop();
			return false;
		}

		Integer nS = 0;
		for (XmlDoc.Element asset : assets) {
			String id = asset.value("@id");
			String  quiescent = asset.value("content/staging/quiescent/@millisecs");   // millisec
			if (quiescent!=null) {
				Long t = Long.parseLong(quiescent);
				if (t!=null && t>delay) {
					nS++;
					if (forceEnd) {
						dm = new XmlDocMaker("args");
						dm.add("id", id);
						dm.add("commit", new String[] {"delay", "0"}, true);
						executor.execute("asset.content.staging.end", dm.root());	
						dmOut.add("id", new String[] {"quiescent-msec", quiescent, "delay", ""+delay, "force-commit", "true"}, id);
					} else {
						dmOut.add("id", new String[] {"quiescent-msec", quiescent, "delay", ""+delay, "force-commit", "false"}, id);
					}
				}
			}
		}

		dmOut.add("count", nS);

		dmOut.pop();

		if (nS>0) {
			w.addAll(dmOut.root().elements());
			return true;
		}
		return false;
	}


	private Boolean checkSumValidationCheck (ServiceExecutor executor, String assetNameSpace, String where, 
			Integer nbThreads, Boolean checkAllVersions, XmlDocMaker w) throws Throwable {	

		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("content-validation");
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		// New symlinks have no content
		// Old symlinks have content
		String query = "namespace>='"+assetNameSpace+"' and asset has content and csize>0 and not(type='posix/symlink')";
		if (where!=null) {
			query += " and (" + where + ")";
		} 
		dm.add("where", query);
		dm.add("include-destroyed", "true");
		dm.add("nb-threads", nbThreads);
		dm.add("all-versions", checkAllVersions);
		XmlDoc.Element r = executor.execute("unimelb.asset.content.validate", dm.root());
		int nMissing = r.intValue("missing");
		int nMisMatch = r.intValue("mismatch");
		boolean some = false;
		// When multiple asset versions are checked, nProc reflects that, whereas
		// nTotal is the total number of assets.
		if (nMissing>0 || nMisMatch>0) {
			dmOut.addAll(r.elements());			
			dmOut.pop();
			some = true;
		}
		//
		dmOut.pop();
		if (some) {
			w.addAll(dmOut.root().elements());
		}
		return some;
	}


	private Boolean indexCheckAndRepair (ServiceExecutor executor, 
			String assetNameSpace, String action, Integer nbThreads, String where, 
			XmlDocMaker w) throws Throwable {	

		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("index-validation");
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		String query = "namespace>='"+assetNameSpace+"'";
		if (where!=null) {
			query += " and (" + where + ")";
		}
		dm.add("where", query);
		dm.add("include-destroyed", "true");
		dm.add("action", action);
		dm.add("use-indexes", true);         // Really slow if we turn off indexes
		dm.add("nb-threads", nbThreads);
		XmlDoc.Element r = executor.execute("unimelb.asset.index.validate.multi", dm.root());
		int nErrors = r.intValue("errors");	
		Boolean some = false;
		if (nErrors>0) {
			dmOut.add("errors", nErrors);
			some = true;
		}
		//
		dmOut.pop();
		if (some) {
			w.addAll(dmOut.root().elements());
		}

		return some;
	}

	private Boolean filenameCheck (ServiceExecutor executor, String assetNameSpace, String where, XmlDocMaker w) throws Throwable {

		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("filename-validation");
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		String query = "namespace>='"+assetNameSpace+"'";
		if (where!=null) {
			query += " and (" + where + ")";
		}
		dm.add("where", query);
		XmlDoc.Element r = executor.execute("unimelb.asset.name.validate", dm.root());

		// count -total N1 N2
		// total is the total number of assets assessed
		// count is the number with invalid filenames
		long nInvalid = r.longValue("count",0);
		Boolean some = false;
		if (nInvalid>0) {
			dmOut.addAll(r.elements());
			dmOut.pop();
			some = true;
		}
		//
		dmOut.pop();
		if (some) {
			w.addAll(dmOut.root().elements());
		}

		return some;
	}
}
