/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixMapDescribe extends PluginService {


	private Interface _defn;

	public SvcProjectPosixMapDescribe()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT, 
				"The ID of the project (of the form 'proj-<name>-<cid>' or just M - that is, you can enter just the child part of the CID of the project).", 1, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Describes the posix identity map associated with the project.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.map.describe";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {


		String projectID = args.value("project-id");
		String t[] = Project.validateProject(executor(), projectID, true);
		XmlDoc.Element r = Posix.describePosixIdentityMap(executor(), t[1]);
		w.add(r.element("map"));
	}

}
