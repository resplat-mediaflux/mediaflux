/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.contentcopy;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetProcessingQueue;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectContentCopy;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectContentCopyQueueRebuild extends PluginService {


	private Interface _defn;

	public SvcProjectContentCopyQueueRebuild()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("reset-only", BooleanType.DEFAULT, "Only reset (default false) the queue on all projects (remove and resets the :enqueue element on the namespace) currently being content copied. Does not destroy and rebuild the queue.   Resetting only is much less risky than rebuilding.  When we fully rebuild, we make a list of projects having content copied, unset the queue from all, remake the queue (from application properties) and reset on all projects in the list.  So all projects have the queue unset and if something fails after that, content-copy making state is lost.  With reset only (useful to change the :enqueue parameters on the namespace), we just iterate through the projects, unset and reset.", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "This service finds all projects using the standard asset processing queue for making content copies.  It then unsets the queue from those projects and suspends and destroys the queue.  Then it rebuilds the queue using the current value of the application properties content-copy.queue.nb-processors and content-copy.queue.store.name " + 
				"and finally sets the new queue on those projects.  Any assets that were active entries at the instant just before the queue is destroyed  will be requeued. Any candidate assets created during the transition will not get content-copies created and must be handled manually.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.content-copy.queue.rebuild";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Check the content copy  environment is configured		
		if (!ProjectContentCopy.isConfigured(executor())) {
			throw new Exception ("Content copies are not configured on this system");
		}

		// Rebuild
		Boolean resetOnly = args.booleanValue("reset-only", false);
		if (resetOnly) {
			reset (executor(), w);
		} else {
			rebuild (executor(), w);
		}
	}	


	private  void reset (ServiceExecutor executor, XmlWriter w) throws Throwable {

		// List  projects
		Collection<String> projectIDs = Project.projectIDs(executor, true, true);

		// Iterate over projects
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor, projectID, false);

			// See if this project  is content copied via the asset processing queue
			String q = ProjectContentCopy.isProjectContentCopied(ph);
			if (q!=null) {
				w.push("project");
				w.add("id", ph.id());
				ProjectContentCopy.unsetStandardQueueOnProject(executor, projectID, w);
				ProjectContentCopy.setStandardQueueOnProject(executor, ph.id(), w);
				w.pop();
			}
		}
	}

	private  void rebuild (ServiceExecutor executor, XmlWriter w) throws Throwable {

		// List  projects
		Collection<String> projectIDs = Project.projectIDs(executor, true, true);

		// First remove the asset processing queue from the projects
		Vector<String> queuedProjects = new Vector<String>();
		w.push("unset");
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor, projectID, false);

			// See if this projects is being content copied via the asset processing queue
			String q = ProjectContentCopy.isProjectContentCopied(ph);
			if (q!=null) {
				ProjectContentCopy.unsetStandardQueueOnProject(executor, projectID, w);
				queuedProjects.add(projectID);
			}
		}
		w.pop();

		// Now suspend the queue
		String qName = ProjectContentCopy.createOrFindStandardQueue(executor, null);
		AssetProcessingQueue.suspendQueue(executor, qName);

		// Describe existing active entries as we will requeue them
		XmlDoc.Element d = AssetProcessingQueue.describeQueue(executor, qName, true);
		Collection<String> entries = d.values("entry/id");

		// Now destroy  the asset processing queue
		AssetProcessingQueue.destroyQueue(executor, qName, false);
		w.push("destroy");
		w.add("queue", new String[] {"destroyed", "true"}, qName);
		w.pop();

		// Now rebuild picking up application properties
		// content-copy.queue.nb-processors
		// content-copy.queue.store.name
		// and reset on the projects
		w.push("recreate");
		qName = ProjectContentCopy.createOrFindStandardQueue(executor, w);
		if (entries!=null && entries.size()>0) {
			w.push("entries");
			for (String entry : entries) {
				w.add("id", entry);
				AssetProcessingQueue.addAssetToQueue (executor, qName, entry);
			}
			w.pop();
		}
		w.pop();
		//
		w.push("rebuild-projects");
		if (queuedProjects.size()>0) {
			for (String queuedProject : queuedProjects) {
				ProjectContentCopy.setStandardQueueOnProject(executor, queuedProject, w);
			}	
		}
		w.pop();
	}
}
