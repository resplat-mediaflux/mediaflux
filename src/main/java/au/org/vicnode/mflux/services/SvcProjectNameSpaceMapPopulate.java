/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DictionaryUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectNameSpaceMapPopulate extends PluginService {




	private Interface _defn;

	public SvcProjectNameSpaceMapPopulate()  throws Throwable {
		_defn = new Interface();
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Populates missing entries in the (pre-existing) dictionary mapping project ID to asset namespace.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.namespace.map.populate";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Find namespace roots
		Collection<String> nameSpaceRoots = Project.assetNameSpaceRoots(executor());

		// Get dictionary name
		String dict = Properties.getProjectNamespaceMapDictionaryName(executor());


		// Iterate
		for (String nameSpaceRoot : nameSpaceRoots) {
			w.push("root");
			w.add("namespace-root", nameSpaceRoot);

			// List relative child namespaces 
			Collection<String> childNameSpaces = NameSpaceUtil.listNameSpaces(executor(), nameSpaceRoot, false);
			if (childNameSpaces!=null) {
				// Iterate through namespaces
				for (String childNameSpace : childNameSpaces) {
					if (!Project.isProjectNameSpaceRoot(executor(), childNameSpace)) {
						
						try {
							// Exception if name not validated
							Project.validateProjectName(executor(), childNameSpace);

							// Add to dictionary (ignores if exists)
							// term is the project and description is  the absolute namespace
							String assetNameSpace = nameSpaceRoot + "/" + childNameSpace;
							DictionaryUtil.addDictionaryTerm(executor(), dict, childNameSpace, assetNameSpace);
							w.add("namespace", new String[]{"status", "added"}, childNameSpace);
						} catch (Throwable t) {
							w.add("namespace", new String[]{"status", "invalid", "error", t.getMessage()}, childNameSpace);
						}
					} else {
						w.add("namespace", new String[]{"status", "is-root"}, childNameSpace);
					}
				}
			}
			w.pop();
		}
	}
}

