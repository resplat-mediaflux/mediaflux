/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;



public class SvcProjectHistogram extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectHistogram()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
		_defn.add(new Interface.Element("contains", StringType.DEFAULT, "A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",	 0, 1));
		_defn.add(new Interface.Element("block-size", LongType.DEFAULT, "Block-size (bytes) to compute wasted space.  If not supplied, this is not computed.", 0, 1));
		_defn.add(new Interface.Element("sort", BooleanType.DEFAULT, "Sort output from largest space waster (if block-size supplied) to smallest (default true).", 0, 1));
		_defn.add(new Interface.Element("size", IntegerType.DEFAULT, "Number of projects to iterate over (default all).", 0, 1));
		_defn.add(new Interface.Element("idx", IntegerType.DEFAULT, "Start project (0-rel) to begin iterations from (default first).", 0, 1));
		SvcProjectDescribe.addProdType(_defn);
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns histograms of the asset sizes in the given projects.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.histogram";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		String projType = args.stringValue("prod-type",  Properties.PROJECT_TYPE_ALL);
		String keyString = args.value("contains");
		if (keyString!=null) {
			keyString = keyString.toUpperCase();
		}
		String blockSize = args.value("block-size");
		Boolean sort = args.booleanValue("sort",  true);
		Integer size = args.intValue("size", -1);
		Integer idx = args.intValue("idx",0);

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) return;


		// Iterate and compute
		Long totalWasted = 0L;
		XmlDocMaker dm2 = new XmlDocMaker("args");
		int pStart = idx;
		if (size==-1) {
			size = projectIDs.size();
		}
		int pEnd = pStart + size - 1;
		int pIdx = 0;
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();


			if (pIdx >= pStart && pIdx <= pEnd) {
				ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
				if (Project.keepOnProjectType(ph, projType) &&
						Project.keepOnKeyString(ph.id(), keyString)) {
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add("where", "namespace>='"+ph.nameSpace()+"'");
					if (blockSize!=null) {
						dm.add("block-size", blockSize);
					}
					XmlDoc.Element r = executor().execute("asset.content.size.distribution", dm.root());
					if( r != null && r.elements() != null ) {
						if (r.elementExists("wasted-space")) {
							Long ts = r.longValue("wasted-space");
							totalWasted += ts;
						}

						dm2.push("project");
						dm2.add("project-id", ph.id());
						dm2.add("namespace", ph.nameSpace());
						dm2.addAll(r.elements());
						dm2.pop();

					}
				}
			}
			pIdx++;
		}
		Collection<XmlDoc.Element> list = dm2.root().elements();
		if (list!=null) {

			if (blockSize!=null && sort) {
				List<XmlDoc.Element> sortedList = sortBySize (dm2.root().elements());
				w.addAll(sortedList);
			} else {
				w.addAll(dm2.root().elements());
			}

			w.add("total-wasted", new String[] {"units", "bytes"}, totalWasted);
		}
	}


	private static ArrayList<XmlDoc.Element> sortBySize (List<XmlDoc.Element> projectResults) throws Throwable {
		if (projectResults == null || projectResults.isEmpty()) {
			return null;
		}
		ArrayList<XmlDoc.Element> list = new ArrayList<XmlDoc.Element>(projectResults);
		Collections.sort(list, new Comparator<XmlDoc.Element>() {

			@Override
			public int compare(XmlDoc.Element d1, XmlDoc.Element d2)  {
				Long w1 = null;
				Long w2 = null;
				try {
					XmlDoc.Element e1 = d1.element("wasted-space");
					XmlDoc.Element e2 = d2.element("wasted-space");
					if (e1!=null) {
						// If block size is not specified, this element will be null
						w1 = e1.longValue();
					}
					if (e2!=null) {
						// If block size is not specified, this element will be null
						w2 = e2.longValue();
					}
				} catch (Throwable e) {
					//
				}

				if (w1==null && w2==null) {
					return 0;
				} else if (w1==null && w2!=null) {
					return -1;        // Treat as w1 < w2
				} else if (w1!=null && w2==null) {
					return 1;         // Treat as w1 > w2
				} else {
					//System.out.println("w1,w2="+w1+","+w2);
					int retval = Long.compare(w1, w2);
					if(retval > 0) {
						return -1;
					} else if(retval < 0) {
						return 1;
					} else {
						return 0;
					}
				}

			}
		});
		return list;
	}
}
