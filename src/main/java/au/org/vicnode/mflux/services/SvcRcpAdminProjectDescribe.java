/**
 * @author Raj
 *
 * Copyright (c) 2025, The University of Melbourne, Australia
 *
 * All rights reserved.a
 */
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SvcRcpAdminProjectDescribe extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "rcpadmin.project.describe";

    private Interface _defn;

    public SvcRcpAdminProjectDescribe() throws Throwable {
        _defn = new Interface();
        _defn.add(new Interface.Element("contains", StringType.DEFAULT,
                "A string to search for in the project identifiers to restrict which projects are selected. Will be ignored if project-id is specified", 0, 1));
        SvcProjectDescribe.addProjectID(_defn,false);
    }

    public Access access() {
        return ACCESS_ACCESS;
    }

    public Interface definition() {
        return _defn;
    }

    public String description() {
        return "Describe projects that the calling user is an rcsadmin of";
    }

    public String name() {
        return SERVICE_NAME;
    }


    public static Collection<String> checkUserIsProjectAdminRcp(String contains, String projectId, ServiceExecutor executor) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("role", Project.ProjectRoleType.RCPUSR.fromEnum());
        XmlDoc.Element projectElement = executor.execute(Properties.SERVICE_ROOT + "user.self.project.list", dm.root());
        List<XmlDoc.Element> projects = projectElement.elements("project/project");
        Collection<String> projectIds = new ArrayList<String>();

        if (projects != null) {
            //w.add("roles",userRoles.toString());
            for (XmlDoc.Element project : projects) {
                if (projectId != null) {
                    if (project.value().equals(projectId)) {
                        projectIds.add(projectId);
                        return projectIds;
                    }
                } else {
                    if (project.value().contains(contains)) {
                        projectIds.add(project.value());
                    }
                }
            }
        }
        return projectIds;
    }

    public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
        String contains = args.value("contains");
        String projectId = args.value("project-id");

        SvcProjectDescribe describe = new SvcProjectDescribe();
        describe.setServiceExecutor(executor());

        Collection<String> projectIds = checkUserIsProjectAdminRcp(contains,projectId,executor());
        for (String project: projectIds){
            XmlDoc.Element newelement = new Element("args");
            XmlDoc.Element element = new Element("project-id",project);
            newelement.add(element);
            describe.execute(newelement,inputs,outputs,w);
        }
    }
}
