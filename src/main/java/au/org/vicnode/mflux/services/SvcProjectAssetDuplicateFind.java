/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectAssetDuplicateFind extends PluginService {

	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectAssetDuplicateFind() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectIDs(_defn, allowPartialCID);
		//
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",
				0, 1));
		_defn.add(new Interface.Element("parent", StringType.DEFAULT,
				"The parent namespace to consider when looking for projects. Should start with a '/'. Defaults to all parents.",
				0, 1));
		_defn.add(new Interface.Element("show", BooleanType.DEFAULT, "Show the actual duplicates (default false) as well as the summary.",
				0, 1));
		_defn.add(new Interface.Element("size", IntegerType.DEFAULT, "The number of projects to list (after sorting). Defaults to all.",	 0, 1));
		//
		SvcProjectDescribe.addProdType(_defn);
		SvcProjectDescribe.addRepType(_defn);
		SvcProjectDescribe.addOwner(_defn);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns descriptions of duplicate assets for the selected projects. Only lists projects that have duplicates and the results are sorted in size.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.asset.duplicate.find";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String repType = args.stringValue("rep-type", Properties.repTypes[0]);
		String prodType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		String keyString = args.value("contains");
		if (keyString != null) {
			keyString = keyString.toUpperCase();
		}
		String parent = args.value("parent");
		Boolean show = args.booleanValue("show",false);
		int size = args.intValue("size", -1);
		String ownerEmail = args.value("owner-email");

		// List of projects				
		Collection<String> theProjectIDs = args.values("project-id");
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs!=null) {
			for (String theProjectID : theProjectIDs) {
				String [] t = Project.validateProject(executor(), theProjectID, allowPartialCID);
				projectIDs.add(t[1]);
			}
		} else{
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) {
			return;
		}

		
		// Iterate  over projects and summarise
		ArrayList<XmlDoc.Element> projectResults = new ArrayList<XmlDoc.Element>();
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);

			// Is project replicated?
			String qName = ProjectReplicate.isProjectReplicated(ph);

			// Filter projects
			if (Project.keepOnRepType (executor(), qName, repType) && 
				Project.keepOnProjectType(ph, prodType) && 
				Project.keepOnKeyString(ph.id(), keyString) &&
				Project.keepOnParent(ph, parent) &&
				Project.keepOnOwner(ph, ownerEmail)) {
				XmlDoc.Element meta = ph.metaData();
				XmlDocMaker dm2 = new XmlDocMaker("args");
				if (findDuplicates (executor(), ph.nameSpace(), show, dm2)) {
					XmlDocMaker dm3 = new XmlDocMaker("args");
					dm3.push("project");
					dm3.add("project-id", new String[] {"namespace", ph.nameSpace()}, ph.id());
					dm3.add(meta.element("namespace/quota"));
					dm3.addAll(dm2.root().elements());			
					dm3.pop();
					projectResults.add(dm3.root());
				}
				PluginTask.checkIfThreadTaskAborted();
			}
		}

		// Sort and report
		if (size==-1) {
			size = projectResults.size();
		}
		if (projectResults.size()>0) {
			ArrayList<XmlDoc.Element> projectsSorted = sortBySize(projectResults);
			int i = 1;
			for (XmlDoc.Element project : projectsSorted) {
				w.addAll(project.elements());
				i++;
				if (i>size) {
					break;
				}
			}
		}
	}

	private static ArrayList<XmlDoc.Element> sortBySize (ArrayList<XmlDoc.Element> projectResults) throws Throwable {
		if (projectResults == null || projectResults.isEmpty()) {
			return null;
		}
		ArrayList<XmlDoc.Element> list = new ArrayList<XmlDoc.Element>(projectResults);
		Collections.sort(list, new Comparator<XmlDoc.Element>() {

			@Override
			public int compare(XmlDoc.Element d1, XmlDoc.Element d2)   {

				Double d1d = null;
				try {
					// bytes
					String s = d1.value("project/total-duplicated-size");
					d1d = Double.parseDouble(s);
				} catch (Throwable e) {
				}

				Double d2d = null;
				try {
					// bytes
					String s = d2.value("project/total-duplicated-size");
					d2d = Double.parseDouble(s);
				} catch (Throwable e) {
				}
				// This will never happen but we have to handle the exception
				if (d1d==null || d2d==null) {
					return 0;
				}
				int retval = Double.compare(d1d, d2d);

				if(retval > 0) {
					return -1;
				} else if(retval < 0) {
					return 1;
				} else {
					return 0;
				}

			}
		});
		return list;
	}



	private static Boolean findDuplicates (ServiceExecutor executor, String namespace,
			Boolean show, XmlDocMaker w) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		// Restrict candidates and what it is compariong with
		// to the same where
		dm.add("where", "namespace>='"+namespace+"'");
		dm.add("duplicate-where", "namespace>='"+namespace+"'");
		XmlDoc.Element r = executor.execute("asset.duplicate.find", dm.root());

		// These elements are always returned
		Integer n = r.intValue("total-number-of-assets");
		if (n.equals(0)) {
			return false;
		}
		if (show) {
			w.addAll(r.elements("duplicates"));
		}
		w.add(r.element("total-number-of-assets"));
		w.add(r.element("total-number-of-duplicates"));
		w.add(r.element("total-duplicated-size"));
		return true;
	}

}
