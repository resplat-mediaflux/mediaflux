/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;
import au.org.vicnode.mflux.plugin.util.Util.StorageUnit;



public class SvcProjectIngestRateSimple extends PluginService {



	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectIngestRateSimple()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
		_defn.add(new Interface.Element("sort", BooleanType.DEFAULT, "Sort projects by total size ingested (true).", 0, 1));
		_defn.add(new Interface.Element("show-users", BooleanType.DEFAULT, "Include all the users per project in the output (expensive in time).", 0, 1));
		_defn.add(new Interface.Element("contains", StringType.DEFAULT, "A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",	 0, 1));
		_defn.add(new Interface.Element("size", IntegerType.DEFAULT, "The number of projects to list (if sorting, it's the sorted list that is shortened).",	 0, 1));
		_defn.add(new Interface.Element("where", StringType.DEFAULT, "Select time range", 0, 1));
		_defn.add(new Interface.Element("unit", new EnumType(Util.StorageUnit.units()), "Human readable unit.  Defaults to TB.", 0, 1));
		SvcProjectDescribe.addProdType (_defn);
		SvcProjectDescribe.addRepType (_defn);
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns  ingest rates of the given projects.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.ingest.rate.simple";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		Boolean sortBySize = args.booleanValue("sort",  true);
		String prodType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		String repType = args.stringValue("rep-type", Properties.repTypes[0]);
		String keyString = args.value("contains");
		if (keyString!=null) keyString = keyString.toUpperCase();
		int size = args.intValue("size",-1);
		String where = args.value("where");
		String unit = args.stringValue("unit", "TB");
		Boolean showUsers = args.booleanValue("show-users", false);

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;


		// Iterate and summarise
		ArrayList<XmlDoc.Element> projectResults = new ArrayList<XmlDoc.Element>();
		Util.StorageUnit storageUnit = Util.StorageUnit.fromString(args.value("unit"), StorageUnit.toUnit(unit));

		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			// FInd replication asset processing queue
			String qName = ProjectReplicate.isProjectReplicated(ph);

			if (Project.keepOnRepType (executor(), qName, repType) &&
				Project.keepOnProjectType(ph, prodType) &&
				Project.keepOnKeyString(ph.id(), keyString)) {
				XmlDocMaker dm = new XmlDocMaker("args");
				String w2 = "namespace>='"+ph.nameSpace()+"'";
				if (where != null) {
					w2 += " and (" + where + ")";
				}
				dm.add("where", w2);
				dm.add("action", "sum");
				dm.add("xpath", "content/size");
				XmlDoc.Element r = executor().execute("asset.query", dm.root());
				//
				String t = r.value("value");
				String hvalue = null;
				if (t!=null) {
					Long t1 = Long.parseLong(t);
					XmlDoc.Element t2 = storageUnit.convert(t1, "size", null);
					hvalue = t2.value() + " " + unit;
				}
				//
				XmlDocMaker dm2 = new XmlDocMaker("args");
				dm2.push("project");
				dm2.add("project-id", ph.id());
				dm2.add("namespace", ph.nameSpace());
				dm2.add("size", new String[] {"n", r.value("value/@nbe"), "size-human", hvalue}, r.value("value"));

				// Add the admin users in so we have some idea who the project pertains to
				// This is expensive in time
				if (showUsers) {
					XmlDoc.Element users = Project.listUsers(executor(), ph.id(), 
							false, false);
					if (users!=null) {
						Collection<XmlDoc.Element> roles = users.elements("role");
						if (roles!=null && roles.size()>0) {
							dm2.push("users");
							dm2.addAll(roles);
							dm2.pop();
						}
					}
				}
				dm2.pop();
				projectResults.add(dm2.root());
			}
			PluginTask.checkIfThreadTaskAborted();
		}
		//
		if (size==-1) {
			size = projectResults.size();
		}
		if (sortBySize) {
			ArrayList<XmlDoc.Element> projectsSorted = sortByTotalIngest(projectResults);
			int i = 1;
			for (XmlDoc.Element project : projectsSorted) {
				w.addAll(project.elements());
				i++;
				if (i>size) {
					break;
				}
			}
		} else {
			int i = 1;
			for (XmlDoc.Element project : projectResults) {
				w.addAll(project.elements());
				i++;
				if (i>size) {
					break;
				}
			}

		}
	}


	private static ArrayList<XmlDoc.Element> sortByTotalIngest (ArrayList<XmlDoc.Element> projectResults) throws Throwable {
		if (projectResults == null || projectResults.isEmpty()) {
			return null;
		}
		ArrayList<XmlDoc.Element> list = new ArrayList<XmlDoc.Element>(projectResults);
		Collections.sort(list, new Comparator<XmlDoc.Element>() {

			@Override
			public int compare(XmlDoc.Element d1, XmlDoc.Element d2)   {

				Double d1d = null;
				try {
					String s = d1.value("project/size");
					if (s==null) {
						d1d = 0.0d;
					} else {
						d1d = Double.parseDouble(s);
					}
				} catch (Throwable e) {
				}

				Double d2d = null;
				try {
					String s = d2.value("project/size");
					if (s==null) {
						d2d = 0.0d;
					} else {
						d2d = Double.parseDouble(s);
					}
				} catch (Throwable e) {
				}

				// This will never happen but we have to handle the exception
				if (d1d==null || d2d==null) {
					return 0;
				}
				int retval = Double.compare(d1d, d2d);

				if(retval > 0) {
					return -1;
				} else if(retval < 0) {
					return 1;
				} else {
					return 0;
				}

			}
		});
		return list;
	}

}
