/**
 * @author Neil Killeen
 * <p>
 * Copyright (c) 2016, The University of Melbourne, Australia
 * <p>
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.datamover;



import java.io.File;
import java.io.InputStreamReader;
import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.UrlType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetUtil;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;


public class SvcProjectShareableDestinationSetFromXML extends PluginService {


	private Interface _defn = null;

	public SvcProjectShareableDestinationSetFromXML() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("url", UrlType.DEFAULT, "Supply the server-side path for the XML file.  Should be of the form file:<path>", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "This service reads the XML file produced by service ds.project.shareable.describe and updates the target from a collection asset to a namespace (assuming the same path) and 2) re-enables the shareable (which should have been disabled by the ds. service.  The XML file can be supplied as a client-side or server-side file.";
	}

	@Override
	public int minNumberOfInputs() {
		return 0;
	}

	@Override
	public int maxNumberOfInputs() {
		return 1;
	}


	public String name() {
		return Properties.SERVICE_ROOT + "project.shareable.destination.set.from.xml";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String url = args.value("url");
		PluginService.Input input = null;
		if (inputs!=null) {
			input = inputs.input(0);
		}
		if (input!=null&&url!=null) {
			throw new Exception("You must specify only one of :in or :url");
		}
		if (input==null && url==null) {
			throw new Exception("You must specify at least one of :in or :url");
		}
		try {
			// Read
			XmlDoc.Element project = null;
			if (input!=null) {
				project = new XmlDoc().parse(new InputStreamReader(input.stream()));
			} else {
				File f = new File(url.substring(5));
				project = Util.parseXmlFile(f);
			}
			String id = project.value("id");
			String path = project.value("path");
			w.add("project-id", id);
			w.add("project-path", path);
			Collection<XmlDoc.Element> shareables = project.elements("shareables/shareable");
			if (shareables!=null) {		
				for (XmlDoc.Element shareable : shareables) {
					String type = shareable.value("@type");
					String shID = shareable.value("@id");
					String assetID = shareable.value("collection");  // written by ds.project.shareable.describe
					if (assetID!=null) {
						XmlDoc.Element asset = AssetUtil.getAssetMeta(executor(), assetID);
						String shPath = asset.value("path");
						
						// Update the shareable target
						setNamespaceInShareable (executor(), type, shID, shPath);
						enableShareable (executor(), shID);
						w.add("shareable", new String[] {"type", type, "namespace", shPath, "enabled", "true"}, shID);
					} else {
						w.add("shareable", new String[] {"type", type, "collection", "missing", "enabled", "false"}, shID);
					}
				}
			}
		} finally {
			if (input!=null) {
				input.close();
			} 
		}
	}



	static private void setNamespaceInShareable (ServiceExecutor executor, String type, String shareable, String namespace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", shareable);
		dm.add("namespace", namespace);
		if (type.equals("upload")) {
			executor.execute("asset.shareable.upload.destination.set", dm.root());
		} else {
			executor.execute("asset.shareable.download.source.set", dm.root());
		}
	}

	static private void enableShareable (ServiceExecutor executor, String shareable) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", shareable);
		executor.execute("asset.shareable.enable", dm.root());
	}

}
