package au.org.vicnode.mflux.services;

import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectSmartLinkDownloadCreate extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.smartlink.download.create";

    private Interface _defn;

    public SvcProjectSmartLinkDownloadCreate() {
        _defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace - must be a project namespace.", 1, 1));
		_defn.add(new Interface.Element("email", EmailAddressType.DEFAULT, "Email recipient of the shareable.",
				1, 1));
		_defn.add(new Interface.Element("valid-to", DateType.DEFAULT, "The date and time after which the download link is no longer valid.",
				1, 1));		
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Find a project.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String nameSpace = args.value("namespace");
        String email = args.value("email");
        Date date = args.dateValue("valid-to");
        //
        //
        
        // Validate and get the Project ID
        String[] ret = Project.validateProjectNamespace(executor(), nameSpace, true, true);
        String projectID = ret[1];
        
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.push("email");
        dm.add("to", email);
        dm.pop();
        //
        dm.push("download");
        dm.add("namespace", nameSpace);
        dm.push("token");
        dm.add("role", new String[] {"type", "role"}, projectID+":participant-a");
        dm.add("to", date);
        dm.pop();
        dm.add("verbose", true);
        dm.add("overwrite", true);
        dm.pop();
        //
        dm.push("token");
        dm.push("perm");
        dm.add("resource", new String[] {"type", "role:namespace"}, projectID);
        dm.add("access", "ADMINISTER");
        dm.pop();
        dm.pop();
        //
        XmlDoc.Element r = executor().execute("unimelb.asset.download.shell.script.url.create", dm.root());
        w.add(r.element("url"));		    
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
