/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;

public class SvcUserProjectHasWritable extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "user.project.has.writable";

    private Interface _defn;

    public SvcUserProjectHasWritable() throws Throwable {
        _defn = new Interface();
        _defn.add(new Interface.Element("authority", StringType.DEFAULT, "The authentication authority for the user.",
                0, 1));
        _defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain for the user.", 1, 1));
        _defn.add(new Interface.Element("user", StringType.DEFAULT, "The user name.", 1, 1));
    }

    public Access access() {
        return ACCESS_ADMINISTER;
    }

    public Interface definition() {
        return _defn;
    }

    public String description() {
        return "Is the specified user a writable member of any projects?  This service is granted access to the dictionary holding the list of projects as calling user may not have access.";
    }

    public String name() {
        return SERVICE_NAME;
    }

    public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
        String authority = args.value("authority");
        String domain = args.value("domain");
        String user = args.value("user");

        // List Projects
        Collection<String> projectIDs = Project.projectIDs(executor(), false, true);
        if (projectIDs == null) {
            w.add("writable-project", false);
            return;
        }

        // Iterate through projects
        for (String projectID : projectIDs) {

            // Get roles
            Vector<String> roles = Project.actualRoleNames(projectID);

            // See if user holds any role from this project (should be just one)
            // Account for possible multiple roles
            for (String role : roles) {
                if (User.hasRole(executor(), authority, domain, user, role)) {
                    if (!Project.isRoleReadOnly(role)) {
                        w.add("writable-project", true);
                        return;
                    }
                }
            }
        }
        w.add("writable-project", false);
    }
}
