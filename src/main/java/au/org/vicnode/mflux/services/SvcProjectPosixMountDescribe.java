/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixMountDescribe extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectPosixMountDescribe()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn,  allowPartialCID);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Describes posix mount points for provisioned projects.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.mount.describe"; 
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;

		// Iterate over projects
		XmlDocMaker dm = new XmlDocMaker("args");
		for (String projectID : projectIDs) {
			dm.push("project");
			dm.add("id", projectID);
			describe (executor(), projectID, dm);
			dm.pop();
		}
		w.add(dm.root(), false);

	}


	public static void describe (ServiceExecutor executor, String projectID, XmlDocMaker w) throws Throwable {
		// Ask the system to see of there is a posix mount point and what its state is
		Boolean isMounted = Posix.isProjectMounted(executor, projectID);
		w.push("posix");
		if (isMounted) {
			XmlDoc.Element mountMeta = Posix.describePosixMountPoint(executor, projectID);
			w.addAll(mountMeta.element("mount").elements());
			XmlDoc.Element identityMeta = Posix.describePosixMountPointIdentity(executor, projectID);
			w.addAll(identityMeta.elements());
		} else {
			w.add("mounted", false);
		}
		w.pop();
	}

}
