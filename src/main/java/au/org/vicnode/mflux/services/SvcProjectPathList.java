/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

/**
 * 
 * @author nebk
 *
 */


public class SvcProjectPathList extends PluginService {

	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectPathList()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "List the project path.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.path.list";
	}

	public boolean canBeAborted() {

		return true;
	}



	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {


		// Args
		String projectID = args.stringValue("project-id");
		
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("project-id", projectID);
		XmlDoc.Element r = executor().execute(Properties.SERVICE_ROOT+"project.describe", dm.root());
		w.add("path", new String[] {"id", r.value("project/id")}, r.value("project/namespace"));
	}



}
