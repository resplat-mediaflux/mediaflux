/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;

public class SvcUserSelfProjectList extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "user.self.project.list";

    private Interface _defn;

    public SvcUserSelfProjectList() throws Throwable {
        _defn = new Interface();
        _defn.add(new Interface.Element("contains", StringType.DEFAULT,
                "A string to search for in the project identifiers to restrict which projects are selected.", 0, 1));
        _defn.add(new Interface.Element("role", new EnumType(Project.ProjectRoleType.projectRoleTypes()),
                "role of the user on the project", 0, 1));

    }

    public Access access() {
        return ACCESS_ACCESS;
    }

    public Interface definition() {
        return _defn;
    }

    public String description() {
        return "List projects that the calling user is a member of.  The service needs access to the dictionary holding the list of projects and must be granted that access (because the calling user may not hgold it).";
    }

    public String name() {
        return SERVICE_NAME;
    }

    public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
        XmlDoc.Element r = User.describeUser(executor());
        // String authority = r.value("user/@authority");
        String authority = null;
        String domain = r.value("user/@domain");
        String user = r.value("user/@user");
        String contains = args.value("contains");
        String role = args.value("role");
        //
        SvcUserProjectList.list(executor(), authority, domain, user, contains,role, w);
    }
}