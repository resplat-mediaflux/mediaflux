/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.atomic.AtomicOperation;
import arc.mf.plugin.atomic.AtomicTransaction;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetProcessingQueue;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectTypeSet extends PluginService {




	private Interface _defn;

	public SvcProjectTypeSet()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT, "The project of interest.", 1, 1));
		_defn.add(new Interface.Element("type",
				new EnumType(new String[]{Properties.PROJECT_TYPE_PROD,Properties.PROJECT_TYPE_TEST,
						Properties.PROJECT_TYPE_STUB}), "Project production type, no default.", 1, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Set the project production type.  If you make the project 'production', it will be added to the standard replication processing queue.  If you make it 'test' or 'closed-stub' it will remove it from the replication asset processing queue.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.type.set";
	}

	public boolean canBeAborted() {
		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}


	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		final String projectID = args.stringValue("project-id");
		final Boolean useQueue = args.booleanValue("use-queue",  false);
		final String type = args.value("type");
		new AtomicTransaction(new AtomicOperation() {

			@Override
			public boolean execute(ServiceExecutor executor) throws Throwable {
				set (executor(), projectID, useQueue, type, w);
				return false;
			}
		}).execute(executor()); 

	}


	private void set (ServiceExecutor executor, String projectID, 
			Boolean useQueue, String prodType, XmlWriter w) throws Throwable {

		// Validate project
		String s[] = Project.validateProject(executor(), projectID, false);
		projectID = s[1];

		// Let's make sure we aren't already the type requested
		String type = Project.projectType(executor, projectID);
		if (prodType.equals(type)) {
			throw new Exception ("This is already a "+ prodType+ " project");
		}	

		// Now set type in the project asset namespace meta-data 
		Project.setType(executor, projectID, prodType);

		// Replications - we no longer auto-create a recipient asset 
		// namespace on the DR.  We just allow the first replication
		// to generate it (and get the store policy from the parent)
		w.push("Replication");
		if (prodType.equals(Properties.PROJECT_TYPE_TEST) ||
			prodType.equals(Properties.PROJECT_TYPE_STUB)) {

			// Remove project from all asset processing queues
			// for test and closed-stub projects
			AssetProcessingQueue.removeAllQueuesFromProject(executor, projectID, w);
		} else {
			// Add project to replication asset processing queue
			ProjectReplicate.setStandardQueueOnProject(executor, projectID, w);
		}
		w.pop();

	}
}
