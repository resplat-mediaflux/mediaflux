/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixMapUIDAdd extends PluginService {


	private Interface _defn;

	public SvcProjectPosixMapUIDAdd()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT, 
				"The ID of the project (of the form 'proj-<name>-<cid>').", 1, 1));
		_defn.add(new Interface.Element("uid", StringType.DEFAULT, "Unix UID.", 1, 1));
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "Authentication domain of user to map the UID to.", 1, 1));
		_defn.add(new Interface.Element("user", StringType.DEFAULT, "User name to map the UID to.", 1, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Add a Unix UID to the posix identity map associated with the project.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.map.uid.add";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {


		String projectID = args.value("project-id");
		String t[] = Project.validateProject(executor(), projectID, false);
		projectID = t[1];
		String uid = args.value("uid");
		String domain = args.value("domain");
		String user = args.value("user");
		//
		Posix.addUIDToPosixIdentityMap (executor(), projectID, uid, domain, user);
		XmlDoc.Element r = Posix.describePosixIdentityMap(executor(), projectID);
		w.add(r.element("map"));
	}

}
