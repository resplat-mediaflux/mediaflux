package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc.Element;

public class DownloadShareable extends Shareable {

	public final String downloadPassword;

	DownloadShareable(Element shareable, String downloadPassword) throws Throwable {
		super(shareable);
		this.downloadPassword = downloadPassword;
	}

	private static XmlDoc.Element create(ServiceExecutor executor, String facilityShareableId, String facilityUploadId,
			String facilityName, Collection<String> facilityNotificationRecipients, String assetNS,
			XmlDoc.Element uploadArgs, XmlDoc.Element shareOptions) throws Throwable {

		Integer durationDays = shareOptions.intOrNullValue("duration");

		// support the old xml layout for download shareable passowrd and notify
		String password = shareOptions.stringValue("download/client/password", shareOptions.value("download/password"));
		boolean notify = shareOptions.booleanValue("download/client/notify",
				shareOptions.booleanValue("download/notify", false));

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", assetNS);

		// Set download shareable name - it will be the top level directory name at
		// destination.
		String downloadShareableName = String.format("%s Data Delivery", facilityName == null ? "Facility" : facilityName);
		dm.add("name", downloadShareableName);

		dm.add("target-dir-name", downloadShareableName);

		if (durationDays != null) {
			dm.add("valid-to", "today+" + durationDays + "day");
		}
		if (password != null) {
			dm.add("password", password);
		}

		if (facilityName != null) {
			dm.add("property", new String[] { "name", PROPERTY_FACILITY_NAME }, facilityName);
		}

		if (facilityNotificationRecipients != null && !facilityNotificationRecipients.isEmpty()) {
			dm.add("property", new String[] { "name", PROPERTY_FACILITY_NOTIFICATION_TO },
					Emails.join(facilityNotificationRecipients));
		}

		if (facilityShareableId != null) {
			dm.add("property", new String[] { "name", PROPERTY_FACILITY_SHAREABLE_ID }, facilityShareableId);
		}

		if (facilityUploadId != null) {
			dm.add("property", new String[] { "name", PROPERTY_FACILITY_UPLOAD_ID }, facilityUploadId);
		}

		String emailSubjectSuffix = uploadArgs == null ? null : uploadArgs.value(Shareable.ARG_EMAIL_SUBJECT_SUFFIX);
		if (emailSubjectSuffix != null) {
			dm.add("property", new String[] { "name", PROPERTY_FACILITY_NOTIFICATION_EMAIL_SUBJECT_SUFFIX },
					emailSubjectSuffix);
		}

		dm.push("completion-service", new String[] { "name", SvcFacilityShareableDownloadComplete.SERVICE_NAME });
		dm.add("notify", notify);
		dm.pop();
		XmlDoc.Element re = executor.execute("asset.shareable.download.create", dm.root());
		String id = re.value("shareable/@id");
		return describe(executor, id);
	}

	static DownloadShareable create(XmlDoc.Element uploadShareable, String uploadId, String uploadNS,
			XmlDoc.Element uploadArgs, XmlDoc.Element shareOptions, ServiceExecutor executor) throws Throwable {
		String uploadShareableId = uploadShareable.value("@id");
		String facilityName = Shareable.getFacilityName(uploadShareable);
		Collection<String> facilityNotificationRecipients = Shareable
				.getFacilityNotificationRecipients(uploadShareable);

		XmlDoc.Element shareable = create(executor, uploadShareableId, uploadId, facilityName,
				facilityNotificationRecipients, uploadNS, uploadArgs, shareOptions);
		return new DownloadShareable(shareable,
				shareOptions.stringValue("download/client/password", shareOptions.value("download/password")));
	}

}
