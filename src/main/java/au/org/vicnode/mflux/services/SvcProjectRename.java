/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.atomic.AtomicOperation;
import arc.mf.plugin.atomic.AtomicTransaction;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectRename extends PluginService {


	private String _oldDRAssetNameSpace = null;

	private Interface _defn;
	private static Boolean allowPartialCID = false;


	public SvcProjectRename()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn,  allowPartialCID);
		_defn.add(new Interface.Element("name", StringType.DEFAULT, "The new name.  Names are of the form proj-<name>-<cid>.  This new name is just the <name> part, you can't change the rest.",
				1, 1));		
		_defn.add(new Interface.Element("doDR", BooleanType.DEFAULT, "Also rename on the DR server, defaults to true.", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Renames projects: asset, document, dictionary and role namespaces are handled. Also updates posix mount point and project-id to namespace map.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.rename";
	}

	public boolean canBeAborted() {
		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}


	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		final String oldProjectID = args.stringValue("project-id");
		final Boolean doDR = args.booleanValue("doDR",  true);
		final String newName = args.value("name");
		new AtomicTransaction(new AtomicOperation() {

			@Override
			public boolean execute(ServiceExecutor executor) throws Throwable {
				renameAll (executor(), oldProjectID, newName, doDR, w);
				return false;
			}
		}).execute(executor()); 
	}



	private  void renameAll(ServiceExecutor executor, String oldProjectID, 
			String newName, Boolean doDR, XmlWriter w) throws Throwable {


		// TBD : lock out users (except self) - maybe revoke and re-grant roles ?

		// Validate
		String s[] = Project.validateProject(executor, oldProjectID, allowPartialCID);
		oldProjectID = s[1];
		

		// Of the form, 'proj-<name>-<cid> the user should just be supplying the  new name
		// So it should not pass this test which is for the full form. If it does
		// the caller has mistakenly supplied the full form
		Boolean ok = true;
		try {
			Project.validateProjectName (executor, newName);
			ok = false;
		} catch (Throwable t) {
			//
		}
		if (!ok) {
			throw new Exception ("You appear to have specified a new name of the form 'proj-<name>-<cid>. You must supply the <name> component only.");
		}

		// Create new name
		String cid = Project.citeableID(oldProjectID);
		String newProjectID = Properties.PROJECT_ROOT_NAME + newName + "-" + cid;
		w.add("projectID", newProjectID);

		// Cache the old DR asset namespace as we are going to need it for the DR...
		_oldDRAssetNameSpace = Project.assetNameSpace(executor, oldProjectID, true);


		// Rename on primary system
		rename(executor, oldProjectID, newProjectID, false, w);


		// DR system
		// We don't have too much to do here. The replications replicate
		//  assets and asset ACLs.  We do not sens document defintions or
		// dictionaries.  Further, namespaces get made at the DR end and
		// hold neither ACLs nor namespace meta-data.  We do have placehohlder
		// roles on the DR system though (to keep the asset ACLs).
		String DRServerUUID = ProjectReplicate.DRServerUUID(executor);
		if (doDR && DRServerUUID!=null) {
			// Rename namespaces
			rename(executor,oldProjectID, newProjectID, true, w);
			
			// The asset processing queue should stick with the namespace
			// after it's renamed.
		}
	}


	public  void rename (ServiceExecutor executor, String oldProjectID, String newProjectID,
			Boolean forDR, XmlWriter w) throws Throwable {

		if (!forDR) {
			w.push("primary");
		} else {
			w.push("DR");
		}
		ServerRoute sr = null;
		if (forDR) {
			sr = ProjectReplicate.DRServerRoute(executor);
		}


		// Rename the asset namespace. 
		String newAssetNameSpace = null;
		if (!forDR) {
			newAssetNameSpace = Project.renameAssetNameSpace(executor, oldProjectID, newProjectID, forDR);
			w.add("asset-namespace", "renamed");
		} else {
			NameSpaceUtil.renameAssetNameSpace(executor, sr, _oldDRAssetNameSpace, newProjectID);	
			w.add("asset-namespace", "renamed");
		}

		// Update the dictionary holding the map of namespaces and project IDs
		if (!forDR) {
			Project.removeProjectFromNameSpaceMap(executor, oldProjectID);
			Project.addProjectToNameSpaceMap(executor, newProjectID, newAssetNameSpace);
			w.add("asset-namespace-map", "updated");

			// Revoke role permissions.  When you rename document, dictionary and role namespaces,
			// any permissions held in roles are not updated.  To handle this situation you have
			// to first revoke the permissions on roles that refer to the old namespace,
			// do the renaming and then grant the new permissions.  On the DR system,
			// roles (maintaining ACLs) are just place-holders containing no permissions
			// so we don't need to do this.
			Project.revokeAllPermissionsOnStandardRoles (sr, executor, oldProjectID);
			w.add("permissions", "revoked");

			// Meta-data Namespace. When you rename a document type namespace, any assets referring to it
			// are updated on rename.  In addition, any meta-data set on the asset namespace itself
			// will be updated. On the DR system, there are no document namespaces. 
			Integer nAssets = 1000;
			NameSpaceUtil.renameDocumentNameSpace(sr, executor, oldProjectID, newProjectID, nAssets);
			updateDocumentNameSpaceDescription (executor, newProjectID, forDR);
			w.add("document-namespace", "renamed");

			// Dictionary namespace.  When you rename a dictionary namespace, document type definitions
			// that refer to it in restrictions are not updated.  Grumble.  On the DR system there
			// are no dictionary or document namespaces
			Project.renameDictionaryNameSpace(sr, executor, oldProjectID, newProjectID, newProjectID);
			w.add("dictionary-namespace", "renamed");
		}

		// Role namespace.  When you rename a role namespace, actors (users, namespace ACLs) referencing 
		// the role will be updated with the new role namespace.
		if (!forDR) {
			NameSpaceUtil.renameRoleNameSpace (null, executor, oldProjectID, newProjectID);
			w.add("role-namespace", "renamed");
		} else {
			
			// The role namespace may not exist on the DR. If it does,
		    // its contained roles are just placeholders with no held permissions.
			String uuid = ProjectReplicate.DRServerUUID(executor);
			if (NameSpaceUtil.roleNameSpaceExists(executor, uuid, oldProjectID)) {
				NameSpaceUtil.renameRoleNameSpace (sr, executor, oldProjectID, newProjectID);
				w.add("role-namespace", "renamed");
			}
		}

		// Now re-grant the ACLs and permissions to the standard project roles.  The users will still
		// hold the roles (now renamed) ok. We don't need to do this on the DR system
		if (!forDR) {
			Project.grantStandardPermissionsAndACLs(executor, newProjectID, true, true);
			w.add("permissions", "regranted");

			// Update posix mount point meta-data on primary
			Posix.renamePosixMountPoint(executor, oldProjectID, newAssetNameSpace, newProjectID);
			w.add("posix-mount", "recreated");
		}

		w.pop();
	}



	private  void updateDocumentNameSpaceDescription (ServiceExecutor executor, String newNamespace, Boolean forDR) throws Throwable {

		// Update the description sitting on the primary namespace document type
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", newNamespace);
		dm.add("description", "Project '" + newNamespace + "' meta-data Namespace");
		ServerRoute sr = null;
		if (forDR) {
			sr = ProjectReplicate.DRServerRoute(executor);
		}
		executor.execute(sr, "asset.doc.namespace.update", dm.root());
	}




	/*
	//These functions are not needed. The asset namespace does update meta-data namespace changes
	private void updateNameSpaces (ServiceExecutor executor, String nameSpace, 
			String oldProjectID, String newProjectID, Boolean forDR) throws Throwable {

		// Replace in parent project namespace
		updateNameSpace (executor, nameSpace, oldProjectID, newProjectID, forDR);

		// replaceInNameSpace
		ServerRoute sr = null;
		if (forDR) {
			sr = ProjectReplicate.DRServerRoute(executor);
		}
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = executor.execute(sr, "asset.namespace.list", dm.root());
		if (r==null) return;
		Collection<String> namespaces = r.values("namespace/namespace");
		if (namespaces==null) return;

		for (String namespace : namespaces) {
			updateNameSpaces(executor, namespace, oldProjectID, newProjectID, forDR);
		}
	}

	private void updateNameSpace (ServiceExecutor executor, String nameSpace, 
			String oldProjectID, String newProjectID, Boolean forDR) throws Throwable {

		ServerRoute sr = null;
		if (forDR) {
			sr = ProjectReplicate.DRServerRoute(executor);
		}
		// Fetch namespace meta-data
		XmlDoc.Element r = NameSpaceUtil.nameSpaceDescribe(sr, executor, nameSpace);
		if (r==null) return;
		XmlDoc.Element meta = r.element("namespace/asset-meta");
		if (meta==null) return;

		// FInd document types
		Collection<XmlDoc.Element> docs = meta.elements();

		// Iterate
		for (XmlDoc.Element doc : docs) {
			String name = doc.qname();
			String mid = doc.value("@id");
			System.out.println("el name = " + name);

			// Look for match in doc type namespace with old Project ID
			String[] parts = name.split(":");
			int n = parts.length;
			if (n==2) {
				String ns = parts[0];
				if (ns.equals(oldProjectID)) {
					name = newProjectID + ":" + parts[1];

					// Remove old
					NameSpaceUtil.removeAssetNameSpaceMetaData(sr, executor, nameSpace, mid);

					// Add new
					NameSpaceUtil.addAssetNameSpaceMetaData(sr, executor, nameSpace, doc);
				} 
			} else {
				throw new Exception ("Unhandled document type convention.");
			}
		}	
	}
	 */
}
