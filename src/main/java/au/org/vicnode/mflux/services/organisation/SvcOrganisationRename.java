package au.org.vicnode.mflux.services.organisation;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcOrganisationRename extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "organisation.rename";

    private Interface _defn;

    public SvcOrganisationRename() {
        _defn = new Interface();
        _defn.add(new Interface.Element("name", StringType.DEFAULT, "The name of the organisation", 1, 1));
        _defn.add(new Interface.Element("new-name", StringType.DEFAULT, "The new name for the organisation", 1, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Rename an organisation";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String name = args.value("name");
        String newName = args.value("new-name");

        if (name.equals(newName)) {
            return;
        }

        String dict = Properties.getResearchOrgDict(executor());

        if (!SvcOrganisationRemove.dictionaryEntryExists(executor(), dict, name)) {
            throw new Exception("Organisation: '" + name + "' does not exist");
        }

        if (SvcOrganisationRemove.dictionaryEntryExists(executor(), dict, newName)) {
            throw new Exception("Organisation: '" + newName + "' already exists");
        }

        PluginTask.checkIfThreadTaskAborted();
        SvcOrganisationAdd.addDictionaryEntry(executor(), dict, newName);

        PluginTask.checkIfThreadTaskAborted();
        updateUsers(executor(), name, newName);

        PluginTask.checkIfThreadTaskAborted();
        SvcOrganisationRemove.removeDictionaryEntry(executor(), dict, name);
    }

    private static void updateUsers(ServiceExecutor executor, String orgName, String newOrgName) throws Throwable {
        String docType = Properties.getUserAccountsDocType(executor);
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "xpath(" + docType + "/organisation/name)='" + orgName + "'");
        dm.add("action", "pipe");
        dm.push("service", new String[] { "name", "asset.set" });
        dm.push("meta");
        dm.push(docType);
        dm.push("organisation");
        dm.add("name", newOrgName);
        dm.pop();
        dm.pop();
        dm.pop();
        dm.pop();
        executor.execute("asset.query", dm.root());
    }

}
