/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.datamover;


import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;
import au.org.vicnode.mflux.services.SvcProjectDescribe;
import au.org.vicnode.mflux.services.shareable.Manifest;

public class SvcProjectDMEndUserExpiryNotification extends PluginService {

	private Interface _defn;
	private Boolean allowPartialCID = true;

	public SvcProjectDMEndUserExpiryNotification()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectIDs(_defn, allowPartialCID);
		_defn.add(new Interface.Element("email", EmailAddressType.DEFAULT, 
				"An additional email address to send a notification to advise the outcome of this service (service arguments and which namespaces were found and who was notified", 0, 1));
		_defn.add(new Interface.Element("send", BooleanType.DEFAULT, 
				"Actually send (default true) the notifications to the end users and facility staff (if notify-facility is true).  If false, do everything (including generating service output) but don't actually send the notifications.", 0, 1));
		_defn.add(new Interface.Element("all", BooleanType.DEFAULT, 
				"When listing results to the terminal, present both expired and unexpired transactions (default is false which presents only unexpired transactions). Regardless of this argument, end users will only be notified for unexpired transactions.", 0, 1));
		_defn.add(new Interface.Element("show-both", BooleanType.DEFAULT, 
				"When listing results to the terminal, present both downloaded and not downloaded transactions (default is false which presents only not yet downloaded transactions).", 0, 1));
		_defn.add(new Interface.Element("contains", StringType.DEFAULT, 
				"A string that the transactional namespace path must contain (a filter). The default is no filter.", 0, 1));
		_defn.add(new Interface.Element("share-with", EmailAddressType.DEFAULT, 
				"Only show results when the share-with email address stored in the manifest asset is this value.",0,1));
		//		
		Interface.Element el = new Interface.Element("notify-facility", BooleanType.DEFAULT, 
				"Also notify facility staff  which end-users were themselves notified (default false). It extracts the notification email out of the relevant manifest asset and/or from project meta-data.", 0, 1);
		el.add(new Interface.Attribute("type", 
				new EnumType(new String[] {"project", "manifest", "both"}),
				"The type of notification recipient. 'project' means the email recipient(s) extracted from the project namespace meta-data element ds-admin:Project\\notification\\data-mover-expiry\\email[type='end-user'], 'manifest' means the facility notification email extracted from the manifest asset, 'both' means both. The default is 'project'.",0));
		_defn.add(el);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Sends a notification (by email) when the __manifest.csv file " +
				"(created by Data Mover instrument uploads) upload recipients have " +
				"apparently not yet accessed their data and it has not yet expired." +
				"The service looks recursively for manifest assets in all child " +
				"namespaces (except those excluded by the meta-data element " +
				"$NS_ADMIN:Project/data-mover-expiry/exclude-child-path) " +
				"to determine if the data have been downloaded and if the " +
				"end-user should be notified.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.DM.enduser.expiry.notification";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Parse inputs
		String email = args.stringValue("email");
		String contains = args.stringValue("contains");
		String shareWith = args.stringValue("share-with");
		Boolean send = args.booleanValue("send", true);
		XmlDoc.Element notifyFacilityEl = args.element("notify-facility");
		Boolean notifyFacility = false;
		String notifyFacilityType = "project";
		if (notifyFacilityEl!=null) {
			notifyFacility = notifyFacilityEl.booleanValue();
			notifyFacilityType = notifyFacilityEl.stringValue("@type","project");
		}
		Boolean showBoth= args.booleanValue("show-both", false);

		Boolean showAll = args.booleanValue("all",false);
		Collection<String> theProjectIDs = args.values("project-id");
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs!=null) {
			for (String theProjectID : theProjectIDs) {
				String [] t = Project.validateProject(executor(), theProjectID, allowPartialCID);
				projectIDs.add(t[1]);
			}
		} else{
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) {
			return;
		}
		//

		// Iterate through projects looking for the ones with
		// the notification meta-data $NS_ADMIN:Project/notification/data-mover-expiry
		XmlDocMaker result = new XmlDocMaker("args");
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			w.push("project");
			w.add("id", projectID);

			// Get namespace meta-data
			XmlDoc.Element nsMeta = ph.metaData();

			// Look for the expiry meta-data (optional) on the project namespace
			XmlDoc.Element meta = Utils.getNotificationMetaData(ph.executor(), nsMeta);

			// This map holds per key (facility email notification) the transactions
			// for which a user was notified.
			HashMap<String,XmlDocMaker> facilityNotifications = new HashMap<String,XmlDocMaker>();

			// Find the configured exclusion project child-paths (if any) 
			// where we don't look for manifest assets.
			Collection<String> excludeChildPaths = null;
			if (meta!=null) {
				excludeChildPaths = meta.values("exclude-child-path");
			}

			// FInd the manifest assets of interest
			Collection<XmlDoc.Element> assets = 
					Utils.findManifestAssets(executor(), null, ph.nameSpace(), excludeChildPaths);

			// Notify the end users and generate information for each 
			// transactional namespace for which end users were  notified
			// that will be used to notify operators (we need to aggregate this
			// information per project across multiple end users)
			Collection<XmlDoc.Element> tr = 
					findAndNotifyUsers (executor(), assets, contains, shareWith,
							showBoth, showAll, send, 
							facilityNotifications, w);
			if (tr!=null && tr.size()>0) {
				result.addAll(tr);
			}

			// Now for this project, notify the facility staff of which 
			// end users were notified. For each facility staff email extracted
			// from the manifest asset, the map stores the details of all 
			// of the transactional namespaces relevant to that email address (only). 
			// We notify, either this notification email address or the notification email
			// address stored in the project namespace meta-data or both
			if (notifyFacility) {
				// The keys are the facility staff email addresses
				Collection<String> keySet = facilityNotifications.keySet();
				if (keySet!=null && !keySet.isEmpty()) {
					for (String key : keySet) {
						XmlDocMaker r = facilityNotifications.get(key);
						String body = XmlPrintStream.outputToString(r.root());
						String uuid = Util.serverUUID(executor());
						String subject = "["+uuid+"] - DataMover instrument upload end user download reminder notifications that were sent (date="+new Date().toString()+")";

						// Add email dug out of manifest asset
						Vector<String> emails = new Vector<String>();
						if (notifyFacilityType.equals("manifest") ||
								notifyFacilityType.equals("both")) {
							// Add the facility email address extracted from the manifest asset
							emails.add(key);
						} 
						if (notifyFacilityType.equals("project") ||
								notifyFacilityType.equals("both")) {							
							// Add the facility email address extracted from the project meta-data
							// If there are any.
							Collection<String> projectNotificationEmails = null;
							if (meta!=null) {
								projectNotificationEmails = meta.values("email[@type='end-user']");
								if (projectNotificationEmails!=null &&
										projectNotificationEmails.size()>0) {
									emails.addAll(projectNotificationEmails);
								}
							}
						}
						// Send the operator emails
						Collection<String> noDups = new HashSet<String>(emails);
						if (send) {
							MailHandler.sendMessage (executor(), noDups, subject, body, null, false, 
									null, null);
						}

						// Similar to Terminal output
						w.push("facility-notification");
						if (noDups!=null && noDups.size()>0) {
							for (String noDup : noDups) {
								w.add("facility-email-notified", new String[] {"send", send.toString()}, noDup);
							}
						}
						w.addAll(r.root().elements());
						w.pop();
					}
				}
			}
			w.pop();
		}

		// Additional notification to e.g. ops team if desired
		if (email!=null) {
			if (result.root().hasSubElements()) {
				result.push("service", new String[] {"name", name()});
				result.add(args);
				result.pop();
				String body = XmlPrintStream.outputToString(result.root());
				String uuid = Util.serverUUID(executor());
				String subject = "["+uuid+"] - DataMover instrument upload end user download reminder notifications (date="+new Date().toString()+")";
				MailHandler.sendMessage(executor(), email, subject, body, null, false, null);
			} 
		}
	}

	/**
	 * For each upload, we want to advise the recipient email users 
	 * (stored in the manifest asset) if they have not yet accessed their 
	 * data and it is not yet expired. We will also advise them 
	 * when it will expire. The end users will be different from upload to upload
	 * 
	 * @param executor
	 * @param assets the manifest assets of interest
	 * @param contains A string that the transactional namespace must contain to
	 *                filter out unwanted paths
	 * @param shareWith AN email address to filter on with the manifest asset must contain
	 *                for the share-with arg.
	 * @param showBoth  If true, show both  downloaded and not downloaded transactions.
	 *                  If false, show not downloaded transactions only
	 * @param showAll   If true, show both expired and unexpired transactions
	 *                  If false, show expired transactions only
	 * @param w
	 * @throws Throwable
	 **/

	// 
	private  Collection<XmlDoc.Element> findAndNotifyUsers (ServiceExecutor executor, 
			Collection<XmlDoc.Element> assets, String contains, String shareWith, Boolean showBoth,
			Boolean showAll, Boolean send, 
			HashMap<String,XmlDocMaker> facilityMap, XmlWriter w) throws Throwable {


		// Handle each upload/manifest asset for this project
		XmlDocMaker result = new XmlDocMaker("args");
		if (assets!=null) {
			for (XmlDoc.Element asset : assets) {

				String assetID = asset.value("@id");

				// Get manifest meta-data
				XmlDoc.Element meta = asset.element("meta/"+Utils.MANIFEST_DOCTYPE);

				// Facility details
				String facilityName = meta.value("shareable/name");
				String facilityNotifyTo = meta.value("shareable/property[@name='facility.notification.to']");

				// Date details. This element was added	after the initial
				// roll out so has to be tested for old shareables
				// such as those for MCFP where they don't destroy data
				XmlDoc.Element  expiry = meta.element("upload/share/duration");
				if (expiry!=null) {
					String transactionalNamespace = asset.value("namespace");
					Date expiryDate = expiry.dateValue("@expiry");	
					Date completionDate = meta.dateValue("upload/completion-time");
					Date today = new Date();
					String thisShareWith = meta.stringValue("upload/arg[@name='share-with']");
					// Now find  the download counters via the asset attributes
					Map<String, Integer> attrs = Manifest.getManifestAssetAttributes(executor, assetID);
					int shareableDownloadConsumed = attrs.getOrDefault(Manifest.ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED, 0);
					int shareableDownloadConsumedSubset = attrs.getOrDefault(Manifest.ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED_SUBSET, 0);
					int userInteractionConsumed = attrs.getOrDefault(Manifest.ATTRIBUTE_USER_INTERACTION_COSUMED, 0);
					int directDownloadAttempt = attrs.getOrDefault(Manifest.ATTRIBUTE_DIRECT_DOWNLOAD_ATTEMPT, 0);
					
					//
					Boolean keepOnContains = true;
					if (contains!=null) {
						if (!transactionalNamespace.contains(contains)) {
							keepOnContains = false;
						}
					}
					//
					Boolean keepOnShare = true;
					if (shareWith!=null) {
						if (thisShareWith!=null && !thisShareWith.contains(shareWith)) {
							keepOnShare = false;
						}					
					}
					// TODO check with Neil
					Boolean downloaded = (shareableDownloadConsumed>0 || userInteractionConsumed>0 || directDownloadAttempt>0);
					Boolean proceed = (showBoth || (!showBoth&&!downloaded));
					if (proceed) {
						if (!keepOnContains || !keepOnShare) {
							proceed = false;
						}
					}
					// Continue if data has not yet expired
					if (proceed && (showAll || (!showAll&&today.before(expiryDate)))) {
						w.push("manifest");
						w.add("namespace", transactionalNamespace);
						w.add("path", asset.value("path"));
						w.add("id", assetID);
						//
						w.push("facility");
						w.add("name", facilityName);
						w.add("notify", facilityNotifyTo);
						w.pop();

						// Extract the date of acquisition from the transactional namespace name
						String transactionalNameSpace = asset.value("namespace");
						Date acquiredDate = Utils.getAcquiredDate(transactionalNameSpace);
						
						// FInd the top-level namespaces under the namespace that
						// the manifest asset is in.  These will be the source
						// data folder names
						Collection<String> dataNameSpaces = NameSpaceUtil.listNameSpaces(executor, transactionalNameSpace, false);

						// Now find  the download counters via the asset attributes
						w.push("counters");
						w.add("data-mover", shareableDownloadConsumed);
						w.add("data-mover-subset", shareableDownloadConsumedSubset);
						w.add("copy", userInteractionConsumed);
						w.add("direct-attempt", directDownloadAttempt);
						w.pop();

						// All we care about is that the users have accessed their data
						// by one of the three methods. TODO: check with Neil
						if (shareableDownloadConsumed>0 || userInteractionConsumed>0 || directDownloadAttempt>0) {
							w.add("has-been-downloaded", true);
							// We don't need to list anything further
						} else {
							w.add("has-been-downloaded", false);
							w.push("dates");
							w.add("acquired", acquiredDate);
							w.add("expiry", expiryDate);
							w.add("completion", completionDate);
							w.add("today", today);
							w.pop();

							// We only actually notify users if the data
							// have not yet expired. 
							if (today.before(expiryDate)) {
								w.add("has-expired", false);

								// Find the email recipients of the upload and notify
								Collection<XmlDoc.Element> args = meta.elements("upload/arg");
								if (args!=null) {
									for (XmlDoc.Element arg : args) {
										String argName = arg.value("@name");
										if (argName.equals("share-with")) {
											String s = arg.value();
											if (s!=null && s.length()>0) {
												String[] emails = s.split(",");
												// Notify the end users
												w.push("notified");
												notifyUsers (executor, transactionalNameSpace, facilityName, acquiredDate, completionDate,
														expiryDate, dataNameSpaces, emails, send, w);
												w.pop();

												// Add notification which will be sent to Mediaflux ops staff.
												// This contains all notifications to all users from the execution
												// of this service
												addNotification (result, transactionalNameSpace, emails, 
														today, acquiredDate, completionDate, expiryDate, send);


												// Build a unique map of facility notification emails for this project.
												// Each manifest asset may have a different facility email to notify
												// So we aggregate, per email address, the relevant end-user notifications
												if (facilityMap.containsKey(facilityNotifyTo)) {
													XmlDocMaker r = facilityMap.get(facilityNotifyTo);
													addNotification (r, transactionalNameSpace, emails, 
															today, acquiredDate, completionDate, expiryDate, send);
													facilityMap.replace(facilityNotifyTo,r);
												} else {
													XmlDocMaker r = new XmlDocMaker("args");
													addNotification (r, transactionalNameSpace, emails, 
															today, acquiredDate, completionDate, expiryDate, send);
													facilityMap.put(facilityNotifyTo,r);
												}
											}
										}
									}
								}
							} else {
								w.add("has-expired", true);
							}
						}
						w.pop();
					}
				}
			}
		}
		// Return information about each transactional namespace
		// for which the end user(s) were notified
		return result.root().elements();
	}


	private void addNotification (XmlDocMaker result, String transactionalNameSpace,
			String[] emails, Date today, Date acquiredDate, Date completionDate, 
			Date expiryDate, Boolean send) throws Throwable {
		result.push("transactional-namespace");
		result.add("namespace", transactionalNameSpace);
		String ts = DateUtil.formatDate(today, true, false);
		for (String email : emails) {
			result.add("acquired", acquiredDate);
			result.add("uploaded", completionDate);
			result.add("expiry", expiryDate);
			result.add("downloaded", false);
			result.add("end-user-email-notified", new String[] {"date-notified", ts, "send", send.toString()}, email);								
		}
		result.pop();
	}


	private  void  notifyUsers (ServiceExecutor executor, String transactionalNameSpace, String name, Date acquiredDate,
			Date completionDate, Date expiryDate, Collection<String> dataNameSpaces,
			String[] emails, Boolean send, XmlWriter w) throws Throwable {
		
		// Find partial path (starting at /proj-<>-<id>
		int idx = transactionalNameSpace.indexOf("/proj-");
		String pTNS = transactionalNameSpace.substring(idx+1);
		
		// The completion date is when the upload completes
		// It's not really when the data were acquired.
		// We don't have that information.
		String cDate = DateUtil.formatDate(completionDate, false, true);
		String eDate = DateUtil.formatDate(expiryDate, false, true);
		String aDate = DateUtil.formatDate(acquiredDate, false, true);

		String msg = "Dear Colleague\n\n"+
				"This is to advise that the data acquired for you " +
				"at the University of Melbourne: \n\n" +
				"   facility name : " + name + "\n" +
				"   date acquired : " + aDate + "\n" + 
				"   date uploaded : " + cDate + "\n" + 
				"   expiry : " + eDate + "\n" +
				// Add the partial transactiuonal parent as it helps ops 
				// staff to find the data if there are issues to
				// resolve. It's not a security issue to include
				// this path.
		        "   transactional parent : " + pTNS + "\n";
		// Add in the top-level data namespaces (below the transactional namespace)
		for (String dataNameSpace : dataNameSpaces) {
			msg += "   instrument source folder : " + dataNameSpace + "\n";
		}

		msg += "\ndoes not yet appear to have been downloaded. \n\n" +
				"Please download as soon as possible since the data "+
				"will be destroyed at the expiry date." +
				"You can access the data via the download links emailed " +
				"to you in the original data dispatch email, or contact " +
				"the instrument platform support staff for assistance.\n\n" +
				"regards, \n" +
				"Data Solutions Team\n" +
				"Research Computing Services | Business Services \n" +
				"The University of Melbourne.";
		String from = Util.getServerProperty(executor, "notification.from");
		String subject = "Data not yet downloaded from " +name;
		for (String email : emails) {
			try {
				if (send) {
					MailHandler.sendMessage(executor, email, subject, msg, null, false, from);
				}
			} catch (Throwable t) {
				// If the email address is invalid (as happens sometimes)
				// we just drop it here.  When the end-2021 DM upgrade
				// is done, there should be none of these because the
				// validation will be improved. So it's not worth trying
				// to do anything about this (one could consider correcting
				// the email address in the manifest asset though).
			}
			w.add("email", new String[] {"send", send.toString()}, email);
		}
	}
}




