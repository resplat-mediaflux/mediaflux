package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.*;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;

import java.util.Collection;

public class SvcUserSelfProjectAdminIs extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "user.self.project.admin.is";

 
    private final Interface _defn;

    public SvcUserSelfProjectAdminIs() {
        _defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace of interest (must be a project namespace or child).", 1, 1));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Determine if the caller is an administrator of the project from which the given namespace is part of.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
    	String namespace= args.value("namespace");
    	
    	// FInd project ID
    	String[] t = Project.validateProjectNamespace(executor(), namespace, 
    			false, true);
    	
    	// Get admins
    	String projectID = t[1];
    	Collection<String> admins = Project.listUsers(executor(),
    			projectID, Project.ProjectRoleType.ADMIN, true);

    	// Find self
    	XmlDoc.Element user = User.describeUser(executor());
    	String actor = user.value("user/@domain") + ":" + user.value("user/@user");
 
    	// See if admin
    	for (String admin : admins) {
    		if (admin.equals(actor)) {
    			w.add("admin", new String[] {"id", projectID}, true);
    			return;
    		}
    	}
    	w.add("admin", new String[] {"id", projectID}, false);;
    }
}
