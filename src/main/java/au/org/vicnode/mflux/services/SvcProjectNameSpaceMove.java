/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DictionaryUtil;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;


public class SvcProjectNameSpaceMove extends PluginService {




	private Interface _defn;

	public SvcProjectNameSpaceMove()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT, "The ID of the project. Should be of the form 'proj-<name>-<cid>'.", 1, 1));
		_defn.add(new Interface.Element("parent", StringType.DEFAULT,
				"The new parent namespace on the primary server.  Must be one of the values returned by service vicnode.project.parent.namespace.list", 1, 1));
		_defn.add(new Interface.Element("doDR", BooleanType.DEFAULT, "Attempt to move the project on  the DR server as well (if configured).", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Moves a project from its existing parent project namespace into another and updates the dictionary holding the map of projectID to namespace.  Will also move on the DR server (new parent must exist and DR configured).  Does the DR first so failure there does not compromnise the primary move. The dictionary is updated last.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.namespace.move";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Parse
		String projectID = args.value("project-id");
		String newPrimaryParent = args.value("parent");
		Boolean doDR = args.booleanValue("doDR", true);

		// This test should be unnecessary (the dictionary feeding the inputs should force it to be correct)
		String s = newPrimaryParent.substring(0,1);
		if (!s.equals("/")) {
			throw new Exception ("The new parent must be a full path starting with '/'");
		}

		// Validate project and get existing parent namespace on primary
		String t[] = Project.validateProject(executor(), projectID, false);
		String oldPrimaryParent = t[0];
		projectID = t[1];
		if (newPrimaryParent.equals(oldPrimaryParent)) {
			throw new Exception ("The old and new parent asset namespaces are the same");
		}

		// Proceed
		w.add("project-id", projectID);

		// We do the DR first, as we may stop if things are wrongly configured.
		// Check the DR environment is configured and move on DR if the project has been replicated	
		if (doDR && ProjectReplicate.isDRConfigured(executor())) {
			w.push("disaster-recovery");
			String oldDRParent = ProjectReplicate.makeNameSpaceNameForDR(executor(), oldPrimaryParent);
			String newDRParent = ProjectReplicate.makeNameSpaceNameForDR(executor(), newPrimaryParent);

			// Receiving parent must exist so that the store policy has been correctly set
			if (!NameSpaceUtil.assetNameSpaceExists(executor(), ProjectReplicate.DRServerUUID(executor()), newDRParent)) {
				throw new Exception ("The parent namespace on the DR server (" + newDRParent + ") must pre-exist (and be configured with the correct store polcy)");
			}
			move (executor(), projectID, newDRParent, true, w);
			w.add("old-parent", oldDRParent);
			w.add("new-parent", newDRParent);
			w.pop();
		}


		// Now move the namespace on the primary system
		w.push("primary");
		move (executor(), projectID, newPrimaryParent, false, w);
		w.add("old-parent", oldPrimaryParent);
		w.add("new-parent", newPrimaryParent);
		w.pop();


		// Now update the mapping dictionary on the primary server witj the project's new namespace
		String mapDictName = Properties.getProjectNamespaceMapDictionaryName(executor());
		DictionaryUtil.removeDictionaryTerm(executor(), mapDictName, projectID);
		String newPrimaryNameSpace = Project.makeProjectPath(newPrimaryParent, projectID);
		DictionaryUtil.addDictionaryTerm(executor(), mapDictName, projectID, newPrimaryNameSpace);
		w.add("namespace", newPrimaryNameSpace);

		// Update the root namespace visibility role.  This role is used to hide namespace roots
		// from users other than those whoh ave a project under that root.
		//  First we revoke the old visibility role (if exists) from the project roles
		String oldVisibilityRole = SvcProjectCreate.findVisibilityRole(executor(), oldPrimaryParent);
		if (oldVisibilityRole!=null) {
			Project.grantRevokeRoleToProjectRoles(executor(), projectID, oldVisibilityRole, false);
		}
		// Now we grant the new visibility role (if exists) to the project roles
		String newVisibilityRole = SvcProjectCreate.findVisibilityRole(executor(), newPrimaryParent);
		if (newVisibilityRole!=null) {
			Project.grantRevokeRoleToProjectRoles(executor(), projectID, newVisibilityRole, true);
		}

	}


	private void move (ServiceExecutor executor, String projectID, String newParent, Boolean forDR, XmlWriter w) throws Throwable {

		// Move the project asset namespace to the new parent namespace
		String oldNameSpace = Project.assetNameSpace(executor, projectID, forDR);
		XmlDocMaker dm = new XmlDocMaker ("args");
		dm.add("namespace", oldNameSpace);
		dm.add("to", newParent);
		ServerRoute serverRoute = null;
		if (forDR) {
			serverRoute = ProjectReplicate.DRServerRoute(executor);
			w.add("uuid", ProjectReplicate.DRServerUUID(executor));
		} else {
			w.add("uuid", Util.serverUUID(executor));
		}
		executor.execute(serverRoute, "asset.namespace.move", dm.root());
	}
}
