package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

import java.nio.file.Paths;
import java.util.Collection;

public class SvcFacilityShareableUploadInteractionComplete extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT
            + "facility.shareable.upload.interaction.complete";

    public static final String ARG_UPLOAD_NS = "upload-ns";

    public static final String ARG_COPY_TO_PROJECT = Interaction.ARG_COPY_TO_PROJECT;

    public static final String ARG_IFEXISTS = "ifexists";

    public static final String ARG_NOTIFY = "notify";

    public static final String FACILITY_SHAREABLE_INTERACTION_LOG = "facility-shareable-interaction";
    private final Interface _defn;

    public SvcFacilityShareableUploadInteractionComplete() {
        _defn = new Interface();
        _defn.add(new Interface.Element("id", LongType.POSITIVE, "Shareable id", 0, 1));
        _defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "Shareable id", 0, 1));
        _defn.add(new Interface.Element("upload-id", LongType.POSITIVE, "Upload id", 0, 1));
        _defn.add(new Interface.Element("interaction-id", LongType.POSITIVE, "User interaction id", 0, 1));
        _defn.add(new Interface.Element("interaction-token", StringType.DEFAULT, "User interaction token", 0, 1));

        /*
         * args
         */
        Interface.Element args = new Interface.Element("args", XmlDocType.DEFAULT, "args", 0, 1);
        args.add(new Interface.Element(ARG_UPLOAD_NS, StringType.DEFAULT,
                "The asset namespace contains the uploaded data to copy from.", 0, 1));
        args.add(new Interface.Element(ARG_COPY_TO_PROJECT, StringType.DEFAULT,
                "The destination path where the uploaded data is copied to.", 0, 1));
        args.add(new Interface.Element(ARG_IFEXISTS, new EnumType(new String[]{"ignore", "rename"}),
                "Action to take if the asset namespace exists at destination. Default to ignore.", 0, 1));
        args.add(new Interface.Element(ARG_NOTIFY, new EnumType(new String[]{"all", "facility", "user", "none"}),
                "Whom to receive notifications when user interaction is completed. Set to facility to send to facility contacts specified in property: "
                        + Shareable.PROPERTY_FACILITY_NOTIFICATION_TO
                        + ". Set to user to send to the end user who invoke the interaction. Defatuls to none.",
                0, 1));
        _defn.add(args);

    }

    static PluginLog log() {
        return PluginLog.log(FACILITY_SHAREABLE_INTERACTION_LOG);
    }

    static void logWarning(String msg) {
        log().add(PluginLog.WARNING, msg);
    }

    static void copyData(ServiceExecutor executor, String srcNamespace, String dstParentNS, String ifexists)
            throws Throwable {

        String nsName = Paths.get(srcNamespace).getFileName().toString();
        String dstNS = Paths.get(dstParentNS, nsName).toString();

        PluginTask.checkIfThreadTaskAborted();
        boolean dstNSExists = assetNamespaceExists(executor, dstNS);
        if (dstNSExists) {
            if ("ignore".equalsIgnoreCase(ifexists)) {
                logWarning("Destination asset namespace: '" + dstNS + "' already exists. Ignored.");
                return;
            } else if ("rename".equalsIgnoreCase(ifexists)) {
                while (dstNSExists) {
                    PluginTask.checkIfThreadTaskAborted();
                    nsName = renameAssetNamespace(nsName);
                    dstNS = Paths.get(dstParentNS, nsName).toString();

                    PluginTask.checkIfThreadTaskAborted();
                    dstNSExists = assetNamespaceExists(executor, dstNS);
                }
            }
            PluginTask.checkIfThreadTaskAborted();
            createAssetNamespace(executor, dstNS);

            // copy sub-namespaces.
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("namespace", srcNamespace);
            Collection<String> subNSNames = executor.execute("asset.namespace.list", dm.root())
                    .values("namespace/namespace");
            if (subNSNames != null) {
                for (String subNSName : subNSNames) {
                    PluginTask.checkIfThreadTaskAborted();
                    copyAssetNamespace(executor, Paths.get(srcNamespace, subNSName).toString(), dstNS);
                }
            }

            // copy assets
            dm = new XmlDocMaker("args");
            dm.add("where", "namespace='" + srcNamespace + "'");
            dm.add("action", "pipe");
            dm.add("stoponerror", "true");
            dm.add("size", "infinity");
            dm.push("service", new String[]{"name", "unimelb.asset.copy0"});
            dm.add("namespace", dstNS);
            dm.add("version", "all");
            dm.pop();
            PluginTask.checkIfThreadTaskAborted();
            executor.execute("asset.query", dm.root());
        } else {
            PluginTask.checkIfThreadTaskAborted();
            copyAssetNamespace(executor, srcNamespace, dstParentNS);
        }
    }

    private static void copyAssetNamespace(ServiceExecutor executor, String srcNS, String dstParentNS)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", srcNS);
        dm.add("to", dstParentNS);
        dm.add("pipe-nb-threads", 1);
        logWarning("copying asset namespace: '" + srcNS + "' to '" + dstParentNS + "'");
        executor.execute("unimelb.asset.namespace.copy0", dm.root());
    }

    private static String renameAssetNamespace(String nsName) {
        StringBuilder sb = new StringBuilder();
        int n = 0;
        if (nsName.matches("^.*\\(\\d+\\)$")) {
            int idx = nsName.lastIndexOf('(');
            n = Integer.parseInt(nsName.substring(idx + 1, nsName.length() - 1));
            sb.append(nsName, 0, idx);
        } else {
            sb.append(nsName);
        }
        sb.append('(').append(n + 1).append(')');
        return sb.toString();
    }

    private static boolean assetNamespaceExists(ServiceExecutor executor, String assetNS) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", assetNS);
        return executor.execute("asset.namespace.exists", dm.root()).booleanValue("exists");
    }

    private static void createAssetNamespace(ServiceExecutor executor, String assetNS) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", assetNS);
        executor.execute("asset.namespace.create", dm.root());
    }

    private static void notify(ServiceExecutor executor, String shareableId, String interactionId, String srcNS,
                               String dstNsPath, String notify) throws Throwable {

        XmlDoc.Element interaction = Interaction.describe(executor, interactionId);

        boolean notifyFacility = notify.equalsIgnoreCase("all") || notify.equalsIgnoreCase("facility");
        boolean notifyUser = notify.equalsIgnoreCase("all") || notify.equalsIgnoreCase("user");
        new InteractionNotification(interaction, getUserSelf(executor), srcNS, dstNsPath, notifyFacility,
                notifyUser, log()).send(executor);
    }

    private static XmlDoc.Element getUserSelf(ServiceExecutor executor) throws Throwable {
        XmlDoc.Element user = executor.execute("user.self.describe").element("user");
        if (!user.elementExists("e-mail")) {
            String domain = user.value("@domain");
            String username = user.value("@user");
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("domain", domain);
            dm.add("user", username);
            user = executor.execute("user.describe", dm.root()).element("user");
        }
        return user;
    }

    // static void moveData(ServiceExecutor executor, String srcNS, String
    // dstParentNS, String ifexists) throws Throwable {
    // String nsName = Paths.get(srcNS).getFileName().toString();
    // String dstNS = Paths.get(dstParentNS, nsName).toString();
    //
    // PluginTask.checkIfThreadTaskAborted();
    // boolean dstNSExists = assetNamespaceExists(executor, dstNS);
    // if (dstNSExists) {
    // if ("ignore".equalsIgnoreCase(ifexists)) {
    // PluginLog.log().add(PluginLog.WARNING,
    // "Destination asset namespace: '" + dstNS + "' already exists. Ignored.");
    // return;
    // } else if ("rename".equalsIgnoreCase(ifexists)) {
    // while (dstNSExists) {
    // PluginTask.checkIfThreadTaskAborted();
    // nsName = renameAssetNamespace(nsName);
    // dstNS = Paths.get(dstParentNS, nsName).toString();
    // PluginTask.checkIfThreadTaskAborted();
    // dstNSExists = assetNamespaceExists(executor, dstNS);
    // }
    // }
    //
    // // create dst namespace
    // PluginTask.checkIfThreadTaskAborted();
    // createAssetNamespace(executor, dstNS);
    //
    // // move sub namespaces.
    // XmlDocMaker dm = new XmlDocMaker("args");
    // dm.add("namespace", srcNS);
    // PluginTask.checkIfThreadTaskAborted();
    // Collection<String> subNSNames = executor.execute("asset.namespace.list",
    // dm.root())
    // .values("namespace/namespace");
    // if (subNSNames != null) {
    // for (String subNSName : subNSNames) {
    // PluginTask.checkIfThreadTaskAborted();
    // moveAssetNamespace(executor, Paths.get(srcNS, subNSName).toString(), dstNS);
    // }
    // }
    //
    // // move assets
    // dm = new XmlDocMaker("args");
    // dm.add("where", "namespace='" + srcNS + "'");
    // dm.add("action", "pipe");
    // dm.add("stoponerror", "true");
    // dm.add("size", "infinity");
    // dm.push("service", new String[]{"name", "asset.move"});
    // dm.add("namespace", dstNS);
    // dm.pop();
    // PluginTask.checkIfThreadTaskAborted();
    // executor.execute("asset.query", dm.root());
    // } else {
    // moveAssetNamespace(executor, srcNS, dstParentNS);
    // }
    // }

    private static void updateManifestAttribute(ServiceExecutor executor, String assetNamespace, String interactionId)
            throws Throwable {
        String manifestAssetPath = assetNamespace + "/" + Manifest.MANIFEST_FILE_NAME;
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", "path=" + manifestAssetPath);
        dm.add("name", Manifest.ATTRIBUTE_USER_INTERACTION_COSUMED);
        logWarning("[interaction: " + interactionId + "] updating attributes for manifest asset: " + manifestAssetPath);
        executor.execute(SvcFacilityShareableUploadManifestAttributeIncrement.SERVICE_NAME, dm.root());
        logWarning("[interaction: " + interactionId + "] updated attributes for manifest asset: " + manifestAssetPath);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Completion handler service for the user interaction initiated by a shareable upload.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    // private static void moveAssetNamespace(ServiceExecutor executor, String
    // srcNS, String dstParentNS)
    // throws Throwable {
    // XmlDocMaker dm = new XmlDocMaker("args");
    // dm.add("namespace", srcNS);
    // dm.add("to", dstParentNS);
    // executor.execute("asset.namespace.move", dm.root());
    // }

    @Override
    public Access access() {
        return ACCESS_MODIFY;
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

        // TODO Debug
//		System.out.println(String.format("DEBUG_001: %s: %s", SERVICE_NAME, XmlPrintStream.outputToString(args)));
//		System.out.println(String.format("DEBUG_002: %s",
//				XmlPrintStream.outputToString(executor().execute("actor.self.describe"))));
//		System.out.println(String.format("DEBUG_003: %s", XmlPrintStream.outputToString(executor()
//				.execute("system.session.self.describe", "<args><permissions>true</permissions></args>", null, null))));

        String shareableId = args.value("shareable-id");
        // String uploadId = args.value("upload-id");
        String interactionId = args.value("interaction-id");
        // String interactionToken = args.value("interaction-token");

        // source namespace
        String uploadNS = args.value("args/" + ARG_UPLOAD_NS);

        // notify sharer and/or user
        String notify = args.stringValue("args/" + ARG_NOTIFY, "none");

        // copy data to destination projects
        String copyToProject = args.value("args/" + ARG_COPY_TO_PROJECT);
        String ifexists = args.stringValue("args/" + ARG_IFEXISTS, "ignore");
        try {
            if (copyToProject != null) {
                logWarning("[interaction: " + interactionId + "] started post-interaction processing");
                String projectNamespace = copyToProject.indexOf('/') != -1 ? copyToProject
                        : Projects.resolveProjectNamespace(executor(), copyToProject);
                // destination path
                copyData(executor(), uploadNS, projectNamespace, ifexists);
                if (!notify.equalsIgnoreCase("none")) {
                    notify(executor(), shareableId, interactionId, uploadNS, projectNamespace, notify);
                }
                updateManifestAttribute(executor(), uploadNS, interactionId);
                logWarning("[interaction: " + interactionId + "] completed post-interaction processing");
            } else {
                logWarning("[interaction: " + interactionId
                        + "] no destination project specified, exit post-interaction processing.");
            }
        } catch (Throwable e) {
            log().add(PluginLog.ERROR,
                    "[interaction: " + interactionId + "] failed post-interaction processing. Error: " + e.getMessage(),
                    e);
            throw e;
        }
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

}
