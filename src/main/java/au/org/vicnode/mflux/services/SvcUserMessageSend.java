/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;
import au.org.vicnode.mflux.plugin.util.Util;

public class SvcUserMessageSend extends PluginService {

	private Interface _defn;

	public SvcUserMessageSend()  throws Throwable {
		_defn = new Interface();
		//
		Interface.Element ie = new Interface.Element("domain", StringType.DEFAULT, "Aauthentication domains you wish to include.", 1, Integer.MAX_VALUE);
		_defn.add(ie);
		//
		_defn.add(new Interface.Element("role", StringType.DEFAULT, "Require that user holds one of these roles to receive an email.", 1, Integer.MAX_VALUE));
		//
		_defn.add(new Interface.Element("body", StringType.DEFAULT, "The message body.", 1, 1));
		_defn.add(new Interface.Element("subject", StringType.DEFAULT, "The message subject.", 1, 1));
		_defn.add(new Interface.Element("from-is-caller",BooleanType.DEFAULT,"Set the 'from' field to the caller if true (default). If false, 'from' is set to the value of the server property 'mail.from'.",0,1));
		_defn.add(new Interface.Element("email", StringType.DEFAULT, "Additional email addresses to send the message to (the filters 'role' and 'email-suffix' do not apply to these.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("email-filter", StringType.DEFAULT, "A string (e.g. @unimelb.edu.au) that the users email address must contain so that they receive the email message.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("send",BooleanType.DEFAULT,"Actually send the message (default is false).  If false, just lists the potential recipients.",0,1));
		_defn.add(new Interface.Element("bulk",BooleanType.DEFAULT,"Send to all recipients in one action, else send one by one (default). Bulk emails are prone to being rejected by servers as spam.",0,1));
		_defn.add(new Interface.Element("debug",BooleanType.DEFAULT,"Add Debug message to output.", 0, 1));
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Sends an email to all users from the specified authentication domains except those users explicitly excluded by filtering arguments. If a bulk message is not sent, there is a one second delay between each message send. If a bulk message is sent, all userts are BCCd.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "user.message.send";
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		// Inputs
		Collection<String> domains = args.values("domain");
		String subject = args.stringValue("subject");
		String body = args.stringValue("body");
		Boolean fromIsCaller = args.booleanValue("from-is-caller",  true);
		Collection<String> emails = args.values("email");
		Collection<String> emailFilters = args.values("email-filter");
		Boolean send = args.booleanValue("send",  false);
		Boolean bulk = args.booleanValue("bulk",  false);
		Collection<String> heldRoles = args.values("role");
		Boolean dbg = args.booleanValue("debug",  false);
		//
		w.add("subject", subject);
		w.add("body", body);
		//
		send (executor(), domains, heldRoles, emails, emailFilters, subject,
				body, fromIsCaller, send, bulk, dbg,  w);
	}


	public static void send (ServiceExecutor executor, Collection<String> domains, Collection<String> heldRoles, 
			Collection<String> extraEmails, Collection<String> emailFilters, String subject, String body, 
			Boolean fromIsCaller, Boolean send, Boolean bulk, 
			Boolean dbg, XmlWriter w) throws Throwable {



		// Populate with user emails
		HashSet<String> emails = new HashSet<String>();        // WIll make unique list
		//
		if (domains!=null) {
			for (String domain : domains) {
				PluginTask.checkIfThreadTaskAborted();
				if (dbg) {
					w.push("domain");
				}
				if (dbg) {
					w.add("name",  domain); 
				}
				if (enabled(executor,domain)) {
					if (dbg) {
						w.add("keep", new String[] {"enabled", "true", "excluded", "false"}, true);
					}
					addUsers (executor, dbg, domain, heldRoles, emails, emailFilters, w);
				} else {
					if (dbg) {
						w.add("keep", new String[] {"enabled", "false"}, "false");
					}
				}
				if (dbg) {
					w.pop();
				}
			}			
		}

		if (extraEmails!=null) {
			emails.addAll(extraEmails);
		}

		// Send
		String from;
		if (fromIsCaller) {
			from = User.getUserEMail(executor);
		} else {
			from = Util.getServerProperty(executor, "mail.from");
		}
		w.add("from", from);
		if (!send) {
			for (String email : emails) {
				w.add("bcc", email);
			}
		} else {
			if (bulk) {
				MailHandler.sendMessage (executor, emails, subject, body, null, true, from, w);
			} else {
				for (String email : emails) {
					MailHandler.sendMessage (executor, email, subject, body, null, false, from);
					TimeUnit.SECONDS.sleep(1);
				}
			}
		}
	}

	private static Boolean enabled (ServiceExecutor executor, String domain) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		XmlDoc.Element r = executor.execute("authentication.domain.enabled", dm.root());
		return r.booleanValue("enabled");
	}



	private static void addUsers (ServiceExecutor executor, Boolean dbg,  String domain, 
			Collection<String> heldRoles, HashSet<String> emails, Collection<String> emailFilters, XmlWriter w) throws Throwable {

		//Iterate through held roles.  user.describe will expect ALL roles to be held (AND). We want an OR.
		for (String heldRole : heldRoles) {
			if (dbg) {
				w.push("role");
			}
			if (dbg) {
				w.add("name", heldRole);
			}
			addUsersWithRole (executor, dbg, domain, heldRole, emails, emailFilters, w);
			if (dbg) {
				w.pop();
			}
		}

	}


	private static void addUsersWithRole (ServiceExecutor executor, Boolean dbg,  String domain, 
			String heldRole, HashSet<String> emails, Collection<String> emailFilters, XmlWriter w) throws Throwable {

		// Get users for domain with held role
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("size", "infinity");
		dm.add("role", new String[] {"type", "role"}, heldRole);
		XmlDoc.Element r = executor.execute("user.describe", dm.root());
		Collection<XmlDoc.Element> users = r.elements("user");
		if (users==null) return;
		if (users.size()==0) return;

		// Iterate through users
		if (dbg) {
			w.push("users");
		}
		for (XmlDoc.Element user : users) {
			PluginTask.checkIfThreadTaskAborted();

			// Only add if account enabled
			String userName = user.value("@user");
			Boolean enabled = user.booleanValue("@enabled", true);
			if (enabled) {
				String email = user.value("e-mail");

				// Now filter on email suffix
				if (email!=null) {
					if (!email.isEmpty()) {
						if (filterEmail(email, emailFilters)) {
							if (dbg) {
								w.add("user", new String[] {"enabled", "true", "email", email, "included", "true"}, userName);
							}
							emails.add(email);
						} else {
							if (dbg) {
								w.add("user", new String[] {"enabled", "true", "email", email, "included", "false"}, userName);
							}
						}
					} else {
						if (dbg) {
							w.add("user", new String[] {"enabled", "true",  "email", "none", "included", "false"}, userName);
						}
					}
				} 
			} else {
				if (dbg) {
					w.add("user", new String[] {"enabled", "false", "email", "none", "included", "false"}, userName);
				}
			}
		}
		if (dbg) {
			w.pop();
		}
	}



	private static Boolean filterEmail (String email, Collection<String> filters) throws Throwable {
		if (filters==null) return true;
		if (filters.size()==0) return true;
		for (String filter : filters) {
			if (email.contains(filter)) {
				// Keep
				return true;
			}
		}

		// Discard
		return false;
	}

	/**
	 * Send a message to the specified users
	 * 
	 * @param executor
	 * @param emails
	 * @param subject
	 * @param body
	 * @param bodyType mime type for body, if  null, defaults to text/plain
	 * @param bcc true for BCC else all can see each other's email
	 * @param from 
	 * @param w  The Writer will have a field "to" or "bcc" appended to it for each recipient
	 * @throws Throwable
	 */
	public static void sendMessage (ServiceExecutor executor, Collection<String> emails, String subject, String body, String bodyType, Boolean bcc, String
			from, XmlWriter w) throws Throwable {
		if (emails==null) return;
		XmlDocMaker dm = new XmlDocMaker("args");
		if (bodyType!=null) {
			dm.add("body", new String[]{"type", bodyType}, body);
		} else {
			dm.add("body", body);
		}
		dm.add("subject", subject);
		dm.add("from", from);
		w.add("from", from);
		//
		for (String email : emails) {
			if (bcc) {
				w.add("bcc", email);
				dm.add("bcc", email);
				//
				dm.add("to", from);     // Need one 'to' field (for bcc) so copy self 
				w.add("to", from);
			} else {
				w.add("to", email);
				dm.add("to", email);
			}
		}

		executor.execute("mail.send", dm.root());
	}
}
