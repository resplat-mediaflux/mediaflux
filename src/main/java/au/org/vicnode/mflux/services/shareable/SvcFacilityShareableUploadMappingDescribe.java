package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.LongType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcFacilityShareableUploadMappingDescribe extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.mapping.describe";

    private Interface _defn;

    public SvcFacilityShareableUploadMappingDescribe() {
        _defn = new Interface();
        _defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "The upload shareable id.", 1, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Describes the mapping definition of the upload shareable if any.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String shareableId = args.value("shareable-id");
        XmlDoc.Element shareable = UploadShareable.describe(executor(), shareableId);
        UploadMapping mapping = UploadMapping.get(executor(), shareable);
        if (mapping != null) {
            mapping.describe(w);
        }
    }

}
