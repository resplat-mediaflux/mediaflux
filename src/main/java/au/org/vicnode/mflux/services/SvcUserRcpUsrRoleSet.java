
/**
 * @author Raj
 *
 * Copyright (c) 2025, The University of Melbourne, Australia
 *
 * All rights reserved.a
 */
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;
import org.json.JSONArray;
import org.json.JSONObject;
import unimelb.utils.JsonUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;


public class SvcUserRcpUsrRoleSet extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "user.rcpusr.role.set";

    private Interface _defn;

    public SvcUserRcpUsrRoleSet() throws Throwable {
        _defn = new Interface();
        _defn.add(new Interface.Element("rcp-url", StringType.DEFAULT,
                "Url for the rcp api", 1, 1));
        _defn.add(new Interface.Element("api-key", StringType.DEFAULT,
                "Api key to access rcp api", 1, 1));
    }

    public Access access() {
        return ACCESS_ADMINISTER;
    }

    public Interface definition() {
        return _defn;
    }

    public String description() {
        return "Set the rcpusr role for all users who should have them and remove from users who shouldn't";
    }

    public String name() {
        return SERVICE_NAME;
    }



    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
        removeRCPUsrRole();
        String rcpUrl = args.stringValue("rcp-url");
        String api_key = args.stringValue("api-key");
        String response = getRcpUsers(rcpUrl,api_key);
        //w.add("rcpusers",response);
        JSONObject jsonObject = new JSONObject(response);
        for (Iterator<String> it = jsonObject.keys(); it.hasNext(); ) {
            JSONObject j = jsonObject.getJSONObject(it.next());
            String path = j.getString("path");
            String rcao = j.getString("rcao");
            Collection<String> users = getStringListFromJsonArray(j.getJSONArray("users_with_manage_resources_permission"));
            users.add(rcao);
            String projectRole = Project.actualRoleName(path, Project.ProjectRoleType.RCPUSR);
            for (String userEmail : users) {
                String domain = "local";
                if (userEmail.contains("student") && userEmail.contains("unimelb")){
                    domain = "student";
                } else if (userEmail.contains("unimelb")) {
                    domain = "unimelb";
                }
                XmlDoc.Element currentUser = null;
                String user = null;
                try {
                    Collection<XmlDoc.Element> userList = User.findUser(executor(), domain, userEmail);
                    currentUser = userList.stream().findFirst().get();
                    user = currentUser.value("@user");
                }catch (Throwable e){
                    w.add("user", userEmail);
                    w.add("project", path);
                    w.add("role", projectRole);
                    w.add("error", "User not found");
                    continue;
                }
                //w.add("user",user);
                //w.add("domain",domain);

                try {
                    User.grantRevokeRole(executor(), domain + ":" + user, projectRole, true);
                }catch (Throwable E){
                    w.add("user", userEmail);
                    w.add("project", path);
                    w.add("role", projectRole);
                    w.add("error", "Could not grant user role");
                }
            }

        }
    }

    public void removeRCPUsrRole() throws Throwable {
        XmlDoc.Element projectElement = executor().execute(Properties.SERVICE_ROOT + "project.list");
        List<XmlDoc.Element> projects = projectElement.elements("project");
        for (XmlDoc.Element project : projects) {
            String rcprole = Project.actualRoleName(project.value(), Project.ProjectRoleType.RCPUSR);
            Vector<String> users = User.haveRole(executor(), rcprole, true);
            if (users.size() > 0) {
                for (String user : users) {
                    User.grantRevokeRole(executor(),user,rcprole,false);
                }
            }
        }
    }

    public Collection<String> getStringListFromJsonArray(JSONArray array){
        Collection<String> list = new ArrayList<String>();
        for (int i = 0; i < array.length(); i++) {
            String s = array.getString(i);
            list.add(s);
        }
        return list;
    }


    public String getRcpUsers(String rcpUrl,String apiKey) throws IOException, InterruptedException {
        //https://rcp.research.unimelb.edu.au/data_resources/get_users_access/mediaflux
        URL url = new URL(rcpUrl);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setRequestMethod("GET");
        // h2Qgr2Gi.EOTIcTfVquDsATpDwCa7TcUlPO4RuivZ
        httpConn.setRequestProperty("Authorization", "Api-Key "+apiKey);

        InputStream responseStream = httpConn.getResponseCode() / 100 == 2
                ? httpConn.getInputStream()
                : httpConn.getErrorStream();
        Scanner s = new Scanner(responseStream).useDelimiter("\\A");
        String response = s.hasNext() ? s.next() : "";

        return response;
    }
}

