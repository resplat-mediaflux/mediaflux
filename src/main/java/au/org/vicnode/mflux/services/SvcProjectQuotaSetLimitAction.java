/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services;

import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.EnumType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectQuotaSetLimitAction extends PluginService {




	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectQuotaSetLimitAction()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn,  allowPartialCID);
		_defn.add(new Interface.Element("action",new EnumType(new String[]{"allow", "fail"}),"Action to take on reaching the quota.",0, 1));

	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Sets the quota to be either 'fail' or 'allow' when the quota is reached.  Will not make changes to inherited quotas.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.quota.set.limit.action";
	}

	public boolean canBeAborted() {
		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}


	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String projectID = args.stringValue("project-id");
		String action = args.stringValue("action");

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (projectID!=null) {
			String t[] = Project.validateProject(executor(), projectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) return;

		//
		for (String pid : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), pid, false);

			XmlDoc.Element r = ph.metaData();
			XmlDoc.Element quota = r.element("namespace/quota");
			if (quota!=null) {
				// We don't update inherited quotas
				XmlDoc.Element inherited = quota.element("inherited");
				if (inherited==null) {
					String allocation = quota.value("allocation/@h");
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add("namespace", ph.nameSpace());
					dm.push ("quota");
					if (action.equals("allow")) {
						dm.add("on-overflow", "allow");
					} else if (action.equals("fail")) {
						dm.add("on-overflow", "fail");
					}
					dm.add("allocation", allocation);
					dm.pop();
					dm.add("replace", true);
					executor().execute("asset.namespace.quota.set", dm.root());
					w.add("project-id", new String[] {"inherited", "false", "allocation", allocation, "action", action}, ph.id());
				} else {
					w.add("project-id", new String[] {"inherited", "true", "action", "none"},ph.id());
				}
			}
		}
	}
}
