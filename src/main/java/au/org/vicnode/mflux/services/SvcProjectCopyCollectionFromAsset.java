/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services;


import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetUtil;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectCopyCollectionFromAsset extends PluginService {


	private Interface _defn;

	public SvcProjectCopyCollectionFromAsset()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("id", StringType.DEFAULT, "The asset ID of the 'user resource asset' tracking details about the user and project.", 1, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Service function to copy the standard '$NS:Collection' meta-data from an onboarding asset to the Project that was originally created from it. This is a remediation service only, it should not normally be needed.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.copy.collection.from.asset";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		String id = args.value("id");

		// Get asset meta-data
		XmlDoc.Element asset = AssetUtil.getAsset(executor(), null, id);
		XmlDoc.Element collectionMeta = asset.element("asset/meta/" + Properties.getCollectionDocType(executor()));
		if (collectionMeta==null) {
			throw new Exception ("No meta-data of type " + Properties.getCollectionDocType(executor()) + "found");
		}
		XmlDoc.Element onBoard = asset.element("asset/meta/" + Properties.getOnboardingDocType(executor()));
		String projectID = onBoard.value("project-id");
		String description = onBoard.value("description");

		if (projectID==null) {
			throw new Exception ("No project-id found in asset");
		}
		ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
		String projectNameSpace = ph.nameSpace();

		// Transfer to Collection as in creation service
		SvcProjectCreate.copyToCollection(executor(), ph.id(), description, collectionMeta);

		// Remove old from namespace
		XmlDoc.Element namespaceMeta = ph.metaData();
		String mid = namespaceMeta.value("namespace/asset-meta/"+Properties.getCollectionDocType(executor())+"/@id");
		NameSpaceUtil.removeAssetNameSpaceMetaData(null, executor(), projectNameSpace, mid);

		// Add new to namespace
		NameSpaceUtil.addAssetNameSpaceMetaData(null, executor(), projectNameSpace, collectionMeta);
	}
}
