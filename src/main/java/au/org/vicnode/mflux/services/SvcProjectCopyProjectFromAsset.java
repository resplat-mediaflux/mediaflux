/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetUtil;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectCopyProjectFromAsset extends PluginService {


	private Interface _defn;

	public SvcProjectCopyProjectFromAsset()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("id", StringType.DEFAULT, "The asset ID of the 'user resource asset' tracking details about the user and project.", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Service function to populate the standard '$NS-ADMIN:Project' meta-data from an onboarding asset to the Project that was originally created from it. This is a remediation service only, it should not normally be needed.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.copy.project.from.asset";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		String id = args.value("id");
		if (id==null) {
			Collection<String> ids = Project.findOnboardingAssets(executor());
			for (String id2 : ids) {
				update(executor(), id2, w);
			}
		} else {
			update (executor(), id, w);
		}
	}


	private void update(ServiceExecutor executor, String id, final XmlWriter w) throws Throwable {
		// Get asset meta-data
		XmlDoc.Element asset = AssetUtil.getAsset(executor, null, id);
		XmlDoc.Element onBoard = asset.element("asset/meta/" + Properties.getOnboardingDocType(executor()));
		if (onBoard==null) {
			throw new Exception ("No User-Onboarding meta-data set on onboarding asset");
		}
		String projectID = onBoard.value("project-id");
		if (projectID!=null) {
			ProjectHolder ph = new ProjectHolder(executor, projectID, false);
			String projectNameSpace = ph.nameSpace();
			if (NameSpaceUtil.assetNameSpaceExists(executor, null, projectNameSpace)) {

				// Remove old from namespace
				XmlDoc.Element namespaceMeta = NameSpaceUtil.describe(null, executor, projectNameSpace);
				XmlDoc.Element projectMeta = namespaceMeta.element("namespace/asset-meta/"+Properties.getProjectDocType(executor()));

				// Remove old
				if (projectMeta!=null) {
					String mid = projectMeta.value("/@id");
					NameSpaceUtil.removeAssetNameSpaceMetaData(null, executor, projectNameSpace, mid);
				}

				// Add new 
				SvcProjectCreate.setStandardOnBoardMeta(executor, asset.value("asset/@id"), ph.id(), projectNameSpace, onBoard);

				w.add("id", new String[]{"project-id", ph.id(), "namespace", projectNameSpace}, id);
			} else {
				w.add("id", new String[]{"project-id", ph.id(), "namespace", "none"}, id);
			}
		} else {
			w.add("id", new String[]{"project-id", "none"}, id);
		}
	}
}
