package au.org.vicnode.mflux.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;

public class SvcUserLDAPFixAAFUsers extends PluginService {


	private Interface _defn;

	public SvcUserLDAPFixAAFUsers() {
		_defn = new Interface();

		addToDefn(_defn);
	}


	static void addToDefn(Interface defn) {

		defn.add(new Interface.Element("check-only", BooleanType.DEFAULT, "Just check the process only (default true) but doesn't actually set the meta-data.", 0, 1));
		defn.add(new Interface.Element("user", StringType.DEFAULT, "Do for just this user. Defaults to all AAF users.", 0, 1));
	}

	@Override
	public String name() {
		return "vicnode.ldap.user.meta.fix";
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String description() {
		return "For an AAF user who has been migrated to AD, fetches (from migration note in mf-note) the AD account details and gets the person's name.  Then sets the person's name and email back into the AAF account (for which the process removed this information).";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		String theUser = args.value("user");
		Boolean checkOnly = args.booleanValue("check-only", true);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", "aaf");
		dm.add("size", "infinity");
		if (theUser!=null) {
			dm.add("user", theUser);
		}

		XmlDoc.Element r = executor().execute("user.describe", dm.root());
		Collection<XmlDoc.Element> users = r.elements("user");
		for (XmlDoc.Element user : users) {
			String aafUserName = user.value("@user");   // This is their email
			if (aafUserName.contains("unimelb.edu.au")) {
				w.push("user");
				w.add("aaf-username", aafUserName);
				String name = user.value("name");
				String email = user.value("e-mail");
				if (name==null && email==null) {
					w.add("meta-data", "missing");
					String note = user.value("asset/meta/mf-note/note");
					if (note!=null) {
						int idx = note.indexOf("'");
						int idx2 = note.lastIndexOf("'");
						String adUser = note.substring(idx+1, idx2);
						w.add("ad-name", adUser);

						// Get name
						name = getName (executor(), adUser);
						w.add("name", name);

						// Set
						w.push("setting");
						w.add("email", aafUserName);
						w.add("name", name);
						w.pop();

						// Set name and email back...
						dm = new XmlDocMaker("args");
						dm.add("domain", "aaf");
						dm.add("user", aafUserName);
						dm.add("email", aafUserName);
						dm.add("name", name);
						if (!checkOnly) {
							executor().execute("user.set", dm.root());
						}
					}
				} 
				w.pop();
			}
		}
	}

	private String getName (ServiceExecutor executor, String name) throws Throwable {
		String[] parts = name.split(":");
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", parts[0]);
		dm.add("user", parts[1]);
		XmlDoc.Element r = executor.execute("user.describe", dm.root());
		return r.value("user/name");	
	}

}