package au.org.vicnode.mflux.services.organisation;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcOrganisationRemove extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "organisation.remove";

    private Interface _defn;

    public SvcOrganisationRemove() {
        _defn = new Interface();
        _defn.add(new Interface.Element("name", StringType.DEFAULT, "The name of the organisation", 1, 1));
        _defn.add(new Interface.Element("check-users", BooleanType.DEFAULT, "Check assocated users. Defaults to true.",
                0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Removes an organisation.";
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String name = args.value("name");
        boolean checkUsers = args.booleanValue("check-users", true);

        String dict = Properties.getResearchOrgDict(executor());

        if (dictionaryEntryExists(executor(), dict, name)) {
            if (checkUsers && hasUser(executor(), name)) {
                throw new Exception("There are users currently associated with organisation: '" + name + "'");
            }
            removeDictionaryEntry(executor(), dict, name);
        }

    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    static boolean dictionaryEntryExists(ServiceExecutor executor, String dict, String term) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("dictionary", dict);
        dm.add("term", term);
        return executor.execute("dictionary.entry.exists", dm.root()).booleanValue("exists");
    }

    static boolean hasUser(ServiceExecutor executor, String organisation) throws Throwable {
        String docType = Properties.getUserAccountsDocType(executor);
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "xpath(" + docType + "/organisation/name)='" + organisation + "'");
        dm.add("action", "get-id");
        dm.add("size", 1);
        XmlDoc.Element re = executor.execute("asset.query", dm.root());
        return re.count("id") > 0;
    }

    static void removeDictionaryEntry(ServiceExecutor executor, String dict, String term) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("dictionary", dict);
        dm.add("term", term);
        executor.execute("dictionary.entry.remove", dm.root());
    }

}
