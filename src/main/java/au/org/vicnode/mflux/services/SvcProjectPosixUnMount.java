/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixUnMount extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectPosixUnMount()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn,  allowPartialCID);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Unmounts the posix mount point for the project.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.unmount";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Inputs
		String projectID = args.value("project-id");
		Boolean throwOnFail = true;
		unMount (executor(), projectID, throwOnFail, w);
	}




	public static void unMount (ServiceExecutor executor, String theProjectID, Boolean throwOnFail, XmlWriter w) throws Throwable {
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor, theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor, false, true));
		}

		// Iterate over projects
		if (projectIDs.size()==0) return;
		for (String projectID : projectIDs) {
			// If mounted, unmount
			Boolean isMounted = Posix.isProjectMounted(executor, projectID);
			if (isMounted) {
				try {
					
					// Get the  namespace meta-data
					XmlDoc.Element mountState = Posix.describePosixMountPoint(executor, projectID);	
								
					// Unmount
					Posix.unMountPosix(executor, projectID);
			
					// Update the namespace meta-data
					Boolean shouldBeMounted = false;
					Posix.updatePosixMountMetaData (executor, projectID, 
							mountState.elements("mount/restrict"), 
							mountState.element("mount/root"),
							mountState.booleanValue("mount/read-only"),
							mountState.element("mount/metadata-export"), 
							mountState.booleanValue("mount/apply-mode-bits", true),
							mountState.stringValue("mount/allowed-protocol", "all"), 
							shouldBeMounted);				
				} catch (Throwable t)  {
					w.add("project-id", new String[]{"state", "invalid", "error", t.getMessage()}, projectID);
					if (throwOnFail) throw new Exception(t);
					break;
				}
			} 
		}
	}
}
