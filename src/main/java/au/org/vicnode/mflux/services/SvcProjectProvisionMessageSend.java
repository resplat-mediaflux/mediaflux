package au.org.vicnode.mflux.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectProvisionMessageSend extends PluginService {

	public final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.provision.message.send";

	private Interface _defn;

	public SvcProjectProvisionMessageSend() {
		_defn = new Interface();
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT, "The project id.", 1, 1));
		_defn.add(new Interface.Element("domain", StringType.DEFAULT,
				"The authentication domain of the recipient user (must be a project administrator).", 1, 1));
		_defn.add(new Interface.Element("user", StringType.DEFAULT,
				"The username of the recipient user (must be a project administrator).", 1, 1));
		_defn.add(new Interface.Element("bcc", EmailAddressType.DEFAULT, "The blind carbon copy recipient(s)", 0, 10));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "send provision email to the specified project administrator.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		String projectId = args.value("project-id");
		String domain = args.value("domain");
		String user = args.value("user");
		if (!SvcProjectProvisionMessageGet.isProjectAdmin(executor(), domain, user, projectId)) {
			throw new IllegalArgumentException(
					"User " + domain + ":" + user + " is not administrator of project: " + projectId);
		}
		Collection<String> bcc = args.values("bcc");
		sendProvisionMessage(executor(), projectId, domain, user, bcc, w);
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	static void sendProvisionMessage(ServiceExecutor executor, String projectId, String domain, String user,
			Collection<String> bcc, XmlWriter w) throws Throwable {
		XmlDoc.Element ue = SvcProjectProvisionMessageGet.describeUser(executor, domain, user);
		String email = ue.value("e-mail");
		if (email == null) {
			throw new Exception("User " + domain + ":" + user + " does not have email set.");
		}
		String message = SvcProjectProvisionMessageGet.generateProvisionMessage(executor, projectId, ue);

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("subject", "New Mediaflux project: " + projectId + " has been provisioned");
		dm.add("body", message);
		dm.add("to", email);
		if (bcc != null) {
			for (String e : bcc) {
				dm.add("bcc", e);
			}
		}
		executor.execute("mail.send", dm.root());

		w.push("sent");
		w.add("to", email);
		if (bcc != null) {
			for (String e : bcc) {
				dm.add("bcc", e);
			}
		}
		w.pop();
	}

}
