package au.org.vicnode.mflux.services.shareable;

import java.util.Map;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.LongType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetUtil;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcFacilityShareableUploadManifestDescribe extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.manifest.describe";

    private Interface _defn;

    public SvcFacilityShareableUploadManifestDescribe() {
        _defn = new Interface();
        _defn.add(new Interface.Element("id", AssetType.DEFAULT,
                "The id of the manifest asset. If specified, shareable-id and upload-id arguments are ignored. Otherwise, shareable-id and upload-id must be specified.",
                0, 1));
        _defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "The associated shareable id.", 0, 1));
        _defn.add(new Interface.Element("upload-id", LongType.POSITIVE, "The associated upload id.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Get the metadata and the asset.attribute of the facility shareable upload manifest asset.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String assetId = args.value("id");
        String shareableId = args.value("shareable-id");
        String uploadId = args.value("upload-id");

        if (assetId != null) {
            describeManifestAsset(assetId, executor(), w);
        } else {
            if (shareableId == null) {
                throw new IllegalArgumentException(uploadId == null ? "Missing id" : "Missing shareable-id");
            }
            if (uploadId == null) {
                throw new IllegalArgumentException("Missing upload-id");
            }
            describeManifestAsset(executor(), shareableId, uploadId, w);
        }
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    private static void describeManifestAsset(String assetId, ServiceExecutor executor, XmlWriter w) throws Throwable {
        // get asset meta
        XmlDoc.Element ae = AssetUtil.getAssetMeta(executor, assetId);
        describeManifestAsset(executor, ae, w);
    }

    private static void describeManifestAsset(ServiceExecutor executor, String shareableId, String uploadId,
            XmlWriter w) throws Throwable {
        // get asset meta
        String assetId = Manifest.getManifestAssetId(executor, shareableId, uploadId, true);
        if (assetId != null && AssetUtil.assetExists(executor, assetId)) {
            XmlDoc.Element ae = AssetUtil.getAssetMeta(executor, assetId);
            describeManifestAsset(executor, ae, w);
        }
    }

    private static void describeManifestAsset(ServiceExecutor executor, XmlDoc.Element ae, XmlWriter w)
            throws Throwable {
        String assetId = ae.value("@id");
        XmlDoc.Element doc = ae.element("meta/" + Manifest.MANIFEST_DOC_TYPE);
        if (doc == null) {
            throw new Exception("Asset " + assetId + " is not a valid manifest asset. meta/"
                    + Manifest.MANIFEST_DOC_TYPE + " element is not found.");
        }

        // get asset.attributes
        Map<String, Integer> attributes = Manifest.getManifestAssetAttributes(executor, assetId);

        Integer shareableDownloadConsumed = attributes.get(Manifest.ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED);
        if (shareableDownloadConsumed != null) {
            if (doc.elementExists("download-shareable")) {
                doc.element("download-shareable").add(new XmlDoc.Element("consumed", shareableDownloadConsumed));
            } else {
                XmlDoc.Element e = new XmlDoc.Element("download-shareable");
                e.add(new XmlDoc.Element("consumed", shareableDownloadConsumed));
                doc.add(e);
            }
        }
        Integer shareableDownloadConsumedSubset = attributes.get(Manifest.ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED_SUBSET);
        if (shareableDownloadConsumedSubset != null) {
            if (doc.elementExists("download-shareable")) {
                doc.element("download-shareable")
                        .add(new XmlDoc.Element("consumed-subset", shareableDownloadConsumedSubset));
            } else {
                XmlDoc.Element e = new XmlDoc.Element("download-shareable");
                e.add(new XmlDoc.Element("consumed-subset", shareableDownloadConsumedSubset));
                doc.add(e);
            }
        }
        Integer userInteractionConsumed = attributes.get(Manifest.ATTRIBUTE_USER_INTERACTION_COSUMED);
        if (userInteractionConsumed != null) {
            if (doc.elementExists("user-interaction")) {
                doc.element("user-interaction").add(new XmlDoc.Element("consumed", userInteractionConsumed));
            } else {
                XmlDoc.Element e = new XmlDoc.Element("user-interaction");
                e.add(new XmlDoc.Element("consumed", userInteractionConsumed));
                doc.add(e);
            }
        }
        Integer directDownloadAttempt = attributes.get(Manifest.ATTRIBUTE_DIRECT_DOWNLOAD_ATTEMPT);
        if (directDownloadAttempt != null) {
            if (doc.elementExists("direct-download")) {
                doc.element("direct-download").add(new XmlDoc.Element("attempt", directDownloadAttempt));
            } else {
                XmlDoc.Element e = new XmlDoc.Element("direct-download");
                e.add(new XmlDoc.Element("attempt", directDownloadAttempt));
                doc.add(e);
            }
        }

        boolean worm = ae.elementExists("worm-status");

        w.push("shareable-upload-manifest", new String[] { "id", ae.value("@id"), "worm", Boolean.toString(worm) });
        w.add(doc, false);
        w.pop();
    }

}
