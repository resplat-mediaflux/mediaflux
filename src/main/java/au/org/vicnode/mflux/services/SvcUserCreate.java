/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.atomic.AtomicOperation;
import arc.mf.plugin.atomic.AtomicTransaction;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcUserCreate extends PluginService {

	private Interface _defn;

	public SvcUserCreate()  throws Throwable {
		_defn = new Interface();
		/*
		Interface.Element ie = new Interface.Element("authority",StringType.DEFAULT,"The authority of interest for users. Defaults to local.",0,1);
		ie.add(new Interface.Attribute("protocol", StringType.DEFAULT,
				"The protocol of the identity authority. If unspecified, defaults to federated user within the same type of repository.", 0));
		_defn.add(ie);
		 */
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "The name of the domain that the users will be created in. Defaults to the local end user domain supplied by the application property 'authentication.local.user' (e.g. local).", 0, 1));
		_defn.add(new Interface.Element("user", StringType.DEFAULT, "The username.", 1, 1));
		//
		Interface.Element me = new Interface.Element("name", StringType.DEFAULT, "User's full name", 1, 1);
		_defn.add(me);
		//
		Interface.Element me2 = new Interface.Element("password", StringType.DEFAULT, "The user's password.", 0, 1);
		me2.add(new Interface.Attribute("notify", BooleanType.DEFAULT,
				"If notification has been requested for account creation, and this attribute it true, then the password will be included in the notification. Defaults to false.", 0));
		_defn.add(me2);
		//
		_defn.add(new Interface.Element("email", StringType.DEFAULT, "The user's email address", 0, 1));
		_defn.add(new Interface.Element("home", StringType.DEFAULT, "The user's home namespace (optional).", 0, 1));
		//
		me2 = new Interface.Element("organisation",XmlDocType.DEFAULT,
				"The user's organisation.",1,1);

		me2.add(new Interface.Element("name", StringType.DEFAULT,
				"The name of the user's organisation. Must be a value found in the dictionary <user namespace>:research-organisation", 1, 1));
		me2.add(new Interface.Element("department", StringType.DEFAULT, "User's department", 0, 1));
		_defn.add(me2);

		_defn.add(new Interface.Element("role", StringType.DEFAULT, "Any additional role you wish to grant to the user.", 0, Integer.MAX_VALUE));
		//
		Interface.Element me3  = new Interface.Element("generate-password", BooleanType.DEFAULT, "Auto-generate the password and send to the user via the given email. Defaults to true.", 0, 1);
		me3.add(new Interface.Attribute("length", IntegerType.DEFAULT,
				"The password length. A length less than the minimum length for the domain will be ignored. If not specified, defaults to the minimum length for the domain.", 0));
		_defn.add(me3);
		//
		_defn.add(new Interface.Element("notify", BooleanType.DEFAULT, "If true, and the user has an e-mail address then notify them of account creation. If generate-password is true, then e-mail the generated password to the user. Defaults to false", 0, 1));
		_defn.add(new Interface.Element("notification", StringType.DEFAULT, "Some extra text sent with the notification to the user.", 0, 1));
		//
		Interface.Element me4  = new Interface.Element("password-expiry", StringType.DEFAULT, "The period before passwords expire. If supplied without units, then defaults to 'day'. Defaults to no expiry. Set to zero for immediate expiry.", 0, 1);
		me4.add(new Interface.Attribute("unit", new EnumType(new String[] {"seconds", "minute", "hour", "day", "year"}), 
				"The unit of expiry. If not provided defaults to 'day'.", 0));
		_defn.add(me4);
		//
		_defn.add(new Interface.Element("disable-date",  DateType.DEFAULT, "The date to disable the account on (will replace any existing specification).  If not set, no disable date is set.  The notification email will advise the user of this.", 0, 1));

	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Creates a standard project end user or administrator in the local authority.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "user.create";
	}

	public void execute(final Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {


		// Atomic creation so if fails we don't get 
		new AtomicTransaction(new AtomicOperation() {

			@Override
			public boolean execute(ServiceExecutor executor) throws Throwable {
				create (executor(), args, w);
				return false;
			}
		}).execute(executor()); 




	}

	private void create (ServiceExecutor executor, Element args, XmlWriter w) throws Throwable {
		// Inputs
		//XmlDoc.Element authority = args.element("authority");
		String domain = args.stringValue("domain", Properties.getUserAuthDomain(executor));
		String user = args.value("user");
		String name = args.value("name");
		String email = args.value("email");
		XmlDoc.Element pw = args.element("password");
		Collection<String> roles = args.values("role");
		Boolean notify = args.booleanValue("notify",false);
		Boolean generate = args.booleanValue("generate-password", true);
		String notification = args.value("notification");
		XmlDoc.Element org = args.element("organisation");
		String home = args.stringValue("home");
		XmlDoc.Element passwordExpiry = args.element("password-expiry");
		Date disableDate = args.dateValue("disable-date");


		// Create user
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		dm.add("name",name);
		if (pw!=null) dm.add(pw);
		if (email!=null) dm.add("email", email);
		dm.add("generate-password",generate);
		if (passwordExpiry!=null) {
			dm.add(passwordExpiry);
		}
		dm.add("notify",notify);
		if (home!=null) dm.add("home", home);
		if (notify) {
			dm.add("notify-subject", "Mediaflux account created");
			String p = Properties.getApplicationProperty(executor(),
					Properties.APPLICATION_PROPERTY_HOST_AND_PORT, Properties.APPLICATION_PROPERTY_APP);
			String[] parts = p.split(":");
			//String protocol = parts[0];
			String host = parts[1].substring(2);
			//String port = parts[2];
			String disableString = "None";
			if (disableDate != null) {
				disableString = DateUtil.formatDate(disableDate, false, false);
			}
			String t ="Dear Colleague,\n\n" +
					"A Mediaflux account has been created for you as follows:\n\n" +
					"Host:      " + host + "\n" +
					"Name:      " + name + "\n" +
					"Domain:    " + domain + "\n" +
					"Username:  " + user + "\n" +
					"Password:  $password$ \n" +
					"Expiry:    " + disableString + "\n\n";

			if (disableDate!=null) {
				t += "Your account will be disabled on the given expiry date and you will receive an email notification. " +
						"You can apply to have the account re-enabled via the same authorizing University of Melbourne " +
						"researcher who requested your account.\n\n";
			}

			t += "To reset your password please visit the following URL with your browser and click " +
					"the Reset Password link in the bottom right of the login dialogue.\n\n";
			t += Properties.getApplicationProperty(executor(),Properties.APPLICATION_PROPERTY_HOST_AND_PORT, Properties.APPLICATION_PROPERTY_APP) + "/desktop/?dti=no\n\n";
			t += "We recommend the Mediaflux Explorer as the initial way you can explore your data in Mediaflux. See: " + Properties.getApplicationProperty(executor(),
					Properties.APPLICATION_PROPERTY_DOC_USER_EXPLORER, Properties.APPLICATION_PROPERTY_APP) +
					"\n\nFor further information, including support, see the top-level page of the Mediaflux documentation at: "
					+ Properties.getApplicationProperty(executor(), Properties.APPLICATION_PROPERTY_DOC_USER_ROOT, Properties.APPLICATION_PROPERTY_APP) + "\n\n";
			if (notification!=null) {
				t += notification + "\n";
			}
			t += "\nRegards,\n" +
					"Research Computing Services - Data Solutions Team\nBusiness Services\nThe University of Melbourne\n";

			dm.add("notify-body", new String[]{"type", "text/plain"}, t);
		}

		// Create the user
		//		if (authority==null) {
		dm.push("meta");
		dm.push(Properties.getUserAccountsDocType(executor));
		dm.add(org);
		dm.pop();
		dm.pop();
		executor.execute("user.create", dm.root());
		//		} else {
		// athentication.user.create is intended for local representations of
		// remote authorities or accounts like DICOM
		//			dm.add(authority);
		//			executor.execute("authentication.user.create", dm.root());
		//		}

		// Set disable date if any
		if (disableDate!=null) {
			dm = new XmlDocMaker("args");
			dm.add("domain", domain);
			dm.add("user", user);
			dm.add("date", disableDate);
			executor.execute("unimelb.user.disable.set", dm.root());
		}

		// Grant roles. 
		dm = new XmlDocMaker("args");

		// Grant standard role to provide basic access to Mediaflux
		addStandardRoles(executor(), dm, false);

		// Add user specified
		dm.add("type", "user");
		//		if (authority!=null) {
		//			dm.add("name", authority + ":" + domain + ":" + user);
		//		} else {
		dm.add("name",domain + ":" + user);
		//		}

		if (roles!=null) {
			for (String role : roles) {
				dm.add("role", new String[]{"type", "role"}, role);
			}
		}

		// Execute
		executor.execute("actor.grant", dm.root());
		
		// Describe
		dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		dm.add("permissions", true);
		w.add(executor.execute("user.describe", dm.root()));
	}


	public static void addStandardRoles (ServiceExecutor executor, XmlDocMaker dm, Boolean isAdmin) throws Throwable {
		// Grant standard roles
		dm.add("role", new String[]{"type", "role"}, Properties.getStandardUserRoleName(executor));
		if (isAdmin){
			dm.add("role", new String[]{"type", "role"}, Properties.getAdministratorRoleName(executor));
		}

	}
}
