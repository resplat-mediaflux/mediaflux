/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;

import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectAssetCount extends PluginService {

	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectAssetCount() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
		_defn.add(new Interface.Element("DR", BooleanType.DEFAULT,
				"List result from DR server also (default false).", 0, 1));
		_defn.add(new Interface.Element("multi-version", BooleanType.DEFAULT,
				"In addition, list the number of assets with multiple content versions (default false).", 0, 1));
		SvcProjectDescribe.addRepType(_defn);
	}




	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Lists asset and namespace counts per project.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.asset.count";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		Boolean multiVersion = args.booleanValue("multi-version", false);
		Boolean doDR = args.booleanValue("DR", false);
		String repType = args.stringValue("rep-type", Properties.repTypes[0]);

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID != null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else {
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size() == 0) {
			return;
		}

		String DRServerUUID = ProjectReplicate.DRServerUUID(executor());
		ServerRoute sr = new ServerRoute(DRServerUUID);

		// Iterate  over projects and summarise
		long nPrimaryTotal = 0l;
		long nDRTotal = 0l;
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			//
			PluginTask.checkIfThreadTaskAborted();
			// FInd replication asset processing queue
		    String qName = 
		    		ProjectReplicate.isProjectReplicated(ph);	

			// See if we want to process this one
			if (Project.keepOnRepType (executor(), qName, repType)) {
				w.push("project");
				w.add("id", projectID);
				String nameSpace = ph.nameSpace();
				long nAssets = countAssets (null, executor(), nameSpace, w);
				long nNamespaces = countNamespaces (null, executor(), nameSpace, w);
				nPrimaryTotal += nAssets;
				w.push("primary");
				w.add("count-namepaces", nNamespaces);
				w.add("count-assets", nAssets);
				if (multiVersion) {
					long n2 = countContentVersions (null, executor(), nameSpace);
					w.add("count-multi-assets", n2);
				}
				w.pop();
				if (doDR) {
					String nameSpaceDR = ph.nameSpaceDR();
					long nAssetsDR = countAssets (sr, executor(), nameSpaceDR, w);
					long nNamespacesDR = countNamespaces (sr, executor(), nameSpaceDR, w);
					nDRTotal += nAssetsDR;
					w.push("DR");
					w.add("count-namepaces", nNamespacesDR);
					w.add("count-assets", nAssetsDR);

					if (multiVersion) {
						long n2 = countContentVersions (sr, executor(), nameSpaceDR);
						w.add("count-multi-assets", n2);
					} 
					w.pop();
				}
				w.pop();
			}
			PluginTask.checkIfThreadTaskAborted();
		}
		w.add("n-assets-total-primary", nPrimaryTotal);
		w.add("n-assets-total-DR", nDRTotal);
	}

	private long  countAssets (ServerRoute sr, ServiceExecutor executor, String namespace, XmlWriter w) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace",namespace);
		XmlDoc.Element r = executor.execute("asset.count", dm.root());
		String n =  r.value("total");
		long nl = Long.parseLong(n);
		return nl;
	}
	
	private long  countNamespaces (ServerRoute sr, ServiceExecutor executor, String namespace, XmlWriter w) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace",namespace);
		XmlDoc.Element r = executor.execute("asset.namespace.count", dm.root());
		String n =  r.value("count");
		long nl = Long.parseLong(n);
		return nl;
	}

	// Find the number of assets with multiple content versions in
	// this namespace. We really want to know total number of content versions
	// since each one equates to a file on the file system
	private long countContentVersions (ServerRoute sr, ServiceExecutor executor, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", "namespace>='"+nameSpace+"' and (content version count>1)");
		dm.add("action", "count");
		XmlDoc.Element r = executor.execute(sr, "asset.query", dm.root());
		if (r==null) {
			return 0l;
		}
		String n = r.value("value");
		long nl = Long.parseLong(n);
		return nl;

		// If we want to find the number content versions per asset
		// then code along these lines
		/*
		asset.query :where id=953 :action get-value :xpath content/@versions
			    :asset -id "953" -version "3"
			        :value "3"
			        
			*/        
	}
}
