package au.org.vicnode.mflux.services.dataregistry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import au.org.vicnode.mflux.plugin.util.Properties;
import arc.xml.XmlWriter;
import unimelb.rcs.data.registry.Collection;
import unimelb.rcs.data.registry.CollectionRef;
import unimelb.rcs.data.registry.DataRegistry;

public class SvcDataRegistryCollectionList extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "data.registry.collection.list";

	private Interface _defn;

	public SvcDataRegistryCollectionList() {
		_defn = new Interface();
		addArgsDefn(_defn, false);
	}

	static void addArgsDefn(Interface defn, boolean restrictCreatedAfter) {
		Interface.Element createdAfter = new Interface.Element("created-after", DateType.DATE_ONLY,
				"List only the collections created after the given date. "
						+ (restrictCreatedAfter ? "Defaults to 'today-30day' if not specified." : ""),
				0, 1);
		createdAfter.add(new Interface.Attribute("inclusive", BooleanType.DEFAULT,
				"Include the specified date. Defaults to true.", 0));
		defn.add(createdAfter);

		Interface.Element createdBefore = new Interface.Element("created-before", DateType.DATE_ONLY,
				"List only the collections created before the given date.", 0, 1);
		createdBefore.add(new Interface.Attribute("inclusive", BooleanType.DEFAULT,
				"Include the specified date. Defaults to true.", 0));
		defn.add(createdBefore);

		Interface.Element status = new Interface.Element("status", new EnumType(Collection.Status.values()),
				"List only the collections of the specified status. If not specified, all status are included.", 0, 1);
		defn.add(status);

		Interface.Element type = new Interface.Element("type", new EnumType(Collection.Type.values()),
				"List only the collections of the specified type. If not specified, all types are included", 0, 1);
		defn.add(type);

		defn.add(new Interface.Element("code-contains", StringType.DEFAULT, "Filter result by collection code.", 0, 1));

		defn.add(new Interface.Element("name-contains", StringType.DEFAULT, "Filter result by collection name.", 0, 1));

		defn.add(new Interface.Element("request-source-contains", StringType.DEFAULT,
				"Filter result by request_source.", 0, 1));

		Interface.Element sort = new Interface.Element("sort",
				new EnumType(new String[] { "oldest-first", "newest-first" }), "Sort order. Defaults to newest-first",
				0, 1);
		defn.add(sort);
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "List collections in RCS data registry.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		List<CollectionRef> collections = listCollections(executor(), args);
		if (collections != null) {
			for (CollectionRef collection : collections) {
				collection.saveXml(w);
			}
		}
		w.add("count", collections.size());
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	public static List<CollectionRef> listCollections(ServiceExecutor executor, XmlDoc.Element args) throws Throwable {
		Date createdAfter = args.dateValue("created-after", null);
		boolean createdAfterInclusive = args.booleanValue("created-after/@inclusive", true);

		Date createdBefore = args.dateValue("created-before", null);
		boolean createdBeforeInclusive = args.booleanValue("created-before/@inclusive", true);

		Collection.Status status = Collection.Status.fromString(args.value("status"));

		Collection.Type type = Collection.Type.fromString(args.value("type"));

		String codeContains = args.value("code-contains");

		String nameContains = args.value("name-contains");

		String requestSourceContains = args.value("request-source-contains");

		String sortOrder = args.stringValue("sort", "newest-first");
		boolean newestFirst = "newest-first".equalsIgnoreCase(sortOrder);

		return listCollections(executor, status, type, createdAfter, createdAfterInclusive, createdBefore,
				createdBeforeInclusive, codeContains, nameContains, requestSourceContains, newestFirst);
	}

	public static List<CollectionRef> listCollections(ServiceExecutor executor, Collection.Status status,
			Collection.Type type, Date createdAfter, boolean createdAfterInclusive, Date createdBefore,
			boolean createdBeforeInclusive, String codeContains, String nameContains, String requestSourceContains,
			boolean newestFirst) throws Throwable {
		List<CollectionRef> resultCollections = new ArrayList<CollectionRef>();

		DataRegistry registry = DataRegistry.get(executor);
		List<CollectionRef> allCollections = registry.listCollections();
		if (allCollections != null) {
			for (CollectionRef collection : allCollections) {
				if (collection.matches(status, type, createdAfter, createdAfterInclusive, createdBefore,
						createdBeforeInclusive, codeContains, nameContains, requestSourceContains)) {
					resultCollections.add(collection);
				}
			}
		}
		Collections.sort(resultCollections, new Comparator<CollectionRef>() {
			@Override
			public int compare(CollectionRef o1, CollectionRef o2) {
				int r = o1.created.compareTo(o2.created);
				if (newestFirst) {
					r = r * (-1);
				}
				if (r == 0) {
					r = o1.code.compareTo(o2.code);
				}
				return r;
			}
		});
		return resultCollections;
	}

}
