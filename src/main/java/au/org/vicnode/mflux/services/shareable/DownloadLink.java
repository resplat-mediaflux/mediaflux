package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.ServiceExecutor;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import au.org.vicnode.mflux.plugin.util.Project;

import java.util.*;

public class DownloadLink {

    public static final int TOKEN_LENGTH = 20;
    public static final int PASSWORD_LENGTH = 20;
    public static final int COMPRESSION_LEVEL = 6;

    /**
     * secure identity token.
     */
    public final String token;
    /**
     * secure identity token id.
     */
    public final String tokenId;
    /**
     * actor name.
     */
    public final String actorName;
    /**
     * the zip archive is compressed or not.
     */
    public final boolean compress;
    /**
     * expiry of the download token.
     */
    public final Date expiry;
    /**
     * password for the download link
     */
    public final String password;
    /**
     * associated upload shareable id
     */
    public final String shareableId;
    /**
     * associated upload id
     */
    public final String uploadId;
    /**
     * associated asset namespace
     */
    public final String assetNamespace;
    /**
     * the download url.
     */
    public final String url;

    DownloadLink(XmlDoc.Element te, String serverAddr, boolean compress, Date expiry, String shareableId,
            String uploadId, String assetNamespace) throws Throwable {
        this.token = te.value();
        this.tokenId = te.value("@id");
        this.actorName = te.value("@actor-name");
        this.password = te.value("@auth-key");
        this.compress = compress;
        this.expiry = expiry;
        this.shareableId = shareableId;
        this.uploadId = uploadId;
        this.assetNamespace = assetNamespace;
        int idx = assetNamespace.lastIndexOf('/');
        String ansName = idx == -1 ? assetNamespace : assetNamespace.substring(idx + 1);
        this.url = String.format("%s/mflux/share.mfjp?_token=%s&browser=true&filename=%s.zip",
                serverAddr.replaceAll("/+$", ""), this.token, ansName);
    }

    public static DownloadLink create(ServiceExecutor executor, String uploadNS, XmlDoc.Element shareOptions,
            XmlDoc.Element shareable, String uploadId) throws Throwable {
        boolean compress = shareOptions.booleanValue("download/direct/compress", true);
        Integer durationDays = shareOptions.intOrNullValue("duration");
        boolean generatePassword = shareOptions.booleanValue("download/direct/generate-password", false);
        return create(executor, uploadNS, compress, durationDays, generatePassword, shareable, uploadId);
    }

    public static DownloadLink create(ServiceExecutor executor, String uploadNS, boolean compress, Integer durationDays,
            boolean generatePassword, XmlDoc.Element shareable, String uploadId) throws Throwable {

//      secure.identity.token.create \
//          :tag sharable-567.1 \
//          :to 25-Mar-2021 23:59:00 \
//          :min-token-length 20 \
//          :max-token-length 20 \
//          :create-auth-key -min-key-length 20 -max-key-length 20 true \
//          :role -type role proj-test-1247.5.1:participant-a \
//          :role -type role ds:standard-user \
// 			:perm < :resource -type service asset.namespace.archive.create :access ACCESS > \
//		    :perm < :resource -type service asset.attribute.atomic.increment :access MODIFY > \
//			:service -name service.execute <
//              :service -name asset.namespace.archive.create -outputs 1 < \
//                  :namespace /projects/proj-test-1247.5.1/wilson/test-data \
//              	:for user \
//              	:format zip \
//              	:clevel 0 \
//              	:remove-path-prefix /projects/proj-test-1247.5.1/wilson/test-data \
//          	>
//              :service -name asset.attribute.atomic.increment < \
//      			:id path=/projects/proj-test-1247.5.1/wilson/test-data/upload_xxx/__manifest.csv \
//  				:name direct.download.attempt
//  			>
//  		>

        String shareableId = shareable.value("@id");
        String shareableNS = shareable.value("namespace");

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("tag", "shareable-" + shareableId + "." + uploadId);
        if (durationDays != null) {
            dm.add("to", "today+" + durationDays + "day");
        }
        dm.add("min-token-length", TOKEN_LENGTH);
        dm.add("max-token-length", TOKEN_LENGTH);
        if (generatePassword) {
            dm.add("create-auth-key", new String[] { "min-key-length", Integer.toString(PASSWORD_LENGTH),
                    "max-key-length", Integer.toString(PASSWORD_LENGTH) }, true);
        }

        String projectId = Projects.resolveProjectIdFromNSPath(shareableNS);
        if (projectId == null) {
            throw new Exception("failed to resolve project id from asset namespace path: " + uploadNS);
        }

        SvcFacilityShareableUploadCreate.validateProjectRoleNamespaceAccess(executor, uploadNS);

        String projectRole = Projects.getProjectRole(projectId, Project.ProjectRoleType.A);
        dm.add("role", new String[] { "type", "role" }, projectRole);
        dm.push("perm");
        dm.add("resource", new String[] { "type", "service" }, "asset.namespace.archive.create");
        dm.add("access", "ACCESS");
        dm.pop();
        dm.push("perm");
        dm.add("resource", new String[] { "type", "service" }, "asset.attribute.atomic.increment");
        dm.add("access", "MODIFY");
        dm.pop();
        dm.add("grant-caller-transient-roles", true);

        dm.push("service", new String[] { "name", "service.execute" });

        dm.push("service", new String[] { "name", "asset.namespace.archive.create", "outputs", "1" });
        dm.add("namespace", uploadNS);
        dm.add("for", "user");
        dm.add("format", "zip");
        dm.add("clevel", compress ? COMPRESSION_LEVEL : 0);
        dm.add("remove-path-prefix", uploadNS);
        dm.add("generate-statistics", false);
        dm.pop();

        dm.push("service", new String[] { "name", "asset.attribute.atomic.increment" });
        String manifestAssetPath = Manifest.getManifestAssetPath(uploadNS);
        dm.add("id", "path=" + manifestAssetPath);
        dm.add("name", Manifest.ATTRIBUTE_DIRECT_DOWNLOAD_ATTEMPT);
        dm.pop();

        dm.pop();

        XmlDoc.Element te = executor.execute("secure.identity.token.create", dm.root()).element("token");
        String serverHostAddr = Shareable.getServerHostAddress(executor);
        Date expiry = durationDays == null ? null : DateTime.parse("today+" + durationDays + "day");
        return new DownloadLink(te, serverHostAddr, compress, expiry, shareableId, uploadId, uploadNS);
    }

}
