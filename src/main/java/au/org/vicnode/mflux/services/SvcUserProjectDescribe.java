/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcUserProjectDescribe extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "user.project.describe";

	private Interface _defn;

	public SvcUserProjectDescribe() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("authority", StringType.DEFAULT, "The authentication authority for the user (only authorities thhat use the default protocol).",
				0, 1));
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain for the user.", 1, 1));
		_defn.add(new Interface.Element("user", StringType.DEFAULT, "The user name. Defaults to all.", 0, 1));
		_defn.add(new Interface.Element("include-disabled", BooleanType.DEFAULT,
				"Include disabled users (default to false).", 0, 1));
		Interface.Element role = new Interface.Element("role", new StringType(128),
				"Only users holding this role will be included.", 0, Integer.MAX_VALUE);
		role.add(new Interface.Attribute("type", new StringType(64), "Role type. Defaults to role", 0));
		_defn.add(role);

	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Describe projects that a user is a member of. ";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		String authority = args.value("authority");
		String domain = args.value("domain");
		String user0 = args.value("user");
		Boolean includeDisabled = args.booleanValue("include-disabled", false);
		XmlDoc.Element role = args.element("role");

		//
		Vector<String> users = new Vector<String>();

		if (user0==null) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("domain", domain);
			dm.add("size", "infinity");
			XmlDoc.Element r = executor().execute("user.list", dm.root());
			if (r!=null) {
				Collection<String> t = r.values("user");
				if (t!=null) {
					users.addAll(t);
				} 
			}
		} else {
			users.add(user0);
		}
		if (users!=null) {
			for (String user : users) {
				describe (executor(), authority, domain, user, 
						includeDisabled, role, w);
			}
		}
	}



	public  void describe (ServiceExecutor executor, String authority, String domain, String user,
			Boolean includeDisabled, XmlDoc.Element role, XmlWriter w) throws Throwable {

		// Describe user. We are going to assume all project roles
		// are held at the top level of this user.  This is invariably true.
		// Otherwise we have to iterate through all projects per user
		// which is too expensive.
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		dm.add("permissions", true);
		if (role!=null) {
			dm.add(role);
		}
		XmlDoc.Element r = executor.execute("user.describe", dm.root());
		if (r==null) {
			return;
		}
		// 
		XmlDoc.Element userMeta = r.element("user");
		if (userMeta==null) {
			return;
		}
		Boolean isEnabled = userMeta.booleanValue("@enabled", true);
		if (includeDisabled || (!includeDisabled&&isEnabled)) {
			
			w.push("user");
			w.add("domain", domain);
			w.add("user", user);
			
			// Date of creation
			String ctime = userMeta.value("asset/ctime");
			w.add("ctime", ctime);
			w.add("enabled", isEnabled);
			
			// Does this account have the UoM disable-ment meta-data
			XmlDoc.Element dbl = userMeta.element("asset/meta/unimelb-admin:user-account/disable");
			if (dbl!=null) {
				w.add(dbl);
			}
			Collection<XmlDoc.Element> userRoles = userMeta.elements("role");
			if (userRoles!=null) {

				// Find unique list of projects (a user might hold
				// multiple roles in a project - but should not)
				// Make hashmap of project-id and roles (comma delimitered)
				HashMap<String,String> roleMap = new HashMap<String,String>();
				for (XmlDoc.Element userRole : userRoles) {
					String actualRole = userRole.value();
					if (actualRole.startsWith("proj-")) {
						int idx = actualRole.indexOf(":");
						String projectID = actualRole.substring(0,idx);
						if (roleMap.containsKey(projectID) ) {
							String value = roleMap.get(projectID);
							value += ","+actualRole;
							roleMap.put(projectID, value);
						} else {
							roleMap.put(projectID,actualRole);
						}
					}
				}

				// OK now we have our hashmap with unique project IDs
				// Iterate through the project IDs and get some
				// information
				if (roleMap.keySet().size() > 0) {
					Collection<String> projectIDs = roleMap.keySet();
					for (String projectID : projectIDs) {
						w.push("project");
						w.add("project-id", projectID);
						w.add("roles", roleMap.get(projectID));
						try {
							addProjectDetails(executor, projectID, w);
						}catch(Throwable e){
							//Ignore
						}
						w.pop();
					}
				}
			}
			w.pop();
		}

	}

	private void addProjectDetails (ServiceExecutor executor, String projectID,
			XmlWriter w) throws Throwable {

		// Get project namespace meta-data
		XmlDoc.Element meta = Project.describeNameSpace(null, executor, projectID, false);
		String collectionDocType = Properties.getCollectionDocType(executor);
		XmlDoc.Element collection = meta.element("namespace/asset-meta/"+collectionDocType);
		if (collection!=null) {
			XmlDoc.Element key = collection.element("key");
			if (key!=null) {
				w.add(key);
			}
			XmlDoc.Element owner = collection.element("owner");
			if (owner!=null) {
				w.add(owner);
			}
		}
	}
}
