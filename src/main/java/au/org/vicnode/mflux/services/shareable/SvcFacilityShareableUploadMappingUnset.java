package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.LongType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcFacilityShareableUploadMappingUnset extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.mapping.unset";

    private Interface _defn;

    public SvcFacilityShareableUploadMappingUnset() {
        _defn = new Interface();
        _defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "The id of the upload shareable.", 1, 1));
        _defn.add(new Interface.Element("destroy-mapping-asset", BooleanType.DEFAULT,
                "Destroy the mapping asset. Defaults to false.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Clear the mapping rules and (optionally) destroy the mapping (asset) for the specified upload shareable.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String shareableId = args.value("shareable-id");
        XmlDoc.Element shareable = Shareable.describe(executor(), shareableId);
        boolean destroyMappingAsset = args.booleanValue("destroy-mapping-asset", false);
        UploadMapping.unset(executor(), shareable, destroyMappingAsset);
    }

}
