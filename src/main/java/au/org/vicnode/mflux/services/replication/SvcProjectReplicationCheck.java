/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.replication;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;
import au.org.vicnode.mflux.services.SvcProjectDescribe;

public class SvcProjectReplicationCheck extends PluginService {

	private static final int DEFAULT_RETRY_TIMES = 120;
	private static final int DEFAULT_RETRY_INTERVAL = 1000; // 1 second
	private static final int REP_SEND_SIZE = 500;
	private static final int SYNC_SIZE = 1000;
	private static Boolean allowPartialCID = true;

	private Interface _defn;

	public SvcProjectReplicationCheck()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace-path", StringType.DEFAULT, "Additional namespaces (which are not part of the projects structure such as the /avarchives) which should be checked. Parameters prod-type and rep-type are irrelevant to this parameter. Defaults to none.", 0, Integer.MAX_VALUE));
		SvcProjectDescribe.addProjectIDs(_defn, allowPartialCID);
		SvcProjectDescribe.addIgnoreProjectIDs(_defn);
		//
		Interface.Element ie = new Interface.Element("email", StringType.DEFAULT, "Email address to send report to. Defaults to no email. If an email is set, if no assets are found to report on, the email will still be sent with a message to that effect.", 0, 1);
		ie.add(new Interface.Attribute("subject", StringType.DEFAULT,
				"Subject of message. If not specified, a default is used.", 0));
		_defn.add(ie);
		//
		_defn.add(new Interface.Element("list-all", BooleanType.DEFAULT, "List the results from all projects. Defaults to just the ones that need attention.", 0, 1));
		_defn.add(new Interface.Element("debug", BooleanType.DEFAULT, "Write stuff in server log file. Defaults to false.", 0, 1));
		//
		ie = new Interface.Element("replication", BooleanType.DEFAULT, "Look for assets to replicate (includes ones for which the namespace has changed).  Defaults to true.  If you set an output client side file with e.g. :out file:/Users/nebk/ids.txt, and if you set -list true and some assets are found to list, the primary asset IDs are writtem to the file.  You can then use this,e.g. to destroy assets on the DR (e.g. asset.destroy :id rid=1128.1234", 0, 1);
		ie.add(new Interface.Attribute("list", BooleanType.DEFAULT, "List the asset IDs and sizes (and total size). Default is false.", 0));
		ie.add(new Interface.Attribute("collection-assets", BooleanType.DEFAULT, "When true (Default is false), sets these arguments of asset.replicate.to (when replicating) :collection-assets/{describe-queries,include-dynamic-members,include-static-members, Binclude-subcollections} to true.", 0));
		ie.add(new Interface.Attribute("clevel", IntegerType.DEFAULT, "Level of compression. 0 is uncompressed, 9 is the highest level. Defaults to 0.", 0));
		ie.add(new Interface.Attribute("send-size", IntegerType.DEFAULT, "When send-method=find-and-send-direct, and when actually replicating assets, pack this many assets into the piped replication at a time. Defaults to " + REP_SEND_SIZE, 0));		
		ie.add(new Interface.Attribute("versions", StringType.DEFAULT, "Replicate all ('all' - default) versions, or just the version matched by the query ('match')", 0));
		ie.add(new Interface.Attribute("send", BooleanType.DEFAULT,
				"Instead of just checking, actually replicate the assets. Defaults to false.", 0));
		ie.add(new Interface.Attribute("nb-threads", IntegerType.DEFAULT, "Number of threads to use when querying for assets to replicate and actually replicating assets. Defaults to 1", 0));
		ie.add(new Interface.Attribute("send-method",
				new EnumType(new String[] { "find-and-send-direct", "find-and-send-queue", "all-in-one"}),
				"The method to actually replicate the data. 'find-and-send-direct' (default) means find assets to replicate, then replicate them subsequently (will not scale [exceed memory] if huge numbers are found). 'find-and-send-queue' means put the found assets in the standard replication asset processing queue (element :queue) which must be configured to repliciate them. 'all-in-one' finds and replicates all in one multi-threaded service call.  Scales right up but less introspection is available.", 0));		
		_defn.add(ie);
		//
		ie = new Interface.Element("sync", BooleanType.DEFAULT, "Look for assets on the DR not on the primary (for potential destruction)? Defaults to false.", 0, 1);
		ie.add(new Interface.Attribute("destroy", BooleanType.DEFAULT,
				"Instead of just checking, actually destroys the assets on the DR server. Defaults to false.", 0));
		ie.add(new Interface.Attribute("size",IntegerType.DEFAULT, "Limit loop that looks for replica assets to delete to this number of assets per iteration (if too large, the VM may run out of virtual memory).  Defaults to 5000.", 0));
		_defn.add(ie);
		//
		_defn.add(new Interface.Element("where",StringType.DEFAULT, "Query predicate to further restrict (for the given project[s])  the selected assets on the local host.", 0, 1));
		_defn.add(new Interface.Element("use-indexes", BooleanType.DEFAULT, "Turn on or off the use of indexes in the query. Defaults to true.", 0, 1));
		//
		SvcProjectDescribe.addProdType (_defn);
		_defn.add(new Interface.Element("rep-type", new EnumType(Properties.repTypes),
				"Select from 'all', (all projects), 'replication' (default;only projects for which the namespace meta-data indicates should be replicated), 'no-replication' (projects for which the namespace meta-data indicates should not be replicated).",
				0, 1));	
		_defn.add(new Interface.Element("format", new EnumType(
				new String[] { "aarv2", "aar", "tar", "tar.gz", "zip" }),
				"The format of the container used to transmit the data. 'zip' is ZIP format, 'tar' is Tar format (.tar) when no compression, or G-Zipped Tar (.tar.gz) when clevel>0, 'aarv2' is the non-concurrent Arictecta format, 'aar' is the latest Arcitecta archive format (.aar) with file size limit of 2^63 per file. The default is 'aarv2'.", 0, 1));		

		//
		ie = new Interface.Element("namespace", BooleanType.DEFAULT, "Look for assets on the DR that need to be moved into a new namespace (tracking what happened on the primary)? Defaults to false.", 0, 1);
		ie.add(new Interface.Attribute("move", BooleanType.DEFAULT,
				"Instead of just checking, actually moves the assets on the DR server into the correct namespaces. Defaults to false.", 0));
		ie.add(new Interface.Attribute("list", BooleanType.DEFAULT,
				"List any namespaces that differ. Defaults to false.", 0));
		ie.add(new Interface.Attribute("list-all", BooleanType.DEFAULT,
				"List all namespaces whether they differ or not. Defaults to false.", 0));
		ie.add(new Interface.Attribute("throw-on-null-namespace", BooleanType.DEFAULT,
				"Sometimes remote asset gets return null and the default is to terminate (true) the process.  Set to false to continue and skip these assets. Defaults to false.", 0));
		ie.add(new Interface.Attribute("nb-threads", IntegerType.DEFAULT,
				"Number of threads to run in parallel. Defaults to 1.", 0));
		_defn.add(ie);

	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Checks projects for:  1) have all data (except that which is in staging state) been replicated, 2) is there data on the DR not on the primary (possibly for destruction) and 3) have there been namespace moves that need to be tracked. Projects are processed sequentially.  Depends on unimelb-essentials services package being installed.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.replication.check";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}


	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		Collection<String> theProjectIDs = args.values("project-id");
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs!=null) {
			for (String theProjectID : theProjectIDs) {
				String [] t = Project.validateProject(executor(), theProjectID, allowPartialCID);
				projectIDs.add(t[1]);
			}
		} else{
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) {
			return;
		}
		//		
		Collection<String> namespacePaths = args.values("namespace-path");
		Boolean listAll = args.booleanValue("list-all",false);
		XmlDoc.Element repl = args.element("replication");
		XmlDoc.Element sync = args.element("sync");
		//
		Boolean debug = args.booleanValue("debug", false);
		//
		Collection<String> theIgnores = args.values("ignore");
		XmlDoc.Element email = args.element("email");
		String repType = args.stringValue("rep-type",  Properties.repTypes[1]);
		String prodType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		String keyString = args.value("contains");
		if (keyString!=null) keyString = keyString.toUpperCase();
		String where = args.value("where");
		String format = args.stringValue("format", "aarv2");
		Boolean useIndexes = args.booleanValue("use-indexes", true);
		XmlDoc.Element moveNS = args.element("namespace");

		// List of ignores
		Vector<String> ignores = new Vector<String>();
		if (theIgnores!=null) {
			for (String theIgnore : theIgnores) {
				String t[] = Project.validateProject(executor(), theIgnore, true);
				ignores.add(t[1]);
			} 
		}


		// Iterate over projects. It can be rather expensive to do this because
		// we re-evaluate the time predicates for each project.
		String today = DateUtil.todaysTime(0);
		XmlDocMaker dm = new XmlDocMaker("args");
		Long totalSize = 0L;
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);

			// FInd replication  asset processing queue
			String qName = ProjectReplicate.isProjectReplicated(ph);

			// We AND the type and ignore checks.
			if (Project.keepOnProjectType(ph, prodType) &&
					Project.keepOnRepType(executor(), qName, repType) &&
					Project.keepOnIgnoreList(ph.id(), ignores) &&
					Project.keepOnKeyString(ph.id(), keyString)) {
				if (debug) {
					System.out.println("Keeping Project " + projectID);
				}
				XmlDocMaker dmp = new XmlDocMaker("args");
				Boolean some = checkProject (debug, ph, where, 
						repl, sync, moveNS, useIndexes, format, dmp);
				if (some || listAll) {
					dm.add(dmp.root(),false);
				}
				XmlDoc.Element projectSize = dmp.root().element("project/replication/total-size");
				if (projectSize!=null) {
					Long t = projectSize.longValue();
					totalSize += t;
				}
			} else {
				if (debug) {
					System.out.println("Dropping Project " + projectID);
				}

			}
			PluginTask.checkIfThreadTaskAborted();
		}


		// Iterate over addition namespaces
		if (namespacePaths!=null) {
			for (String namespacePath :namespacePaths) {
				String namespacePathDR = ProjectReplicate.makeNameSpaceNameForDR(executor(), namespacePath);
				XmlDocMaker dmp = new XmlDocMaker("args");
				Boolean some = checkNameSpacePath (executor(), debug, namespacePath, namespacePathDR, where, 
						repl, sync, moveNS, useIndexes, format, dmp);
				if (some || listAll) {
					dm.add(dmp.root(),false);
				}
			}
		}

		// Report to caller
		w.add(dm.root(), false);

		// We know the units that come from unimelb.replicate.check are bytes.
		if (totalSize>0) {
			if (repl.booleanValue("replication", false)) {
				w.add("projects-total-size-to-replicate", new String[] {"units", "bytes"}, totalSize);
			} else {
				w.add("projects-total-size-replicated", new String[] {"units", "bytes"}, totalSize);
			}
		}

		// Email
		if (email!=null) {
			String msg = XmlPrintStream.outputToString(dm.root());
			if (msg==null || (msg!=null && msg.isEmpty())) {
				msg = "No assets found to action";
			}
			String subject = email.value("@subject");
			if (subject==null) {
				subject = "[" + Util.serverUUID(executor()) + "]" +
						" Mediaflux Project Replication Summaries (date=" + today + ", where = " + where +")";
			}
			ArrayList<String> emails = new ArrayList<String>();
			emails.add(email.value());
			String from = Util.getServerProperty(executor(), "notification.from");
			MailHandler.sendMessage (executor(), emails, subject, msg, null, false, from, w);
		}
		
		if (outputs != null) {
			// These are the primary asset ids from the replication check
			// The primary asset ids from the namespace move check can be found
			// in project/namespace-move/asset/id. However, removing the replica
			// of a non-moving asset does not generally solve the issue (it's
			// what is already there in the way that matters) so currently
			// these are not exported.
			Collection<String> ids = dm.root().values("project/replication/id");
			Util.writeClientSideFile(outputs.output(0), ids);
		}
	}



	private Boolean  checkProject (Boolean debug, ProjectHolder ph, String where, 
			XmlDoc.Element repl, XmlDoc.Element sync, XmlDoc.Element moveNS,
			Boolean useIndexes, String format, XmlDocMaker dm) throws Throwable {		

		String assetNameSpace = ph.nameSpace();
		String assetNameSpaceDR = ph.nameSpaceDR();
		dm.push("project", new String[]{"id", ph.id()});
		Boolean some = checkNameSpace (ph.executor(), debug, assetNameSpace, assetNameSpaceDR, 
				where, repl, sync, moveNS, useIndexes, format, dm);
		dm.pop();
		return some;
	}

	private Boolean  checkNameSpacePath (ServiceExecutor executor, Boolean debug, String assetNameSpace, 
			String assetNameSpaceDR,  String where, 
			XmlDoc.Element repl, XmlDoc.Element sync, XmlDoc.Element moveNS,
			Boolean useIndexes, String format,  XmlDocMaker dm) throws Throwable {		

		dm.push("namespace", new String[]{"path", assetNameSpace});
		Boolean some = checkNameSpace (executor, debug, assetNameSpace, assetNameSpaceDR,  where, 
				repl, sync, moveNS, useIndexes, format, dm);
		dm.pop();
		return some;
	}


	private Boolean  checkNameSpace (ServiceExecutor executor, Boolean debug, String assetNameSpace, String assetNameSpaceDR, String where, 
			XmlDoc.Element repl, XmlDoc.Element sync, XmlDoc.Element moveNS,
			Boolean useIndexes, String format, XmlDocMaker dm) throws Throwable {		

		Boolean someR = false;
		Boolean someS = false;
		Boolean someM = false;

		// Have all data been replicated
		if (repl==null || (repl!=null && repl.booleanValue())) {
			if (debug) {
				System.out.println("   Starting replication check");
			}
			PluginTask.checkIfThreadTaskAborted();
			Boolean send = false;
			Boolean collectionAssets = false;
			String versions = "match";   
			Integer nbThreads = 1;
			Integer clevel = 0;
			Integer sendSize = REP_SEND_SIZE;
			Boolean list = false;
			String sendMethod = "find-and-send-direct";
			String queueName = ProjectReplicate.getProjectStandardQueueName(executor);

			if (repl!=null) {
				send = repl.booleanValue("@send", false);
				//
				collectionAssets = repl.booleanValue("@collection-assets", false);
				// Send all versions or just that matching query predicate
				versions = repl.stringValue("@versions", "all"); 
				nbThreads = repl.intValue("@nb-threads", 1);
				sendSize = repl.intValue("@send-size", REP_SEND_SIZE);
				clevel = repl.intValue("@clevel", 0);
				list = repl.booleanValue("@list", false);
				sendMethod = repl.stringValue("@send-method","find-and-send-direct");
			}
			if (send&&sendMethod.equals("all-in-one")) {
				if (debug) {
					System.out.println("   Starting new find/send using asset.replicate.to");
				}
				//
				// In this new approach of sending we don't use unimelb.replicate.check
				// Instead asset.replicate.to is asked to check all the assets
				// in  the predicate and send them and in parallel
				// We give up the list option in this approach which 
				// is fine until something doesn't work!
				someR = replicationCheckAllInOne (executor(), assetNameSpace, where, 
						collectionAssets, versions, nbThreads, 
						clevel, debug, useIndexes, format, dm);
			} else {
				if (debug) {
					System.out.println("   Starting old find/send using unimelb.replicate.check");
				}
				someR = replicationCheck (executor(), sendMethod, queueName,
						assetNameSpace, where, send, collectionAssets, 
						versions, nbThreads, clevel, list, debug, 
						useIndexes, format, sendSize, dm);
			}
		}

		// Do data need to be synchronized
		if (sync!=null && sync.booleanValue()) {
			if (debug) {
				System.out.println("   Starting synchronisation check");
			}
			PluginTask.checkIfThreadTaskAborted();
			Boolean destroy = sync.booleanValue("@destroy", false);
			Integer size = sync.intValue("@size", SYNC_SIZE);
			someS = synchronizationCheck (executor(), assetNameSpaceDR,  where, destroy, 
					debug, useIndexes, size, dm);
		}

		// Do data need to be moved
		//		System.out.println("moveNS="+moveNS);
		//		System.out.println("moveNS.value="+moveNS.booleanValue());
		if (moveNS!=null && moveNS.booleanValue()) {
			if (debug) {
				System.out.println("   Starting namespace move check");
			}
			PluginTask.checkIfThreadTaskAborted();
			Boolean move = moveNS.booleanValue("@move", false);
			Integer nbThreads = moveNS.intValue("@nb-threads", 1);
			Boolean throwOnNull = moveNS.booleanValue("@throw-on-null-namespace", true);
			Boolean listPaths = moveNS.booleanValue("@list", false);
			Boolean listAllPaths = moveNS.booleanValue("@list-all", false);

			someM = moveCheck (executor(), assetNameSpace, where, move, 
					nbThreads, throwOnNull, listPaths,listAllPaths, debug, dm);
		}



		return (someR || someS || someM);
	}


	private Boolean replicationCheck (ServiceExecutor executor, String sendMethod,
			String queueName, String assetNameSpace, String where, 
			Boolean send, Boolean collectionAssets, String versions, 
			Integer nbThreads, Integer clevel, Boolean list, Boolean debug, 
			Boolean useIndexes, String format, Integer sendSize, XmlDocMaker w) throws Throwable {	
		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("replication");
		//		
		XmlDocMaker dm = new XmlDocMaker("args");
		String query = "namespace>='"+assetNameSpace+"'";
		if (where!=null) {
			query += " and (" + where + ")";
		}
		dm.add("where", query);
		// Add the standard replication filter 
		dm.add("where", ProjectReplicate.REPLICATION_FILTER);
		dm.add("peer", ProjectReplicate.DRServerPeerName(executor));
		dm.add("debug", debug);
		dm.add("use-indexes", useIndexes);
		dm.add("list", list);
		dm.add("send-size", sendSize);
		dm.add("versions", versions);
		dm.add("nb-threads", nbThreads);
		dm.add("clevel", clevel);
		dm.add("collection-assets", collectionAssets);
		if (send) {
			dm.add("dst", Util.serverUUID(executor));             // Destination NS is server UUID
			dm.add("send", "true");
		}
		dm.add("format", format);
		dm.add("send-method", sendMethod);
		if (sendMethod.equals("find-and-send-queue")) {
			if (queueName==null) {
				throw new Exception("The replication queue name is unexpectedly null");
			}
			dm.add("queue", queueName);
		}
		XmlDoc.Element r = executor.execute("unimelb.replicate.check", dm.root());
		int nReplicate = 0;
		boolean someFailed = false;
		if (r!=null) {
			nReplicate = r.intValue("total-to-replicate", 0);
			dmOut.addAll(r.elements());
			// These were failures to fetch rstate meta-data from peer
			someFailed = r.elementExists("total-peer-failed");
		}
		dmOut.pop();
		if (nReplicate>0 || someFailed) {
			w.addAll(dmOut.root().elements());
		}
		return (nReplicate>0);
	}


	private Boolean replicationCheckAllInOne (ServiceExecutor executor, String assetNameSpace, 
			String where, Boolean collectionAssets, String versions, 
			Integer nbThreads, Integer clevel,  Boolean debug, 
			Boolean useIndexes, String format, XmlDocMaker w) throws Throwable {	

		// TBD. I think this is the same as unimelb.replicate.check :send-method all-in-one true
		// If so, call that instead of duplicating code here.
		// The parsing of the results would need to be made consistent

		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("replication");

		// We can now do all checking, version iteration and sending directly
		// with asset.replicate.to
		XmlDocMaker dm = new XmlDocMaker("args");	
		dm.add("check-if-required", true);
		String query = "namespace>='"+assetNameSpace+"' and " + ProjectReplicate.REPLICATION_FILTER;
		if (where!=null) {
			query += " and (" + where + ")";
		}
		dm.add("where", query);
		dm.add("nb-threads", nbThreads);
		dm.add("format", format);
		dm.add("cmode", "push");
		dm.add("dst", Util.serverUUID(executor));             // Destination NS is server UUID
		dm.push("peer");
		dm.add("name", ProjectReplicate.DRServerPeerName(executor));
		dm.pop();
		dm.add("related", "0");
		dm.add("update-doc-types", true);
		dm.add("update-models", false);
		dm.add("allow-move", true);
		dm.add("content-must-have-checksum", true);     // Staged assets will have no checksum
		dm.add("if-staging", "skip");                   // Default is error
		dm.add("locally-modified-only", false);         // Allows us to replicate foreign assets (that are already replicas on our primary)
		dm.add("versions", versions);
		dm.add("auto-name", true);
		dm.add("overwrite", false);
		dm.add("onerror", "report");
		// These 2 added in 4.9.033
		// May like to add control for these
		dm.add("restore-missing-content", true);
		dm.add("restore-mismatched-content", new String[] {"validate", "true"}, false);		

		dm.add("clevel", clevel);
		//
		dm.add("include-destroyed", false);
		if (collectionAssets) {
			dm.push("collection-assets");
			dm.add("describe-queries", true);
			dm.add("include-dynamic-members", true);
			dm.add("include-static-members", true);
			dm.add("include-subcollections", true);
			dm.pop();
		}

		// Force a peer check with retries
		int nRetry = checkPeerStatus (executor, ProjectReplicate.DRServerPeerName(executor));
		if (debug) {
			if (nRetry<0) {
				System.out.println("replicateAssetsNew - " + (-1*nRetry) + " peer checks were made but the peer was not established");
			} else {
				System.out.println("replicateAssetsNew - " + nRetry + " peer checks were made before the peer was established");
			}
		}
		// Do the replication
		XmlDoc.Element r = executor.execute("asset.replicate.to", dm.root());

		// Parse result
		XmlDoc.Element count = r.element("peer/count");
		if (count!=null) {
			int nSent = count.intValue();
			int nReplicated = count.intValue("@updated");
			int nNotReplicated = nSent - nReplicated;
			int nFailed = 0;
			if (r!=null) {
				XmlDoc.Element errors = r.element("errors");
				if (errors!=null) {
					nFailed = errors.intValue("fail");
				}
			}
			dmOut.add("nsent", 
					new String[] {"replicated", ""+nReplicated, "not-replicated", ""+nNotReplicated, "failed", ""+nFailed}, nSent);
			dmOut.pop();
			if (nSent>0 || nFailed>0) {
				w.addAll(dmOut.root().elements());
			}

			if (nSent>0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}



	private Boolean synchronizationCheck (ServiceExecutor executor, String assetNameSpace, String where, Boolean destroy, 
			Boolean debug, Boolean useIndexes, Integer size, XmlDocMaker w) throws Throwable {
		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("synchronize");
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		String query = "namespace>='"+assetNameSpace+"'";
		if (where!=null) {
			query += " and (" + where + ")";
		} 
		dm.add("where", query);
		dm.add("peer", ProjectReplicate.DRServerPeerName(executor));
		dm.add("size", size);
		dm.add("debug", debug);
		dm.add("use-indexes", useIndexes);
		if (destroy) {
			dm.add("destroy", "true");
		} else {
			dm.add("destroy", "false");
		}
		XmlDoc.Element r = executor.execute("unimelb.replicate.synchronize", dm.root());
		int nT = 0;
		if (r!=null) {
			Collection<XmlDoc.Element> ids = r.elements("id");
			if (ids!=null) nT = ids.size();
			if (destroy) {
				//COunt them up in case of a failure on some assets
				int nD = 0;
				if (ids!=null){
					for (XmlDoc.Element id : ids) {
						if (id.booleanValue("@destroyed")) nD++;
					}
				}
				dmOut.add("total-assets-to-destroy", nT);
				dmOut.add("total-assets-destroyed", nD);
			} else {
				dmOut.add("total-assets-to-destroy", nT);
			}
		} else {
			dmOut.add("total-assets-to-destroy", 0);
			if (destroy) {
				w.add("total-assets-destroyed", 0);
			}
		}
		dmOut.pop();
		if (nT>0) {
			w.addAll(dmOut.root().elements());
		}
		return (nT>0);
	}



	// This version does not iterate through the results with a cursor.
	// It calls unimelb.replciate.namespace.check with minimal outputs
	// so that we minimize the chance of exceeding virtual memory.
	private Boolean moveCheck (ServiceExecutor executor, String assetNameSpace,  String where, 
			Boolean move, Integer nbThreads,  Boolean throwOnNull, 
			Boolean listPaths, Boolean listAllPaths, Boolean debug, 
			XmlDocMaker w) throws Throwable {

		Integer nTotalMoved = 0;
		Integer nTotalToMove = 0;
		Integer nTotalErrors = 0;
		Integer nList = 0;

		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("namespace-move");

		String localUUID = Util.serverUUID(executor);

		//
		XmlDocMaker dm = new XmlDocMaker("args");
		String query = "namespace>='"+assetNameSpace+"'";
		if (where!=null) {
			query += " and (" + where + ")";
		}
		dm.add("where", query);

		// The service can take a single id.  With more than about 3 threads, we get greater
		// efficiency with a query and pipe than just calling the service directly.
		dm.add("action", "pipe");
		dm.push("service", new String[] {"name", "unimelb.replicate.path.check"});
		dm.add("peer", ProjectReplicate.DRServerPeerName(executor));
		//dm.add("debug", debug);
		dm.add("dst", "/"+localUUID);
		dm.add("move", move);
		dm.add("total", false);   // don't list every asset counted
		if (listPaths||listAllPaths) {
			dm.add("list-paths", new String[] {"all",listAllPaths.toString()}, true);
		}
		dm.pop();
		dm.add("pipe-nb-threads", nbThreads);
		dm.add("pipe-generate-result-xml", true);
		XmlDoc.Element r = executor.execute("asset.query", dm.root());

		// Each service call generates 
		// total-checked (mandatory), total-to-move (mandatory), 
		// total-moved (optional), total-errors (optional) set of results.
		// Each one will be for one asset only.
		// So we have to count them up (which takes time!)
		// If you have too many assets, you can exceed the JVM
		// memory in returning a huge list so there is also a cursor 
		// version of this function (although it gets slower as idx increases)
		if (r!=null) {

			// This is present only if the asset actually needs to be moved
			// but is not actually moved
			Collection<String> totalToMove = r.values("total-to-move");
			if (totalToMove!=null) {
				nTotalToMove = totalToMove.size();
				dmOut.add("total-to-move", nTotalToMove);
			}		

			// This is present only if the asset is actually moved
			Collection<String> totalMoved = r.values("total-moved");
			if (totalMoved!=null) {
				nTotalMoved = totalMoved.size();
				dmOut.add("total-moved", nTotalMoved);
			}		

			// This is present only if move requested and failed
			Collection<String> totalErrors = r.values("total-errors");
			if (totalErrors!=null) {
				nTotalErrors = totalErrors.size();
				dmOut.add("total-errors", nTotalErrors);
			}

			Collection<XmlDoc.Element> assets = r.elements("asset");
			if (assets!=null) {
				dmOut.addAll(assets);
				nList = assets.size();
			}

		}
		dmOut.pop();

		//
		Collection<XmlDoc.Element> t = dmOut.root().elements();
		if (nTotalToMove>0 || nTotalMoved>0 || nTotalErrors>0 || nList>0) {
			if (t!=null) {
				w.addAll(dmOut.root().elements());
			}
			return true;
		}

		return false;

	}


	private int checkPeerStatus (ServiceExecutor executor, String peerName) throws Throwable {
		for (int i=0;i<DEFAULT_RETRY_TIMES;i++) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("name", peerName);
			dm.add("check-peer-is-reachable", true);
			XmlDoc.Element r = executor.execute("server.peer.status", dm.root());
			if (r!=null) {
				String status = r.value("peer/status");
				if (status!=null && status.equals("reachable")) {
					return i+1;
				}
			}
			Thread.sleep(DEFAULT_RETRY_INTERVAL);
		}
		return -1*DEFAULT_RETRY_TIMES;
	}



}
