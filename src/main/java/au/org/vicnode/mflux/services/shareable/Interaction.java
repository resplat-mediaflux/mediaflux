package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class Interaction {

    public static final String INTERACTION_NAME = "facility-data-delivery";
    public static final String INTERACTION_TITLE = "Facility Data Delivery";

    public static final String URL_PATH = "maw/";
    public static final String PARAM_APP_NAME = "app";
    public static final String PARAM_APP_VALUE = "user-interaction";
    public static final String PARAM_UIT_NAME = "uit";

    public static final String ARG_COPY_TO_PROJECT = "copy-to-project";

    public static final String DESTINATION_ROOT = "/projects";
    public static final String INPUT_CONSTANT_FACILITY_NAME = "facility-name";
    public static final String INPUT_CONSTANT_FACILITY_NOTIFICATION_TO = "facility-notification-to";
    public static final String INPUT_CONSTANT_FACILITY_NOTIFICATION_EMAIL_SUBJECT_SUFFIX = "facility-notification-email-subject-suffix";

    public final String id;
    public final String token;
    public final String url;
    public final Date expiresAt;

    Interaction(String id, String token, String url, Date expiresAt) {
        this.id = id;
        this.token = token;
        this.url = url;
        this.expiresAt = expiresAt;
    }

    static String userInteractionName(String uploadShareableId, String uploadId) {
        return String.format("%s-%s.%s", INTERACTION_NAME, uploadShareableId, uploadId);
    }

    static boolean userInteractionExists(ServiceExecutor executor, String uploadShareableId, String uploadId)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("name", userInteractionName(uploadShareableId, uploadId));
        return executor.execute("automation.user.interaction.exists", dm.root()).booleanValue("exists");
    }

    static Interaction createUserInteraction(ServiceExecutor executor, String shareableId, String uploadId,
                                             String assetNS, Integer durationDays, String ifexists, String notify, String emailSubjectSuffix)
            throws Throwable {

        XmlDoc.Element shareable = Shareable.describe(executor, shareableId);
        String facilityName = Shareable.getFacilityName(shareable);
        String facilityNotificationTo = Shareable.getFacilityNotificationTo(shareable);

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("name", userInteractionName(shareableId, uploadId));
        dm.add("title", INTERACTION_TITLE);
        dm.add("description", "Copy facility/instrument data to user's Mediaflux project.");
        dm.add("input-constant", new String[]{"name", "shareable-id", "secret", "false"}, shareableId);
        dm.add("input-constant", new String[]{"name", "upload-id", "secret", "false"}, uploadId);
        dm.add("input-constant", new String[]{"name", "upload-ns", "secret", "false"}, assetNS);
        if (facilityName != null) {
            dm.add("input-constant", new String[]{"name", INPUT_CONSTANT_FACILITY_NAME, "secret", "false"},
                    facilityName);
        }
        if (facilityNotificationTo != null) {
            dm.add("input-constant",
                    new String[]{"name", INPUT_CONSTANT_FACILITY_NOTIFICATION_TO, "secret", "false"},
                    facilityNotificationTo);
        }

        if (emailSubjectSuffix != null) {
            dm.add("input-constant", new String[]{"name", INPUT_CONSTANT_FACILITY_NOTIFICATION_EMAIL_SUBJECT_SUFFIX,
                    "secret", "false"}, emailSubjectSuffix);
        }


        dm.add("input-constant", new String[]{"name", "collection.navigator." + ARG_COPY_TO_PROJECT}, "write");

        dm.push("field-definitions");

        dm.push("element", new String[]{"name", ARG_COPY_TO_PROJECT, "label", "Destination path", "type",
                "enumeration", "min-occurs", "1", "max-occurs", "1"});
        dm.add("description", "Browse to select the destination path where the data is copied to.");
        dm.push("restriction", new String[]{"base", "enumeration"});
        dm.add("value", DESTINATION_ROOT);
        dm.pop();
        dm.pop();

        dm.pop();

        dm.add("option", new String[]{"name", "cancel", "label", "Cancel", "description", "Cancels the operation"});

        dm.push("option",
                new String[]{"name", "copy", "label", "Copy", "description", "Copy the data to your project."});
        dm.push("service", new String[]{"name", "asset.shareable.upload.interaction.complete"});
        dm.add("function",
                "shell.to.xml(string.format(':id " + shareableId + " :upload-id " + uploadId +
                        " :interaction-id %s :args < :upload-ns " + assetNS + " :copy-to-project %s :ifexists " + ifexists + " :notify " + notify + " >', "
                        + "xvalue('id'), xvalue('form/field[@name=\\'" + ARG_COPY_TO_PROJECT + "\\']')))");
        dm.pop();
        dm.pop();

        dm.push("validation-service",
                new String[]{"name", SvcFacilityShareableUploadInteractionValidate.SERVICE_NAME});
        dm.add("function",
                "shell.to.xml(string.format(':shareable-id %s :upload-id %s :interaction-id %s :interaction-token %s :args < :copy-to-project %s >', xvalue('input-vars/shareable-id'), xvalue('input-vars/upload-id'), xvalue('id'), xvalue('token'), xvalue('form/field[@name=\\'copy-to-project\\']')))");
        dm.pop();
        if (durationDays != null) {
            dm.add("expires-at", "today+" + durationDays + "day");
        }
        String interactionId = executor.execute("automation.user.interaction.create", dm.root())
                .value("user-interaction/@id");

        XmlDoc.Element interaction = describe(executor, interactionId);
        String token = interaction.value("@token");
        Date expiresAt = interaction.dateValue("expires-at", null);
//        String url = String.format("%s/%s?%s=%s&%s=%s", Shareable.getServerHostAddress(executor), URL_PATH, PARAM_APP_NAME,
//                PARAM_APP_VALUE, PARAM_UIT_NAME, token);
        String url = url(Shareable.getServerHostAddress(executor), token);

        return new Interaction(interactionId, token, url, expiresAt);
    }

    private static String url(String serverHostAddr, String interactionToken) {
        return String.format("%s/%s?%s=%s&%s=%s", serverHostAddr, URL_PATH, PARAM_APP_NAME,
                PARAM_APP_VALUE, PARAM_UIT_NAME, interactionToken);
    }

    public static String url(ServiceExecutor executor, String interactionId) throws Throwable {
        String serverHostAddr = Shareable.getServerHostAddress(executor);
        XmlDoc.Element interaction = describe(executor, interactionId);
        String interactionToken = interaction.value("@token");
        return url(serverHostAddr, interactionToken);
    }

    static Interaction createUserInteraction(ServiceExecutor executor, String shareableId, String uploadId,
                                             String assetNS, XmlDoc.Element uploadArgs, XmlDoc.Element shareOptions) throws Throwable {
        Integer durationDays = shareOptions.intOrNullValue("duration");
        String ifexists = shareOptions.stringValue("interaction/ifexists", "ignore");
        String notify = shareOptions.stringValue("interaction/notify", "none");
        String emailSubjectSuffix = uploadArgs == null ? null : uploadArgs.value(Shareable.ARG_EMAIL_SUBJECT_SUFFIX);
        return createUserInteraction(executor, shareableId, uploadId, assetNS, durationDays, ifexists, notify,
                emailSubjectSuffix);
    }

    static XmlDoc.Element describe(ServiceExecutor executor, String userInteractionId) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", userInteractionId);
        return executor.execute("automation.user.interaction.describe", dm.root()).element("user-interaction");
    }

    static String getFacilityName(XmlDoc.Element interaction) throws Throwable {
        return interaction.value("input-constants/value[@name='" + INPUT_CONSTANT_FACILITY_NAME + "']");
    }

    static String getFacilityNotificationTo(XmlDoc.Element interaction) throws Throwable {
        return interaction.value("input-constants/value[@name='" + INPUT_CONSTANT_FACILITY_NOTIFICATION_TO + "']");
    }

    static String getFacilityNotificationEmailSubjectSuffix(XmlDoc.Element interaction) throws Throwable {
        return interaction.value(
                "input-constants/value[@name='" + INPUT_CONSTANT_FACILITY_NOTIFICATION_EMAIL_SUBJECT_SUFFIX + "']");
    }

    static Collection<String> getFacilityNotificationRecipients(XmlDoc.Element interaction) throws Throwable {
        return Emails.parse(getFacilityNotificationTo(interaction));
    }

}
