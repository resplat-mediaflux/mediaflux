package au.org.vicnode.mflux.services.datamover;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.Properties;

public class Utils {

	public static String MANIFEST_DOCTYPE = "unimelb:shareable-upload-manifest";
	private static String NOTIFICATION_CHILD_PATH = "/notification/data-mover-expiry";

	/**
	 * Find all manifest assets recursively under the given namespace and return the
	 * asset meta-data for each
	 * 
	 * @param executor
	 * @param sr                ServerRoute (null for local host)
	 * @param namespace
	 * @param excludeChildPaths - exclude these child paths. "." means exclude the
	 *                          parent namespace itself (only), otherwise, all
	 *                          exclude paths are child paths of "namespace"
	 * @return
	 * @throws Throwable
	 */
	public static Collection<XmlDoc.Element> findManifestAssets(ServiceExecutor executor, ServerRoute sr,
			String namespace, Collection<String> excludeChildPaths) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("size", "infinity");
		dm.add("action", "get-meta");
		if (excludeChildPaths == null) {
			dm.add("where", "namespace>='" + namespace + "'");
		} else {
			String t = "namespace>='" + namespace + "'";
			for (String excludeChildPath : excludeChildPaths) {
				if (excludeChildPath.equals(".")) {
					t += "and not(namespace='" + namespace + "')";
				} else {
					t += "and not(namespace>='" + namespace + "/" + excludeChildPath + "')";
				}
				dm.add("where", t);
			}
		}

		// Second where clause for manifest asset details
		dm.add("where", "name='__manifest.csv' and " + MANIFEST_DOCTYPE + " has value");

		XmlDoc.Element r = null;
		if (sr == null) {
			r = executor.execute("asset.query", dm.root());
		} else {
			r = executor.execute(sr, "asset.query", dm.root());
		}
		if (r != null) {
			return r.elements("asset");
		} else {
			return null;
		}
	}

	/**
	 * Fetch the DM expiry notification meta-data from the project asset namespace
	 * meta-data (full document)
	 * 
	 * @param executor
	 * @param nsMeta
	 * @return
	 * @throws Throwable
	 */
	public static XmlDoc.Element getNotificationMetaData(ServiceExecutor executor, XmlDoc.Element nsMeta)
			throws Throwable {
		return nsMeta
				.element("namespace/asset-meta/" + Properties.getProjectDocType(executor) + NOTIFICATION_CHILD_PATH);
	}

	/**
	 * Extract the date the data were acquired by parsing the date from the
	 * transactional namespace name
	 * 
	 * @param transactionalNameSpace
	 * @return
	 * @throws Throwable
	 */
	public static Date getAcquiredDate(String transactionalNameSpace) throws Throwable {
		int idx = transactionalNameSpace.lastIndexOf("/");
		String str = transactionalNameSpace.substring(idx + 1, idx + 11);
		return DateUtil.dateFromString(str, "YYYY-MM-dd");
	}

	/**
	 * Extract the expiry date of the download links from the manifest asset
	 * meta-data
	 * 
	 * @param meta The manifest asset meta-data for document type
	 *             unimelb:shareable-upload-manifest
	 * @return
	 * @throws Throwable
	 */
	public static Date getExpiryDate(XmlDoc.Element meta) throws Throwable {
		XmlDoc.Element expiry = meta.element("upload/share/duration");
		return expiry.dateValue("@expiry");
	}
}
