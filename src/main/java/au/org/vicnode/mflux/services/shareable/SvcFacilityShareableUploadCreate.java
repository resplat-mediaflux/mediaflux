package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.*;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

/**
 * @author wliu5
 */
public class SvcFacilityShareableUploadCreate extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.create";

    private final Interface _defn;

    public SvcFacilityShareableUploadCreate() {

        _defn = new Interface();

        addArgDefns(_defn, false);

        // root namespace (once set, cannot be modified)
        _defn.add(new Interface.Element("namespace", StringType.DEFAULT,
                "The parent asset namespace to receive uploaded data.", 1, 1));

        Interface.Element emailToken = new Interface.Element("email-token", XmlDocType.DEFAULT,
                "Specifies who to email the shareable token to.", 0, 1);
        emailToken.add(new Interface.Element("facility", BooleanType.DEFAULT,
                "Send to the upload shareable token and instructions to facility contacts specified in property: '"
                        + Shareable.PROPERTY_FACILITY_NOTIFICATION_TO + "'. Defaults to false.",
                0, 1));
        emailToken.add(new Interface.Element("to", StringType.DEFAULT,
                "Send the upload shareable token and instructions to this email address.", 0, Integer.MAX_VALUE));
        _defn.add(emailToken);

        _defn.add(new Interface.Element("allow-system-admin", BooleanType.DEFAULT,
                "Allow system-administrator to create upload shareable. Defaults to false.", 0, 1));

    }

    static void addArgDefns(Interface defn, boolean update) {

        // name
        defn.add(new Interface.Element("name", new StringType(Pattern.compile(Shareable.PATTERN_SHAREABLE_NAME)),
                "The name of the shareable. If not specified, defaults to facility name.", 0, 1));

        // description
        defn.add(
                new Interface.Element("description", StringType.DEFAULT, "The description about the shareable.", 0, 1));

        // upload name suffix
        Interface.Element uploadNameSuffix = new Interface.Element("upload-name-suffix", XmlDocType.DEFAULT,
                "Suffix for the transactional upload namespace name.", 0, 1);
        uploadNameSuffix.add(new Interface.Element("facility", new StringType(Pattern.compile("[\\w-.]+")),
                "A (static) suffix component for the upload namespace name. The value will be stored in shareable property: "
                        + Shareable.PROPERTY_FACILITY_UPLOAD_NAME_SUFFIX
                        + ". It will be appended to the namespace name of every upload.",
                0, 1));
        uploadNameSuffix.add(new Interface.Element("user", BooleanType.DEFAULT,
                "Allow the user who uploads data to specify a suffix for the upload namespace name. Defaults to true. If enabled, the argument: '"
                        + Shareable.ARG_NAME_SUFFIX
                        + "' will be presented as an input field in the Data Mover client for the user to specified the suffix. The user specified value will be appended after the facility suffix (specified with upload-name-suffix/facility).",
                0, 1));
        defn.add(uploadNameSuffix);

        // share-with-email-directory
        Interface.Element shareWithEmailDictionary = new Interface.Element("share-with-email-dictionary", StringType.DEFAULT,
                "Suggestive dictionary for share-with argument.", 0,1);
        shareWithEmailDictionary.add(new Interface.Attribute("add-entry", new EnumType(new String[]{"yes", "no", "confirm"}), "add new email/entry to dictionary. Defaults to no. If set to 'confirm', it will send an email to facility contacts to confirm.",0));
        if(update) {
            shareWithEmailDictionary.add(new Interface.Attribute("remove", BooleanType.DEFAULT, "Remove the suggestive dictionary if set. Defaults to false", 0));
        }
        defn.add(shareWithEmailDictionary);

        // facility
        Interface.Element facility = new Interface.Element("facility", XmlDocType.DEFAULT,
                "Facility properties (name, notification settings).", update ? 0 : 1, 1);
        defn.add(facility);

        // facility name
        facility.add(new Interface.Element("name", new StringType(Pattern.compile(Shareable.PATTERN_FACILITY_NAME)),
                "Facility name", update ? 0 : 1, 1));

        // facility notification recipients
        Interface.Element facilityNotification = new Interface.Element("notification", XmlDocType.DEFAULT,
                "Email notification settings.", update ? 0 : 1, 1);
        facility.add(facilityNotification);

        facilityNotification.add(new Interface.Element("to", EmailAddressType.DEFAULT,
                "Recipient email address which will receive notifications during the data delivery process to the end user.",
                update ? 0 : 1, 255));

        facilityNotification.add(new Interface.Element("from", EmailAddressType.DEFAULT, "From email address.", 0, 1));

        // valid-from
        defn.add(new Interface.Element("valid-from", arc.mf.plugin.dtype.DateType.DEFAULT,
                "The date and time at which the is valid.  If not specified then this will be the date and time at which the shareable was created.",
                0, 1));

        // valid-to
        defn.add(new Interface.Element("valid-to", arc.mf.plugin.dtype.DateType.DEFAULT,
                "The date and time after which the shareable is no longer valid.  If not specified the shareable will remain valid indefinitely, or for the period specified by server property asset.shareable.validity.default.",
                0, 1));

        Interface.Element property = new Interface.Element("property", StringType.DEFAULT,
                "An arbitrary application defined property that can by used by applications to control workflows, etc.",
                0, Integer.MAX_VALUE);
        property.add(new Interface.Attribute("name", StringType.DEFAULT, "property name."));
        defn.add(property);

        // options
        Interface.Element completion = new Interface.Element("completion", XmlDocType.DEFAULT,
                "Options for the completion service: " + SvcFacilityShareableUploadComplete.SERVICE_NAME, 0, 1);
        defn.add(completion);
        SvcFacilityShareableUploadComplete.addOptions(null, completion);

        // email-subject-suffix
        defn.add(new Interface.Element("email-subject-suffix", BooleanType.DEFAULT,
                "Allow users who upload data to specify a suffix tag (both in the GUI and CLI) to the notification email subject. "
                        + (update ? "If not set, no change made to the shareable regarding this."
                                : "Defaults to false."),
                0, 1));
        
        // data-note
        defn.add(new Interface.Element("data-note", BooleanType.DEFAULT,
                "Allow users who upload data to add a brief note about the data. "
                        + (update ? "If not set, no change made to the shareable regarding this."
                                : "Defaults to false."),
                0, 1));

        // request-path-context
        Interface.Element requestPathContext = new Interface.Element("request-path-context", BooleanType.DEFAULT,
                "Whether the original path context for uploaded files should be requested from the client. Defaults to false.",
                0, 1);
        requestPathContext.add(new Interface.Attribute("nb-parents", IntegerType.POSITIVE,
                "The number of parents to request.  Defaults to 0.", 0));
        defn.add(requestPathContext);

    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "create shareable for facility uploads.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_MODIFY;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        boolean allowSystemAdmin = args.booleanValue("allow-system-admin", false);
        if (isSelfSystemAdmin(executor()) && !allowSystemAdmin) {
            throw new Exception(
                    "Creating facility upload shareable is disallowed by default. If it is intended, set allow-system-admin to true.");
        }

        // @formatter:off
        // asset.shareable.upload.create \
        //     :multi-use true \
        //     :name "RCS Facility Upload" \
        //     :namespace /projects/proj-platform-archive-1247.5.47 \
        //     :property -name "facility.name" "RCS Facility" \
        //     :property -name "facility.notification.to" wliu5@unimelb.edu.au \
        //     :property -name "facility.upload.name.suffix" rcs-facility-data \
        //     :parent-name < :var -type argument name-suffix > \
        //     :args < \
        //         :definition < \
        //             :element -name name-suffix -label Name -type string -min-occurs 0 -max-occurs 1 < :description "Name suffix" > \
        //             :element -name keywords -label Keyword -type string -min-occurs 0 -max-occurs 1 < :description "Keywords for the uploaded data. Separate with comma if multiple." > \
        //             :element -name data-note -label "Data Note" -type string -min-occurs 0 -max-occurs 1 < :description "A brief description about the uploaded data." > \
        //             :element -name share-with -label "(Share with) Email" -type string -min-occurs 0 -max-occurs 1 < :description "Email address of the data recipient. Separate with comma if multiple." > > \
        //     :validation-service -name vicnode.facility.shareable.upload.args.validate > \
        //     :completion-service -name vicnode.facility.shareable.upload.complete < \
        //         :manifest true \
        //         :notify true \
        //         :share -always true < \
        //             :duration 31 \
        //             :download < \
        //                 :type threshold
        //                 :threshold -unit MB 100 \
        //                 :notify true
        //                 :direct < :compress true :generate-password true > \
        //                 :client < :password rcs-mflux2020 > > \
        //             :interaction < \
        //                 :ifexists ignore \
        //                 :notify all > > > \
        //     :interaction-service -name vicnode.facility.shareable.upload.interaction.complete
        // @formatter:on

        String name = args.value("name");
        String description = args.value("description");
        String namespace = args.value("namespace");

        // make sure the current user has project-admin role and participan-a role.
        validateProjectRoleNamespaceAccess(executor(), namespace);

        String validFrom = args.value("valid-from");
        String validTo = args.value("valid-to");

        String facilityName = args.value("facility/name");
        Collection<String> facilityNotificationRecipients = args.values("facility/notification/to");
        String facilityNotificationTo = Emails.join(facilityNotificationRecipients);
        String facilityNotificationFrom = args.value("facility/notification/from");
        String facilityUploadNameSuffix = args.value("upload-name-suffix/facility");
        boolean userUploadNameSuffix = args.booleanValue("upload-name-suffix/user", true);
        boolean emailSubjectSuffix = args.booleanValue("email-subject-suffix", false);
        boolean dataNote = args.booleanValue("data-note", false);

        XmlDoc.Element emailToken = args.element("email-token");

        if (name == null) {
            name = String.format("%s Upload", facilityName);
        }

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("multi-use", true);
        dm.add("name", name);
        if (description != null) {
            dm.add("description", description);
        }
        dm.add("namespace", namespace);
        if (validFrom != null) {
            dm.add("valid-from", validFrom);
        }
        if (validTo != null) {
            dm.add("valid-to", validTo);
        }

        String shareWithEmailDict = args.value("share-with-email-dictionary");

        List<XmlDoc.Element> pes = args.elements("property");
        if (pes != null && !pes.isEmpty()) {
            dm.addAll(pes);
        }
        dm.add("property", new String[] { "name", Shareable.PROPERTY_FACILITY_NAME }, facilityName);
        if (facilityNotificationTo != null) {
            dm.add("property", new String[] { "name", Shareable.PROPERTY_FACILITY_NOTIFICATION_TO },
                    facilityNotificationTo);
        }
        if (facilityNotificationFrom != null) {
            dm.add("property", new String[] { "name", Shareable.PROPERTY_FACILITY_NOTIFICATION_FROM },
                    facilityNotificationFrom);
        }
        if (facilityUploadNameSuffix != null) {
            dm.add("property", new String[] { "name", Shareable.PROPERTY_FACILITY_UPLOAD_NAME_SUFFIX },
                    facilityUploadNameSuffix);
        }
        if (shareWithEmailDict!=null){
            String shareWithEmailDictAddEntry = args.stringValue("share-with-email-dictionary/@add-entry", "no");
            dm.add("property", new String[] { "name", Shareable.PROPERTY_FACILITY_EMAIL_DICT_ADD_ENTRY },
                    shareWithEmailDictAddEntry);
        }

        addParentNameArg(dm, facilityUploadNameSuffix, userUploadNameSuffix);

        addArgsArg(dm, userUploadNameSuffix, emailSubjectSuffix, dataNote, shareWithEmailDict);

        dm.push("completion-service", new String[] { "name", SvcFacilityShareableUploadComplete.SERVICE_NAME });
        XmlDoc.Element completion = args.element("completion");
        if (completion != null && completion.hasSubElements()) {
            dm.add(completion, false);
        }
        dm.pop();

        dm.add("interaction-service",
                new String[] { "name", SvcFacilityShareableUploadInteractionComplete.SERVICE_NAME });

        if (args.elementExists("request-path-context")) {
            dm.add(args.element("request-path-context"));
        }

        dm.add("create-parent", true);

        XmlDoc.Element re = executor().execute("asset.shareable.upload.create", dm.root());

        if (emailToken != null) {
            boolean doFacility = emailToken.booleanValue("facility", false);
            Collection<String> tos = emailToken.values("to");
            String shareableId = re.value("shareable/@id");
            String shareableToken = re.value("shareable/token");
            if (doFacility && facilityNotificationRecipients != null && !facilityNotificationRecipients.isEmpty()) {
                emailToken(executor(), shareableId, shareableToken, facilityNotificationRecipients, w);
            }
            if (tos != null && !tos.isEmpty()) {
                w.add("user", "true");
                emailToken(executor(), shareableId, shareableToken, tos, w);
            }
        }

        w.add(re, false);

    }

    static void addParentNameArg(XmlDocMaker dm, String facilityUploadNameSuffix, boolean userUploadNameSuffix)
            throws Throwable {
        dm.push("parent-name");
        if (facilityUploadNameSuffix != null) {
            dm.add("var", new String[] { "type", "property" }, Shareable.PROPERTY_FACILITY_UPLOAD_NAME_SUFFIX);
        }
        if (userUploadNameSuffix) {
            dm.add("var", new String[] { "type", "argument" }, Shareable.ARG_NAME_SUFFIX);
        }
        dm.pop();
    }

    static void addArgsArg(XmlDocMaker dm, boolean userUploadNameSuffix, boolean emailSubjectSuffix, boolean dataNote,
                           String shareWithEmailDict) throws Throwable {
        dm.push("args");

        dm.push("definition");
        if (userUploadNameSuffix) {
            dm.push("element", new String[] { "name", Shareable.ARG_NAME_SUFFIX, "label", "Name", "type", "string",
                    "min-occurs", "0", "max-occurs", "1" });
            dm.add("description", "Upload namespace name suffix.");
            dm.push("restriction", new String[] { "base", "string" });
            dm.add("pattern", Shareable.PATTERN_NAME_SUFFIX);
            dm.pop();
            dm.pop();
        }

        dm.push("element", new String[] { "name", Shareable.ARG_KEYWORDS, "label", "Keyword", "type", "string",
                "min-occurs", "0", "max-occurs", "1" });
        dm.add("description", "Keywords for the uploaded data. Separate with comma if multiple.");
        dm.push("restriction", new String[] { "base", "string" });
        dm.add("pattern", Shareable.PATTERN_KEYWORDS);
        dm.pop();
        dm.pop();
        
        if(dataNote) {
            dm.push("element", new String[] { "name", Shareable.ARG_DATA_NOTE, "label", "Data Note", "type", "string",
                    "min-occurs", "0", "max-occurs", "1" });
            dm.add("description", "A brief description of the uploaded dataset.");
            dm.push("restriction", new String[] { "base", "string" });
            dm.add("lines", 10);
            dm.add("max-length", 1024);
            dm.pop();
            dm.pop();            
        }

        if(shareWithEmailDict==null) {
            dm.push("element", new String[]{"name", Shareable.ARG_SHARE_WITH, "label", "(Share with) Email", "type",
                    "string", "min-occurs", "0", "max-occurs", "1"});
            dm.add("description", "Email address of the data recipient. Separate with comma if multiple.");
        } else {
            dm.push("element", new String[]{"name", Shareable.ARG_SHARE_WITH, "label", "(Share with) Email", "type",
                    "email-address", "suggested-values-dictionary", shareWithEmailDict, "min-occurs", "0", "max-occurs", "16"});
            dm.add("description", "Email address of the data recipient.");
        }
        dm.pop();

        if (emailSubjectSuffix) {
            dm.push("element", new String[] { "name", Shareable.ARG_EMAIL_SUBJECT_SUFFIX, "label",
                    "Email subject suffix", "type", "string", "min-occurs", "0", "max-occurs", "1" });
            dm.add("description", "Notification email subject suffix.");
            dm.push("restriction", new String[] { "base", "string" });
            dm.add("lines", 1);
            dm.add("max-length", Shareable.EMAIL_SUBJECT_SUFFIX_MAX_LENGTH);
            dm.pop();
            dm.pop();
        }

        dm.pop(); // pop definition

        dm.add("validation-service", new String[] { "name", SvcFacilityShareableUploadArgsValidate.SERVICE_NAME });

        dm.pop(); // pop args
    }

    private static void emailToken(ServiceExecutor executor, String shareableId, String shareableToken,
            Collection<String> facilityNotificationRecipients, XmlWriter w) throws Throwable {
        XmlDoc.Element shareable = Shareable.describe(executor, shareableId);
        String facilityName = Shareable.getFacilityName(shareable);

        String subject = facilityName == null ? "Facility Upload Token"
                : String.format("Facility Upload Token for %s", facilityName);

        StringBuilder sb = new StringBuilder();
        sb.append("<p>Hello,</p><br>\n");
        sb.append("<p>A new instrument platform upload shareable token has been created as follows.</p>");

        String tableOpen = "<table style=\"border:1px solid black; border-collapse:collapse; width:85%;\">";
        String tdLeftOpen = "<td align=\"right\" style=\"width:90px; border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em;\">";
        String tdRightOpen = "<td style=\"border:1px solid black; border-collapse:collapse; line-height:1.5em; padding:0.25em\">";

        sb.append(tableOpen).append("\n");
        if (facilityName != null) {
            sb.append("<tr>").append(tdLeftOpen).append("<b>Facility Name:</b></td>").append(tdRightOpen)
                    .append(facilityName).append("</td></tr>\n");
        }

        sb.append("<tr>").append(tdLeftOpen).append("<b>ID:</b></td>").append(tdRightOpen).append(shareableId)
                .append("</td></tr>\n");

        sb.append("<tr>").append(tdLeftOpen).append("<b>Upload Token:</b></td>").append(tdRightOpen)
                .append(shareableToken).append("</td></tr>\n");

        String url = shareable.value("url");
        sb.append("<tr>").append(tdLeftOpen).append("<b>Upload URL:</b></td>").append(tdRightOpen).append("<a href=\"")
                .append(url).append("\">").append(url).append("</a>").append("</td></tr>\n");
        sb.append("</table><br><br><br>\n");

        sb.append("<h3>Configure Mediaflux Data Mover Client</h3>\n");
        sb.append(
                "<p>You can add the token into the configuration file of the <b>Mediaflux Data Mover Client</b>.</p>\n");
        sb.append("<ul>\n");
        sb.append("<li>Create the configuration directory if it does not exist:</li>\n");
        sb.append("<ul>\n");
        sb.append(
                "<li>On Windows, execute the command in <b>Command Prompt</b> window:<pre>mkdir %userprofile%\\.Arcitecta\\DataMover</pre></li>\n");
        sb.append(
                "<li>On Linux/Mac OS, execute the command in <b>Terminal</b> window:<pre>mkdir -p ~/.Arcitecta/DataMover</pre></li>\n");
        sb.append("</ul>\n");
        sb.append(
                "<li>Create the configuration file: <b>settings.xml</b>,if it does not exist. And add the above token:\n");
        sb.append("<pre>\n");
        sb.append("&lt;settings&gt;\n");
        sb.append("&lt;token&gt;").append(shareableToken).append("&lt;/token&gt;\n");
        sb.append("&lt;/settings&gt;\n");
        sb.append("</pre>\n");
        sb.append("</li>\n");
        sb.append(
                "<li>Alternatively, you can save the attached <b>settings.xml</b> file into the configuration directory.</li>");
        sb.append("</ul>\n");

        XmlDocMaker dm = new XmlDocMaker("args");
        for (String recipient : facilityNotificationRecipients) {
            dm.add("to", recipient);
            w.add("token-emailed-to", recipient);
        }
        dm.add("subject", subject);
        dm.add("body", new String[] { "type", "text/html" }, sb.toString());

        // attach settings.xml
        dm.push("attachment");
        dm.add("name", "settings.xml");
        dm.add("type", "text/xml");
        dm.pop();
        PluginService.Input in = new PluginService.StringInput(
                String.format("<settings><token>%s</token></settings>\n", shareableToken), "text/xml");

        executor.execute("mail.send", dm.root(), new PluginService.Inputs(in), null);
    }

    static void validateProjectRoleNamespaceAccess(ServiceExecutor executor, String namespace) throws Throwable {
        String projectId = Projects.resolveProjectIdFromNSPath(namespace);
        if (projectId == null) {
            throw new Exception("Cannot resolve project id from asset namespace path: " + namespace);
        }

        String projectRoleNS = projectId;
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.push("perm");
        dm.add("access", "ADMINISTER");
        dm.add("resource", new String[] { "type", "role:namespace" }, projectRoleNS + ":");
        dm.pop();
        XmlDoc.Element re = executor.execute("actor.self.have", dm.root());
        boolean haveRoleNSAccess = re.booleanValue("perm", false);

        if (!haveRoleNSAccess) {
            String actorSelfName = executor.execute("actor.self.describe").value("actor/@name");
            String errorMsg = actorSelfName + " does not have ADMINISTER access to role namespace: " + projectRoleNS;
            throw new Exception(errorMsg);
        }

//        String projectAdminRole = Projects.getProjectRole(projectId, Project.ProjectRoleType.ADMIN);
//        String projectARole = Projects.getProjectRole(projectId, Project.ProjectRoleType.A);
//        XmlDocMaker dm = new XmlDocMaker("args");
//        dm.add("role", new String[]{"type", "role"}, projectAdminRole);
//        dm.add("role", new String[]{"type", "role"}, projectARole);
//        XmlDoc.Element re = executor.execute("actor.self.have", dm.root());
//        boolean haveProjectAdminRole = re.booleanValue("role[@name='" + projectAdminRole + "']");
//        boolean haveProjectARole = re.booleanValue("role[@name='" + projectARole + "']");
//        if (!haveProjectAdminRole || !haveProjectARole) {
//            String actorSelfName = executor.execute("actor.self.describe").value("actor/@name");
//            String errorMsg = actorSelfName + " does not have ";
//            if (!haveProjectAdminRole) {
//                errorMsg += " role: " + projectAdminRole;
//            }
//            if (!haveProjectARole) {
//                if (!haveProjectAdminRole) {
//                    errorMsg += " and ";
//                }
//                errorMsg += " role: " + projectARole;
//            }
//            throw new Exception(errorMsg);
//        }

    }

    private static boolean isSelfSystemAdmin(ServiceExecutor executor) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("role", new String[] { "type", "role" }, "system-administrator");
        return executor.execute("actor.self.have", dm.root()).booleanValue("role");
    }
}
