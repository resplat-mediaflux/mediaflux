/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;

/**
 * TBD: send email to project owner then schedule
 * 
 * @author nebk
 *
 */


public class SvcProjectSummaryForMembers extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectSummaryForMembers()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
		_defn.add(new Interface.Element("who", new EnumType(Properties.notificationRecipients), 
				"Select from 'owner' (users specified as owners in meta-data)," +
						"'administrator' (users with administrator projectrole), " +
						"'manage (users with administrator plus owner), " +
						"'user' (all users with a project role) and 'all' (user + owner).  Default is send to no-one.", 0, 1));
		_defn.add(new Interface.Element("email", StringType.DEFAULT, 
				"An email address to send the report to as well as the emails selected by arg 'who'.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("sum", BooleanType.DEFAULT, "Sum project content on primary and DR systems (defaults to false)",
				0, 1));		
		_defn.add(new Interface.Element("ingest", IntegerType.DEFAULT, "If specified, calculate the ingest rate for the previous given number of days. Default is no ingest calculation.",
				0, 1));		
		_defn.add(new Interface.Element("exclude", StringType.DEFAULT, "Exclude sending an email to this address.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("provision", BooleanType.DEFAULT, "Notify the standard provisioning user (that holds the admin role for all projects) as well (default false) .", 0, 1));
		_defn.add(new Interface.Element("send", BooleanType.DEFAULT, "Actually send the message (default), else just list ot the terminal.", 0, 1));
		SvcProjectDescribe.addProdType (_defn);
		_defn.add(new Interface.Element("name-validation", IntegerType.DEFAULT,
				"If specified, check assets for possible invalid names (across multipel operating systems) for the previous given number of days. Default is no name validation.", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Sends a summary of each specified project to the specified member types of the project. See vicnode.project.describe for details of what gets sent.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.summary.for.members";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		Boolean doSum = args.booleanValue("sum",  false);
		String projType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		Collection<String> excludes = args.values("exclude");
		String who = args.stringValue("who");
		Collection<String> emails = args.values("email");
		Boolean dropProvision = !(args.booleanValue("provision", false));
		Boolean send = args.booleanValue("send", true);
		Integer nameValidationDays = args.intValue("name-validation");
		Integer ingestDays = args.intValue("ingest");


		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String s[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(s[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) return;

		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);

			if (Project.keepOnProjectType(ph, projType)) {

				w.push("project", new String[]{"id", projectID});

				// Returns empty collection if who is null
				Collection<String> emails2 = Project.listProjectEmails(executor(), theProjectID, who, dropProvision);
				if (emails!=null) {
					// Add given email
					emails2.addAll(emails);
				}
				if (emails2.size()>0) {
					for (String email : emails2) {
						if (keepEmail(email,excludes)) {
							w.add("email", email);
						}
					}
				}

				// Do it
				makeAndSendSummary (executor(),  ph.id(), nameValidationDays, doSum, ingestDays, emails2, send,  w);

				w.pop();
			}
		}
	}

	private Boolean keepEmail (String email, Collection<String> excludes) {
		if (excludes==null) return true;
		for (String exclude : excludes) {
			if (email.equals(exclude)) return false;
		}
		return true;
	}


	public static void makeAndSendSummary (ServiceExecutor executor, String projectID,  
			Integer nameValidationDays, Boolean doSum, Integer ingestDays, Collection<String> emails, Boolean send, XmlWriter w) throws Throwable {

		// Summarise
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("DR", false);
		dm.add("prod-type", "all");
		dm.add("project-id", projectID);
		dm.add("rep-type", "all");
		dm.add("sum", doSum);
		dm.add("posix", "false");
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element r = executor.execute(Properties.SERVICE_ROOT + "project.describe", dm.root());

		// Build summary	
		String summary = "ID : " + projectID + "\n";
		String name = r.value("project/name");
		if (name!=null) {
			summary += "Name : " + name + "\n";
		}
		XmlDoc.Element owner = r.element("project/owner");
		if (owner!=null) {
			summary += "Owner : " + owner.value("first") + " " + owner.value("last") + " (" + owner.value("email") + ") \n";
		}
		String namespace = r.value("project/namespace");
		summary += "Namespace : " + namespace + "\n";
		//
		XmlDoc.Element quota = r.element("project/quota");
		if (quota!=null) {
			XmlDoc.Element inherited = quota.element("inherited");
			if (inherited!=null) {
				summary += "Quota (inherited) \n";
				summary += "   Allocation : " + inherited.value("allocation/@h") + " \n";
				summary += "   Used : " + inherited.value("used/@h") + " \n";
			} else {
				summary += "Quota \n";
				summary += "   Allocation : " + quota.value("allocation/@h") + " \n";
				summary += "   Used : " + quota.value("used/@h") + " \n";
			}
		}
		summary += "Members \n";
		Collection<XmlDoc.Element> roles = r.elements("project/users/role");
		if (roles!=null) {
			for (XmlDoc.Element role : roles) {
				String roleName = role.value("@name");
				String[] s = roleName.split(":");
				summary += "   Role : " + s[1] + "\n";
				Collection<String> users = role.values("user");
				for (String user : users) {
					summary += "      " + user + "\n";
				}
			}
		}
		//
		String replication = "Replication queue : " + r.value("project/replication/processing-queue") + "\n";

		// Work out ingest rate for given period
		if (ingestDays!=null) {
			PluginTask.checkIfThreadTaskAborted();
			dm = new XmlDocMaker("args");
			String s = Integer.toString(ingestDays);
			dm.add("where", "namespace>='"+namespace+"' and ctime>='now-"+s+"day'");
			r = executor.execute("vicnode.project.ingest.rate.simple", dm.root());
			String t =  r.value("project/size/@size-human");
			if (t==null) {
				t = "0";
			}
			summary += "Ingest (last " + s + "days) : " + t + "\n";
		}
		// Add replication detail
		summary += replication;

		// Have look for files with unfavoured names in given period
		if (nameValidationDays!=null) {
			String s = Integer.toString(nameValidationDays);
			dm = new XmlDocMaker("args");
			dm.add("where", "namespace>='"+namespace+"' and ctime>='now-"+s+"day'");
			r = executor.execute("unimelb.asset.name.validate", dm.root());
			if (r!=null) {
				Collection<XmlDoc.Element> invalid = r.elements("invalid");
				if (invalid!=null) {
					String wikiLink = Properties.getApplicationProperty(executor,
							Properties.APPLICATION_PROPERTY_DOC_USER_VALID_FILENAMES, 
							Properties.APPLICATION_PROPERTY_APP);

					summary += "File Name Validation (last " + s + "days) \n";
					summary += "   These assets and namespaces may have invalid names on some operating systems with some protocols. \n";
					summary += "   wiki-reference " + wikiLink + "\n";
					for (XmlDoc.Element iv : invalid) {
						String t = iv.value("@path");
						summary += "   " + t + "\n";
					}
				}
			}
		}

		// Send to users or just report message to terminal
		if (send && emails.size()>0) {
			String today = DateUtil.todaysDate(2);
			String subject = new String("Mediaflux Project " + projectID + " summary (" + today + ")");
			String from = Util.getServerProperty(executor, "notification.from");
			MailHandler.sendMessage(executor, emails, subject, summary, null, false, from, w);
		} else {	
			w.add("message", summary);
		}
	}
}
