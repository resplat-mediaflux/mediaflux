/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EnumType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Project.ProjectRoleType;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectRoleAdd extends PluginService {




	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectRoleAdd()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
		_defn.add(new Interface.Element("project-role", new EnumType(Project.ProjectRoleType.projectRoleTypes()),
				"The new role in the project.", 1, 1));	
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Service function so that when a new role is added to the project structures, the role is created and the namespace updated with a new ACL for the given project.  If the role pre-exists in the project namespace, the service does nothing.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.role.add";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");
		String projectRole = args.stringValue("project-role");
		// Validate role
		String[] roles = ProjectRoleType.projectRoleTypes();
		Boolean found = false;
		for (int i=0; i<roles.length; i++) {
			if (roles[i].equals(projectRole)) {
				found = true;
			}
		}
		if (!found) {
			throw new Exception("Illegal project role type - " + projectRole);
		}

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) return;

		// Iterate over projects 
		ProjectRoleType projectRoleType = ProjectRoleType.toEnum(projectRole);
		for (String projectID : projectIDs) {
			// Actual role name with namespace
			String actualRoleName = Project.actualRoleName(projectID, projectRoleType);

			// DOes role exist already ?
			if (!roleExists(executor(), projectID, actualRoleName)) {
				// If not, create role - will ignore roles if pre-exists
				Project.createStandardRoles(executor(), projectID);
				
				// Set ACLs and grant permissions
				Project.grantPermissionsAndACLs(executor(), projectID, projectRoleType, true, true);
				w.add("role", new String[]{"status", "added"}, actualRoleName);
			} else {
				w.add("role", new String[]{"status", "exists"}, actualRoleName);
			}
		}
	}

	private Boolean roleExists (ServiceExecutor executor, String projectID, String projectRole) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", projectID);
		XmlDoc.Element r = executor.execute("authorization.role.list");
		Collection<String> roles = r.values("role");
		for (String role : roles) {
			if (projectRole.equals(role)) return true;
		}
		return false;
	}


}
