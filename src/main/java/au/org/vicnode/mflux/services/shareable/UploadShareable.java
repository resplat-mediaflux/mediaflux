package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import au.org.vicnode.mflux.plugin.util.AssetNamespaceUtil;

public class UploadShareable extends Shareable {

    UploadShareable(Element shareable) throws Throwable {
        super(shareable);
    }

    static String getUploadNamespace(ServiceExecutor executor, XmlDoc.Element shareable, String uploadId,
            boolean checkMoved) throws Throwable {
        String shareableId = shareable.value("@id");
        String shareableType = shareable.value("@type");
        if (!"upload".equals(shareableType)) {
            throw new Exception("Shareable " + shareableId + " is not a upload shareable");
        }
        String shareableNS = shareable.value("namespace");
        String parentName = getUploadParentName(shareable, uploadId);
        if (parentName == null) {
            throw new Exception("Could not find upload: " + uploadId + " in shareable " + shareableId);
        }
        String uploadNS = String.format("%s/%s", shareableNS, parentName);
        if (checkMoved && !AssetNamespaceUtil.assetNamespaceExists(executor, uploadNS)) {
            // NOTE:
            // For uploads have been moved out of shareable namespace, we need to use
            // asset.query to locate the manifest asset first, then locate the parent
            // (upload) namespace.
            XmlDoc.Element re = Manifest.findManifestAsset(executor, shareableId, uploadId, "get-meta");
            uploadNS = re.value("asset/namespace");
        }
        return uploadNS;
    }

    static String getUploadParentName(XmlDoc.Element shareable, String uploadId) throws Throwable {
        if (shareable != null && uploadId != null) {
            return shareable.stringValue("uploads/upload[@id='" + uploadId + "']/parent-name",
                    shareable.value("uploads/upload[@id='" + uploadId + "']/@parent-name"));
        }
        return null;
    }

    static void setInteraction(ServiceExecutor executor, String shareableId, String uploadId, String interactionId)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", shareableId);
        dm.add("upload-id", uploadId);
        dm.add("interaction-id", interactionId);
        executor.execute("asset.shareable.upload.interaction.set", dm.root());
    }

    public static String getUploadPathContext(Element shareable, String uploadId) throws Throwable {
        return shareable.value("uploads/upload[@id='" + uploadId + "']/path-context");
    }

}
