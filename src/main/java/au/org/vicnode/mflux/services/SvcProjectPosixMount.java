/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Posix;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixMount extends PluginService {



	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectPosixMount()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("apply-mode-bits", BooleanType.DEFAULT, 
				"If set to 'false' requests to set permissions, owner, or  group will be ignored. If set to 'true' requests to set permissions, owner, or group will be honoured.", 1, 1));
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
		_defn.add(new Interface.Element("read-only", BooleanType.DEFAULT, "If mounting, should the mount point be read-only - you must specify. ", 1, 1));
		//
		_defn.add(makeXMLExportDefn());
		//	
		_defn.add(makeRestrictDefn(Integer.MAX_VALUE, true));
		_defn.add(new Interface.Element("restrict-action",new EnumType(new String[]{"replace", "remove", "merge"}),"Action to take. 'merge' (default) means the restrictions you supply will be added to the existing ones (so if the there is no new restriction, that just leaves the existing one in place).  No checks are made for duplicates'replace' means all restrictions will be replaced by the new ones. 'remove' means all restrictions will be removed (and if you supply any new ones they will be ignored), .",0, 1));
		_defn.add(new Interface.Element("allowed-protocol",new EnumType(new String[]{"smb", "nfs", "ssh", "all"}),"Specifies a network protocol that will allow users access to the share through POSIX interface. For example, if type is set to 'smb' only connections that use the SMB network protocol will be allowed to access the share. Setting it to 'all' will clear the setting. By default it is set to 'all'.",1, 1));	
		//
		Interface.Element root = new Interface.Element("root",XmlDocType.DEFAULT,"Root access, if any. See argument root-action to see how current state root restrictions are handled.",0,1);
		root.add(makeRestrictDefn(Integer.MAX_VALUE, true));
		root.add(new Interface.Element("uid", new LongType(0,Long.MAX_VALUE),  "If set, then the UID that will be given 'root' access for mounting (can be specified as 0). If not specified, then no root access for mount, unless the UID is in the identity map.",1, 1));		
		_defn.add(root);
		_defn.add(new Interface.Element("root-action",new EnumType(new String[]{"merge", "replace", "remove"}),"Action to take on root restrictions. If 'root' is set, you must always specifiy the 'uid' (which is a singleton). If 'merge', then set the existing UID or replace with supplied, and add the specified restrictions to the existing ones the existing root restriction - no checks for duplicates are made. 'replace' means the root element will be replaced by the new one.  'remove' means the root element  will be removed (and if you supply any new ones they will be ignored)..",0, 1));
	}


	private static Interface.Element  makeXMLExportDefn() {

		Interface.Element re = new Interface.Element("metadata-export",XmlDocType.DEFAULT,"Define asset metadata export.  If set, replaces existing.  If not set, then use the existing state of the mount point (if it exists).  If not set and does not exist, use the defaults (below).",0,1);
		re.add(new Interface.Element("export", BooleanType.DEFAULT,"Export asset metadata in XML sidecar files. Defaults to false.",0,1));
		re.add(new Interface.Element("extended", BooleanType.DEFAULT,"Export the full metadata definition rather than just the active fields. Defaults to false.",0,1));
		re.add(new Interface.Element("extension", StringType.DEFAULT,"The sidecar file extension. Defaults to .xml",0,1));
		re.add(new Interface.Element("prettyprint", BooleanType.DEFAULT,"Pretty print the XML metadata so it's human readable. Defaults to true.",0,1));
		re.add(new Interface.Element("replace-dots", BooleanType.DEFAULT,"Replace the '.' beforebefore the file extension in XML sidecar filenames with '_'. Defaults to false.",0,1));
		re.add(new Interface.Element("transform", StringType.DEFAULT,"Specifies an XSLT transform for metadata presentation in the sidecar file. 'none' (the default) means raw XML format. Use the service 'asset.meta.transform.provider.describe' to view available transforms.",0,1));
		return re;	

	}

	private static Interface.Element  makeRestrictDefn(int maxNbRestrict,boolean allowProtocolSelection) {

		Interface.Element re = new Interface.Element("restrict",XmlDocType.DEFAULT,"Access restrictions, if any. See argument restrict-action to see how current state restrictions are handled.",0,maxNbRestrict);
		re.add(new Interface.Attribute("description",StringType.DEFAULT,"Arbitrary description for the purpose of the restriction.",0));

		if ( allowProtocolSelection ) {
			re.add(new Interface.Attribute("protocol",StringType.DEFAULT,"The name of a calling protocol name (e.g. network service) tofor which this restriction applies. If not specified, then applies to all protocols.",0));
		}

		Interface.Element ne = new Interface.Element("network",XmlDocType.DEFAULT,"Network access restrictions, if any.",0,1);

		Interface.Element address = new Interface.Element("address", StringType.DEFAULT,"IP network address (IPv4 or IPv6) to restrict user access.",0,Integer.MAX_VALUE);
		address.add(new Interface.Attribute("description",StringType.DEFAULT,"Arbitrary description for this address.",0));
		Interface.Attribute mask = new Interface.Attribute("mask", StringType.DEFAULT, "The netmask for the corresponding network address. Defaults to 255.255.255.255", 0);

		address.add(mask);
		ne.add(address);

		ne.add(new Interface.Element("encrypted",BooleanType.DEFAULT,"Indicates whether encrypted only network transports will be accepted. Defaults to false.",0,1));
		re.add(ne);

		return re;	

	}


	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Mounts or remounts a posix mount point for the project. You must supply the values for allowed-protocol, apply-mode-bits abd readonly. Can merge existing state. Mount points are case insensitive to the mounting client.  If the mount point meta-data does not exist for some reason, it will be created (with read-only input honoured if set and false if not). ";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.mount";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Inputs
		String projectID = args.value("project-id");
		Boolean readOnly = args.booleanValue("read-only"); 
		Collection<XmlDoc.Element> restrictions = args.elements("restrict");
		String restrictAction = args.stringValue("restrict-action", "merge");
		XmlDoc.Element metaExport = args.element("metadata-export");
		//
		XmlDoc.Element root = args.element("root");
		String rootAction = args.stringValue("root-action", "merge");
		Boolean applyModeBits = args.booleanValue("apply-mode-bits");
		Boolean throwOnFail = true;
		String allowedProtocol = args.stringValue("allowed-protocol", "all");
		mount (executor(), projectID, metaExport, restrictions, restrictAction, 
				root, rootAction, readOnly, throwOnFail, 
				applyModeBits, allowedProtocol, w);
	}



	public static void mount (ServiceExecutor executor, String theProjectID,  
			XmlDoc.Element metaExport, 
			Collection<XmlDoc.Element> userRestrictions, String restrictAction, 
			XmlDoc.Element root, String rootAction, 
			Boolean readOnly, Boolean throwOnFail, Boolean applyModeBits, 
			String allowedProtocol, XmlWriter w) throws Throwable {

		if (root==null) {
			// For remove leave empty, for merge can be empty, for replace must supply something
			if (rootAction.equals("replace")) {
				throw new Exception ("You must specify a root element for rootAction=replace");
			}
		}
		if (rootAction==null) {
			throw new Exception ("Variable 'rootAction' cannot be null");
		}
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor, theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor, false, true));
		}

		// Iterate over projects
		if (projectIDs.size()==0) {
			return;
		}
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor, projectID, true);


			// If mounted, unmount. Then we remount in the desired state.
			// Could check if states the same and then do nothing but this is easier !
			Boolean isMounted = Posix.isProjectMounted(executor, ph.id());
			if (isMounted) {
				Posix.unMountPosix(executor, ph.id());
			} 

			// Get the current mount point state from the saved namespace 
			// meta-data. This includes the IP,  root restrictions and allowed
			// protocols (if not there all)
			String nameSpace = ph.nameSpace();
			XmlDoc.Element mountMeta = Posix.describePosixMountMetaData(executor, nameSpace);

			// Now mount in the state we want. We are going to assume the identity map exists
			// as it is created when the project is provisioned.  If it has been destroyed
			// an exception will arise which is probably better than silently making it.
			w.push("project");
			w.add("id", ph.id());
			try {

				// Generate desired IP restrictions from old state and new user specification plus action
				Collection<XmlDoc.Element> currentRestrictions = null; 
				if (mountMeta!=null) {
					currentRestrictions = mountMeta.elements("restrict");
				}
				Collection<XmlDoc.Element> newRestrictions = makeNewRestrictions (userRestrictions, restrictAction, currentRestrictions);

				// Generate desired root IP restrictions and UID specification from old state and new user specification plus action
				// If you specify a root restriction you must specify the UID
				XmlDoc.Element newRoot = null;   // holds restrict and uid
				Collection<XmlDoc.Element> newRootRestrictions = null;
				if (rootAction.equals("remove")) {
					// Leave null to remove it when set
				} else {
					// Use an XmlDocMaker to holds the new 'root' values
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.push("root");
					String userUID = null;
					if (root!=null) userUID = root.value("uid");
					String currentUID = null;
					if (mountMeta!=null) {
						currentUID = mountMeta.value("root/uid");
					}

					// Transfer uid from user specification and/or current
					if (rootAction.equals("merge")) {
						// merge is really a replace if there is a new value supplied.
						// Otherwise merge just means keep extant
						if (userUID!=null) {
							dm.add("uid", userUID);
						} else if (currentUID!=null) {
							dm.add("uid", currentUID);					
						}
					} else if (rootAction.equals("replace")) {
						if (userUID!=null) {
							dm.add("uid", userUID);
						}
					}
					// Transfer restrict from user specification and/or current
					Collection<XmlDoc.Element> currentRootRestrictions = null; 
					if (mountMeta!=null) {
						currentRootRestrictions = mountMeta.elements("root/restrict");	
					}
					Collection<XmlDoc.Element> userRootRestrictions = null;
					if (root!=null) {
						userRootRestrictions = root.elements("restrict");
					}
					
					newRootRestrictions = makeNewRestrictions (userRootRestrictions, rootAction, currentRootRestrictions);
					if (newRootRestrictions!=null) {
						dm.addAll(newRootRestrictions);
					}
					dm.pop();
					//
					newRoot = dm.root().element("root");
					if (newRoot.elements()==null) newRoot = null;
				}
				// If supplied, use the given meta-data export.
				// If not supplied use the current state
				XmlDoc.Element newMetaExport = metaExport;
				if (newMetaExport==null) {
					newMetaExport = mountMeta.element("metadata-export");
				} 
				
				// We could also merge the state of :read-only and :apply-mode-bits here.
				// That is, if these arguments are made optional, and if not set,
				// then simply use the existing state for those arguments  (as
				// we do with the more complex elements)
				/*
				 if (readOnly==null) {
				 	readOnly = mountMeta.booleanValue("read-only", false);
				 }
				 if (applyModeBits==null) {
				    applyModeBits = mountMeta.booleanValue("apply-mode-bits", true);  // If does not yet exist in this state it's true
				 }
				 */
				
				// Mount
				Posix.mountPosix(executor, ph.id(), newMetaExport, nameSpace, newRestrictions, newRoot, readOnly, 
						applyModeBits, allowedProtocol);

				// Update the posix meta-data state stored on the project namespace (will create if missing).
				// This holds the mount state when the mount point was last mounted (now).
				// This is stored because posix mount points are lost on a server restart.  
				// This enables us to re-create the mount state on restart

				// Fetch the actual mount point state again, rather than relying on what the user supplied.
				// This is because defaults in services down the tree may have inserted additional
				// values (e.g. the mask on IP restrictions, encryption setting)
				XmlDoc.Element mountState = Posix.describePosixMountPoint(executor, projectID);	
				Collection<XmlDoc.Element> finalRestrictions = mountState.elements("mount/restrict");
				XmlDoc.Element finalRoot = mountState.element("mount/root");
				readOnly = mountState.booleanValue("mount/read-only");
				
				// If the native posix mount point description does not show this value
				// it means it's true.  Grrr.
				applyModeBits = mountState.booleanValue("mount/apply-mode-bits", true);
				
				// If it's not set (meaning all protocols), the posix mount point 
				// description does not show this
				allowedProtocol = mountState.stringValue("mount/allowed-protocol", "all");
				
				// It's a bit wierd, but if you set the export to false, then no meta-data
				// are set on the mount point.
				XmlDoc.Element finalMetaExport = mountState.element("mount/metadata-export");

				// Update namespace meta-data preserving mount point state
				Boolean shouldBeMounted = true;
				Posix.updatePosixMountMetaData (executor, ph.id(), finalRestrictions, finalRoot, readOnly,
						finalMetaExport, applyModeBits, allowedProtocol, shouldBeMounted);
				//
				w.addAll(mountState.element("mount").elements());
			} catch (Throwable t) {
				if (throwOnFail) throw new Exception(t);
				w.add("mounted",  new String[]{"state", "failed"}, false);
			}
			w.pop();
		}
	}



	private static Collection<XmlDoc.Element> makeNewRestrictions (Collection<XmlDoc.Element> restrictions,
			String restrictAction, Collection<XmlDoc.Element> currentRestrictions) throws Throwable {
		// Restrictions on IP
		Vector<XmlDoc.Element> newRestrictions = new Vector<XmlDoc.Element>();
		if (restrictions==null) {

			// Special case that the user does not supply any restrictions
			if (restrictAction.equals("merge")) {
				// If we have no specified restrictions, the default is to
				// preserve the existing ones.
				if (currentRestrictions!=null) {
					newRestrictions.addAll(currentRestrictions);
				}
			} else if (restrictAction.equals("replace")) {
				//
			} else if (restrictAction.equals("remove")) {
				//
			}
		} else {
			if (restrictAction.equals("merge")) {
				if (currentRestrictions!=null) {
					newRestrictions.addAll(currentRestrictions);
				}
				if (restrictions!=null) {
					newRestrictions.addAll(restrictions);
				}
			} else if (restrictAction.equals("replace")) {
				if (restrictions!=null) {
					newRestrictions.addAll(restrictions);
				}
			} else if (restrictAction.equals("remove")) {
				//
			}			
		}
		return newRestrictions;
	}
}
