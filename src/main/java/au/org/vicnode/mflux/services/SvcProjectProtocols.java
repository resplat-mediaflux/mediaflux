/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;



public class SvcProjectProtocols extends PluginService {


	public static final int DEFAULT_BATCH_SIZE = 10000;


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectProtocols()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectIDs(_defn,  allowPartialCID);
		_defn.add(new Interface.Element("rep-type", new EnumType(Properties.repTypes),
				"Select from 'all', (default, all projects), 'replication' (only projects which are being replicated), 'no-replication' (projects that are not being replicated).",
				0, 1));
		_defn.add(new Interface.Element("contains", StringType.DEFAULT, "A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",	 0, 1));
		_defn.add(new Interface.Element("where", StringType.DEFAULT, "Predicate to further restrict the assets (e.g. time based). ",	 0, 1));
		_defn.add(new Interface.Element("posix-only", BooleanType.DEFAULT, "Only list (default false) projects for which there are some asset creations with the posix (ssh,smb,nfs) protocols.",	 0, 1));
		SvcProjectDescribe.addProdType (_defn);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Summarises the protocols (http,smb,sftp,dicom,nfs) used to upload data. Only projects with some assets found in the given query are listed.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.protocols";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String prodType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		Boolean posixOnly = args.booleanValue("posix-only", false);
		String keyString = args.value("contains");
		if (keyString!=null) {
			keyString = keyString.toUpperCase();
		}
		String repType = args.stringValue("rep-type", Properties.repTypes[0]);
		String where = args.stringValue("where");
		
		// List of projects				
		Collection<String> theProjectIDs = args.values("project-id");
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs!=null) {
			for (String theProjectID : theProjectIDs) {
				String [] t = Project.validateProject(executor(), theProjectID, allowPartialCID);
				projectIDs.add(t[1]);
			}
		} else{
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) {
			return;
		}


		// Iterate and summarise
		Integer size = 5000;
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);

			// FInd replication asset processing queue
			String qName = ProjectReplicate.isProjectReplicated(ph);

			if (Project.keepOnRepType(executor(), qName, repType) &&
					Project.keepOnProjectType(ph, prodType) &&
					Project.keepOnKeyString(ph.id(), keyString)) {

				summarise (executor(), ph, where, size, posixOnly, w);
			}
			PluginTask.checkIfThreadTaskAborted();
		}
	}


	private void summarise (ServiceExecutor executor, ProjectHolder ph, 
			String where, Integer size, Boolean posixOnly, XmlWriter w) throws Throwable {

		String assetNameSpace = ph.nameSpace();
		//
		PluginTask.checkIfThreadTaskAborted();
		XmlDocMaker dm = new XmlDocMaker("args");
		String query = "namespace>='" + assetNameSpace  + "'";
		if (where!=null) {
			query += " and (" + where + ")";
		}
		dm.add("where", query);
		dm.add("size", "infinity");
		dm.add("as", "iterator");
		dm.add("action", "get-meta");
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element r = executor().execute("asset.query", dm.root());
		if (r==null) {
			return;
		}

		// Iterate and accumulate
		Long[] sums = new Long[6];
		sums[0] = 0L;     // unknown
		sums[1] = 0L;     // http/s
		sums[2] = 0L;     // smb
		sums[3] = 0L;     // ssh
		sums[4] = 0L;     // nfs
		sums[5] = 0L;     // dicom
		//
		long iteratorID = r.longValue("iterator");
		accumulate (iteratorID, sums);
		//
		long total = 0l;
		for (int i=0;i<sums.length;i++) {
			total += sums[i];
		}
		if (total>0l) {
			if (posixOnly) {
				if (sums[2]>0 || sums[3]>0 || sums[4]>0) {
					w.add ("project-id", new String[] {"total", ""+total, "unknown", ""+sums[0], "https", ""+sums[1],
							"smb", ""+sums[2], "ssh", ""+sums[3], "nfs", ""+sums[4], "dicom", ""+sums[5]}, ph.id());	
				}
			} else {
				w.add ("project-id", new String[] {"total", ""+total, "unknown", ""+sums[0], "https", ""+sums[1],
						"smb", ""+sums[2], "ssh", ""+sums[3], "nfs", ""+sums[4], "dicom", ""+sums[5]}, ph.id());
			}
		}
	}


	private void accumulate (Long iteratorID, Long[] sums) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", iteratorID);
		dm.add("size", 	DEFAULT_BATCH_SIZE);

		//
		Boolean complete = false;
		try {
			while (!complete) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDoc.Element r = executor().execute("asset.query.iterate", dm.root());
				complete = r.booleanValue("iterated/@complete");
				Collection<XmlDoc.Element> res = r.elements("asset");
				if (res != null) {
					for (XmlDoc.Element re : res) {

						String protocol = re.value("created-via");
						if (protocol==null) {
							sums[0] += 1;
						} else if (protocol.equals("http")) {
							// Could distinguish between http and https if we want (there is an attribute)
							sums[1]++;
						} else if (protocol.equals("smb")) {
							sums[2]++;
						} else if (protocol.equals("ssh")) {
							sums[3]++;
						} else if (protocol.equals("nfs")) {
							sums[4]++;
						} else if (protocol.equals("dicom")) {
							sums[5]++;
						}
					}
				}
			}
		} catch (Throwable e) {
			// make sure the iterator is destroyed.
			executor().execute("asset.query.iterator.destroy",
					"<args><ignore-missing>true</ignore-missing><id>" + iteratorID + "</id></args>", null, null);
			throw e;
		}
	}
}

