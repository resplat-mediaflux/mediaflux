/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.contentcopy;


import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetProcessingQueue;
import au.org.vicnode.mflux.plugin.util.ProjectContentCopy;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectContentCopyQueueRecreate extends PluginService {


	private Interface _defn;

	public SvcProjectContentCopyQueueRecreate()  throws Throwable {
		_defn = new Interface();
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "This service destroys and re-creates the asset processing queue used to make content copies.  Any queued entries just before the queue is destroyed will be re-queued. Any copy actually being processed.  This service is the same as the rebuild service except it does NOT unset and reset the queue on the project asset namespace.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.content-copy.queue.recreate";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Check the content copy  environment is configured		
		if (!ProjectContentCopy.isConfigured(executor())) {
			throw new Exception ("Replications are not configured on this system");
		}

		// Recreate
		recreate (executor(), w);
	}	


	private  void recreate (ServiceExecutor executor, XmlWriter w) throws Throwable {

		// In this approach we assert that the :enqueue element
		// set on project namespaces refers to the queue by name
		// not id.  So we don't actually need to unset the queue
		// from the namespace (like the rebuild service does)

		// Suspend the queue
		String qName = ProjectContentCopy.createOrFindStandardQueue(executor, null);
		AssetProcessingQueue.suspendQueue(executor, qName);

		// Describe existing active entries as we will requeue them
		XmlDoc.Element d = AssetProcessingQueue.describeQueue(executor, qName, true);
		Collection<String> entries = d.values("entry/id");

		// Now destroy  the asset processing queue
		AssetProcessingQueue.destroyQueue(executor, qName, false);
		w.push("destroy");
		w.add("queue", new String[] {"destroyed", "true"}, qName);
		w.pop();

		// Now rebuild (picking up application properties 
		// content-copy.queue.nb-processors
		// content-copy.queue.store.name
		w.push("recreate");
		qName = ProjectContentCopy.createOrFindStandardQueue(executor, w);
		if (entries!=null && entries.size()>0) {
			w.push("entries");
			for (String entry : entries) {
				w.add("id", entry);
				AssetProcessingQueue.addAssetToQueue (executor, qName, entry);
			}
			w.pop();
		}
		w.pop();
	}
}
