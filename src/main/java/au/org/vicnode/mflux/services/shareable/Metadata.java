package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDocMaker;

public class Metadata {

    public static final String METADATA_FILE_NAME = "__metadata.xml";

    public static String createMetadataAsset(ServiceExecutor executor, String uploadNamespace, String keywords,
                                             String dataNote) throws Throwable {

        String path = String.format("%s/%s", uploadNamespace, METADATA_FILE_NAME);
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", "path=" + path);
        dm.add("create", true);
        dm.push("xml-content");
        dm.push("metadata");
        if (keywords != null) {
            dm.add("keywords", keywords);
        }
        if (dataNote != null) {
            dm.add("data-note", dataNote);
        }
        dm.pop();
        dm.pop();
        executor.execute("asset.set", dm.root());

        dm = new XmlDocMaker("args");
        dm.add("id", "path=" + path);
        return executor.execute("asset.identifier.get", dm.root()).value("id");
    }
}
