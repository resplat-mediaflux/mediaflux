package au.org.vicnode.mflux.services.shareable;

import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcFacilityShareableUploadMappingSet extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.mapping.set";
    private Interface _defn;

    public SvcFacilityShareableUploadMappingSet() {
        _defn = new Interface();

        _defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "The id of the upload shareable.", 1, 1));

        Interface.Element rule = new Interface.Element("rule", XmlDocType.DEFAULT, "Mapping rule.", 0,
                Integer.MAX_VALUE);
        Interface.Element source = new Interface.Element("source", StringType.DEFAULT,
                "The regular expression pattern to test the context parent path of the upload.", 1, 1);
        source.add(new Interface.Attribute("ignore-case", BooleanType.DEFAULT, "Ignore case. Defaults to false.", 0));
        rule.add(source);

        Interface.Element destination = new Interface.Element("destination", StringType.DEFAULT,
                "The destination path pattern.", 1, 1);
        destination.add(new Interface.Attribute("create", BooleanType.DEFAULT,
                "Create parent namespaces if not exist. Defaults to false.", 0));
        rule.add(destination);
        _defn.add(rule);

        _defn.add(new Interface.Element("worm", BooleanType.DEFAULT,
                "Set the mapping asset to worm state that allows only to update but not to destroy or move. Defaults to false.",
                0, 1));

        _defn.add(new Interface.Element("add-note", BooleanType.DEFAULT,
                "Add mf-note meta data to the moved assets to trigger replication. Defaults to true.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Set the upload destination mapping rules. These rules will be stored in an asset under the asset namespace of the upload shareable.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String shareableId = args.value("shareable-id");
        boolean worm = args.booleanValue("worm", false);
        boolean addNote = args.booleanValue("add-note", true);
        List<UploadMapping.Rule> rules = UploadMapping.Rule.parse(args.elements("rule"));
        UploadMapping mapping = UploadMapping.set(executor(), shareableId, rules, worm, addNote);
        mapping.describe(w);
    }

}
