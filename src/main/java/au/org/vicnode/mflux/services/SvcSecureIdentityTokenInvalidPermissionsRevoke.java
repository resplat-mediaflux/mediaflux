package au.org.vicnode.mflux.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcSecureIdentityTokenInvalidPermissionsRevoke extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT +  "secure.identity.token.invalidPermissions.find";

	private String project_admin = "administrator";
	private String project_fulledit = "participant-acmd-n";
	private Interface _defn;

	public SvcSecureIdentityTokenInvalidPermissionsRevoke() {
		_defn = new Interface();
		_defn.add(new Interface.Element("email", EmailAddressType.DEFAULT,
				"If any tokens were found, email the result to the specified recipient(s).", 0, 5));
		_defn.add(new Interface.Element("revoke-roles", BooleanType.DEFAULT,
				"Revoke extra roles of tokens and destroy token if no roles", 0, 1));

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Find the tokens whose owner accounts dont have the correct permissions anymore.";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(arc.xml.XmlDoc.Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		boolean destroy = args.booleanValue("revoke-roles", false);
		Collection<String> emails = args.values("email");
		StringBuilder sb = (emails == null || emails.isEmpty()) ? null : beginEmailMessage();
		int count = 0;
		List<XmlDoc.Element> ies = describeAllIdentityTokens(executor());
		if (ies != null) {
			for (XmlDoc.Element ie : ies) {
				PluginTask.checkIfThreadTaskAborted();
				String ownerDomain = ie.value("owner/domain");
				String ownerUser = ie.value("owner/user");
				String ownerName = ie.value("owner/name");
				String ownerEmail = ie.value("owner/email");
				if (!userExists(executor(), ownerDomain, ownerUser)){
					continue;
				}
				Collection<String> excessPermissions = findExcessPermissions(executor(), ownerDomain, ownerUser, ie, w);
				if (!excessPermissions.isEmpty()) {
					String tokenId = ie.value("@id");
					String actorName = ie.value("actor");
					if (destroy) {
						String lifetime = ie.value("@lifetime");
						destroyExcessPermissions(executor(), tokenId,actorName, lifetime,excessPermissions);
						if (ownerEmail != null && ownerEmail.contains("@")) {
							emailTokenRoleRevoked(executor(), ownerEmail, excessPermissions);
						}
					}
					w.push("identity", new String[] { "id", tokenId, "destroyed", Boolean.toString(destroy) });
					w.add("actor", new String[] { "type", "identity" }, actorName);
					w.add("domain", ownerDomain);
					w.add("user", ownerUser);
					w.add("name", ownerName);
					w.add("email", ownerEmail);
					w.pop();
					if (sb != null) {
						sb.append("<tr>");
						sb.append("<td>").append(tokenId).append("</td>");
						sb.append("<td>").append(actorName).append("</td>");
						sb.append("<td>").append(ownerDomain + ":" + ownerUser).append("</td>");
						sb.append("<td>").append(destroy ? "Yes" : "No").append("</td>");
						sb.append("<td>").append(excessPermissions.toString()).append("</td>");
						sb.append("</tr>");
					}
					count++;
				}
			}
		}
		w.add("count", count);
		if (sb != null && count > 0) {
			endEmailMessage(sb, count);
			sendEmails(executor(), emails, count, sb.toString());
			for (String email : emails) {
				w.add("sent-to", email);
			}
		}
	}

	private void emailTokenRoleRevoked(ServiceExecutor executor,  String ownerEmail, Collection<String> excessPermissions) throws Throwable {
		String subject = "Roles revoked for one of your tokens";
        String body = String.format("Hi, this is to let you know that one of your tokens has one or more permissions revoked as you no longer have that permission with your user. The revoked permissions are: %s. Regards, mediaflux team \n",excessPermissions);
        sendEmail(executor, Collections.singleton(ownerEmail), subject, body);
	}

	private Collection<String>  extractProjectRoles(List<XmlDoc.Element> rolesElement) throws Throwable {
		Collection<String> roles = new ArrayList<String>();
		if (rolesElement != null) {
			//w.add("roles",userRoles.toString());
			for (XmlDoc.Element role : rolesElement) {
				if (role.value("@type").equals("role") && role.value().startsWith("proj")) {
					roles.add(role.value());
				}
			}
		}
		return roles;
	}

	private Collection<String> findExcessPermissions(ServiceExecutor executor, String ownerDomain, String ownerUser, XmlDoc.Element ie, XmlWriter w) throws Throwable {
		Collection<String> excessPerms = new ArrayList<String>();
		if (ownerDomain.equals("system") || ownerDomain.equals("local-admin")) {
			return excessPerms;
		}

		List<XmlDoc.Element> tokenRolesElement = ie.elements("role");
		Collection<String> roles = extractProjectRoles(tokenRolesElement);
		//w.add("roles", roles);

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("user", ownerUser);
		dm.add("domain", ownerDomain);
		dm.add("permissions",true);

		List<XmlDoc.Element> userRolesElement = executor.execute("user.describe", dm.root()).elements("user/role");

		Collection<String> userRoles = extractProjectRoles(userRolesElement);
		// if a user has a role ending with administrator, add a role with same project ending with participant-acmd-n
		// check if token has a role that the user does not have
		// if a token role is a subset of a user role, the user has the permission
		Collection<String> userRoles1 = new ArrayList<String>();
		for (String role : userRoles) {
			if (role.endsWith(project_admin)) {
				String project = role.substring(0, role.length() - project_admin.length());
				userRoles1.add(project + project_fulledit);
			}
		}
		userRoles.addAll(userRoles1);
		//w.add("user-roles", userRoles);

		for (String tokenRole : roles) {
			boolean hasPermission = false;
			for (String userRole : userRoles) {
				if (tokenRole.equals(userRole) || userRole.startsWith(tokenRole)) {
					hasPermission = true;
					break;
				}
			}
			if (!hasPermission) {
				w.add("perm", false);
				w.add("tokenperm",tokenRole);
				excessPerms.add(tokenRole);
			}
		}

		if (!excessPerms.isEmpty()) {
			w.add("userperm",userRoles);
		}
		return excessPerms;
	}

	private static List<XmlDoc.Element> describeAllIdentityTokens(ServiceExecutor executor) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("all-users", true);
		dm.add("size", "infinity");
		dm.add("permissions",true);
		return executor.execute("secure.identity.token.describe", dm.root()).elements("identity");
	}

	private static boolean userExists(ServiceExecutor executor, String domain, String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		boolean domainExists = executor.execute("authentication.domain.exists", dm.root()).booleanValue("exists");
		if (!domainExists) {
			return false;
		}
		dm.add("user", user);
		return executor.execute("user.exists", dm.root()).booleanValue("exists");
	}

	private void destroyExcessPermissions(ServiceExecutor executor, String id,String actorName, String lifetime, Collection<String> excessPermissions) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name",actorName);
		dm.add("type","identity");
		for (String perm : excessPermissions) {
            dm.add("role", new String[]{"type", "role"}, perm);
        }
		executor.execute("actor.revoke", dm.root());

		/*XmlDocMaker dm1 = new XmlDocMaker("args");
		dm1.add("name",actorName);
		dm1.add("type","identity");
		List<XmlDoc.Element> tokenRolesNow = executor.execute("actor.describe", dm1.root()).elements("actor/role");

		if (extractProjectRoles(tokenRolesNow).isEmpty()) {
            XmlDocMaker dm2 = new XmlDocMaker("args");
            dm2.add("id", new String[]{"lifetime", lifetime}, id);
            executor.execute("secure.identity.token.destroy", dm2.root());
        }*/
	}

	private static StringBuilder beginEmailMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("<html>\n");
		sb.append("<head>\n");
		sb.append("<style>\n");
		sb.append("table, th, td { border: 1px solid black; }\n");
		sb.append("tr:nth-child(even) {background: #CCC}\n");
		sb.append("tr:nth-child(odd) {background: #FFF}\n");
		sb.append("</style>\n");
		sb.append("</head>\n");
		sb.append("<body>\n");
		sb.append("<h3>Secure Identity Tokens with Permissionless Owners</h3><br>");
		sb.append("<table>");
		sb.append(
				"<thead><tr><th>ID</th><th>Actor Name</th><th>Owner</th><th>Role Revoked or Destroyed</th><th>Excess Permissions</th></tr></thead>");
		sb.append("<tbody>");
		return sb;
	}

	private static void endEmailMessage(StringBuilder sb, int count) {
		sb.append("<tbody></table>");
		sb.append("<ul><li><b>Count:</b>").append(count).append("</li></ul>");
		sb.append("</body></html>");
	}

	private static void sendEmails(ServiceExecutor executor, Collection<String> emails, int nbInvalidTokens,
			String message) throws Throwable {
		String serverUUID = executor.execute("server.uuid").value("uuid");

		String subject = "[" + serverUUID + "] Found " + nbInvalidTokens
				+ " secure identity tokens with excess permissions";
		sendEmail(executor,emails, subject, message);
	}

	private static void sendEmail(ServiceExecutor executor, Collection<String> emails, String subject, String message) throws Throwable {
		try {
			XmlDocMaker dm = new XmlDocMaker("args");
			for (String email : emails) {
				dm.add("to", email);
			}


			dm.add("subject", subject);
			dm.add("body", new String[]{"type", "text/html"}, message);
			executor.execute("mail.send", dm.root());
		}catch (Throwable ignored){
		}
	}

}
