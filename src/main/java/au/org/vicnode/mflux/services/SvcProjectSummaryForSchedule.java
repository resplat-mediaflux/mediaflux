/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import java.util.HashSet;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectSummaryForSchedule extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectSummaryForSchedule()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("frequency", new EnumType(Properties.notificationFrequency), 
				"Frequency to notify the selected project members or owner.", 1, 1));
		_defn.add(new Interface.Element("provision", BooleanType.DEFAULT, "Notify the standard provisioning user (that holds the admin role for all projects) as well (default false) .", 0, 1));
		_defn.add(new Interface.Element("send", BooleanType.DEFAULT, "Actually send the notifications (default).  If false, return the messages to the terminal but don't send the user notifications.", 0, 1));
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
		_defn.add(new Interface.Element("sum", BooleanType.DEFAULT, "Sum project content on primary and DR systems (defaults to false). If false, just rely on quota usage.",
				0, 1));
		_defn.add(new Interface.Element("ingest", BooleanType.DEFAULT, "Include ingest rates (last 7/30 days) in message. Defaults to true.",
						0, 1));	
	    _defn.add(new Interface.Element("name-validation", BooleanType.DEFAULT,
						"Include a listing of assets (could be a lot!) that are found to have name validation cross-operating system inconsistency issues.  Defaults to true.", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Sends a summary message of the project to configured recipients of projects that match the given frequency.  Projects are not resummed - we just use the namespace quota/usage.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.summary.for.schedule";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String freq = args.value("frequency");
		Boolean dropProvision = !(args.booleanValue("provision", false));
		Boolean send  = args.booleanValue("send", true);
		String theProjectID = args.value("project-id");
		Boolean doSum = args.booleanValue("sum", false);
		Boolean doIngest = args.booleanValue("ingest", true);
		Boolean nameValidation = args.booleanValue("name-validation", true);

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID != null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else {
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size() == 0) {
			return;
		}

		// Iterate through projects
		String prodType = Properties.PROJECT_TYPE_ALL;
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);

			if (Project.keepOnProjectType(ph, prodType)) {
				Boolean some = false;
				w.mark();
				w.push("project", new String[]{"id", projectID});

				// Fetch meta-data from $NS_ADMIN:Project/notification
				XmlDoc.Element nsMeta = ph.metaData();
				XmlDoc.Element m = nsMeta.element("namespace/asset-meta/" + Properties.getProjectDocType(executor()));
				if (m!=null) {
					Collection<XmlDoc.Element> ss = m.elements("notification/summary");
					if (ss!=null) {
						HashSet<String> emails = new HashSet<String>();        // WIll make unique list

						for (XmlDoc.Element s : ss) {
							String who = s.value();
							String when = s.value("@frequency");
							String email = s.value("@email");
							//
							if (when.equals(freq)) {
								if (addUsers (executor(), who, email, ph.id(), dropProvision, emails)) {
									some = true;
								}
							}
						}

						// If there is nobody to write to we don't proceed.
						Integer ingestDays = null;
						if (doIngest) {
							if (freq.equals("weekly")) {
								ingestDays = 7;
							} else if (freq.equals("monthly")) {
								ingestDays = 30;
							}
						}
						Integer nameValidationDays = null;
						if (nameValidation) {
							if (freq.equals("weekly")) {
								nameValidationDays = 7;
							} else if (freq.equals("monthly")) {
								nameValidationDays = 30;
							}
						}				
						SvcProjectSummaryForMembers.makeAndSendSummary (executor(), projectID,
								nameValidationDays, doSum, ingestDays, emails, send, w);				
					}
				}
				w.pop();
				if (!some) {
					//					w.resetToMark();
				}
			}
		}
	}

	public static boolean addUsers (ServiceExecutor executor, String who, String email, String projectID, Boolean dropProvision, HashSet<String> emails) throws Throwable {

		// Generate emails
		if (who==null) {
			return false;
		}
		int n1 = emails.size();
		if (!who.equals("none")) {
			Collection<String> t =Project.listProjectEmails(executor, projectID, who, dropProvision);
			if (t!=null && t.size()>0) {
				emails.addAll(t);
			}
		}
		if (email!=null) {
			// See if the email string contains multiple comma separated emails
			String[] t = email.split(",");
			for (int i=0;i<t.length;i++) {
				emails.add(t[i].trim());
			}
		}
		int n2 = emails.size();
		return n2>n1;
	}
}
