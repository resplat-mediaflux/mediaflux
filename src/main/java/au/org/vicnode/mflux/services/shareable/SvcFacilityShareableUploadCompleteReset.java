package au.org.vicnode.mflux.services.shareable;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.Session;
import arc.mf.plugin.atomic.AtomicTransaction;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.LongType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Attribute;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;

public class SvcFacilityShareableUploadCompleteReset extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.complete.reset";

    private final Interface _defn;

    public SvcFacilityShareableUploadCompleteReset() {
        _defn = new Interface();
        _defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "Id of the (upload) shareable.", 1, 1));
        _defn.add(new Interface.Element("upload-id", LongType.POSITIVE, "Id of the upload.", 1, 1));
        _defn.add(new Interface.Element("generate-reprocess-command", BooleanType.DEFAULT,
                "Generate service command to reprocess the upload. Defaults to true.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Reset the upload by deleting the associated manifest asset, download shareable, direct download link token and user interaction. Also, generate the service command for reprocessing the upload. Note: if the manifest asset is in WORM state, this service must be executed as system:manager.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

        String shareableId = args.value("shareable-id");
        String uploadId = args.value("upload-id");
        boolean generateReprocessCommand = args.booleanValue("generate-reprocess-command", true);

        boolean hasSystemAdministratorRole = User.hasSystemAdministratorRole(executor());

        if (!hasSystemAdministratorRole) {
            throw new Exception("You must have system-administrator role to execute this service.");
        }

        XmlDoc.Element me = executor().execute(SvcFacilityShareableUploadManifestDescribe.SERVICE_NAME, args)
                .element("shareable-upload-manifest");
        String primaryManifestAssetId = me.value("@id");

        boolean worm = me.booleanValue("@worm");

        if (worm && !("system".equals(Session.user().domain()) && "manager".equals(Session.user().name()))) {
            throw new Exception("You must execute this service as system:manager because manifest asset "
                    + primaryManifestAssetId + " is in WORM state.");
        }

        Set<String> manifestAssetIds = findAllManifestAssets(executor(), shareableId, uploadId);

        if (!manifestAssetIds.contains(primaryManifestAssetId)) {
            throw new Exception("asset.query returned " + manifestAssetIds.size()
                    + " manifest assets. However, it does not include the primary manifest asset "
                    + primaryManifestAssetId);
        }

        XmlDoc.Element reprocessCommand = generateReprocessCommand
                ? generateReprocessCommand(executor(), shareableId, uploadId, me)
                : null;

        new AtomicTransaction(executor -> {

            String downloadShareableId = me.value("download-shareable/id");
            if (downloadShareableId != null) {
                destroyDownloadShareable(executor, downloadShareableId);
            }

            String userInteractionId = me.value("user-interaction/id");
            if (userInteractionId != null) {
                destroyUserInteraction(executor, userInteractionId);
            }

            String directDownloadTokenId = me.value("direct-download/token-id");
            if (directDownloadTokenId != null) {
                destroyDirectDownloadToken(executor, directDownloadTokenId);
            }

            destroyManifestAssets(executor, manifestAssetIds);

            w.add("download-shareable", new String[]{"id", downloadShareableId, "destroyed", "true"});
            w.add("user-interaction", new String[]{"id", userInteractionId, "destroyed", "true"});
            w.add("direct-download", new String[]{"token-id", directDownloadTokenId, "destroyed", "true"});
            for (String assetId : manifestAssetIds) {
                String type = assetId.equals(primaryManifestAssetId) ? "original" : "copy";
                w.add("manifest-asset", new String[]{"id", assetId, "type", type, "destroy", "true"});
            }
            if (reprocessCommand != null) {
                w.add(reprocessCommand, true);
            }
            return false;
        }).execute(executor());

    }

    private static void destroyDownloadShareable(ServiceExecutor executor, String downloadShareableId)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", downloadShareableId);
        executor.execute("asset.shareable.destroy", dm.root());
    }

    private static void destroyUserInteraction(ServiceExecutor executor, String userInteractionId) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", userInteractionId);
        executor.execute("automation.user.interaction.destroy", dm.root());
    }

    private static void destroyDirectDownloadToken(ServiceExecutor executor, String tokenId) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", new String[]{"lifetime", "persistent"}, tokenId);
        executor.execute("secure.identity.token.destroy", dm.root());
    }

    private static Set<String> findAllManifestAssets(ServiceExecutor executor, String shareableId, String uploadId)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("action", "get-id");
        String where = "xpath(unimelb:shareable-upload-manifest/shareable/@id)='" + shareableId
                + "' and xpath(unimelb:shareable-upload-manifest/upload/@id)='" + uploadId + "'";
        dm.add("where", where);

        Collection<String> ids = executor.execute("asset.query", dm.root()).values("id");

        int count = ids == null ? 0 : ids.size();
        if (count < 1) {
            throw new Exception("asset.query :where \"" + where + "\" returned " + count
                    + " manifest assets. Expects at least 1.");
        }

        if (count > 2) {
            throw new Exception("asset.query :where \"" + where + "\" returned " + count
                    + " manifest assets. Expects at most 2.");
        }

        return new LinkedHashSet<>(ids);
    }

    private static void destroyManifestAssets(ServiceExecutor executor, Collection<String> assetIds) throws Throwable {
        if (assetIds != null && !assetIds.isEmpty()) {
            XmlDocMaker dm = new XmlDocMaker("args");
            for (String assetId : assetIds) {
                XmlDoc.Element ae = executor.execute("asset.get", "<args><id>" + assetId + "</id></args>", null, null)
                        .element("asset");
                if (ae.elementExists("worm-status")) {
                    unsetWorm(executor, assetId);
                }
                dm.add("id", assetId);
            }
            executor.execute("asset.destroy", dm.root());
        }
    }

    private static void unsetWorm(ServiceExecutor executor, String assetId) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", assetId);
        dm.add("force", true);
        dm.add("expiry", "today-1day");
        executor.execute("asset.worm.expiry.set", dm.root());
    }

    private static XmlDoc.Element generateReprocessCommand(ServiceExecutor executor, String shareableId,
                                                           String uploadId, XmlDoc.Element manifest) throws Throwable {

        XmlDoc.Element shareable = Shareable.describe(executor, shareableId);
        String ownerDomain = shareable.value("owner/domain");
        String ownerUsername = shareable.value("owner/user");
        XmlDoc.Element shareableCompleteServiceOptions = shareable
                .element("completion-service[@name='vicnode.facility.shareable.upload.complete']");
        List<XmlDoc.Element> uploadArgs = manifest.elements("upload/arg");
        String reprocessCommand = generateReprocessCommand(shareableId, uploadId, shareableCompleteServiceOptions,
                uploadArgs);
        XmlDocMaker dm0 = new XmlDocMaker("reprocess");
        dm0.add("user", ownerDomain + ":" + ownerUsername);
        dm0.add("command", reprocessCommand);
        return dm0.root();
    }

    private static String generateReprocessCommand(String shareableId, String uploadId,
                                                   XmlDoc.Element shareableCompleteServiceOptions, List<XmlDoc.Element> uploadArgs) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("shareable-id", shareableId);
        dm.add("upload-id", uploadId);
        dm.add(shareableCompleteServiceOptions, false);
        if (uploadArgs != null && !uploadArgs.isEmpty()) {
            dm.push("args");
            for (XmlDoc.Element arg : uploadArgs) {
                String argName = arg.value("@name");
                String argValue = arg.value();
                dm.add(argName, argValue);
            }
            dm.pop();
        }

        StringBuilder sb = new StringBuilder(SvcFacilityShareableUploadComplete.SERVICE_NAME);
        convertXmlToShell(dm.root(), false, sb);
        return sb.toString();
    }

    static void convertXmlToShell(XmlDoc.Element e, boolean includeRoot, StringBuilder sb) {
        if (includeRoot) {
            sb.append(" :").append(e.name());
            if (e.hasAttributes()) {
                for (Attribute attr : e.attributes()) {
                    sb.append(" -").append(attr.name()).append(" \"").append(attr.value()).append("\"");
                }
            }
        }
        if (e.hasSubElements()) {
            if (includeRoot) {
                sb.append(" <");
            }
            for (XmlDoc.Element se : e.elements()) {
                convertXmlToShell(se, true, sb);
            }
            if (includeRoot) {
                sb.append(" >");
            }
        }
        if (includeRoot) {
            if (e.hasValue()) {
                sb.append(" \"").append(e.value()).append("\"");
            }
        }
    }

}
