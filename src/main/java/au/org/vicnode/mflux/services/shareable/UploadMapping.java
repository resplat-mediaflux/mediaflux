package au.org.vicnode.mflux.services.shareable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetNamespaceUtil;
import au.org.vicnode.mflux.plugin.util.AssetUtil;
import au.org.vicnode.mflux.plugin.util.Project.ProjectRoleType;
import unimelb.utils.AssetPathUtils;

public class UploadMapping {

    public static final String DOC_TYPE = "unimelb:shareable-upload-mapping";

    public static final String ASSET_NAMESPACE = "ShareableMapping";
    public static final String ASSET_NAME_SUFFIX = ".mapping";

    public static class Rule {

        public final String srcPattern;
        public final boolean srcIgnoreCase;
        public final String dstPattern;
        public final boolean dstCreate;

        public Rule(XmlDoc.Element re) throws Throwable {
            this.srcPattern = re.value("source");
            this.srcIgnoreCase = re.booleanValue("source/@ignore-case", false);
            this.dstPattern = re.value("destination");
            this.dstCreate = re.booleanValue("destination/@create", false);
        }

        public void saveXml(XmlDocMaker dm) throws Throwable {
            dm.push("rule");
            dm.add("source", new String[] { "ignore-case", this.srcIgnoreCase ? "true" : null }, this.srcPattern);
            dm.add("destination", new String[] { "create", Boolean.toString(this.dstCreate) }, this.dstPattern);
            dm.pop();
        }

        public void describe(XmlWriter w) throws Throwable {
            w.push("rule");
            w.add("source", new String[] { "ignore-case", this.srcIgnoreCase ? "true" : null }, this.srcPattern);
            w.add("destination", new String[] { "create", Boolean.toString(this.dstCreate) }, this.dstPattern);
            w.pop();
        }

        /**
         * Evaluate the rule against the specified value.
         * 
         * @param srcParentPath
         * @return the destination if the value matches the source pattern; otherwise,
         *         return null;
         */
        protected String evaluate(String srcParentPath) {
            if (srcParentPath == null) {
                return null;
            }
            Pattern sp = Pattern.compile(this.srcPattern, this.srcIgnoreCase ? Pattern.CASE_INSENSITIVE : 0);
            Matcher m = sp.matcher(srcParentPath);
            if (m.matches()) {
                return m.replaceAll(this.dstPattern);
            }
            return null;
        }

        public static List<Rule> parse(List<XmlDoc.Element> res) throws Throwable {
            List<Rule> rules = new ArrayList<>();
            if (res != null) {
                for (XmlDoc.Element re : res) {
                    rules.add(new Rule(re));
                }
            }
            return rules;
        }
    }

    public static class Result {
        public final String srcContextPath;
        public final Rule rule;
        public final String dstParentPath;

        public Result(String srcContextPath, Rule rule, String dstParentPath) {
            this.srcContextPath = srcContextPath;
            this.rule = rule;
            this.dstParentPath = dstParentPath;
        }
    }

    private String _assetId;
    private boolean _worm;
    private String _shareableId;
    private List<Rule> _rules;
    private boolean _addNote;

    UploadMapping(XmlDoc.Element ae) throws Throwable {
        _assetId = ae.value("@id");
        _worm = ae.elementExists("worm-status");
        _rules = new ArrayList<Rule>();
        XmlDoc.Element me = ae.element("meta/" + DOC_TYPE);
        if (me != null) {
            _shareableId = me.value("shareable-id");
            List<XmlDoc.Element> res = me.elements("rule");
            _rules = Rule.parse(res);
        }
        _addNote = me == null ? true : me.booleanValue("add-note", true);
    }

    public void describe(XmlWriter w) throws Throwable {
        w.push("shareable-upload-mapping", new String[] { "asset-id", _assetId, "worm", Boolean.toString(_worm) });
        w.add("shareable-id", _shareableId);
        for (Rule rule : _rules) {
            rule.describe(w);
        }
        w.pop();
    }

    /**
     * Evaluate the rules against the specified value. It returns the destination of
     * the first matching rule. If no rule matches it returns null.
     * 
     * @param srcContextPath the source context path. Note: it includes the upload
     *                       folder name as the last path component. It need to be
     *                       trimmed before evaluating with the mapping rules.
     * @return the destination returned by the first matching rule. If no rule
     *         matches it returns null;
     */
    public Result evaluate(String srcContextPath) {
        logWarning("src context path: " + srcContextPath);
        String srcParentPath = AssetPathUtils.getParent(srcContextPath);
        logWarning("src parent path: " + srcParentPath);
        for (Rule rule : _rules) {
            logWarning("src parent pattern: " + rule.srcPattern + " (ignore-case=" + rule.srcIgnoreCase + ")");
            logWarning("dst parent pattern: " + rule.dstPattern);
            String dstParentPath = rule.evaluate(srcParentPath);
            logWarning("matches=" + (dstParentPath != null));
            if (dstParentPath != null) {
                logWarning("dst parent path: " + dstParentPath);
                return new Result(srcParentPath, rule, dstParentPath);
            }
        }
        return null;
    }

    public String assetId() {
        return _assetId;
    }

    public String shareableId() {
        return _shareableId;
    }

    public List<Rule> rules() {
        return Collections.unmodifiableList(_rules);
    }

    public boolean worm() {
        return _worm;
    }

    public boolean addNote() {
        return _addNote;
    }

    static String getMappingAssetName(String shareableId) throws Throwable {
        return shareableId + ASSET_NAME_SUFFIX;
    }

    static String getMappingAssetNamespace(XmlDoc.Element shareable) throws Throwable {
        String shareableNS = shareable.value("namespace");
        String assetNS = String.format("%s/%s", shareableNS, ASSET_NAMESPACE);
        return assetNS;
    }

    static String getMappingAssetPath(XmlDoc.Element shareable) throws Throwable {
        String shareableId = shareable.value("@id");
        String assetNS = getMappingAssetNamespace(shareable);
        String assetName = getMappingAssetName(shareableId);
        String assetPath = String.format("%s/%s", assetNS, assetName);
        return assetPath;
    }

    private static void setWorm(ServiceExecutor executor, String assetId) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", assetId);
        dm.add("can-add-versions", true);
        dm.add("can-alter-if", "can-modify");
        dm.add("can-move", false);
        executor.execute("asset.worm.set", dm.root());
    }

    private static void createMappingAssetNamespace(ServiceExecutor executor, String assetNS) throws Throwable {
        if (!AssetNamespaceUtil.assetNamespaceExists(executor, assetNS)) {
            AssetNamespaceUtil.createAssetNamespace(executor, assetNS, false);
            String projectId = Projects.resolveProjectIdFromNSPath(assetNS);
            String projectAdminRole = Projects.getProjectRole(projectId, ProjectRoleType.ADMIN);
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("namespace", assetNS);
            dm.push("visible-to");
            dm.add("actor", new String[] { "type", "role" }, projectAdminRole);
            dm.pop();
            executor.execute("asset.namespace.visibility.set", dm.root());
        }
    }

    static UploadMapping set(ServiceExecutor executor, XmlDoc.Element shareable, List<Rule> rules, boolean worm,
            boolean addNote) throws Throwable {
        String shareableId = shareable.value("@id");
        String assetNamespace = getMappingAssetNamespace(shareable);
        createMappingAssetNamespace(executor, assetNamespace);
        String assetName = getMappingAssetName(shareableId);
        String assetPath = getMappingAssetPath(shareable);
        String assetId = "path=" + assetPath;

        /*
         * create/update asset
         */
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", assetId);
        dm.add("create", true);
        dm.add("name", assetName);
        dm.push("meta", new String[] { "action", "replace" });
        dm.push(DOC_TYPE);
        dm.add("shareable-id", shareableId);
        for (Rule rule : rules) {
            rule.saveXml(dm);
        }
        dm.add("add-note", addNote);
        dm.pop();
        dm.pop();
        executor.execute("asset.set", dm.root());
        XmlDoc.Element ae = AssetUtil.getAssetMeta(executor, assetId);
        assetId = ae.value("@id");

        /*
         * set worm
         */
        if (worm && !ae.elementExists("worm-status")) {
            setWorm(executor, assetId);
            ae = AssetUtil.getAssetMeta(executor, assetId);
        }

        return new UploadMapping(ae);
    }

    static UploadMapping set(ServiceExecutor executor, String shareableId, List<Rule> rules, boolean worm,
            boolean addNote) throws Throwable {
        return set(executor, Shareable.describe(executor, shareableId), rules, worm, addNote);
    }

    static void unset(ServiceExecutor executor, XmlDoc.Element shareable, boolean destroyAsset) throws Throwable {
        String shareableId = shareable.value("@id");
        String assetPath = getMappingAssetPath(shareable);
        String assetId = "path=" + assetPath;
        if (!AssetUtil.assetExists(executor, assetId)) {
            return;
        }

        if (destroyAsset) {
            AssetUtil.destroyAsset(executor, assetId);
        } else {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("id", assetId);
            dm.push("meta", new String[] { "action", "replace" });
            dm.push(DOC_TYPE);
            dm.add("shareable-id", shareableId);
            dm.pop();
            dm.pop();
            executor.execute("asset.set", dm.root());
        }
    }

    static UploadMapping get(ServiceExecutor executor, XmlDoc.Element shareable) throws Throwable {
        String assetPath = getMappingAssetPath(shareable);
        String assetId = "path=" + assetPath;
        if (!AssetUtil.assetExists(executor, assetId)) {
            return null;
        }
        XmlDoc.Element ae = AssetUtil.getAssetMeta(executor, assetId);
        // return null if no mapping rule.
        if (!ae.elementExists("meta/" + DOC_TYPE + "/rule")) {
            return null;
        }
        return new UploadMapping(ae);
    }

    private static PluginLog log() {
        // facility-shareable-upload.log
        return SvcFacilityShareableUploadComplete.log();
    }

    private void logWarning(String msg) {
        log().add(PluginLog.WARNING,
                String.format("[shareable: %s, asset: %s] mapping: %s", this.shareableId(), this.assetId(), msg));
    }
}
