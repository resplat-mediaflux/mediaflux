/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectPosixMapDomainIdentityAdd extends PluginService {

	// When posix mount points are provisioned, the domain security identifier
	// must be included in the identity map.  It's required only for some
	// SMB clients.  We have too many domains to be able to iterate through them
	// all automatically (many are irrelevant to this process) so we list them
	// here.   If the domains do not exist, then no action is taken.
	private static String[] Types = {"local", "ldap", "all"};

	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectPosixMapDomainIdentityAdd()  throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID(_defn, allowPartialCID);
		_defn.add(new Interface.Element("type", new EnumType(Types),
				"Which domain type should the identity be added for ?  Choose from 'local', 'ldap' and 'all' (ldap and local). Default is 'all'.", 0, 1));
		_defn.add(new Interface.Element("fail-if-not-enabled", 
				BooleanType.DEFAULT, "If the authentication domain (standard AD and local domains) is not enabled, either fail or simply don't add the identity (default) to the identity map. In the latter case, the domain will just be listed indicating it is not enabled. ", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Adds the authentication domain identities (for our standard LDAP [unimelb,student] and local [local] domains) into a posix map for SMB protocol.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.posix.map.domain.identity.add";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Parse arguments
		String theProjectID = args.stringValue("project-id");
		Boolean failIfNotEnabled = args.booleanValue("fail-if-not-enabled",  false);
		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) return;
		//
		String type = args.value("type");

		// Iterate over projects
		for (String projectID : projectIDs) {
			w.push("project");
			w.add("id", projectID);

			//
			if (type.equals("all") || type.equals("ldap")) {
				addLDAPDomainSIDs(executor(), projectID, failIfNotEnabled, w);
			} else if (type.equals("all") || type.equals("local")) {
				addLocalDomainSIDs(executor(), projectID, failIfNotEnabled, w);
			}
			w.pop();
		}
	}

	public static void addLDAPDomainSIDs (ServiceExecutor executor, 
			String projectID, Boolean failIfNotEnabled, 
			XmlWriter w) throws Throwable {

		addLDAPDomainSID(executor, Properties.getLDAPStaffDomain(executor), projectID, failIfNotEnabled, w);
		addLDAPDomainSID(executor, Properties.getLDAPStudentDomain(executor), projectID, failIfNotEnabled, w);	
	}

	public static void addLocalDomainSIDs (ServiceExecutor executor, 
			String projectID, Boolean failIfNotEnabled,
			XmlWriter w) throws Throwable {

		addLocalDomainSID(executor, Properties.getUserAuthDomain(executor), 
				projectID, failIfNotEnabled, w);
	}


	private static void addLDAPDomainSID (ServiceExecutor executor, String domain, 
			String projectID, Boolean failIfNotEnabled, XmlWriter w) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		XmlDoc.Element r = executor.execute("authentication.domain.exists", dm.root());
		if (r.booleanValue("exists")) {
			dm = new XmlDocMaker("args");
			dm.add("domain", domain);
			r = executor.execute("authentication.domain.describe", dm.root());
			String type = r.value("domain/@type");
			String protocol = r.value("domain/@protocol");
			Boolean enabled = r.booleanValue("domain/@enabled", true);
			if (!enabled) {
				if (failIfNotEnabled) {
					throw new Exception ("The domain '" + domain + "is not enabled");
				} else {
					w.push("identity");
					w.add("domain", new String[]{"type", "external", "protocol", "ldap", "enabled", "false"}, domain);	
					w.pop();
				}
			}
			if (enabled && protocol!=null && protocol.equals("ldap") && type.equals("external")) {
				dm = new XmlDocMaker("args");
				dm.add("domain", domain);
				r = executor.execute("unimelb.ldap.domain.sid.get", dm.root());
				String sid = r.value("sid");
				dm = new XmlDocMaker("args");
				dm.add("domain", domain);
				dm.add("identity", new String[] {"protocol", "smb"}, sid);
				dm.add("map", projectID);
				executor.execute("posix.fs.identity.domain.identity.add",  dm.root());
				if (w!=null) {
					w.push("identity");
					w.add("domain", new String[]{"type", "external", "protocol", "ldap"}, domain);
					w.add("sid", sid);
					w.pop();
				}
			}
		}
	}	


	private static void addLocalDomainSID (ServiceExecutor executor, String domain, 
			String projectID, Boolean failIfNotEnabled, XmlWriter w) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		XmlDoc.Element r = executor.execute("authentication.domain.exists", dm.root());
		if (r.booleanValue("exists")) {
			dm = new XmlDocMaker("args");
			dm.add("domain", domain);
			r = executor.execute("authentication.domain.describe", dm.root());
			String type = r.value("domain/@type");
			Boolean enabled = r.booleanValue("domain/@enabled", true);
			if (!enabled) {
				if (failIfNotEnabled) {
					throw new Exception ("The domain '" + domain + "is not enabled");
				} else {
					w.push("identity");
					w.add("domain", new String[]{"type", "local", "enabled", "false"}, domain);	
					w.pop();
				}
			}
			if (enabled && type.equals("local")) {
				dm = new XmlDocMaker("args");
				dm.add("domain", domain);
				dm.add("map", projectID);
				dm.add("protocol", "smb");
				r = executor.execute("posix.fs.identity.domain.identity.generate.and.add", dm.root());
				String sid = r.value("sid");
				if (w!=null) {
					w.push("identity");
					w.add("domain", new String[]{"type", "local"}, domain);
					w.add("sid", new String[] {"protocol", "smb"}, sid);
					w.pop();
				}
			}
		}
	}
}
