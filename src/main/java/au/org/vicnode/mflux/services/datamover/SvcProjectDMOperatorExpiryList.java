/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.datamover;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;

import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.services.SvcProjectDescribe;
import au.org.vicnode.mflux.services.shareable.Manifest;

public class SvcProjectDMOperatorExpiryList extends PluginService {

	// This service is granted permission to access
	// ds-admin:project-namespace-map
	private Interface _defn;
	private static Boolean allowPartialCID = true;

	public SvcProjectDMOperatorExpiryList() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("expiry-offset", IntegerType.DEFAULT,
				"Subtract this many days from the actual acquisition time to force data to look they have expired. For testing only. Defaults to 0.",
				0, 1));
		_defn.add(new Interface.Element("sort", BooleanType.DEFAULT,
				"If true (default) sort the found namespaces by age.", 0, 1));
		SvcProjectDescribe.addProjectIDs(_defn, allowPartialCID);
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Looks for and lists DataMover uploaded transactional namespaces "
				+ "that have expired (meaning the data are older than the lifetime "
				+ "of the download links that were dispatched to end users) and " + "should be destroyed. "
				+ "The service looks recursively for manifest assets in all child "
				+ "namespaces (except those excluded by the meta-data element "
				+ "data-mover-expiry/exclude-child-path) to determine if the "
				+ "data have expired and are candidates for destruction.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.DM.operator.expiry.list";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		// Parse inputs
		Collection<String> theProjectIDs = args.values("project-id");
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs != null) {
			for (String theProjectID : theProjectIDs) {
				String[] t = Project.validateProject(executor(), theProjectID, allowPartialCID);
				projectIDs.add(t[1]);
			}
		} else {
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size() == 0) {
			return;
		}
		//
		Date today = new Date();
		Integer expiryOffset = args.intValue("expiry-offset", 0);
		Boolean sort = args.booleanValue("sort", true);

		// Iterate through projects looking for the ones with the notification
		// meta-data $NS_ADMIN:Project/notification/data-mover-expiry
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);

			// Get namespace meta-data
			XmlDoc.Element nsMeta = ph.metaData();

			// Look for expiry meta-data on the project namespace
			// and parse it if exists.
			XmlDoc.Element meta = nsMeta.element("namespace/asset-meta/" + Properties.getProjectDocType(executor())
					+ "/notification/data-mover-expiry");
			Collection<String> excludeChildPaths = null;
			Collection<XmlDoc.Element> emails = null;
			if (meta != null) {
				excludeChildPaths = meta.values("exclude-child-path");
				emails = meta.elements("email");
			}
			// FInd the direct (or direct+childPath) children namespaces for this project
			// in which the manifest asset is older than the threshold (days)
			Vector<String> noExpiryCandidates = new Vector<String>(); // can't extract download link duration
			Vector<XmlDoc.Element> skippedCandidates = new Vector<XmlDoc.Element>(); // not yet expired
			Vector<XmlDoc.Element> sortedCandidates = new Vector<XmlDoc.Element>();
			Vector<XmlDoc.Element> candidates = findAndMake(executor(), ph, today, excludeChildPaths,
					noExpiryCandidates, skippedCandidates, expiryOffset);

			// Handle
			if (candidates.size() > 0 || noExpiryCandidates.size() > 0) {
				w.push("project");
				w.add("id", ph.id());
				if (emails != null) {
					w.addAll(emails);
				}
				if (candidates.size() > 0) {
					w.push("expired");

					// Sort into age order (oldest to youngest).
					// It would be nice if we used the :sort element
					// of asset.query when finding the assets, but the per namespace
					// iteration structure does not suit this
					if (sort) {
						sortedCandidates = sortByAge(candidates);
						w.addAll(sortedCandidates);
					} else {
						w.addAll(candidates);
					}
					w.pop();

				}
				if (skippedCandidates.size() > 0) {
					w.push("not-yet-expired");
					for (XmlDoc.Element candidate : skippedCandidates) {
						w.add(candidate);
					}
					w.pop();
				}
				if (noExpiryCandidates.size() > 0) {
					w.push("missing-download-expiry-date");
					for (String candidate : noExpiryCandidates) {
						w.add("namespace", candidate);
					}
					w.pop();
				}

				// This new approach for finding manifest assets means
				// we no longer have an output element
				// "failed-upload-candidates" because we aren't
				// iterating through namespaces anymore (so we can't
				// look for transactional namespaces).
				w.pop();
			}

		}

	}

	private static Vector<XmlDoc.Element> sortByAge(Collection<XmlDoc.Element> namespaces) throws Throwable {
		if (namespaces == null || namespaces.isEmpty()) {
			return null;
		}
		Vector<XmlDoc.Element> list = new Vector<XmlDoc.Element>(namespaces);
		Collections.sort(list, new Comparator<XmlDoc.Element>() {

			@Override
			public int compare(XmlDoc.Element d1, XmlDoc.Element d2) {

				Long d1d = null;
				try {
					// bytes
					String s = d1.value("diff");
					d1d = Long.parseLong(s);
				} catch (Throwable e) {
				}

				Long d2d = null;
				try {
					// bytes
					String s = d2.value("diff");
					d2d = Long.parseLong(s);
				} catch (Throwable e) {
				}
				// This will never happen but we have to handle the exception
				if (d1d == null || d2d == null) {
					return 0;
				}
				if (d2d > d1d) {
					return 1;
				} else if (d2d < d1d) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return list;
	}

	/**
	 * Finds information for each candidate namespace to be destroyed
	 * 
	 * @param executor
	 * @param ph
	 * @param today
	 * @param childPaths
	 * @param noExpiryCandidates (could not extract download link expiry)
	 * @param skippedCandidates  (not ready yet for destruction)
	 * @return
	 * @throws Throwable
	 */
	private Vector<XmlDoc.Element> findAndMake(ServiceExecutor executor, ProjectHolder ph, Date today,
			Collection<String> excludeChildPaths, Vector<String> noExpiryCandidates,
			Vector<XmlDoc.Element> skippedCandidates, Integer expiryOffset) throws Throwable {

		// Look for manifest assets directly in and below the project
		// namespace except for in specified exclusion child paths.
		String nameSpace = ph.nameSpace(); // project namespace
		Collection<XmlDoc.Element> assets = Utils.findManifestAssets(executor, null, nameSpace, excludeChildPaths);

		// Fill in details about the candidates and failed and skipped candidates
		// With this approach we cannot find transactional namespaces with no
		// manifest asset
		Vector<XmlDoc.Element> candidates = new Vector<XmlDoc.Element>();
		populateCandidates(executor, today, expiryOffset, assets, candidates, noExpiryCandidates, skippedCandidates);

		return candidates;
	}

	// Iterate over found manifest assets
	private void populateCandidates(ServiceExecutor executor, Date today, Integer expiryOffset,
			Collection<XmlDoc.Element> assets, Collection<XmlDoc.Element> candidates,
			Collection<String> noExpiryCandidates, Collection<XmlDoc.Element> skippedCandidates) throws Throwable {
		if (assets == null) {
			return;
		}
		// Iterate over manifest assets
		for (XmlDoc.Element asset : assets) {
			// We have a manifest asset so this namespace is of interest
			XmlDoc.Element t = asset.element("meta/" + Utils.MANIFEST_DOCTYPE + "/upload/share/duration");
			String assetID = asset.value("@id");
			String absPath = asset.value("namespace");

			if (t != null) {
				Date expiryDate = t.dateValue("@expiry");
				if (expiryOffset > 0) {
					// Push into the past for testing
					expiryDate = DateUtil.addDays(expiryDate, -expiryOffset); // testing
				}
				Long expiryDur = t.longValue();

				// Continue if today is after the expiry date of the download links
				if (today.after(expiryDate)) {
					XmlDoc.Element sum = NameSpaceUtil.sumContent(executor, null, absPath, null);
					String sumBytes = sum.value("value");
					if (sumBytes == null) {
						sumBytes = "0";
					}
					//
					// Find when the manifest asset was created and how old it is
					Date ctime = asset.dateValue("ctime");
					Long dayDiff = DateUtil.dateDifferenceInDays(today, ctime);
					//
					XmlDocMaker dmw = new XmlDocMaker("args");
					dmw.push("namespace");
					dmw.add("path", absPath);
					dmw.add("today", today);

					dmw.push("manifest");
					dmw.add("id", assetID);
					dmw.add("ctime", ctime);
					dmw.add("expiry", expiryDate);
					dmw.add("duration", expiryDur);
					dmw.add("diff", new String[] { "units", "day" }, dayDiff);

					// Get the asset attributes (where we store the number
					// of downloads)
					// If it hasn't been measured (because we the data
					// were acquired before the attribute was being stored)
					// we will treat it as not downloaded (wrong though that is)
					Map<String, Integer> attrs = Manifest.getManifestAssetAttributes(executor, assetID);

					dmw.add("datamover-downloaded",
							attrs.getOrDefault(Manifest.ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED, 0));

					dmw.add("datamover-downloaded-subset",
							attrs.getOrDefault(Manifest.ATTRIBUTE_SHAREABLE_DOWNLOAD_COSUMED_SUBSET, 0));

					dmw.add("copied-to-mediaflux", attrs.getOrDefault(Manifest.ATTRIBUTE_USER_INTERACTION_COSUMED, 0));

					dmw.add("direct-download-attempt",
							attrs.getOrDefault(Manifest.ATTRIBUTE_DIRECT_DOWNLOAD_ATTEMPT, 0));

					dmw.pop();

					dmw.add("sum", new String[] { "unit", "bytes" }, sumBytes);
					dmw.pop();
					//
					candidates.add(dmw.root().element("namespace"));
				} else {
					// Not yet ready for destruction
					XmlDocMaker dmw = new XmlDocMaker("args");
					dmw.add("namespace", new String[] { "expiry", DateUtil.formatDate(expiryDate, false, false) },
							absPath);
					skippedCandidates.add(dmw.root().element("namespace"));
				}
			} else {
				// We could not extract the downlink expiry from the shareable
				noExpiryCandidates.add(absPath);
			}
		}
	}
}
