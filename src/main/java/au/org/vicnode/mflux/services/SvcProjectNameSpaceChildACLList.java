/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectNameSpaceChildACLList extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectNameSpaceChildACLList() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectIDs (_defn, allowPartialCID);
		SvcProjectDescribe.addIgnoreProjectIDs(_defn);
		_defn.add(new Interface.Element("contains", StringType.DEFAULT, "A string that the actor of the ACL must contain (case insensitive) to be shown. Default is all actors.", 0, 1));
		_defn.add(new Interface.Element("recurse", BooleanType.DEFAULT, "Recurse down the namespace tree. Default is false (just inspect immediate children namespaces).", 0, 1));
		_defn.add(new Interface.Element("nlevels", IntegerType.DEFAULT, "If recurse=true (ignored if false)  only recurse this many levels, default infinity.", 0, 1));
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.namespace.ACL.child.list";
	}

	public String description() {
		return "Service to list any namespace ACLs on children of the root project namespace.   The returned namespace count is the number of namespaces found with an ACL (any).";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Parse inputs				
		Collection<String> theProjectIDs = args.values("project-id");
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectIDs!=null) {
			for (String theProjectID : theProjectIDs) {
				String [] t = Project.validateProject(executor(), theProjectID, allowPartialCID);
				projectIDs.add(t[1]);
			}
		} else{
			projectIDs.addAll(Project.projectIDs(executor(), false, true));
		}
		if (projectIDs.size()==0) {
			return;
		}

		Collection<String> theIgnores = args.values("ignore");
		// List of ignores
		Vector<String> ignores = new Vector<String>();
		if (theIgnores!=null) {
			for (String theIgnore : theIgnores) {
				String t[] = Project.validateProject(executor(), theIgnore, allowPartialCID);
				ignores.add(t[1]);
			} 
		}
		// 
		String contains = args.value("contains");
		Integer nLevels = args.intValue("nlevels",-1);
		Boolean recurse = args.booleanValue("recurse", false);


		// Iterate over projects
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);

			if (Project.keepOnIgnoreList(ph.id(), ignores)) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("namespace", ph.nameSpace());
				dm.add("recurse", recurse);
				if (contains!=null) {
					dm.add("contains", contains);
				}
				dm.add("nlevels", nLevels);
				XmlDoc.Element r = executor().execute("unimelb.asset.namespace.acl.child.list", dm.root());
				if (r!=null && r.hasSubElements()) {
					w.push("project", new String[] {"id", ph.id()});
					w.addAll(r.elements());
					w.pop();
				}
			}
		}
	}
}
