package au.org.vicnode.mflux.services.shareable;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.LongType;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;
import unimelb.utils.DNSUtils;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

public class SvcFacilityShareableUploadRecipientList extends PluginService {
    public final String SERVICE_NAME = Properties.SERVICE_ROOT + "facility.shareable.upload.recipient.list";

    private final Interface _defn;

    public SvcFacilityShareableUploadRecipientList() {
        _defn = new Interface();
        _defn.add(new Interface.Element("shareable-id", LongType.POSITIVE, "The facility shareable id.",
                1, 1));
        _defn.add(new Interface.Element("exclude-invalid", BooleanType.DEFAULT,
                "Exclude email address with invalid domain name. Defaults to false.", 0, 1));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "List the recipient emails of the facility shareable uploads";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String shareableId = args.value("shareable-id");
        boolean excludeInvalid = args.booleanValue("exclude-invalid", false);
        Set<String> recipients = getFacilityUploadRecipients(executor(), shareableId);

        int count = recipients.size();
        for (String recipient : recipients) {
            if (excludeInvalid) {
                String domainName = recipient.substring(recipient.lastIndexOf('@') + 1);
                PluginTask.checkIfThreadTaskAborted();
                if (!DNSUtils.isMailDomain(domainName)) {
                    count -= 1;
                    continue;
                }
            }
            w.add("recipient", recipient);
        }
        w.add("count", count);
    }

    private static Set<String> getFacilityUploadRecipients(ServiceExecutor executor, String shareableId)
            throws Throwable {
        Set<String> recipients = new TreeSet<>();

        XmlDoc.Element se = Shareable.describe(executor, shareableId);
        Collection<String> uploadIds = se.values("uploads/upload/@id");
        if (uploadIds != null) {
            for (String uploadId : uploadIds) {
                PluginTask.checkIfThreadTaskAborted();
                XmlDoc.Element me = executor.execute(SvcFacilityShareableUploadManifestDescribe.SERVICE_NAME,
                                "<args><shareable-id>" + shareableId + "</shareable-id><upload-id>" + uploadId
                                        + "</upload-id></args>", null, null)
                        .element("shareable-upload-manifest");
                if (me != null) {
                    Collection<String> sws = me.values("upload/arg[@name='share-with']");
                    if (sws != null) {
                        for (String sw : sws) {
                            Collection<String> emails = Emails.parse(sw.toLowerCase());
                            if (emails != null) {
                                recipients.addAll(emails);
                            }
                        }
                    }
                }
            }
        }
        return recipients;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

}
