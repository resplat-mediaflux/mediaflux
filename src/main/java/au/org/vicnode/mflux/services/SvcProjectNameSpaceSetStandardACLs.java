/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;


import java.util.Vector;

import arc.mf.plugin.*;
import arc.mf.plugin.atomic.AtomicOperation;
import arc.mf.plugin.atomic.AtomicTransaction;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectNameSpaceSetStandardACLs extends PluginService {


	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectNameSpaceSetStandardACLs() throws Throwable {
		_defn = new Interface();
		SvcProjectDescribe.addProjectID (_defn, allowPartialCID);
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.namespace.set.standard-ACLs";
	}

	public String description() {
		return "Service to re-set the ACLS on the project asset namespace to the standard set.  This is done by revoking all ACLs and then re-granting the new ones.  This is done in an atomic transaction, so if an exception occurs, the namespace is left in its original state.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String theProjectID = args.stringValue("project-id");

		// List of projects
		Vector<String> projectIDs = new Vector<String>();
		if (theProjectID!=null) {
			String t[] = Project.validateProject(executor(), theProjectID, allowPartialCID);
			projectIDs.add(t[1]);
		} else{
			// FInd all the projects and add to the list
			projectIDs.addAll(Project.projectIDs(executor(), true, true));
		}
		if (projectIDs.size()==0) return;


		// Reset
		for (final String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);

			new AtomicTransaction(new AtomicOperation() {

				@Override
				public boolean execute(ServiceExecutor executor) throws Throwable {
					revokeAndGrantACLs(executor, ph.id(), ph.nameSpace());
					return false;
				}
			}).execute(executor());
			w.add("project-id", ph.id());

		}
	}
	
	
	private  void revokeAndGrantACLs (ServiceExecutor executor, String projectID, String nameSpace) throws Throwable {
		// Revoke all asset namespace ACLs
		NameSpaceUtil.revokeAllACLs(executor, nameSpace);
		
		// Grant the standard asset namespace ACLs only
		Boolean doACLs = true;
		Boolean doPerms = false;
		Project.grantStandardPermissionsAndACLs(executor, projectID, doACLs, doPerms);
		// We no longer do this. The :namespace execute is now incorporated
		// into the standard project-role ACLs
		// Posix.grantAssetNameSpacePosixPerm(executor, nameSpace);
		
	}

}
