/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectContentCopy;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;

/**
 * 
 * @author nebk
 *
 */
public class SvcProjectList extends PluginService {

	public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "project.list";

	private Interface _defn;

	public SvcProjectList() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",
				0, 1));
		_defn.add(new Interface.Element("parent", StringType.DEFAULT,
				"The parent namespace to consider when looking for projects. Should start with a '/'.  Defaults to all parent.",
				0, 1));
		_defn.add(new Interface.Element("collection", StringType.DEFAULT,
				"A string to search for in the project's data registry collection code.", 0, 1));
		_defn.add(new Interface.Element("DR", BooleanType.DEFAULT,
				"Show the DR namespace existence as well if applicable. Defaults to false.", 0, 1));
		_defn.add(new Interface.Element("size", IntegerType.DEFAULT,
				"The number to list (defaults to all).",
				0, 1));
		_defn.add(new Interface.Element("empty", BooleanType.DEFAULT,
				"Only list projects that have zero assets (defaults to false).", 0, 1));
		//
		SvcProjectDescribe.addProdType (_defn);
		SvcProjectDescribe.addRepType (_defn);
		SvcProjectDescribe.addOwner (_defn);
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns a list of specified projects.";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String repType = args.stringValue("rep-type", Properties.repTypes[0]);
		String projType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		String keyString = args.value("contains");
		if (keyString != null) {
			keyString = keyString.toUpperCase();
		}
		String parent = args.value("parent");
		String collection = args.value("collection");
		Boolean doDR = args.booleanValue("DR",false);
		String ownerEmail = args.value("owner-email");
		Integer size = args.intValue ("size", Integer.MAX_VALUE);
		Boolean empty = args.booleanValue("empty", false);

		// List of projects
		Collection<String> projectIDs = Project.projectIDs(executor(), true, true);
		if (projectIDs.size() == 0)
			return;
		int n = 0;
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, true);
			
			// Filter on primary types		
			if (Project.keepOnProjectType(ph, projType) && 
				Project.keepOnKeyString(ph.id(), keyString) && 
				Project.keepOnParent(ph, parent) &&
				Project.keepOnOwner(ph,ownerEmail)) {

				// Filter out on replication type
				String qName = ProjectReplicate.isProjectReplicated(ph);
				if (Project.keepOnRepType(executor(), qName, repType)) {

					// Filter on empty...
					Boolean proceed = true;
					if (empty) {
						proceed = noAssets(executor(), ph);
					}
					if (proceed && list(executor(), ph, repType, collection, doDR, w)) {
						n++;
						if (n>=size) {
							break;
						}
					}
				}
			}
		}
		w.add("n-projects", new String[] { "total-number", "" + projectIDs.size() }, n);
	}

	private Boolean noAssets (ServiceExecutor executor, ProjectHolder ph) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", "namespace>='"+ph.nameSpace()+"'");
		dm.add("size", 1);
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		if (r==null) {
			return true;
		}
		return !(r.hasSubElements());
	}
	private Boolean list(ServiceExecutor executor, ProjectHolder ph,
			String repType, String collection, Boolean doDR, XmlWriter w)
					throws Throwable {


		// Get namespace meta-data
		String parentNameSpace = ph.parentNameSpace();
		XmlDoc.Element nsMeta = ph.metaData();
		String collectionKey = ph.collectionKey();
		String projectType = Project.projectType(executor, nsMeta);

		if (collection != null) {
			if (collectionKey == null || !collectionKey.toLowerCase().contains(collection.toLowerCase())) {
				return false;
			}
		}

		String collectionCode = collectionKey == null ? null : collectionKey.replaceAll("^.*:", "");

		// Content copies
		String ccQueue = ProjectContentCopy.isProjectContentCopied(ph);

		// Look for the replication processing queue
		XmlDoc.Element enq = ProjectReplicate.getStandardEnqueueElement(nsMeta);
		String qName = "none";
		if (enq!=null) {
			qName = enq.value("queue");
		} 

		// List
		if (doDR) {
			String drNameSpace = ph.nameSpaceDR();
			String drUUID = ProjectReplicate.DRServerUUID(executor);
			Boolean exists = NameSpaceUtil.assetNameSpaceExists(executor, drUUID, drNameSpace);
			w.add("project", new String[] { "parent", parentNameSpace, 
					"path", ph.nameSpace(), "type", projectType, 
				  "replication-queue", qName, "dr-namespace-exists", exists.toString(), "content-copy-queue", ccQueue, "collection", collectionCode }, ph.id());
		} else {
			w.add("project", new String[] { "parent", parentNameSpace, 
					"path", ph.nameSpace(), "type", projectType, 
				  "replication-queue", qName, "content-copy-queue", ccQueue,"collection", collectionCode }, ph.id());
		}
		
	

		return true;
	}

}
