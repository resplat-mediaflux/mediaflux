package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static au.org.vicnode.mflux.plugin.util.MailHandler.sendMessage;
import static au.org.vicnode.mflux.plugin.util.Properties.*;

public class SvcUserStandardRoleRevoke extends PluginService {

    public static final String SERVICE_NAME = Properties.SERVICE_ROOT + "user.standard.role.revoke";

    private PluginService.Interface _defn;

    public String STANDARD_USER_ROLE;

    public String staff_domain;

    public String student_domain;

    public String local_domain;


    public SvcUserStandardRoleRevoke() {
        _defn = new PluginService.Interface();
        _defn.add(new PluginService.Interface.Element("revoke-roles", BooleanType.DEFAULT,
                "Revoke standard user role of user if no project roles", 0, 1));
        _defn.add(new Interface.Element("email", EmailAddressType.DEFAULT,
                "If any users were found, email the result to the specified recipient(s).", 0, 5));

    }

    @Override
    public PluginService.Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public PluginService.Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Find the users without any project roles and revoke standard user role if no project roles";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(arc.xml.XmlDoc.Element args, PluginService.Inputs arg1, PluginService.Outputs arg2, XmlWriter w) throws Throwable {
        STANDARD_USER_ROLE = getStandardUserRoleName(executor());
        staff_domain = getLDAPStaffDomain((executor()));
        student_domain = getLDAPStudentDomain((executor()));
        local_domain = getUserAuthDomain((executor()));

        boolean destroy = args.booleanValue("revoke-roles", false);
        Collection<String> emails = args.values("email");
        StringBuilder sb = (emails == null || emails.isEmpty()) ? null : beginEmailMessage();

        int count = 0;
        List<XmlDoc.Element> ies = describeAllUsers(executor());
        if ( ies != null) {
            for (XmlDoc.Element ie : ies) {
                PluginTask.checkIfThreadTaskAborted();
                String name = ie.value("@name");
                String ownerDomain;
                String ownerUser;
                try {
                    ownerDomain = name.split(":")[0];
                    ownerUser = name.split(":")[1];
                }
                catch(Exception e){
                        continue;
                }

                boolean projectPermissions = hasProjectPermissions(executor(), ownerDomain, ownerUser);
                if (!projectPermissions) {
                    if (destroy) {
                        destroyStandardUserPermission(executor(), ownerDomain, ownerUser);
                    }
                    w.add("domain", ownerDomain);
                    w.add("user", ownerUser);
                    sb.append("<tr>");
                    sb.append("<td>").append(ownerDomain).append("</td>");
                    sb.append("<td>").append(ownerUser).append("</td>");
                    sb.append("</tr>");
                    count++;
                }
            }
        }
        w.add("revoked-role",destroy);
        w.add("count", count);
        endEmailMessage(sb, count);
        sendMessage(executor(),emails,"Found users with standard user role without project roles", sb.toString(),"text/html",false,null,null);

    }

    private StringBuilder beginEmailMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append("<html>\n");
        sb.append("<head>\n");
        sb.append("<style>\n");
        sb.append("table, th, td { border: 1px solid black; }\n");
        sb.append("tr:nth-child(even) {background: #CCC}\n");
        sb.append("tr:nth-child(odd) {background: #FFF}\n");
        sb.append("</style>\n");
        sb.append("</head>\n");
        sb.append("<body>\n");
        sb.append("<h3>Found users with standard user role without project roles</h3><br>");
        sb.append("<table>");
        sb.append(
                "<thead><tr><th>Domain</th><th>User</th></tr></thead>");
        sb.append("<tbody>");
        return sb;
    }

    private static void endEmailMessage(StringBuilder sb, int count) {
        sb.append("<tbody></table>");
        sb.append("<ul><li><b>Count:</b>").append(count).append("</li></ul>");
        sb.append("</body></html>");
    }


    private Collection<String> extractAllRoles(List<XmlDoc.Element> rolesElement) throws Throwable {
        Collection<String> roles = new ArrayList<String>();
        if (rolesElement != null) {
            //w.add("roles",userRoles.toString());
            for (XmlDoc.Element role : rolesElement) {
                if (role.value("@type").equals("role") && !role.value().equals("user")) {
                    roles.add(role.value());
                }
            }
        }
        return roles;
    }

    private boolean hasProjectPermissions(ServiceExecutor executor, String ownerDomain, String ownerUser) throws Throwable {
        if (!ownerDomain.equals(staff_domain) && !ownerDomain.equals(student_domain) && !ownerDomain.equals(local_domain)) {
            return true;
        }
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("domain", ownerDomain);
        dm.add("user", ownerUser);
        boolean exists = executor.execute("user.exists", dm.root()).booleanValue("exists");
        if (!exists) {
            return true;
        }
        XmlDocMaker dm1 = new XmlDocMaker("args");
        dm1.add("domain", ownerDomain);
        dm1.add("user", ownerUser);
        dm1.add("permissions",true);
        List<XmlDoc.Element> userRolesElement = executor.execute("user.describe", dm1.root()).elements("user/role");
        Collection<String> roles = extractAllRoles(userRolesElement);
        return !roles.isEmpty();
    }

    private List<XmlDoc.Element> describeAllUsers(ServiceExecutor executor) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", "user");
        dm.add("role",  new String[]{"type", "role"}, STANDARD_USER_ROLE);
        return executor.execute("actors.granted", dm.root()).elements("actor");
    }


    private void destroyStandardUserPermission(ServiceExecutor executor, String userDomain, String username) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        // user.revoke :role -type role ds:standard-user :domain unimelb :user user
        dm.add("domain",userDomain);
        dm.add("user", username);
        dm.add("role", new String[]{"type", "role"}, STANDARD_USER_ROLE);

        executor.execute("user.revoke", dm.root());

    }

}
