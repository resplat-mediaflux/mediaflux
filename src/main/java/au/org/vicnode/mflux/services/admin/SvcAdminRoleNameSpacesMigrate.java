/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2023, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.admin;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.AssetUtil;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcAdminRoleNameSpacesMigrate extends PluginService {


	private Interface _defn;


	public SvcAdminRoleNameSpacesMigrate()  throws Throwable {
		_defn = new Interface();
		Interface.Element ie = new Interface.Element("assess-role", BooleanType.DEFAULT, "Assess roles in step 2 (default false).", 0, 1);
		ie.add(new Interface.Attribute("apply", BooleanType.DEFAULT,
				"If true, actually apply the changes. If false (default), just check and report.", 0));
		_defn.add(ie);
		//
		ie = new Interface.Element("assess-network", BooleanType.DEFAULT, "Assess network restrictions in step 3 (default false).", 0, 1);
		ie.add(new Interface.Attribute("apply", BooleanType.DEFAULT,
				"If true, actually apply the changes. If false (default), just check and report.", 0));
		_defn.add(ie);
		//		
		ie = new Interface.Element("assess-visibility", BooleanType.DEFAULT, "Assess visibility actors in step 4 (default false).", 0, 1);
		ie.add(new Interface.Attribute("apply", BooleanType.DEFAULT,
				"If true, actually apply the changes. If false (default), just check and report.", 0));
		_defn.add(ie);
		//		
		ie = new Interface.Element("assess-admin-ACL", BooleanType.DEFAULT, "Assess admin ACLs in step 5a (default false).", 0, 1);
		ie.add(new Interface.Attribute("apply", BooleanType.DEFAULT,
				"If true, actually apply the changes. If false (default), just check and report.", 0));
		_defn.add(ie);
		//	
		ie = new Interface.Element("assess-project-ACL", BooleanType.DEFAULT, "Assess project ACLs in step 5b (default false).", 0, 1);
		ie.add(new Interface.Attribute("apply", BooleanType.DEFAULT,
				"If true, actually apply the changes. If false (default), just check and report.", 0));
		_defn.add(ie);
		//
		ie = new Interface.Element("assess-admin-project-perms", BooleanType.DEFAULT, "Assess the roles in 6a and 6b (default false).", 0, 1);
		ie.add(new Interface.Attribute("apply", BooleanType.DEFAULT,
				"If true, actually apply the changes. If false (default), just check and report.", 0));
		_defn.add(ie);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Use with extreme care. One off service to 1) create any missing roles in the ds and ds-admin namespaces (based on the roles in the VicNode and VicNode-Admin equivalents), 2) grant roles from ds (2a) and ds-admin (2b) role namespace to all appropriate external (not managed by the ds package) actors, 3) add  network service restrictions with ds and ds-admin roles, 4) add new visibility actors to project parent namespaces with roles from the ds and ds-admin namespaces, if any, 5a) add ACLs for roles (ds: and ds-admin:) for asset namespaces /local, /local-admin, /local-admin/instrument-upload-manifests, /training,  5b) add ACLs for roles (ds: and ds-admin:) to the /projects tree, 6a) Grant all project permissions held by VicNode-Admin:project-access to ds-admin:project-access, 6b) grant all permissions held by VicNode-Admin:project-role-namespace-administrator to ds-admin:project-role-namespace-administrator, and 7) operator roles. Step 1 is always done, the others are optional. At the end of this service execution, all ds and ds-admin role namespaes roles should be correctly applied, but we still have not cut over to actually use them.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "admin.role.namespaces.migrate";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		XmlDoc.Element assessRole = args.element("assess-role");
		XmlDoc.Element assessNetwork = args.element("assess-network");
		XmlDoc.Element assessVisibility = args.element("assess-visibility");
		XmlDoc.Element assessAdminACLs = args.element("assess-admin-ACL");
		XmlDoc.Element assessProjectACLs = args.element("assess-project-ACL");
		XmlDoc.Element assessAdminProjectPerms = args.element("assess-admin-project-perms");

		// Step 1
		// We have installed the vicnode package with the role namespaces
		// set to ds and ds-admin.  These namespaces and standard roles will already
		// be populated.  However, there may be some VicNode and VicNode-Admin roles created outside of 
		// the standard package installers.
		// We need to duplicate those in the new namespaces. 
		PluginTask.checkIfThreadTaskAborted();
		//w.push("add-missing-roles");
		//addMissingRoles (executor(), "VicNode", "ds", w);
		//w.pop();

		// Step 2a
		// Now we need to grant the new ds roles to any
		// actors that hold the equivalent VicNode roles
		PluginTask.checkIfThreadTaskAborted();
		if (assessRole!=null && assessRole.booleanValue()) {
			w.push("grant-ds-roles-to-actors");
			grantDSRolesToActors (executor(), "VicNode", 
					assessRole.booleanValue("@apply",  false), w);
			w.pop();
		}

		// Step 2b
		// Now we need to grant the new ds-admin roles to any
		// actors that hold the equivalent VicNode-Admin roles
		PluginTask.checkIfThreadTaskAborted();
		if (assessRole!=null && assessRole.booleanValue()) {
			w.push("grant-ds-admin-roles-to-actors");
			grantDSRolesToActors (executor(), "VicNode-Admin", 
					assessRole.booleanValue("@apply",  false), w);
			w.pop();
		}

		// Step 3
		// Handle network services. There are some restrictions on authentication domains
		// utilising roles.
		PluginTask.checkIfThreadTaskAborted();
		if (assessNetwork!=null && assessNetwork.booleanValue()) {
			w.push("update-network-restrictions");
			updateNetworkRestrictions (executor(), 
					assessNetwork.booleanValue("@apply", false), w);
			w.pop();
		}

		// Step 4
		// Handle visibility actors. Visibility actors are applied to some
		// project parent namespaces
		PluginTask.checkIfThreadTaskAborted();
		if (assessVisibility!=null && assessVisibility.booleanValue()) {
			w.push("update-visibility-actors");
			updateVisibilityActors (executor(), 
					assessVisibility.booleanValue("@apply",  false), w);
			w.pop();
		}

		// STep 5
		// ACL updates. 
		// Step 5a
		PluginTask.checkIfThreadTaskAborted();
		if (assessAdminACLs!=null && assessAdminACLs.booleanValue()) {
			w.push("update-admin-ACLs");
			updateAdminACLs (executor(), 
					assessAdminACLs.booleanValue("@apply", false), w);
			w.pop();
		}

		// Step 5b
		PluginTask.checkIfThreadTaskAborted();
		if (assessProjectACLs!=null && assessProjectACLs.booleanValue()) {
			w.push("projects");
			updateProjectACLs (executor(), 
					assessProjectACLs.booleanValue("@apply",  false), w);
			w.pop();
		}

		// STep 6a & 6b
		// Revoke Project role perms and role grants
		if  (assessAdminProjectPerms!=null && assessAdminProjectPerms.booleanValue()) {
			w.push("update-admin-project-perms");
			updateAdminProjectPerms(executor(), assessAdminProjectPerms.booleanValue("@apply", false), w);
			w.pop();
			w.pop();
		}

	}


	private static void updateAdminProjectPerms(ServiceExecutor executor, 
			Boolean apply, XmlWriter w) throws Throwable {

		w.push("VicNode-Admin:project-access", 
				new String[] {"to", "ds-admin:project-access", "apply", apply.toString()});
		copyPerms (executor, "VicNode-Admin:project-access", "ds-admin:project-access", apply, w);
		w.pop();
		w.push("VicNode-Admin:project-role-namespace-administrator",
				new String[] {"to", "ds-admin:project-role-namespace-administrator",
						"apply", apply.toString()});
		copyPerms (executor, "VicNode-Admin:project-role-namespace-administrator",
				"ds-admin:project-role-namespace-administrator", apply, w);
		w.pop();
	}

	
	private static void copyPerms (ServiceExecutor executor, String roleFrom, String roleTo, Boolean apply, XmlWriter w) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", roleFrom);
		dm.add("type", "role");
		XmlDoc.Element r = executor.execute("actor.describe", dm.root());
		Collection<XmlDoc.Element> perms = r.elements("actor/perm");
		if (perms==null) {
			return;
		}
		for (XmlDoc.Element perm : perms) {
			w.add(perm);
			if (apply) {
				dm = new XmlDocMaker("args");
				dm.add(perm);
				dm.add("name", roleTo);
				dm.add("type", "role");
				executor.execute("actor.grant", dm.root());
			}
		}
	}

	private static void updateAdminACLs (ServiceExecutor executor, Boolean apply, XmlWriter w) throws Throwable {
		/*
		/local
		/local-admin
		/local-admin/instrument-upload-manifests 
		/training

		 */

		// Package installer actually should have done 
		// /local and /local-admin  already

		String ns = "/local";
		if (NameSpaceUtil.assetNameSpaceExists(executor, null, ns)) {
			w.push("namespace", new String[] {"path", ns});
			findAndAddACLs (executor, ns, "VicNode:", "ds:", apply, w);
			findAndAddACLs (executor, ns, "VicNode-Admin", "ds-admin:", apply, w);
			w.pop();
		}
		//
		ns = "/local-admin";
		if (NameSpaceUtil.assetNameSpaceExists(executor, null, ns)) {
			w.push("namespace", new String[] {"path", ns});
			findAndAddACLs (executor, ns, "VicNode:", "ds:", apply, w);
			findAndAddACLs (executor, ns, "VicNode-Admin", "ds-admin:", apply, w);
			w.pop();
		}
		//
		ns = "/local-admin/instrument-upload-manifests";
		if (NameSpaceUtil.assetNameSpaceExists(executor, null, ns)) {
			w.push("namespace", new String[] {"path", ns});
			findAndAddACLs (executor, ns, "VicNode:", "ds:", apply, w);
			findAndAddACLs (executor, ns, "VicNode-Admin", "ds-admin:", apply, w);
			w.pop();
		}
		//
		ns = "/training";
		if (NameSpaceUtil.assetNameSpaceExists(executor, null, ns)) {
			w.push("namespace", new String[] {"path", ns});
			findAndAddACLs (executor, ns, "VicNode:", "ds:", apply, w);
			findAndAddACLs (executor, ns, "VicNode-Admin", "ds-admin:", apply, w);
			w.pop();
		}
	}

	private static void updateProjectACLs (ServiceExecutor executor, Boolean apply, XmlWriter w) throws Throwable {

		// First add an ACL for /projects
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", "/projects");
		dm.push("acl", new String[] {"type","inclusive"});
		dm.add("actor", new String[] {"type", "role"}, "ds:standard-user");
		dm.push("access");
		dm.add("namespace", "access");
		dm.add("namespace", "execute");
		dm.pop();
		dm.pop();
		if (apply) {
			executor.execute("asset.namespace.acl.grant",dm.root());
		}
		w.add("namespace", new String[] {"actor", "ds:standard-user", "ACL-added", apply.toString()}, "/projects");

		// Now all of the projects
		Collection<String> ids = Project.projectIDs(executor, true, true);

		for (String id : ids) {
			PluginTask.checkIfThreadTaskAborted();

			ProjectHolder ph = new ProjectHolder(executor, id, true);

			// Get namespace
			String ns = ph.nameSpace();
			w.push("project", new String[] {"id", ph.id()});

			// Grant required standard ACLs.
			dm = new XmlDocMaker("args");
			dm.add("namespace", ns);
			dm.push("acl", new String[] {"type","inclusive"});
			dm.add("actor", new String[] {"type", "role"}, "ds-admin:project-access");
			dm.push("access");
			dm.add("namespace", "access");
			dm.add("namespace", "execute");
			dm.add("asset", "access");
			dm.add("asset-content", "access");
			dm.pop();
			dm.pop();
			if (apply) {
				PluginTask.checkIfThreadTaskAborted();
				executor.execute("asset.namespace.acl.grant",dm.root());
			}
			w.add("namespace", new String[] {"actor", "ds-admin:project-access", "ACL-added", apply.toString()}, ns);

			//
			dm = new XmlDocMaker("args");
			dm.add("namespace", ns);
			dm.push("acl", new String[] {"type","inclusive"});
			dm.add("actor", new String[] {"type", "role"}, "ds-admin:project-asset-namespace-administrator");
			dm.push("access");
			dm.add("namespace", "administer");
			dm.add("namespace", "access");
			dm.pop();
			dm.pop();
			if (apply) {
				PluginTask.checkIfThreadTaskAborted();
				executor.execute("asset.namespace.acl.grant",dm.root());
			}
			w.add("namespace", new String[] {"actor", "ds-admin:project-asset-namespace-administrator", "ACL-added", apply.toString()}, ns);

			//
			w.pop();
		}

	}


	private static void findAndAddACLs (ServiceExecutor executor, String namespace, String oldNS, String newNS, Boolean apply, XmlWriter w) throws Throwable {

		XmlDoc.Element meta = NameSpaceUtil.describe(null, executor, namespace);
		Collection<XmlDoc.Element> acls = meta.elements("namespace/acl"); 
		PluginTask.checkIfThreadTaskAborted();
		if (acls!=null) {
			for (XmlDoc.Element acl : acls) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDoc.Element actor = acl.element("actor");
				String actorName = actor.value();
				if (actorName.contains(oldNS)) {
					PluginTask.checkIfThreadTaskAborted();
					// Add a new ACL by modifying the existing one
					AssetUtil.removeAttribute(executor, actor, "id");
					int idx = actorName.indexOf(":");
					String newActorName = newNS + actorName.substring(idx+1);
					actor.setValue(newActorName);

					// We can just grant the new actor
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add("namespace", namespace);
					dm.add(acl);

					if (apply) {
						PluginTask.checkIfThreadTaskAborted();
						executor.execute("asset.namespace.acl.grant", dm.root());
					}
					w.add("namespace", new String[] {"actor-old", actorName, "actor-new", newActorName, "added", apply.toString()}, namespace);
				}
			}
		}
	}

	private static void updateVisibilityActors (ServiceExecutor executor, Boolean apply, XmlWriter w) throws Throwable {

		// We have VicNode-Admin:<role> visibility actors on project parent namespaces
		XmlDoc.Element r = executor.execute("vicnode.project.parent.namespace.list");
		if (r==null) {
			return;
		}
		PluginTask.checkIfThreadTaskAborted();
		//
		Collection<String> parents = r.values("parent");
		if (parents!=null) {
			for (String parent : parents) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("namespace", parent);
				r = executor.execute("asset.namespace.describe", dm.root());
				XmlDoc.Element visibleTo = r.element("namespace/visible-to");
				if (visibleTo!=null) {				
					Collection<XmlDoc.Element> actors = visibleTo.elements("actor");
					Boolean found = false;
					Vector<String> addedActors = new Vector<String>();
					if (actors!=null) {
						for (XmlDoc.Element actor : actors) {
							PluginTask.checkIfThreadTaskAborted();
							AssetUtil.removeAttribute(executor, actor, "id");
							//
							String actorName = actor.value();
							if (actorName.contains("VicNode-Admin:")) {	
								int idx = actorName.indexOf(":");
								String t = actorName.substring(idx+1);	
								//
								XmlDoc.Element newActor = actor.copy();							
								newActor.setValue("ds-admin:" + t);

								// DOn't add this actor if it already exists
								// which would occur with repeated executions
								// of this service.
								if (!visibilityActorExists (actors, newActor.value())) {
									PluginTask.checkIfThreadTaskAborted();
									visibleTo.add(newActor);
									addedActors.add("ds-admin:"+t);
									found = true;	
								}
							}
						}

						// We can only set all the actors, not add one.
						if (found) {
							if (apply) {
								PluginTask.checkIfThreadTaskAborted();
								dm = new XmlDocMaker("args");
								dm.add("namespace", parent);
								dm.add(visibleTo);
								executor.execute("asset.namespace.visibility.set", dm.root());
							} 
							w.push("namespace", new String[] {"path", parent});
							for (String addedActor : addedActors) {
								PluginTask.checkIfThreadTaskAborted();
								w.add("visible-to", new String[] {"added", apply.toString()}, addedActor);
							}
							w.pop();
						}
					}
				}
			}
		}
	}

	private static Boolean visibilityActorExists (Collection<XmlDoc.Element> actors,
			String name) throws Throwable {
		for (XmlDoc.Element actor : actors) {
			String actorName = actor.value();
			if (actorName.equals(name)) {
				return true;
			}
		}
		return false;
	}
	private static void updateNetworkRestrictions (ServiceExecutor executor, Boolean apply, XmlWriter w) throws Throwable {

		//	:service < :restrict < :exclude-role <role> > >
		XmlDoc.Element r = executor.execute("network.describe");
		if (r==null) {
			return;
		}
		PluginTask.checkIfThreadTaskAborted();

		// We only have restrictions of the form    
		// :service < :restrict < :exclude-role "VicNode-Admin:enigma-external" > >
		Collection<XmlDoc.Element> services = r.elements("service");
		if (services!=null) {
			for (XmlDoc.Element service : services) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDoc.Element restrict = service.element("restrict");   // Singleton
				if (restrict!=null) {
					// We have a restriction
					Boolean found = false;
					Collection<String> ers = restrict.values("exclude-role");
					Vector<String> addedRestrictionRoles = new Vector<String>();
					for (String er : ers) {
						PluginTask.checkIfThreadTaskAborted();
						if (er.contains("VicNode-Admin:")) {
							// We have a restriction using VicNode-Admin:<role>. Add in ds-admin:<role>
							int idx = er.indexOf(":");
							String t = er.substring(idx+1);
							XmlDoc.Element newExclusion = new XmlDoc.Element("exclude-role",  "ds-admin:"+t);
							restrict.add(newExclusion);
							addedRestrictionRoles.add("ds-admin:"+t);
							found = true;
						}
					}
					if (found) {
						if (apply) {
							XmlDocMaker dm = new XmlDocMaker("args");
							dm.add("port", service.value("@port"));
							dm.add("type", service.value("@type"));
							dm.add(restrict);
							executor.execute("network.restrict.set", dm.root());
						} 
						w.push("service", new String[] {"type", service.value("@type"), 
								"port", service.value("@port")});
						for (String addedRestrictionRole : addedRestrictionRoles) {
							PluginTask.checkIfThreadTaskAborted();
							w.add("exclude-role", new String[] {"added", apply.toString()}, addedRestrictionRole);
						}
						w.pop();
					}
				}
			}
		}
	}

	private static void grantDSRolesToActors (ServiceExecutor executor,
			String vnNamespace, Boolean grant, XmlWriter w) throws Throwable {

		// FInd all roles in VicNode or VicNode-Admin role namespace
		Collection<XmlDoc.Element> vnRoles = describeRoles(executor, vnNamespace);
		PluginTask.checkIfThreadTaskAborted();

		// For each role, find the actors granted that role
		for (XmlDoc.Element vnRole : vnRoles) {
			PluginTask.checkIfThreadTaskAborted();
			Collection<XmlDoc.Element> actors = findGrantedActors (executor,
					vnRole);

			// Now for each actor, grant it the equivalent role from 
			// the ds namespace. Some of the found actors will be roles and they
			// will be in the VicNode or VicNode-Admin role namespace.
			// We need to replace the role namespace from VicNode to ds
			// and VicNode-Admin to ds-admin.  It does not matter that some
			// of the roles we grant here were already granted by the 
			// package installer (e.g. ds:standard-user has already been granted
			// to ds-admin:provisioning by the installer)
			if (actors!=null) {
				for (XmlDoc.Element actor : actors) {
					PluginTask.checkIfThreadTaskAborted();
					String actorName = actor.value("@name");
					String actorType = actor.value("@type");
					Boolean destroyed = actor.booleanValue("@destroyed",  false);

					// Work around the LDAP domains not working on old test (where I have disabled the domains)
					// actors.granted will find them ok but I can't grant to them. So just drop them.
					Boolean proceed = true;
					if (destroyed || (actorType.equals("user") && !isActorDomainEnabled(executor,actorName))) {
						proceed = false;
					}
					/*
					w.push("actor");
					w.add("name", actorName);
					w.add("type", actorType);
					w.add("destroyed", destroyed);
					if (actorType.equals("user")) {
						w.add("enabled-domain", isActorDomainEnabled(executor, actorName));
					}
					w.pop();
					 */
					//

					if (proceed) {
						PluginTask.checkIfThreadTaskAborted();
						// We will find lots of destroyed plugin service actors
						// from previous installations.  Ignore these.
						String newActorName = null;
						if (actorType.equals("role")) {
							// See if this actor that holds the (VicNode or VicNode-ADmin) role 
							// is itself a VicNode or VicNode-Admin role. If so, replace the
							// VicNode by ds and VicNode-ADmin by ds-admin so we grant to
							// the roles in the new namespaces
							if (actorName.contains("VicNode-Admin:")) {
								int idx = actorName.indexOf(":");
								newActorName = "ds-admin:" + actorName.substring(idx+1);
							} else if (actorName.contains("VicNode:")) {
								int idx = actorName.indexOf(":");
								newActorName = "ds:" + actorName.substring(idx+1);			
							} else {
								newActorName = actorName;
							}
						} else {
							newActorName = actorName;
						}

						// Replace VicNode or VicNode-Admin in namespace of role to grant
						String dsRoleName = null;
						String vnRoleName = vnRole.value("name");
						int idx = vnRoleName.indexOf(":");
						if (vnRoleName.contains("VicNode-Admin:")) {
							dsRoleName = "ds-admin:" + vnRoleName.substring(idx+1);
						} else if (vnRoleName.contains("VicNode:")) {
							dsRoleName = "ds:" + vnRoleName.substring(idx+1);
						} else {
							throw new Exception ("Should not get here");
						}

						// Grant role
						if (grant) {
							PluginTask.checkIfThreadTaskAborted();
							grantRole (executor, newActorName, actorType, dsRoleName);
						}
						w.add("actor", new String[]{"type", actorType,
								"role-to-grant", dsRoleName, "granted", grant.toString()},
								newActorName);

					}
				}
			}
		}

	}

	public static Boolean isActorDomainEnabled (ServiceExecutor executor, String actorName) throws Throwable {
		int idx = actorName.indexOf(":");
		String domain = actorName.substring(0,idx);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		XmlDoc.Element r = executor.execute("authentication.domain.describe", dm.root());
		return r.booleanValue("domain/@enabled",  true);
	}

	private static void grantRole (ServiceExecutor executor, String actorName,
			String actorType, String roleToGrant) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", actorName);
		dm.add("type",actorType);
		dm.add("role", new String[] {"type", "role"}, roleToGrant);
		executor.execute("actor.grant", dm.root());
	}

	private static Collection<XmlDoc.Element> findGrantedActors (ServiceExecutor executor,
			XmlDoc.Element role) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.push("role", new String[] {"type", "role"}, role.value("name"));
		XmlDoc.Element r = executor.execute("actors.granted", dm.root());
		return r.elements("actor");
	}


	private static Collection<XmlDoc.Element> describeRoles (ServiceExecutor executor, String namespace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		XmlDoc.Element r = executor.execute("authorization.role.describe", dm.root());
		return r.elements("role");
	}

	private static void addMissingRoles (ServiceExecutor executor, 
			String vnNamespace, String dsNamespace, XmlWriter w) throws Throwable {

		Collection<XmlDoc.Element> vnRoles = describeRoles (executor, vnNamespace);
		Collection<XmlDoc.Element> dsRoles = describeRoles (executor, dsNamespace);
		//
		w.push("namespace", new String[] {"namespace-source", vnNamespace},
				dsNamespace);
		if (vnRoles!=null) {
			for (XmlDoc.Element vnRole : vnRoles) {
				String t = vnRole.value("name");
				int idx = t.indexOf(":");
				String vnName = t.substring(idx+1);
				String description = vnRole.value("description");
				Boolean found = false;
				for (XmlDoc.Element dsRole : dsRoles) {
					String t2 = dsRole.value("name");
					int idx2 = t2.indexOf(":");
					String dsName = t2.substring(idx2+1);
					if (dsName.equals(vnName)) {
						found = true;
						break;
					} 
				}

				// If we did not find this VicNode role, then create it in ds
				if (!found) {
					createRole (executor, dsNamespace, vnName, description, w);
				}

			}
		}
		w.pop();
	}

	private static void createRole (ServiceExecutor executor, String namespace,
			String name, String description, XmlWriter w) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		String roleName = namespace+":"+name;
		dm.add("role", roleName);
		if (description!=null) {
			dm.add("description", description);
		}
		executor.execute("authorization.role.create", dm.root());
		w.add("role-added", new String[] {"description", description}, roleName); 
	}



}
