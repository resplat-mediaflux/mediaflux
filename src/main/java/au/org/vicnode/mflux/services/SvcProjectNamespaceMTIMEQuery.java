/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.DateType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.DateUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.Properties;

/**
 * 
 * @author nebk
 *
 */
public class SvcProjectNamespaceMTIMEQuery extends PluginService {

	public static final String SERVICE_NAME = 
			Properties.SERVICE_ROOT + "project.namespace.mtime.query";

	private Interface _defn;

	public SvcProjectNamespaceMTIMEQuery() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("date", DateType.DEFAULT,
				"See if there are any assets modified since this date.",
				1, 1));
		//
		SvcProjectDescribe.addProdType (_defn);

	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "List projects with no assets modified (mtime) since the given date.";
	}

	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String projType = args.stringValue("prod-type", Properties.PROJECT_TYPE_ALL);
		Date date = args.dateValue("date");
		String sdate = DateUtil.formatDate(date, true, false);
		// List of projects
		Collection<String> projectIDs = Project.projectIDs(executor(), true, true);
		if (projectIDs.size() == 0) {
			return;
		}
		for (String projectID : projectIDs) {
			PluginTask.checkIfThreadTaskAborted();
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
			if (Project.keepOnProjectType(ph, projType)) {
				list (executor(), ph, sdate, w);
			}
		}
	}

	private void list (ServiceExecutor executor, ProjectHolder ph, String sdate, XmlWriter w) throws Throwable {
		String namespace = ph.nameSpace();
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", "namespace>='" + namespace + "' and mtime>='" + sdate +"'");
		dm.add("action", "count");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		String n = r.value("value");
		if (n.equals("0")) {
			// See if it has no assets at all
			String n2 = countAll (executor, namespace);
			w.add("project-id", new String[] {"n-total", n2}, ph.id());
		}
	}

	private String countAll (ServiceExecutor executor, String namespace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", "namespace>='" + namespace + "'");
		dm.add("action", "count");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		return r.value("value");
	}		
}
