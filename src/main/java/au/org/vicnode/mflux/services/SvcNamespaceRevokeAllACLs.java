/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;



import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.NameSpaceUtil;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcNamespaceRevokeAllACLs extends PluginService {


	private Interface _defn;

	public SvcNamespaceRevokeAllACLs()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The project child namespace.", 1, 1));
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "For a given namespace which must be a project child namespace (can't be the root namespace of a project), recursively revoke all ACLs on the given namespace and all  child namespaces and assets.  Use with extreme caution.  Will generate an error if the given namespace is not a project child namespace.  The caller will need write privilege on all the namespaces and assets.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "namespace.revoke.ACL.all";
	}

	
	public boolean canBeAborted() {

		return true;
	}


	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String nameSpace = args.value("namespace");

		// Validate the namespace. Must be a child of a project namespace.
		// Exception if error.
		Project.validateProjectNamespace(executor(), nameSpace, true, true);

		// Recursively revoke all ACLs
		NameSpaceUtil.recursivelyRevokeAllACLs(executor(), nameSpace);
	}

}
