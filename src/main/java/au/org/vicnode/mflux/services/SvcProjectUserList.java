/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;

public class SvcProjectUserList extends PluginService {


	private Interface _defn;

	public SvcProjectUserList()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT, "The ID of the project. Should be of the form 'proj-<name>-<cid>'.", 1, 1));
		_defn.add(new Interface.Element("all", BooleanType.DEFAULT, "List all roles (default false) instead of only the ones which are held by a user", 0, 1));
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "List users of a project.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.user.list";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		String projectID = args.value("project-id");
		Boolean all = args.booleanValue("all", false);
		String t[] = Project.validateProject(executor(), projectID, false);
		projectID = t[1];
		w.add(Project.listUsers(executor(), projectID, all, false));

	}
}
