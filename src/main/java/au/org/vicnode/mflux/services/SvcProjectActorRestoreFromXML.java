/**
 * @author Neil Killeen
 * <p>
 * Copyright (c) 2016, The University of Melbourne, Australia
 * <p>
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;



import java.io.File;
import java.io.InputStreamReader;
import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.UrlType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.User;
import au.org.vicnode.mflux.plugin.util.Util;




public class SvcProjectActorRestoreFromXML extends PluginService {


	private Interface _defn = null;

	public SvcProjectActorRestoreFromXML() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("user", BooleanType.DEFAULT, "Apply the process to users (default false).", 0, 1));
		_defn.add(new Interface.Element("identity", BooleanType.DEFAULT, "Apply the process to secure identity tokens (default false).", 0, 1));
		_defn.add(new Interface.Element("url", UrlType.DEFAULT, "Supply the server-side path for the XML file.  Should be of the form file:<path>", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Reads the XML file produced by service vicnode.project.describe :identity true and restores the users and identity tokens to the project with their specified roles.  If there are multiple projects described in the XML file, all will be handled.";
	}

	@Override
	public int minNumberOfInputs() {
		return 0;
	}

	@Override
	public int maxNumberOfInputs() {
		return 1;
	}


	public String name() {
		return Properties.SERVICE_ROOT + "project.actor.restore.from.xml";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		Boolean doUser = args.booleanValue("user", false);
		Boolean doIdentity = args.booleanValue("identity", false);
		String url = args.value("url");
		PluginService.Input input = null;
		if (inputs!=null) {
			input = inputs.input(0);
		}
		if (input!=null&&url!=null) {
			throw new Exception("You must specify only one of :in or :url");
		}
		if (input==null && url==null) {
			throw new Exception("You must specify at least one of :in or :url");
		}
		try {
			// Read
			XmlDoc.Element actors = null;
			if (input!=null) {
				actors = new XmlDoc().parse(new InputStreamReader(input.stream()));
			} else {
				File f = new File(url.substring(5));
				actors = Util.parseXmlFile(f);
			}

			Collection<XmlDoc.Element> projects = actors.elements("project");
			if (projects!=null) {
				String standardUserRoleName = Properties.getStandardUserRoleName(executor());

				for (XmlDoc.Element project : projects) {
					w.push("project");
					w.add("id", project.value("id"));
					if (doUser) {
						w.push("users");
						Collection<XmlDoc.Element> roles = project.elements("users/role");
						if (roles!=null) {
							for (XmlDoc.Element role : roles) {
								String roleName = role.value("@name");
								Collection<String> users = role.values("user");
								if (users!=null) {
									for (String user : users) {
										w.push("user", new String[] {"name", user});
										// Grant project role
										User.grantRevokeRole(executor(), user, roleName, true);
										w.add("role", new String[] {"granted", "true"}, roleName);
										// Grant standard-user
										User.grantRevokeRole(executor(), user, standardUserRoleName, true);
										w.add("role", new String[] {"granted", "true"}, standardUserRoleName);
										w.pop();
									}
								}
							}
						}
						w.pop();
					}

					if (doIdentity) {
						w.push("identity-tokens");
						Collection<XmlDoc.Element> roles = project.elements("identity-tokens/role");
						if (roles!=null) {
							for (XmlDoc.Element role : roles) {
								String roleName = role.value("@name");
								Collection<String> tokenActorNames = role.values("identity-token/@actor-name");
								if (tokenActorNames!=null) {
									for (String tokenActorName : tokenActorNames) {
										w.push("identity-token", new String[] {"name", tokenActorName});
										// Grant project role
										grantRoleToToken(executor(), tokenActorName, roleName);											//
										w.add("role", new String[] {"granted", "true"}, roleName);
										grantRoleToToken(executor(), tokenActorName, standardUserRoleName);											//
										w.add("role", new String[] {"granted", "true"}, standardUserRoleName);
										w.pop();
									}
								}
							}
						}
						w.pop();
					}
					//
					w.pop();
				}
			}


		} finally {
			if (input!=null) {
				input.close();
			}
		}
	}


	static private void grantRoleToToken (ServiceExecutor executor, String token, String role) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", token);
		dm.add("type", "identity");
		dm.add("role", new String[] {"type", "role"}, role);
		executor.execute("actor.grant", dm.root());
	}
}
