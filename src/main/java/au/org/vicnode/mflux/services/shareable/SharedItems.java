package au.org.vicnode.mflux.services.shareable;

import arc.xml.XmlDoc;

public class SharedItems {

	public final DownloadShareable downloadShareable;
	public final DownloadLink downloadLink;
	public final Interaction userInteraction;
	public final String uploadNS;
	public final XmlDoc.Element uploadShareable;
	public final XmlDoc.Element uploadArgs;
	public final XmlDoc.Element shareOptions;

	public SharedItems(DownloadShareable downloadShareable, DownloadLink downloadLink, Interaction userInteraction,
			String uploadNS, XmlDoc.Element uploadShareable, XmlDoc.Element uploadArgs, XmlDoc.Element shareOptions) {
		this.downloadShareable = downloadShareable;
		this.downloadLink = downloadLink;
		this.userInteraction = userInteraction;
		this.uploadNS = uploadNS;
		this.uploadShareable = uploadShareable;
		this.uploadArgs = uploadArgs;
		this.shareOptions = shareOptions;
	}

}
