/** 
* @author Neil Killeen
*
* Copyright (c) 2016, The University of Melbourne, Australia
*
* All rights reserved.
*/
package au.org.vicnode.mflux.services.replication;


import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.ProjectHolder;
import au.org.vicnode.mflux.plugin.util.ProjectReplicate;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.services.SvcProjectDescribe;

public class SvcProjectReplicateQueueList extends PluginService {


	private Interface _defn;

	public SvcProjectReplicateQueueList()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",
				0, 1));
		SvcProjectDescribe.addProdType (_defn);
	}
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Lists projects that have the standard replication asset processing queue set.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.replicate.queue.list";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		String prodType = args.stringValue("prod-type",  Properties.PROJECT_TYPE_ALL);
		String keyString = args.value("contains");
		if (keyString != null) {
			keyString = keyString.toUpperCase();
		}

		// List  projects
		Collection<String> projectIDs = Project.projectIDs(executor(), true, true);

		// List 
		for (String projectID : projectIDs) {
			ProjectHolder ph = new ProjectHolder(executor(), projectID, false);
			if (Project.keepOnProjectType(ph, prodType) &&
				Project.keepOnKeyString(ph.id(), keyString)) {
				String q = ProjectReplicate.isProjectReplicated(ph);
				if (q!=null) {
					w.add("project", new String[] {"replication-queue", q}, ph.id());
				}
			}
		}
	}
}
