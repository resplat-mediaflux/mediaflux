/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services.datamover;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import au.org.vicnode.mflux.plugin.util.MailHandler;
import au.org.vicnode.mflux.plugin.util.Project;
import au.org.vicnode.mflux.plugin.util.Properties;
import au.org.vicnode.mflux.plugin.util.Util;
import au.org.vicnode.mflux.services.SvcProjectDescribe;

public class SvcProjectDMOperatorExpiryNotification extends PluginService {

	private static final String URL_SUFFIX = "/dm-del-manage?id=";
	
	
	// This service is granted permission to access ds-admin:project-namespace-map
	private Interface _defn;
	private static Boolean allowPartialCID = true;


	public SvcProjectDMOperatorExpiryNotification()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("email", StringType.DEFAULT, 
				"If an email is provided, send the notification here instead of the email provided in the namespace meta-data ($NS_ADMIN:Project/data-mover-expiry/email[@type='destroy-expired']).", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("expiry-offset", IntegerType.DEFAULT, "Subtract this many days from the actual acquisition time to force data to look they have expired. For testing only. Defaults to 0.", 0, 1));
		SvcProjectDescribe.addProjectIDs(_defn, allowPartialCID);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Sends a notification (by email) to recipients when DataMover " +
				"upload transactional namespaces have expired (meaning the data " +
				"are older than the lifetime of the download links that were " +
				"dispatched to end users) and should be destroyed. The notification "+
				"includes an authenticated web-page URL with which they can destroy " +
				"the data." +
				"Run this service in a regular scheduled job.  The service looks " +
				"recursively for manifest assets in all child namespaces (except " +
				"those excluded by the meta-data element " +
				"data-mover-expiry/exclude-child-path) to determine if the " + 
				"data have expired and are candidates for destruction.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.DM.operator.expiry.notification";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute (Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		String email = args.value("email");

		// Generate  list of expired transactional namespaces per project
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add(args.element("project-id"));
		dm.add(args.element("expiry-offset"));
		dm.add("sort", true);
		
		// This service also uses allowPartialCID = true. Very ugly coupling!
		XmlDoc.Element r = executor().execute(Properties.SERVICE_ROOT+"project.DM.operator.expiry.list", dm.root());
		if (r==null) {
			return;
		}

		// Parse results
		Collection<XmlDoc.Element> projects = r.elements("project");
		if (projects==null) {
			return;
		}

		// Iterate over each project.  Each project receives a separate
		// email notification
		for (XmlDoc.Element project : projects) {
			String projectID = project.value("id");
			String cid = Project.citeableID(projectID);	
			// CIDs maybe 3 (older servers) or 4 (new servers where the schema is included as an element)
			String[] n = cid.split("\\.");
			int l = n.length;
			if (l!=3) {
				throw new Exception("Illegal project CID found ("+cid+") extracted from " + projectID);
			}
			String id = n[l-1];    // Last component of CID
			//
			XmlDoc.Element notExpired = project.element("not-yet-expired");
			XmlDoc.Element expired = project.element("expired");
			XmlDoc.Element noExpiry = project.element("missing-download-expiry-date");

			if (expired!=null  || noExpiry!=null) {
				// Fetch the relevant notification recipient emails
				Collection<String> emails = project.values("email[@type='destroy-expired']");

				// We must have either an email address extracted from the project
				// namespace meta-data or from the email argument of this service.
				if (emails==null && email==null) {
					throw new Exception ("There was no configured email address to send the notification to for project " + projectID);
				}
				
				// Create URL
				String serverName = Properties.getServerProperty(executor(), "asset.shareable.address");
				String URL = serverName + URL_SUFFIX +id;
				
				// Notify operators
				notify (executor(), email, URL, emails, notExpired, expired, noExpiry, w);
			}
		}
	}

	private void notify (ServiceExecutor executor, String email,
			String URL, Collection<String> emails, 
			XmlDoc.Element notExpired, XmlDoc.Element expired, 
			XmlDoc.Element noExpiry, XmlWriter w) throws Throwable {

		String body = "<html><head><title>Project Data Mover Upload Expiry Check for"+"</title></head><body>";
		body += "<br/><p>Dear Mediaflux Project Administrator,<br/><br/>";
		body += "Summary of transient Data Mover uploaded and expired transactional namespaces assessed for destruction.<br/><br/>";
		body += "<p></p>";

		//
		if (expired!=null) {
			int n = expired.elements("namespace").size();
			if (n>1) {
				body += "There are " + n + " candidate expired transactional namespaces." + "<br/><br/>";
				body += "Please authenticate to this web page to manage them." + "<br/><br/>";			
			} else {
				body += "There is 1 candidate expired transactional namespace." + "<br/>";
				body += "Please authenticate to this web page to manage it." + "<br/><br/>";			
			}
			body += URL + "<br/><br/>";			
		} else {
			body += "There are no candidate expired transactional namespaces." + "<br/><br/>";
		}

		if (noExpiry!=null) {
			Collection<String> namespaces = noExpiry.values("namespace");
			body += "<p></p>";
			body += "The following namespaces are from the DataMover but the duration of the download links <br/>";
			body += "  could not be extracted from the manifest asset.<br/>";
			body += "  They are likely failed uploads and should be followed up, and likely destroyed. <br/><br/>";
			for (String namespace : namespaces) {
				body += namespace + "<br/>";
			}
			body += "<p></p>";
		}
				
		//
		body += "<br/>regards<br/> Mediaflux Operations Team";
		body += "</body></html>";

		// Send message to recipients
		String subject = "Transient Data Mover Upload Expired Namespaces";
		String from = Util.getServerProperty(executor, "notification.from");
		if (email!=null) {
			Vector<String> t = new Vector<String>();
			t.add(email);
			MailHandler.sendMessage(executor, t, subject, body, "text/html", false, from, w);
		} else {
			MailHandler.sendMessage(executor, emails, subject, body, "text/html", false, from, w);
		}
	}
}