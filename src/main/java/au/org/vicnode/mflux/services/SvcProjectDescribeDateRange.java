/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package au.org.vicnode.mflux.services;

import java.util.Collection;
import java.util.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.PasswordType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import au.org.vicnode.mflux.plugin.util.Properties;


public class SvcProjectDescribeDateRange extends PluginService {

	private Interface _defn;

	public SvcProjectDescribeDateRange() throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("date", StringType.DEFAULT,
				"A date/time to apply to all asset queries (counting and summing will only find data inclusively before this date) and to inject into the CSV file for when the service was run. If not supplied, today's date is used.", 2,
				2));
		_defn.add(new Interface.Element("project-id", StringType.DEFAULT,
				"The ID of the project. Should be of " + "the form 'proj-<name>-<cid>'. Defaults to all projects.", 0,
				1));
		_defn.add(new Interface.Element("namespace-path", StringType.DEFAULT, "Additional namespaces (which are not part of the projects structure such as the /avarchives) which should be checked. Parameters prod-type and rep-type are irrelevant to this parameter. Defaults to none.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("posix", BooleanType.DEFAULT,
				"Show posix mount point information (default true).",
				0, 1));
		_defn.add(new Interface.Element("users", BooleanType.DEFAULT,
				"Show information about the users (default true). A bit expensive.",
				0, 1));
		_defn.add(new Interface.Element("sum", BooleanType.DEFAULT,
				"Sum project content on primary and DR systems (defaults to false). By default, the namespace report includes the used amount on the primary only. Using this option forces a resum on both primary and DR (slow).",
				0, 1));
		_defn.add(new Interface.Element("email", StringType.DEFAULT, "Send the report to this email address.", 0,
				Integer.MAX_VALUE));
		_defn.add(new Interface.Element("store", StringType.DEFAULT,
				"If supplied, limit the summaries to only projects that use this store (defaults to all stores).", 0,
				1));
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A string to search for in the project IDs to restrict which projects are selected (defaults to all projects).",
				0, 1));
		_defn.add(new Interface.Element("parent", StringType.DEFAULT,
				"The parent namespace to consider when looking for projects. Should start with a '/'. Defaults to all parents.",
				0, 1));

		Interface.Element me = new Interface.Element("csv", XmlDocType.DEFAULT,
				"Creates a CSV with selected VicNode reporting and sends to email address.", 0, 1);
		me.add(new Interface.Element("email", StringType.DEFAULT, "email address", 0, Integer.MAX_VALUE));
		me.add(new Interface.Element("save-on-server", BooleanType.DEFAULT, "If true (default false), will save a file server side named <date>.csv. Will not overwrite.  You must clean up afterwards manually.", 0, 1));

		/*
		 * send csv report to remote HTTP server (POST)
		 */
		Interface.Element httpPost = new Interface.Element("post", XmlDocType.DEFAULT,
				"Send CSV report to remote HTTP server using HTTP POST.", 0, 1);

		httpPost.add(new Interface.Element("ssl", BooleanType.DEFAULT,
				"Is the remote server has SSL(HTTPS) enabled? Defaults to true.", 0, 1));
		httpPost.add(new Interface.Element("host", StringType.DEFAULT, "Remote HTTP server host", 1, 1));
		httpPost.add(new Interface.Element("username", StringType.DEFAULT,
				"Username to access the remote HTTP server. If token is supplied, username and password are ignored. Otherwise, username and password are used to retrieve a auth token from remote HTTP server.",
				0, 1));
		httpPost.add(new Interface.Element("password", PasswordType.DEFAULT,
				"Password to access the remote HTTP server. If token is supplied, username and password are ignored. Otherwise, username and password are used to retrieve a auth token from remote HTTP server.",
				0, 1));
		httpPost.add(new Interface.Element("token", PasswordType.DEFAULT,
				"Auth token to access the remote HTTP server. If token is supplied, username and password are ignored. Otherwise, username and password are used to retrieve a auth token from remote HTTP server.",
				0, 1));
		me.add(httpPost);
		_defn.add(me);

		_defn.add(new Interface.Element("sort", BooleanType.DEFAULT,
				"Sort projects by name (the default is to sort by CID).", 0, 1));
		_defn.add(new Interface.Element("DR", BooleanType.DEFAULT,
				"Attempt to discover (default true) information from the Disatser Recovery server.", 0, 1));
		_defn.add(new Interface.Element("multi-version", BooleanType.DEFAULT,
				"Count and sum the content of assets with multiple versions and with one version only (default false).", 0, 1));
		_defn.add(new Interface.Element("debug", BooleanType.DEFAULT,
				"Print information to server log (default false).", 0, 1));
		//
		SvcProjectDescribe.addProdType (_defn);
		SvcProjectDescribe.addRepType (_defn);
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Returns desciptions of all or specified projects.  The report can be 1) sent as email, 2) saved as XML in a client side file.    3) converted to CSV and send to an email address. You can abort the service as it accumulates information. The check point is between projects.";
	}

	public String name() {
		return Properties.SERVICE_ROOT + "project.describe.in.date.range";
	}

	public boolean canBeAborted() {

		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		Collection<Date> t = args.dateValues("date");
		Date[] dates = t.toArray(new Date[t.size()]);
		
		// Iterate over date range
		LocalDate startDate = dates[0].toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate endDate = dates[1].toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
			PluginTask.checkIfThreadTaskAborted();
			w.push("date");
			w.add("date", date);
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add(args.element("project-id"));
			dm.add(args.element("namespace-path"));
			dm.add(args.element("multi-version"));
			dm.add(args.element("posix"));
			dm.add(args.element("DR"));
			dm.add(args.element("sum"));
			dm.add(args.element("sort"));
			dm.add(args.element("users"));
			dm.add(args.element("debug"));
			dm.add(args.element("email"));
			dm.add(args.element("rep-type"));
			dm.add(args.element("prod-type"));
			dm.add(args.element("store"));
			dm.add(args.element("contains"));
			dm.add(args.element("parent"));
			dm.add(args.element("csv"));
			dm.add(args.element("post"));
			dm.add("date", date.format(DateTimeFormatter.ofPattern("dd-MMM-YYYY")));
			XmlDoc.Element r = executor().execute(Properties.SERVICE_ROOT + "project.describe", dm.root());
			w.addAll(r.elements());
			w.pop();
		}
	}
}
